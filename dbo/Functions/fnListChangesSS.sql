﻿
     

 

CREATE FUNCTION [dbo].[fnListChangesSS]

(

      @LocationID int,

      @UpdateDate datetime

)

RETURNS varchar(MAX)

AS

/*

      UNIT TESTING

      ------------

      SELECT dbo.fnListChangesSS(1133 , '2012-02-23 12:50:40.347')

      SELECT dbo.fnListChangesSS(2152 , '2012-02-23 12:58:16.677')

*/

BEGIN

DECLARE @Changes varchar(MAX)

SELECT @Changes = ''

SELECT @Changes = @Changes +  FieldName + ', '

      FROM [Audit]

      WHERE TableName='Location' AND PrimaryKeyValue = @LocationID

      and DateAdded = @UpdateDate
      AND [Audit].FieldName !='LastUpdatedUserID'
 
if LEN(@Changes)>1
SELECT @Changes = SUBSTRING(@Changes, 1, LEN(@Changes) - 1)
 

RETURN @Changes

END

