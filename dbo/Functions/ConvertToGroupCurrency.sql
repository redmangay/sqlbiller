﻿
create  function ConvertToGroupCurrency
(
@Value float, @ProjectRate float, @GroupRate  float
)
returns float
as

begin

declare   @ConvertToGroupCurrency  float
  set @ConvertToGroupCurrency = 0
  
  If IsNull(@Value,0) =0 
  begin
  set @ConvertToGroupCurrency = 0
  end
  
  else
  begin
   If IsNull(@GroupRate,0) =0
   set @GroupRate = 1
   If IsNull(@ProjectRate,0)=0
   set @ProjectRate = 1

  
  
   set @GroupRate = Round(@GroupRate, 2)
   set @ProjectRate = Round(@ProjectRate, 2)
   set  @ConvertToGroupCurrency = Round(@Value * (@GroupRate / @ProjectRate), 2)
  
  
  end
  
  
Set  @ConvertToGroupCurrency=  @ConvertToGroupCurrency

return @ConvertToGroupCurrency
end
