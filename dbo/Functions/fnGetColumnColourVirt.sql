﻿

CREATE FUNCTION [dbo].[fnGetColumnColourVirt]
(
	@record dbo.TDBRecord READONLY
	,@nID INT
	,@Context varchar(25)
)
RETURNS varchar(10)
AS
BEGIN
	
	DECLARE @sReturn VARCHAR(10);	
	DECLARE @sSystemName VARCHAR(50);
	DECLARE @sEachConrollingRecordValue VARCHAR(MAX);
	DECLARE @Sql NVARCHAR(4000)
	
	DECLARE T_Cursor CURSOR FOR
	SELECT ControllingColumnID,Operator,Value,ColourText FROM ColumnColour
	WHERE ID=@nID AND Context=@Context ORDER BY ColumnColourID;
	
	DECLARE @nControllingColumnID INT;
	DECLARE @sOperator VARCHAR(25);
	DECLARE @sValue NVARCHAR(250);
	DECLARE @sColour VARCHAR(10);
	
	DECLARE @ColumnType varchar(20)
	
	
	OPEN T_Cursor;
	FETCH NEXT FROM T_Cursor INTO @nControllingColumnID,@sOperator,@sValue,@sColour;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
		  SET @sEachConrollingRecordValue=NULL;
		  SET @sSystemName=NULL;
		  SET @Sql=NULL;
		  SELECT @sSystemName=SystemName,@ColumnType=ColumnType FROM [Column] WHERE ColumnID=@nControllingColumnID
		  
		 
		  SELECT @sEachConrollingRecordValue=[dbo].[fnGetRecordValueBySysVirt](@record, @sSystemName)
		  
		  IF [dbo].[fnCompareValuesForColour](@sEachConrollingRecordValue,@sValue,@sOperator,@ColumnType)=1
			BEGIN
				SET @sReturn=@sColour
				BREAK;
			END
		  
		  FETCH NEXT FROM T_Cursor INTO @nControllingColumnID,@sOperator,@sValue,@sColour;
	   END;
	CLOSE T_Cursor;
	DEALLOCATE T_Cursor;
	
	RETURN @sReturn;   
	
END



