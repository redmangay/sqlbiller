﻿ 

 

CREATE FUNCTION [dbo].[fnListChanges_RecordCR]

(
      @RecordID int,
      @UpdateDate datetime

)

RETURNS varchar(MAX)

AS

/*

      UNIT TESTING

      ------------

      SELECT dbo.fnListChanges_RecordCR(2152 , '2012-02-23 12:50:40.347')

      SELECT dbo.fnListChanges_RecordCR(2152 , '2012-02-23 12:58:16.677')

*/

BEGIN

DECLARE @Changes varchar(MAX)

SELECT @Changes = ''

SELECT  TOP 1 @Changes = NewValue 
      FROM [Audit]
      WHERE TableName='Record' AND PrimaryKeyValue = @RecordID
      AND [Audit].DateAdded = @UpdateDate     
      AND [Audit].FieldName ='ChangeReason'       
 

RETURN @Changes

END

