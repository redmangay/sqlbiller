﻿CREATE FUNCTION [dbo].[fnListChangesSC]
(
      @ColumnID int,
      @UpdateDate datetime
)
RETURNS varchar(MAX)
AS
BEGIN
	DECLARE @Changes varchar(MAX)
	DECLARE @ColumnDisplayName VARCHAR(100)

	SELECT @ColumnDisplayName = DisplayName
		FROM [Column]
		WHERE ColumnID = @ColumnID

	SELECT @Changes = ''

	SELECT @Changes = @Changes +  FieldName + ', '
		  FROM [Audit]
		  WHERE TableName='Column' AND  PrimaryKeyValue = CONVERT(varchar(1000), @ColumnID)
		  and DateAdded = @UpdateDate
		  AND [Audit].FieldName !='LastUpdatedUserID'
 
	if LEN(@Changes)>1
	SELECT @Changes = SUBSTRING(@Changes, 1, LEN(@Changes) - 1) + ' for Field ' + @ColumnDisplayName
 
 	RETURN @Changes
END
