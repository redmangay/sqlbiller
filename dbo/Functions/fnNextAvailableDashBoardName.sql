﻿
CREATE FUNCTION [dbo].[fnNextAvailableDashBoardName]
(	
	@sBaseDocumentText varchar(max),@nAccountID int,@nDocumentID int=null
)
RETURNS varchar(4000)
AS
	/*
		UNIT TESTING 
		 select dbo.[fnNextAvailableDashBoardName]('Default',24800,null)
		 select dbo.[fnNextAvailableDashBoardName]('Default 4',24800,null)
		  select dbo.[fnNextAvailableDashBoardName]('D1',24800,646)
		  select dbo.[fnNextAvailableDashBoardName]('D1',24800,NULL)

		 
	*/
BEGIN
	-- Declare the return variable here
	DECLARE @sResult varchar(max)
	DECLARE @nEditDocumentID INT=-1
	
	IF @nDocumentID IS NOT NULL
		SET @nEditDocumentID=@nDocumentID
	
	IF NOT EXISTS(SELECT * FROM [Document] WHERE ForDashBoard=1 AND AccountID=@nAccountID  AND DocumentID NOT IN(@nEditDocumentID) AND DocumentText = @sBaseDocumentText) 
		SET @sResult = @sBaseDocumentText 
	ELSE
	BEGIN
		DECLARE @counter int
		SET @counter = 2

		WHILE @counter < 1000
		BEGIN
			
			IF EXISTS(SELECT * FROM [Document] WHERE ForDashBoard=1 AND AccountID=@nAccountID AND DocumentID NOT IN(@nEditDocumentID) AND DocumentText = @sBaseDocumentText + ' ' + CAST(@counter AS varchar(10)))
				SET @counter = @counter + 1
			ELSE 
			BEGIN
				SET @sResult = @sBaseDocumentText + ' ' +CAST(@counter AS varchar(10))
				SET @counter = 1000 -- EXIT
			END
		END 
	END
	-- Return the result of the function
	RETURN @sResult

END


