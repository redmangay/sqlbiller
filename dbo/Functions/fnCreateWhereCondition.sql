﻿​CREATE FUNCTION [dbo].[fnCreateWhereCondition]
(
	@Operator varchar(MAX) =  'contains' --'greaterthan' 'lessthan' 'empty'  'notempty' 
	,@TableName varchar(MAX) = 'Record'
	,@FieldName varchar(MAX) 
	,@CompareValue varchar(MAX)
	,@ColumnType varchar(MAX) = 'string'
)
RETURNS varchar(MAX)
AS
BEGIN
	DECLARE @WhereCondition varchar(MAX) = ''

	IF @Operator = 'contains'
	BEGIN
		SET	@WhereCondition = ' AND ' + @TableName + '.' + @FieldName + ' NOT LIKE ''%' +  @CompareValue + '%'''
	END
	ELSE IF @Operator = 'greaterthan' 
	BEGIN
		IF @ColumnType = 'date'
		BEGIN
			SET @WhereCondition = ' AND ( IsDate(' + @TableName + '.' + @FieldName + ')=1 AND CONVERT(Datetime, ' + @TableName + '.' + 
			@FieldName + ',103) < CONVERT(Datetime,''' + @CompareValue +''',103) )'
		END
		ELSE
		BEGIN
			SET @WhereCondition = ' AND ( IsNumeric(' + @TableName + '.' + @FieldName + ')=1 AND  CONVERT(decimal(20,10),' + @TableName + '.' + 
			@FieldName + ') < ' + CONVERT(VARCHAR(MAX), ISNULL(DBO.[dbgNum](@CompareValue),0))  + ')'
		END
	END

	ELSE IF @Operator = 'lessthan' 
	BEGIN
	IF @ColumnType = 'date'
		BEGIN
			SET @WhereCondition = ' AND ( IsDate(' + @TableName + '.' + @FieldName + ')=1 AND CONVERT(Datetime, ' + @TableName + '.' + 
			@FieldName + ',103) > CONVERT(Datetime,''' + @CompareValue +''',103) )'
		END
		ELSE
		BEGIN
		SET @WhereCondition = ' AND ( IsNumeric(' + @TableName + '.' + @FieldName + ')=1 AND  CONVERT(decimal(20,10),' + @TableName + '.' + 
		@FieldName + ') > ' + CONVERT(VARCHAR(MAX), ISNULL(DBO.[dbgNum](@CompareValue),0))  + ')'
		END
	END
	ELSE IF @Operator = 'empty' 
	BEGIN
		SET @WhereCondition = ' AND ((' + @TableName + '.' + @FieldName + ' IS NOT NULL) OR (LEN(' + @TableName + '.' + @FieldName + ') > 0))' 
	END

	ELSE IF @Operator = 'notempty' 
	BEGIN
		SET @WhereCondition = ' AND ((' + @TableName + '.' + @FieldName + ' IS NULL) OR (LEN(' + @TableName + '.' + @FieldName + ') = 0))' 
	END
	ELSE 
	BEGIN
		SET @WhereCondition = ' AND ' + @TableName + '.' + @FieldName + ' NOT IN (''' + @CompareValue + ''')' 
	END

	RETURN @WhereCondition
END​
