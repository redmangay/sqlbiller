﻿
CREATE FUNCTION [dbo].[GetReportDates]
(
	@ReportID int
)
RETURNS 
	@ReportDates TABLE 
	(
		DateFrom datetime,
		DateTo datetime
	)
AS
BEGIN
	DECLARE @PeriodID int = 0
	DECLARE @StartDate datetime
	DECLARE @EndDate datetime
	DECLARE @Today datetime = GETDATE()

	SELECT @PeriodID = ReportDates, @StartDate = StartDate, @EndDate = EndDate 
		FROM [dbo].[Report] 
		WHERE ReportID = @ReportID

	INSERT INTO @ReportDates
		(DateFrom, DateTo)
	VALUES
	(
		CASE @PeriodID
			WHEN 1 THEN @StartDate
			WHEN 2 THEN DATEADD(DAY, -30, @Today)
			WHEN 3 THEN DATEADD(DAY, -60, @Today)
			WHEN 4 THEN DATEADD(DAY, -90, @Today)
			WHEN 5 THEN DATEADD(YEAR, -1, @Today)
			WHEN 6 THEN DATEFROMPARTS(YEAR(@Today), 1, 1)
		END,
		CASE @PeriodID
			WHEN 1 THEN @EndDate
			WHEN 2 THEN @Today
			WHEN 3 THEN @Today
			WHEN 4 THEN @Today
			WHEN 5 THEN @Today
			WHEN 6 THEN @Today
		END
	)

	RETURN 
END
