﻿
CREATE FUNCTION [dbo].[fnNextAvailableReportName]
(	
	@sBaseReportName varchar(max),@nAccountID int,@nReportID int=null
)
RETURNS varchar(4000)
AS
	/*
		UNIT TESTING 
		 select dbo.fnNextAvailableReportName('Student',2721,null)
		 select dbo.fnNextAvailableReportName('Student Child Summary',2721,null)
		  select dbo.fnNextAvailableReportName('Student Child Summary',2721,654)
		select dbo.fnNextAvailableReportName('Student Summary',2721,654)

		 
	*/
BEGIN
	-- Declare the return variable here
	DECLARE @sResult varchar(max)
	DECLARE @nEditReportID INT=-1
	
	IF @nReportID IS NOT NULL
		SET @nEditReportID=@nReportID
	
	IF NOT EXISTS(SELECT * FROM [Report] WHERE AccountID=@nAccountID  AND ReportID NOT IN(@nEditReportID) AND ReportName = @sBaseReportName)
		SET @sResult = @sBaseReportName 
	ELSE
	BEGIN
		DECLARE @counter int
		SET @counter = 2

		WHILE @counter < 1000
		BEGIN
			
			IF EXISTS(SELECT * FROM [Report] WHERE AccountID=@nAccountID AND ReportID NOT IN(@nEditReportID) AND ReportName = @sBaseReportName + ' ' + CAST(@counter AS varchar(10)))
				SET @counter = @counter + 1
			ELSE 
			BEGIN
				SET @sResult = @sBaseReportName + ' ' +CAST(@counter AS varchar(10))
				SET @counter = 1000 -- EXIT
			END
		END 
	END
	-- Return the result of the function
	RETURN @sResult

END


