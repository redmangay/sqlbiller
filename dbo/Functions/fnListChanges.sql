﻿ 

 

CREATE FUNCTION [dbo].[fnListChanges]

(
      @RecordID int,
      @UpdateDate datetime

)

RETURNS varchar(MAX)

AS

/*

      UNIT TESTING

      ------------

      SELECT dbo.fnListChanges(2152 , '2012-02-23 12:50:40.347')

      SELECT dbo.fnListChanges(2152 , '2012-02-23 12:58:16.677')

*/

BEGIN

DECLARE @Changes varchar(MAX)

SELECT @Changes = ''

--SELECT @Changes = @Changes + CASE WHEN [Column].DisplayTextDetail IS NULL THEN FieldName ELSE [Column].DisplayTextDetail END + ', '
--SELECT @Changes = @Changes +  [Column].DisplayName+ ', '
SELECT @Changes = @Changes + CASE WHEN [Column].DisplayName IS NULL THEN FieldName 
ELSE [Column].DisplayName END + ', '
      FROM [Audit]
      JOIN  [Record] ON [Record].RecordID = @RecordID
      LEFT JOIN [Column] on [Column].TableID = [Record].TableID AND 
      [Column].SystemName = FieldName
      WHERE TableName='Record' AND PrimaryKeyValue = @RecordID
      AND [Audit].DateAdded = @UpdateDate
      AND [Audit].FieldName !='LastUpdatedUserID'
      AND [Audit].FieldName != 'WarningResults' 
      AND [Audit].FieldName != 'ChangeReason' 

if LEN(@Changes)>1
SELECT @Changes = SUBSTRING(@Changes, 1, LEN(@Changes) - 1)

 

RETURN @Changes

END

