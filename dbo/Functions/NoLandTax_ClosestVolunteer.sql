﻿CREATE FUNCTION dbo.NoLandTax_ClosestVolunteer
(
	@nLat real,
	@nLong real
)
RETURNS int
AS
BEGIN
	DECLARE @RecordID int
	SELECT TOP 1 @RecordID = RecordID  
		FROM [Record] V
		WHERE V.TableID = 2101
		AND v.IsActive = 1 
		AND ISNUMERIC(V.V013)=1 
		AND ISNUMERIC(V.V014)=1
		AND V.V019 is NULL 
		ORDER BY SQRT(SQUARE(ABS(CAST(V.V013 as real) - @nLat)) + SQUARE(ABS(CAST(V.V014 as real) - @nLong))) asc

	RETURN @RecordID
END