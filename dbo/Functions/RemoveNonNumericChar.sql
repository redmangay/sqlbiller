﻿CREATE function [dbo].[RemoveNonNumericChar]
(
	@str varchar(500)
)
RETURNS varchar(500)  

BEGIN  
		 

		

    IF @str is null
		RETURN null

		DECLARE @s2 varchar(256) = '', 
		@bOnlyOneDot bit=0,
		@bOnlyOneMinus bit=0,
		@l int = LEN(@str),
		@p int = 1,
		@c int 

	WHILE @p <= @l 
	BEGIN
		SET @c = ASCII(SUBSTRING(@str, @p, 1))
		IF @c BETWEEN 48 AND 57
		SET @s2 = @s2 + CHAR(@c)
		IF @c =46 AND @bOnlyOneDot=0
		BEGIN
			SET @s2 = @s2 + CHAR(@c)
			SET @bOnlyOneDot=1
		END 
		IF @c =45 AND @bOnlyOneMinus=0
		BEGIN
			SET @s2 = @s2 + CHAR(@c)
			SET @bOnlyOneMinus=1
		END 		
		SET @p = @p + 1
	END

	IF LEN(@s2) = 0
		RETURN null

   SET @str = @s2
	DECLARE @startingIndex int = 0  
	WHILE 1=1  
	BEGIN  
		SET @startingIndex= PATINDEX('%[^0-9^.]%',@str)  
		IF @startingIndex <> 0  
		BEGIN  
			SET @str = REPLACE(@str,SUBSTRING(@str,@startingIndex,1),'')  
		END
		ELSE BREAK;   
	END  
	RETURN @str  
END
