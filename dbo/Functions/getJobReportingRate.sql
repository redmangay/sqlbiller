﻿CREATE FUNCTION dbo.getJobReportingRate(@ProdjectNumber VARCHAR(20))
RETURNS FLOAT
AS
BEGIN
	DECLARE @ReturnValue FLOAT
	DECLARE @RptCurrencyRate VARCHAR(50)

	SET @RptCurrencyRate = ISNULL((SELECT RptCurrencyRate FROM Account24918.vProject Project WHERE ProjectNumber=@ProdjectNumber),'0')
	SET @RptCurrencyRate = REPLACE(@RptCurrencyRate,',','')

	SET @ReturnValue = CAST(@RptCurrencyRate AS FLOAT)

	IF @ReturnValue = 0 
		SET @ReturnValue = 1
	ELSE

	SET @ReturnValue = ROUND(@ReturnValue,2)	
	RETURN @ReturnValue
END