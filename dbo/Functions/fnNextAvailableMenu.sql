﻿
CREATE FUNCTION [dbo].[fnNextAvailableMenu]
(	
	@sMenu nvarchar(255),@nAccountID int,@nParentMenuID int=null,@nMenuID int=null
)
RETURNS varchar(4000)
AS
	/*
		UNIT TESTING 
		 select dbo.[fnNextAvailableMenu]('EMD',24800,null,null)
		 select dbo.[fnNextAvailableMenu]('Report 2',24800,3033,null)

		 
	*/
BEGIN
	-- Declare the return variable here
	DECLARE @sResult varchar(max)
	DECLARE @nEditMenuID INT=-1
	
	IF @nMenuID IS NOT NULL
		SET @nEditMenuID=@nMenuID
	
	DECLARE @counter int
			SET @counter = 2

	DECLARE @myParm INT=0 -- @nParentMenuID IS NULL
	
	IF @nParentMenuID IS NOT NULL
		SET @myParm=1
	
	
	--BEGIN
		IF NOT EXISTS(SELECT * FROM [Menu] WHERE IsActive=1 AND AccountID=@nAccountID  AND MenuID NOT IN(@nEditMenuID)
		 AND Menu<>'---' AND  ((@myParm=0 AND ParentMenuID IS NULL ) OR (@myParm=1 AND ParentMenuID=@nParentMenuID )) AND Menu = @sMenu) 
			SET @sResult = @sMenu 
		ELSE
		BEGIN
			
			WHILE @counter < 1000
			BEGIN
				
				IF EXISTS(SELECT * FROM [Menu] WHERE IsActive=1 AND AccountID=@nAccountID AND MenuID NOT IN(@nEditMenuID) 
				AND  Menu<>'---' AND   ((@myParm=0 AND ParentMenuID IS NULL ) OR (@myParm=1 AND ParentMenuID=@nParentMenuID )) AND Menu = @sMenu + ' ' + CAST(@counter AS varchar(10)))
					SET @counter = @counter + 1
				ELSE 
				BEGIN
					SET @sResult = @sMenu + ' ' +CAST(@counter AS varchar(10))
					SET @counter = 1000 -- EXIT
				END
			END 
		END
	
		
	-- Return the result of the function
	RETURN @sResult

END


