﻿CREATE FUNCTION [dbo].[fnCheckSPParameter]
(
	 @sProcedureName varchar(MAX)
	,@sParameterName varchar(MAX)
)
RETURNS bit
AS
BEGIN
	DECLARE @bParameterExists bit

	IF EXISTS (SELECT SCHEMA_NAME(SCHEMA_ID)
			FROM sys.objects AS SO
			INNER JOIN sys.parameters AS P ON SO.OBJECT_ID = P.OBJECT_ID
			WHERE SO.OBJECT_ID IN (SELECT OBJECT_ID FROM sys.objects WHERE TYPE IN ('P','FN'))
			AND SO.name = @sProcedureName
			AND P.name = @sParameterName)
		SET @bParameterExists = 1
	ELSE
		SET @bParameterExists = 0

	RETURN @bParameterExists
END
