﻿
CREATE FUNCTION [dbo].[fnListChangesST]

(
      @TableID int,
      @UpdateDate datetime
)
RETURNS varchar(MAX)
AS
BEGIN
	DECLARE @Changes varchar(MAX)
	DECLARE @ColumnDisplayName varchar(100)
	DECLARE @sTableName varchar(200)

	SELECT TOP 1 @sTableName=TableName FROM [Audit]
		WHERE [Audit].TableID = @TableID
		  AND [Audit].DateAdded = @UpdateDate

	if @sTableName = 'Column'
		SELECT @Changes = @Changes +  FieldName + ', ',
			   @ColumnDisplayName = [Column].DisplayName
			  FROM [Audit]
			  LEFT JOIN [Column] ON CONVERT(int, [Audit].PrimaryKeyValue)=[Column].ColumnID
			  WHERE (TableName='Column')
				AND [Audit].TableID = @TableID
				AND [Audit].DateAdded = @UpdateDate
				AND [Audit].FieldName !='LastUpdatedUserID'	
	else     
		SELECT @Changes = @Changes +  FieldName + ', '
			  FROM [Audit]
			  WHERE TableName=@sTableName
				  AND [Audit].TableID = @TableID
				  AND [Audit].DateAdded = @UpdateDate
				  AND [Audit].FieldName !='LastUpdatedUserID'
				  AND [Audit].FieldName !='UserUpdated'   

	if LEN(@Changes)>1
	SELECT @Changes = SUBSTRING(@Changes, 1, LEN(@Changes) - 1) + IIF(@Changes NOT LIKE 'Deleted%' AND @sTableName='Column', ' for Field ' + IIF(@ColumnDisplayName IS NULL, 'DELETED',@ColumnDisplayName), '')
 

	RETURN @Changes
END
