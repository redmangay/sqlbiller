﻿

CREATE FUNCTION [dbo].[RemoveSpecialChars_IngnoreSymbols]
(
	@s varchar(255)
) 
RETURNS varchar(255)
   WITH SCHEMABINDING

BEGIN
	IF @s is null
		RETURN null

		DECLARE @s2 varchar(255) = '', 
		
		@l int = LEN(@s),
		@p int = 1,
		@c int 

	WHILE @p <= @l 
	BEGIN
		SET @c = ASCII(SUBSTRING(@s, @p, 1))
		IF @c= 46 OR @c BETWEEN 48 AND 57 
		SET @s2 = @s2 + CHAR(@c)
				
		SET @p = @p + 1
	END
	IF LEN(@s2) = 0
		RETURN null
   RETURN @s2
END
