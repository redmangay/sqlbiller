﻿

-- Removes special characters from a string value.
-- All characters except 0-9, a-z and A-Z are removed and
-- the remaining characters are returned.
-- Author: Christian d'Heureuse, www.source-code.biz
-- SELECT [dbo].[RemoveSpecialChars] ('AB [%] test()')
CREATE FUNCTION [dbo].[RemoveSpecialChars]
(
	@s varchar(256)
) 
RETURNS varchar(256)
   WITH SCHEMABINDING

BEGIN
	 if @s is null
      return null
   declare @s2 varchar(256)
   set @s2 = ''
   declare @l int
   set @l = len(@s)
   declare @p int
   set @p = 1
   while @p <= @l begin
      declare @c int
      set @c = ascii(substring(@s, @p, 1))
      --if @c >=32 and @c<=127
		--91=[,93=]
		if  @c=91 
			set @s2 = @s2 + '('
		else if  @c=93 
			set @s2 = @s2 + ')'
	    else
			set @s2 = @s2 + char(@c)
        
      set @p = @p + 1
      end
   if len(@s2) = 0
      return null
   return RTRIM(LTRIM (@s2))
END


