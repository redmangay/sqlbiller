﻿CREATE FUNCTION [dbo].[mUtilEncrypt](@Value AS NVARCHAR(MAX), @Password AS NVARCHAR(MAX))

RETURNS NVARCHAR(MAX)

AS

BEGIN

      DECLARE @Len                  INT

      DECLARE @X                    INT

      DECLARE @Char                 INT

      DECLARE @ReturnValue    NVARCHAR(MAX)

 

      SET @Len = LEN(@Password)

      SET @X = 1

      SET @ReturnValue = @Value

 

      WHILE @X<=LEN(@Value)

      BEGIN

            SET @Char = ASCII(SUBSTRING(@Password, (@X % @Len) - @Len * (CASE WHEN (@X % @Len) = 0 THEN -1 ELSE 0 END), 1))

            SET @ReturnValue = STUFF(@ReturnValue, @X, 1, CHAR(ASCII(SUBSTRING(@Value, @X, 1)) ^ @Char))

            SET @X = @X+1

      END

      RETURN @ReturnValue

END

