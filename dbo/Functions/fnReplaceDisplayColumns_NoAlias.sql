﻿CREATE FUNCTION [dbo].[fnReplaceDisplayColumns_NoAlias]
(
	@ColumnID INT
)
RETURNS varchar(MAX)
AS
BEGIN
	DECLARE @sDisplay varchar(MAX) 
 	DECLARE @nTableID int
	SELECT @sDisplay=DisplayColumn,@nTableID=TableTableID FROM [Column] WHERE ColumnID=@ColumnID
	IF @nTableID<0 RETURN @sDisplay

	DECLARE @Field varchar(254), @Column varchar(254), @nStart int, @nEnd int
	SELECT @nStart = 1
	
	
	WHILE @nStart >= 1 and @nStart < LEN(@sDisplay) 
	BEGIN 
		SELECT @nStart = CHARINDEX('[', @sDisplay, @nStart)
		SELECT @nEnd  = CHARINDEX(']', @sDisplay, @nStart)


		IF @nEnd=0
		BEGIN
			SELECT @Field = SUBSTRING(@sDisplay, @nStart,LEN(@sDisplay))
			SELECT @sDisplay = REPLACE(@sDisplay, @Field,'' )
			--PRINT '@Field:' + @Field
			--PRINT '@sDisplay:' + @sDisplay
		BREAK
		END


		IF @nStart > 0 AND (@nEnd > @nStart)
		BEGIN
			SELECT @Field = SUBSTRING(@sDisplay, @nStart+1, @nEnd-@nStart-1)
			--print '@Field ' + @Field
			select @Column = SystemName from [Column] WHERE TableID = @nTableID and DisplayName = @Field
			--print '@Column: ' + @Column 
			select @sDisplay = REPLACE(@sDisplay, @Field, @Column)
			--PRINT @sDisplay
			--PRINT '@n: ' + CAST(@nStart AS varchar)
			SELECT @nStart = @nStart+1
		END
	END
	SELECT @sDisplay = '''' + REPLACE(@sDisplay, '[', '''+[') + ''''
	SELECT @sDisplay = REPLACE(@sDisplay, ']', ']+''')
	IF LEFT(@sDisplay, 3) = '''''+' set @sDisplay = SUBSTRING(@sDisplay,4,len(@sDisplay)-3)
	IF RIGHT(@sDisplay, 3) = '+''''' set @sDisplay = LEFT(@sDisplay,Len(@sDisplay)-3)
	RETURN @sDisplay
END
