﻿CREATE FUNCTION [dbo].[Ufn_PercentWonByNumberofJobCalculate](@GroupDivision VARCHAR(255), @txtCurrency VARCHAR(255),@TotalNumberOfUnfixedJobs Int,@TotalNumberOfWonJobs Int)

RETURNS VARCHAR(255)

AS

BEGIN
 

      DECLARE @ReturnValue                      VARCHAR(255)

     

      

      If        (@GroupDivision='Currency')

                  begin

                    SELECT @TotalNumberOfUnfixedJobs= COUNT(*)  FROM v_rptJobBidSucces  INNER JOIN  Account24918.vProject as Project ON v_rptJobBidSucces.JobNo=Project.ProjectNumber WHERE (JobValue>0)

                                    AND (ISNULL(Project.RptCurrency,'')=@txtCurrency)

                                    AND (convert(date,v_rptJobBidSucces.[Date])>='2014-12-30' AND convert(Date,v_rptJobBidSucces.[Date])<='2015-12-30')

                        

                        

                              SELECT  @TotalNumberOfWonJobs =COUNT(*) FROM v_rptJobBidSucces  INNER JOIN Account24918.vProject Project ON v_rptJobBidSucces.JobNo=Project.ProjectNumber    WHERE  IntStatus NOT IN ('1', '3') 

                                    AND (ISNULL(Project.RptCurrency,'')=@txtCurrency)

                                    AND (JobValue>0)

                                  AND (convert(date,v_rptJobBidSucces.[Date])>='2014-12-30' AND convert(Date,v_rptJobBidSucces.[Date])<='2015-12-30')

                        

                  end
                 

          
 

      IF @TotalNumberOfUnfixedJobs <> 0

           SET @ReturnValue= CAST(CAST((CAST(@TotalNumberOfWonJobs AS DECIMAL(10,2)) / CAST(@TotalNumberOfUnfixedJobs AS DECIMAL(10,2))) * 100 AS DECIMAL(18,2)) AS VARCHAR) 
  
      ELSE 

            SET @ReturnValue='0.0'

     

      RETURN @ReturnValue

end

