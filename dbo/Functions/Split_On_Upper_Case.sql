﻿CREATE FUNCTION dbo.Split_On_Upper_Case
 (
     @String VARCHAR(4000)
 )
RETURNS VARCHAR(4000)
AS
BEGIN

DECLARE @Char1 CHAR(1);
DECLARE @Char2 CHAR(1);
DECLARE @i    INT = 0;
DECLARE @OutString VARCHAR(4000) = '';

SELECT @String = REPLACE(@String,'_',' ')

WHILE (@i <= LEN(@String))
BEGIN
    SELECT @Char1 = SUBSTRING(@String, @i,1)
    IF (@i+1 <= LEN(@String))
		BEGIN
			SELECT @Char2 = SUBSTRING(@String, @i+1,1)
			IF (@Char1 in (' ','/','.','(',')')) or (@Char2 in (' ','/','.','(',')'))
				SET @OutString = @OutString + @Char1;
			ELSE IF (ISNUMERIC(@Char1) = 1) AND (ISNUMERIC(@Char2) = 0)
				SET @OutString = @OutString + @Char1 + ' ';
			--ELSE IF (@Char1 = UPPER(@Char1) Collate Latin1_General_CS_AI) AND (@Char2 = LOWER(@Char2) Collate Latin1_General_CS_AI)
			--	SET @OutString = RTRIM(@OutString) + ' ' +  @Char1;
			ELSE IF (@Char1 = LOWER(@Char1) Collate Latin1_General_CS_AI) AND (@Char2 = UPPER(@Char2) Collate Latin1_General_CS_AI)
				SET @OutString = @OutString + @Char1 + ' ';
			ELSE 
				SET @OutString = @OutString + @Char1;
		END
	ELSE
		SET @OutString = @OutString +  @Char1;
	
     SET @i += 1;
END

 SET @OutString =  LTRIM(@OutString);

 RETURN @OutString;

END 
