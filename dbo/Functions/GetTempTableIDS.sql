﻿CREATE FUNCTION [dbo].[GetTempTableIDS](@input AS Varchar(MAX) )

RETURNS

      @Result TABLE(ID INT)

AS
-- SELECT * FROM [dbo].GetTempTableIDS ('1289172,1289171')
BEGIN

      DECLARE @str VARCHAR(20)

      DECLARE @ind Int

      IF(@input is not null)

      BEGIN

            SET @ind = CharIndex(',',@input)

            WHILE @ind > 0

            BEGIN

                  SET @str = SUBSTRING(@input,1,@ind-1)

                  SET @input = SUBSTRING(@input,@ind+1,LEN(@input)-@ind)

                  INSERT INTO @Result values (@str)

                  SET @ind = CharIndex(',',@input)

            END

            SET @str = @input

            INSERT INTO @Result values (@str)

      END

      RETURN

END 