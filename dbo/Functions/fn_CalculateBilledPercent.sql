﻿

create function fn_CalculateBilledPercent (@ProjectNumber VARCHAR(max), @cValue money)

RETURNS VARCHAR(255)
as
begin


 DECLARE @ReturnValue VARCHAR(255)
   
 select
@ReturnValue =(Sum(cast(ISNULL(BillingInformation.[Invoice Amount], 0) as money))/@cValue) * 100 
   FROM Account24918.[vBilling Information]BillingInformation
   
   wHERE BillingInformation.[Project Number]=@ProjectNumber and BillingInformation.Type in (1, 4)    GROUP BY BillingInformation.[project number]
   
     
   return @ReturnValue
   
  
 end
