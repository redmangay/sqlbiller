﻿
CREATE function [dbo].[fn_CalculatePaidPercent] (@ProjectNumber VARCHAR(max), @cValue money)

RETURNS VARCHAR(255)
as
begin


 DECLARE @ReturnValue VARCHAR(255)
   
    SELECT 
   -- BillingInformation.ProjectNumber
    --Sum(CCUR(IIF(ISNULL(BillingInformation.PaymentAmount), 0, BillingInformation.PaymentAmount))) As SumOfPaymentAmount "
    @ReturnValue =(Sum(cast(ISNULL(BillingInformation.[Payment Amount], 0) as money))/@cValue) * 100 
    FROM Account24918.[vBilling Information] BillingInformation 
    WHERE 
    BillingInformation.[Project Number]=@ProjectNumber AND      BillingInformation.Type = 3 
    GROUP BY BillingInformation.[Project Number] 
    
    return @ReturnValue
  
   
   
  
 end