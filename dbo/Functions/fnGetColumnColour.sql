﻿

CREATE FUNCTION [dbo].[fnGetColumnColour]
(
	@nRecordID INT  
	,@nID INT
	,@Context varchar(25)
)  -- SELECT [dbo].[fnGetColumnColour](1362429,60592,'columnid')
RETURNS varchar(10)
AS
BEGIN
	
	DECLARE @sReturn VARCHAR(10);	
	DECLARE @sSystemName VARCHAR(50);
	DECLARE @sEachConrollingRecordValue VARCHAR(MAX);
	DECLARE @Sql NVARCHAR(4000)
	
	DECLARE T_Cursor CURSOR FOR
	SELECT ControllingColumnID,Operator,Value,ColourText,ColourCell FROM ColumnColour
	WHERE ID=@nID AND Context=@Context ORDER BY ColumnColourID;
	
	DECLARE @nControllingColumnID INT;
	DECLARE @sOperator VARCHAR(25);
	DECLARE @sValue NVARCHAR(250);
	DECLARE @sColourText VARCHAR(10);
	DECLARE @sColourCell VARCHAR(10);
	
	DECLARE @ColumnType varchar(20)
	
	
	OPEN T_Cursor;
	FETCH NEXT FROM T_Cursor INTO @nControllingColumnID,@sOperator,@sValue,@sColourText,@sColourCell;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
		  SET @sEachConrollingRecordValue=NULL;
		  SET @sSystemName=NULL;
		  SET @Sql=NULL;
		  SELECT @sSystemName=SystemName,@ColumnType=ColumnType FROM [Column] WHERE ColumnID=@nControllingColumnID
		  
		 
		  SELECT @sEachConrollingRecordValue=[dbo].[fnGetRecordValueBySys](@nRecordID,@sSystemName)
		 
		  IF [dbo].[fnCompareValuesForColour](@sEachConrollingRecordValue,@sValue,@sOperator,@ColumnType)=1
			BEGIN
				SET @sReturn=@sColourText
				BREAK;
			END
		  
		  
		  FETCH NEXT FROM T_Cursor INTO @nControllingColumnID,@sOperator,@sValue,@sColourText, @sColourCell;
	   END;
	CLOSE T_Cursor;
	DEALLOCATE T_Cursor;
	
	RETURN @sReturn;   
	
END

