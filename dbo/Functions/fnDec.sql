﻿CREATE FUNCTION dbo.fnDec
(
	@s varchar(MAX)
)
RETURNS decimal(18,4)
AS
BEGIN
	DECLARE @n decimal(18,4)
	IF ISNUMERIC(@s) = 1
		set @n = 0.00 + CAST(@s AS decimal(18,4))
	ELSE
		set @n = 0.00
	RETURN @n
END
