﻿
CREATE FUNCTION [dbo].[fnStringToDate]
(
	@InDateString varchar(MAX)
)
RETURNS datetime
AS
BEGIN
	/* HISTORY
	14 Nov 2017 JB commented out ISDATE check as it is not reliable
	9th Jan 2018 JV added another else if condition when date format is d/mm/yyyy. e.g 8/12/2018
	*/
	--declare @InDateString varchar(MAX) = '31/12/2016' 
	/*
	SELECT [dbo].[fnStringToDate3]('31/12/2016')
	SELECT [dbo].[fnStringToDate3]('31-12-2016')
	SELECT [dbo].[fnStringToDate3]('31/12/2016 13:14')
	SELECT [dbo].[fnStringToDate3]('31/12/2016 13:14:01')
	*/

	DECLARE @OutDateString varchar(50), @Return datetime, @sYear char(4), @sMonth char(2), @sDay char(2)
	SET @Return = NULL
	--IF @InDateString IS NULL OR LEN(@InDateString) < 8
	--	RETURN @Return

	IF LEN(@InDateString) = 8 OR SUBSTRING(@InDateString, 9, 1) = ' '
	BEGIN
		IF SUBSTRING(@InDateString, 3, 1) IN ('/','-')  AND SUBSTRING(@InDateString, 6, 1) IN ('/','-')
		BEGIN
			-- 'Format: 31-12-99 or 31/12/99'
			SET @sYear = '20' + SUBSTRING(@InDateString, 7, 2)
			SET @sMonth = SUBSTRING(@InDateString, 4,2)
			SET @sDay = SUBSTRING(@InDateString, 1,2)
		END
		ELSE 
		BEGIN
			-- 'Format: 31121999'
			SET @sYear = SUBSTRING(@InDateString, 5,4)
			SET @sMonth = SUBSTRING(@InDateString, 3,2)
			SET @sDay = SUBSTRING(@InDateString, 1,2)
		END
	END
	ELSE 
	BEGIN
		IF SUBSTRING(@InDateString, 3, 1) IN ('/','-')  AND SUBSTRING(@InDateString, 6, 1) IN ('/','-') 
		BEGIN
			-- '31-12-2015 or 31/12/2015'
			SET @sYear = SUBSTRING(@InDateString, 7,4)
			SET @sMonth = SUBSTRING(@InDateString, 4,2)
			SET @sDay = SUBSTRING(@InDateString, 1,2)
		END

		ELSE IF SUBSTRING(@InDateString, 2, 1) IN ('/','-')  AND SUBSTRING(@InDateString, 5, 1) IN ('/','-') 
		BEGIN
			-- 'Format: 8-12-2018 or 8/12/2018
			SET @sYear = SUBSTRING(@InDateString, 6,4)
			SET @sMonth = SUBSTRING(@InDateString, 3,2)
			SET @sDay = '0' + SUBSTRING(@InDateString, 1,1)
		END

		ELSE IF SUBSTRING(@InDateString, 5, 1) IN ('/','-')  AND SUBSTRING(@InDateString, 8, 1) IN ('/','-')
		BEGIN
			-- 'Format 2015-12-31 or 2015/12/31'
			SET @sYear = SUBSTRING(@InDateString, 1,4)
			SET @sMonth = SUBSTRING(@InDateString, 6,2)
			SET @sDay = SUBSTRING(@InDateString, 9,2)
		END
	END				

	-- Commented out 14 Nov 2017. We cannot use this: IF ISDATE(@OutDateString) = 1
	IF ISNUMERIC(@sYear)=1 AND ISNUMERIC(@sMonth)=1 AND ISNUMERIC(@sDay)=1
		AND CAST(@sYear AS int) >= 1900
		AND CAST(@sMonth AS int) < 13
		AND CAST(@sDay AS int) < 32
	BEGIN
		SET @OutDateString = @sYear + '-' + @sMonth + '-' + @sDay

		-- Is there a time component?
		IF LEN(@InDateString) > 10 
		BEGIN
			-- 'Time'
			IF SUBSTRING(@InDateString, 9, 1) = ' '
				SET @OutDateString = @OutDateString + ' ' + SUBSTRING(@InDateString, 10,12)
			ELSE IF SUBSTRING(@InDateString, 10, 1) = ' '
				SET @OutDateString = @OutDateString + ' ' + SUBSTRING(@InDateString, 11,12)
			ELSE 
				SET @OutDateString = @OutDateString + ' ' + SUBSTRING(@InDateString, 12,12)
		END
		ELSE 
			SET @OutDateString = @OutDateString + ' 00:00:00.000'

	END
	--IF LEN(@OutDateString) > 7 and ISDATE(@OutDateString)=1 
	SET @Return = CONVERT(datetime, @OutDateString, 121)

--print @OutDateString
--print @Return
RETURN @Return

END
