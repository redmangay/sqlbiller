﻿
CREATE FUNCTION [dbo].[fnGetSystemUserDisplayText]
(
	@sDisplay varchar(255)  -- [Name] OR [Email]
	,@sUserID VARCHAR(255)  --SELECT [dbo].[fnGetSystemUserDisplayText] ('[Name]','24976')
)
RETURNS varchar(MAX)
AS
BEGIN
	IF @sUserID ='' OR ISNUMERIC(@sUserID)=0 return '';
	
	DECLARE @sReturn VARCHAR(1000);	

	IF @sDisplay='[Name]' 
			SELECT @sReturn=(ISNULL(FirstName,'') +' ' + ISNULL(LastName,''))  FROM [User] WHERE UserID=CAST(@sUserID AS INT)
	ELSE 
		  SELECT @sReturn=Email FROM [User] WHERE UserID=CAST(@sUserID AS INT)
	 
	IF @sReturn IS NULL SET @sReturn='';
	
	RETURN @sReturn;   
	
			
	
END

