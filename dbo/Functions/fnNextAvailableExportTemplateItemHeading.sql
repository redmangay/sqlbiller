﻿

CREATE FUNCTION [dbo].[fnNextAvailableExportTemplateItemHeading]
(	
	@sExportHeaderName varchar(250),@nExportTemplateID int,@nExportTemplateItemID int=null
)
RETURNS varchar(800)
AS
	/*
		UNIT TESTING 
		 select dbo.[fnNextAvailableExportTemplateItemHeading] ('Checkbox1',853,null)
	*/
BEGIN
	-- Declare the return variable here
	DECLARE @sResult varchar(800)
	DECLARE @nEditExportTemplateItemID INT=-1
	
	IF @nExportTemplateItemID IS NOT NULL
		SET @nEditExportTemplateItemID=@nExportTemplateItemID
	
	DECLARE @counter int
			SET @counter = 2

	--BEGIN
		IF NOT EXISTS(SELECT ExportTemplateItemID  FROM [ExportTemplateItem] WHERE ExportTemplateID=@nExportTemplateID  AND ExportTemplateItemID NOT IN(@nEditExportTemplateItemID)
		AND ExportHeaderName=@sExportHeaderName) 
			SET @sResult = @sExportHeaderName 
		ELSE
		BEGIN
			
			WHILE @counter < 1000
			BEGIN
				
				IF EXISTS(SELECT ExportTemplateItemID  FROM [ExportTemplateItem] WHERE ExportTemplateID=@nExportTemplateID AND ExportTemplateItemID NOT IN(@nEditExportTemplateItemID) 
				AND ExportHeaderName= @sExportHeaderName  + ' ' + CAST(@counter AS nvarchar(10)))
						SET @counter = @counter + 1
				ELSE 
				BEGIN
					SET @sResult = @sExportHeaderName + ' ' +CAST(@counter AS nvarchar(10))
					SET @counter = 1000 -- EXIT
				END
			END 
		END
		
	-- Return the result of the function
	RETURN @sResult

END




