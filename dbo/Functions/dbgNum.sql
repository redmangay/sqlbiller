﻿CREATE FUNCTION [dbo].[dbgNum]
(
	@FieldVal varchar(MAX)
)
RETURNS decimal(18,4)
AS
BEGIN
	DECLARE @Return decimal(18,4) = 0
	SET @Return = TRY_PARSE(@FieldVal AS decimal(18,4))

	IF @Return IS NULL 
	BEGIN
		DECLARE @IsNegative bit
		IF PATINDEX('%-%',@FieldVal) > 0
			SET @Return = -TRY_PARSE(dbo.[RemoveNonNumericChar](@FieldVal)  AS decimal(18,4))
		ELSE 
			SET @Return = TRY_PARSE(dbo.[RemoveNonNumericChar](@FieldVal)  AS decimal(18,4))
	END
		
	RETURN @Return
END

