﻿

create FUNCTION [dbo].[fnNextAvailableImportTemplateItemHeading]
(	
	@sImportHeaderName varchar(250),@nImportTemplateID int,@nImportTemplateItemID int=null
)
RETURNS varchar(800)
AS
	/*
		UNIT TESTING 
		 select dbo.[fnNextAvailableImportTemplateItemHeading] ('Checkbox1',853,null)
	*/
BEGIN
	-- Declare the return variable here
	DECLARE @sResult varchar(800)
	DECLARE @nEditImportTemplateItemID INT=-1
	
	IF @nImportTemplateItemID IS NOT NULL
		SET @nEditImportTemplateItemID=@nImportTemplateItemID
	
	DECLARE @counter int
			SET @counter = 2

	--BEGIN
		IF NOT EXISTS(SELECT ImportTemplateItemID  FROM [ImportTemplateItem] WHERE ImportTemplateID=@nImportTemplateID  AND ImportTemplateItemID NOT IN(@nEditImportTemplateItemID)
		AND ImportHeaderName=@sImportHeaderName) 
			SET @sResult = @sImportHeaderName 
		ELSE
		BEGIN
			
			WHILE @counter < 1000
			BEGIN
				
				IF EXISTS(SELECT ImportTemplateItemID  FROM [ImportTemplateItem] WHERE ImportTemplateID=@nImportTemplateID AND ImportTemplateItemID NOT IN(@nEditImportTemplateItemID) 
				AND ImportHeaderName= @sImportHeaderName  + ' ' + CAST(@counter AS nvarchar(10)))
						SET @counter = @counter + 1
				ELSE 
				BEGIN
					SET @sResult = @sImportHeaderName + ' ' +CAST(@counter AS nvarchar(10))
					SET @counter = 1000 -- EXIT
				END
			END 
		END
		
	-- Return the result of the function
	RETURN @sResult

END




