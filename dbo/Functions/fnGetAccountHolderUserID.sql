﻿
CREATE FUNCTION [dbo].[fnGetAccountHolderUserID]
(	
	@AccountID INT
)  
	RETURNS INT
AS
	BEGIN
		DECLARE @UserID INT --Account Holder
		SELECT TOP 1 @UserID=U.UserID FROM [User] U INNER JOIN UserRole UR ON U.UserID=UR.UserID
			WHERE UR.IsAccountHolder=1 AND UR.IsPrimaryAccount=1
			AND UR.AccountID=@AccountID  ORDER BY U.UserID 

		RETURN @UserID
	END


