﻿CREATE FUNCTION [dbo].[fnReplaceDisplayColumns]
(
	@sDisplay varchar(MAX) --'test - [Sample Date] id [ID_N] [Sample Site:Sample Site Name]'
	,@nTableID int, -- TableTableID, parent
	@nColumnID INT=null -- Child ColumnID, need this to set a base. Null when we populate dropdown/listbox etc
)
RETURNS varchar(MAX)
AS
BEGIN
	
IF @nTableID<0 RETURN REPLACE(@sDisplay,'''','''''')

	DECLARE @Field varchar(254), @Column varchar(254), @nStart int, @nEnd int,@nAccountID INT
	SELECT @nAccountID=AccountID FROM [Table] WHERE TableID=@nTableID
	SELECT @nStart = 1
	WHILE @nStart >= 1 and @nStart < LEN(@sDisplay) 
	BEGIN 
		SELECT @nStart = CHARINDEX('[', @sDisplay, @nStart)
		SELECT @nEnd  = CHARINDEX(']', @sDisplay, @nStart)

		IF @nEnd=0
		BEGIN
			SELECT @Field = SUBSTRING(@sDisplay, @nStart,LEN(@sDisplay))
			SELECT @sDisplay = REPLACE(@sDisplay, @Field,'' )
			--PRINT '@Field:' + @Field
			--PRINT '@sDisplay:' + @sDisplay
		BREAK
		END


		IF @nStart > 0 AND (@nEnd > @nStart)
		BEGIN
			SELECT @Field = SUBSTRING(@sDisplay, @nStart+1, @nEnd-@nStart-1)
			--print '@Field ' + @Field
			IF CHARINDEX(':', @Field, 1)>0  
				BEGIN
					DECLARE @sTableName VARCHAR(255)=SUBSTRING(@Field,1,CHARINDEX(':', @Field, 1)-1)
					--PRINT '@sTableName=' + @sTableName
					DECLARE @s3Sys VARCHAR(20)=NULL -- parent's parent
					DECLARE @p3TableID INT=NULL
					SELECT TOP 1 @s3Sys=C.SystemName,@p3TableID=T.TableID FROM [Column] C INNER JOIN [Table] T ON C.TableID=T.TableID
						WHERE T.AccountID=@nAccountID AND T.IsActive=1 AND (T.TableName + ':'+C.DisplayName)=@Field

					 IF @s3Sys IS NOT NULL
						BEGIN
							DECLARE @nSys VARCHAR(20) 
							DECLARE @n2ColumnID INT=NULL
							SELECT TOP 1 @nSys=C.SystemName,@n2ColumnID=C.ColumnID FROM [Column] C 
								WHERE C.TableTableID=@p3TableID AND C.TableID=@nTableID AND C.ColumnType='dropdown' 

							IF @n2ColumnID IS NOT NULL
								SET @Column='P' + CAST(@n2ColumnID as VARCHAR) + '.' + @s3Sys
						END
						
				END
			ELSE IF @nColumnID IS NOT NULL
				SELECT @Column = 'P' + CAST(@nColumnID as VARCHAR) + '.' + SystemName from [Column] WHERE TableID = @nTableID and DisplayName = @Field
			ELSE 
				SELECT @Column = 'Record.' + SystemName from [Column] WHERE TableID = @nTableID and DisplayName = @Field

			
			--print '@Column: ' + @Column 
			--SELECT @sDisplay = REPLACE(@sDisplay, @Field,@Column)

			-- Fix by JB on 27 April 2017 
			-- SELECT @sDisplay = REPLACE(@sDisplay, @Field,' ISNULL(' +@Column+ ','')' )
			SELECT @sDisplay = REPLACE(@sDisplay, '['+@Field+']','[ ISNULL(' +@Column+ ','')]' )
			-- end Fix by JB on 27 April 2017 

			--PRINT @sDisplay
			--PRINT '@n: ' + CAST(@nStart AS varchar)
			SELECT @nStart = @nStart+1
		END
	END
	SET @sDisplay=REPLACE(@sDisplay,'''','''''')
	SELECT @sDisplay = '''' + REPLACE(@sDisplay, '[', '''+') + ''''
	SELECT @sDisplay = REPLACE(@sDisplay, ']', '+''')
	IF LEFT(@sDisplay, 3) = '''''+' set @sDisplay = SUBSTRING(@sDisplay,4,len(@sDisplay)-3)
	IF RIGHT(@sDisplay, 3) = '+''''' set @sDisplay = LEFT(@sDisplay,Len(@sDisplay)-3)
	RETURN @sDisplay

END
