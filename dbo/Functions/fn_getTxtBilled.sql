﻿
CREATE function [dbo].[fn_getTxtBilled]  (@jobNumber VARCHAR(max), @cValue money)

RETURNS VARCHAR(255)
as
begin

declare @BilledPercent varchar(max)
declare @PaidPercent varchar(max)

 DECLARE @ReturnValue VARCHAR(255)
   
     If @cValue <> 0 
     begin
          set  @BilledPercent = dbo.fn_CalculateBilledPercent(@jobNumber, @cValue)
            set @BilledPercent =ISNULL(@BilledPercent,'')
            
          set  @PaidPercent = dbo.fn_CalculatePaidPercent(@jobNumber, @cValue)
            If @PaidPercent <> '0' 
            begin 
            set @BilledPercent = @BilledPercent + ' (' + @PaidPercent + ')'
            set @ReturnValue =@BilledPercent
      end
  end
   
  return @ReturnValue
  
 end