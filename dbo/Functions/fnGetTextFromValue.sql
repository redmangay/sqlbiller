﻿

-- Get Text from [Column].DropdownValue by value
-- 
--SELECT [dbo].[fnGetTextFromValue] ('P',
--'C,Completed 
-- P, Progress
--   A, Accepted 
--    H,HolD')

CREATE FUNCTION [dbo].[fnGetTextFromValue]
(
	@value varchar(256),@DropdownValues varchar(max)
) 
RETURNS varchar(256)
   --WITH SCHEMABINDING

BEGIN

	DECLARE @DropdownValues2 VARCHAR(MAX)
	DECLARE @Text VARCHAR(MAX)

	SELECT @DropdownValues2= SUBSTRING(@DropdownValues, CHARINDEX(@value+',',@DropdownValues),LEN(@DropdownValues))
	set @DropdownValues2=  REPLACE(@DropdownValues2,@value+',','')

	IF CHARINDEX(CHAR(13),@DropdownValues2)>0
		SELECT @Text=SUBSTRING(@DropdownValues2, 0,CHARINDEX(CHAR(13),@DropdownValues2))
	ELSE
		SET @Text=@DropdownValues2
	--PRINT @DropdownValues
	return @Text


END


