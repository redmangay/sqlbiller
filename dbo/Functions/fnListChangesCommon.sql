﻿CREATE FUNCTION [dbo].[fnListChangesCommon]

(
	  @sTableName varchar(200),
      @nPKID int,
      @UpdateDate datetime   
)
RETURNS varchar(MAX)
AS
/*
      UNIT TESTING
      ------------
      SELECT dbo.fnListChangesCommon('Table',183 , '2012-04-06 12:02:55.257')

      SELECT dbo.fnListChangesCommon('Record',2152 , '2012-02-23 12:58:16.677')
*/
BEGIN


	DECLARE @Changes varchar(MAX)
	DECLARE @ColumnDisplayName varchar(100)

	SELECT @Changes = ''

	--SELECT @Changes = @Changes + CASE WHEN [Column].DisplayTextDetail IS NULL THEN FieldName ELSE [Column].DisplayTextDetail END + ', '

	if @sTableName ='Record'
		  --SELECT @Changes = @Changes + [Column].DisplayName + ', '
		  SELECT @Changes = @Changes + CASE WHEN [Column].DisplayName IS NULL THEN FieldName ELSE [Column].DisplayName END + ', '
		  FROM [Audit]
		  LEFT JOIN  [Record] ON [Record].RecordID = @nPKID
		  LEFT JOIN [Column] on [Column].TableID = [Record].TableID 
			AND [Column].SystemName = FieldName
		  WHERE TableName='Record' AND PrimaryKeyValue = @nPKID
			  AND [Audit].DateAdded = @UpdateDate 
			  AND [Audit].FieldName !='LastUpdatedUserID'
			  AND [Audit].FieldName != 'WarningResults'
			  AND [Audit].FieldName != 'ChangeReason' 
      
	else if @sTableName = 'Column'
		SELECT @Changes = @Changes +  FieldName + ', ',
			   @ColumnDisplayName = [Column].DisplayName
			  FROM [Audit]
			  LEFT JOIN [Column] ON CONVERT(int, [Audit].PrimaryKeyValue)=[Column].ColumnID
			  WHERE (TableName='Column')
				AND [Audit].PrimaryKeyValue = @nPKID
				AND [Audit].DateAdded = @UpdateDate
				AND [Audit].FieldName !='LastUpdatedUserID'	
	else     
		SELECT @Changes = @Changes +  FieldName + ', '
			  FROM [Audit]
			  WHERE TableName=@sTableName
				  AND PrimaryKeyValue = @nPKID
				  AND DateAdded = @UpdateDate
				  AND [Audit].FieldName !='LastUpdatedUserID'
				  AND [Audit].FieldName !='UserUpdated'    

	--if LEN(@Changes)>1
	--SELECT @Changes = SUBSTRING(@Changes, 1, LEN(@Changes) - 1) 
	if LEN(@Changes)>1
	SELECT @Changes = SUBSTRING(@Changes, 1, LEN(@Changes) - 1) + IIF(@Changes NOT LIKE 'Deleted%' AND @sTableName='Column', ' for Field ' + @ColumnDisplayName, '')
 
	RETURN @Changes

END
