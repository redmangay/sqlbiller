﻿CREATE FUNCTION dbo.nolandtax_ClosestPP
(
	@Lat real,
	@Long real
)
RETURNS int
as
BEGIN
	DECLARE @PPID int

	SELECT TOP 1 @PPID = P.RecordID 
	FROM [Record] P 
	WHERE P.TableID = 2100 
		AND P.IsActive = 1 
		AND ISNUMERIC(P.V014)=1 
		AND ISNUMERIC(P.V015)=1
	
	-- HAS NOT BEEN ALLOCATED ALREADY:  ------------------------------ V078 !!!! ------------------------------
	AND P.RecordID NOT IN (SELECT CAST(V078 AS INT) FROM [Record] WHERE TableID = 2101  and RecordID > 754493 and V078 is not null)
	ORDER BY SQRT(SQUARE(ABS(@Lat - CAST(P.V014 as real))) + SQUARE(ABS(@Long - CAST(P.V015 as real)))) asc

	return @PPID
END
