﻿Create FUNCTION [dbo].[fnNextAvailableAccountName]
(	
	@sBaseAccountName nvarchar(100)
)
RETURNS nvarchar(4000)
AS
	/*
		UNIT TESTING
		 select dbo.fnNextAvailableAccountName('jon')
		 select dbo.fnNextAvailableAccountName('other')
	*/
BEGIN
	-- Declare the return variable here
	DECLARE @sResult nvarchar(4000)
	
	IF NOT EXISTS(SELECT * FROM [Account] WHERE Accountname = @sBaseAccountName )
		SET @sResult = @sBaseAccountName 
	ELSE
	BEGIN
		DECLARE @counter int
		SET @counter = 1

		WHILE @counter < 1000
		BEGIN
			
			IF EXISTS(SELECT * FROM [Account] WHERE Accountname = @sBaseAccountName + CAST(@counter AS nvarchar(10)))
				SET @counter = @counter + 1
			ELSE 
			BEGIN
				SET @sResult = @sBaseAccountName + CAST(@counter AS nvarchar(10))
				SET @counter = 1000 -- EXIT
			END
		END 
	END
	-- Return the result of the function
	RETURN @sResult

END

