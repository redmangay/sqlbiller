﻿
CREATE FUNCTION [dbo].[fnCompareValuesForColour]
(
	
	@sEachConrollingRecordValue  varchar(max)
	,@sValue varchar(max)   
	,@sOperator VARCHAR(25)
	,@ColumnType varchar(20)
)  -- SELECT [dbo].[fnCompareValuesForColour]('31','31','equals','')
RETURNS bit
AS
BEGIN
	-- @ColumnType will be used for more in depth
	
	DECLARE @bReturn BIT=0
	
	IF @sEachConrollingRecordValue IS NULL OR @sValue IS NULL 
		RETURN 0
	
	IF @sOperator='equals' 
		IF @sEachConrollingRecordValue=@sValue RETURN 1
	
	IF @sOperator='notequal' 
		IF @sEachConrollingRecordValue!=@sValue RETURN 1	
		
	IF @sOperator='greaterthan' 
		IF @sEachConrollingRecordValue>@sValue RETURN 1	
		
	IF @sOperator='greaterthanequal' 
		IF @sEachConrollingRecordValue>=@sValue RETURN 1	
		
	IF @sOperator='lessthan' 
		IF @sEachConrollingRecordValue<@sValue RETURN 1	
		
	IF @sOperator='lessthanequal' 
		IF @sEachConrollingRecordValue<=@sValue RETURN 1	
		
	IF @sOperator='contains' 
		IF CHARINDEX(@sEachConrollingRecordValue,@sValue)>0 RETURN 1	
		
	IF @sOperator='notcontains' 
		IF CHARINDEX(@sEachConrollingRecordValue,@sValue)=0 RETURN 1			
			
	RETURN 0
END

