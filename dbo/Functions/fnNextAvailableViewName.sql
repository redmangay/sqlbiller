﻿
CREATE FUNCTION [dbo].[fnNextAvailableViewName]
(	
	@sBaseViewName varchar(max),@nTableID int,@nViewID int=null
)
RETURNS varchar(4000)
AS
	/*
		UNIT TESTING 
		 select dbo.fnNextAvailableViewName('Student',2721,null)
		 select dbo.fnNextAvailableViewName('Student Child Summary',2721,null)
		  select dbo.fnNextAvailableViewName('Student Child Summary',2721,654)
		select dbo.fnNextAvailableViewName('Student Summary',2721,654)

		 
	*/
BEGIN
	-- Declare the return variable here
	DECLARE @sResult varchar(max)
	DECLARE @nEditViewID INT=-1
	
	IF @nViewID IS NOT NULL
		SET @nEditViewID=@nViewID
	
	IF NOT EXISTS(SELECT * FROM [View] WHERE TableID=@nTableID  AND ViewID NOT IN(@nEditViewID) AND ViewName = @sBaseViewName)
		SET @sResult = @sBaseViewName 
	ELSE
	BEGIN
		DECLARE @counter int
		SET @counter = 2

		WHILE @counter < 1000
		BEGIN
			
			IF EXISTS(SELECT * FROM [View] WHERE TableID=@nTableID AND ViewID NOT IN(@nEditViewID) AND ViewName = @sBaseViewName + ' ' + CAST(@counter AS varchar(10)))
				SET @counter = @counter + 1
			ELSE 
			BEGIN
				SET @sResult = @sBaseViewName + ' ' +CAST(@counter AS varchar(10))
				SET @counter = 1000 -- EXIT
			END
		END 
	END
	-- Return the result of the function
	RETURN @sResult

END


