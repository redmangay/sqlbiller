﻿CREATE FUNCTION dbo.getGroupReportingCurrency(@Group VARCHAR(255))
RETURNS VARCHAR(255)
AS
BEGIN
	DECLARE @ReturnValue VARCHAR(255)
	SET @ReturnValue = ISNULL((SELECT ReportingCurrency FROM Account24918.vList_Of_Group_Division [List_of_Group/Division] WHERE [Group/Division]=@Group),'')
	
	RETURN @ReturnValue
END