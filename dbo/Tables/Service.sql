﻿CREATE TABLE [dbo].[Service] (
    [ServiceID]         INT           IDENTITY (1, 1) NOT NULL,
    [ServiceType]       VARCHAR (227) NOT NULL,
    [TableID]           INT           NULL,
    [TableChildID]      INT           NULL,
    [AccountID]         INT           NULL,
    [IsServerCode]      BIT           NULL,
    [ServiceJSON]       VARCHAR (MAX) NOT NULL,
    [ProcessNotes]      VARCHAR (MAX) NULL,
    [ServiceDefinition] VARCHAR (MAX) NULL,
    [SameEventOrder]    INT           NULL,
    [ColumnSystemName]  VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([ServiceID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Service_Account_AccountID] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_Service_Table_TableID] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [FK_Service_TableChild_TableChildID] FOREIGN KEY ([TableChildID]) REFERENCES [dbo].[TableChild] ([TableChildID])
);

