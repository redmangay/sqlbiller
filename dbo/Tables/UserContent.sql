﻿CREATE TABLE [dbo].[UserContent] (
    [UserContentID] INT IDENTITY (1, 1) NOT NULL,
    [UserID]        INT NOT NULL,
    [ContentID]     INT NOT NULL,
    [IsDefaultShow] BIT CONSTRAINT [DF_UserContent_IsDefaultShow] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_UserContent] PRIMARY KEY CLUSTERED ([UserContentID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_UserContent_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID]) ON DELETE CASCADE,
    CONSTRAINT [UQ_UserIDContentID] UNIQUE NONCLUSTERED ([UserID] ASC, [ContentID] ASC) WITH (FILLFACTOR = 90)
);

