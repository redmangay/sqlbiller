﻿CREATE TABLE [dbo].[ScheduledTask] (
    [ScheduledTaskID]   INT          IDENTITY (1, 1) NOT NULL,
    [AccountID]         INT          NOT NULL,
    [TableID]           INT          NOT NULL,
    [Frequency]         CHAR (1)     NOT NULL,
    [FrequencyWhen]     VARCHAR (20) NULL,
    [ScheduleType]      VARCHAR (20) NOT NULL,
    [RecordDateAdded]   DATETIME     NULL,
    [LastEmailSentDate] DATETIME     NULL,
    [MessageID]         INT          NULL,
    CONSTRAINT [PK_ScheduledTask] PRIMARY KEY CLUSTERED ([ScheduledTaskID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ScheduledTask_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_ScheduledTask_Message_MessageID] FOREIGN KEY ([MessageID]) REFERENCES [dbo].[Message] ([MessageID]),
    CONSTRAINT [FK_ScheduledTask_SampleType] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID])
);

