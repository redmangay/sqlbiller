﻿CREATE TABLE [dbo].[DataView] (
    [DataViewID] INT            IDENTITY (1, 1) NOT NULL,
    [ViewName]   NVARCHAR (255) NOT NULL,
    [AccountID]  INT            NOT NULL,
    [SPName]     NVARCHAR (255) NOT NULL,
    [IsActive]   BIT            DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([DataViewID] ASC) WITH (FILLFACTOR = 90)
);

