﻿CREATE TABLE [dbo].[View] (
    [ViewID]               INT           IDENTITY (1, 1) NOT NULL,
    [TableID]              INT           NOT NULL,
    [ViewName]             VARCHAR (500) NOT NULL,
    [UserID]               INT           NULL,
    [RowsPerPage]          INT           NULL,
    [SortOrder]            VARCHAR (MAX) NULL,
    [Filter]               VARCHAR (MAX) NULL,
    [ShowSearchFields]     BIT           DEFAULT ((1)) NOT NULL,
    [ShowAddIcon]          BIT           DEFAULT ((1)) NOT NULL,
    [ShowEditIcon]         BIT           DEFAULT ((1)) NOT NULL,
    [ShowDeleteIcon]       BIT           DEFAULT ((1)) NOT NULL,
    [ShowViewIcon]         BIT           DEFAULT ((1)) NOT NULL,
    [ShowBulkUpdateIcon]   BIT           DEFAULT ((1)) NOT NULL,
    [FilterControlsInfo]   VARCHAR (MAX) NULL,
    [ViewPageType]         VARCHAR (250) NULL,
    [FixedFilter]          VARCHAR (MAX) NULL,
    [ShowFixedHeader]      BIT           NULL,
    [ParentTableID]        INT           NULL,
    [DateUpdated]          DATETIME      CONSTRAINT [DF_View_DateUpdated] DEFAULT (getdate()) NULL,
    [TableChildID]         INT           NULL,
    [DateAdded]            DATETIME      CONSTRAINT [DF_View_DateAdded] DEFAULT (getdate()) NOT NULL,
    [ViewPageType_Backup]  VARCHAR (250) NULL,
    [ParentTableID_Backup] INT           NULL,
    [DisplayInColumnID]    INT           NULL,
    [SortOrder2]           VARCHAR (MAX) NULL,
    [ShowExportIcon]       BIT           DEFAULT ((1)) NOT NULL,
    [ShowEmailIcon]        BIT           DEFAULT ((0)) NOT NULL,
    [ShowHeader]           BIT           NULL,
    CONSTRAINT [PK_View_ViewID] PRIMARY KEY CLUSTERED ([ViewID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_View_Column_DisplayInColumnID] FOREIGN KEY ([DisplayInColumnID]) REFERENCES [dbo].[Column] ([ColumnID]) ON DELETE CASCADE,
    CONSTRAINT [FK_View_ParentTableID] FOREIGN KEY ([ParentTableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [FK_View_Table_TableID] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [FK_View_User_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])
);


GO
Create TRIGGER [dbo].[tr_View_Updated] ON [dbo].[View] AFTER UPDATE AS 
	UPDATE [View] 
		SET DateUpdated = GETDATE() 
		FROM inserted 
		WHERE ([View].ViewID = inserted.ViewID)
GO

CREATE TRIGGER [dbo].[ViewUpdated] ON [dbo].[View] AFTER UPDATE AS UPDATE [View] SET DateUpdated = GETDATE() FROM inserted WHERE ([View].[ViewID] = inserted.[ViewID])

GO
DISABLE TRIGGER [dbo].[ViewUpdated]
    ON [dbo].[View];

