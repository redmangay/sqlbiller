﻿CREATE TABLE [dbo].[WorkFlow] (
    [WorkFlowID]   INT             IDENTITY (1, 1) NOT NULL,
    [AccountID]    INT             NOT NULL,
    [WorkFlowName] NVARCHAR (1000) NOT NULL,
    CONSTRAINT [PK_WorkFlow] PRIMARY KEY CLUSTERED ([WorkFlowID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_WorkFlow_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
);

