﻿CREATE TABLE [dbo].[UserPasswordReset] (
    [PasswordResetID] INT      IDENTITY (1, 1) NOT NULL,
    [UserID]          INT      NOT NULL,
    [DateRequested]   DATETIME NOT NULL,
    [Expiration]      DATETIME NOT NULL,
    [IsActive]        BIT      NOT NULL,
    CONSTRAINT [PK_UserPasswordReset] PRIMARY KEY CLUSTERED ([PasswordResetID] ASC) WITH (FILLFACTOR = 90)
);

