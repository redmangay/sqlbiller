﻿CREATE TABLE [dbo].[RoleTable] (
    [RoleTableID]            INT      IDENTITY (1, 1) NOT NULL,
    [TableID]                INT      NOT NULL,
    [RoleType]               INT      NOT NULL,
    [DateAdded]              DATETIME CONSTRAINT [DF_UserSampleType_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]            DATETIME CONSTRAINT [DF_UserSampleType_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [CanExport]              BIT      CONSTRAINT [DF_UserTable_CanExport] DEFAULT ((1)) NOT NULL,
    [RoleID]                 INT      NULL,
    [ViewsDefaultFromUserID] INT      NULL,
    [AllowEditView]          BIT      NULL,
    [ShowMenu]               BIT      CONSTRAINT [DF_RoleTable_ShowMenu] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_UserSampleType] PRIMARY KEY CLUSTERED ([RoleTableID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_RoleTable_Role_RoleID] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role] ([RoleID]),
    CONSTRAINT [FK_RoleTable_ViewsDefaultFromUserID] FOREIGN KEY ([ViewsDefaultFromUserID]) REFERENCES [dbo].[User] ([UserID]),
    CONSTRAINT [FK_UserSampleType_SampleType] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID])
);


GO

CREATE TRIGGER [dbo].[tg_UserSampleTypeUpdated] ON [dbo].[RoleTable] AFTER UPDATE AS UPDATE [RoleTable]
 SET DateUpdated = GETDATE() FROM inserted WHERE ([RoleTable].RoleTableID = inserted.RoleTableID)



