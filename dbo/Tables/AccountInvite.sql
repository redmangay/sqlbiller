﻿CREATE TABLE [dbo].[AccountInvite] (
    [AccountInviteID]  INT           IDENTITY (1, 1) NOT NULL,
    [AccountID]        INT           NOT NULL,
    [Email]            VARCHAR (200) NOT NULL,
    [Name]             VARCHAR (200) NULL,
    [AcceptedDate]     DATETIME      NULL,
    [PaidToDate]       DATETIME      NULL,
    [ExpiredDate]      DATETIME      NULL,
    [IsActive]         BIT           NULL,
    [DateAdded]        DATETIME      DEFAULT (getdate()) NOT NULL,
    [DateUpdated]      DATETIME      DEFAULT (getdate()) NOT NULL,
    [InvitedAccountID] INT           NULL,
    [PhoneNumber]      VARCHAR (25)  NULL,
    [InvitedUserID]    INT           NULL,
    [InviterUserID]    INT           NULL,
    [InviteStatus]     CHAR (1)      NULL,
    [InviteType]       CHAR (1)      NULL,
    [TaskRecordID]     INT           NULL,
    [AddAsTeamMember]  BIT           NULL,
    [RandomPassword]   VARCHAR (50)  NULL,
    CONSTRAINT [PK_AccountInviteID] PRIMARY KEY CLUSTERED ([AccountInviteID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_AccountInvite_Account_AccountID] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_AccountInvite_InvitedAccountID] FOREIGN KEY ([InvitedAccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_AccountInvite_InvitedUserID] FOREIGN KEY ([InvitedUserID]) REFERENCES [dbo].[User] ([UserID]),
    CONSTRAINT [FK_AccountInvite_InviterUserID] FOREIGN KEY ([InviterUserID]) REFERENCES [dbo].[User] ([UserID])
);


GO
CREATE TRIGGER [dbo].[tr_AccountInvite_Updated] ON [dbo].[AccountInvite] AFTER UPDATE AS 
	UPDATE AccountInvite 
		SET DateUpdated = GETDATE() 
		FROM inserted 
		WHERE (AccountInvite.AccountInviteID = inserted.AccountInviteID)