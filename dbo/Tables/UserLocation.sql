﻿CREATE TABLE [dbo].[UserLocation] (
    [UserLocationID]    INT             IDENTITY (1, 1) NOT NULL,
    [UserID]            INT             NOT NULL,
    [Latitude]          DECIMAL (10, 6) NOT NULL,
    [Longitude]         DECIMAL (10, 6) NOT NULL,
    [DateAdded]         DATETIME        CONSTRAINT [DF_UserLocation_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DistanceTravelled] DECIMAL (18, 4) NULL,
    [LastSeen]          DATETIME        NULL,
    CONSTRAINT [PK_UserLocation] PRIMARY KEY CLUSTERED ([UserLocationID] ASC)
);

