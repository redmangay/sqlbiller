﻿CREATE TABLE [dbo].[Upload] (
    [UploadID]         INT           IDENTITY (1, 1) NOT NULL,
    [TableID]          INT           NOT NULL,
    [UploadName]       VARCHAR (100) NOT NULL,
    [EmailFrom]        VARCHAR (250) NOT NULL,
    [Filename]         VARCHAR (250) NOT NULL,
    [UseMapping]       BIT           CONSTRAINT [DF_Upload_UseMapping] DEFAULT ((0)) NOT NULL,
    [TemplateID]       INT           NULL,
    [SaveAttachment]   BIT           NULL,
    [FileNameCriteria] VARCHAR (250) NULL,
    [DocumentTypeID]   INT           NULL,
    [UploadType]       VARCHAR (20)  CONSTRAINT [DF_Upload_UploadType] DEFAULT ('Email') NOT NULL,
    [MappingKey]       SMALLINT      NULL,
    CONSTRAINT [PK_Upload] PRIMARY KEY CLUSTERED ([UploadID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Upload_DocumentType] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentType] ([DocumentTypeID]),
    CONSTRAINT [FK_Upload_ImportTemplate] FOREIGN KEY ([TemplateID]) REFERENCES [dbo].[ImportTemplate] ([ImportTemplateID]) ON DELETE SET NULL,
    CONSTRAINT [FK_Upload_SampleType] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [UQ_EmailFrom_FileName] UNIQUE NONCLUSTERED ([EmailFrom] ASC, [Filename] ASC) WITH (FILLFACTOR = 90)
);

