﻿CREATE TABLE [dbo].[ImportTemplateItem] (
    [ImportTemplateItemID]       INT           IDENTITY (1, 1) NOT NULL,
    [ImportTemplateID]           INT           NOT NULL,
    [ColumnID]                   INT           NOT NULL,
    [ImportHeaderName]           VARCHAR (255) NOT NULL,
    [ColumnIndex]                INT           NOT NULL,
    [ParentImportColumnID]       INT           NULL,
    [PositionOnImport]           VARCHAR (127) NULL,
    [IsDateSingleColumn_DELETED] BIT           NULL,
    PRIMARY KEY CLUSTERED ([ImportTemplateItemID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ImportTemplateItem_Column_ColumnID] FOREIGN KEY ([ColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_ImportTemplateItem_Column_ParentImportColumnID] FOREIGN KEY ([ParentImportColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_ImportTemplateItem_ImportTemplate_ImportTemplateID] FOREIGN KEY ([ImportTemplateID]) REFERENCES [dbo].[ImportTemplate] ([ImportTemplateID]) ON DELETE CASCADE,
    CONSTRAINT [UX_ColumnID_ImportTemplateID] UNIQUE NONCLUSTERED ([ColumnID] ASC, [ImportTemplateID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [UX_ImportHeaderName_ImportTemplateID] UNIQUE NONCLUSTERED ([ImportHeaderName] ASC, [ImportTemplateID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_ParentImportColumnID]
    ON [dbo].[ImportTemplateItem]([ParentImportColumnID] ASC) WITH (FILLFACTOR = 90);

