﻿CREATE TABLE [dbo].[Invoice] (
    [InvoiceID]        INT             IDENTITY (1, 1) NOT NULL,
    [AccountID]        INT             NULL,
    [AccountName]      VARCHAR (200)   NULL,
    [AccountTypeID]    INT             NULL,
    [NetAmountAUD]     DECIMAL (18, 2) NOT NULL,
    [GSTAmountAUD]     DECIMAL (18, 2) NULL,
    [GrossAmountAUD]   DECIMAL (18, 2) NULL,
    [InvoiceDate]      DATE            NOT NULL,
    [StartDate]        DATE            NOT NULL,
    [EndDate]          DATE            NOT NULL,
    [PaymentMethod]    CHAR (25)       NOT NULL,
    [PaypalID]         INT             NULL,
    [PaidDate]         DATETIME        NULL,
    [Notes]            VARCHAR (MAX)   NULL,
    [OrganisationName] VARCHAR (200)   NULL,
    [BillingEmail]     VARCHAR (200)   NULL,
    [BillingAddress]   VARCHAR (MAX)   NULL,
    [Country]          VARCHAR (20)    NULL,
    [ClientRef]        VARCHAR (20)    NULL,
    [DateAdded]        DATETIME        CONSTRAINT [DF_Invoice_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]      DATETIME        CONSTRAINT [DF_Invoice_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [PaidAmount]       DECIMAL (18, 2) NULL,
    CONSTRAINT [FK_Invoice_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_Invoice_AccountType] FOREIGN KEY ([AccountTypeID]) REFERENCES [dbo].[AccountType] ([AccountTypeID])
);


GO
CREATE NONCLUSTERED INDEX [IX_AccountTypeID]
    ON [dbo].[Invoice]([AccountTypeID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE TRIGGER [dbo].[tr_Invoice_Updated] ON [dbo].[Invoice] AFTER UPDATE AS 
	UPDATE Invoice 
		SET DateUpdated = GETDATE() 
		FROM inserted 
		WHERE (Invoice.InvoiceID = inserted.InvoiceID)