﻿CREATE TABLE [dbo].[MonitorScheduleUser] (
    [MonitorScheduleUserID] INT          IDENTITY (1, 1) NOT NULL,
    [MonitorScheduleID]     INT          NOT NULL,
    [UserID]                INT          NOT NULL,
    [EmailSMS]              VARCHAR (15) CONSTRAINT [DF_MonitorScheduleUser_EmailSMS] DEFAULT ('email') NULL,
    CONSTRAINT [PK_MonitorScheduleUser] PRIMARY KEY CLUSTERED ([MonitorScheduleUserID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_MonitorScheduleUser_MonitorSchedule] FOREIGN KEY ([MonitorScheduleID]) REFERENCES [dbo].[MonitorSchedule] ([MonitorScheduleID]) ON DELETE CASCADE,
    CONSTRAINT [FK_MonitorScheduleUser_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID]) ON DELETE CASCADE
);

