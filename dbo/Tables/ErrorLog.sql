﻿CREATE TABLE [dbo].[ErrorLog] (
    [ErrorLogID]   INT             IDENTITY (1, 1) NOT NULL,
    [Module]       NVARCHAR (50)   NOT NULL,
    [ErrorMessage] NVARCHAR (MAX)  NOT NULL,
    [ErrorTrack]   NVARCHAR (MAX)  NOT NULL,
    [ErrorTime]    DATETIME        NOT NULL,
    [Path]         NVARCHAR (1000) NULL,
    CONSTRAINT [PK_ErrorLog] PRIMARY KEY CLUSTERED ([ErrorLogID] ASC) WITH (FILLFACTOR = 90)
);

