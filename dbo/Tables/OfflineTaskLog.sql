﻿CREATE TABLE [dbo].[OfflineTaskLog] (
    [OfflineTaskLogID] INT           IDENTITY (1, 1) NOT NULL,
    [OfflineTaskID]    INT           NOT NULL,
    [Result]           VARCHAR (MAX) NOT NULL,
    [DateAdded]        DATETIME      CONSTRAINT [DF_OfflineTaskLog_DateAdded] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_OfflineTaskLogID] PRIMARY KEY CLUSTERED ([OfflineTaskLogID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_OfflineTaskLog_OfflineTask_OfflineTaskID] FOREIGN KEY ([OfflineTaskID]) REFERENCES [dbo].[OfflineTask] ([OfflineTaskID])
);

