﻿CREATE TABLE [dbo].[VisitorLog] (
    [VisitorLogID] INT            IDENTITY (1, 1) NOT NULL,
    [UserID]       INT            NULL,
    [IPAddress]    VARCHAR (20)   NULL,
    [Browser]      VARCHAR (500)  NULL,
    [PageURL]      VARCHAR (1000) NULL,
    [DateAdded]    DATETIME       CONSTRAINT [DF_VisitorLog_DateAdded] DEFAULT (getdate()) NULL,
    [RefSite]      VARCHAR (50)   NULL,
    CONSTRAINT [PK_VisitorLog] PRIMARY KEY CLUSTERED ([VisitorLogID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_VisitorLog_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])
);

