﻿CREATE TABLE [dbo].[Message] (
    [MessageID]          INT            IDENTITY (1, 1) NOT NULL,
    [RecordID]           INT            NULL,
    [DateTime]           DATETIME       NOT NULL,
    [MessageType]        CHAR (1)       NOT NULL,
    [DeliveryMethod]     CHAR (1)       NOT NULL,
    [IsIncoming]         BIT            NULL,
    [OtherParty]         VARCHAR (MAX)  NULL,
    [Subject]            VARCHAR (MAX)  NULL,
    [Body]               VARCHAR (MAX)  NOT NULL,
    [Link]               VARCHAR (MAX)  NULL,
    [ExternalMessageKey] VARCHAR (1000) NULL,
    [TableID]            INT            NULL,
    [AccountID]          INT            NULL,
    [IsSent]             BIT            NULL,
    [EmailTypeOrKey]     VARCHAR (227)  NULL,
    [DateTimeSent]       DATETIME       NULL,
    [AttachmentFileName] VARCHAR (MAX)  NULL,
    [Parameters]         VARCHAR (MAX)  NULL,
    CONSTRAINT [PK_MessageID] PRIMARY KEY CLUSTERED ([MessageID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Message_Account_AccountID] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_Message_Table_TableID] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_Message_DateTime]
    ON [dbo].[Message]([DateTime] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Message_RecordID]
    ON [dbo].[Message]([RecordID] ASC) WITH (FILLFACTOR = 90);

