﻿CREATE TABLE [dbo].[ReportTableItem] (
    [ReportTableItemID] INT            IDENTITY (1, 1) NOT NULL,
    [ReportItemID]      INT            NOT NULL,
    [ColumnID]          INT            NOT NULL,
    [ColumnTitle]       VARCHAR (1000) NOT NULL,
    [ColumnPosition]    INT            NOT NULL,
    CONSTRAINT [PK_ReportTableItem] PRIMARY KEY CLUSTERED ([ReportTableItemID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ReportTableItem_Column] FOREIGN KEY ([ColumnID]) REFERENCES [dbo].[Column] ([ColumnID]) ON DELETE CASCADE,
    CONSTRAINT [FK_ReportTableItem_ReportItem] FOREIGN KEY ([ReportItemID]) REFERENCES [dbo].[ReportItem] ([ReportItemID]) ON DELETE CASCADE
);

