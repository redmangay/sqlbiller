﻿CREATE TABLE [dbo].[OfflineTask] (
    [OfflineTaskID]  INT           IDENTITY (1, 1) NOT NULL,
    [AccountID]      INT           NOT NULL,
    [AddedByUserID]  INT           NOT NULL,
    [Priority]       TINYINT       NOT NULL,
    [Processtorun]   VARCHAR (200) NOT NULL,
    [Parameters]     VARCHAR (MAX) NULL,
    [RepeatMins]     INT           NULL,
    [ScheduledToRun] DATETIME      NULL,
    [ActuallyRun]    DATETIME      NULL,
    [DateAdded]      DATETIME      CONSTRAINT [DF_OfflineTask_DateAdded] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_OfflineTaskID] PRIMARY KEY CLUSTERED ([OfflineTaskID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_OfflineTask_Account_AccountID] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_OfflineTask_User_AddedByUserID] FOREIGN KEY ([AddedByUserID]) REFERENCES [dbo].[User] ([UserID])
);

