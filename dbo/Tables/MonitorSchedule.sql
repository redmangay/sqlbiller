﻿CREATE TABLE [dbo].[MonitorSchedule] (
    [MonitorScheduleID] INT            IDENTITY (1, 1) NOT NULL,
    [AccountID]         INT            NOT NULL,
    [TableID]           INT            NULL,
    [ScheduleDateTime]  DATETIME       NOT NULL,
    [Description]       NVARCHAR (250) NOT NULL,
    [HasAlarm]          BIT            CONSTRAINT [DF_MonitorSchedule_HasAlarm] DEFAULT ((0)) NOT NULL,
    [AlarmDateTime]     DATETIME       NULL,
    [DateAdded]         DATETIME       CONSTRAINT [DF_MonitorSchedule_DateAdded] DEFAULT (getdate()) NULL,
    [DateUpdated]       DATETIME       CONSTRAINT [DF_MonitorSchedule_DateUpadted] DEFAULT (getdate()) NULL,
    [UserAdded]         INT            NULL,
    [UserUpdated]       INT            NULL,
    [InitialScheduleID] INT            NULL,
    [IsAlarmSent]       BIT            CONSTRAINT [DF_MonitorSchedule_IsAlarmSent] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MonitorSchedule] PRIMARY KEY CLUSTERED ([MonitorScheduleID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_MonitorSchedule_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_MonitorSchedule_SampleType] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [FK_MonitorSchedule_User] FOREIGN KEY ([UserAdded]) REFERENCES [dbo].[User] ([UserID]),
    CONSTRAINT [FK_MonitorSchedule_User1] FOREIGN KEY ([UserUpdated]) REFERENCES [dbo].[User] ([UserID])
);


GO
CREATE TRIGGER [dbo].[tg_MonitorScheduleUpdated] ON [dbo].[MonitorSchedule] AFTER UPDATE AS UPDATE MonitorSchedule
 SET DateUpdated = GETDATE() FROM inserted WHERE (MonitorSchedule.MonitorScheduleID = inserted.MonitorScheduleID)