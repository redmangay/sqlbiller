﻿CREATE TABLE [dbo].[GraphOption] (
    [GraphOptionID]      INT              IDENTITY (1, 1) NOT NULL,
    [AccountID]          INT              NOT NULL,
    [GraphPanel]         INT              NOT NULL,
    [Heading]            VARCHAR (100)    NULL,
    [ShowLimits]         BIT              NULL,
    [ShowMissing]        BIT              NULL,
    [TimePeriod]         CHAR (1)         NULL,
    [FromDate]           DATETIME         NULL,
    [ToDate]             DATETIME         NULL,
    [Legend]             VARCHAR (10)     NULL,
    [Width]              DECIMAL (18, 10) NULL,
    [Height]             DECIMAL (18, 10) NULL,
    [UserReportDate]     BIT              NULL,
    [CustomTimePeriod]   CHAR (1)         NULL,
    [Display3D]          BIT              NULL,
    [ReportChart]        BIT              CONSTRAINT [DF_GraphOption_ReportChart] DEFAULT ((0)) NULL,
    [IsActive]           BIT              CONSTRAINT [DF_GraphOption_IsActive] DEFAULT ((1)) NULL,
    [WarningCaption]     NVARCHAR (100)   NULL,
    [WarningValue]       DECIMAL (18, 10) NULL,
    [WarningColor]       VARCHAR (50)     NULL,
    [ExceedanceCaption]  NVARCHAR (100)   NULL,
    [ExceedanceValue]    DECIMAL (18, 10) NULL,
    [ExceedanceColor]    VARCHAR (50)     NULL,
    [DateFormat]         CHAR (2)         NULL,
    [SubHeading]         VARCHAR (100)    NULL,
    [GraphDefinitionID]  INT              NULL,
    [YAxisHighestValue]  DECIMAL (18, 10) NULL,
    [YAxisLowestValue]   DECIMAL (18, 10) NULL,
    [YAxisInterval]      DECIMAL (18, 10) NULL,
    [WarningValueMin]    DECIMAL (18, 10) NULL,
    [ExceedanceValueMin] DECIMAL (18, 10) NULL,
    [GraphName]          VARCHAR (200)    NULL,
    [VisibleToUser]      INT              NULL,
    [MultiChartData]     VARCHAR (MAX)    NULL,
    [YAxisOrder]         INT              NULL,
    [ShowTrendline]      BIT              NULL,
    [SeriesColumnID]     INT              NULL,
    [SeriesDataSelected] VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_GraphOption] PRIMARY KEY CLUSTERED ([GraphOptionID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_GraphOption_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_GraphOption_GraphDefinition] FOREIGN KEY ([GraphDefinitionID]) REFERENCES [dbo].[GraphDefinition] ([GraphDefinitionID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_GraphDefinitionID]
    ON [dbo].[GraphOption]([GraphDefinitionID] ASC) WITH (FILLFACTOR = 90);

