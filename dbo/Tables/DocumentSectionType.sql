﻿CREATE TABLE [dbo].[DocumentSectionType] (
    [DocumentSectionTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [SectionType]           VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_dbo.dgDocumentSectionType] PRIMARY KEY CLUSTERED ([DocumentSectionTypeID] ASC) WITH (FILLFACTOR = 90)
);

