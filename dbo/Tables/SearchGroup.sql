﻿CREATE TABLE [dbo].[SearchGroup] (
    [SearchGroupID] INT           IDENTITY (1, 1) NOT NULL,
    [GroupName]     VARCHAR (250) NOT NULL,
    [TableID]       INT           NOT NULL,
    [DisplayOrder]  INT           DEFAULT ((1)) NOT NULL,
    [SummarySearch] BIT           NULL,
    CONSTRAINT [pk_SearchGroup_SearchGroupID] PRIMARY KEY CLUSTERED ([SearchGroupID] ASC) WITH (FILLFACTOR = 90)
);

