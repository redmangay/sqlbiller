﻿CREATE TABLE [dbo].[QuickLink] (
    [QuickLinkID] INT           IDENTITY (1, 1) NOT NULL,
    [URL]         VARCHAR (400) NULL,
    [RecordID]    INT           DEFAULT (NULL) NULL,
    PRIMARY KEY CLUSTERED ([QuickLinkID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_QuickLink_URL]
    ON [dbo].[QuickLink]([URL] ASC) WITH (FILLFACTOR = 90);

