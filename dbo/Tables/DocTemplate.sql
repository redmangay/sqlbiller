﻿CREATE TABLE [dbo].[DocTemplate] (
    [DocTemplateID]   INT            IDENTITY (1, 1) NOT NULL,
    [FileUniqueName]  NVARCHAR (500) NOT NULL,
    [FileName]        NVARCHAR (400) NOT NULL,
    [DataRetrieverID] INT            NOT NULL,
    CONSTRAINT [PK_DocTemplate] PRIMARY KEY CLUSTERED ([DocTemplateID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_DocTemplate_DataRetriever] FOREIGN KEY ([DataRetrieverID]) REFERENCES [dbo].[DataRetriever] ([DataRetrieverID])
);


GO
CREATE NONCLUSTERED INDEX [IX_DataRetrieverID]
    ON [dbo].[DocTemplate]([DataRetrieverID] ASC) WITH (FILLFACTOR = 90);

