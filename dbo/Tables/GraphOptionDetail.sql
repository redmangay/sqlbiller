﻿CREATE TABLE [dbo].[GraphOptionDetail] (
    [GraphOptionDetailID] INT              IDENTITY (1, 1) NOT NULL,
    [GraphOptionID]       INT              NOT NULL,
    [TableID]             INT              NOT NULL,
    [ColumnID]            INT              NOT NULL,
    [Label]               VARCHAR (200)    NULL,
    [Axis]                VARCHAR (20)     NULL,
    [GraphOrder]          INT              NULL,
    [Scale]               DECIMAL (18, 10) NULL,
    [GraphType]           VARCHAR (20)     NULL,
    [Colour]              VARCHAR (20)     NULL,
    [High]                DECIMAL (18, 10) NULL,
    [Low]                 DECIMAL (18, 10) NULL,
    [GraphSeriesColumnID] VARCHAR (20)     NULL,
    [GraphSeriesID]       VARCHAR (200)    NULL,
    [TrueTableID]         INT              NULL,
    CONSTRAINT [PK_GraphOptionDetail] PRIMARY KEY CLUSTERED ([GraphOptionDetailID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_GraphOptionDetail_GraphOption] FOREIGN KEY ([GraphOptionID]) REFERENCES [dbo].[GraphOption] ([GraphOptionID]) ON DELETE CASCADE,
    CONSTRAINT [FK_GraphOptionDetail_SampleColumn] FOREIGN KEY ([ColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_GraphOptionDetail_SampleType] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID])
);


GO
CREATE NONCLUSTERED INDEX [IX_ColumnID]
    ON [dbo].[GraphOptionDetail]([ColumnID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_GraphOptionID]
    ON [dbo].[GraphOptionDetail]([GraphOptionID] ASC) WITH (FILLFACTOR = 90);

