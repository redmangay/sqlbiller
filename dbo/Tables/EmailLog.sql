﻿CREATE TABLE [dbo].[EmailLog] (
    [EmailLogID]   INT             IDENTITY (1, 1) NOT NULL,
    [AccountID]    INT             NOT NULL,
    [EmailSubject] NVARCHAR (1000) NOT NULL,
    [EmailTo]      NVARCHAR (500)  NOT NULL,
    [EmailDate]    DATETIME        NOT NULL,
    [TableID]      INT             NULL,
    [RecordID]     INT             NULL,
    [EmailType]    VARCHAR (25)    NULL,
    [RawMessage]   NVARCHAR (MAX)  NOT NULL,
    [EmailUID]     VARCHAR (256)   NOT NULL,
    CONSTRAINT [PK_EmailLog] PRIMARY KEY CLUSTERED ([EmailLogID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_EmailLog_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_EmailLog_Table] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID])
);

