﻿CREATE TABLE [dbo].[Usage] (
    [UsageID]       INT      IDENTITY (1, 1) NOT NULL,
    [AccountID]     INT      NOT NULL,
    [Date]          DATETIME NOT NULL,
    [SignedInCount] INT      NULL,
    [UploadedCount] INT      NULL,
    CONSTRAINT [PK_Usage] PRIMARY KEY CLUSTERED ([UsageID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Usage_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
);

