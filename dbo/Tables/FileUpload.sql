﻿CREATE TABLE [dbo].[FileUpload] (
    [FileUploadID]     INT           IDENTITY (1, 1) NOT NULL,
    [TableID]          INT           NOT NULL,
    [UploadName]       VARCHAR (100) NOT NULL,
    [Location]         VARCHAR (250) NOT NULL,
    [Filename]         VARCHAR (250) NOT NULL,
    [UseMapping]       BIT           CONSTRAINT [DF_FileUpload_UseMapping] DEFAULT ((0)) NOT NULL,
    [TemplateID]       INT           NULL,
    [FileNameCriteria] VARCHAR (250) NULL,
    [DocumentTypeID]   INT           NULL,
    [SaveDocument]     BIT           NULL,
    CONSTRAINT [PK_FileUpload] PRIMARY KEY CLUSTERED ([FileUploadID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_FileUpload_DocumentType] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentType] ([DocumentTypeID]),
    CONSTRAINT [FK_FileUpload_ImportTemplate] FOREIGN KEY ([TemplateID]) REFERENCES [dbo].[ImportTemplate] ([ImportTemplateID]),
    CONSTRAINT [FK_FileUpload_Table] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID])
);

