﻿CREATE TABLE [dbo].[ViewItem] (
    [ViewItemID]           INT          IDENTITY (1, 1) NOT NULL,
    [ViewID]               INT          NOT NULL,
    [ColumnID]             INT          NOT NULL,
    [SearchField]          BIT          NULL,
    [FilterField]          BIT          DEFAULT ((1)) NOT NULL,
    [Alignment]            VARCHAR (25) NULL,
    [Width]                INT          NULL,
    [ShowTotal]            BIT          NULL,
    [SummaryCellBackColor] VARCHAR (50) NULL,
    [ColumnIndex]          INT          DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ViewItem_ViewItemID] PRIMARY KEY CLUSTERED ([ViewItemID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ViewItem_Column_ColumnID] FOREIGN KEY ([ColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_ViewItem_View_ViewID] FOREIGN KEY ([ViewID]) REFERENCES [dbo].[View] ([ViewID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ColumnID]
    ON [dbo].[ViewItem]([ColumnID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_ViewItem_ViewIDColumnID]
    ON [dbo].[ViewItem]([ViewID] ASC, [ColumnID] ASC) WITH (FILLFACTOR = 90);

