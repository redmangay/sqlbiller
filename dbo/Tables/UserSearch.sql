﻿CREATE TABLE [dbo].[UserSearch] (
    [UserSearchID] INT           IDENTITY (1, 1) NOT NULL,
    [UserID]       INT           NOT NULL,
    [ViewID]       INT           NOT NULL,
    [SearchXML]    VARCHAR (MAX) NOT NULL,
    [DateUpdated]  DATETIME      CONSTRAINT [DF_UserSearch_DateUpdated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_UserSearch] PRIMARY KEY CLUSTERED ([UserSearchID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [UQ_UserID_ViewID] UNIQUE NONCLUSTERED ([UserID] ASC, [ViewID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_UserID]
    ON [dbo].[UserSearch]([UserID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_ViewID]
    ON [dbo].[UserSearch]([ViewID] ASC) WITH (FILLFACTOR = 90);

