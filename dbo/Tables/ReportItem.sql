﻿CREATE TABLE [dbo].[ReportItem] (
    [ReportItemID]      INT           IDENTITY (1, 1) NOT NULL,
    [ReportID]          INT           NOT NULL,
    [ObjectID]          INT           NOT NULL,
    [ItemType]          INT           NOT NULL,
    [ItemTitle]         VARCHAR (500) NULL,
    [ItemPosition]      INT           NOT NULL,
    [PeriodColumnID]    INT           NULL,
    [UseColors]         BIT           CONSTRAINT [DF_ReportItem_UseColors] DEFAULT ((0)) NULL,
    [DateAdded]         DATETIME      CONSTRAINT [DF_ReportItem_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]       DATETIME      CONSTRAINT [DF_ReportItem_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [UserID]            INT           NULL,
    [ApplyFilter]       BIT           NULL,
    [ApplySort]         BIT           NULL,
    [HighlightWarnings] BIT           NULL,
    CONSTRAINT [PK_ReportItem] PRIMARY KEY CLUSTERED ([ReportItemID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_ReportItem] CHECK ([ItemType]>=(0) AND [ItemType]<=(3)),
    CONSTRAINT [FK_ReportItem_Column] FOREIGN KEY ([PeriodColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_ReportItem_Report] FOREIGN KEY ([ReportID]) REFERENCES [dbo].[Report] ([ReportID]) ON DELETE CASCADE,
    CONSTRAINT [FK_ReportItem_ReportItem] FOREIGN KEY ([ReportItemID]) REFERENCES [dbo].[ReportItem] ([ReportItemID]),
    CONSTRAINT [FK_ReportItem_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])
);

