﻿CREATE TABLE [dbo].[xSQLUpdates] (
    [Name]        VARCHAR (MAX) NULL,
    [DateAdded]   DATETIME      NULL,
    [Description] VARCHAR (MAX) NULL,
    [Sequence]    INT           NOT NULL
);

