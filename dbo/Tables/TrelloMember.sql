﻿CREATE TABLE [dbo].[TrelloMember] (
    [TrelloMemberID] INT           IDENTITY (1, 1) NOT NULL,
    [RefId]          VARCHAR (50)  NOT NULL,
    [Username]       VARCHAR (50)  NOT NULL,
    [Initials]       VARCHAR (10)  NULL,
    [AvatarHash]     VARCHAR (100) NULL,
    CONSTRAINT [PK_TrelloMember] PRIMARY KEY CLUSTERED ([TrelloMemberID] ASC) WITH (FILLFACTOR = 90)
);

