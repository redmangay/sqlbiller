﻿CREATE TABLE [dbo].[Content_Changes] (
    [ChangeID]        INT            IDENTITY (1, 1) NOT NULL,
    [ContentID]       INT            NULL,
    [ContentKey]      NVARCHAR (50)  NULL,
    [Heading]         NVARCHAR (200) NULL,
    [Content]         NVARCHAR (MAX) NULL,
    [DateCreated]     DATETIME       CONSTRAINT [DF_Content_Changes_DateCreated] DEFAULT (getdate()) NULL,
    [ContentType]     INT            NULL,
    [StoredProcedure] NVARCHAR (200) NULL,
    PRIMARY KEY CLUSTERED ([ChangeID] ASC) WITH (FILLFACTOR = 90)
);

