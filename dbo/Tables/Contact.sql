﻿CREATE TABLE [dbo].[Contact] (
    [ContactID]        INT            IDENTITY (1, 1) NOT NULL,
    [Email]            VARCHAR (150)  NOT NULL,
    [SubscriptionDate] DATETIME       CONSTRAINT [DF_Subscription_SubscriptionDate] DEFAULT (getdate()) NOT NULL,
    [Name]             VARCHAR (150)  NULL,
    [Phone]            VARCHAR (150)  NULL,
    [Message]          VARCHAR (1000) NULL,
    [DateAdded]        DATETIME       CONSTRAINT [DF_Contact_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]      DATETIME       CONSTRAINT [DF_Contact_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [ContactTypeID]    INT            CONSTRAINT [DF_Contact_ContactTypeID] DEFAULT ((1)) NULL,
    CONSTRAINT [PK_Subscription] PRIMARY KEY CLUSTERED ([ContactID] ASC) WITH (FILLFACTOR = 90)
);


GO
Create TRIGGER [dbo].[ContactUpdated] ON [dbo].[Contact] AFTER UPDATE AS UPDATE [Contact]
 SET DateUpdated = GETDATE() FROM inserted WHERE ([Contact].ContactID = inserted.ContactID)
