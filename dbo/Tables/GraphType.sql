﻿CREATE TABLE [dbo].[GraphType] (
    [GraphTypeID]   INT          IDENTITY (1, 1) NOT NULL,
    [GraphTypeName] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_GraphType] PRIMARY KEY CLUSTERED ([GraphTypeID] ASC) WITH (FILLFACTOR = 90)
);

