﻿CREATE TABLE [dbo].[Folder] (
    [FolderID]       INT           IDENTITY (1, 1) NOT NULL,
    [AccountID]      INT           NOT NULL,
    [ParentFolderID] INT           NULL,
    [FolderName]     VARCHAR (250) NOT NULL,
    CONSTRAINT [PK_Folder] PRIMARY KEY CLUSTERED ([FolderID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Folder_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_Folder_Folder] FOREIGN KEY ([ParentFolderID]) REFERENCES [dbo].[Folder] ([FolderID])
);


GO
CREATE NONCLUSTERED INDEX [IX_ParentFolderID]
    ON [dbo].[Folder]([ParentFolderID] ASC) WITH (FILLFACTOR = 90);

