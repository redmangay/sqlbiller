﻿CREATE TABLE [dbo].[TableTab] (
    [TableTabID]   INT           IDENTITY (1, 1) NOT NULL,
    [TableID]      INT           NOT NULL,
    [TabName]      VARCHAR (200) NOT NULL,
    [DisplayOrder] INT           NOT NULL,
    CONSTRAINT [pk_TableTab_TableTabID] PRIMARY KEY CLUSTERED ([TableTabID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [fk_TableTab_TableID] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID])
);

