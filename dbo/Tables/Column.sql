﻿CREATE TABLE [dbo].[Column] (
    [ColumnID]                    INT              IDENTITY (1, 1) NOT NULL,
    [TableID]                     INT              NOT NULL,
    [SystemName]                  VARCHAR (50)     NOT NULL,
    [DisplayName]                 VARCHAR (100)    NULL,
    [DisplayTextSummary]          VARCHAR (100)    NULL,
    [DisplayTextDetail]           VARCHAR (255)    NULL,
    [NameOnImport_DELETED]        VARCHAR (50)     NULL,
    [NameOnExport_DELETED]        VARCHAR (50)     NULL,
    [MobileName_DELETED]          VARCHAR (100)    NULL,
    [DisplayOrder]                INT              NULL,
    [DisplayRight]                BIT              CONSTRAINT [DF_SampleColumn_DisplayRight] DEFAULT ((0)) NOT NULL,
    [Alignment]                   VARCHAR (25)     NULL,
    [TextWidth]                   INT              NULL,
    [TextHeight]                  INT              NULL,
    [IsStandard]                  BIT              CONSTRAINT [DF_SampleColumn_IsStandard] DEFAULT ((0)) NOT NULL,
    [GraphTypeID]                 INT              NULL,
    [GraphLabel]                  VARCHAR (100)    NULL,
    [ValidationOnWarning]         NVARCHAR (4000)  NULL,
    [ValidationOnEntry]           NVARCHAR (4000)  NULL,
    [CheckUnlikelyValue]          BIT              CONSTRAINT [DF_SampleColumn_CheckUnlikelyValue] DEFAULT ((0)) NOT NULL,
    [PositionOnImport_DELETED]    INT              NULL,
    [Constant]                    VARCHAR (MAX)    NULL,
    [Calculation]                 NVARCHAR (4000)  NULL,
    [ShowTotal]                   BIT              CONSTRAINT [DF_SampleColumn_ShowTotal] DEFAULT ((0)) NOT NULL,
    [IgnoreSymbols]               BIT              CONSTRAINT [DF_SampleColumn_IgnoreSymbols] DEFAULT ((0)) NOT NULL,
    [IsRound]                     BIT              CONSTRAINT [DF_SampleColumn_IsRound] DEFAULT ((0)) NOT NULL,
    [RoundNumber]                 INT              CONSTRAINT [DF_SampleColumn_RoundNumber] DEFAULT ((2)) NOT NULL,
    [Notes]                       NVARCHAR (MAX)   NULL,
    [DropdownValues]              VARCHAR (MAX)    NULL,
    [NumberType]                  INT              NULL,
    [DefaultValue]                VARCHAR (MAX)    NULL,
    [AvgColumnID]                 INT              NULL,
    [AvgNumberOfRecords]          INT              NULL,
    [IsDateSingleColumn_DELETED]  BIT              CONSTRAINT [DF_SampleColumn_IsDateSingleColumn] DEFAULT ((0)) NULL,
    [ShowGraphExceedance]         DECIMAL (18, 10) NULL,
    [ShowGraphWarning]            DECIMAL (18, 10) NULL,
    [FlatLineNumber]              INT              NULL,
    [MaxValueAt]                  DECIMAL (18, 10) NULL,
    [ColumnType]                  VARCHAR (15)     CONSTRAINT [DF_SampleColumn_ColumnType] DEFAULT ('text') NOT NULL,
    [DropDownType]                VARCHAR (15)     NULL,
    [TableTableID]                INT              NULL,
    [DisplayColumn]               VARCHAR (MAX)    NULL,
    [SummaryCellBackColor]        VARCHAR (50)     NULL,
    [FormVerticalPosition]        INT              NULL,
    [FormHorizontalPosition]      INT              NULL,
    [ParentColumnID]              INT              NULL,
    [TextType]                    VARCHAR (50)     NULL,
    [RegEx]                       VARCHAR (MAX)    NULL,
    [HideColumnID]                INT              NULL,
    [HideColumnValue]             NVARCHAR (250)   NULL,
    [DateCalculationType]         VARCHAR (15)     NULL,
    [OnlyForAdmin_DELETED]        INT              NULL,
    [IsSystemColumn]              BIT              CONSTRAINT [DF_Column_IsSystemColumn] DEFAULT ((0)) NOT NULL,
    [LinkedParentColumnID]        INT              NULL,
    [DataRetrieverID]             INT              NULL,
    [VerticalList]                BIT              NULL,
    [SummarySearch_DELETED]       BIT              CONSTRAINT [DF_Column_SummarySearch] DEFAULT ((1)) NULL,
    [QuickAddLink]                BIT              NULL,
    [HideOperator]                VARCHAR (25)     NULL,
    [CalculationIsActive]         BIT              NULL,
    [DateAdded]                   DATETIME         CONSTRAINT [DF_SampleColumn_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]                 DATETIME         CONSTRAINT [DF_SampleColumn_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [LastUpdatedUserID]           INT              NULL,
    [TableTabID]                  INT              NULL,
    [ViewName]                    VARCHAR (50)     NULL,
    [SPDefaultValue]              VARCHAR (200)    NULL,
    [DefaultType]                 VARCHAR (20)     NULL,
    [DefaultColumnID]             INT              NULL,
    [ShowViewLink]                VARCHAR (10)     NULL,
    [FilterParentColumnID]        INT              NULL,
    [FilterOtherColumnID]         INT              NULL,
    [FilterValue]                 VARCHAR (MAX)    NULL,
    [MapPinHoverColumnID]         INT              NULL,
    [CompareColumnID]             INT              NULL,
    [CompareOperator]             VARCHAR (50)     NULL,
    [MapPopup]                    VARCHAR (MAX)    NULL,
    [TrafficLightColumnID]        INT              NULL,
    [TrafficLightValues]          VARCHAR (MAX)    NULL,
    [DefaultRelatedTableID]       INT              NULL,
    [DefaultUpdateValues]         BIT              NULL,
    [ValidationCanIgnore]         BIT              NULL,
    [DefaultGraphDefinitionID]    INT              NULL,
    [ImageOnSummary]              BIT              NULL,
    [AllowCopy]                   BIT              NULL,
    [ValidationOnExceedance]      NVARCHAR (4000)  NULL,
    [ColourCells]                 BIT              NULL,
    [ButtonInfo]                  VARCHAR (1000)   NULL,
    [FilterOperator]              VARCHAR (25)     NULL,
    [Importance]                  CHAR (1)         NULL,
    [IsReadOnly]                  BIT              NULL,
    [ShowGraphWarningMin]         DECIMAL (18, 10) NULL,
    [ShowGraphExceedanceMin]      DECIMAL (18, 10) NULL,
    [Xcoordinate]                 INT              NULL,
    [Ycoordinate]                 INT              NULL,
    [HideTimeSecond]              BIT              NULL,
    [ColSpan]                     TINYINT          NULL,
    [RowSpan]                     TINYINT          NULL,
    [FilterParentColumnID2]       INT              NULL,
    [FilterOtherColumnID2]        INT              NULL,
    [FilterValue2]                VARCHAR (MAX)    NULL,
    [FilterOperator2]             VARCHAR (25)     NULL,
    [IgnoreSymbolMode]            INT              NULL,
    [IgnoreSymbolConstant]        DECIMAL (18, 10) NULL,
    [ShowAllValuesOnSearch]       BIT              DEFAULT ((0)) NULL,
    [CalculationNullAsZero]       BIT              DEFAULT ((1)) NULL,
    [ChildTableID]                INT              NULL,
    [EditableOnSummary]           BIT              NULL,
    [RolesWhoCanIgnoreValidation] VARCHAR (MAX)    NULL,
    [SQLRetrieval]                VARCHAR (MAX)    NULL,
    [TriggerColumnID]             INT              NULL,
    [SQLRetrievalOption]          VARCHAR (100)    NULL,
    CONSTRAINT [PK_DataFields] PRIMARY KEY CLUSTERED ([ColumnID] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([TriggerColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Column_Column_AvgColumnID] FOREIGN KEY ([AvgColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Column_Column_CompareColumnID] FOREIGN KEY ([CompareColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Column_Column_DefaultColumnID] FOREIGN KEY ([DefaultColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Column_Column_HideColumnID] FOREIGN KEY ([HideColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Column_Column_LinkedParentColumnID] FOREIGN KEY ([LinkedParentColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Column_Column_MapPinHoverColumnID] FOREIGN KEY ([MapPinHoverColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Column_Column_TrafficLightColumnID] FOREIGN KEY ([TrafficLightColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Column_DataRetriever_DataRetrieverID] FOREIGN KEY ([DataRetrieverID]) REFERENCES [dbo].[DataRetriever] ([DataRetrieverID]),
    CONSTRAINT [FK_Column_FilterOtherColumnID] FOREIGN KEY ([FilterOtherColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Column_FilterParentColumnID] FOREIGN KEY ([FilterParentColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Column_GraphDefinition] FOREIGN KEY ([DefaultGraphDefinitionID]) REFERENCES [dbo].[GraphDefinition] ([GraphDefinitionID]),
    CONSTRAINT [FK_Column_GraphType] FOREIGN KEY ([GraphTypeID]) REFERENCES [dbo].[GraphType] ([GraphTypeID]),
    CONSTRAINT [FK_Column_Table] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [FK_Column_Table_DefaultRelatedTableID] FOREIGN KEY ([DefaultRelatedTableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [FK_Column_TableTab_TableTabID] FOREIGN KEY ([TableTabID]) REFERENCES [dbo].[TableTab] ([TableTabID]),
    CONSTRAINT [FK_Column_User] FOREIGN KEY ([LastUpdatedUserID]) REFERENCES [dbo].[User] ([UserID]),
    CONSTRAINT [UQ_ColumnDisplayName] UNIQUE NONCLUSTERED ([TableID] ASC, [DisplayName] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_SampleTypeSystemName]
    ON [dbo].[Column]([TableID] ASC, [SystemName] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_TableID]
    ON [dbo].[Column]([TableID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_TableTabID]
    ON [dbo].[Column]([TableTabID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_AvgColumnID]
    ON [dbo].[Column]([AvgColumnID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_CompareColumnID]
    ON [dbo].[Column]([CompareColumnID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_DataRetrieverID]
    ON [dbo].[Column]([DataRetrieverID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_DefaultColumnID]
    ON [dbo].[Column]([DefaultColumnID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_DefaultGraphDefinitionID]
    ON [dbo].[Column]([DefaultGraphDefinitionID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_FilterOtherColumnID]
    ON [dbo].[Column]([FilterOtherColumnID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_FilterParentColumnID]
    ON [dbo].[Column]([FilterParentColumnID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_GraphTypeID]
    ON [dbo].[Column]([GraphTypeID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_HideColumnID]
    ON [dbo].[Column]([HideColumnID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_LinkedParentColumnID]
    ON [dbo].[Column]([LinkedParentColumnID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_MapPinHoverColumnID]
    ON [dbo].[Column]([MapPinHoverColumnID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_TrafficLightColumnID]
    ON [dbo].[Column]([TrafficLightColumnID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE TRIGGER [dbo].[tg_Column_Update] on [dbo].[Column] for insert, update
AS
BEGIN
	UPDATE [Column] SET DateUpdated = GETDATE() FROM inserted WHERE ([Column].ColumnID = inserted.ColumnID)

    DECLARE @bit int,
        @field int ,
        @maxfield int ,
        @char int ,
        @fieldname varchar(128) ,
        @TableName varchar(128) ,
        @PKCols varchar(1000) ,
        @sql varchar(2000),
        @UpdateDate varchar(21) ,
        @UserName varchar(128) ,
        @Type char(1) ,
        @PKFieldSelect varchar(1000),
        @PKValueSelect varchar(1000),
        @nPKID int,
		@nTableID int

    SELECT @TableName = 'Column'

    -- date and user
    SELECT @UserName = null ,
           @UpdateDate = convert(varchar(8), getdate(), 112) + ' ' + convert(varchar(12), getdate(), 114)

        -- Action
        IF EXISTS (SELECT * FROM inserted)

        BEGIN
                SELECT top 1 @UserName = LastUpdatedUserID FROM inserted
                SELECT top 1 @nPKID = ColumnID, @nTableID = TableID FROM inserted

                IF EXISTS (SELECT * FROM deleted)
                    SELECT @Type = 'U'
                ELSE
                    SELECT @Type = 'I'
        END
                ELSE
                    SELECT @Type = 'D'

             -- get list of columns

            SELECT * INTO #ins FROM inserted

            SELECT * INTO #del FROM deleted

            -- Get primary key columns for full outer join

            SELECT @PKCols = coalesce(@PKCols + ' and', ' on') + ' i.' + c.COLUMN_NAME + ' = d.' + c.COLUMN_NAME

                  FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,

                  INFORMATION_SCHEMA.KEY_COLUMN_USAGE c

                  WHERE pk.TABLE_NAME = @TableName

                  AND CONSTRAINT_TYPE = 'PRIMARY KEY'

                  AND c.TABLE_NAME = pk.TABLE_NAME

                  AND c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME

 

            -- Get primary key fields select for insert

            SELECT @PKFieldSelect = coalesce(@PKFieldSelect+'+','') + '''' + COLUMN_NAME + ''''

                  FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,

                  INFORMATION_SCHEMA.KEY_COLUMN_USAGE c

                  WHERE pk.TABLE_NAME = @TableName

                  AND CONSTRAINT_TYPE = 'PRIMARY KEY'

                  AND c.TABLE_NAME = pk.TABLE_NAME

                  AND c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME

 

            SELECT @PKValueSelect = coalesce(@PKValueSelect+'+','') + 'convert(varchar(100), coalesce(i.' + COLUMN_NAME + ',d.' + COLUMN_NAME + '))'

                  FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,  

                  INFORMATION_SCHEMA.KEY_COLUMN_USAGE c 

                  WHERE  pk.TABLE_NAME = @TableName 

                  AND CONSTRAINT_TYPE = 'PRIMARY KEY' 

                  AND c.TABLE_NAME = pk.TABLE_NAME 

                  AND c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME

 

            IF @PKCols IS NULL

            BEGIN

                  raiserror('no PK on table %s', 16, -1, @TableName)

                  RETURN

            END

 

            SELECT @field = 0, @maxfield = max(ORDINAL_POSITION) from INFORMATION_SCHEMA.COLUMNS 
            where TABLE_NAME = @TableName

            WHILE @field < @maxfield

            BEGIN

                  SELECT @field = min(ORDINAL_POSITION) from INFORMATION_SCHEMA.COLUMNS 
                  where TABLE_NAME = @TableName and ORDINAL_POSITION > @field

                  SELECT @bit = (@field - 1 )% 8 + 1

                  SELECT @bit = power(2,@bit - 1)

                  SELECT @char = ((@field - 1) / 8) + 1

                  SELECT @fieldname = COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where 
   TABLE_NAME = @TableName and ORDINAL_POSITION = @field

                  --IF substring(COLUMNS_UPDATED(),@char, 1) & @bit > 0 or @Type in ('I','D')
                  IF substring(COLUMNS_UPDATED(),1, 1)>0  or @Type in ('I','D')

                        AND @fieldname LIKE 'DateUpdated'

                        AND @fieldname LIKE 'LastUpdatedUserID'

                  BEGIN

                        -- SELECT @sql = 'insert Audit (Type, TableName, PrimaryKeyField, PrimaryKeyValue, FieldName, OldValue, NewValue, UpdateDate, UserName)'
						DECLARE @nAccountID int
						
						SELECT @nAccountID=AccountID FROM [Column] INNER JOIN [Table] ON [Column].TableID=[Table].TableID WHERE
						[Column].ColumnID=@nPKID --CAST(REPLACE (@PKValueSelect,'''','') AS INT)   --convert(int, REPLACE (@PKValueSelect,'''',''))
						
						
                        SELECT @sql = 'insert Audit (TableName,AccountID,PrimaryKeyValue, FieldName, 
                        OldValue, NewValue, DateAdded, UserID, TableID)'

                        SELECT @sql = @sql + ' select ''' + @TableName + ''''
						 SELECT @sql = @sql + ',' + cast( @nAccountID as varchar)                        

                        --SELECT @sql = @sql + ',' + @PKFieldSelect

                        SELECT @sql = @sql + ',' + @PKValueSelect

                        SELECT @sql = @sql + ',''' + @fieldname + ''''

                        SELECT @sql = @sql + ',convert(varchar(1000),d.' + @fieldname + ')'

                        SELECT @sql = @sql + ',convert(varchar(1000),i.' + @fieldname + ')'

                        SELECT @sql = @sql + ',''' + @UpdateDate + ''''

                        SELECT @sql = @sql + ',' + @UserName 

						SELECT @sql = @sql + ',' + cast( @nTableID as varchar)  

                        SELECT @sql = @sql + ' from #ins i full outer join #del d'

                        SELECT @sql = @sql + @PKCols

                        SELECT @sql = @sql + ' where i.' + @fieldname + ' <> d.' + @fieldname

                        SELECT @sql = @sql + ' or (i.' + @fieldname + ' is null and  d.' + @fieldname + ' is not null)'

                        SELECT @sql = @sql + ' or (i.' + @fieldname + ' is not null and  d.' + @fieldname + ' is null)'

                        EXEC (@sql)

                  END

            END
END
