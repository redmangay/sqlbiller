﻿CREATE TABLE [dbo].[ScheduleReport] (
    [ScheduleReportID] INT           IDENTITY (1, 1) NOT NULL,
    [MainDocumentID]   INT           NOT NULL,
    [Frequency]        CHAR (1)      NOT NULL,
    [FrequencyWhen]    VARCHAR (20)  NOT NULL,
    [DateAdded]        DATETIME      CONSTRAINT [DF_ScheduleReport_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]      DATETIME      CONSTRAINT [DF_ScheduleReport_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [LastCreatedDate]  DATETIME      CONSTRAINT [DF_ScheduleReport_LastCreatedDate] DEFAULT (getdate()) NOT NULL,
    [ReportPeriod]     INT           CONSTRAINT [DF_ScheduleReport_ReportPeriod] DEFAULT ((1)) NOT NULL,
    [ReportPeriodUnit] CHAR (1)      CONSTRAINT [DF_ScheduleReport_ReportPeriodUnit] DEFAULT ('M') NOT NULL,
    [Emails]           VARCHAR (MAX) NULL,
    CONSTRAINT [PK_ScheduleReport] PRIMARY KEY CLUSTERED ([ScheduleReportID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ScheduleReport_Document] FOREIGN KEY ([MainDocumentID]) REFERENCES [dbo].[Document] ([DocumentID])
);


GO
/****** Object:  Trigger [dbo].[tr_ScheduleReport_Updated]    Script Date: 11/26/2012 21:00:33 ******/

CREATE TRIGGER [dbo].[tr_ScheduleReport_Updated] ON [dbo].[ScheduleReport] AFTER UPDATE AS 
	UPDATE ScheduleReport 
		SET DateUpdated = GETDATE() 
		FROM inserted 
		WHERE (ScheduleReport.ScheduleReportID = inserted.ScheduleReportID)
