﻿CREATE TABLE [dbo].[TableUser] (
    [TableUserID]        INT          IDENTITY (1, 1) NOT NULL,
    [TableID]            INT          NOT NULL,
    [UserID]             INT          NULL,
    [LateWarningEmail]   BIT          CONSTRAINT [DF_SampleTypeUser_WarningEmail1] DEFAULT ((0)) NOT NULL,
    [LateWarningSMS]     BIT          CONSTRAINT [DF_SampleTypeUser_WarningSMS1] DEFAULT ((0)) NOT NULL,
    [UploadEmail]        BIT          CONSTRAINT [DF_SampleTypeUser_WarningEmail1_1] DEFAULT ((0)) NOT NULL,
    [UploadSMS]          BIT          CONSTRAINT [DF_SampleTypeUser_WarningSMS1_1] DEFAULT ((0)) NOT NULL,
    [UploadWarningEmail] BIT          CONSTRAINT [DF_SampleTypeUser_UploadWarningEmail] DEFAULT ((1)) NOT NULL,
    [UploadWarningSMS]   BIT          CONSTRAINT [DF_SampleTypeUser_UploadWarningSMS] DEFAULT ((0)) NOT NULL,
    [AddDataEmail]       BIT          NULL,
    [AddDataSMS]         BIT          NULL,
    [ExceedanceEmail]    BIT          NULL,
    [ExceedanceSMS]      BIT          NULL,
    [RecipientOption]    VARCHAR (20) NULL,
    [EmailColumnID]      INT          NULL,
    [PhoneColumnID]      INT          NULL,
    CONSTRAINT [PK_SampleTypeUser] PRIMARY KEY CLUSTERED ([TableUserID] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([EmailColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    FOREIGN KEY ([PhoneColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_SampleTypeUser_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])
);

