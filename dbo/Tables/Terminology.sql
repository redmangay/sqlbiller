﻿CREATE TABLE [dbo].[Terminology] (
    [TerminologyID] INT           IDENTITY (1, 1) NOT NULL,
    [AccountID]     INT           NULL,
    [PageName]      VARCHAR (300) NULL,
    [InputText]     VARCHAR (MAX) NOT NULL,
    [OutputText]    VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_Terminology] PRIMARY KEY CLUSTERED ([TerminologyID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Terminology_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
);

