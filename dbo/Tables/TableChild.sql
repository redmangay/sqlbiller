﻿CREATE TABLE [dbo].[TableChild] (
    [TableChildID]         INT            IDENTITY (1, 1) NOT NULL,
    [ParentTableID]        INT            NOT NULL,
    [ChildTableID]         INT            NOT NULL,
    [Description]          VARCHAR (250)  NULL,
    [DetailPageType]       VARCHAR (25)   NOT NULL,
    [ShowAddButton]        BIT            CONSTRAINT [DF_SampleTypeChild_ShowAddButton] DEFAULT ((0)) NOT NULL,
    [ShowEditButton]       BIT            CONSTRAINT [DF_SampleTypeChild_ShowEditButton] DEFAULT ((0)) NOT NULL,
    [DisplayOrder]         INT            NULL,
    [ConditionColumnID]    INT            NULL,
    [ConditionValue]       NVARCHAR (250) NULL,
    [ConditionOperator]    VARCHAR (25)   NULL,
    [ChildService_DELETED] VARCHAR (MAX)  NULL,
    [FilterSQL]            VARCHAR (MAX)  NULL,
    [DocumentID]           INT            NULL,
    [ShowGraphIcon]        BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_SampleTypeChild] PRIMARY KEY CLUSTERED ([TableChildID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_SampleTypeChild_SampleType] FOREIGN KEY ([ParentTableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [FK_SampleTypeChild_SampleType1] FOREIGN KEY ([ChildTableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [FK_TableChild_Column] FOREIGN KEY ([ConditionColumnID]) REFERENCES [dbo].[Column] ([ColumnID])
);

