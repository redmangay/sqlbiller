﻿CREATE TABLE [dbo].[zCleanData] (
    [TableID]      INT NULL,
    [RecordCount]  INT NULL,
    [SN]           INT NULL,
    [HasTime]      BIT NULL,
    [HasDateTime]  BIT NULL,
    [HasDuplicate] BIT NULL
);

