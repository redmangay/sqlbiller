﻿CREATE TABLE [dbo].[DataRetriever] (
    [DataRetrieverID]   INT           IDENTITY (1, 1) NOT NULL,
    [TableID]           INT           NOT NULL,
    [DataRetrieverName] VARCHAR (250) NOT NULL,
    [CodeSnippet]       VARCHAR (MAX) NULL,
    [SPName]            VARCHAR (250) NULL,
    CONSTRAINT [PK_DataRetriever] PRIMARY KEY CLUSTERED ([DataRetrieverID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_DataRetriever_Table] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID])
);

