﻿CREATE TABLE [dbo].[tempMeanMinMax] (
    [ID]               INT             NULL,
    [DateTimeRecorded] DATETIME        NULL,
    [Value]            DECIMAL (18, 8) NULL,
    [Percent]          DECIMAL (18, 4) NULL
);


GO
CREATE NONCLUSTERED INDEX [id]
    ON [dbo].[tempMeanMinMax]([ID] ASC) WITH (FILLFACTOR = 90);

