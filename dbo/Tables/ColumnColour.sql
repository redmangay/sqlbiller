﻿CREATE TABLE [dbo].[ColumnColour] (
    [ColumnColourID]      INT            IDENTITY (1, 1) NOT NULL,
    [ControllingColumnID] INT            NOT NULL,
    [Operator]            VARCHAR (25)   NOT NULL,
    [Value]               NVARCHAR (250) NULL,
    [ColourText]          VARCHAR (10)   NOT NULL,
    [ID]                  INT            NOT NULL,
    [Context]             VARCHAR (25)   NOT NULL,
    [ColourCell]          VARCHAR (10)   NULL,
    CONSTRAINT [PK_ColumnColourID] PRIMARY KEY CLUSTERED ([ColumnColourID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ColumnColour_Column_ControllingColumnID] FOREIGN KEY ([ControllingColumnID]) REFERENCES [dbo].[Column] ([ColumnID])
);


GO
CREATE NONCLUSTERED INDEX [IX_ControllingColumnID]
    ON [dbo].[ColumnColour]([ControllingColumnID] ASC) WITH (FILLFACTOR = 90);

