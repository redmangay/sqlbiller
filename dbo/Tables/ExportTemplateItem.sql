﻿CREATE TABLE [dbo].[ExportTemplateItem] (
    [ExportTemplateItemID] INT           IDENTITY (1, 1) NOT NULL,
    [ExportTemplateID]     INT           NOT NULL,
    [ColumnID]             INT           NOT NULL,
    [ExportHeaderName]     VARCHAR (250) NOT NULL,
    [ColumnIndex]          INT           NOT NULL,
    [Option]               VARCHAR (MAX) NULL,
    CONSTRAINT [pk_ExportTemplateItem_ExportTemplateItemID] PRIMARY KEY CLUSTERED ([ExportTemplateItemID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [fk_ExportTemplate_ColumnID] FOREIGN KEY ([ColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [fk_ExportTemplate_ExportTemplateID] FOREIGN KEY ([ExportTemplateID]) REFERENCES [dbo].[ExportTemplate] ([ExportTemplateID]),
    CONSTRAINT [UX_ExportTemplateIDExportHeaderName] UNIQUE NONCLUSTERED ([ExportTemplateID] ASC, [ExportHeaderName] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_ColumnID]
    ON [dbo].[ExportTemplateItem]([ColumnID] ASC) WITH (FILLFACTOR = 90);

