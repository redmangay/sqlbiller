﻿CREATE TABLE [dbo].[UserFolder] (
    [UserFolderID] INT          IDENTITY (1, 1) NOT NULL,
    [FolderID]     INT          NULL,
    [UserID]       INT          NOT NULL,
    [RightType]    VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_UserFolder] PRIMARY KEY CLUSTERED ([UserFolderID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_UserFolder_Folder] FOREIGN KEY ([FolderID]) REFERENCES [dbo].[Folder] ([FolderID]),
    CONSTRAINT [FK_UserFolder_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])
);

