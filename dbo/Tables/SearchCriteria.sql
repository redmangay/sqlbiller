﻿CREATE TABLE [dbo].[SearchCriteria] (
    [SearchCriteriaID] INT            IDENTITY (1, 1) NOT NULL,
    [SearchText]       NVARCHAR (MAX) NULL,
    [DateAdded]        DATETIME       CONSTRAINT [DF_SearchCriteria_DateAdded] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK__SearchCr__BDE918B255009F39] PRIMARY KEY CLUSTERED ([SearchCriteriaID] ASC) WITH (FILLFACTOR = 90)
);


GO
-- =============================================
-- Author:		Anton
-- Create date: Nov 07 2016
-- Description:	
-- =============================================
CREATE TRIGGER dbo.tr_UpdateBoundedSConInsert 
   ON  dbo.SearchCriteria 
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE 
	@RC int,
	@nViewID int,
	@TableID int,
	@TimesheetTableID int=2488, --CONFIGURABLE
	@DisbursementTableID int=2725, --CONFIGURABLE
	@xml XML

SELECT TOP 1 @xml=i.[SearchText]
  FROM INSERTED i 
  ORDER BY i.[SearchCriteriaID] DESC 

  
SELECT @nViewID=@xml.value('(/root/strViewID)[1]', 'int') 
SELECT @TableID=TableID FROM [dbo].[View]  WHERE ViewID=@nViewID
IF @TableID=@TimesheetTableID or @TableID=@DisbursementTableID
	EXECUTE @RC = [dbo].[SearchCriteria_UpdateBoundedSC] 
		@nViewID

END

GO
DISABLE TRIGGER [dbo].[tr_UpdateBoundedSConInsert]
    ON [dbo].[SearchCriteria];

