﻿CREATE TABLE [dbo].[xDelete_dgDataMap] (
    [DataMapID]           INT           IDENTITY (1, 1) NOT NULL,
    [DisplayText]         VARCHAR (200) NOT NULL,
    [StoredProcedureName] VARCHAR (200) NOT NULL,
    [ReturnFields]        VARCHAR (500) NULL,
    [DateCreated]         DATETIME      NULL,
    [DateUpdated]         DATETIME      NULL,
    CONSTRAINT [PK_dbo.dgDataMap] PRIMARY KEY CLUSTERED ([DataMapID] ASC) WITH (FILLFACTOR = 90)
);

