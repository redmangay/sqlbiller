﻿CREATE TABLE [dbo].[RegularTaskLog] (
    [RegularTaskLogID] INT           IDENTITY (1, 1) NOT NULL,
    [RegularTaskID]    INT           NOT NULL,
    [StartTime]        DATETIME      NOT NULL,
    [EndTime]          DATETIME      NOT NULL,
    [ResultMessage]    VARCHAR (MAX) NULL,
    CONSTRAINT [PK_RegularTaskLog] PRIMARY KEY CLUSTERED ([RegularTaskLogID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_RegularTaskLog_RegularTask] FOREIGN KEY ([RegularTaskID]) REFERENCES [dbo].[RegularTask] ([RegularTaskID])
);

