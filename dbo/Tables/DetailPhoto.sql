﻿CREATE TABLE [dbo].[DetailPhoto] (
    [DetailPhotoID] INT           IDENTITY (1, 1) NOT NULL,
    [Filename]      VARCHAR (100) NULL,
    [TableID]       INT           NOT NULL,
    [Size]          INT           NULL,
    [IsShow]        VARCHAR (1)   NULL,
    [ColumnID]      INT           NULL
);

