﻿CREATE TABLE [dbo].[ReportRecipient] (
    [ReportRecipientID] INT           IDENTITY (1, 1) NOT NULL,
    [ReportID]          INT           NOT NULL,
    [Email]             VARCHAR (256) NOT NULL,
    CONSTRAINT [PK_ReportRecipient] PRIMARY KEY CLUSTERED ([ReportRecipientID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ReportRecipient_Report] FOREIGN KEY ([ReportID]) REFERENCES [dbo].[Report] ([ReportID]) ON DELETE CASCADE
);

