﻿CREATE TABLE [dbo].[SearchGroupColumn] (
    [SearchGroupColumnID] INT IDENTITY (1, 1) NOT NULL,
    [SearchGroupID]       INT NOT NULL,
    [ColumnID]            INT NOT NULL,
    CONSTRAINT [pk_SearchGroupColumn_SearchGroupColumnID] PRIMARY KEY CLUSTERED ([SearchGroupColumnID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [fk_SearchGroupColumn_SearchGroup_SearchGroupID_SearchGroupID] FOREIGN KEY ([SearchGroupID]) REFERENCES [dbo].[SearchGroup] ([SearchGroupID])
);

