﻿CREATE TABLE [dbo].[File] (
    [FileID]          INT            IDENTITY (1, 1) NOT NULL,
    [FileTitle]       NVARCHAR (255) NULL,
    [FileType]        NVARCHAR (50)  NULL,
    [FileName]        NVARCHAR (255) NULL,
    [UniqueName]      NVARCHAR (50)  NULL,
    [DateAdded]       DATETIME       CONSTRAINT [DF_emdDocument_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]     DATETIME       CONSTRAINT [DF_emdDocument_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [AccountID]       INT            NOT NULL,
    [IsTemp]          BIT            CONSTRAINT [DF_File_IsTemp] DEFAULT ((0)) NULL,
    [IsInComingEmail] BIT            NULL,
    CONSTRAINT [PK__File] PRIMARY KEY CLUSTERED ([FileID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_File_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
);

