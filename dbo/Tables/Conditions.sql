﻿CREATE TABLE [dbo].[Conditions] (
    [ConditionsID]          INT            IDENTITY (1, 1) NOT NULL,
    [ColumnID]              INT            NULL,
    [ConditionColumnID]     INT            NULL,
    [ConditionValue]        NVARCHAR (250) NULL,
    [ConditionOperator]     VARCHAR (25)   NULL,
    [DisplayOrder]          INT            NOT NULL,
    [JoinOperator]          VARCHAR (10)   NULL,
    [Context]               VARCHAR (25)   NULL,
    [DocumentSectionID]     INT            NULL,
    [TableTabID]            INT            NULL,
    [SpecialNotificationID] INT            NULL,
    CONSTRAINT [pk_ShowWhen_ShowWhenID] PRIMARY KEY CLUSTERED ([ConditionsID] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([SpecialNotificationID]) REFERENCES [dbo].[SpecialNotification] ([SpecialNotificationID]),
    CONSTRAINT [fk_Column_ColumnID_ColumnID] FOREIGN KEY ([ColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [fk_Column_HideColumnID_ColumnID] FOREIGN KEY ([ConditionColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_ShowWhen_DocumentSection_DocumentSectionID] FOREIGN KEY ([DocumentSectionID]) REFERENCES [dbo].[DocumentSection] ([DocumentSectionID]),
    CONSTRAINT [FK_ShowWhen_TableTabID] FOREIGN KEY ([TableTabID]) REFERENCES [dbo].[TableTab] ([TableTabID])
);


GO
ALTER TABLE [dbo].[Conditions] NOCHECK CONSTRAINT [fk_Column_HideColumnID_ColumnID];

