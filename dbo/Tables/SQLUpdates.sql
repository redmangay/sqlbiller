﻿CREATE TABLE [dbo].[SQLUpdates] (
    [Name]        VARCHAR (MAX) NULL,
    [DateAdded]   DATETIME      DEFAULT (getdate()) NULL,
    [Description] VARCHAR (MAX) NULL,
    [Sequence]    INT           NOT NULL
);

