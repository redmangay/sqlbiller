﻿CREATE TABLE [dbo].[TrelloBoard] (
    [TrelloBoardID]    INT            IDENTITY (1, 1) NOT NULL,
    [RefId]            VARCHAR (50)   NOT NULL,
    [Name]             NVARCHAR (200) NOT NULL,
    [ShortLink]        VARCHAR (100)  NOT NULL,
    [DateLastActivity] DATETIME       NULL,
    [CompanyID]        INT            NOT NULL,
    CONSTRAINT [PK_TrelloBoard] PRIMARY KEY CLUSTERED ([TrelloBoardID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_TrelloBoard]
    ON [dbo].[TrelloBoard]([CompanyID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_TrelloBoard_RefId]
    ON [dbo].[TrelloBoard]([RefId] ASC) WITH (FILLFACTOR = 90);

