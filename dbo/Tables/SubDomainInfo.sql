﻿CREATE TABLE [dbo].[SubDomainInfo] (
    [SubDomainInfoID] INT           IDENTITY (1, 1) NOT NULL,
    [RootURL]         VARCHAR (250) NOT NULL,
    [LogoFileName]    VARCHAR (250) NOT NULL,
    [Footer]          VARCHAR (MAX) NULL,
    [Notes]           VARCHAR (MAX) NULL
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SubDomainInfo]
    ON [dbo].[SubDomainInfo]([RootURL] ASC) WITH (FILLFACTOR = 90);

