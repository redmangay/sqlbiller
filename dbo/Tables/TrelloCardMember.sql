﻿CREATE TABLE [dbo].[TrelloCardMember] (
    [TrelloCardMemberID] INT IDENTITY (1, 1) NOT NULL,
    [TrelloCardID]       INT NOT NULL,
    [TrelloMemberID]     INT NOT NULL,
    CONSTRAINT [PK_TrelloCardMember] PRIMARY KEY CLUSTERED ([TrelloCardMemberID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_TrelloCardMember_TrelloCard] FOREIGN KEY ([TrelloCardID]) REFERENCES [dbo].[TrelloCard] ([TrelloCardID]) ON DELETE CASCADE,
    CONSTRAINT [FK_TrelloCardMember_TrelloMember] FOREIGN KEY ([TrelloMemberID]) REFERENCES [dbo].[TrelloMember] ([TrelloMemberID]) ON DELETE CASCADE
);

