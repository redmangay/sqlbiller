﻿CREATE TABLE [dbo].[Table] (
    [TableID]                         INT              IDENTITY (1, 1) NOT NULL,
    [TableName]                       NVARCHAR (255)   NOT NULL,
    [MenuID_NO]                       INT              NULL,
    [DateAdded]                       DATETIME         CONSTRAINT [DF_SampleType_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]                     DATETIME         CONSTRAINT [DF_SampleType_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [IsImportPositional_DELETED]      BIT              NULL,
    [IsActive]                        BIT              CONSTRAINT [DF_SampleType_IsActive] DEFAULT ((1)) NOT NULL,
    [AccountID]                       INT              NOT NULL,
    [PinImage]                        VARCHAR (255)    NULL,
    [MaxTimeBetweenRecords]           DECIMAL (20, 10) NULL,
    [MaxTimeBetweenRecordsUnit]       VARCHAR (50)     NULL,
    [LastUpdatedUserID]               INT              NULL,
    [LateDataDays]                    INT              NULL,
    [ImportDataStartRow_DELETED]      INT              NULL,
    [DateFormat]                      VARCHAR (30)     CONSTRAINT [DF_SampleType_DateFormat] DEFAULT ('DD/MM/YYYY') NOT NULL,
    [DisplayOrder]                    INT              NULL,
    [IsRecordDateUnique_DELETED]      BIT              CONSTRAINT [DF_SampleType_IsSampleDateUnique] DEFAULT ((0)) NULL,
    [FlashAlerts]                     BIT              NULL,
    [FilterColumnID]                  INT              NULL,
    [FilterDefaultValue]              VARCHAR (50)     NULL,
    [ReasonChangeType]                VARCHAR (20)     NULL,
    [ChangeHistoryType]               VARCHAR (20)     NULL,
    [AddWithoutLogin]                 BIT              NULL,
    [ParentTableID]                   INT              NULL,
    [AddUserRecord]                   BIT              NULL,
    [AddUserUserColumnID]             INT              NULL,
    [AddUserPasswordColumnID]         INT              NULL,
    [AddUserNotification]             BIT              NULL,
    [ImportColumnHeaderRow_DELETED]   INT              NULL,
    [SortColumnID]                    INT              NULL,
    [HeaderName]                      VARCHAR (MAX)    NULL,
    [HideAdvancedOption]              BIT              NULL,
    [ValidateColumnID1]               INT              NULL,
    [ValidateColumnID2]               INT              NULL,
    [HeaderColor]                     VARCHAR (6)      NULL,
    [JSONAttachmentPOP3]              VARCHAR (MAX)    NULL,
    [JSONAttachmentInfo]              VARCHAR (MAX)    NULL,
    [ShowTabVertically]               BIT              NULL,
    [CopyToChildrenAfterImport]       BIT              NULL,
    [CustomUploadSheet]               VARCHAR (MAX)    NULL,
    [FilterType]                      VARCHAR (25)     NULL,
    [TabColour]                       VARCHAR (6)      NULL,
    [TabTextColour]                   VARCHAR (6)      NULL,
    [BoxAroundField]                  BIT              NULL,
    [FilterTopColour]                 VARCHAR (6)      NULL,
    [FilterBottomColour]              VARCHAR (6)      NULL,
    [ShowEditAfterAdd]                BIT              NULL,
    [AddOpensForm]                    BIT              NULL,
    [AddRecordSP]                     VARCHAR (MAX)    NULL,
    [SPSaveRecord]                    VARCHAR (200)    NULL,
    [HideFilter]                      BIT              NULL,
    [SaveAndAdd]                      BIT              NULL,
    [NavigationArrows]                BIT              DEFAULT ((1)) NOT NULL,
    [GraphXAxisColumnID]              INT              NULL,
    [GraphSeriesColumnID]             INT              NULL,
    [GraphDefaultPeriod]              INT              NULL,
    [DataUpdateUniqueColumnID]        INT              NULL,
    [AllowCopyRecords]                BIT              CONSTRAINT [DF_Table_AllowCopyRecords] DEFAULT ((1)) NULL,
    [SPSendEmail]                     VARCHAR (200)    NULL,
    [SPUpdateConfirm]                 VARCHAR (256)    NULL,
    [ShowSentEmails]                  BIT              NULL,
    [ShowReceivedEmails]              BIT              NULL,
    [UniqueColumnID]                  INT              NULL,
    [UniqueColumnID2]                 INT              NULL,
    [SPAfterImport_DELETED]           VARCHAR (100)    NULL,
    [PinDisplayOrder]                 INT              NULL,
    [GraphOnStart]                    VARCHAR (25)     NULL,
    [GraphDefaultYAxisColumnID]       INT              NULL,
    [SummaryPageContent]              VARCHAR (MAX)    NULL,
    [IsDataUpdateAllowed]             BIT              NULL,
    [DefaultImportTemplateID]         INT              NULL,
    [DuplicateRecordAction]           VARCHAR (50)     NULL,
    [SPSearchGo]                      VARCHAR (127)    NULL,
    [SPSearchReset]                   VARCHAR (127)    NULL,
    [ShowChildTabsOnAdd]              BIT              NULL,
    [WarningMinPeriod]                DECIMAL (10, 2)  NULL,
    [ExceedanceMinPeriod]             DECIMAL (10, 2)  NULL,
    [FlatDataMinPeriod]               DECIMAL (10, 2)  NULL,
    [DefaultExportTemplateID]         INT              NULL,
    [LastEmailWarning]                DATETIME         NULL,
    [LastEmailExceedance]             DATETIME         NULL,
    [LastEmailFlatData]               DATETIME         NULL,
    [MapCircleDiameters]              VARCHAR (100)    NULL,
    [ShowCircleOnStart]               BIT              NULL,
    [DeleteAction]                    VARCHAR (100)    NULL,
    [CustomPreventDeleteNotification] VARCHAR (MAX)    NULL,
    [LateDataUnit]                    VARCHAR (50)     NULL,
    [CopyDataWithTemplate]            BIT              NULL,
    [PreventDeleteFilter]             VARCHAR (MAX)    NULL,
    [PreventDeleteFilterInfo]         VARCHAR (MAX)    NULL,
    [ArchiveMonths]                   INT              NULL,
    [ArchiveDatabase]                 VARCHAR (MAX)    NULL,
    [ArchiveLastRun]                  DATETIME         NULL,
    [ArchiveDateColumnID]             INT              NULL,
    [GraphMissingValues]              VARCHAR (10)     NULL,
    [GraphSpecialCharacters]          VARCHAR (10)     NULL,
    [CopyfilterSQL]                   VARCHAR (MAX)    NULL,
    [PinImageAdvanced]                VARCHAR (MAX)    NULL,
    [SystemName]                      VARCHAR (255)    NULL,
    [DefaultGraphDefinitionID]        INT              NULL,
    [DefaultTableGraphOptionID]       INT              NULL,
    [SPBeforeDeleteRecord]            VARCHAR (300)    NULL,
    CONSTRAINT [PK_SampleType] PRIMARY KEY CLUSTERED ([TableID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_SampleType_SampleColumn] FOREIGN KEY ([FilterColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Table_ArchiveDateColumnID] FOREIGN KEY ([ArchiveDateColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Table_Column] FOREIGN KEY ([AddUserUserColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Table_Column_DataUpdateUniqueColumnID] FOREIGN KEY ([DataUpdateUniqueColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Table_Column_GraphDefaultYAxisColumnID] FOREIGN KEY ([GraphDefaultYAxisColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Table_Column_UniqueColumnID] FOREIGN KEY ([UniqueColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Table_Column_UniqueColumnID2] FOREIGN KEY ([UniqueColumnID2]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Table_Column1] FOREIGN KEY ([AddUserPasswordColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Table_Column2] FOREIGN KEY ([SortColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Table_DefaultGraphDefinitionID] FOREIGN KEY ([DefaultGraphDefinitionID]) REFERENCES [dbo].[GraphDefinition] ([GraphDefinitionID]),
    CONSTRAINT [FK_Table_DefaultTableGraphOptionID] FOREIGN KEY ([DefaultTableGraphOptionID]) REFERENCES [dbo].[GraphOption] ([GraphOptionID]),
    CONSTRAINT [FK_Table_GraphSeriesColumnID] FOREIGN KEY ([GraphSeriesColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Table_GraphXAixsColumnID] FOREIGN KEY ([GraphXAxisColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Table_ImportTemplate_DefaultImportTemplateID] FOREIGN KEY ([DefaultImportTemplateID]) REFERENCES [dbo].[ImportTemplate] ([ImportTemplateID]) ON DELETE SET NULL,
    CONSTRAINT [FK_Table_Table] FOREIGN KEY ([ParentTableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [fk_Table_ValidateColumnID1] FOREIGN KEY ([ValidateColumnID1]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [fk_Table_ValidateColumnID2] FOREIGN KEY ([ValidateColumnID2]) REFERENCES [dbo].[Column] ([ColumnID])
);


GO
CREATE TRIGGER [dbo].[TR_Table_DateUpdated] ON [dbo].[Table] AFTER UPDATE AS UPDATE [Table]
 SET DateUpdated = GETDATE() FROM inserted WHERE ([Table].TableID = inserted.TableID)

GO

CREATE TRIGGER [dbo].[TR_Table_Delete_Audit]  ON [dbo].[Table] AFTER DELETE 
AS
BEGIN
	DECLARE @sTableName VARCHAR(1000)=NULL
	DECLARE @nTableID INT
	DECLARE @nUserID INT
	DECLARE @nAccountID INT

	SELECT  
		@nTableID=TableID,
		@sTableName=TableName, 
		@nUserID = LastUpdatedUserID,
		@nAccountID = AccountID
	FROM DELETED

	INSERT Audit (TableName,
			  AccountID,
			  PrimaryKeyValue, 
			  FieldName, 
	   		  OldValue, 
			  NewValue, 
			  DateAdded, 
			  UserID,
			  TableID)
	VALUES ('Table',
			 @nAccountID,
			 @nTableID,
			 'Deleted Table ' + @sTableName,
			 NULL,
			 NULL,
			 getdate(),
			 @nUserID,
			 @nTableID)
END

GO

CREATE TRIGGER [dbo].[TR_Table_Audit] on [dbo].[Table] for insert, update --, delete
AS
BEGIN	  
	    DECLARE @bit int,
        @field int ,
        @maxfield int ,
        @char int ,
        @fieldname varchar(128) ,
        @TableName varchar(128) ,
        @PKCols varchar(1000) ,
        @sql varchar(2000),
        @UpdateDate varchar(21) ,
        @UserName varchar(128) ,
        @Type char(1) ,
        @PKFieldSelect varchar(1000),
        @PKValueSelect varchar(1000),
        @nPKID int

        SELECT @TableName = 'Table'

        -- date and user
        SELECT @UserName = NULL ,
                @UpdateDate = convert(varchar(8), getdate(), 112) + ' ' + convert(varchar(12), getdate(), 114)
	
        -- Action
        IF EXISTS (SELECT * FROM inserted)

        BEGIN

            SELECT top 1 @UserName = LastUpdatedUserID FROM inserted
			SELECT top 1 @nPKID = TableID FROM inserted
                IF EXISTS (SELECT * FROM deleted)
                    SELECT @Type = 'U'
                ELSE
                    SELECT @Type = 'I'
        END
                ELSE
                    SELECT @Type = 'D'
		
        -- get list of columns
        SELECT * INTO #ins FROM inserted
        SELECT * INTO #del FROM deleted


        -- Get primary key columns for full outer join

        SELECT @PKCols = coalesce(@PKCols + ' and', ' on') + ' i.' + c.COLUMN_NAME + ' = d.' + c.COLUMN_NAME

                FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,

                INFORMATION_SCHEMA.KEY_COLUMN_USAGE c

                WHERE pk.TABLE_NAME = @TableName

                AND CONSTRAINT_TYPE = 'PRIMARY KEY'

                AND c.TABLE_NAME = pk.TABLE_NAME

                AND c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME

 
        -- Get primary key fields select for insert

        SELECT @PKFieldSelect = coalesce(@PKFieldSelect+'+','') + '''' + COLUMN_NAME + ''''

                FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,

                INFORMATION_SCHEMA.KEY_COLUMN_USAGE c

                WHERE pk.TABLE_NAME = @TableName

                AND CONSTRAINT_TYPE = 'PRIMARY KEY'

                AND c.TABLE_NAME = pk.TABLE_NAME

                AND c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME


			
        SELECT @PKValueSelect = coalesce(@PKValueSelect+'+','') + 'convert(varchar(100), coalesce(i.' + COLUMN_NAME + ',d.' + COLUMN_NAME + '))'

                FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,  

                INFORMATION_SCHEMA.KEY_COLUMN_USAGE c 

                WHERE  pk.TABLE_NAME = @TableName 

                AND CONSTRAINT_TYPE = 'PRIMARY KEY' 

                AND c.TABLE_NAME = pk.TABLE_NAME 

                AND c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME

 

        IF @PKCols IS NULL

        BEGIN

                raiserror('no PK on table %s', 16, -1, @TableName)

                RETURN

        END

        SELECT @field = 0, @maxfield = max(ORDINAL_POSITION) from INFORMATION_SCHEMA.COLUMNS 
        where TABLE_NAME = @TableName
			
        WHILE @field < @maxfield

        BEGIN

                SELECT @field = min(ORDINAL_POSITION) from INFORMATION_SCHEMA.COLUMNS 
                where TABLE_NAME = @TableName and ORDINAL_POSITION > @field

                SELECT @bit = (@field - 1 )% 8 + 1

                SELECT @bit = power(2,@bit - 1)

                SELECT @char = ((@field - 1) / 8) + 1

                SELECT @fieldname = COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where 
                TABLE_NAME = @TableName and ORDINAL_POSITION = @field
				AND TABLE_SCHEMA='dbo'

				--print @fieldname
				--print @field
                IF substring(COLUMNS_UPDATED(),1, 1)>0  or @Type in ('I','D')

                    AND @fieldname LIKE 'DateUpdated'

                    AND @fieldname LIKE 'LastUpdatedUserID'

                BEGIN

                    -- SELECT @sql = 'insert Audit (Type, TableName, PrimaryKeyField, PrimaryKeyValue, FieldName, OldValue, NewValue, UpdateDate, UserName)'
					DECLARE @nAccountID int
						
					SELECT @nAccountID=AccountID FROM  [Table] WHERE [Table].TableID=@nPKID 

					IF @fieldname<>'MenuID' --AND EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Table' AND COLUMN_NAME = @fieldname)
					BEGIN
						
						SELECT @sql = 'insert Audit (TableName,AccountID,PrimaryKeyValue, FieldName, 
						OldValue, NewValue, DateAdded, UserID, TableID)'								
												
						SELECT @sql = @sql + ' select ''' + @TableName + ''''
						SELECT @sql = @sql + ',' + cast( @nAccountID as varchar) 
						--SELECT @sql = @sql + ',' + @PKFieldSelect

						SELECT @sql = @sql + ',' + @PKValueSelect

						SELECT @sql = @sql + ',''' + @fieldname + ''''

						SELECT @sql = @sql + ',convert(varchar(1000),d.' + @fieldname + ')'

						SELECT @sql = @sql + ',convert(varchar(1000),i.' + @fieldname + ')'

						SELECT @sql = @sql + ',''' + @UpdateDate + ''''

						SELECT @sql = @sql + ',' + @UserName
							
						SELECT @sql = @sql + ',' + @PKValueSelect

						SELECT @sql = @sql + ' from #ins i full outer join #del d'

						SELECT @sql = @sql + @PKCols

						SELECT @sql = @sql + ' where i.' + @fieldname + ' <> d.' + @fieldname

						SELECT @sql = @sql + ' or (i.' + @fieldname + ' is null and  d.' + @fieldname + ' is not null)'

						SELECT @sql = @sql + ' or (i.' + @fieldname + ' is not null and  d.' + @fieldname + ' is null)'

				
						EXEC (@sql)
                    END
                      

                END

        END
END

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The stored procedure to run after adding a record. Parameters are: TBC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Table', @level2type = N'COLUMN', @level2name = N'AddRecordSP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The stored procedure to run after saving a record (Add or Update). Parameters are:
	@RecordID int,
	@UserID int,
	@Return varchar(max) output
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Table', @level2type = N'COLUMN', @level2name = N'SPSaveRecord';

