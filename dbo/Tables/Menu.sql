﻿CREATE TABLE [dbo].[Menu] (
    [MenuID]             INT             IDENTITY (1, 1) NOT NULL,
    [Menu]               NVARCHAR (255)  NOT NULL,
    [AccountID]          INT             NOT NULL,
    [ShowOnMenu]         BIT             CONSTRAINT [DF_SampleTypeGroup_ShowOnMenu] DEFAULT ((1)) NOT NULL,
    [IsActive]           BIT             CONSTRAINT [DF_SampleTypeGroup_IsActive] DEFAULT ((1)) NOT NULL,
    [DisplayOrder]       INT             NULL,
    [ParentMenuID]       INT             NULL,
    [TableID]            INT             NULL,
    [DocumentID]         INT             NULL,
    [ExternalPageLink]   NVARCHAR (1000) NULL,
    [OpenInNewWindow]    BIT             NULL,
    [DocumentTypeID]     INT             NULL,
    [MenuType]           VARCHAR (10)    NULL,
    [ReportID]           INT             NULL,
    [IsAdvancedSecurity] BIT             NULL,
    CONSTRAINT [PK_SampleTypeGroup] PRIMARY KEY CLUSTERED ([MenuID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CHK_Menu_NotSelfParent] CHECK ([MenuID]<>[ParentMenuID]),
    CONSTRAINT [fk_Menu_Document_DocumentID] FOREIGN KEY ([DocumentID]) REFERENCES [dbo].[Document] ([DocumentID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Menu_DocumentType_DocumentTypeID] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentType] ([DocumentTypeID]),
    CONSTRAINT [FK_Menu_Menu] FOREIGN KEY ([ParentMenuID]) REFERENCES [dbo].[Menu] ([MenuID]),
    CONSTRAINT [FK_Menu_Report] FOREIGN KEY ([ReportID]) REFERENCES [dbo].[Report] ([ReportID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Menu_Table] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [FK_SampleTypeGroup_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_Menu_Name_Parent_AccountID]
    ON [dbo].[Menu]([Menu] ASC, [ParentMenuID] ASC, [AccountID] ASC) WHERE ([IsActive]=(1) AND [Menu]<>'---') WITH (FILLFACTOR = 90);

