﻿CREATE TABLE [dbo].[UserRole] (
    [UserRoleID]            INT            IDENTITY (1, 1) NOT NULL,
    [UserID]                INT            NOT NULL,
    [RoleID]                INT            NOT NULL,
    [DateAdded]             DATETIME       CONSTRAINT [DF_UserRole_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]           DATETIME       CONSTRAINT [DF_UserRole_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [AccountID]             INT            NULL,
    [IsPrimaryAccount]      BIT            NULL,
    [IsAccountHolder]       BIT            CONSTRAINT [DF_UserRole_IsAccountHolder] DEFAULT ((0)) NOT NULL,
    [IsAdvancedSecurity]    BIT            CONSTRAINT [DF_UserRole_IsAdvancedSecurity] DEFAULT ((0)) NOT NULL,
    [SessionID]             VARCHAR (100)  NULL,
    [AlertSeen]             DATETIME       NULL,
    [IsDocSecurityAdvanced] BIT            NULL,
    [DocSecurityType]       VARCHAR (20)   NULL,
    [DashBoardDocumentID]   INT            NULL,
    [DataScopeTableID]      INT            NULL,
    [DataScopeColumnID]     INT            NULL,
    [DataScopeValue]        NVARCHAR (500) NULL,
    [AllowDeleteTable]      BIT            NULL,
    [AllowDeleteColumn]     BIT            NULL,
    [AllowDeleteRecord]     BIT            NULL,
    CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED ([UserRoleID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_UserRole_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_UserRole_Column_DataScopeColumnID] FOREIGN KEY ([DataScopeColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_UserRole_Document_DashBoardDocumentID] FOREIGN KEY ([DashBoardDocumentID]) REFERENCES [dbo].[Document] ([DocumentID]),
    CONSTRAINT [FK_UserRole_Table_DataScopeTableID] FOREIGN KEY ([DataScopeTableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [UserRole_Role] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role] ([RoleID]),
    CONSTRAINT [UserRole_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID]) ON DELETE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_UserIDAccountID]
    ON [dbo].[UserRole]([UserID] ASC, [AccountID] ASC) WITH (FILLFACTOR = 90);


GO
Create TRIGGER [dbo].[UserRoleUpdated] ON [dbo].[UserRole] AFTER UPDATE AS UPDATE UserRole SET DateUpdated = GETDATE() FROM inserted WHERE (UserRole.RoleID = inserted.RoleID)