﻿CREATE TABLE [dbo].[TestTable] (
    [SampleTypeGroupID] INT            IDENTITY (1, 1) NOT NULL,
    [SampleTypeGroup]   NVARCHAR (255) NOT NULL,
    [AccountID]         INT            NULL,
    [ShowOnMenu]        BIT            CONSTRAINT [DF_TestTable_ShowOnMenu] DEFAULT ((1)) NOT NULL,
    [SampleDate]        DATE           NULL,
    [SampleTime]        TIME (7)       NULL,
    CONSTRAINT [PK_TestTable] PRIMARY KEY CLUSTERED ([SampleTypeGroupID] ASC) WITH (FILLFACTOR = 90)
);

