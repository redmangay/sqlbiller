﻿CREATE TABLE [dbo].[Content] (
    [ContentID]       INT            IDENTITY (1, 1) NOT NULL,
    [ContentKey]      NVARCHAR (50)  NOT NULL,
    [Heading]         NVARCHAR (500) NULL,
    [Content]         NVARCHAR (MAX) NOT NULL,
    [StoredProcedure] NVARCHAR (200) NULL,
    [DateAdded]       DATETIME       CONSTRAINT [DF_Content_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]     DATETIME       CONSTRAINT [DF_Content_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [AccountID]       INT            NULL,
    [MainContentID]   INT            NULL,
    [ForAllAccount]   BIT            CONSTRAINT [DF_Content_ForAllAccount] DEFAULT ((0)) NOT NULL,
    [ContentTypeID]   INT            NULL,
    CONSTRAINT [PK_Content] PRIMARY KEY CLUSTERED ([ContentID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Content_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]) ON DELETE CASCADE,
    CONSTRAINT [FK_ContentType_ContentTypeID_ContentTypeID] FOREIGN KEY ([ContentTypeID]) REFERENCES [dbo].[ContentType] ([ContentTypeID]),
    CONSTRAINT [IX_Content_And_AccountID] UNIQUE NONCLUSTERED ([ContentID] ASC, [AccountID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_ContentTypeID]
    ON [dbo].[Content]([ContentTypeID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE TRIGGER [dbo].[tg_ContentUpdated] ON dbo.[Content] 
	AFTER UPDATE AS 
	UPDATE Content
		SET DateUpdated = GETDATE() FROM inserted WHERE (Content.ContentID = inserted.ContentID)
		
    INSERT INTO Content_Changes ([ContentID], [ContentKey], [Heading], [Content])
           SELECT [ContentID], [ContentKey], [Heading], [Content] 
                  FROM deleted