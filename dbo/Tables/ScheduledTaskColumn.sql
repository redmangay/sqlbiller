﻿CREATE TABLE [dbo].[ScheduledTaskColumn] (
    [ScheduledTaskColumnID] INT           IDENTITY (1, 1) NOT NULL,
    [ScheduledTaskID]       INT           NOT NULL,
    [ColumnID]              INT           NOT NULL,
    [URL]                   VARCHAR (500) NOT NULL,
    [DateTimeSampledNode]   VARCHAR (200) NOT NULL,
    [ValueNode]             VARCHAR (200) NOT NULL,
    CONSTRAINT [PK_ScheduledTaskColumn] PRIMARY KEY CLUSTERED ([ScheduledTaskColumnID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ScheduledTaskColumn_SampleColumn] FOREIGN KEY ([ColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_ScheduledTaskColumn_ScheduledTask] FOREIGN KEY ([ScheduledTaskID]) REFERENCES [dbo].[ScheduledTask] ([ScheduledTaskID]) ON DELETE CASCADE
);

