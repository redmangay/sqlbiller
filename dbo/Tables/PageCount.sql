﻿CREATE TABLE [dbo].[PageCount] (
    [PageCountID] INT            IDENTITY (1, 1) NOT NULL,
    [Date]        DATETIME       NOT NULL,
    [PageURL]     VARCHAR (1000) NOT NULL,
    [Count]       INT            NOT NULL,
    [DateAdded]   DATETIME       CONSTRAINT [DF_PageCount_DateAdded] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PageCount] PRIMARY KEY CLUSTERED ([PageCountID] ASC) WITH (FILLFACTOR = 90)
);

