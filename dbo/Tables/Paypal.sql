﻿CREATE TABLE [dbo].[Paypal] (
    [PaypalID]       INT             IDENTITY (1, 1) NOT NULL,
    [txn_id]         VARCHAR (50)    NOT NULL,
    [payment_status] VARCHAR (50)    NOT NULL,
    [pending_reason] VARCHAR (MAX)   NULL,
    [payer_email]    VARCHAR (250)   NOT NULL,
    [receiver_email] VARCHAR (250)   NOT NULL,
    [mc_gross]       DECIMAL (18, 5) NOT NULL,
    [DateAdded]      DATETIME        CONSTRAINT [DF_Paypal_DateAdded] DEFAULT (getdate()) NOT NULL,
    [txn_type]       VARCHAR (50)    NULL,
    CONSTRAINT [PK_Paypal] PRIMARY KEY CLUSTERED ([PaypalID] ASC) WITH (FILLFACTOR = 90)
);

