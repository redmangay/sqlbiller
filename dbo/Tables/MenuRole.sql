﻿CREATE TABLE [dbo].[MenuRole] (
    [MenuRoleID] INT IDENTITY (1, 1) NOT NULL,
    [MenuID]     INT NULL,
    [RoleID]     INT NULL,
    PRIMARY KEY CLUSTERED ([MenuRoleID] ASC),
    FOREIGN KEY ([MenuID]) REFERENCES [dbo].[Menu] ([MenuID]),
    FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role] ([RoleID])
);

