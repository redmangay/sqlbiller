﻿CREATE TABLE [dbo].[SpeedLog] (
    [SpeedLogID]         INT           IDENTITY (1, 1) NOT NULL,
    [UserID]             INT           NULL,
    [PageUrl]            VARCHAR (MAX) NULL,
    [FunctionName]       VARCHAR (MAX) NULL,
    [FunctionLineNumber] INT           NULL,
    [Runtime]            DATETIME      NOT NULL,
    [SessionID]          VARCHAR (MAX) NULL,
    CONSTRAINT [PK_SpeedLog_SpeedLogID] PRIMARY KEY CLUSTERED ([SpeedLogID] ASC) WITH (FILLFACTOR = 90)
);

