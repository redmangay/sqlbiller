﻿CREATE TABLE [dbo].[EMD_System_Column_Lookup] (
    [EMD_System_Column_LookupID]           INT           IDENTITY (1, 1) NOT NULL,
    [TableID]                              INT           NULL,
    [Record ID]                            INT           NULL,
    [Account Name]                         VARCHAR (50)  NULL,
    [Table Name]                           VARCHAR (200) NULL,
    [ColumnID]                             INT           NULL,
    [Column Display Name (Auto Populated)] VARCHAR (200) NULL,
    [Column System Name (Auto Populated)]  VARCHAR (200) NULL,
    [NameOnImport (Auto Populated)]        VARCHAR (200) NULL,
    [TableID (Auto Populated)]             VARCHAR (200) NULL,
    [DateAdded]                            DATETIME      NULL,
    CONSTRAINT [PK_EMD_System_Column_Lookup] PRIMARY KEY CLUSTERED ([EMD_System_Column_LookupID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_TableID]
    ON [dbo].[EMD_System_Column_Lookup]([TableID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_RecordID]
    ON [dbo].[EMD_System_Column_Lookup]([Record ID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_NameOnImport]
    ON [dbo].[EMD_System_Column_Lookup]([NameOnImport (Auto Populated)] ASC) WITH (FILLFACTOR = 90);

