﻿CREATE TABLE [dbo].[WorkFlowSection] (
    [WorkFlowSectionID]     INT           IDENTITY (1, 1) NOT NULL,
    [WorkFlowID]            INT           NOT NULL,
    [WorkFlowSectionTypeID] INT           NOT NULL,
    [SectionName]           VARCHAR (100) NULL,
    [Position]              INT           NOT NULL,
    [JSONContent]           VARCHAR (MAX) NULL,
    [ParentSectionID]       INT           NULL,
    [ColumnIndex]           INT           NULL,
    CONSTRAINT [PK_WorkFlowSection] PRIMARY KEY CLUSTERED ([WorkFlowSectionID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_WorkFlowSection_WorkFlow] FOREIGN KEY ([WorkFlowID]) REFERENCES [dbo].[WorkFlow] ([WorkFlowID]),
    CONSTRAINT [FK_WorkFlowSection_WorkFlowSectionType] FOREIGN KEY ([WorkFlowSectionTypeID]) REFERENCES [dbo].[WorkFlowSectionType] ([WorkFlowSectionTypeID])
);

