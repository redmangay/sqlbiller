﻿CREATE TABLE [dbo].[TrelloCard] (
    [TrelloCardID]     INT            IDENTITY (1, 1) NOT NULL,
    [RefId]            VARCHAR (50)   NOT NULL,
    [TrelloBoardID]    INT            NOT NULL,
    [Name]             NVARCHAR (200) NOT NULL,
    [Description]      NVARCHAR (MAX) NULL,
    [ShortLink]        VARCHAR (50)   NOT NULL,
    [Due]              DATETIME       NULL,
    [DueComplete]      BIT            NULL,
    [Closed]           BIT            NULL,
    [DateLastActivity] DATETIME       NOT NULL,
    [MemberInitials]   VARCHAR (200)  NULL,
    CONSTRAINT [PK_Card] PRIMARY KEY CLUSTERED ([TrelloCardID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_TrelloCard_TrelloBoard] FOREIGN KEY ([TrelloBoardID]) REFERENCES [dbo].[TrelloBoard] ([TrelloBoardID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_TrelloCard_RefId]
    ON [dbo].[TrelloCard]([RefId] ASC) WITH (FILLFACTOR = 90);

