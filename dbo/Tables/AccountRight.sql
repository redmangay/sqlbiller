﻿CREATE TABLE [dbo].[AccountRight] (
    [AccountRightID] INT           IDENTITY (1, 1) NOT NULL,
    [AccountID]      INT           NOT NULL,
    [RightId]        INT           NOT NULL,
    [Resource]       NVARCHAR (50) NULL,
    [DateAdded]      DATETIME      CONSTRAINT [DF_AccountRight_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]    DATETIME      CONSTRAINT [DF_AccountRight_DateUpdated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK__AccountRight] PRIMARY KEY CLUSTERED ([AccountRightID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK__AccountRight_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_AccountRight_Right] FOREIGN KEY ([RightId]) REFERENCES [dbo].[Right] ([RightId])
);


GO
CREATE NONCLUSTERED INDEX [IX_RightId]
    ON [dbo].[AccountRight]([RightId] ASC) WITH (FILLFACTOR = 90);


GO
Create TRIGGER [dbo].[tg_AccountRightUpdated] ON [dbo].AccountRight AFTER UPDATE AS UPDATE AccountRight
 SET DateUpdated = GETDATE() FROM inserted WHERE (AccountRight.AccountRightID = inserted.AccountRightID)