﻿CREATE TABLE [dbo].[xDelete_tempMeanMinMax] (
    [ID]               INT             NULL,
    [LocationID]       INT             NULL,
    [DateTimeRecorded] DATETIME        NULL,
    [Value]            DECIMAL (18, 8) NULL,
    [Percent]          DECIMAL (18, 4) NULL
);


GO
CREATE NONCLUSTERED INDEX [id]
    ON [dbo].[xDelete_tempMeanMinMax]([ID] ASC) WITH (FILLFACTOR = 90);

