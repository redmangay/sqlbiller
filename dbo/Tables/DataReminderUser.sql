﻿CREATE TABLE [dbo].[DataReminderUser] (
    [DataReminderUserID] INT IDENTITY (1, 1) NOT NULL,
    [DataReminderID]     INT NOT NULL,
    [UserID]             INT NULL,
    [ReminderColumnID]   INT NULL,
    CONSTRAINT [PK_DataReminderUser] PRIMARY KEY CLUSTERED ([DataReminderUserID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_DataReminderUser_Column] FOREIGN KEY ([ReminderColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_DataReminderUser_DataReminder] FOREIGN KEY ([DataReminderID]) REFERENCES [dbo].[DataReminder] ([DataReminderID]),
    CONSTRAINT [FK_DataReminderUser_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])
);


GO
CREATE NONCLUSTERED INDEX [IX_DataReminderID]
    ON [dbo].[DataReminderUser]([DataReminderID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_ReminderColumnID]
    ON [dbo].[DataReminderUser]([ReminderColumnID] ASC) WITH (FILLFACTOR = 90);

