﻿CREATE TABLE [dbo].[ALS_Mapping] (
    [ALS_MappingID]         INT           IDENTITY (1, 1) NOT NULL,
    [TableID]               INT           NULL,
    [Record ID]             INT           NULL,
    [Incoming Site]         VARCHAR (50)  NULL,
    [Incoming Sample Type]  VARCHAR (200) NULL,
    [Incoming Analyte Name] VARCHAR (200) NULL,
    [Total Or Dissolved]    VARCHAR (50)  NULL,
    [Target EMD]            VARCHAR (50)  NULL,
    [Target Table]          VARCHAR (200) NULL,
    [Target Column]         VARCHAR (200) NULL,
    [Expected Units]        VARCHAR (50)  NULL,
    [Target Column ID]      INT           NULL,
    [DateAdded]             DATETIME      NULL,
    CONSTRAINT [PK_ALS_Mapping] PRIMARY KEY CLUSTERED ([ALS_MappingID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_TableID]
    ON [dbo].[ALS_Mapping]([TableID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_RecordID]
    ON [dbo].[ALS_Mapping]([Record ID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_IncomingSampleType]
    ON [dbo].[ALS_Mapping]([Incoming Sample Type] ASC) WITH (FILLFACTOR = 90);

