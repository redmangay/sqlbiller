﻿CREATE TABLE [dbo].[SpecialNotification] (
    [SpecialNotificationID] INT           IDENTITY (1, 1) NOT NULL,
    [NotificationHeader]    VARCHAR (MAX) NULL,
    [NotificationContent]   VARCHAR (MAX) NULL,
    [TableID]               INT           NULL,
    [Delivery]              VARCHAR (20)  NULL,
    PRIMARY KEY CLUSTERED ([SpecialNotificationID] ASC),
    FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID])
);

