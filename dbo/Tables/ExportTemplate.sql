﻿CREATE TABLE [dbo].[ExportTemplate] (
    [ExportTemplateID]   INT           IDENTITY (1, 1) NOT NULL,
    [TableID]            INT           NOT NULL,
    [ExportTemplateName] VARCHAR (250) NOT NULL,
    CONSTRAINT [pk_ExportTemplate_ExportTemplateID] PRIMARY KEY CLUSTERED ([ExportTemplateID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [fk_ExportTemplate_TableID] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID])
);

