﻿CREATE TABLE [dbo].[Role] (
    [RoleID]                     INT            IDENTITY (1, 1) NOT NULL,
    [Role]                       NVARCHAR (200) NOT NULL,
    [RoleType]                   CHAR (1)       NULL,
    [RoleNotes]                  NVARCHAR (150) NULL,
    [DateAdded]                  DATETIME       CONSTRAINT [DF_Role_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]                DATETIME       CONSTRAINT [DF_Role_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [AccountID]                  INT            NULL,
    [IsSystemRole]               BIT            NULL,
    [IsActive]                   BIT            NULL,
    [OwnerUserID]                INT            NULL,
    [AllowEditDashboard]         BIT            NULL,
    [DashboardDefaultFromUserID] INT            NULL,
    [ViewsDefaultFromUserID]     INT            NULL,
    [AllowEditView]              BIT            NULL,
    [DashboardType]              CHAR (1)       NULL,
    [DashboardTableID]           INT            NULL,
    [DashboardLink]              VARCHAR (MAX)  NULL,
    CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED ([RoleID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Role_Account_AccountID] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_Role_DashboardDefaultFromUserID] FOREIGN KEY ([DashboardDefaultFromUserID]) REFERENCES [dbo].[User] ([UserID]),
    CONSTRAINT [FK_Role_DashboardTableID] FOREIGN KEY ([DashboardTableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [FK_Role_User_OwnerUserID] FOREIGN KEY ([OwnerUserID]) REFERENCES [dbo].[User] ([UserID]),
    CONSTRAINT [FK_Role_ViewsDefaultFromUserID] FOREIGN KEY ([ViewsDefaultFromUserID]) REFERENCES [dbo].[User] ([UserID])
);


GO
Create TRIGGER [dbo].[tr_RoleUpdate] ON [dbo].[Role] AFTER UPDATE AS UPDATE [Role]
 SET DateUpdated = GETDATE() FROM inserted WHERE ([Role].RoleID = inserted.RoleID)