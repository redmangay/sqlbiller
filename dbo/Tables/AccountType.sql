﻿CREATE TABLE [dbo].[AccountType] (
    [AccountTypeID]     INT          IDENTITY (1, 1) NOT NULL,
    [AccountTypeName]   VARCHAR (50) NOT NULL,
    [MaxTotalRecords]   INT          NULL,
    [MaxUsers]          INT          NULL,
    [DiskSpaceMB]       INT          NULL,
    [CostPerMonth]      MONEY        NULL,
    [MaxEmailsPerMonth] INT          NULL,
    [MaxSMSPerMonth]    INT          NULL,
    CONSTRAINT [PK_AccountType] PRIMARY KEY CLUSTERED ([AccountTypeID] ASC) WITH (FILLFACTOR = 90)
);

