﻿CREATE TABLE [dbo].[DocumentType] (
    [DocumentTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [DocumentTypeName] VARCHAR (150) NOT NULL,
    [DateAdded]        DATETIME      CONSTRAINT [DF_DocumentType_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]      DATETIME      CONSTRAINT [DF_DocumentType_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [AccountID]        INT           NOT NULL,
    CONSTRAINT [PK_DocumentType] PRIMARY KEY CLUSTERED ([DocumentTypeID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_DocumentType_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
);


GO
Create TRIGGER [dbo].[tg_DocumentTypeUpdated] ON dbo.DocumentType AFTER UPDATE AS UPDATE DocumentType
 SET DateUpdated = GETDATE() FROM inserted WHERE (DocumentType.DocumentTypeID = inserted.DocumentTypeID)
