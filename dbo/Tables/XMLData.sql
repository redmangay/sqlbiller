﻿CREATE TABLE [dbo].[XMLData] (
    [XMLDataID]        INT           IDENTITY (1, 1) NOT NULL,
    [XMLText]          VARCHAR (MAX) NOT NULL,
    [SearchCriteriaID] INT           NULL,
    CONSTRAINT [PK_TXMLData_XMLDataid] PRIMARY KEY CLUSTERED ([XMLDataID] ASC) WITH (FILLFACTOR = 90)
);

