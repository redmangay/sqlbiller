﻿CREATE TABLE [dbo].[Payment] (
    [PaymentID]       INT            IDENTITY (1, 1) NOT NULL,
    [AccountID]       INT            NOT NULL,
    [AccountInviteID] INT            NULL,
    [Amount]          MONEY          NOT NULL,
    [PaymentDate]     DATETIME       NULL,
    [PaymentStatus]   VARCHAR (100)  NULL,
    [PaymentDetails]  NVARCHAR (MAX) NULL,
    [DateAdded]       DATETIME       DEFAULT (getdate()) NOT NULL,
    [DateUpdated]     DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PaymentID] PRIMARY KEY CLUSTERED ([PaymentID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Payment_Account_AccountID] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_Payment_AccountInvite_AccountInviteID] FOREIGN KEY ([AccountInviteID]) REFERENCES [dbo].[AccountInvite] ([AccountInviteID])
);


GO
CREATE TRIGGER [dbo].[tr_Payment_Updated] ON [dbo].[Payment] AFTER UPDATE AS 
	UPDATE Payment 
		SET DateUpdated = GETDATE() 
		FROM inserted 
		WHERE (Payment.PaymentID = inserted.PaymentID)