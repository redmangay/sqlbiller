﻿CREATE TABLE [dbo].[SpecialNotificationRecipient] (
    [RecipientID]           INT          IDENTITY (1, 1) NOT NULL,
    [SpecialNotificationID] INT          NULL,
    [UserID]                INT          NULL,
    [ColumnID]              INT          NULL,
    [RecipientOption]       VARCHAR (20) NULL,
    PRIMARY KEY CLUSTERED ([RecipientID] ASC),
    FOREIGN KEY ([ColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    FOREIGN KEY ([SpecialNotificationID]) REFERENCES [dbo].[SpecialNotification] ([SpecialNotificationID]),
    FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])
);

