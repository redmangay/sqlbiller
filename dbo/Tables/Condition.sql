﻿CREATE TABLE [dbo].[Condition] (
    [ConditionID]   INT           IDENTITY (1, 1) NOT NULL,
    [ColumnID]      INT           NOT NULL,
    [ConditionType] CHAR (1)      NOT NULL,
    [CheckColumnID] INT           NOT NULL,
    [CheckValue]    VARCHAR (MAX) NULL,
    [CheckFormula]  VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ConditionID] PRIMARY KEY CLUSTERED ([ConditionID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Condition_Column_CheckColumnID] FOREIGN KEY ([CheckColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_Condition_Column_ColumnID] FOREIGN KEY ([ColumnID]) REFERENCES [dbo].[Column] ([ColumnID])
);

