﻿CREATE TABLE [dbo].[GraphDefinition] (
    [GraphDefinitionID] INT           IDENTITY (1, 1) NOT NULL,
    [AccountID]         INT           NULL,
    [DefinitionName]    VARCHAR (200) NOT NULL,
    [Definition]        VARCHAR (MAX) NOT NULL,
    [IsSystem]          BIT           CONSTRAINT [DF_GraphDefinition_IsSystem] DEFAULT ((1)) NOT NULL,
    [TableID]           INT           NULL,
    [ColumnID]          INT           NULL,
    [DefinitionKey]     VARCHAR (200) NULL,
    [IsActive]          BIT           CONSTRAINT [DF_GraphDefinition_IsActive] DEFAULT ((1)) NULL,
    [IsHidden]          BIT           CONSTRAINT [DF_GraphDefinition_IsHidden] DEFAULT ((0)) NULL,
    [DataColumn1ID]     INT           NULL,
    [DataColumn2ID]     INT           NULL,
    [DataColumn3ID]     INT           NULL,
    [DataColumn4ID]     INT           NULL,
    [DateAdded]         DATETIME      CONSTRAINT [DF_GraphDefinition_DateAdded] DEFAULT (getdate()) NULL,
    [IsSingleSeries]    BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_GraphDefinition] PRIMARY KEY CLUSTERED ([GraphDefinitionID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_GraphDefinition_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_GraphDefinition_Column] FOREIGN KEY ([ColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_GraphDefinition_Column1] FOREIGN KEY ([DataColumn1ID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_GraphDefinition_Column2] FOREIGN KEY ([DataColumn2ID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_GraphDefinition_Column3] FOREIGN KEY ([DataColumn3ID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_GraphDefinition_Column4] FOREIGN KEY ([DataColumn4ID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_GraphDefinition_Table] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID])
);


GO
CREATE NONCLUSTERED INDEX [IX_ColumnID]
    ON [dbo].[GraphDefinition]([ColumnID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_DataColumn1ID]
    ON [dbo].[GraphDefinition]([DataColumn1ID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_DataColumn2ID]
    ON [dbo].[GraphDefinition]([DataColumn2ID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_DataColumn3ID]
    ON [dbo].[GraphDefinition]([DataColumn3ID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_DataColumn4ID]
    ON [dbo].[GraphDefinition]([DataColumn4ID] ASC) WITH (FILLFACTOR = 90);

