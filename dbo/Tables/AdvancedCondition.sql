﻿CREATE TABLE [dbo].[AdvancedCondition] (
    [AdvancedConditionID]  INT            IDENTITY (1, 1) NOT NULL,
    [ConditionType]        VARCHAR (25)   NOT NULL,
    [ConditionSubType]     CHAR (1)       NULL,
    [ColumnID]             INT            NOT NULL,
    [ConditionColumnID]    INT            NOT NULL,
    [ConditionColumnValue] NVARCHAR (250) NULL,
    [ConditionOperator]    VARCHAR (25)   NULL,
    [DisplayOrder]         INT            NOT NULL,
    [JoinOperator]         VARCHAR (10)   NULL,
    [Status]               VARCHAR (1)    NULL,
    CONSTRAINT [PK_AdvancedCondition] PRIMARY KEY CLUSTERED ([AdvancedConditionID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_AdvancedCondition_Column] FOREIGN KEY ([ColumnID]) REFERENCES [dbo].[Column] ([ColumnID]),
    CONSTRAINT [FK_AdvancedCondition_ConditionColumn] FOREIGN KEY ([ConditionColumnID]) REFERENCES [dbo].[Column] ([ColumnID]) ON DELETE CASCADE
);

