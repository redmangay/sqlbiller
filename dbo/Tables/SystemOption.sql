﻿CREATE TABLE [dbo].[SystemOption] (
    [SystemOptionID] INT             IDENTITY (1, 1) NOT NULL,
    [OptionKey]      NVARCHAR (200)  NOT NULL,
    [OptionValue]    NVARCHAR (1000) NOT NULL,
    [OptionNotes]    NTEXT           NOT NULL,
    [DateAdded]      DATETIME        CONSTRAINT [DF_SystemOption_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]    DATETIME        CONSTRAINT [DF_SystemOption_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [AccountID]      INT             NULL,
    [TableID]        INT             NULL,
    CONSTRAINT [PK_SystemOption] PRIMARY KEY CLUSTERED ([SystemOptionID] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [UQ_SystemOption_OptionKey] UNIQUE NONCLUSTERED ([OptionKey] ASC, [AccountID] ASC, [TableID] ASC) WITH (FILLFACTOR = 90)
);


GO
Create TRIGGER [dbo].[tg_SystemOptionUpdated] ON [dbo].SystemOption AFTER UPDATE AS UPDATE SystemOption
 SET DateUpdated = GETDATE() FROM inserted WHERE (SystemOption.SystemOptionID = inserted.SystemOptionID)