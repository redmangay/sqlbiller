﻿CREATE TABLE [dbo].[LinkedUser] (
    [LinkedUserID] INT IDENTITY (1, 1) NOT NULL,
    [UserID]       INT NOT NULL,
    [AccountID]    INT NOT NULL,
    CONSTRAINT [PK_LinkedUser] PRIMARY KEY CLUSTERED ([LinkedUserID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_LinkedUser_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_LinkedUser_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID]),
    CONSTRAINT [UQ_LinkedUser_User_Account] UNIQUE NONCLUSTERED ([UserID] ASC, [AccountID] ASC) WITH (FILLFACTOR = 90)
);

