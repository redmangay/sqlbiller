﻿CREATE TABLE [dbo].[ReportItemFilter] (
    [ReportItemFilterID] INT            IDENTITY (1, 1) NOT NULL,
    [ReportItemID]       INT            NOT NULL,
    [FilterColumnID]     INT            NOT NULL,
    [FilterColumnValue]  NVARCHAR (250) NULL,
    [FilterOperator]     VARCHAR (25)   NULL,
    [DisplayOrder]       INT            NOT NULL,
    [JoinOperator]       VARCHAR (10)   NULL,
    CONSTRAINT [PK_ReportItemFilter] PRIMARY KEY CLUSTERED ([ReportItemFilterID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ReportItemFilter_Column] FOREIGN KEY ([FilterColumnID]) REFERENCES [dbo].[Column] ([ColumnID]) ON DELETE CASCADE,
    CONSTRAINT [FK_ReportItemFilter_ReportItem] FOREIGN KEY ([ReportItemID]) REFERENCES [dbo].[ReportItem] ([ReportItemID]) ON DELETE CASCADE
);

