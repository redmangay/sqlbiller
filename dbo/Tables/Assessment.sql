﻿CREATE TABLE [dbo].[Assessment] (
    [AssessmentID]  INT           IDENTITY (1, 1) NOT NULL,
    [AssementName]  VARCHAR (MAX) NOT NULL,
    [ParentTableID] INT           NOT NULL,
    [ChildTableID]  INT           NOT NULL,
    [DisplayOrder]  INT           NOT NULL,
    CONSTRAINT [PK_Assessment] PRIMARY KEY CLUSTERED ([AssessmentID] ASC) WITH (FILLFACTOR = 90)
);

