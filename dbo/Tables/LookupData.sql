﻿CREATE TABLE [dbo].[LookupData] (
    [LookupDataID] INT           IDENTITY (1, 1) NOT NULL,
    [LookupTypeID] INT           NOT NULL,
    [DisplayText]  VARCHAR (200) NOT NULL,
    [DateAdded]    DATETIME      CONSTRAINT [DF_LookupData_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]  DATETIME      CONSTRAINT [DF_LookupData_DateUpdated_1] DEFAULT (getdate()) NOT NULL,
    [LockedValue]  BIT           CONSTRAINT [DF_LookupData_LockedValue] DEFAULT ((0)) NOT NULL,
    [Value]        VARCHAR (200) NULL,
    CONSTRAINT [PK_LookupData] PRIMARY KEY CLUSTERED ([LookupDataID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_LookupData_LookupType] FOREIGN KEY ([LookupTypeID]) REFERENCES [dbo].[LookupType] ([LookupTypeID])
);


GO
CREATE NONCLUSTERED INDEX [IX_LookupTypeID]
    ON [dbo].[LookupData]([LookupTypeID] ASC) WITH (FILLFACTOR = 90);


GO
Create TRIGGER [dbo].[tr_LookUpDataUpdated] ON [dbo].[LookupData] AFTER UPDATE AS UPDATE [LookUpData]
 SET DateUpdated = GETDATE() FROM inserted WHERE ([LookUpData].LookUpDataID = inserted.LookUpDataID)
