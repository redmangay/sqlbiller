﻿CREATE TABLE [dbo].[RegularTask] (
    [RegularTaskID]    INT            IDENTITY (1, 1) NOT NULL,
    [ClassName]        VARCHAR (1000) NOT NULL,
    [MethodName]       VARCHAR (1000) NOT NULL,
    [MethodParameters] VARCHAR (MAX)  NULL,
    [IsAsync]          BIT            CONSTRAINT [DF_RegularTask_IsAsync] DEFAULT ((0)) NOT NULL,
    [Notes]            VARCHAR (MAX)  NULL,
    [Interval]         INT            NOT NULL,
    [NextRun]          DATETIME       NULL,
    [LastRun]          DATETIME       NULL,
    [LastRunResult]    VARCHAR (MAX)  NULL,
    [LockInterval]     INT            DEFAULT ((60)) NOT NULL,
    CONSTRAINT [PK_RegularTask] PRIMARY KEY CLUSTERED ([RegularTaskID] ASC) WITH (FILLFACTOR = 90)
);

