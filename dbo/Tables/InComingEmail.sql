﻿CREATE TABLE [dbo].[InComingEmail] (
    [InComingEmailID] INT             IDENTITY (1, 1) NOT NULL,
    [EmailSubject]    NVARCHAR (4000) NULL,
    [EmailFrom]       NVARCHAR (300)  NOT NULL,
    [EmailTo]         NVARCHAR (4000) NOT NULL,
    [CC]              NVARCHAR (4000) NULL,
    [BCC]             NVARCHAR (4000) NULL,
    [Attachments]     NVARCHAR (200)  NULL,
    [EmailDate]       DATETIME        NULL,
    [MessageID]       NVARCHAR (1000) NULL,
    [RawMessage]      NTEXT           NULL,
    [TextMessage]     NTEXT           NULL,
    [HTMLTextMessage] NTEXT           NULL,
    [MIMEVersion]     NVARCHAR (500)  NULL,
    [DateCreated]     DATETIME        NULL,
    [ParentEmailID]   INT             NULL,
    [BatchIDs]        NVARCHAR (200)  NULL,
    [POPServer]       VARCHAR (200)   NULL,
    CONSTRAINT [PK_InComingEmail] PRIMARY KEY CLUSTERED ([InComingEmailID] ASC) WITH (FILLFACTOR = 90)
);

