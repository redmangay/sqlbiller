﻿CREATE TABLE [dbo].[Audit] (
    [AuditID]         INT            IDENTITY (1, 1) NOT NULL,
    [TableName]       VARCHAR (256)  NOT NULL,
    [TableID]         INT            NULL,
    [PrimaryKeyValue] VARCHAR (1000) NULL,
    [FieldName]       VARCHAR (256)  NULL,
    [OldValue]        VARCHAR (MAX)  NULL,
    [NewValue]        VARCHAR (MAX)  NULL,
    [DateAdded]       DATETIME       DEFAULT (getdate()) NOT NULL,
    [UserID]          INT            NULL,
    [AccountID]       INT            NULL,
    [ChangeReason]    VARCHAR (MAX)  NULL
);

