﻿CREATE TABLE [dbo].[User] (
    [UserID]                     INT            IDENTITY (1, 1) NOT NULL,
    [FirstName]                  NVARCHAR (50)  NULL,
    [LastName]                   NVARCHAR (50)  NULL,
    [PhoneNumber]                VARCHAR (20)   NULL,
    [Email]                      NVARCHAR (200) NOT NULL,
    [IsActive]                   BIT            CONSTRAINT [DF__User__IsActive__145C0A3F] DEFAULT ((1)) NOT NULL,
    [DateAdded]                  DATETIME       CONSTRAINT [DF_User_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]                DATETIME       CONSTRAINT [DF_User_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [LoginCount]                 INT            DEFAULT ((0)) NOT NULL,
    [LastLoginTime]              DATETIME       DEFAULT (getdate()) NOT NULL,
    [SecondFactorAuthentication] NVARCHAR (200) NULL,
    [EncryptedPassword]          BINARY (64)    DEFAULT ((0)) NOT NULL,
    [SaltPassword]               VARBINARY (8)  DEFAULT (Crypt_Gen_Random((8))) NOT NULL,
    [MemorableDate]              BINARY (64)    NULL,
    [SaltMemDate]                VARBINARY (8)  NULL,
    [IsTempPassword]             BIT            DEFAULT ((0)) NULL,
    [ProfilePicture]             VARCHAR (MAX)  NULL,
    [LoginToken]                 VARCHAR (36)   DEFAULT (newid()) NULL,
    [Attributes]                 VARCHAR (MAX)  NULL,
    [IsEmailConfirmed]           BIT            NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([UserID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [UQ_User_Email] UNIQUE NONCLUSTERED ([Email] ASC) WITH (FILLFACTOR = 90)
);


GO
Create TRIGGER [dbo].[UserUpdated] ON dbo.[User] AFTER UPDATE AS UPDATE [User]
 SET DateUpdated = GETDATE() FROM inserted WHERE ([User].UserID = inserted.UserID)
