﻿CREATE TABLE [dbo].[DocumentSection] (
    [DocumentSectionID]      INT            IDENTITY (1, 1) NOT NULL,
    [DocumentID]             INT            NOT NULL,
    [DocumentSectionTypeID]  INT            NOT NULL,
    [Position]               INT            NOT NULL,
    [SectionName]            VARCHAR (100)  NULL,
    [Content]                VARCHAR (MAX)  NULL,
    [Filter]                 VARCHAR (MAX)  NULL,
    [Details]                VARCHAR (MAX)  NULL,
    [DateAdded]              DATETIME       NOT NULL,
    [DateUpdated]            DATETIME       NOT NULL,
    [DocumentSectionStyleID] INT            NULL,
    [ValueFields]            NVARCHAR (MAX) NULL,
    [ParentSectionID]        INT            NULL,
    [ColumnIndex]            INT            NULL,
    CONSTRAINT [PK_dbo.dgDocumentSection] PRIMARY KEY CLUSTERED ([DocumentSectionID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [dgDocumentSectionType_dgDocumentSection] FOREIGN KEY ([DocumentSectionTypeID]) REFERENCES [dbo].[DocumentSectionType] ([DocumentSectionTypeID]),
    CONSTRAINT [FK_DocumentSection_Document] FOREIGN KEY ([DocumentID]) REFERENCES [dbo].[Document] ([DocumentID]),
    CONSTRAINT [FK_DocumentSection_DocumentSectionStyle] FOREIGN KEY ([DocumentSectionStyleID]) REFERENCES [dbo].[DocumentSectionStyle] ([DocumentSectionStyleID])
);


GO
CREATE NONCLUSTERED INDEX [IX_DocumentID]
    ON [dbo].[DocumentSection]([DocumentID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_DocumentSectionStyleID]
    ON [dbo].[DocumentSection]([DocumentSectionStyleID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_DocumentSectionTypeID]
    ON [dbo].[DocumentSection]([DocumentSectionTypeID] ASC) WITH (FILLFACTOR = 90);

