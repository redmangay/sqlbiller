﻿CREATE TABLE [dbo].[ImportTemplate] (
    [ImportTemplateID]           INT            IDENTITY (1, 1) NOT NULL,
    [TableID]                    INT            NOT NULL,
    [ImportTemplateName]         VARCHAR (255)  NOT NULL,
    [HelpText]                   VARCHAR (1000) NULL,
    [TemplateUniqueFileName]     VARCHAR (500)  NULL,
    [FileFormat]                 VARCHAR (10)   NULL,
    [SPAfterImport]              VARCHAR (255)  NULL,
    [Notes]                      VARCHAR (1000) NULL,
    [ImportDataStartRow]         INT            NULL,
    [ImportColumnHeaderRow]      INT            NULL,
    [IsImportPositional]         BIT            CONSTRAINT [DF_ImportTemplate_IsImportPositional] DEFAULT ((0)) NOT NULL,
    [SPAfterUpload]              VARCHAR (100)  NULL,
    [SPProcessFile]              VARCHAR (255)  NULL,
    [IsAdvanced]                 BIT            NULL,
    [DataTableStartCustomMethod] VARCHAR (250)  NULL,
    PRIMARY KEY CLUSTERED ([ImportTemplateID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ImportTemplate_Table_TableID] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [UX_TableID_ImportTemplateName] UNIQUE NONCLUSTERED ([TableID] ASC, [ImportTemplateName] ASC) WITH (FILLFACTOR = 90)
);

