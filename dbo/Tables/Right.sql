﻿CREATE TABLE [dbo].[Right] (
    [RightId]      INT           IDENTITY (1, 1) NOT NULL,
    [RightName]    NVARCHAR (50) NOT NULL,
    [MaxCount]     INT           NULL,
    [ValidNumDays] INT           NULL,
    CONSTRAINT [PK_Right] PRIMARY KEY CLUSTERED ([RightId] ASC) WITH (FILLFACTOR = 90)
);

