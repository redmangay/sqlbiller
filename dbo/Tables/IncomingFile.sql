﻿CREATE TABLE [dbo].[IncomingFile] (
    [IncomingFileID] INT            IDENTITY (1, 1) NOT NULL,
    [Location]       NVARCHAR (250) NULL,
    [FileName]       NVARCHAR (250) NOT NULL,
    [Files]          NVARCHAR (200) NOT NULL,
    [FileDate]       DATETIME       NULL,
    [DateCreated]    DATETIME       NULL,
    [BatchIDs]       NVARCHAR (200) NULL,
    CONSTRAINT [PK_IncomingFile] PRIMARY KEY CLUSTERED ([IncomingFileID] ASC) WITH (FILLFACTOR = 90)
);

