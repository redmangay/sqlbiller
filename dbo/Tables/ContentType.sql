﻿CREATE TABLE [dbo].[ContentType] (
    [ContentTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [ContentTypeKey]  CHAR (1)      NOT NULL,
    [ContentTypeName] VARCHAR (200) NOT NULL,
    PRIMARY KEY CLUSTERED ([ContentTypeID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [UX_ContentTypeKey] UNIQUE NONCLUSTERED ([ContentTypeKey] ASC) WITH (FILLFACTOR = 90)
);

