﻿CREATE TABLE [dbo].[ReportItemSortOrder] (
    [ReportItemSortOrderID] INT IDENTITY (1, 1) NOT NULL,
    [ReportItemID]          INT NOT NULL,
    [SortColumnID]          INT NOT NULL,
    [IsDescending]          BIT NOT NULL,
    [DisplayOrder]          INT NOT NULL,
    CONSTRAINT [PK_ReportItemSortOrder] PRIMARY KEY CLUSTERED ([ReportItemSortOrderID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ReportItemSortOrder_Column] FOREIGN KEY ([SortColumnID]) REFERENCES [dbo].[Column] ([ColumnID]) ON DELETE CASCADE,
    CONSTRAINT [FK_ReportItemSortOrder_ReportItem] FOREIGN KEY ([ReportItemID]) REFERENCES [dbo].[ReportItem] ([ReportItemID]) ON DELETE CASCADE
);

