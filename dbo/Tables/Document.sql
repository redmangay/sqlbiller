﻿CREATE TABLE [dbo].[Document] (
    [DocumentID]          INT              IDENTITY (1, 1) NOT NULL,
    [AccountID]           INT              NOT NULL,
    [DocumentText]        VARCHAR (250)    NOT NULL,
    [DocumentTypeID]      INT              NULL,
    [UniqueName]          NVARCHAR (500)   NOT NULL,
    [FileTitle]           NVARCHAR (255)   NOT NULL,
    [DocumentDate]        DATETIME         NULL,
    [DateAdded]           DATETIME         CONSTRAINT [DF_Document_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]         DATETIME         CONSTRAINT [DF_Document_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [UserID]              INT              NULL,
    [TableID]             INT              NULL,
    [ReportHTML]          VARCHAR (MAX)    NULL,
    [IsReportPublic]      BIT              CONSTRAINT [DF_Document_IsReportPublic] DEFAULT ((0)) NOT NULL,
    [DocumentDescription] VARCHAR (MAX)    NULL,
    [DocumentEndDate]     DATETIME         NULL,
    [ForDashBoard]        BIT              NULL,
    [FolderID]            INT              NULL,
    [Size]                DECIMAL (18, 10) NULL,
    [ReportType]          VARCHAR (25)     NULL,
    [ReportOptionTable]   INT              NULL,
    CONSTRAINT [PK_Document] PRIMARY KEY CLUSTERED ([DocumentID] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([ReportOptionTable]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [FK_Document_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_Document_DocumentType] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentType] ([DocumentTypeID]),
    CONSTRAINT [FK_Document_Folder] FOREIGN KEY ([FolderID]) REFERENCES [dbo].[Folder] ([FolderID]),
    CONSTRAINT [FK_Document_SampleType] FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [FK_Document_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])
);


GO
CREATE NONCLUSTERED INDEX [IX_DocumentTypeID]
    ON [dbo].[Document]([DocumentTypeID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_FolderID]
    ON [dbo].[Document]([FolderID] ASC) WITH (FILLFACTOR = 90);


GO
 
Create TRIGGER [dbo].[tg_DocumentUpdated] ON dbo.[Document] AFTER UPDATE AS UPDATE Document
 SET DateUpdated = GETDATE() FROM inserted WHERE (Document.DocumentID = inserted.DocumentID)