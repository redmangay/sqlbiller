﻿CREATE TABLE [dbo].[DataReminder] (
    [DataReminderID]  INT           IDENTITY (1, 1) NOT NULL,
    [ColumnID]        INT           NOT NULL,
    [NumberOfDays]    INT           NOT NULL,
    [ReminderHeader]  VARCHAR (500) NULL,
    [ReminderContent] VARCHAR (MAX) NULL,
    CONSTRAINT [PK_DataReminder] PRIMARY KEY CLUSTERED ([DataReminderID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_DataReminder_SampleColumn] FOREIGN KEY ([ColumnID]) REFERENCES [dbo].[Column] ([ColumnID])
);


GO
CREATE NONCLUSTERED INDEX [IX_ColumnID]
    ON [dbo].[DataReminder]([ColumnID] ASC) WITH (FILLFACTOR = 90);

