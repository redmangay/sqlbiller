﻿CREATE TABLE [dbo].[Account] (
    [AccountID]                INT             IDENTITY (1, 1) NOT NULL,
    [AccountName]              NVARCHAR (100)  NOT NULL,
    [DateAdded]                DATETIME        CONSTRAINT [DF_Account_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]              DATETIME        CONSTRAINT [DF_Account_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [Logo]                     IMAGE           NULL,
    [IsActive]                 BIT             CONSTRAINT [DF_Account_IsActive] DEFAULT ((1)) NULL,
    [AccountTypeID]            INT             NULL,
    [ExpiryDate]               DATETIME        NULL,
    [UseDefaultLogo]           BIT             CONSTRAINT [DF_Account_UseDefaultLogo] DEFAULT ((1)) NOT NULL,
    [MapCentreLat]             DECIMAL (9, 6)  NULL,
    [MapCentreLong]            DECIMAL (9, 6)  NULL,
    [MapZoomLevel]             INT             NULL,
    [MapDefaultTableID]        INT             NULL,
    [ExceedLastEmail]          DATETIME        NULL,
    [OtherMapZoomLevel]        INT             NULL,
    [CountryID]                INT             NULL,
    [PhoneNumber]              NVARCHAR (20)   NULL,
    [CreatedByWizard]          BIT             CONSTRAINT [DF_Account_CreatedByWizard] DEFAULT ((0)) NOT NULL,
    [ExtensionPacks]           INT             CONSTRAINT [DF_Account_ExtensionPacks] DEFAULT ((0)) NOT NULL,
    [Alerts]                   BIT             CONSTRAINT [DF_Account_Alerts] DEFAULT ((0)) NOT NULL,
    [ReportGen]                BIT             CONSTRAINT [DF_Account_ReportGen] DEFAULT ((0)) NOT NULL,
    [SMSCount]                 INT             CONSTRAINT [DF_Account_SMSCount] DEFAULT ((0)) NULL,
    [Layout]                   INT             CONSTRAINT [DF_Account_Layout] DEFAULT ((0)) NOT NULL,
    [SystemAccount]            BIT             CONSTRAINT [DF__Account__SystemA__5C2D8B0C] DEFAULT ((0)) NULL,
    [NextBilledAccountTypeID]  INT             NULL,
    [GSTApplicable]            BIT             NULL,
    [OrganisationName]         NVARCHAR (200)  NULL,
    [BillingPhoneNumber]       NVARCHAR (20)   NULL,
    [BillingAddress]           NVARCHAR (MAX)  NULL,
    [BillingEmail]             NVARCHAR (200)  NULL,
    [BillEveryXMonths]         TINYINT         NULL,
    [PaymentMethod]            CHAR (1)        NULL,
    [ConfirmationCode]         VARCHAR (5)     CONSTRAINT [DF__Account__Confirm__025333F4] DEFAULT (right(CONVERT([varchar](20),rand(),(0)),(5))) NULL,
    [BillingFirstName]         NVARCHAR (50)   NULL,
    [BillingLastName]          NVARCHAR (50)   NULL,
    [Comment]                  NVARCHAR (MAX)  NULL,
    [ClientRef]                VARCHAR (20)    NULL,
    [DefaultGraphOptionID]     INT             NULL,
    [CopyRightInfo]            NVARCHAR (1000) NULL,
    [LoginContentID]           INT             NULL,
    [HideMyAccount]            BIT             NULL,
    [HideDashBoard]            BIT             NULL,
    [MasterPage]               VARCHAR (100)   NULL,
    [UseDataScope]             BIT             NULL,
    [HomeMenuCaption]          NVARCHAR (100)  NULL,
    [ShowOpenMenu]             BIT             DEFAULT ((1)) NOT NULL,
    [IsReportTopMenu]          BIT             NULL,
    [UploadAfterVerificaition] BIT             NULL,
    [EmailCount]               INT             NULL,
    [LabelOnTop]               BIT             NULL,
    [ReportServer]             VARCHAR (100)   NULL,
    [ReportUser]               VARCHAR (100)   NULL,
    [ReportPW]                 VARCHAR (100)   NULL,
    [ReportServerUrl]          VARCHAR (250)   NULL,
    [SMTPEmail]                VARCHAR (255)   NULL,
    [SMTPUserName]             VARCHAR (255)   NULL,
    [SMTPPassword]             VARCHAR (50)    NULL,
    [SMTPPort]                 VARCHAR (10)    NULL,
    [SMTPServer]               VARCHAR (100)   NULL,
    [SMTPSSL]                  VARCHAR (10)    NULL,
    [SMTPReplyToEmail]         VARCHAR (255)   NULL,
    [POP3Email]                VARCHAR (255)   NULL,
    [POP3UserName]             VARCHAR (255)   NULL,
    [POP3Password]             VARCHAR (50)    NULL,
    [POP3Port]                 VARCHAR (10)    NULL,
    [POP3Server]               VARCHAR (100)   NULL,
    [POP3SSL]                  VARCHAR (10)    NULL,
    [SPAfterLogin]             VARCHAR (250)   NULL,
    CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED ([AccountID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Account_AccountType] FOREIGN KEY ([AccountTypeID]) REFERENCES [dbo].[AccountType] ([AccountTypeID]),
    CONSTRAINT [FK_Account_DefaultMap_SampleType] FOREIGN KEY ([MapDefaultTableID]) REFERENCES [dbo].[Table] ([TableID]),
    CONSTRAINT [FK_Account_GraphOption] FOREIGN KEY ([DefaultGraphOptionID]) REFERENCES [dbo].[GraphOption] ([GraphOptionID]),
    CONSTRAINT [FK_Account_NextBilledAccountTypeID] FOREIGN KEY ([NextBilledAccountTypeID]) REFERENCES [dbo].[AccountType] ([AccountTypeID]),
    CONSTRAINT [UQ_AccountName] UNIQUE NONCLUSTERED ([AccountName] ASC) WITH (FILLFACTOR = 90)
);


GO
ALTER TABLE [dbo].[Account] NOCHECK CONSTRAINT [FK_Account_DefaultMap_SampleType];


GO
CREATE NONCLUSTERED INDEX [IX_AccountTypeID]
    ON [dbo].[Account]([AccountTypeID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_DefaultGraphOptionID]
    ON [dbo].[Account]([DefaultGraphOptionID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_NextBilledAccountTypeID]
    ON [dbo].[Account]([NextBilledAccountTypeID] ASC) WITH (FILLFACTOR = 90);


GO
Create TRIGGER [dbo].[tr_Account_Updated] ON dbo.Account AFTER UPDATE AS 
	UPDATE Account 
		SET DateUpdated = GETDATE() 
		FROM inserted 
		WHERE (Account.AccountID = inserted.AccountID)