﻿CREATE TABLE [dbo].[DocumentSectionStyle] (
    [DocumentSectionStyleID] INT           IDENTITY (1, 1) NOT NULL,
    [AccountID]              INT           NOT NULL,
    [StyleName]              VARCHAR (100) NOT NULL,
    [StyleDefinition]        VARCHAR (MAX) NOT NULL,
    [DateAdded]              DATETIME      CONSTRAINT [DF_dgDocumentTextStyle_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]            DATETIME      CONSTRAINT [DF_dgDocumentTextStyle_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [IsSystem]               BIT           CONSTRAINT [DF_DocumentSectionStyle_IsSystem] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_dbo.dgDocumentTextStyle] PRIMARY KEY CLUSTERED ([DocumentSectionStyleID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [UQ_DocumentSectionStyle_AccountStyleName] UNIQUE NONCLUSTERED ([AccountID] ASC, [StyleName] ASC) WITH (FILLFACTOR = 90)
);

