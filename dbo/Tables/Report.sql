﻿CREATE TABLE [dbo].[Report] (
    [ReportID]           INT           IDENTITY (1, 1) NOT NULL,
    [AccountID]          INT           NOT NULL,
    [ReportName]         VARCHAR (250) NOT NULL,
    [ReportDescription]  VARCHAR (MAX) NULL,
    [ReportDates]        INT           NOT NULL,
    [StartDate]          DATETIME      NULL,
    [EndDate]            DATETIME      NULL,
    [DateAdded]          DATETIME      CONSTRAINT [DF_Report_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]        DATETIME      CONSTRAINT [DF_Report_DateUpdated] DEFAULT (getdate()) NOT NULL,
    [UserID]             INT           NULL,
    [ReportDatesBefore]  INT           CONSTRAINT [DF_Report_ReportDatesBefore] DEFAULT ((0)) NOT NULL,
    [ScheduledFrequency] INT           NULL,
    [NextRunDateTime]    DATETIME      NULL,
    [ScheduleOptions]    VARCHAR (MAX) NULL,
    [DaysFrom]           INT           NULL,
    [DaysTo]             INT           NULL,
    CONSTRAINT [PK_Report] PRIMARY KEY CLUSTERED ([ReportID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Report_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_Report_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])
);

