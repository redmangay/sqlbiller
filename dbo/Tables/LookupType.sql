﻿CREATE TABLE [dbo].[LookupType] (
    [LookupTypeID]   INT           NOT NULL,
    [LookupTypeName] VARCHAR (200) NOT NULL,
    [LockedValue]    BIT           CONSTRAINT [DF_LookupType_LockedValues] DEFAULT ((0)) NOT NULL,
    [DateAdded]      DATETIME      CONSTRAINT [DF_LookupType_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateUpdated]    DATETIME      CONSTRAINT [DF_LookupType_DateUpdated_1] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_LookupType] PRIMARY KEY CLUSTERED ([LookupTypeID] ASC) WITH (FILLFACTOR = 90)
);


GO
Create TRIGGER [dbo].[tr_LookUpTypeUpdated] ON [dbo].[LookupType] AFTER UPDATE AS UPDATE [LookUpType]
 SET DateUpdated = GETDATE() FROM inserted WHERE ([LookUpType].LookUpTypeID = inserted.LookUpTypeID)
