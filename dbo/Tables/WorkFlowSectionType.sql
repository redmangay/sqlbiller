﻿CREATE TABLE [dbo].[WorkFlowSectionType] (
    [WorkFlowSectionTypeID]   INT           NOT NULL,
    [WorkFlowSectionTypeName] VARCHAR (200) NOT NULL,
    CONSTRAINT [PK_WorkFlowSectionType] PRIMARY KEY CLUSTERED ([WorkFlowSectionTypeID] ASC) WITH (FILLFACTOR = 90)
);

