﻿CREATE TABLE [dbo].[RoleType] (
    [RoleType]           SMALLINT     NOT NULL,
    [RoleName]           VARCHAR (20) NOT NULL,
    [CanInsert]          CHAR (1)     NOT NULL,
    [CanUpdate]          CHAR (1)     NOT NULL,
    [CanDelete]          CHAR (1)     NOT NULL,
    [CanSelect]          CHAR (1)     NOT NULL,
    [CanAccessAdminMenu] CHAR (1)     NULL,
    CONSTRAINT [PK_RoleType] PRIMARY KEY CLUSTERED ([RoleType] ASC) WITH (FILLFACTOR = 90)
);

