﻿

CREATE PROCEDURE [dbo].[Get_Comma_Sep_IDs]
(
	@sIDName varchar(50),
	@sTableName varchar(50),
	@sWHERE varchar(1000)
)
AS
-- EXEC [dbo].[Get_Comma_Sep_IDs] 'ViewItemID','[ViewItem]',' ViewID=41547'
BEGIN TRY
	DECLARE @sSQL varchar(MAX)='
			DECLARE @sID varchar(MAX)=NULL;
			SELECT @sID=COALESCE(@sID+'','','''')+CAST('+@sIDName+' AS varchar) FROM '+@sTableName+' WHERE '+@sWHERE+';
			SELECT @sID;
			'

	EXEC (@sSQL)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Get_Comma_Sep_IDs', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
