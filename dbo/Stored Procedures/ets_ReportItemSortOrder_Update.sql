﻿

CREATE PROCEDURE [dbo].[ets_ReportItemSortOrder_Update]
(
	@nReportItemSortOrderID int output,
	@nReportItemID int,
	@nSortColumnID int,
	@bIsDescending bit = 0,
	@nDisplayOrder int
)
AS

BEGIN TRY
	UPDATE [ReportItemSortOrder]
	SET 
		ReportItemID = @nReportItemID,
		SortColumnID = @nSortColumnID,
		IsDescending = @bIsDescending,
		DisplayOrder = @nDisplayOrder
	WHERE ReportItemSortOrderID = @nReportItemSortOrderID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_ReportItemSortOrder_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
