﻿-- =============================================
-- Author:		<Manoj sharma>
-- Create date: <20 may 2015>
--modify date : <22 may 2015>
-- Description:	<use to return sql based in tableid>
-- =============================================
CREATE PROCEDURE [dbo].[spGetTableSQL]
(
     @TableID		VARCHAR(MAX) = NULL
	,@TableName		VARCHAR(MAX) = NULL
)	
AS
BEGIN
	
		SET NOCOUNT ON;
	declare @tableexist int

	select top 1  @tableexist= count(*) from [Table] where (TableID =@TableID OR TableName Like'%' + @TableName + '%') and 	[Table].IsActive = 1

	if isnull(@tableexist,0) =0
	PRINT			'Warning: Table  with id  ' + @TableID + ', '  + @TableName + ' does not exists'
	
	
	else
	
	begin   --For  Select SQL
	declare @SQL varchar(max)
	
		SET				@SQL = 'SELECT			['+@TableName+'].[TableID]
'

		SELECT			@SQL = @SQL + '				,['+@TableName+'].' + [Column].[SystemName] + '	AS				[' + [Column].[DisplayName] + ']
'
		FROM			[Table]
		JOIN			[Column] ON						[Column].TableID = [Table].TableID
											AND			([Column].IsStandard = 0
											OR			[Column].SystemName = 'RecordID')
		WHERE			[Table].IsActive = 1
		AND				[Table].TableID = @TableID
		AND				[Table].[TableName] = @TableName
		ORDER BY		[Column].[DisplayOrder]

		SET				@SQL = SUBSTRING(@SQL, 1, LEN(@SQL) - 2) + '
				,['+@TableName+'].DateAdded
FROM			[Record] ['+@TableName+']
WHERE			['+@TableName+'].IsActive = 1
AND				['+@TableName+'].TableID = ''' + @TableID + ''''
		
		
		
	end  -- select sql end

	Print	@SQL

	begin  -- for delete sql

	declare @deletesql varchar(max)
	
	
	set @deletesql ='delete from [Record] Where TableID = ''' + @TableID + ''''


	end   -- delete query end

	
begin   --For  insert SQL
	declare @insertsql varchar(max)
	
		SET				@insertsql = 'insert into	Record 	([TableID],DateTimeRecorded,EnteredBy,IsActive,DateUpdated
		'

		SELECT			@insertsql = @insertsql + '			, ' + [Column].[SystemName] + ' --					[' + [Column].[DisplayName] + ']
		'
		FROM			[Table]
		JOIN			[Column] ON						[Column].TableID = [Table].TableID
											AND			([Column].IsStandard = 0
											OR			[Column].SystemName = 'RecordID')
		WHERE			[Table].IsActive = 1
		AND				[Table].TableID = @TableID
		AND				[Table].[TableName] = @TableName
		and [Column].SystemName not in ('RecordID')
		ORDER BY		[Column].[DisplayOrder]

		
		SET				@insertsql = SUBSTRING(@insertsql, 1, LEN(@insertsql) - 4) + '
		,DateAdded) 
		(
		'
		
  set 	 @insertsql =@insertsql + ' SeLECT			['+@TableName+'].[TableID],getdate(),EnteredBy,IsActive,DateUpdated
		'

		SELECT			@insertsql = @insertsql + '			, ['+@TableName+'].' + [Column].[SystemName] + '	AS				[' + [Column].[DisplayName] + ']
		'
		FROM			[Table]
		JOIN			[Column] ON						[Column].TableID = [Table].TableID
											AND			([Column].IsStandard = 0
											OR			[Column].SystemName = 'RecordID')
		WHERE			[Table].IsActive = 1
		AND				[Table].TableID = @TableID
		AND				[Table].[TableName] = @TableName
		and [Column].SystemName not in ('RecordID')
		ORDER BY		[Column].[DisplayOrder]

		SET				@insertsql = SUBSTRING(@insertsql, 1, LEN(@insertsql) - 4) + '
		,['+@TableName+'].DateAdded 
		FROM			
					[Record] ['+@TableName+']
		WHERE		['+@TableName+'].IsActive = 1
		AND			['+@TableName+'].TableID = ''' + @TableID + ''')'
		
	end  -- insert sql end

	


	


	select 			@SQL as selectquery, @deletesql as deletequery,@insertsql as insertquery
	
end
