﻿


CREATE PROCEDURE [dbo].[ets_Menu_Delete]
(
	@nMenuID int 
)
AS

BEGIN TRY

	--UPDATE [Menu] 
	--	SET IsActive=0 
	--	WHERE MenuID = @nMenuID
	

	DELETE MenuRole 
	   WHERE MenuID = @nMenuID
     DELETE FROM [Menu]
	   WHERE MenuID = @nMenuID

 


END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Menu_Delete', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
