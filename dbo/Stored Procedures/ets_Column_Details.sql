﻿

CREATE PROCEDURE [dbo].[ets_Column_Details]
(
	@nColumnID int 
)
AS

BEGIN TRY
	SET NOCOUNT ON;

	SELECT     [Table].TableName, [Column].*
		FROM         [Column] INNER JOIN
                     [Table] ON [Column].TableID = [Table].TableID
		WHERE ColumnID = @nColumnID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Column_Details', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

