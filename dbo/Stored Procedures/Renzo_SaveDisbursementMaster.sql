﻿CREATE PROCEDURE [dbo].[Renzo_SaveDisbursementMaster]
(
	@RecordID  int,		/*	Record ID passed from the table on execute, base your solution on this value	*/
	@UserID int,
	@Return varchar(max) output
	, @Context varchar(max) = null
)
AS

	Declare @MasterRate money
	Declare @Rate money
	DECLARE @MasterStatus varchar(max)
	
	
	SELECT @MasterRate = V002, @MasterStatus = V004 FROM [RECORD]
			WHERE RECORDID = @RecordID
--			EXEC [Renzo_SaveDisbursementMaster] '1901580','',''
	
		DECLARE @RecordID_RateUsed int
		
		DECLARE Disbursement_Master CURSOR FOR
		SELECT [RecordID] FROM [RECORD] WHERE V008 = @RecordID AND TABLEID = 2478 ORDER BY [RecordID];
	

		OPEN Disbursement_Master;
		FETCH NEXT FROM Disbursement_Master
		INTO @RecordID_RateUsed

		WHILE @@FETCH_STATUS = 0
		   BEGIN
		   
			SELECT @Rate= V002 FROM [RECORD]
			WHERE RECORDID = @RecordID_RateUsed
					
			IF @MasterRate = @Rate
			BEGIN
					--SELECT @MasterRate,@Rate
					UPDATE [Record] SET V005 = 'No'
					WHERE [RecordID] = @RecordID_RateUsed
			END
			ELSE
			BEGIN
					--SELECT @MasterRate,@Rate
					UPDATE [Record] SET V005 = 'Yes'
					WHERE [RecordID] = @RecordID_RateUsed
			END
		   
		   
		  FETCH NEXT FROM Disbursement_Master
		  INTO @RecordID_RateUsed
			  
		   END;
		

		CLOSE Disbursement_Master;
		DEALLOCATE Disbursement_Master;



/*======================================*/

/* lets update the status */
/* lets get the last status from Disbursement Rate table 
    as the basis if the Master status is changed  */

DECLARE @DisbursementStatus varchar(max) = '';
SELECT @DisbursementStatus = V009 
    FROM [Record]
	   WHERE RecordID = (SELECT MAX(RecordID) FROM [Record]  WHERE TableID = 2478)

/* lets match the status to the Master status */
/* if not matched; lets update all */
IF (@MasterStatus <> @DisbursementStatus)
BEGIN
    UPDATE Record
	   SET V009 = @MasterStatus
		  WHERE V008 = @RecordID
			 AND TableID = 2478
END

/*======================================*/
		

RETURN @@ERROR;