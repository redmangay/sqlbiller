﻿

CREATE PROCEDURE [dbo].[ets_UserRoleAccount_Select]
(
	@nUserID int  = NULL,
	@sOrder nvarchar(200) = UserRoleID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	[ets_UserRoleAccount_Select] @nUserID=658
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE IsPrimaryAccount=0'
	IF @nUserID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND UserID = '+CAST(@nUserID AS NVARCHAR)

	--SET @sSelect = 'SELECT * FROM 
	--(SELECT [UserRole].*, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum FROM [UserRole]' + @sWhere + ') as UserInfo'
	--SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [UserRole] ' + @sWhere 
 
	 SET @sSelect='SELECT * FROM 
	(SELECT IsPrimaryAccount,UserRoleID,  AccountName,RoleType,UserRole.RoleID,[Role].Role,ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum FROM 
	UserRole INNER JOIN Account ON UserRole.AccountID=Account.AccountID
	INNER JOIN [Role] ON UserRole.RoleID=[Role].RoleID ' + @sWhere + ') as UserInfo '
	 + ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)

	SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM UserRole INNER JOIN Account ON UserRole.AccountID=Account.AccountID
	INNER JOIN [Role] ON UserRole.RoleID=[Role].RoleID ' + @sWhere 

	-- Extend WHERE to include paging:
	--SET @sWhere = @sWhere + ' AND RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	--SET @sOrder = ' ORDER BY ' + @sOrder
	--SET @sSelect = @sSelect + @sWhere + @sOrder

	PRINT @sSelect

	EXEC (@sSelect)
	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_UserRoleAccount_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
