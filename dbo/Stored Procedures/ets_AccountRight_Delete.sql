﻿
 
CREATE PROCEDURE [dbo].[ets_AccountRight_Delete]
(
	@nAccountRightID int 
)
	/*----------------------------------------------------------------
	-- Stored Procedure: ets_AccountRight_Delete
	-- Produced by: DB Gurus (SPGen) 
	-- Date: Dec  3 2010  6:31PM
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	DELETE FROM AccountRight
	WHERE AccountRightID = @nAccountRightID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_AccountRight_Delete', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
