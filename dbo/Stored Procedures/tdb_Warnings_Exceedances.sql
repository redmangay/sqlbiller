﻿

CREATE PROCEDURE [dbo].[tdb_Warnings_Exceedances]
(
	@AccountID INT
)
AS
/*
	UNIT TESTING
	EXEC tdb_Warnings_Exceedances @AccountID= 24800
*/
BEGIN TRY

	/*
		-- SETUP REQUIRED
		DROP TABLE EMD_WarningsReportTable 
		CREATE TABLE EMD_WarningsReportTable 
			(ID int IDENTITY, LimitType varchar(10), TableID int, TableName varchar(200), FieldName varchar(200), SampleSiteColumnID int, SampleSiteID int, SampleSite varchar(200), Warning varchar(200), Exceedance varchar(200))
	*/

	TRUNCATE TABLE EMD_WarningsReportTable
	INSERT INTO EMD_WarningsReportTable (LimitType, TableID, TableName, FieldName, Warning, Exceedance)
		SELECT 'Base'
			,T.TableID 
			,T.TableName  
			,C.DisplayName 
			,C.ValidationOnWarning
			,C.ValidationOnExceedance
			FROM [Column] C
			JOIN [Table] T ON T.TableID = C.TableID 
			WHERE T.AccountID = @AccountID
			AND (C.ValidationOnWarning IS NOT NULL
			OR C.ValidationOnExceedance IS NOT NULL)
			AND T.IsActive=1
	 INSERT INTO EMD_WarningsReportTable (LimitType, TableID, TableName, FieldName, SampleSiteColumnID, SampleSiteID, Warning, Exceedance)
		SELECT 'Special'
			,T.TableID 
			,T.TableName  
			,Col.DisplayName 
			,Con.[CheckColumnID]
			,Con.CheckValue 
			--,Con.CheckFormula as ValidationOnWarning
			--,Col.ValidationOnExceedance
			,Col.ValidationOnWarning
			,Con.CheckFormula as ValidationOnExceedance
			FROM [Condition] Con
			JOIN [Column] Col ON Col.ColumnID  = Con.ColumnID
			JOIN [Table] T ON T.TableID = Col.TableID 
			WHERE T.AccountID = @AccountID
			AND Con.ConditionType = 'E'
			AND T.IsActive=1
	 INSERT INTO EMD_WarningsReportTable (LimitType, TableID, TableName, FieldName, SampleSiteColumnID, SampleSiteID, Warning, Exceedance)
		SELECT 'Special'
			,T.TableID 
			,T.TableName  
			,Col.DisplayName 
			,Con.[CheckColumnID]
			,Con.CheckValue 
			,Con.CheckFormula as ValidationOnWarning
			,Col.ValidationOnExceedance
			FROM [Condition] Con
			JOIN [Column] Col ON Col.ColumnID  = Con.ColumnID
			JOIN [Table] T ON T.TableID = Col.TableID
			JOIN [Column] CheckCol ON Con.CheckColumnID = CheckCol.ColumnID
			WHERE T.AccountID = @AccountID
			AND Con.ConditionType = 'W'
			AND T.IsActive=1
	-----------------------------------
	-- set the sample sites
	-----------------------------------

	DECLARE @sSQL varchar(8000) = ''
	DECLARE @Counter int = 1

	WHILE EXISTS(SELECT * FROM EMD_WarningsReportTable WHERE [ID] >= @Counter)
	BEGIN
		PRINT @Counter

		SELECT @sSQL = 'UPDATE EMD_WarningsReportTable SET SampleSite = R.' + SC.[SystemName] + ' 
		FROM EMD_WarningsReportTable 
		JOIN [Record] R ON [RecordID] = ' + cast(EMD_WarningsReportTable.SampleSiteID as varchar(10)) + '
		WHERE [ID] = ' + CAST(@Counter AS varchar(10))
			 FROM EMD_WarningsReportTable 
			 JOIN [Column] C ON C.ColumnID = EMD_WarningsReportTable.SampleSiteColumnID
			 JOIN [Column] SC ON C.TableTableID = SC.TableID AND SC.DisplayName='Site Name'
			 WHERE EMD_WarningsReportTable.[ID] = @Counter

		EXEC (@sSQL)
		SET @Counter = @Counter + 1
	END 

	SELECT TableID, TableName, FieldName, SampleSite, MAX(Warning) as Warning, MAX(Exceedance) as Exceedance
		FROM EMD_WarningsReportTable
		GROUP BY TableName, TableID, FieldName, SampleSite, LimitType
		ORDER BY TableName, TableID, FieldName, SampleSite, LimitType

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('tdb_Warnings_Exceedances', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
