﻿CREATE procedure [dbo].[usr_rptEmpChargeability]

(
 @Group  varchar(max) =null,
 @Employee varchar(max)=null,
 @startdate date =null,
 @enddate date =null

)
as
  exec usr_rpt_EmployeeHistroy
    
       --& "([GUID], EmployeeID, EmployeeName, GroupDivision, TimeSheetDollar, TotalHrWorked) " _
       --                                 & "Values ('" & GUID & "', " & rs("Employee") & ", '" & rs("Name") & "', '" _
       --                                 & rs("GroupDivision") & "', " & rs("TimeSheetDollar") & ", " & rs("TotalHrsWorked") & ")")
    
    
    
    --SELECT EmployeeHistroy.EmployeeID AS Employee, EmployeeHistroy.[Group/Division] AS GroupDivision, "
    --sQuery = sQuery & "Sum(TimeSheet.Hours) AS TotalHrsScheduledLeave, EmployeeHistroy.Name "
    --sQuery = sQuery & "FROM "
   
    
    
    SELECT  EmployeeHistroy.EmployeeID AS Employee, EmployeeHistroy.[Group/Division] AS GroupDivision, 
    sum(case when (cast (Administration.Code_Status as int)) ='2' then (cast(TimeSheet.Hours as money)) else 0 end ) as TotalHrsScheduledLeave,
    sum(case when (cast( Administration.Code_Status as int)) ='3' then (cast(TimeSheet.Hours as money))  else 0 end) as TotalHrsOtherLeave, 
    Sum(cast(TimeSheet.[$_Amount] as money)) AS AdminTimeSheetDollar,
  EmployeeHistroy.Name,  Sum(cast([TimeSheet].[Total] as money)) AS TimeSheetDollar, Sum(cast(TimeSheet.Hours as money)) AS TotalHrsWorked 
    
    into #tempdata 
    FROM 
    (Account24918.vTimeSheet TimeSheet INNER JOIN ((Account24918.vPNEmployeeIDTimeSheetID PNEmployeeIDTimeSheetID INNER JOIN ##EmployeeHistroy qryEmployeeHistory ON 
    PNEmployeeIDTimeSheetID.EmployeeID = qryEmployeeHistory.EmployeeID) INNER JOIN 
   Account24918.vEmployeeHistroy EmployeeHistroy ON (qryEmployeeHistory.EmployeeID = EmployeeHistroy.EmployeeID) 
    AND (qryEmployeeHistory.MaxOfDate = EmployeeHistroy.Date)) ON TimeSheet.TimeSheetID = PNEmployeeIDTimeSheetID.TimeSheetID) 
     LEFT JOIN Account24918.vProject Project ON PNEmployeeIDTimeSheetID.ProjectNumber = Project.ProjectNumber 
     left JOIN Account24918.vAdministration Administration ON PNEmployeeIDTimeSheetID.ProjectNumber = Administration.Code
    WHERE 
    (((cast(TimeSheet.Date as date)) >=@startdate  AND  (TimeSheet.Date) <= @enddate)
	and
 (
((@Group = 'all')  or  (EmployeeHistroy.[Group/division] =@Group))
and
((@Employee = 'all')  or  (EmployeeHistroy.EmployeeID  =@Employee)))
	
	
	)
    
    --If Forms!frmemployeebilling.fraOptions = 2 Then
    --    sQuery = sQuery & "AND EmployeeHistroy.[Group/Division]='" & Forms!frmemployeebilling.cboGroupNames & "' "
    --ElseIf Forms!frmemployeebilling.fraOptions = 3 Then
    --    sQuery = sQuery & "AND EmployeeHistroy.Name='" & Forms!frmemployeebilling.cboController & "' "
    --End If
   
    
    GROUP BY
    EmployeeHistroy.[Group/Division], EmployeeHistroy.Name, EmployeeHistroy.EmployeeID 
    
       select 
       
         Employee, 
         GroupDivision, 
         TotalHrsScheduledLeave,
        TotalHrsOtherLeave,
         AdminTimeSheetDollar,
  Name,  
  TimeSheetDollar, 
   TotalHrsWorked -(TotalHrsScheduledLeave + TotalHrsOtherLeave)
        
       
       
        from #tempdata
        order by name