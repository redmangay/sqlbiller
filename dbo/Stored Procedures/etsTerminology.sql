﻿

CREATE PROCEDURE [dbo].[etsTerminology]

(

      @nAccountID int=NULL,

      @sPageName varchar(300)=NULL,

      @sInputText varchar(MAX)

)

AS

BEGIN TRY

      DECLARE @sOutputText varchar(MAX)

      SELECT @sOutputText = OutputText FROM Terminology

            WHERE AccountID = @nAccountID

            AND PageName = @sPageName

            AND InputText = @sInputText

 

      

      
      IF @sOutputText IS NULL

            SELECT @sOutputText = OutputText FROM Terminology

                  WHERE AccountID = @nAccountID

                  AND PageName IS NULL

                  AND InputText = @sInputText
                  
      IF @sOutputText IS NULL

            SELECT @sOutputText = OutputText FROM Terminology

                  WHERE AccountID IS NULL

                  AND PageName = @sPageName

                  AND InputText = @sInputText
                

      IF @sOutputText IS NULL

            SELECT @sOutputText = OutputText FROM Terminology

                  WHERE AccountID IS NULL

                  AND PageName IS NULL

                  AND InputText = @sInputText

 

      SELECT @sOutputText

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('etsTerminology', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
