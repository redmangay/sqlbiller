﻿--sp_helptext 'dbo.rptTranscationDetails'

    
CREATE PROCEDURE [dbo].[rptTranscationDetails]  
 @StartDate DateTime = null,  
 @EndDate DateTime = null  
AS  
BEGIN  
 IF OBJECT_ID('tempdb..#tempTableForDateRange') IS NOT NULL  
begin  
        drop table #tempTableForDateRange  
end  
  
SELECT CONVERT(Datetime, b.[Date], 103)  as [Date]  , b.[Project Number], b.Description, b.[Payment Amount Converted],  
 b.[Invoice Amount Converted] AS ['Invoice Amount Converted'], CurrencyRate,   
 b.[Debtors Converted] AS ['Debtors Converted'], b.[Bad Debt Exp Converted], b.Type,  
   p.RptCurrencyRate, b.[Rpt Debtors Converted] AS ['Rpt Debtors Converted'],    
  p.Currency, b.ID, b.[Group Currency] AS Group_Currency,(Select Symbol from [thedatabase_windtech].   
[Account24918].[vCurrency Rates] where Currency=b.[Group Currency]) AS CurrencySymbol  
  into #tempTableForDateRange  
   
FROM [thedatabase_windtech].[Account24918].[vBilling Information] AS b,   
[thedatabase_windtech].[Account24918].[vProject] AS p  
Where (b.[Project Number]=p.ProjectNumber) AND (b.Type<>7)   
  
Select * from #tempTableForDateRange  
Where (Date BETWEEN @StartDate And @EndDate )   
  
ORDER BY Date, [Project Number]  
END  
  
