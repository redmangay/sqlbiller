﻿
CREATE PROCEDURE [dbo].[Location_GetByTable]

      @nTableID int

AS

BEGIN TRY

      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.

    SET NOCOUNT ON;

    SELECT  Location.*, [Table].PinImage
      FROM  Location
      JOIN  LocationTable ON (Location.LocationID = LocationTable.LocationID)
      JOIN  [Table] on [Table].TableID = LocationTable.TableID
      WHERE LocationTable.TableID = @nTableID
     

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Location_GetByTable', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
