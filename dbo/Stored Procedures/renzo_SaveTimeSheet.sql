﻿CREATE PROCEDURE		[dbo].[renzo_SaveTimeSheet]
(
	@RecordID  int,
	@FormSetID int = null,
	@UserID int=null,
	@Return varchar(max) output,
	@Context varchar(max) = null
)
AS
BEGIN TRY

/*
		Set/update the rate based on the designation of the employee and the rate against that designation on the jobb

		DECLARE			@Return		VARCHAR(MAX)
		EXEC			[renzo_AddDesignatedRate] @RecordID = 1754626, @Return = @Return



		exec [dbo].[renzo_SaveTimeSheet] @RecordID =   2712146, @Return = ''
*/

DECLARE			@Employee		INT
				,@Job			INT
				,@Designation	VARCHAR(MAX)
				,@Rate			VARCHAR(MAX)
				,@CurrencyRate	varchar(max)
				,@TaxRate varchar(max)
				,@IsAdmin varchar(10)

--		Get the job from the timesheet record

SELECT			@Job = [Record].[V018]				
FROM			[Record]
WHERE			[Record].TableID = 2488	-- Timesheet Detail table
AND				ISNUMERIC( [Record].[V018] ) = 1
AND				[Record].IsActive = 1
AND				[Record].RecordID = @RecordID


--		Get the Employee and IsAdmin from the timesheet record

SELECT			@Employee = [Record].[V017], @IsAdmin = V041		
FROM			[Record]
WHERE			[Record].TableID = 2488	-- Timesheet Detail table
AND				ISNUMERIC( [Record].[V017] ) = 1
AND				[Record].IsActive = 1
AND				[Record].RecordID = @RecordID


--		Get the Designation from the Employee List

SELECT			@Designation = [Record].[V021]
FROM			[Record]
WHERE			[Record].TableID = 2669 -- Employee List table
AND				[Record].[RecordID] = @Employee
AND				ISNUMERIC( [Record].[V021] ) = 1
AND				[Record].[IsActive] = 1
ORDER BY		[Record].[V003]


-- Red: lets get the currency rate

SELECT  @CurrencyRate = V007,
	   @TaxRate = (V011 / 100.00) FROM [Record]
WHERE RECORDID = @Job



--		Use the Job and Designation info to get the Rate stored for that designation against the Job
IF (@IsAdmin = '2')
BEGIN
SELECT			TOP 1 @Rate = REPLACE( [Record].[V002], '$', '' )
FROM			[Record]
WHERE			[Record].TableID = 2477 -- Designation table
AND				[Record].[V003] = CAST( @Job AS VARCHAR(MAX) )
AND				LTRIM(RTRIM([Record].[V005])) = @Designation
AND				[Record].[IsActive] = 1
ORDER BY		[Record].[V003]
END
IF (@IsAdmin = '1')
BEGIN

SELECT @Rate = DesigRate.V002
	   FROM Record EmpList
		  JOIN Record DesigRate ON DesigRate.TableID = 2712
		  AND DesigRate.IsActive = 1
		  AND DesigRate.v004 = '1'
		  AND DesigRate.V003 = CAST(EmpList.V021 as int)
	 WHERE EmpList.TableID = 2669
	   AND EmpList.IsActive = 1
	   AND EmpList.RecordID =  @Employee

END

PRINT			'@Employee ' + CAST( @Employee AS VARCHAR(MAX) )
PRINT			'@Job ' + CAST( @Job AS VARCHAR(MAX) ) 
PRINT			'@Designation ' + CAST( @Designation AS VARCHAR(MAX) ) 
PRINT			'@Rate ' + CAST( @Rate AS VARCHAR(MAX) ) 


/* Lets get the Entered By */

UPDATE [Record]
SET V036 = EnteredBy
WHERE [Record].[RecordID] = @RecordID



/* Update start and end time */

UPDATE Record
SET V013 = NULL, V014 = NULL, V044 = '1'
WHERE RecordID = @RecordID
AND ((V044 IS NULL) OR (V044 = '1'))


--		Upadte the Rate for the Timesheet entry to the rate stored against the job.

UPDATE			[Record]
SET				[V009] = ISNULL( @Rate, 0 )
				,[V035] = Round(CAST(ISNULL( @Rate, 0 ) AS DEC(10,2)) * cast(ISNULL(@CurrencyRate,1) AS DEC(10,2)),2)
WHERE			[Record].[RecordID] = @RecordID



--		Upadte the Amount for the Timesheet entry to the rate stored against the job.

UPDATE			[Record]
SET				V019 = CAST( ( CAST( [V009] AS DEC(10,2) ) * cast(isnull(@CurrencyRate,1) AS DEC(10,2))) * CAST( V015 AS DEC(10,2) ) * CAST( V007 AS DEC(10,2) ) AS VARCHAR(MAX) )
WHERE			[Record].[RecordID] = @RecordID
AND				ISNUMERIC( [V009] ) = 1
AND				ISNUMERIC( V015 ) = 1 
AND				ISNUMERIC( V007 ) = 1 

/*	Insert/update the Timesheet Detail entry	*/

/*	SELECT [Timesheet Details] */

IF EXISTS	(	SELECT			[Timesheet Details].RecordID	AS				[Record ID]
					FROM		[Record] [Timesheet Details]
					WHERE		[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
					AND			[Timesheet Details].IsActive = 1
					AND			[Timesheet Details].V011 = CAST( @RecordID AS VARCHAR(MAX))			/*	[Timesheet]	*/
			)
BEGIN

	/*	UPDATE [Timesheet Details] */
	print 'exists'
	PRINT @CurrencyRate
	UPDATE			[Timesheet Details] 			/*	[Timesheet Details]	*/
		SET			DateTimeRecorded = [Timesheet].DateTimeRecorded
					,EnteredBy = [Timesheet].EnteredBy
					,IsActive = [Timesheet].IsActive
					,DateUpdated = [Timesheet].DateUpdated
					,V001 = [Timesheet].V018				/*	[Job No]	*/
					,V009 = [Timesheet].V004				/*	[Date]	*/
					,V002 = [Timesheet].V017				/*	[Initials]	*/
					,V003 = [Timesheet].V006				/*	[Details]	*/
					,V004 = [Timesheet].V007				/*	[(X)]	*/
					,V005 = [Timesheet].V015				/*	[Hr/Unit]	*/
					,V006 = Round(cast([Timesheet].V009 AS DEC(10,2)) * cast(isnull(@CurrencyRate,1) AS DEC(10,2)),2)
					--,V006 = [Timesheet].V009				/*	[Rate]	*/
					--,V012 = [Timesheet].V018				/*	[GST]	*/
					--,V007 = Round(cast([Timesheet].V019 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)),2)				/*	[$ Amount]	*/
					,V007 = Round(cast([Timesheet].V009 AS money) * cast(isnull(@CurrencyRate,1) AS DEC(10,2)) * cast([Timesheet].V007 AS DEC(10,2))	* cast([Timesheet].V015 AS DEC(10,2)),2)			/*	[$ Amount]	*/	
					,V008 = 'By Rate'						/*	[Status]	*/
					--,V010 = [Timesheet].V018				/*	[Disbursement]	*/
					,V011 = [Timesheet].RecordID			/*	[Timesheet]	*/
					,V022 = '1'
					-- Dummy
					,V028 = [Timesheet].V007
					,V029 = [Timesheet].V015	
					,V030 = Round(cast([Timesheet].V009 AS DEC(10,2)) * cast(isnull(@CurrencyRate,1) AS DEC(10,2)),2)
					,V031 = Round(cast([Timesheet].V009 AS money) * cast(isnull(@CurrencyRate,1) AS DEC(10,2)) * cast([Timesheet].V007 AS DEC(10,2))	* cast([Timesheet].V015 AS DEC(10,2)),2)			/*	[$ Amount]	*/	
					,[V032] = 'By Rate'
					,[V033] = [Timesheet].V018
					,[IsReadOnly] = 1
					,V040 = cast([Timesheet].V009 AS DEC(10,2)) 
		FROM		[Record] [Timesheet Details]
		JOIN		Record [Timesheet] ON				[Timesheet].TableID = '2488'		/*	Timesheet	*/
												AND		[Timesheet].IsActive = 1
												AND		CAST( [Timesheet].RecordID AS VARCHAR(MAX)) = [Timesheet Details].V011		/*	V011	*/
												AND		[Timesheet].RecordID = @RecordID
		WHERE		[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
		AND			[Timesheet Details].IsActive = 1
	
END
ELSE
BEGIN
print 'not exists'
	/*	Insert [Timesheet Details]	*/

	INSERT INTO		[Record] 			/*	[Timesheet Details]	*/
		(			[TableID]
					,DateTimeRecorded
					,EnteredBy
					,IsActive
					,DateUpdated
					,V001				/*	[Job No]	*/
					,V009				/*	[Date]	*/
					,V002				/*	[Initials]	*/
					,V003				/*	[Details]	*/
					,V004				/*	[(X)]	*/
					,V005				/*	[Hr/Unit]	*/
					,V006				/*	[Rate]	*/
					,V007				/*	[$ Amount]	*/
					,V008				/*	[Status]	*/
					,V011				/*	[Timesheet]	*/
					,DateAdded
					,V022
					-- Dummy
					,V028  
					,V029  
					,V030    
					,V031
					,[V032]  
					,[V033]  
					,[IsReadOnly] 
					,V040
		)
	SELECT			'2729'		/*	Timesheet Details	*/
					,[Timesheet].DateTimeRecorded
					,[Timesheet].EnteredBy
					,[Timesheet].IsActive
					,[Timesheet].DateUpdated
					,[Timesheet].V018		AS				[Job No]
					,[Timesheet].V004		AS				[Date]
					,[Timesheet].V017		AS				[Employee]
					,[Timesheet].V006		AS				[Details]
					,[Timesheet].V007		AS				[(X)]
					,[Timesheet].V015		AS				[Unit]
					,Round(cast([Timesheet].V009 AS DEC(10,2)) * cast(isnull(@CurrencyRate,1) AS DEC(10,2)),2) AS [Rate]
					--,[Timesheet].V009		AS				[Rate]
					--,Round(cast([Timesheet].V019 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)),2)				/*	[$ Amount]	*/		AS				[Amount]
					,Round(cast([Timesheet].V009 AS money) * cast(isnull(@CurrencyRate,1) AS DEC(10,2)) * cast([Timesheet].V007 AS DEC(10,2))	* cast([Timesheet].V015 AS DEC(10,2)),2)			/*	[$ Amount]	*/		AS				[Amount]
					,'By Rate'				AS				[Status]
					,[Timesheet].RecordID	AS				[Timesheet]
					,[Timesheet].DateAdded
					,'1'
					,[Timesheet].V007		AS				[(X)]
					,[Timesheet].V015		AS				[Unit]
					,Round(cast([Timesheet].V009 AS DEC(10,2)) * cast(isnull(@CurrencyRate,1) AS DEC(10,2)),2) AS [Rate]
					,Round(cast([Timesheet].V009 AS money) * cast(isnull(@CurrencyRate,1) AS DEC(10,2)) * cast([Timesheet].V007 AS DEC(10,2))	* cast([Timesheet].V015 AS DEC(10,2)),2)			/*	[$ Amount]	*/		AS				[Amount]
					,'By Rate'
					,[Timesheet].V018		AS				[Job No]
					,1
					,cast([Timesheet].V009 AS money) 
		FROM		[Record] [Timesheet]
		WHERE		[Timesheet].TableID = '2488'		/*	Timesheet	*/
		AND			[Timesheet].IsActive = 1
		AND			[Timesheet].V041 = '2' /* Is Administration = Yes */
		AND			[Timesheet].RecordID = @RecordID;
END
/*	Update the GST */

UPDATE			[Timesheet Details] 									/*	[Timesheet Details]	*/
--	SET			V012 = CAST( CAST( REPLACE([Timesheet Details].V007, ',', '')  AS DEC(20,4) ) / 10 AS VARCHAR(MAX) )				/*	[GST]	*/
SET			V012 = CAST( CAST( REPLACE([Timesheet Details].V007, ',', '')  AS DEC(20,4) ) * @TaxRate AS VARCHAR(MAX) )				/*	[GST]	*/
--SELECT			[Timesheet Details].V007
	FROM		[Record] [Timesheet Details]
	WHERE		[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
	AND			[Timesheet Details].IsActive = 1
	AND			ISNUMERIC( ISNULL( [Timesheet Details].V007, 0 ) ) = 1
	AND			[Timesheet Details].V011 = CAST( @RecordID AS VARCHAR(MAX));



/* ================================================================================================ Save the Admin Code or Job No == */
DECLARE @AdminOrJob VARCHAR(10)
DECLARE @AdminRecordID int
DECLARE @AdminDescription VARCHAR(MAX)
DECLARE @JobNoRecordID varchar(max)
DECLARE @JobNo varchar(max)

SELECT @AdminOrJob = V041, @AdminRecordID = V042, @JobNoRecordID = V018  FROM Record
    WHERE RecordID = @RecordID

IF @AdminOrJob = 1
BEGIN
    SELECT @AdminDescription= V001 FROM Record
	   WHERE RecordID = @AdminRecordID

    UPDATE Record
    SET V043 = @AdminDescription,
	   V018 = NULL /* reset the Job No */
    WHERE RecordID = @RecordID
END

IF @AdminOrJob = 2
BEGIN
    SELECT @JobNo= V001 FROM Record
	   WHERE RecordID = @JobNoRecordID

    UPDATE Record
    SET V043 = @JobNo,
	   V042 = NULL /* reset the Admin Code */
    WHERE RecordID = @RecordID
END


/* ===== ============= Update existing data == */

--UPDATE [Time Sheet]
--SET [Time Sheet].V043 = [Job Info].V001
--FROM Record [Time Sheet]
--JOIN Record [Job Info] ON [Job Info].TableID = 2475
--AND [Job Info].RecordID =  CAST([Time Sheet].V018 as int)
--WHERE [Time Sheet].TableID = 2488
--AND [Time Sheet].IsActive = 1

/* ===== ============= Update existing data == */


/* ================================================================================================ End Save the Admin Code or Job No == */
	
RETURN @@ERROR;
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('renzo_SaveTimeSheet', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH