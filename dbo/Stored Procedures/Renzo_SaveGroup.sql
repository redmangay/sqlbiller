﻿CREATE PROCEDURE [dbo].[Renzo_SaveGroup]
(
	@RecordID  int,		/*	Record ID passed from the table on execute, base your solution on this value	*/
	@UserID int,
	@Return varchar(max) output
	, @Context varchar(max) = null
)
AS

/*

EXEC Renzo_SaveGroup @RecordID = '2717569',@UserID = '25197',@Return=''

*/
BEGIN

UPDATE [Group]
SET [Group].V004 = [Currency Rate].V001 /* we need [Group].V004 because it is being used everywhere */
FROM [Record] [Group]
INNER JOIN [Record] [Currency Rate] ON [Currency Rate].TableID = 2665
	   AND CAST([Currency Rate].RecordID AS VARCHAR) = [Group].V005
WHERE [Group].TableID = 2727
    AND [Group].IsActive = 1
    AND [Group].RecordID =  @RecordID


/* Lets add the new group in the document layouts */

IF NOT EXISTS(SELECT RecordID 
			 FROM [Record] 
				WHERE TableID = 2744
				    AND V005 = CAST(@RecordID	AS VARCHAR))
BEGIN

/* Lets Insert the heck */
INSERT INTO Record (
					 [TableID]
					,[DateTimeRecorded]
					,[Notes]
					,[EnteredBy]
					,[IsActive]
					,[TempRecordID]
					,[ValidationResults]
					,[LastUpdatedUserID]
					,[WarningResults]
					,[ChangeReason]
					,[OwnerUserID]
					,[ReminderSentDate]
					,[V005] /* Linked RecID */
					,[V025] /* Group Name */
)
SELECT				 2744 /* Document Layouts */
					,[DateTimeRecorded]
					,[Notes]
					,[EnteredBy]
					,[IsActive]
					,[TempRecordID]
					,[ValidationResults]
					,[LastUpdatedUserID]
					,[WarningResults]
					,[ChangeReason]
					,[OwnerUserID]
					,[ReminderSentDate]
					,[RecordID]
					,[V001]
	FROM				[Record]
	WHERE			[Record].TableID = 2727
	AND				[IsActive] = 1
	AND				[Record].[RecordID] = @RecordID


/* Lets Update the heck; layouts to default */

UPDATE		[Documment Layouts]	
SET			[Documment Layouts].V002 = [Documment Layouts 2].V002,
			[Documment Layouts].V004 = [Documment Layouts 2].V004,
			[Documment Layouts].V007 = [Documment Layouts 2].V007,
			[Documment Layouts].V009 = [Documment Layouts 2].V009,
			[Documment Layouts].V011 = [Documment Layouts 2].V011,
			[Documment Layouts].V013 = [Documment Layouts 2].V013,
			[Documment Layouts].V015 = [Documment Layouts 2].V015,
			[Documment Layouts].V017 = [Documment Layouts 2].V017,
			[Documment Layouts].V019 = [Documment Layouts 2].V019,
			[Documment Layouts].V021 = [Documment Layouts 2].V021,
			[Documment Layouts].[LastUpdatedUserID] = @UserID,
			[Documment Layouts].[DateUpdated] = getdate()
FROM		[Record] [Documment Layouts] 
JOIN	    [Record] [Documment Layouts 2]	ON				1=1
								AND		[Documment Layouts 2].RecordID  = '2717618'	--	Default layouts settings	
								AND		[Documment Layouts 2].TableID	  = '2744'		 
								AND		[Documment Layouts 2].IsActive  = 1										
	
WHERE		 1=1
AND		[Documment Layouts].V005 = CAST(@RecordID AS VARCHAR)
AND		[Documment Layouts].TableID = '2744'		/*	Document Layouts	*/
AND		[Documment Layouts].IsActive = 1



END

/* Update the group status in default layouts */
IF EXISTS(SELECT RecordID 
			 FROM [Record] 
				WHERE TableID = 2744
				    AND V005 = CAST(@RecordID	AS VARCHAR))
BEGIN

DECLARE @GroupStatus varchar(max)

SELECT @GroupStatus = V007 FROM [Record] WHERE RecordId = @RecordID

UPDATE[RECORD]
SET V026 = @GroupStatus
WHERE TableID = 2744
AND IsActive = 1
AND V005 = CAST(@RecordID AS VARCHAR)

END




	RETURN @@ERROR;
END;
