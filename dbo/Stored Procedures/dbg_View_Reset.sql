﻿

CREATE PROCEDURE   [dbo].[dbg_View_Reset]
(
	@rsViewID INT,
	@ViewID INT OUTPUT
)
AS

BEGIN TRY
			
			DECLARE @AccountID INT,@UserID INT,@ViewPageType varchar(10),@TableID INT,@ParentTableID INT,@TableChildID INT,@DisplayInColumnID INT
			-- IF @UserID is account holder or NULL it is ok, it does not matter

			SELECT  @AccountID=T.AccountID,@TableID=V.TableID,@ViewPageType=V.ViewPageType,
			@UserID=V.UserID,@ParentTableID=V.ParentTableID,@TableChildID=TableChildID,@DisplayInColumnID=DisplayInColumnID 
			FROM [View] V INNER JOIN [Table] T ON V.TableID=T.TableID WHERE V.ViewID=@rsViewID

					
			-- DELETE OLD View
			EXEC [dbo].[dbg_View_Delete] @rsViewID
			
			-- Now get GET/CREATE Default View

			DECLARE @AccHolUserID INT =[dbo].[fnGetAccountHolderUserID] (@AccountID)	--Account Holder
			DECLARE @bfUserID INT=@UserID
			IF @bfUserID IS NULL
				SET @bfUserID=@AccHolUserID  -- IF @UserID is NULL then get account holder view for this NULL

			DECLARE @bfViewID INT
			EXEC [dbo].[dbg_View_BestFittingNew] @ViewPageType,@bfUserID,@TableID,@ParentTableID,@TableChildID,@bfViewID OUT,@DisplayInColumnID

			DECLARE @bNeedCopy bit=0

			IF @UserID IS  NULL
				SET @bNeedCopy=1

			-- Now lets copy

			IF @bNeedCopy=1
				EXEC [dbo].[dbg_View_Copy] @bfViewID,@UserID,@ViewID OUT
			ELSE 
				SET @ViewID=@bfViewID

							
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_View_Reset', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
