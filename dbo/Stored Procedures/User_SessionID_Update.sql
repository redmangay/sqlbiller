﻿

CREATE PROCEDURE [dbo].[User_SessionID_Update]
(
	@nUserID int,
	@sSessionID varchar(100),@nAccountID INT
)
AS
/*

*/
BEGIN TRY
	UPDATE  [UserRole]
	SET SessionID=@sSessionID
	WHERE     (UserID = @nUserID AND AccountID=@nAccountID)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('User_SessionID_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
