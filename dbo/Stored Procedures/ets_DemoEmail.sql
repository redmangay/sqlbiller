﻿

CREATE PROCEDURE  [dbo].[ets_DemoEmail] (
	@sKey varchar(10) = NULL
	,@sRoot varchar(200) = NULL
	)
AS
/*

 

      UNIT TESTING

 

      ============

 

     EXEC [ets_DemoEmail] @sKey=344, @sRoot='www.x.com'

 

*/
BEGIN TRY
	DECLARE @nAccountID INT

	IF @sKey IS NULL
	BEGIN
		PRINT 'NULL'

		SELECT '' TableList
			,'' URL
			,'' AccountName
			,'' FirstName
			,'' LastName
			,'' PhoneNumber
			,'' Email
	END
	ELSE
	BEGIN
		SELECT @nAccountID = CAST(@sKey AS INT)

		DECLARE @sEmail varchar(200)
		DECLARE @sPassWord varchar(100)

		PRINT '@nAccountID = ' + CAST(@nAccountID AS varchar)

		DECLARE @sTableList varchar(MAX) = '<ul>'

		-- New bit
		SELECT @sTableList = @sTableList + '<li>' + TableName + '</li>'
		FROM [Table] INNER JOIN Menu ON [Table].TableID=Menu.TableID
		WHERE [Table].IsActive = 1
			AND [Table].AccountID = @nAccountID
		ORDER BY ParentMenuID
			,TableName;

		SET @sTableList = @sTableList + '</ul>'

		SELECT A.AccountName
			,U.FirstName
			,U.LastName
			,U.PhoneNumber
			,U.Email
			,@sTableList AS TableList
			--,@sRoot + '/Login.aspx?Email=' + U.Email + '&Password=' + U.[PassWord] + '&RememberMe=Yes' AS URL -- Red removed this for Ticket 3059
			,@sRoot + '/Login.aspx?Email=' + U.Email + '&RememberMe=Yes' AS URL
			
		 FROM [Account] A
			INNER JOIN UserRole  UR ON  A.AccountID=UR.AccountID
			INNER JOIN [User] U ON U.UserID = UR.UserID AND UR.IsAccountHolder = 1
            WHERE A.AccountID = @nAccountID
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_DemoEmail', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

