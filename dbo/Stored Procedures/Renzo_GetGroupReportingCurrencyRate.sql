﻿-- =============================================
-- Author:		<Red Mangay>
-- Create date:	<10/07/2018>
-- Description:	<Get the tableid of the orginal table ie timesheet or disbursement>
-- =============================================
/*

exec [Renzo_GetGroupReportingCurrencyRate] 1574923

*/


CREATE PROCEDURE [dbo].[Renzo_GetGroupReportingCurrencyRate]
(
	@RecordID int
	
)
AS
BEGIN			
   
    SELECT * FROM [Record] 
    WHERE ISACTIVE=1 AND RecordID IN (
    SELECT V005  FROM [Record] 
    WHERE ISACTIVE=1 AND 
    RecordID = @RecordID AND 
    V005 IS NOT NULL )


END

