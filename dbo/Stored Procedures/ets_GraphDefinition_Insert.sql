﻿

CREATE PROCEDURE [dbo].[ets_GraphDefinition_Insert]
(
	@nNewID int output,
	@nAccountID int = NULL,
	@sDefinitionName varchar(200) = NULL ,
	@sDefinition varchar(MAX) = NULL,
	@bIsSystem bit = NULL, 
	@bIsHidden bit = NULL,
	@nTableID int = NULL,
	@nColumnID int = NULL ,	
	@sDefinitionKey varchar(200) = NULL,
	@nDataColumn1ID int = NULL,
	@nDataColumn2ID int = NULL,
	@nDataColumn3ID int = NULL,
	@nDataColumn4ID int = NULL,
	@bIsSingleSeries bit = 0
)
	/*----------------------------------------------------------------
	-- Stored Procedure: ets_GraphDefinition_Insert
	---------------------------------------------------------------*/
 
AS

BEGIN TRY
	INSERT INTO [GraphDefinition] (
		AccountID
		,DefinitionName
		,[Definition]
		,IsSystem
		,IsHidden
		,TableID
		,ColumnID
		,DefinitionKey
		,IsActive
		,DataColumn1ID
		,DataColumn2ID
		,DataColumn3ID
		,DataColumn4ID
		,IsSingleSeries
	)
	VALUES (
		@nAccountID
		,@sDefinitionName
		,@sDefinition
		,@bIsSystem
		,@bIsHidden
		,@nTableID
		,@nColumnID
		,@sDefinitionKey
		,1
		,@nDataColumn1ID
		,@nDataColumn2ID
		,@nDataColumn3ID
		,@nDataColumn4ID
		,@bIsSingleSeries
	)
	SELECT @nNewID = @@IDENTITY

-- ===================================================
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_GraphDefinition_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
