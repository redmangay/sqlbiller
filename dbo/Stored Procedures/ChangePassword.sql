﻿

CREATE PROCEDURE [dbo].[ChangePassword]
(
	@OldPassword nvarchar(200),
	@NewPassword nvarchar(200),
	@nUserID int,
	@Result int output
)
AS

BEGIN TRY
	DECLARE @Salt VARBINARY(8) = CRYPT_GEN_RANDOM(8)

	SELECT @Result=Count (*) FROM  dbo.[user] WHERE  UserID=@nUserID AND [EncryptedPassword] = HASHBYTES('SHA2_512', @OldPassword + CAST([SaltPassword] AS NVARCHAR) )

	IF @Result = 1
	BEGIN
		UPDATE dbo.[user]
		SET 	[EncryptedPassword] = HASHBYTES('SHA2_512', @NewPassword + cast(@Salt as nvarchar)),
				[SaltPassword] = @Salt
		WHERE [UserID] = @nUserID
	END

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ChangePassword', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
