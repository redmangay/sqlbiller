﻿
 
CREATE PROCEDURE [dbo].[ets_GetValidClaibration]
(
	@nSensorID int ,
	@dRecordDateTime datetime
)

	/*----------------------------------------------------------------
	-- Stored Procedure: ets_Document_Delete
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SELECT CalibrationID 
		FROM Calibration 
		WHERE SensorID=@nSensorID AND (@dRecordDateTime >= CalibrationValidForm
		and @dRecordDateTime <= CalibrationValidTo) AND Calibration.IsActive=1
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_GetValidClaibration', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
