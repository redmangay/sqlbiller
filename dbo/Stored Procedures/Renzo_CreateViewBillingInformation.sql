﻿-- =============================================
-- Author:		Barry Matutina
-- Create date: 09/16/2016
-- Description:	Updates Job Number
-- =============================================

/*	sp for Save [View Calculation] */
CREATE PROCEDURE	[dbo].[Renzo_CreateViewBillingInformation]
(
	@RecordID  int = null,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output
)
AS
BEGIN
	DECLARE			@JOBVALUE		VARCHAR(MAX)
	DECLARE			@TAXVALUE		VARCHAR(MAX)

SELECT			@JOBVALUE =  [Bill & Payment Activity].V011						/*	[Job value]	*/
	FROM		[Record] [Bill & Payment Activity] WITH (NOLOCK)
	WHERE		[Bill & Payment Activity].TableID = '2486'						/*	Job Information	*/
	AND			[Bill & Payment Activity].IsActive = 1
	AND			[Bill & Payment Activity].RecordID = @RecordID

SELECT			@TAXVALUE = [Bill & Payment Activity].V015						/*	[Job value]	*/
	FROM		[Record] [Bill & Payment Activity] WITH (NOLOCK)
	WHERE		[Bill & Payment Activity].TableID = '2486'						/*	Job Information	*/
	AND			[Bill & Payment Activity].IsActive = 1
	AND			[Bill & Payment Activity].RecordID = @RecordID

/*	SELECT [View Report for Calculation] */

SELECT (CONVERT(DECIMAL(4,2),@JOBVALUE) * CONVERT(DECIMAL(4,2),@TAXVALUE)) 
	FROM		[Record] [Bill & Payment Activity] WITH (NOLOCK)
	WHERE		[Bill & Payment Activity].TableID = '2486'		/*	Bill & Payment Activity	*/
	AND			[Bill & Payment Activity].IsActive = 1
	AND			[Bill & Payment Activity].RecordID = @RecordID

END

/**************************************************/