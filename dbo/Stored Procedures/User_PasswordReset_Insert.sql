﻿


CREATE PROCEDURE [dbo].[User_PasswordReset_Insert]
(
	@nPasswordResetID int output,
	@nUserID int
)
AS

BEGIN TRY
	
	INSERT INTO [UserPasswordReset]
	(
		 [UserID]
		,[DateRequested]
		,[Expiration]
		,[IsActive]
	) 
	VALUES 
	(
		@nUserID
		,GETDATE()
		,DATEADD(HH,1, GETDATE())
		,1
	)
	SELECT @nPasswordResetID = @@IDENTITY
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('User_PasswordReset_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
