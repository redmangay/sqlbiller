﻿

CREATE PROCEDURE [dbo].[Record_Audit_Summary]

(
      @RecordID int,
      @nStartRow int = 1, 
	 @nMaxRows int = 20000000 
	

)

AS
--EXEC Record_Audit_Summary @RecordID=395263,@nStartRow=1,@nMaxRows=5
BEGIN TRY
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	--SET ROWCOUNT @nMaxRows

		 --changed by KG 23/8/2017
		 --SELECT * FROM ( SELECT [Audit].DateAdded,
		 --       [User].FirstName + ' ' + [User].LastName as [User],
		 --       dbo.fnListChanges(@RecordID, [Audit].DateAdded) as ColumnList,
		 --        dbo.fnListChanges_RecordCR(@RecordID, [Audit].DateAdded) as ResonForChange,
		 --       ROW_NUMBER() 
		 --       OVER(ORDER BY [Audit].DateAdded DESC) as RowNum 
		 --       FROM [Audit]
		 --       JOIN [User] ON [User].UserID = [Audit].UserID
		 --       WHERE TableName='Record'
		 --       AND FieldName != 'DateUpdated' AND FieldName != 'LastUpdatedUserID' 
		 --       AND PrimaryKeyValue = @RecordID AND dbo.fnListChanges(@RecordID, [Audit].DateAdded)!=''
		 --       GROUP BY [Audit].DateAdded,
		 --             [User].FirstName + ' ' + [User].LastName,
		 --             dbo.fnListChanges(@RecordID, [Audit].DateAdded),  dbo.fnListChanges_RecordCR(@RecordID, [Audit].DateAdded)
		 --             ) AS AuditInfo
		 --             WHERE RowNum>=@nStartRow AND RowNum<=(@nStartRow+@nMaxRows)
    
	
     SELECT * FROM ( SELECT [Audit].DateAdded,
            [User].FirstName + ' ' + [User].LastName as [User],
            dbo.fnListChanges(@RecordID, [Audit].DateAdded) as ColumnList,
            [Audit].[ChangeReason] as [ResonForChange],
            ROW_NUMBER() 
            OVER(ORDER BY [Audit].DateAdded DESC) as RowNum 
            FROM [Audit]
            JOIN [User] ON [User].UserID = [Audit].UserID
            WHERE TableName='Record'
            AND FieldName != 'DateUpdated' AND FieldName != 'LastUpdatedUserID' 
            AND PrimaryKeyValue = @RecordID AND dbo.fnListChanges(@RecordID, [Audit].DateAdded)!=''
            GROUP BY [Audit].DateAdded,
                  [User].FirstName + ' ' + [User].LastName,
                  dbo.fnListChanges(@RecordID, [Audit].DateAdded),  [ChangeReason]
                  ) AS AuditInfo
                  WHERE RowNum>=@nStartRow AND RowNum<=(@nStartRow+@nMaxRows)	              
            
            --now we need to get TotalRows number
            
            SELECT MAX(RowNum) AS TotalRows FROM ( SELECT [Audit].DateAdded,
            [User].FirstName + ' ' + [User].LastName as [User],
            dbo.fnListChanges(@RecordID, [Audit].DateAdded) as Change,ROW_NUMBER() 
            OVER(ORDER BY [Audit].DateAdded DESC) as RowNum 
            FROM [Audit]
            JOIN [User] ON [User].UserID = [Audit].UserID
            WHERE TableName = 'Record'
            AND FieldName != 'DateUpdated' AND FieldName != 'LastUpdatedUserID' 
            AND PrimaryKeyValue = @RecordID AND dbo.fnListChanges(@RecordID, [Audit].DateAdded)!=''
            GROUP BY [Audit].DateAdded,
                  [User].FirstName + ' ' + [User].LastName,
                  dbo.fnListChanges(@RecordID, [Audit].DateAdded)
                  ) AS AuditInfo

END TRY-- sp
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Record_Audit_Summary', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
