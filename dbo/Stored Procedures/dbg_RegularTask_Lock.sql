﻿CREATE PROCEDURE [dbo].[dbg_RegularTask_Lock] 
(
	@TaskID int
)
AS
/*	Modification History
	====================
	18 Feb 2019		JB changed SET [NextRun] = DATEADD(YEAR, 1, GETDATE()) to use DAY instead so if there is a problem it is
					reset again the next day - plenty of time for something else to finish. A year makes no sense.

*/
BEGIN TRY
	SET NOCOUNT ON;

	UPDATE [dbo].[RegularTask]
		SET [NextRun] = DATEADD(DAY, 1, GETDATE()) 
		WHERE [RegularTaskID] = @TaskID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_RegularTask_Lock', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
