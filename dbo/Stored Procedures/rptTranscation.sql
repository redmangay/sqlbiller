﻿--sp_helptext 'rptTranscation'

  
CREATE PROCEDURE [dbo].[rptTranscation]  
@StartDate DateTime = null  
AS  
BEGIN  
  
IF (SELECT object_id('TempDB..#tempTable1')) IS NOT NULL  
BEGIN  
    DROP TABLE #tempTable1  
END  
  
IF (SELECT object_id('TempDB..#tempTableGroup1')) IS NOT NULL  
BEGIN  
    DROP TABLE #tempTableGroup1  
END  
  
IF (SELECT object_id('TempDB..#tempTableSub')) IS NOT NULL  
BEGIN  
    DROP TABLE #tempTableSub  
END  
  
SELECT   CAST(ISNULL(b.[Debtors Converted], 0) AS MONEY)  
             AS Debtors,   
ISNULL(b.[Group Currency], '') AS [Group Currency],CONVERT(Datetime, b.[Date], 103)  as [Date] , b.[Project Number]  
into #tempTable1  
FROM          
    Account24918.[vBilling Information] AS b ,  
                         Account24918.vProject AS p   
WHERE        (b.[Project Number] IN  
                             (SELECT        ProjectNumber  
                               FROM            Account24918.vProject))   
                               AND b.[Project Number] = p.ProjectNumber  
ORDER BY b.Date, b.[Project Number]  
  
select SUM(Debtors) AS DBSum into #tempTableSub  
from #tempTable1   
  
select SUM(Debtors) AS DBSum into #tempTableGroup1   
from #tempTable1   
--where Date >= CONVERT(datetime ,@StartDate,103)
where Date >= @StartDate  
  
  
select (t1.DBSum-ISNULL(t2.DbSum,0)) As Begining_Balance   
FROM #tempTableSub t1 , #tempTableGroup1 t2  
  
END  