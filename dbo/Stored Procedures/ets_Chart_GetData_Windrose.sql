﻿

CREATE PROCEDURE [dbo].[ets_Chart_GetData_Windrose]
       -- Add the parameters for the stored procedure here
       @nTableID int,
	   @sGraphXAxisColumnID varchar(MAX),
       @sValueFields varchar(MAX),
       @dStartDate datetime,
       @dEndDate datetime,
       @sPeriod varchar(20), -- Possible values: Year, Month, Day, Hour
       -- When you pass Year it will return the month averages
       -- when you pass Month it will return the day averages
       -- when you pass Week it will return the day averages
       -- when you pass day it will return hour aveages
       -- when you pass hour it will return all data
	   @GraphSeriesColumnID varchar(MAX),
	   @GraphSeriesID varchar(MAX),
	   @nDataColumn1ID int = NULL,
	   @nDataColumn2ID int = NULL,
	   @nDataColumn3ID int = NULL,
	   @nDataColumn4ID int = NULL,
	   @ForceAverage bit,
       @bPercent bit= 0,
	   @nMaxRows int = NULL,
	   @sFilter varchar(MAX)
AS
/*
       PRINT SYSDATETIME()
       EXEC ets_Chart_GetData_New
              @nTableID=919
              ,@sValueFields = 'V002'
              ,@dStartDate='2012-12-07 12:28'
              ,@dEndDate='2013-12-09 16:00'
              ,@sPeriod='Hour'
              ,@bPercent=1,@nMaxRows=200
       PRINT SYSDATETIME()
       -- 3603515 1st run
       -- 2919922 2nd run : no IF
       -- 2529297 3rd run : using IN
*/

BEGIN TRY
    SET NOCOUNT ON;
    --SET dateformat dmy  
    DECLARE @sql nvarchar(MAX)       
	DECLARE @DirectionColumn varchar(MAX)
	DECLARE @ValueColumn varchar(MAX)
	DECLARE @WhereClause varchar(MAX)
	DECLARE @cnt int

	SELECT @DirectionColumn = SystemName FROM [Column] WHERE ColumnID = @nDataColumn1ID
	SELECT @ValueColumn = SystemName FROM [Column] WHERE ColumnID = @nDataColumn2ID

	DECLARE @bIgnoreSymbols bit=0
	SELECT @bIgnoreSymbols=IgnoreSymbols FROM [Column] WHERE SystemName=@sValueFields AND TableID=@nTableID AND ColumnType='number'

	IF (@bIgnoreSymbols IS NOT NULL AND @bIgnoreSymbols = 1)
		SET @sValueFields = 'dbo.RemoveNonNumericChar(' + @ValueColumn + ')'
	ELSE
		SET @sValueFields = @ValueColumn

	SET @WhereClause = ' WHERE IsActive = 1' +
			' AND CAST((SELECT dbo.[fnStringToDate](' + @sGraphXAxisColumnID + ')) AS datetime) >= ''' + CONVERT(varchar(20), @dStartDate) +
				''' AND  CAST((SELECT dbo.[fnStringToDate](' + @sGraphXAxisColumnID + ')) AS datetime) <= ''' + CONVERT(varchar(20), @dEndDate) + '''' +
			' AND TableID = ' + CAST(@nTableID AS Varchar) +
			CASE
				WHEN @GraphSeriesColumnID IS NULL THEN ''
				WHEN @GraphSeriesID IS NULL THEN ''
				WHEN LEN(@GraphSeriesColumnID) = 0 THEN ''
				WHEN LEN(@GraphSeriesID) = 0 THEN ''
				ELSE ' AND [' + @GraphSeriesColumnID + '] = ''' + @GraphSeriesID + ''''
			END +
			' AND ' + @ValueColumn + ' IS NOT NULL AND ' + @DirectionColumn + ' IS NOT NULL'

	SET @sql = 'SELECT @cntOut = COUNT(*) FROM [Record] ' + @WhereClause
	EXEC sp_executesql @sql, N'@cntOut int out', @cntOut = @cnt out
	IF @cnt = 0 SET @cnt = 1
	PRINT @cnt

    SET @sql = 'SELECT Direction,' +
			' [0] * 100.0 / ' + CAST(@cnt AS varchar(20)) + ' AS [&lt;0.5 m/s],' +
			' [1] * 100.0 / ' + CAST(@cnt AS varchar(20)) + ' AS [0.5 - 2.1 m/s],' +
			' [2] * 100.0 / ' + CAST(@cnt AS varchar(20)) + ' AS [2.1 - 2.6 m/s],' +
			' [3] * 100.0 / ' + CAST(@cnt AS varchar(20)) + ' AS [2.6 - 5.7 m/s],' +
			' [4] * 100.0 / ' + CAST(@cnt AS varchar(20)) + ' AS [5.7 - 8.8 m/s],' +
			' [5] * 100.0 / ' + CAST(@cnt AS varchar(20)) + ' AS [8.8 - 11.1 m/s],' +
			' [6] * 100.0 / ' + CAST(@cnt AS varchar(20)) + ' AS [&gt;11.1 m/s]' +
		' FROM' +
			' (SELECT RecordID, '+
				' CASE ROUND(CAST(' + @DirectionColumn + ' AS decimal(18,4))/22.5, 0)' + 
					' WHEN 1 THEN 1' +
					' WHEN 2 THEN 2' +
					' WHEN 3 THEN 3' +
					' WHEN 4 THEN 4' +
					' WHEN 5 THEN 5' +
					' WHEN 6 THEN 6' +
					' WHEN 7 THEN 7' +
					' WHEN 8 THEN 8' +
					' WHEN 9 THEN 9' +
					' WHEN 10 THEN 10' +
					' WHEN 11 THEN 11' +
					' WHEN 12 THEN 12' +
					' WHEN 13 THEN 13' +
					' WHEN 14 THEN 14' +
					' WHEN 15 THEN 15' +
					' ELSE 0' +
				' END AS Dir,' +
				' CASE ROUND(CAST(' + @DirectionColumn + ' AS decimal(18,4))/22.5, 0)' + 
					' WHEN 1 THEN ''NNE''' +
					' WHEN 2 THEN ''NE''' +
					' WHEN 3 THEN ''ENE''' +
					' WHEN 4 THEN ''E''' +
					' WHEN 5 THEN ''ESE''' +
					' WHEN 6 THEN ''SE''' +
					' WHEN 7 THEN ''SSE''' +
					' WHEN 8 THEN ''S''' +
					' WHEN 9 THEN ''SSW''' +
					' WHEN 10 THEN ''SW''' +
					' WHEN 11 THEN ''WSW''' +
					' WHEN 12 THEN ''W''' +
					' WHEN 13 THEN ''WNW''' +
					' WHEN 14 THEN ''NW''' +
					' WHEN 15 THEN ''NNW''' +
					' ELSE ''N''' +
				' END AS Direction,' +
				' CASE ' +
					' WHEN CAST(' + @sValueFields + ' AS decimal (18, 4)) < 0.5 THEN 0' +
					' WHEN CAST( ' + @sValueFields + ' AS decimal (18, 4)) >= 0.5 AND  CAST(' + @sValueFields + ' AS decimal (18, 4)) < 2.1 THEN 1' +
					' WHEN CAST( ' + @sValueFields + ' AS decimal (18, 4)) >= 2.1 AND  CAST(' + @sValueFields + ' AS decimal (18, 4)) < 2.6 THEN 2' +
					' WHEN CAST( ' + @sValueFields + ' AS decimal (18, 4)) >= 2.6 AND  CAST(' + @sValueFields + ' AS decimal (18, 4)) < 5.7 THEN 3' +
					' WHEN CAST( ' + @sValueFields + ' AS decimal (18, 4)) >= 5.7 AND  CAST(' + @sValueFields + ' AS decimal (18, 4)) < 8.8 THEN 4' +
					' WHEN CAST( ' + @sValueFields + ' AS decimal (18, 4)) >= 8.8 AND  CAST(' + @sValueFields + ' AS decimal (18, 4)) < 11.1 THEN 5' +
					' WHEN CAST( ' + @sValueFields + ' AS decimal (18, 4)) >= 11.1 THEN 6' +
				' END AS Range' +
			' FROM [Record] INNER JOIN ('+ @sFilter +') tblFilter ON [Record].RecordID = tblFilter.DBGSystemRecordID' +
			@WhereClause + 
			') x' +
			' PIVOT' +
			' (' +
				' COUNT([RecordID])' +
				' FOR [Range] IN ([0], [1], [2], [3], [4], [5], [6])' +
			') AS pvt' +
			' ORDER BY Dir'
    PRINT @sql
    EXEC(@sql)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Chart_GetData_Windrose', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
