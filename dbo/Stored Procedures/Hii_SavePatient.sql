﻿CREATE PROCEDURE [dbo].[Hii_SavePatient]
(
	@RecordID int,
	@UserID int,
	@Return varchar(max) output
)
AS
BEGIN
	-- Check the initial conditions and get the values
	DECLARE @Condition varchar(max), @ID varchar(max), @PatientNumber varchar(max)
	SELECT @Condition = V048, @PatientNumber = V005
		FROM [Record] WHERE RecordID = @RecordID 
		AND V048 IS NOT NULL

	IF @PatientNumber = @RecordID
	BEGIN
		SELECT @ID = MAX(CAST(dbo.RemoveNonNumericChar(V005) AS INT)) 
			FROM [Record] 
			WHERE TableID = 2058 
			AND ISACTIVE = 1 
			AND V048 LIKE @Condition
			AND RecordID != @RecordID
		SELECT @ID = ISNULL(@ID, 0) + 1
			print @Condition
			print @ID
		
		IF @Condition LIKE '%Achalasia%'
			UPDATE [Record] SET V005 = 'A' + @ID WHERE RecordID = @RecordID
		IF @Condition LIKE '%Bariatric%'
			UPDATE [Record] SET V005 = 'B' + @ID WHERE RecordID = @RecordID
		IF @Condition LIKE '%Hiatus hernia%'
			UPDATE [Record] SET V005 = 'HH' + @ID WHERE RecordID = @RecordID
		IF @Condition LIKE '%OG Cancer%'
			UPDATE [Record] SET V005 = 'OG' + @ID WHERE RecordID = @RecordID
		IF @Condition LIKE '%Liver and biliary tumours%'
			UPDATE [Record] SET V005 = 'LB' + @ID WHERE RecordID = @RecordID
		IF @Condition LIKE '%Pancreatic and periampullary tumours%'
			UPDATE [Record] SET V005 = 'PP' + @ID WHERE RecordID = @RecordID

	END
END
