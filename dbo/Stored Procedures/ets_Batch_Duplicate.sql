﻿

CREATE PROCEDURE [dbo].[ets_Batch_Duplicate]
(
	@nBatchID int,
	@sUniqueColumnIDSys varchar(20)=NULL,
	@sUniqueColumnID2Sys varchar(20)=NULL
	
)
AS

BEGIN TRY
	-- EXEC [ets_Batch_Duplicate] 3683,'DateTimeRecorded'


	DECLARE @sSQL varchar(MAX)
	DECLARE @sSQL2 varchar(MAX)


	SELECT @sSQL ='UPDATE TempRecord 
	 SET RejectReason=ISNULL(RejectReason,'''') + '' DUPLICATE Record!''
	 FROM TempRecord 
	 WHERE RecordID IN (SELECT
		A.RecordID
	FROM (
		 SELECT 
		 '
		 + CASE WHEN @sUniqueColumnIDSys IS NOT NULL THEN @sUniqueColumnIDSys + ',' ELSE '' END +
		 + CASE WHEN @sUniqueColumnID2Sys IS NOT NULL THEN @sUniqueColumnID2Sys + ',' ELSE '' END +
		 '	 
		 COUNT(*) as intCount
		 FROM TempRecord WHERE BatchID='+  CAST(@nBatchID as varchar)  + '
		 GROUP BY 
		 '
		  + CASE WHEN @sUniqueColumnIDSys IS NOT NULL THEN @sUniqueColumnIDSys  ELSE '' END +
		  + CASE WHEN (@sUniqueColumnIDSys IS NOT NULL AND @sUniqueColumnID2Sys IS NOT NULL) THEN ','  ELSE '' END +
		 + CASE WHEN @sUniqueColumnID2Sys IS NOT NULL THEN @sUniqueColumnID2Sys  ELSE '' END +
		 '
		 HAVING COUNT(*) > 1
	) AS B
		INNER JOIN TempRecord A ON 
		'
		+ CASE WHEN @sUniqueColumnIDSys IS NOT NULL THEN ' A.'+@sUniqueColumnIDSys+' = B.' + @sUniqueColumnIDSys  ELSE '' END +
		  + CASE WHEN (@sUniqueColumnIDSys IS NOT NULL AND @sUniqueColumnID2Sys IS NOT NULL) THEN ' AND '  ELSE '' END +
		 + CASE WHEN @sUniqueColumnID2Sys IS NOT NULL THEN ' A.'+@sUniqueColumnID2Sys+' = B.' + @sUniqueColumnID2Sys   ELSE '' END +
		' WHERE A.BatchID='+  CAST(@nBatchID as varchar)  + ')'


	PRINT @sSQL
	EXEC (@sSQL)

	SELECT @sSQL2 ='UPDATE TempRecord 
	 SET RejectReason='' DUPLICATE Record!''
	 FROM TempRecord
	 JOIN [Record] S ON S.TableID = TempRecord.TableID
	 AND 
	 '
		+ CASE WHEN @sUniqueColumnIDSys IS NOT NULL THEN ' S.'+@sUniqueColumnIDSys+' = TempRecord.' + @sUniqueColumnIDSys   ELSE '' END +
		  + CASE WHEN (@sUniqueColumnIDSys IS NOT NULL AND @sUniqueColumnID2Sys IS NOT NULL) THEN ' AND '  ELSE '' END +
		 + CASE WHEN @sUniqueColumnID2Sys IS NOT NULL THEN ' S.'+@sUniqueColumnID2Sys+' = TempRecord.' + @sUniqueColumnID2Sys  ELSE '' END +
		'	
	  AND S.IsActive=1
	 WHERE TempRecord.BatchID='+  CAST(@nBatchID as varchar)  + ' AND TempRecord.RejectReason IS NULL'
 
	 PRINT @sSQL2
	EXEC (@sSQL2)

	 --UPDATE TempRecord 
	 --SET RejectReason='DUPLICATE Record!'
	 --FROM TempRecord 
	 --JOIN [Record] S ON S.TableID = TempRecord.TableID
	 --AND S.DateTimeRecorded = TempRecord.DateTimeRecorded
	 --AND S.IsActive=1
	 --WHERE BatchID=@nBatchID AND RejectReason IS NULL
 
END TRY 
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Batch_Duplicate', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
