﻿
 
CREATE PROCEDURE [dbo].[ets_InComingEmail_Delete]
(
	@nInComingEmailID int 
)
	/*----------------------------------------------------------------
	-- Stored Procedure: ets_InComingEmail_Delete
	-- Generated by: gpGenerateSP (Jon Bosker, DB Gurus Australia) 
	-- Date: Jul 10 2011 12:35AM
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	DELETE FROM [InComingEmail]
	WHERE InComingEmailID = @nInComingEmailID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_InComingEmail_Delete', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
