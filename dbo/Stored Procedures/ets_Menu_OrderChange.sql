﻿

CREATE PROCEDURE [dbo].[ets_Menu_OrderChange]
(
	@nMenuID int,
	@bMoveUp bit
)
AS
/*
	EXEC [ets_Menu_OrderChange]
		@nMenuID = 454,
		@bMoveUp = 0
	SELECT * FROM Menu WHERE AccountID = 8 order by DisplayOrder
		SELECT * FROM Menu WHERE MenuID =454


*/
BEGIN TRY
	SET NOCOUNT ON;

	DECLARE @nAccountID int, @nDisplayOrder int
	SELECT @nAccountID = AccountID, @nDisplayOrder = DisplayOrder 
		FROM Menu 
		WHERE MenuID = @nMenuID
			
	DECLARE @nOtherMenuID int
	DECLARE @nNewOrder int
	
	IF (@bMoveUp = 1)
	BEGIN
			SELECT TOP 1 @nNewOrder = MAX(DisplayOrder)
				FROM Menu
				WHERE AccountID = @nAccountID --AND IsActive=1
				AND DisplayOrder < @nDisplayOrder
	END
	ELSE
	BEGIN
			SELECT TOP 1 @nNewOrder = MIN(DisplayOrder)
				FROM Menu
				WHERE AccountID = @nAccountID -- AND IsActive=1
				AND DisplayOrder > @nDisplayOrder
	END
	
	IF @nNewOrder IS NOT NULL
	BEGIN TRY

	BEGIN TRANSACTION --beginning a transaction..

		-- Switch them round
		UPDATE Menu SET DisplayOrder = @nDisplayOrder 
			WHERE AccountID = @nAccountID
			AND DisplayOrder = @nNewOrder
		UPDATE Menu SET DisplayOrder = @nNewOrder 
			WHERE MenuID = @nMenuID
	COMMIT TRANSACTION --Commit the transaction as both Success.

	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION --RollBack Transaction as some where error happend.
	END CATCH
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Menu_OrderChange', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
