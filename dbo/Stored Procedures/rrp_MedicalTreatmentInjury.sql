﻿

CREATE PROCEDURE [dbo].[rrp_MedicalTreatmentInjury]
(
	@AccountID int,
	@nYear1 int, 
	@nYear2 int
) 
AS
/*
	-- First Series:
	EXEC Account24822.rrp_MedicalTreatmentInjury '2014', '2013'
	-- Second Series:
	EXEC rrp_MedicalTreatmentInjury '2015-01-01', '2015-12-31'
*/
-- Table to store all months
BEGIN TRY
	SET DATEFORMAT DMY
	DECLARE @tMonths TABLE (MonthNum int)

	-- table to store results
	DECLARE @tResults1 TABLE (MonthNum int, TotalCount int)
	DECLARE @tResults2 TABLE (MonthNum int, TotalCount int)

	;with Numbers (Number) as
		(SELECT row_number() over (order by object_id)
		from sys.all_objects)
		INSERT INTO @tMonths (MonthNum)
			SELECT number
			FROM Numbers
			WHERE number <= 12

	-- This is the table ID for the Incident table for the current account
	DECLARE @TableID int
	SELECT @TableID = TableID FROM [Table] WHERE [TableName] = 'Incidents' and [AccountID] = @AccountID
	PRINT @TableID

	-- SELECT * FROM [Table] WHERE [TableName] = 'Incidents' and [AccountID] = 24822
	-- SELECT * FROM [Column] WHERE TableID = 1840 
	-- UPDATE [Column] SET IsSystemColumn=1 WHERE TableID = 1840 AND SystemName IN ('V010', 'V012') -- we do not want them to change
	-- Get the counts
	INSERT INTO @tResults1
		SELECT DATEPART(month, V010 /*[Date/time of incident]*/), COUNT(*) as Cnt
			FROM [Record]
			WHERE TableID = @TableID /*Incidents*/
			AND V012 /*Type of Incident*/ = 'Medical Treatment Injury'
			AND ISDATE(V010 /*[Date/time of incident]*/) = 1
			AND DATEPART(year, CAST(V010 /*[Date/time of incident]*/ as date)) = @nYear1
			GROUP BY DATEPART(month, V010 /*[Date/time of incident]*/)

	INSERT INTO @tResults2
		SELECT DATEPART(month, V010 /*[Date/time of incident]*/), COUNT(*) as Cnt
			FROM [Record]
			WHERE TableID = @TableID /*Incidents*/
			AND V012 /*Type of Incident*/ = 'Medical Treatment Injury'
			AND ISDATE(V010 /*[Date/time of incident]*/) = 1
			AND DATEPART(year, CAST(V010 /*[Date/time of incident]*/ as date)) = @nYear2
			GROUP BY DATEPART(month, V010 /*[Date/time of incident]*/)


	-- Join then up
	SELECT CAST( @nYear1 AS varchar ) + ' vs ' + CAST( @nYear2 AS varchar ) AS Title,
		M.MonthNum, ISNULL(Y1.TotalCount, 0)  as Year1, ISNULL(Y2.TotalCount, 0)  as Year2
		FROM @tMonths M
		LEFT JOIN @tResults1 Y1 ON Y1.MonthNum = M.MonthNum
		LEFT JOIN @tResults2 Y2 ON Y2.MonthNum = M.MonthNum
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('rrp_MedicalTreatmentInjury', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
