﻿

CREATE PROCEDURE [dbo].[Location_GetList]
	-- Add the parameters for the stored procedure here	
	@nCount int,
	@nAccountID INT
AS

BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET ROWCOUNT @nCount

    -- Insert statements for procedure here
	SELECT	LocationID,
			LocationName,
			Notes,
			Latitude,
			Longitude
	FROM	Location	
	WHERE IsActive=1 AND AccountID=@nAccountID
	
	SET ROWCOUNT 0
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Location_GetList', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
