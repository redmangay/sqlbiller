﻿CREATE PROCEDURE [dbo].[Kate_SaveED2]
(
	@RecordID int,
	@UserID int,
	@Return varchar(max) output
)
AS
-- 
-- Copy from the Notification table
BEGIN

	UPDATE [Record] 
		SET V236= N.V015 
		FROM [Record]
		JOIN [Record] N ON N.TableID = 2454  -- Join to Notification Form 
			AND N.RecordID = [Record].V021		 -- on Trauma Record Number
		WHERE [Record].RecordID = @RecordID
END
