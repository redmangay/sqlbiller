﻿

CREATE PROCEDURE [dbo].[PreProcessorTest] 
	@nSourceBatchID int
AS

BEGIN TRY
	SET NOCOUNT ON;

	DECLARE @RecordID int
	DECLARE @V001 varchar(200)
	DECLARE @V002 varchar(200)

	DECLARE @CurrentRecordID int = 0
	DECLARE @SampleSite varchar(200) = NULL

	DECLARE cursor1 CURSOR FOR
		SELECT RecordID, V001, V002
		FROM TempRecord
		WHERE BatchID = @nSourceBatchID
		ORDER BY RecordID

	OPEN cursor1

	FETCH NEXT FROM cursor1
		INTO @RecordID, @V001, @V002

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF UPPER(@V001) = 'NAME'
		BEGIN
			SET @SampleSite = @V002
		END
		IF UPPER(@V001) = 'UNITS'
		BEGIN
			SET @CurrentRecordID = @RecordID
			BREAK
		END
		FETCH NEXT FROM cursor1
			INTO @RecordID, @V001, @V002
	END

	CLOSE cursor1
	DEALLOCATE cursor1

	/* === REQUIRED === */
	DECLARE @Errors TABLE
	(
		ErrorID int IDENTITY(1, 1),
		ErrorText varchar(MAX)
	)

	DECLARE @Out TABLE
	(
		[Sample Site] varchar(200),
		[Date/Time Sampled] varchar(200),
		[Sample Value] varchar(200)
	)

	DECLARE @bContinue bit = 1

	IF @SampleSite IS NULL
	BEGIN
		INSERT INTO @Errors (ErrorText) VALUES ('Couldn''t find Sample Site Name row: there is no row with word "Name" in the first column in the source file')
		SET @bContinue = 0
	END
	IF @CurrentRecordID  = 0
	BEGIN
		INSERT INTO @Errors (ErrorText) VALUES ('Couldn''t find data start: there is no row with word "Units" in the first column in the source file')
		SET @bContinue = 0
	END

	PRINT @bContinue

	IF @bContinue = 1
	BEGIN
		DECLARE @V004 varchar(200)
		DECLARE @V006 varchar(200)
		DECLARE @V008 varchar(200)

		DECLARE cursor2 CURSOR FOR
			SELECT V001, V004, V006, V008
			FROM TempRecord
			WHERE BatchID = @nSourceBatchID AND RecordID > @CurrentRecordID
			ORDER BY RecordID

		OPEN cursor2

		FETCH NEXT FROM cursor2
			INTO @V001, @V004, @V006, @V008

		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO @Out
				([Sample Site], [Date/Time Sampled], [Sample Value])
			VALUES
				(@SampleSite + '-1', @V001, @V004)
			INSERT INTO @Out
				([Sample Site], [Date/Time Sampled], [Sample Value])
			VALUES
				(@SampleSite + '-2', @V001, @V006)
			INSERT INTO @Out
				([Sample Site], [Date/Time Sampled], [Sample Value])
			VALUES
				(@SampleSite + '-3', @V001, @V008)

			FETCH NEXT FROM cursor2
				INTO @V001, @V004, @V006, @V008
		END

		CLOSE cursor2
		DEALLOCATE cursor2
	END

	/* === REQUIRED === */
	SELECT * FROM @Out
	SELECT * FROM @Errors
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('PreProcessorTest', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
