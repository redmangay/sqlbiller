﻿
--------------------------------------------------------------------------------------
-- PLEASE DO NOT CHANGE THE SCRIPT ABOVE - Simply set @Sequence & @Description
-- and paste your script in BELOW:
--------------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[SystemOption_Insert]
(
	@nNewID int = null OUTPUT,
	@sOptionKey nvarchar(200) ,
	@sOptionValue nvarchar(1000) ,
	@sOptionNotes ntext=NULL,
	@nAccountID int=NULL,
	@nTableID int=NULL 
)
 
AS
/*
	MODIFICATION HISTORY
	====================
	20 Nov 2019	Jon Bosker	Made @sOptionNotes optional
*/


BEGIN TRY 

	IF @sOptionNotes IS NULL
	BEGIN
		SELECT TOP 1 @sOptionNotes = [OptionNotes] FROM [SystemOption] WHERE [OptionKey]=@sOptionKey ORDER BY [SystemOptionID]
	END

	INSERT INTO [SystemOption]
	(
		OptionKey, 
		OptionValue, 
		OptionNotes,
		AccountID,
		TableID
	) VALUES (
		@sOptionKey,
		@sOptionValue,
		ISNULL(@sOptionNotes,''),
		@nAccountID,
		@nTableID
	)
	SELECT @nNewID = @@IDENTITY
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('SystemOption_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
