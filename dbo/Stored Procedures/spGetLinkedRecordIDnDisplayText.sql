﻿


CREATE PROCEDURE [dbo].[spGetLinkedRecordIDnDisplayText]
(
	@sDisplayColumn varchar(1000),
	@nColumnID int= NULL,
	@nTableTableID int ,
	@nMaxRow int =NULL,
	@WhereEqualTextSearch varchar(MAX)=NULL, -- ' AND Record.V001 =''OK'''
	@WhereEqualParentSearch varchar(MAX)=NULL,
	@DisplayPredict varchar(500)= NULL
)
	/*----------------------------------------------------------------

	HISTIRORY
	=========
	Red Mar 03 2018:	Added ColumnID and WhereEqualParenSearch params. 
						This is to get the valid records for dropdown list parent and child tables.
	Red Aug 06 2018:    Added "AND ISNUMERIC('+ @sSystemName + ')=1'" in WHERE Clause, to show only records with RecordIDs
						
	Jon	Nov 07 2018:	Addded SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	Red Apr 18 2019:    Added SET DATEFORMAT dmy;

	Usage
	=====


	EXEC spGetLinkedRecordIDnDisplayText '[Job Title]', 2429,NULL,' AND Record.V002=906563', NULL	
	EXEC spGetLinkedRecordIDnDisplayText @sDisplayColumn='[Job Title]',@nTableTableID=2429,@WhereEqual=' AND Record.V002=906563'
	
	EXEC spGetLinkedRecordIDnDisplayTextx 
	@nColumnID=60611, 
	@nTableTableID=3123,
	@nMaxRow=NULL,
	@WhereEqualTextSearch='',
	@WhereEqualParentSearch='AND RECORD.RECORDID IN (SELECT [DBGSystemRecordID] FROM (SELECT RecordInfo.*,ROW_NUMBER() OVER(ORDER BY [DBGSystemRecordID] DESC ) as RowNum FROM (SELECT  [User].FirstName + '' '' + [User].LastName  as _EnteredBy, Record.RecordID AS [Record ID],  ISNULL(P60591.V001,'''') AS [ID],  ISNULL(P60611.V001,'''') AS [Name], Record.V002 AS [Account Number], Record.V003 AS [Account Number 2], Record.V004 AS [Number], Record.V005 AS [Date], [User].FirstName + '' '' + [User].LastName  AS [Entered By], Record.V007 AS [Text], Record.V008 AS [Time],  ISNULL(P60612.V002,'''') AS [Dropdown2], Record.V006 AS [Date2], Record.RecordID AS [DBGSystemRecordID]  FROM [Record] JOIN [User] ON Record.EnteredBy = [User].UserID  LEFT OUTER JOIN [Record] P60591 ON P60591.RecordID = CASE WHEN ISNUMERIC([Record].V001)=1 THEN [Record].V001 ELSE -1 END  LEFT OUTER JOIN [Record] P60611 ON P60611.RecordID = CASE WHEN ISNUMERIC([Record].V009)=1 THEN [Record].V009 ELSE -1 END  LEFT OUTER JOIN [Record] P60612 ON P60612.RecordID = CASE WHEN ISNUMERIC([Record].V010)=1 THEN [Record].V010 ELSE -1 END   WHERE (Record.TableID = 4230)  AND (Record.IsActive = 1)  AND (  Record.V009 =''1344791'' AND  Record.V007 LIKE''%r%''  AND ((( 1=1   AND   1=1 )  AND   1=1 )  AND   1=1 ))) as RecordInfo) as RecordFinalInfo WHERE RowNum >= 1)'
	,@DisplayPredict =NULL
	
	---------------------------------------------------------------*/
 
AS

BEGIN TRY

SET DATEFORMAT dmy;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @sFieldsToShow varchar(1000)
	DECLARE @sSQL varchar(MAX)
	DECLARE @FROM varchar(MAX)
	DECLARE @ParentAlias varchar(100)

	DECLARE @nTableChildID int
	DECLARE @sSystemName varchar(MAX)
	DECLARE @SQL varchar(MAX)

	SELECT @nTableChildID = TableID, @sSystemName = [SystemName] FROM [Column]
	WHERE [ColumnID] = @nColumnID

    SELECT @SQL = 'SELECT ' + @sSystemName + ' FROM Record WHERE cast(TableID as varchar)=' + cast(@nTableChildID as varchar) + ' AND ISNUMERIC('+ @sSystemName + ')=1' + ' ' +  @WhereEqualParentSearch 

	DECLARE @ChildTableRecordList Table ([Counter] int identity(1,1), ChildRecordID int)
	DECLARE @ChildRecordIDs varchar(MAX)

	INSERT INTO @ChildTableRecordList (ChildRecordID) EXEC(@SQL);

		SELECT  @ChildRecordIDs = LEFT(ChildRecordIDs, LEN(ChildRecordIDs) - 1) 
									FROM ( SELECT CONVERT(varchar(50), ChildRecordID) + ',' FROM @ChildTableRecordList FOR XML PATH ('') ) C (ChildRecordIDs)
									SET @ChildRecordIDs = ' AND Record.RecordID IN ('+ @ChildRecordIDs +')'								

	IF @nMaxRow IS NOT NULL
		SET ROWCOUNT @nMaxRow

	SELECT @sFieldsToShow=dbo.fnReplaceDisplayColumns(@sDisplayColumn, @nTableTableID,NULL)

	SELECT @FROM = ' FROM [Record] '

	IF EXISTS (SELECT TOP 1 ColumnID FROM [Column] WHERE TableID=@nTableTableID AND TableTableID IS NOT NULL AND TableTableID>0 AND ColumnType<>'number')
			BEGIN
				DECLARE @t2TableTable Table
				(
					ID int identity(1,1),
					ColumnID int,
					SystemName varchar(50),
					TableTableID INT				
				)

				INSERT INTO @t2TableTable(ColumnID,SystemName,TableTableID) 
					SELECT ColumnID,SystemName,TableTableID FROM [Column] 
						WHERE TableID=@nTableTableID AND TableTableID IS NOT NULL AND TableTableID>0 AND ColumnType<>'number'
				
				DECLARE @counter2 int
				SET @counter2 = 1
				WHILE EXISTS(SELECT * FROM @t2TableTable WHERE ID >= @counter2)
					BEGIN
							DECLARE @n2TableTableID INT
							SELECT @ParentAlias = 'P' + CAST(ColumnID as varchar),@n2TableTableID=TableTableID FROM @t2TableTable WHERE ID = @counter2
							IF CHARINDEX(@ParentAlias,@sFieldsToShow,1)>0 AND CHARINDEX(@ParentAlias,@FROM,1)=0
								BEGIN
									-- parent
									SELECT
										@FROM = @FROM + ' LEFT OUTER JOIN [Record] ' + @ParentAlias + ' ON ' 
												+ @ParentAlias + '.RecordID = CASE WHEN ISNUMERIC([Record].' + SystemName +')=1 THEN [Record].' + SystemName + ' ELSE -1 END '
												+ ' AND ' + @ParentAlias + '.[IsActive] = 1' 
										FROM @t2TableTable WHERE ID = @counter2
								END								
									
						SET @counter2 = @counter2 + 1
					END

			END

	IF @DisplayPredict IS NOT NULL
		SET @DisplayPredict=' AND ' + @sFieldsToShow + ' LIKE ''%'+@DisplayPredict+'%'''


    IF(@WhereEqualTextSearch = ' AND Record.RecordID=' AND @ChildRecordIDs IS NULL)
    BEGIN
    SET @WhereEqualTextSearch = NULL
    END

	SELECT @sSQL='SELECT Record.RecordID,'+@sFieldsToShow+' as [DisplayText] '+@FROM 
					+'  WHERE Record.IsActive=1 AND Record.TableID=' + CAST(@nTableTableID AS varchar) 
					 + ' '  + ISNULL(@WhereEqualTextSearch,'') + ' ' + ISNULL(@ChildRecordIDs,'') + ' ' +ISNULL(@DisplayPredict,'')+ ' '+ ' ORDER BY DisplayText'

				
	EXEC (@sSQL)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	'@sSQL: ' +  @sSQL +
	' @WhereEqualTextSearch: ' + @WhereEqualTextSearch +
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
	 '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
	 '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
	 '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spGetLinkedRecordIDnDisplayText', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
