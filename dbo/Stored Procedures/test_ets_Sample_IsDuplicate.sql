﻿

CREATE PROCEDURE [dbo].[test_ets_Sample_IsDuplicate]
(
	@SampleSiteID int,
	@SampleTypeID int,
	@DateTimeSampled datetime,
	@V001 varchar(MAX) = NULL,
	@V002 varchar(MAX) = NULL,
	@V003 varchar(MAX) = NULL,
	@V004 varchar(MAX) = NULL,
	@V005 varchar(MAX) = NULL,
	@V006 varchar(MAX) = NULL,
	@V007 varchar(MAX) = NULL,
	@V008 varchar(MAX) = NULL,
	@V009 varchar(MAX) = NULL,
	@V010 varchar(MAX) = NULL,
	@V011 varchar(MAX) = NULL,
	@V012 varchar(MAX) = NULL,
	@V013 varchar(MAX) = NULL,
	@V014 varchar(MAX) = NULL,
	@V015 varchar(MAX) = NULL,
	@V016 varchar(MAX) = NULL,
	@V017 varchar(MAX) = NULL,
	@V018 varchar(MAX) = NULL,
	@V019 varchar(MAX) = NULL,
	@V020 varchar(MAX) = NULL,
	@V021 varchar(MAX) = NULL,
	@V022 varchar(MAX) = NULL,
	@V023 varchar(MAX) = NULL,
	@V024 varchar(MAX) = NULL,
	@V025 varchar(MAX) = NULL,
	@V026 varchar(MAX) = NULL,
	@V027 varchar(MAX) = NULL,
	@V028 varchar(MAX) = NULL,
	@V029 varchar(MAX) = NULL,
	@V030 varchar(MAX) = NULL,
	@V031 varchar(MAX) = NULL,
	@V032 varchar(MAX) = NULL,
	@V033 varchar(MAX) = NULL,
	@V034 varchar(MAX) = NULL,
	@V035 varchar(MAX) = NULL,
	@V036 varchar(MAX) = NULL,
	@V037 varchar(MAX) = NULL,
	@V038 varchar(MAX) = NULL,
	@V039 varchar(MAX) = NULL,
	@V040 varchar(MAX) = NULL,
	@V041 varchar(MAX) = NULL,
	@V042 varchar(MAX) = NULL,
	@V043 varchar(MAX) = NULL,
	@V044 varchar(MAX) = NULL,
	@V045 varchar(MAX) = NULL,
	@V046 varchar(MAX) = NULL,
	@V047 varchar(MAX) = NULL,
	@V048 varchar(MAX) = NULL,
	@V049 varchar(MAX) = NULL,
	@V050 varchar(MAX) = NULL
)
AS

BEGIN TRY
	DECLARE @sSQL varchar(MAX)

	SELECT @sSQL= 'SELECT * from [Sample] WHERE
				SampleSiteID =' +  CAST(@SampleSiteID as varchar)  + 
				'AND [SampleTypeID] =' +  CAST(@SampleTypeID as varchar)  +
				'AND [DateTimeSampled] =''' + CONVERT(varchar(30), @DateTimeSampled, 120) + '''' 
          
	IF @V001 is not NULL
		SELECT @sSQL=   @sSQL + ' AND [V001] =''' + @V001 + ''''
		ELSE
		SELECT @sSQL=   @sSQL + ' AND [V001] is NULL'
    
	IF @V002 is not NULL
		SELECT @sSQL=   @sSQL + ' AND [V002] =''' + @V002 + '''' 
		ELSE
		SELECT @sSQL=   @sSQL + ' AND [V002] is NULL'
        
      
	IF @V003 is not NULL
		SELECT @sSQL=   @sSQL + ' AND [V003] =''' + @V003 + ''''  
		ELSE
		SELECT @sSQL=   @sSQL + ' AND [V003] is NULL'  

	IF @V004 is not NULL
		SELECT @sSQL=   @sSQL + ' AND [V004] =''' + @V004 + '''' 
		ELSE
		SELECT @sSQL=   @sSQL + ' AND [V004] is NULL'   
        
	IF @V005 is not NULL
		SELECT @sSQL=   @sSQL + ' AND [V005] =''' + @V005 + ''''  
		ELSE
		SELECT @sSQL=   @sSQL + ' AND [V005] is NULL'  

	IF @V006 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V006] =''' + @V006 + ''''  
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V006] is NULL'  
        
	IF @V007 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V007] =''' + @V007 + ''''  
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V007] is NULL'  

	IF @V008 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V008] =''' + @V008 + '''' 
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V008] is NULL'   
  
	IF @V009 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V009] =''' + @V009 + ''''  
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V009] is NULL' 
        
	IF @V010 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V010] =''' + @V010 + ''''  
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V010] is NULL' 
 

	IF @V011 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V011] =''' + @V011 + '''' 
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V011] is NULL' 
  

	IF @V012 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V012] =''' + @V012 + '''' 
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V012] is NULL' 
  

	IF @V013 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V013] =''' + @V013 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V013] is NULL' 

	IF @V014 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V014] =''' + @V014 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V014] is NULL' 


	IF @V015 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V015] =''' + @V015 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V015] is NULL' 


	IF @V016 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V016] =''' + @V016 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V016] is NULL' 


	IF @V017 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V017] =''' + @V017 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V017] is NULL' 


	IF @V018 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V018] =''' + @V018 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V018] is NULL' 


	IF @V019 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V019] =''' + @V019 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V019] is NULL' 


	IF @V020 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V020] =''' + @V020 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V020] is NULL' 


	IF @V021 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V021] =''' + @V021 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V021] is NULL' 


	IF @V022 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V022] =''' + @V022 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V022] is NULL' 


	IF @V023 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V023] =''' + @V023 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V023] is NULL' 


	IF @V024 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V024] =''' + @V024 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V024] is NULL' 


	IF @V025 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V025] =''' + @V025 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V025] is NULL' 


	IF @V026 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V026] =''' + @V026 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V026] is NULL' 


	IF @V027 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V027] =''' + @V027 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V027] is NULL' 


	IF @V028 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V028] =''' + @V028 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V028] is NULL' 


	IF @V029 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V029] =''' + @V029 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V029] is NULL' 


	IF @V030 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V030] =''' + @V030 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V030] is NULL' 


	IF @V031 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V031] =''' + @V031 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V031] is NULL' 


	IF @V032 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V032] =''' + @V032 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V032] is NULL' 


	IF @V033 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V033] =''' + @V033 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V033] is NULL' 


	IF @V034 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V034] =''' + @V034 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V034] is NULL' 


	IF @V035 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V035] =''' + @V035 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V035] is NULL' 


	IF @V036 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V036] =''' + @V036 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V036] is NULL' 


	IF @V037 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V037] =''' + @V037 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V037] is NULL' 


	IF @V038 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V038] =''' + @V038 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V038] is NULL' 


	IF @V039 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V039] =''' + @V039 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V039] is NULL' 


	IF @V040 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V040] =''' + @V040 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V040] is NULL' 


	IF @V041 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V041] =''' + @V041 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V041] is NULL' 


	IF @V042 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V042] =''' + @V042 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V042] is NULL' 


	IF @V043 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V043] =''' + @V043 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V043] is NULL' 


	IF @V044 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V044] =''' + @V044 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V044] is NULL' 


	IF @V045 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V045] =''' + @V045 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V045] is NULL' 


	IF @V046 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V046] =''' + @V046 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V046] is NULL' 


	IF @V047 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V047] =''' + @V047 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V047] is NULL' 


	IF @V048 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V048] =''' + @V048 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V048] is NULL' 


	IF @V049 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V049] =''' + @V049 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V049] is NULL' 


	IF @V050 is not NULL
			SELECT @sSQL=   @sSQL + ' AND [V050] =''' + @V050 + ''''   
			ELSE
			SELECT @sSQL=   @sSQL + ' AND [V050] is NULL' 


	PRINT @DateTimeSampled
           
	PRINT @sSQL
	EXEC (@sSQL)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('test_ets_Sample_IsDuplicate', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
