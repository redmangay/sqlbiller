﻿

CREATE PROCEDURE [dbo].[ConvertTableToCSV]
(			
	@Database				varchar(255) = ''
	,@TableName				varchar(255)
	,@TableNameID			varchar(255)
)
AS

BEGIN TRY

	DECLARE			@SQL			varchar(MAX)

	-- Enter the table name, and connect to the correct DB, press run

	SET				@SQL = '
	DECLARE			@ColumnList			varchar(MAX)
					,@ColumnHeading		varchar(MAX)
					,@SQL				varchar(MAX)

	SET				@ColumnList = ''''
	SET				@ColumnHeading = ''''

	SET				CONCAT_NULL_YIELDS_NULL OFF

	SELECT			@ColumnList =	@ColumnList 
								+	CASE	WHEN		@ColumnList <> ''''
											THEN		'' + '''','''' + ''
											ELSE		''''
									END
								+	CASE	WHEN		DATA_TYPE IN (''smallint'', ''int'', ''bigint'', ''bit'', ''decimal'')
											THEN		''QUOTENAME( CAST( ['' + COLUMN_NAME + ''] AS varchar(MAX) ), ''''"'''')''
											WHEN		DATA_TYPE IN (''datetime'')
											THEN		''QUOTENAME( CONVERT( varchar(MAX), ['' + COLUMN_NAME + ''], 120 ), ''''"'''')''
											WHEN		DATA_TYPE IN (''uniqueidentifier'')
											THEN		''QUOTENAME( CAST( ['' + COLUMN_NAME + ''] AS varchar(MAX) ), ''''"'''')''
											ELSE		''QUOTENAME( ['' + COLUMN_NAME + ''], ''''"'''')''
									END
					,@ColumnHeading =	@ColumnHeading 
								+	CASE	WHEN		@ColumnHeading <> ''''
											THEN		'''' + '','' + ''''
											ELSE		''''
									END
								+	QUOTENAME( COLUMN_NAME, ''"'' )
	FROM			' + @Database + 'INFORMATION_SCHEMA.COLUMNS
	WHERE			TABLE_NAME = ''' + @TableName + '''
	AND				COLUMN_NAME NOT IN ( ''' + @TableNameID + ''' )

	-- Store the data in a temp table
	CREATE TABLE	#Build
	(
					BuildID			INT	IDENTITY (1,1)
					,[Patient ID]		varchar(MAX)
					,ColumnData		varchar(MAX)
	)

	INSERT INTO		#Build
	(
					[Patient ID]
					,ColumnData
	)
	VALUES
	(				''' + @TableNameID + '''
					,@ColumnHeading
	)

	SET				@SQL = ''
	INSERT INTO		#Build
	(
					[Patient ID]
					,ColumnData
	)

	SELECT			[' + @TableNameID + ']
					,'' + @ColumnList + ''
	FROM			[' + @TableName + ']
	''

	EXEC			(@SQL)


	SELECT			*
	FROM			#Build
	ORDER BY		BuildID
	'

	PRINT @SQL
	EXEC			(@SQL)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ConvertTableToCSV', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
