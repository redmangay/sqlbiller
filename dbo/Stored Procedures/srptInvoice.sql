﻿CREATE PROCEDURE [dbo].[srptInvoice] 
	@InvoiceAmount money,
	@ProjectNumber varchar(MAX),
	@Date DateTime
AS
BEGIN
select	(
@InvoiceAmount +

IsNull((SELECT Sum(CAST(Account24918.[vBilling Information].[Invoice Amount] AS MONEY)) AS SumOfInvoiceAmount 
FROM Account24918.[vBilling Information]
WHERE TYPE in (1, 4) AND Account24918.[vBilling Information].[Project Number]=@ProjectNumber AND CONVERT(datetime, Account24918.[vBilling Information].[DATE],103) <@Date ),0)
+
ISNull((SELECT Sum(CAST(Account24918.[vBilling Information].[Invoice Amount] AS MONEY)) AS SumOfInvoiceAmount 
FROM Account24918.[vBilling Information]
WHERE TYPE in (2, 8) AND Account24918.[vBilling Information].[Project Number]=@ProjectNumber AND CONVERT(datetime, Account24918.[vBilling Information].[DATE],103) <@Date ),0)

) 
 AS TotalInvoicedIncludingThisAmount,
 
 (
IsNull((SELECT Sum(CAST(Account24918.[vBilling Information].[Invoice Amount] AS MONEY)) AS SumOfInvoiceAmount 
FROM Account24918.[vBilling Information]
WHERE TYPE in (1, 4) AND Account24918.[vBilling Information].[Project Number]=@ProjectNumber AND CONVERT(datetime, Account24918.[vBilling Information].[DATE],103) <@Date ),0)
+
ISNull((SELECT Sum(CAST(Account24918.[vBilling Information].[Invoice Amount] AS MONEY)) AS SumOfInvoiceAmount 
FROM Account24918.[vBilling Information]
WHERE TYPE in (2, 8) AND Account24918.[vBilling Information].[Project Number]=@ProjectNumber AND CONVERT(datetime, Account24918.[vBilling Information].[DATE],103) <@Date ),0)

) as PreviousInvoicedTotal
 
 
END