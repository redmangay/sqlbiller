﻿CREATE procedure [dbo].[usr_rptJobApprovalFollowup]

@Option int,
@Group int = NULL,
@ProjectController int = NULL,
@DateStart date ,
@DateEnd date 

AS

BEGIN TRY
/*

    Red: Options
    1 - All Group
    2 - Selected Group
    3 - Selected Project Controller

*/

--       EXEC [usr_rptJobApprovalFollowup] @Option =2,@DateStart = '01/08/2018',@DateEnd='31/08/2018',@Group =  15154968-- 15154972 --15154968


	   DECLARE @JobApprovalFollowupNotes TABLE (
	   JobRecordID int,
		  [Group Division] varchar(max),
		  [Job No] varchar(max),
		  [Project Title] varchar(max),
		  [Note Date] varchar(max),
		  [Note] varchar(max),
		  Initials varchar(max),
		  Controller varchar(max),
		  [Project Value] money,
		  [Fixed Price] varchar(max),
		  Company varchar(max),
		  Billed money,
		  [% Billed] NUMERIC(10,2),
		  Timesheet money,
		  [% Timesheet] money,
		  NoteRecordID int
	   )

	   DECLARE @JobApprovalFollowup TABLE (
	   JobRecordID int,
		  [Group Division] varchar(max),
		  [Job No] varchar(max),
		  [Project Title] varchar(max),
		  [Note Date] varchar(max),
		  [Note] varchar(max),
		  Initials varchar(max),
		  Controller varchar(max),
		  [Project Value] money,
		  [Fixed Price] varchar(max),
		  Company varchar(max),
		  Billed money,
		  [% Billed] NUMERIC(10,2),
		  Timesheet money,
		  [% Timesheet] money,
		  NoteRecordID int
	   )

	   DECLARE @JobApprovalFollowupFinal TABLE (
	   JobRecordID int,
		  [Group Division] varchar(max),
		  [Job No] varchar(max),
		  [Project Title] varchar(max),
		  [Note Date] date,
		  [Note] varchar(max),
		  Initials varchar(max),
		  Controller varchar(max),
		  [Project Value] money,
		  [Fixed Price] varchar(max),
		  Company varchar(max),
		  Billed money,
		  [% Billed] NUMERIC(10,2),
		  Timesheet money,
		  [% Timesheet] money,
		   NoteRecordID int,
		  YesWithin bit,
		  YesBeyond bit
	   )



SET DATEFORMAT DMY;
DECLARE @SQL varchar(max) = ''
DECLARE @SelectedGroup varchar(max)
DECLARE @SelectedProjectController varchar(max)

DECLARE @MinimumValue money, @MinimumPercentage money

SELECT @MinimumValue= V024,@MinimumPercentage = V025  FROM RECORD WHERE TABLEID = 2743

IF @Option = 1
BEGIN
    SET @SelectedGroup = ''
    SET @SelectedProjectController = ''
END 
ELSE IF @Option = 2
BEGIN
    SET @SelectedGroup = 'AND [Job Information].V027 = ''' + CAST(ISNULL(@Group,'') AS VARCHAR) +''''
    SET @SelectedProjectController = ''
END
ELSE IF @Option = 3
BEGIN
    SET @SelectedGroup = ''
    SET @SelectedProjectController = 'AND [Job Information].V026 = ''' + CAST(ISNULL(@ProjectController,'') AS VARCHAR) + ''''
END 


	/* == Lets get the Timesheets (Percent and Amount) */	
	
	DECLARE @JobRecordIDs varchar(max)
	DECLARE @SQLJobRecordIDs varchar(max) = ''
	DECLARE @TimesheetTable Table (JobRecordID int,TimesheetAmount money,TimesheetPercent money)
	
	INSERT INTO @TimesheetTable
	  SELECT	
		  
			
			 [Job Information].RecordID,
			  Round(
				 ISNULL((
				 SELECT SUM(CONVERT(MONEY,ISNULL([Timesheet Details].V007,0))) 												     
				 ),0)
			 ,2) AS  [Timesheet Amount],
			 Round(
				ISNULL((
				    SELECT SUM(CONVERT(MONEY,ISNULL([Timesheet Details].V007,0))) 												     
				    ),0)
				    / 
				CONVERT(MONEY,ISNULL([Job Information].V008,1))
				,2) as PercentTimesheet		
	
			 FROM  [Record] [Job Information] 	
			 JOIN  Record [Timesheet Details]   ON [Timesheet Details].TABLEID = 2729 
							AND [Timesheet Details].ISACTIVE =1  
							AND [Timesheet Details].V001 = [Job Information].RecordID
			 WHERE   [Job Information].TableID = 2475		/*	Job Information	*/
			 AND	    [Job Information].IsActive = 1 
			 AND [Job Information].V021 ='Bid'
			 --AND [Job Information].V027 = @Group
			 GROUP BY [Job Information].RecordID ,[Job Information].V008
			 ORDER BY  [Job Information].RecordID

			--SELECT * FROM  @TimesheetTable
			
SELECT  @JobRecordIDs = LEFT(TimesheetRecordIDs, LEN(TimesheetRecordIDs) - 1) 
FROM (
		  SELECT			  
		  CONVERT(varchar(50), JobRecordID) + ','		
			 FROM @TimesheetTable  WHERE ((TimesheetAmount >= @MinimumValue) OR ([TimesheetPercent]>=@MinimumPercentage)) 
			 FOR XML PATH ('')) T (TimesheetRecordIDs)

			--SELECT @JobRecordIDs
	SET @SQLJobRecordIDs = ' AND [Job Information].RecordID IN ('+ @JobRecordIDs +') '
	   /* == End Timesheets == */


SET @SQL = '


SELECT			 
				[Job Information].RecordID				AS  [Job RecordID]
				,NULL 
				,[Job Information].V001				AS  [Job No]
				,NULL 
				, [Job Notes].V003 AS [Note Date]
				,[Job Notes].V006					AS  [Note]	
				,ISNULL([Employee_Note_Initial].V004,'''')		AS  Initials
				,NULL 
				,NULL
				,NULL 
				,NULL 
				,NULL
				    ,NULL
				    ,NULL
				    ,NULL
				    ,[Job Notes].RecordID	
	
					    FROM		[Record] [Job Notes] WITH (NOLOCK) 

			 JOIN Record [Job Information]  WITH (NOLOCK) ON [Job Information].TableID = 2475
			AND	[Job Information].IsActive = 1 
			AND			 [Job Information].[V021] IN (''Bid'')
			' + @SelectedGroup + ' 
			' + @SelectedProjectController + '
			' + @SQLJobRecordIDs + '
			 AND [Job Information].RecordID = CAST([Job Notes].[V009]  as int)
			    JOIN [Record] [Employee_Note_Initial] WITH (NOLOCK) ON [Employee_Note_Initial].TableID = 2669 /*	Employee List	*/
				AND		[Employee_Note_Initial].IsActive = 1
				AND		[Employee_Note_Initial].RecordID = CAST([Job Notes].V007 as int) 		/*	Job Initial	*/

	WHERE		[Job Notes].TableID = 2676		/*	Job Information	*/
	AND			[Job Notes].IsActive = 1
	'
	
	

	PRINT @SQL
	INSERT INTO @JobApprovalFollowupNotes EXEC(@SQL);
	--select * from @JobApprovalFollowupNotes






	   /* == Lets get the BAPA, Amount and Percent */
	   
	   DECLARE @BAPARecords Table(JobRecordID int, AmountBilled money,PercentBilled money)

	   INSERT INTO @BAPARecords
			 SELECT			 [Job Information].RecordID	AS  [Job RecordID]
						  ,SUM(CONVERT(MONEY,ISNULL([BAPA].V005,0))) AS Billed
						  --,[BAPA].V005
						  ,CAST(CAST(Round(SUM(CONVERT(MONEY,ISNULL([BAPA].V005,0)))  / 
							 CONVERT(MONEY,[Job Information].V008),2) AS money) * 100 AS NUMERIC(10,2))  AS [% Billed]
						  --,[Job Information].V008
				    FROM		[Record] [Job Information] 	
				    JOIN	Record [BAPA]  ON [BAPA].TableID = 2711		/*	Group	*/
					   AND		[BAPA].IsActive = 1
					   AND	     CAST([BAPA].V002 as int) = [Job Information].RecordID 
					   AND		[BAPA].V029 = 'Invoice'

				WHERE		[Job Information].TableID = 2475		/*	Job Information	*/
				AND			[Job Information].IsActive = 1 
				AND [Job Information].V021 IN ('Bid')
				-- AND [Job Information].V027 = '15154968' 
				GROUP BY [Job Information].RecordID,[Job Information].V008

		  --SELECT * FROM @BAPARecords
	   /* == End Red == */


SET @SQL = '


SELECT			 [Job Information].RecordID				AS  [Job RecordID]
				,[Group].V001						AS  [Group Division]
				,[Job Information].V001				AS  [Job No]
				,[Job Information].V003				AS  [Project Title]
				,NULL --[Job Notes].V003					AS  [Note Date]
				,NULL --[Job Notes].V006					AS  [Note]	
				,NULL --ISNULL([Employee_Note_Initial].V004,'''')		AS  Initials
				,ISNULL([Controller].V003,'''')		AS  Controller
				,[Job Information].V008				AS  [Project Value]
				,[Job Information].V009				AS  [Fixed Price]
				,ISNULL([Office Name].[V001],''-- NONE --'')	AS Company
				--,(SELECT SUM(CONVERT(MONEY,ISNULL(BILLS.V005,0))) FROM RECORD BILLS WHERE BILLS.TABLEID = 2711 AND BILLS.ISACTIVE =1 AND BILLS.V029 = ''Invoice'' AND CAST(BILLS.V002 as int) = [Job Information].RECORDID) AS Billed
				,NULL
				--,(SELECT COUNT(RECORDID) FROM RECORD BILLS WHERE BILLS.TABLEID = 2711 AND BILLS.ISACTIVE =1 AND BILLS.V029 = ''Invoice'' AND BILLS.V002 = CAST([Job Information].RECORDID AS VARCHAR))  as countBAPA
				--,CASE 
				--    WHEN [Job Information].V008 IS NULL THEN NULL
				--    WHEN (SELECT COUNT(RECORDID) FROM RECORD BILLS WHERE BILLS.TABLEID = 2711 AND BILLS.ISACTIVE =1 AND BILLS.V029 = ''Invoice'' AND BILLS.V002 = CAST([Job Information].RECORDID AS VARCHAR)) <= 0 THEN NULL
				--    ELSE CAST(CAST(Round((SELECT SUM(CONVERT(MONEY,ISNULL(BILLS.V005,0))) FROM RECORD BILLS WHERE BILLS.TABLEID = 2711 AND BILLS.ISACTIVE =1 AND BILLS.V029 = ''Invoice'' AND BILLS.V002 = CAST([Job Information].RECORDID AS VARCHAR)) / 
				--    CONVERT(MONEY,[Job Information].V008),2) AS money) * 100 AS NUMERIC(10,2)) END AS [% Billed]
				,NULL
				--    , (SELECT ISNULL(SUM(CONVERT(MONEY,ISNULL([Timesheet Details].V007,0))),0) FROM [Record] [Timesheet Details] WHERE [Timesheet Details].TableID = 2729 AND [Timesheet Details].IsActive =1 AND [Timesheet Details].V001 = CAST([Job Information].RECORDID AS VARCHAR)) AS Timesheet
				,NULL
				    --,CASE 
					   --WHEN [Job Information].V009 = ''True'' AND [Job Information].V008 IS NOT NULL THEN 
						  --CASE WHEN (SELECT COUNT(RECORDID) FROM RECORD [Timesheet Details] WHERE [Timesheet Details].TABLEID = 2729 AND [Timesheet Details].ISACTIVE =1 AND [Timesheet Details].V001 = CAST([Job Information].RECORDID AS VARCHAR)) <= 0 THEN 0
						  --ELSE ISNULL(CAST(CAST(Round((SELECT SUM(CONVERT(MONEY,ISNULL([Timesheet Details].V007,0))) FROM RECORD [Timesheet Details] WHERE [Timesheet Details].TABLEID = 2729 AND [Timesheet Details].ISACTIVE =1  AND [Timesheet Details].V001 = CAST([Job Information].RECORDID AS VARCHAR)) / 
						  --CONVERT(MONEY,[Job Information].V008),2) AS money) * 100 AS NUMERIC(10,2)),0) END ELSE 0 END AS [% Timesheet]
				,NULL
	  ,NULL,NULL,NULL
	
	
					    FROM		[Record] [Job Information] 
	
					    LEFT JOIN	Record [Group]  ON					[Group].TableID = 2727		/*	Group	*/
															    AND		[Group].IsActive = 1
															    AND		[Group].RecordID  = CAST([Job Information].V027 as int)		/*	Group	*/

					    LEFT JOIN	Record [Controller] ON					[Controller].TableID = 2669		/*	Employee_List	*/
															    AND		[Controller].IsActive = 1
															    AND		[Controller].RecordID = cast([Job Information].V026 as int)		/*	Controller	*/
					    LEFT JOIN	Record [Office Name]  ON					[Office Name].TableID = 2474		/*	Office Information	*/
															    AND		[Office Name].IsActive = 1
															     AND		[Office Name].RecordID = cast([Job Information].V024 as int)		/*	Office Name	*/
	WHERE		[Job Information].TableID = 2475		/*	Job Information	*/
	AND			[Job Information].IsActive = 1 
     AND [Job Information].V021 IN (''Bid'')
	'
	+ @SQLJobRecordIDs + ' '
	+ @SelectedGroup + ' '
	+ @SelectedProjectController 
	

	PRINT @SQL
	INSERT INTO @JobApprovalFollowupFinal EXEC(@SQL);

	--SELECT 
	--	  [Counter] ,
	--	  [Group Division] ,
	--	  [Job No] ,
	--	  [Project Title] ,
	--	  [Note Date] ,
	--	  [Note] ,
	--	  Initials ,
	--	  Controller ,
	--	  ISNULL([Project Value],0) AS [Project Value] ,
	--	  [Fixed Price] ,
	--	  Company ,
	--	  ISNULL(Billed,0) AS Billed ,
	--	  ISNULL([% Billed],0) AS [Percent Billed] ,
	--	  TimeSheet,
	--	  [% TimeSheet]
 --   FROM @JobApprovalFollowup 




	--DELETE FROM @JobApprovalFollowup
	--WHERE Timesheet < @MinimumValue AND [% Timesheet] < @MinimumPercentage
	

	--SELECT 
	--	  [Group Division] ,
	--	  [Job No] ,
	--	  [Project Title] ,
	--	  [Note Date] ,
	--	  [Note] ,
	--	  Initials ,
	--	  Controller ,
	--	  ISNULL([Project Value],0) AS [Project Value] ,
	--	  [Fixed Price] ,
	--	  Company ,
	--	  ISNULL(Billed,0) AS Billed ,
	--	  ISNULL([% Billed],0) AS [Percent Billed] ,
	--	  TimeSheet,
	--	  [% TimeSheet]
 --   FROM @JobApprovalFollowup 
 --   WHERE Timesheet < @MinimumValue AND [% Timesheet] < @MinimumPercentage



	
	
/* ========================================= Get all Notes within the date range ==================================================== */

IF(
    ( SELECT COUNT(*)
      FROM @JobApprovalFollowupNotes JobApprovalFollowupNotes 
      WHERE CONVERT(DATE,JobApprovalFollowupNotes.[Note Date]) >= CAST(@DateStart AS DATE) 
      AND CONVERT(DATE,JobApprovalFollowupNotes.[Note Date]) <= CAST(@DateEnd AS DATE)
    ) > 0 
 )
	   BEGIN
			 /* LETS GET THE Notes within the date range */
			 INSERT INTO @JobApprovalFollowupFinal
				SELECT   
				    JobApprovalFollowupNotes.JobRecordID ,
				    JobApprovalFollowupNotes.[Group Division] ,
				    JobApprovalFollowupNotes.[Job No] ,
				    JobApprovalFollowupNotes.[Project Title] ,
				    JobApprovalFollowupNotes.[Note Date] ,
				    JobApprovalFollowupNotes.[Note] ,
				    JobApprovalFollowupNotes.Initials ,
				    JobApprovalFollowupNotes.Controller ,
				    JobApprovalFollowupNotes.[Project Value] ,
				    JobApprovalFollowupNotes.[Fixed Price] ,
				    JobApprovalFollowupNotes.Company ,
				    JobApprovalFollowupNotes.Billed ,
				    JobApprovalFollowupNotes.[% Billed] ,
				    JobApprovalFollowupNotes.Timesheet ,
				    JobApprovalFollowupNotes.[% Timesheet] ,
					JobApprovalFollowupNotes.NoteRecordID ,
				    NULL,NULL
			 FROM @JobApprovalFollowupNotes JobApprovalFollowupNotes
		  WHERE CONVERT(DATE,JobApprovalFollowupNotes.[Note Date]) >= CAST(@DateStart AS DATE) and CONVERT(DATE,JobApprovalFollowupNotes.[Note Date]) <= CAST(@DateEnd AS DATE)

	   END


	    /* Lets update the YesWithin */			
			 UPDATE wFinal 
				SET wFinal.YesWithin = 1
				FROM @JobApprovalFollowupFinal wFinal
				JOIN (
				    SELECT JobRecordID 
				    FROM @JobApprovalFollowupFinal wf						    	 
				    GROUP BY JobRecordID
				    HAVING COUNT(*) >1 
				) WF on
				    wFinal.JobRecordID = WF.JobRecordID

				 


/* ========================================= LETS GET THE MOST RECENT NOTES WHEN JOB HAS NO NOTES WITHIN THE DATES RANGE ==================================================== */


    DECLARE @ApprovalJobsBelowNotes TABLE (-- @WorkLoadJobsBelowNotes TABLE (
		  [Counter] int identity(1,1),
		  [NoteDate] date,
		  [NoteRecordID] int
	   )
	   
	   INSERT INTO @ApprovalJobsBelowNotes ([NoteDate],[NoteRecordID])
	   SELECT  
		   MostRecentBelowRange2.[Note Date]  ,
		   MostRecentBelowRange2.[NoteRecordID] --,
		  FROM @JobApprovalFollowupFinal MostRecentBelowRange 
		  JOIN(
			 SELECT  
				[Note Date]  ,
				[NoteRecordID] ,
				[JobRecordID]  ,  
				ROW_NUMBER() OVER (PARTITION BY MostRecentBelowRange1.[JobRecordID] ORDER BY CONVERT(date,MostRecentBelowRange1.[Note Date]) desc) rank
			  FROM @JobApprovalFollowupNotes MostRecentBelowRange1
			  WHERE CONVERT(DATE,MostRecentBelowRange1.[Note Date]) > CAST(@DateEnd AS DATE)
	
			 ) MostRecentBelowRange2
		  ON MostRecentBelowRange2.rank = 1 and 
		  MostRecentBelowRange2.[JobRecordID] = MostRecentBelowRange.[JobRecordID] 
		  WHERE MostRecentBelowRange.[Note Date] IS NULL 
		  ORDER BY MostRecentBelowRange.[Job No]

		  
		IF ((SELECT COUNT(*) FROM @ApprovalJobsBelowNotes) > 0)
		BEGIN
		    INSERT INTO @JobApprovalFollowupFinal
				SELECT   
				    JobApprovalFollowupNotes.JobRecordID ,
				    JobApprovalFollowupNotes.[Group Division] ,
				    JobApprovalFollowupNotes.[Job No] ,
				    JobApprovalFollowupNotes.[Project Title] ,
				    JobApprovalFollowupNotes.[Note Date] ,
				    JobApprovalFollowupNotes.[Note] ,
				    JobApprovalFollowupNotes.Initials ,
				    JobApprovalFollowupNotes.Controller ,
				    JobApprovalFollowupNotes.[Project Value] ,
				    JobApprovalFollowupNotes.[Fixed Price] ,
				    JobApprovalFollowupNotes.Company ,
				    JobApprovalFollowupNotes.Billed ,
				    JobApprovalFollowupNotes.[% Billed] ,
				    JobApprovalFollowupNotes.Timesheet ,
				    JobApprovalFollowupNotes.[% Timesheet] ,
					JobApprovalFollowupNotes.NoteRecordID ,
				    NULL,NULL
			 FROM @JobApprovalFollowupNotes JobApprovalFollowupNotes
			 JOIN @ApprovalJobsBelowNotes ApprovalJobsBelowNotes ON ApprovalJobsBelowNotes.[NoteRecordID] = JobApprovalFollowupNotes.[NoteRecordID]
			 AND ApprovalJobsBelowNotes.[NoteDate] = JobApprovalFollowupNotes.[Note Date]

			 DELETE  FROM @ApprovalJobsBelowNotes

			 /* GET Second dates i.e. if records have the same dates */
			 INSERT INTO @ApprovalJobsBelowNotes ([NoteDate],[NoteRecordID])
				SELECT  
				    MostRecentBelowRange2.[Note Date]  ,
				    MostRecentBelowRange2.[NoteRecordID] 
				FROM @JobApprovalFollowupFinal MostRecentBelowRange JOIN(
			    SELECT 
				    [Note Date]  ,
				    [NoteRecordID] ,
				    [JobRecordID]  --,  
				    FROM @JobApprovalFollowupNotes MostRecentBelowRange1
				    WHERE CONVERT(DATE,MostRecentBelowRange1.[Note Date]) > CAST(@DateEnd AS DATE)
	   
	
				    ) MostRecentBelowRange2
				ON  MostRecentBelowRange.[JobRecordID] = MostRecentBelowRange2.[JobRecordID]
				AND  MostRecentBelowRange.[Note Date] = MostRecentBelowRange2.[Note Date]
				AND  MostRecentBelowRange.[NoteRecordID] <> MostRecentBelowRange2.[NoteRecordID]
  
			   INSERT INTO @JobApprovalFollowupFinal
				SELECT   
				    JobApprovalFollowupNotes.JobRecordID ,
				    JobApprovalFollowupNotes.[Group Division] ,
				    JobApprovalFollowupNotes.[Job No] ,
				    JobApprovalFollowupNotes.[Project Title] ,
				    JobApprovalFollowupNotes.[Note Date] ,
				    JobApprovalFollowupNotes.[Note] ,
				    JobApprovalFollowupNotes.Initials ,
				    JobApprovalFollowupNotes.Controller ,
				    JobApprovalFollowupNotes.[Project Value] ,
				    JobApprovalFollowupNotes.[Fixed Price] ,
				    JobApprovalFollowupNotes.Company ,
				    JobApprovalFollowupNotes.Billed ,
				    JobApprovalFollowupNotes.[% Billed] ,
				    JobApprovalFollowupNotes.Timesheet ,
				    JobApprovalFollowupNotes.[% Timesheet] ,
					JobApprovalFollowupNotes.NoteRecordID ,
				    NULL,NULL
			 FROM @JobApprovalFollowupNotes JobApprovalFollowupNotes
			  JOIN @ApprovalJobsBelowNotes ApprovalJobsBelowNotes ON ApprovalJobsBelowNotes.[NoteRecordID] = JobApprovalFollowupNotes.[NoteRecordID]
			  AND ApprovalJobsBelowNotes.[NoteDate] = JobApprovalFollowupNotes.[Note Date]


			   DELETE  FROM @ApprovalJobsBelowNotes

			 /* Lets update the YesBeyond */			  
			  	UPDATE wFinal 
				SET wFinal.YesBeyond = 1
				FROM @JobApprovalFollowupFinal wFinal
				JOIN (
				    SELECT JobRecordID 
				    FROM @JobApprovalFollowupFinal wf
				    --WHERE wf.YesWithin = 1	
				    WHERE [Note Date] >  CAST(@DateEnd AS DATE) 
				    GROUP BY JobRecordID
				    HAVING COUNT(*) >1 
				) WF on
				    wFinal.JobRecordID = WF.JobRecordID			
				   
		  END



/* ============================================================ Before Date Range =============================================================================================================================================== */  

		

		  DELETE  FROM @ApprovalJobsBelowNotes
		  INSERT INTO @ApprovalJobsBelowNotes ([NoteDate],[NoteRecordID])
			 SELECT  
				MostRecentBelowRange2.[Note Date]  ,
				MostRecentBelowRange2.[NoteRecordID] --,
			 FROM @JobApprovalFollowupFinal MostRecentBelowRange JOIN(
			 SELECT  
				[Note Date]  ,
				[NoteRecordID] ,
				[JobRecordID]  ,  
				ROW_NUMBER() OVER (PARTITION BY MostRecentBelowRange1.[JobRecordID] ORDER BY CONVERT(date,MostRecentBelowRange1.[Note Date]) desc) rank
			   FROM @JobApprovalFollowupNotes MostRecentBelowRange1
			   WHERE CONVERT(DATE,MostRecentBelowRange1.[Note Date]) < CAST(@DateStart AS DATE)
	
				) MostRecentBelowRange2
			 ON MostRecentBelowRange2.rank = 1 and 
			 MostRecentBelowRange2.[JobRecordID] = MostRecentBelowRange.[JobRecordID]
 
			 WHERE MostRecentBelowRange.[Note Date] is null 
			 AND MostRecentBelowRange.YesWithin IS NULL AND MostRecentBelowRange.YesBeyond IS NULL
			 --AND MostRecentBelowRange.NoteRecordID is not null
			
			 ORDER BY MostRecentBelowRange.[Job No]

	IF ((SELECT COUNT(*) FROM @ApprovalJobsBelowNotes) > 0)
		BEGIN
		  INSERT INTO @JobApprovalFollowupFinal
				SELECT   
				    JobApprovalFollowupNotes.JobRecordID ,
				    JobApprovalFollowupNotes.[Group Division] ,
				    JobApprovalFollowupNotes.[Job No] ,
				    JobApprovalFollowupNotes.[Project Title] ,
				    JobApprovalFollowupNotes.[Note Date] ,
				    JobApprovalFollowupNotes.[Note] ,
				    JobApprovalFollowupNotes.Initials ,
				    JobApprovalFollowupNotes.Controller ,
				    JobApprovalFollowupNotes.[Project Value] ,
				    JobApprovalFollowupNotes.[Fixed Price] ,
				    JobApprovalFollowupNotes.Company ,
				    JobApprovalFollowupNotes.Billed ,
				    JobApprovalFollowupNotes.[% Billed] ,
				    JobApprovalFollowupNotes.Timesheet ,
				    JobApprovalFollowupNotes.[% Timesheet] ,
					JobApprovalFollowupNotes.NoteRecordID ,
				    NULL,NULL
			 FROM @JobApprovalFollowupNotes JobApprovalFollowupNotes
			  JOIN @ApprovalJobsBelowNotes ApprovalJobsBelowNotes ON ApprovalJobsBelowNotes.[NoteRecordID] = JobApprovalFollowupNotes.[NoteRecordID]
			  AND ApprovalJobsBelowNotes.[NoteDate] = JobApprovalFollowupNotes.[Note Date]
			 AND JobApprovalFollowupNotes.JobRecordID not in (SELECT JobRecordID FROM @JobApprovalFollowupFinal WHERE [Note Date] > cast(@DateStart as date))
			  DELETE  FROM @ApprovalJobsBelowNotes

			 INSERT INTO @ApprovalJobsBelowNotes ([NoteDate],[NoteRecordID])
			 SELECT  
				MostRecentBelowRange2.[Note Date]  ,
				MostRecentBelowRange2.[NoteRecordID] 
			 FROM @JobApprovalFollowupFinal MostRecentBelowRange JOIN(
			    SELECT  
				[Note Date]  ,
				[NoteRecordID] ,
				[JobRecordID]  
				    FROM @JobApprovalFollowupNotes MostRecentBelowRange1
				    WHERE CONVERT(DATE,MostRecentBelowRange1.[Note Date]) < CAST(@DateStart AS DATE)
	   
	
				    ) MostRecentBelowRange2
				ON  MostRecentBelowRange.[JobRecordID] = MostRecentBelowRange2.[JobRecordID]
				AND  MostRecentBelowRange.[Note Date] = MostRecentBelowRange2.[Note Date]
				AND  MostRecentBelowRange.[NoteRecordID] <> MostRecentBelowRange2.[NoteRecordID]
				 AND MostRecentBelowRange.YesWithin IS NULL AND MostRecentBelowRange.YesBeyond IS NULL
  


			    INSERT INTO @JobApprovalFollowupFinal
				SELECT   
				    JobApprovalFollowupNotes.JobRecordID ,
				    JobApprovalFollowupNotes.[Group Division] ,
				    JobApprovalFollowupNotes.[Job No] ,
				    JobApprovalFollowupNotes.[Project Title] ,
				    JobApprovalFollowupNotes.[Note Date] ,
				    JobApprovalFollowupNotes.[Note] ,
				    JobApprovalFollowupNotes.Initials ,
				    JobApprovalFollowupNotes.Controller ,
				    JobApprovalFollowupNotes.[Project Value] ,
				    JobApprovalFollowupNotes.[Fixed Price] ,
				    JobApprovalFollowupNotes.Company ,
				    JobApprovalFollowupNotes.Billed ,
				    JobApprovalFollowupNotes.[% Billed] ,
				    JobApprovalFollowupNotes.Timesheet ,
				    JobApprovalFollowupNotes.[% Timesheet] ,
					JobApprovalFollowupNotes.NoteRecordID ,
				    NULL,NULL
			 FROM @JobApprovalFollowupNotes JobApprovalFollowupNotes
			  JOIN @ApprovalJobsBelowNotes ApprovalJobsBelowNotes ON ApprovalJobsBelowNotes.[NoteRecordID] = JobApprovalFollowupNotes.[NoteRecordID]
			  AND ApprovalJobsBelowNotes.[NoteDate] = JobApprovalFollowupNotes.[Note Date]
			   AND JobApprovalFollowupNotes.JobRecordID not in (SELECT JobRecordID FROM @JobApprovalFollowupFinal WHERE [Note Date] > cast(@DateStart as date))
			  --END
		  
		  
		  /* Lets update the YesBeyond */			
			 UPDATE wFinal 
				SET wFinal.YesBeyond = 1
				FROM @JobApprovalFollowupFinal wFinal
				JOIN (
				    SELECT JobRecordID 
				    FROM @JobApprovalFollowupFinal wf	
				      WHERE [Note Date] <  CAST(@DateStart AS DATE) 				 
				    GROUP BY JobRecordID
				    HAVING COUNT(*) > 1 
				) WF on
				    wFinal.JobRecordID = WF.JobRecordID			

		END

		    


    UPDATE A 
    SET A.[Group Division] = B.[Group Division], A.Controller = B.Controller,
	   A.[Timesheet] = B.[Timesheet], A.[% Timesheet] = B.[% Timesheet]
    FROM @JobApprovalFollowupFinal A 
    JOIN @JobApprovalFollowupFinal B ON A.JobRecordID = B.JobRecordID
    AND B.[Group Division] IS NOT NULL AND B.Company IS NOT NULL
    WHERE A.[Group Division] IS NULL AND A.Controller IS NULL


    UPDATE A 
    SET A.Billed = B.[AmountBilled], A.[% Billed] = B.[PercentBilled]
    FROM @JobApprovalFollowupFinal A 
    JOIN @BAPARecords B ON A.JobRecordID = B.JobRecordID
   -- WHERE A.[Group Division] IS NULL AND A.Controller IS NULL


   UPDATE @JobApprovalFollowupFinal
   SET YesBeyond = 1
   WHERE ([Note Date] IS NOT NULL)
   AND (([Note Date] > CAST(@DateEnd as Date)) OR ([Note Date] < CAST(@DateStart as Date))) 


SELECT 
		  JobRecordID ,
		  [Group Division] ,
		  [Job No],
		  [Project Title] ,
		  CONVERT(VARCHAR(10),  [Note Date], 103) as [Note Date],
		  [Note] ,
		  Initials ,
		  Controller ,
		  [Project Value] ,
		  [Fixed Price] ,
		  Company ,
		  Billed ,
		  [% Billed] ,
		  Timesheet ,
		  [% Timesheet] ,
		   NoteRecordID ,
		  YesWithin ,
		  YesBeyond  
		  FROM @JobApprovalFollowupFinal
		  --WHERE [JOB NO] =  'WB963-01'
--WHERE ((Timesheet >= @MinimumValue) OR ([% Timesheet]>=@MinimumPercentage)) 
--AND 
--[Job No] = 'TK660-01'

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
	 '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
	 '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
	 '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('usr_rptJobApprovalFollowup', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
