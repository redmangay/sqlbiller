﻿

CREATE PROCEDURE [dbo].[ets_Column_Details_By_Sys]
(
	@iTableID int,
	@sSystemName varchar(50)
	 
)
AS

BEGIN TRY
	SET NOCOUNT ON;

	SELECT     [Table].TableName, [Column].*
		FROM   [Column] 
		JOIN	[Table] ON [Column].TableID = [Table].TableID
		WHERE [Column].SystemName = @sSystemName AND [Column].TableID=@iTableID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Column_Details_By_Sys', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
