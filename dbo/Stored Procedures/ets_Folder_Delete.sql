﻿

CREATE PROCEDURE [dbo].[ets_Folder_Delete]
(
	@nFolderID int 
)
	/*----------------------------------------------------------------

	EXEC ets_Folder_Delete 2
	---------------------------------------------------------------*/
 
AS

BEGIN TRY
	BEGIN TRANSACTION --beginning a transaction..


	;WITH T(xParent, xChild)AS
	(
	SELECT ParentFolderID, FolderID FROM Folder WHERE ParentFolderID=@nFolderID
	UNION ALL
	SELECT ParentFolderID, FolderID FROM Folder INNER JOIN T ON ParentFolderID=xChild
	)
	DELETE  FROM Document WHERE FolderID IN (SELECT xChild FROM T)


	;WITH T(xParent, xChild)AS
	(
	SELECT ParentFolderID, FolderID FROM Folder WHERE ParentFolderID=@nFolderID
	UNION ALL
	SELECT ParentFolderID, FolderID FROM Folder INNER JOIN T ON ParentFolderID=xChild
	)
	DELETE  FROM UserFolder WHERE FolderID IN (SELECT xChild FROM T)


	DELETE  FROM Document WHERE FolderID =@nFolderID
	DELETE  FROM UserFolder WHERE FolderID =@nFolderID


	;WITH T(xParent, xChild)AS
	(
	SELECT ParentFolderID, FolderID FROM Folder WHERE ParentFolderID=@nFolderID
	UNION ALL
	SELECT ParentFolderID, FolderID FROM Folder INNER JOIN T ON ParentFolderID=xChild
	)
	DELETE  FROM Folder WHERE FolderID IN (SELECT xChild FROM T)

	DELETE  FROM Folder WHERE FolderID =@nFolderID


	COMMIT TRANSACTION --finally, Commit the transaction as all Success.

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Folder_Delete', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
