﻿-- =============================================
-- Author:		<Red Mangay>
-- Create date:	<10/07/2018>
-- Description:	<Get the tableid of the orginal table ie timesheet or disbursement>
-- =============================================
/*

exec [GetTimeSheetTableIDFrom] 2713164

*/


CREATE PROCEDURE [dbo].[Renzo_EnableTimeSheetDetailsStatus]
(@RecordID  INT,
 @FormSetID INT          = NULL,
 @UserID    INT          = NULL,
 @Return    VARCHAR(MAX) OUTPUT
)
AS
BEGIN
	
		
		  --UPDATE [COLUMN]
    --                   SET
    --                       [ISREADONLY] = 0
    --                 WHERE [COLUMNID] IN(49888, 49889, 49887, 50073
				--			 ,49884, 49890)
    --                 AND [TableID] = 2729;

				UPDATE [COLUMN]
                    SET
                    [DisplayTextDetail] = NULL
                    WHERE [COLUMNID] IN(50090, 50091, 50092, 50093
				,50094, 50095, 50074) --, 49890) lacking Job no
                     AND [TableID] = 2729;

				

				 UPDATE [COLUMN]
				 SET [DisplayTextDetail] = 'Done'
				 WHERE [COLUMNID] = 50075
				 AND [TableID] = 2729;
				

				 UPDATE [COLUMN]
				 SET [DisplayTextDetail] = 'Hr/Unit'
				 WHERE [COLUMNID] = 49888
				 AND [TableID] = 2729;

				  UPDATE [COLUMN]
				 SET [DisplayTextDetail] = 'Rate'
				 WHERE [COLUMNID] = 49889
				 AND [TableID] = 2729;

				  UPDATE [COLUMN]
				 SET [DisplayTextDetail] = '(X)'
				 WHERE [COLUMNID] = 49887
				 AND [TableID] = 2729;

				   UPDATE [COLUMN]
				 SET [DisplayTextDetail] = 'Status'
				 WHERE [COLUMNID] = 50073
				 AND [TableID] = 2729;

				 UPDATE [COLUMN]
				 SET [DisplayTextDetail] = 'Job No'
				 WHERE [COLUMNID] = 49884
				 AND [TableID] = 2729;

				  UPDATE [COLUMN]
				 SET [DisplayTextDetail] = '$ Amount'
				 WHERE [COLUMNID] = 49890
				 AND [TableID] = 2729;

				 


END

