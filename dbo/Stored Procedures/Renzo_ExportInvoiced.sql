﻿CREATE PROCEDURE [dbo].[Renzo_ExportInvoiced]
(
	@RecordID  int,		/*	Record ID passed from the table on execute, base your solution on this value	*/
	@UserID int=null,
	@Return varchar(max) output
)
AS

	
 SELECT 
	  [Job Information].[V001] AS [Job Number]
	 ,[Bill & Payment Activity].[V004] AS [Date]
	 ,[Bill & Payment Activity].[V017] AS [Invoice No]
	 ,[Bill & Payment Activity].[V005] AS [Invoice Amount]
	 ,[Bill & Payment Activity].[V028] AS [% Complete]
	 ,[Bill & Payment Activity].[V015] AS [Description]
 
 FROM [Record] [Bill & Payment Activity] WITH (NOLOCK) 
 JOIN [Record] [Job Information] ON [Job Information].[TableID] = 2475
			   AND CAST([Job Information].[RecordID] AS VARCHAR)= [Bill & Payment Activity].[V002]
			   AND [Job Information].[IsActive] = 1

 WHERE [Bill & Payment Activity].[TableID] = 2711	AND
	   [Bill & Payment Activity].[IsActive] = 1		AND
	   [Bill & Payment Activity].[RecordID] = @RecordID

RETURN @@ERROR;