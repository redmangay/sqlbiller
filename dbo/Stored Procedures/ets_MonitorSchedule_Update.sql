﻿
 
CREATE PROCEDURE [dbo].[ets_MonitorSchedule_Update]
(
	@nMonitorScheduleID int ,
	@nAccountID int ,
	@nTableID int  = NULL,
	@dScheduleDateTime datetime ,
	@sDescription nvarchar(250) ,
	@bHasAlarm bit ,
	@dAlarmDateTime datetime  = NULL,
	@nUserUpdated int  = NULL,
	@nInitialScheduleID int=NULL
)
	/*----------------------------------------------------------------
	-- Stored Procedure: ets_MonitorSchedule_Update
	-- Generated by: gpGenerateSP (Jon Bosker, DB Gurus Australia) 
	-- Date: Oct 19 2011  2:56PM
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	UPDATE [MonitorSchedule]
	SET 
		AccountID = @nAccountID,
		TableID = @nTableID,
		ScheduleDateTime = @dScheduleDateTime,
		Description = @sDescription,
		HasAlarm = @bHasAlarm,
		AlarmDateTime = @dAlarmDateTime,	
		UserUpdated = @nUserUpdated,
		InitialScheduleID=@nInitialScheduleID
	WHERE MonitorScheduleID = @nMonitorScheduleID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_MonitorSchedule_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
