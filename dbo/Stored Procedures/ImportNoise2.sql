﻿

CREATE PROCEDURE [dbo].[ImportNoise2]

AS

BEGIN TRY
	--Temp. Config Table
	DECLARE @tvSitesInfo table(
		AccountID int NOT NULL,
		AccountName varchar(MAX),
		NoiseTableID int,
		SiteInfoTableID int,
		SiteID int,
		SiteName varchar(MAX),
		SiteNameOnImportCol varchar(MAX),
		SiteNameOnImport varchar(MAX));

	--Get All Necessary IDs
	INSERT  INTO @tvSitesInfo( AccountID,	AccountName,    NoiseTableID,	SiteInfoTableID,   SiteID, SiteName,    SiteNameOnImportCol)
		SELECT        t.AccountID, a.AccountName, t.NoiseTableID, s.SitesTableID, s1.SiteID, s1.SiteName, i.SystemName AS SiteNameOnImportCol  
	FROM            (SELECT        RecordID AS SiteID, TableID, V001 AS SiteName
							  FROM            Record
							  WHERE        (IsActive = 1)) AS s1 INNER JOIN
								 (SELECT        AccountID, AccountName
								   FROM            Account
								   WHERE        (IsActive = 1)) AS a INNER JOIN
								 (SELECT        TableID AS NoiseTableID, AccountID
								   FROM            [Table]
								   WHERE        (TableName = N'Noise') AND (IsActive = 1)) AS t ON a.AccountID = t.AccountID INNER JOIN
								 (SELECT        TableID AS SitesTableID, AccountID
								   FROM            [Table] AS Table_1
								   WHERE        (TableName LIKE N'%Site%Noise%') AND (IsActive = 1)) AS s ON t.AccountID = s.AccountID ON s1.TableID = s.SitesTableID LEFT OUTER JOIN
								 (SELECT        TableID, SystemName
								   FROM            [Column]
								   WHERE        (DisplayName = 'Site Name On Import File')) AS i ON s.SitesTableID = i.TableID

	--Get SiteNameOnImport value IF filled
	UPDATE @tvSitesInfo 
	SET SiteNameOnImport=(SELECT CASE 
	 WHEN t.SiteNameOnImportCol='V020' THEN V020 
	 WHEN t.SiteNameOnImportCol='V021' THEN V021 
	 WHEN t.SiteNameOnImportCol='V022' THEN V022 
	 WHEN t.SiteNameOnImportCol='V023' THEN V023 
	 WHEN t.SiteNameOnImportCol='V024' THEN V024 
	 WHEN t.SiteNameOnImportCol='V025' THEN V025 
	 WHEN t.SiteNameOnImportCol='V026' THEN V026 
	 WHEN t.SiteNameOnImportCol='V027' THEN V027 
	 WHEN t.SiteNameOnImportCol='V028' THEN V028 
	 WHEN t.SiteNameOnImportCol='V029' THEN V029 
	 ELSE NULL END 
	FROM Record WHERE (RecordID = t.SiteID))  from @tvSitesInfo t;

	--debug
	--SELECT * from @tvSitesInfo;

	WITH ContinuousNoiseData(NoiseTableID, AccountID, SiteID, SiteName, AccountName, StatDate,  LA1_max, LeqDay, LFDay, LeqEvening, LFEvening, LeqNight, LFNight ) AS 
	(SELECT    distinct    NoiseTableID, AccountID, SiteID, SiteName, AccountName, StatDate,   LA1_max, LeqDay, LFDay, LeqEvening, LFEvening, LeqNight, LFNight
	FROM            (SELECT        t.NoiseTableID, t.AccountID, t.SiteID, COALESCE(t.SiteNameOnImport, t.SiteName) as SiteName, t.AccountName, xcnemd_upload.dbo.ContinuousNoise.StatDate, 
							 xcnemd_upload.dbo.ContinuousNoise.LA1_max, xcnemd_upload.dbo.ContinuousNoise.LeqDay, xcnemd_upload.dbo.ContinuousNoise.LFDay, xcnemd_upload.dbo.ContinuousNoise.LeqEvening, xcnemd_upload.dbo.ContinuousNoise.LFEvening, xcnemd_upload.dbo.ContinuousNoise.LeqNight, xcnemd_upload.dbo.ContinuousNoise.LFNight
	FROM            @tvSitesInfo t LEFT OUTER JOIN
							 xcnemd_upload.dbo.ContinuousNoise ON REPLACE(REPLACE(REPLACE(COALESCE(t.SiteNameOnImport, t.SiteName),' ',''),CHAR(9),''),'SX','') = xcnemd_upload.dbo.ContinuousNoise.UnitId + '-' + xcnemd_upload.dbo.ContinuousNoise.ModId) 
							 AS tmp
	WHERE        (StatDate IS NOT NULL))

	INSERT INTO Record
							 (TableID, Notes, EnteredBy, V001, V002, V003, V004, V005, V006, V007, V008, V009)
		SELECT        c.NoiseTableID, 'Imported from xcnemd_upload', 61, c.SiteID, CONVERT(nvarchar(MAX), CONVERT(smalldatetime, c.StatDate, 102), 103), 
			CASE WHEN c.LA1_max='-99' THEN NULL ELSE c.LA1_max END,
			CASE WHEN c.LeqDay='-99' THEN NULL ELSE c.LeqDay END,
			CASE WHEN c.LFDay='-99' THEN NULL ELSE c.LFDay END,
			CASE WHEN c.LeqEvening='-99' THEN NULL ELSE c.LeqEvening END,
			CASE WHEN c.LFEvening='-99' THEN NULL ELSE c.LFEvening END,
			CASE WHEN c.LeqNight='-99' THEN NULL ELSE c.LeqNight END,
			CASE WHEN c.LFNight='-99' THEN NULL ELSE c.LFNight END
		 FROM     ContinuousNoiseData AS c LEFT OUTER JOIN
				  Record AS r ON c.NoiseTableID = r.TableID AND CAST(c.SiteID AS nvarchar(MAX)) = r.V001 AND CONVERT(nvarchar(MAX), CONVERT(smalldatetime, c.StatDate, 102), 103) = r.V002
		 WHERE        (r.V001 IS NULL)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ImportNoise2', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
