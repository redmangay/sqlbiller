﻿

CREATE PROCEDURE [dbo].[ets_Chart_GetData_Dust]
       -- Add the parameters for the stored procedure here
       @nTableID int,
	   @sGraphXAxisColumnID varchar(MAX),
       @sValueFields varchar(MAX),
       @dStartDate datetime,
       @dEndDate datetime,
       @sPeriod varchar(20), -- Possible values: Year, Month, Day, Hour
       -- When you pass Year it will return the month averages
       -- when you pass Month it will return the day averages
       -- when you pass Week it will return the day averages
       -- when you pass day it will return hour aveages
       -- when you pass hour it will return all data
	   @GraphSeriesColumnID varchar(MAX),
	   @GraphSeriesID varchar(MAX),
	   @nDataColumn1ID int = NULL,
	   @nDataColumn2ID int = NULL,
	   @nDataColumn3ID int = NULL,
	   @nDataColumn4ID int = NULL,
	   @ForceAverage bit,
       @bPercent bit= 0,
	   @nMaxRows int = NULL
AS
/*
       PRINT SYSDATETIME()
       EXEC ets_Chart_GetData_New
              @nTableID=919
              ,@sValueFields = 'V002'
              ,@dStartDate='2012-12-07 12:28'
              ,@dEndDate='2013-12-09 16:00'
              ,@sPeriod='Hour'
              ,@bPercent=1,@nMaxRows=200
       PRINT SYSDATETIME()
       -- 3603515 1st run
       -- 2919922 2nd run : no IF
       -- 2529297 3rd run : using IN
*/

BEGIN TRY
	SET dateformat dmy

	SELECT V001, [Feb-2015], [Mar-2015], [Apr-2015]
	FROM 
	(SELECT V001, LEFT(DATENAME(month, CAST(V002 AS datetime)),3) + '-' + CAST(YEAR(CAST(V002 AS datetime)) AS char(4)) AS Period,
		SUM(CAST(dbo.RemoveNonNumericChar(V004) AS decimal (18,4))) AS Value
		FROM [Record]
		WHERE CAST(V002 AS datetime) BETWEEN '1 Feb 2015 0:00:00' AND '30 Apr 2015 23:59:59'
			AND TableID = 2466
		GROUP BY V001, LEFT(DATENAME(month, CAST(V002 AS datetime)),3) + '-' + CAST(YEAR(CAST(V002 AS datetime)) AS char(4))) x
	PIVOT
	(
		MAX(Value)
		FOR Period IN ([Feb-2015], [Mar-2015], [Apr-2015])
	) y
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Chart_GetData_Dust', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
