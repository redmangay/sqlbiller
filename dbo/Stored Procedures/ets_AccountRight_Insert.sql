﻿
 
CREATE PROCEDURE [dbo].[ets_AccountRight_Insert]
(
	@nNewID int output,
	@nAccountID int ,
	@nRightId int ,
	@sResource nvarchar(50)  = NULL
)
 
AS

BEGIN TRY 
	INSERT INTO AccountRight
	(
		AccountID, 
		RightId, 
		Resource 
	) VALUES (
		@nAccountID,
		@nRightId,
		@sResource
	)
	SELECT @nNewID = @@IDENTITY
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_AccountRight_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
