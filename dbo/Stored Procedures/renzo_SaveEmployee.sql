﻿CREATE procedure [dbo].[renzo_SaveEmployee]	

(
	@RecordID  int,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output
    , @Context varchar(max) = null
)
AS

/*
		DECLARE			@Return		VARCHAR(MAX)
		EXEC			[renzo_SaveEmployee] @RecordID = 2072086, @Return = @Return

		EXEC [dbo].[spGetTableSQL] 2669, 'Employee_list'
		EXEC [dbo].[spGetTableSQL] 2670, 'EmployeeHistory'
*/

--Creating parameter from table employee_list
Declare @RecID bigint
Declare @EnteredBy varchar(500)
declare @IsActive varchar(50)
Declare @EmployeeID varchar(500)  --V001
Declare @OldYesNo varchar(50) --V002
Declare @Name  varchar(max) --v003
Declare @SystemUser varchar(500) --V019
Declare @Initial varchar(50) --V004
Declare @Designation_Orig varchar(500) --V005
Declare @Designation varchar(500) -- V021
Declare @GroupDivision varchar(500) --V022
Declare @GroupDivision_old  varchar(500) -- V006
Declare @Annual_Leave_Yr varchar(500) --V008
Declare @OthersSchedule_Leave varchar(500) -- V009
Declare @Package varchar(500) --V010
Declare @ChkPackage varchar(500) --V011
Declare @Monday varchar(500) --V012
Declare @Tuesday varchar(500)--V013
Declare @Wednesday varchar(500) --V014
Declare @Thursday varchar(500) --V015
Declare @Friday varchar(500) -- V016
Declare @Saturday varchar(500)  --V017
Declare @Sunday  varchar(500) --V018
Declare @Work_Hrs_Week varchar(500) --V007
Declare @TesCheckbox varchar(max) -- V020
Declare @RenumerationHr varchar(max) -- V025
Declare @RenumerationYr varchar(max) -- V026
Declare @Renumeration varchar(max) -- V023

--		Reset values depending on package
UPDATE			[Employee_list]
SET				[Employee_list].V012	= 0		--		[Monday]
				,[Employee_list].V013	= 0		--		[Tuesday]
				,[Employee_list].V014	= 0		--		[Wednesday]
				,[Employee_list].V015	= 0		--		[Thursday]
				,[Employee_list].V016	= 0		--		[Friday]
				,[Employee_list].V017	= 0		--		[Saturday]
				,[Employee_list].V018	= 0		--		[Sunday]
				,[Employee_list].V007	= 0		--		[Work_Hrs/Week]
				,[Employee_list].V026	= 0		--		[Renumeration $/Yr]
FROM			[Record] [Employee_list]
WHERE			[Employee_list].IsActive = 1
AND				[Employee_list].TableID = '2669'
AND				[Employee_list].RecordID = @RecordID
AND				[Employee_list].V023 = '$Hourly Rate'

UPDATE			[Employee_list]
SET				[Employee_list].V025	= 0		--		[Renumeration $/Hr]
FROM			[Record] [Employee_list]
WHERE			[Employee_list].IsActive = 1
AND				[Employee_list].TableID = '2669'
AND				[Employee_list].RecordID = @RecordID
AND				[Employee_list].V023 = '$Annual Package'

--		Get the Employee  from the Employee_list record against parameter record id

SELECT		
 @RecID	=Record.Recordid,
 @EmployeeID =  [Record].[V001],
 @OldYesNo = [Record].[V002],
 @Name  = [Record].[v003],
 @SystemUser = [Record].[V019],
 @Initial = [Record].[V004],
 @Designation_Orig = [Record].[V005],
 @Designation = [Record].[V021],
 @GroupDivision = [Record].[V022],
 @GroupDivision_old  = [Record].[V006],
 @Annual_Leave_Yr = [Record].[V008],
 @OthersSchedule_Leave = [Record].[V009],
 @Package = [Record].[V010],
 @ChkPackage = [Record].[V011],
 @Monday = [Record].[V012],
 @Tuesday =[Record].[V013],
 @Wednesday = [Record].[V014],
 @Thursday = [Record].[V015],
 @Friday = [Record].[V016],
 @Saturday =  [Record].[V017],
 @Sunday  = [Record].[V018],
 @Work_Hrs_Week = [Record].[V007],
 @TesCheckbox = [Record].[V020],
 @RenumerationHr = [Record].V025,
 @RenumerationYr = [Record].V026,
 @Renumeration = [Record].V023,
 @EnteredBy =Record.EnteredBY,
 @IsActive =Record.IsActive 
 
FROM			[Record]
WHERE			[Record].TableID = 2669	-- Employee_list  table
AND				[Record].RecordID = @RecordID


-- Get the previous record from employee history to check against employee list parameter
declare @ispreviousrecordexist int
declare @numberofchange int
declare @prevEmployeeID varchar(500)
declare @prevName varchar(500)
declare @prevInitials varchar(50)
declare @prevDesignation varchar(500)
declare @prevGroup_Division varchar(500)
declare @prev_Package varchar(500)
declare @prevChkPackage varchar(500)
declare @prevOthersSchedule_Leave varchar(500)
declare @prevAnnual_Leave_Yr varchar(500)
declare @prevMonday varchar(500)
declare @prevTuesday varchar(500)
declare @prevWednesday varchar(500)
declare @prevThursday varchar(500)
declare @prevFriday varchar(500)
declare @prevSaturday varchar(500)
declare @prevSunday varchar(500)
declare @prevRenumerationHr varchar(500)
declare @prevRenumerationYr varchar(500)
declare @prevWork_Hrs_Week varchar(500)

select @ispreviousrecordexist= count(*) from [Record] WHERE			[Record].TableID = 2670 and [V021] = @RecID and ISNUMERIC( [Record].[V021]) = 1

set @ispreviousrecordexist =isnull(@ispreviousrecordexist,0)
set @numberofchange =0
if @ispreviousrecordexist >0  -- if employee exist in history table then pick latest top 1 and make difference from change data and previous data
begin
select top 1

@prevEmployeeID  =V002, --EmployeeID,
@prevName = V004,--Name
@prevInitials =V005,--Initials
@prevDesignation = V006,--Designation
@prevGroup_Division= V007,--Group/Division
@prev_Package =V011,--Package
@prevChkPackage=V012,--ChkPackage
@prevOthersSchedule_Leave=V010,--OthersSchedule_Leave
@prevAnnual_Leave_Yr=V009, --Annual_Leave/Yr
@prevMonday=V014,--Monday
@prevTuesday =V015,--Tuesday
@prevWednesday=V016,--Wednesday
@prevThursday =V017,--Thursday
@prevFriday =V018,--Friday
@prevSaturday =V019,--Saturday
@prevSunday =V020,--Sunday
@prevWork_Hrs_Week=V008,--Work_Hrs/Week
@prevRenumerationHr=V025,--[Renumeration $/Hr]
@prevRenumerationYr=V026-- [Renumeration $/Yr]

from [Record] WHERE			[Record].TableID = 2670 and [V021] = @RecID  and ISNUMERIC( [Record].[V021]) = 1 order by dateTimeRecorded desc

if ISNULL(@prevEmployeeID , '') != ISNULL(@EmployeeID, '')
set  @numberofchange = @numberofchange + 1

if ISNULL(@prevName , '') != ISNULL(@Name, '')
set  @numberofchange = @numberofchange + 1

if ISNULL(@prevInitials , '') != ISNULL(@Initial , '')
set  @numberofchange = @numberofchange + 1

if ISNULL(@prevDesignation , '') != ISNULL(@Designation, '')
set  @numberofchange = @numberofchange + 1


if ISNULL(@prevGroup_Division , '') != ISNULL(@GroupDivision, '')
set  @numberofchange = @numberofchange + 1

if ISNULL(@prev_Package  , '') != ISNULL(@Package, '')
set  @numberofchange = @numberofchange + 1


if ISNULL(@prevChkPackage , '') != ISNULL(@ChkPackage, '')
set  @numberofchange = @numberofchange + 1

if ISNULL(@prevOthersSchedule_Leave , '') != ISNULL(@OthersSchedule_Leave, '')
set  @numberofchange = @numberofchange + 1

if ISNULL(@prevAnnual_Leave_Yr , '') != ISNULL(@Annual_Leave_Yr, '')
set  @numberofchange = @numberofchange + 1

if ISNULL(@prevMonday , '') != ISNULL(@Monday, '')
 set  @numberofchange = @numberofchange + 1
 
if ISNULL(@prevTuesday , '') != ISNULL(@Tuesday, '')
set  @numberofchange = @numberofchange + 1


if ISNULL(@prevWednesday , '') != ISNULL(@Wednesday, '')
set  @numberofchange = @numberofchange + 1

if ISNULL(@prevThursday  , '') != ISNULL(@Thursday, '')
set  @numberofchange = @numberofchange + 1

if ISNULL(@prevFriday , '') != ISNULL(@Friday, '')
set  @numberofchange = @numberofchange + 1

if ISNULL(@prevSaturday , '') != ISNULL(@Saturday, '')
set  @numberofchange = @numberofchange + 1

if ISNULL(@prevSunday , '') != ISNULL(@Sunday, '')
set  @numberofchange = @numberofchange + 1

if ISNULL(@prevWork_Hrs_Week , '') != ISNULL(@Work_Hrs_Week, '')
set  @numberofchange = @numberofchange + 1

if @Renumeration = '$Hourly Rate' AND ISNULL(@RenumerationHr, '') != ISNULL(@prevRenumerationHr, '')
BEGIN
	set			@numberofchange = @numberofchange + 1
	SET			@Package = @RenumerationHr
END

if @Renumeration = '$Annual Package' AND ISNULL(@RenumerationYr, '') != ISNULL(@prevRenumerationYr, '')
BEGIN
	set			@numberofchange = @numberofchange + 1
	SET			@Package = @RenumerationYr
END

end

if @Renumeration = '$Hourly Rate'
BEGIN
	SET			@ChkPackage = 'False' --'/Hour'
END

if @Renumeration = '$Annual Package'
BEGIN
	SET			@ChkPackage = 'True' -- 'Package'
END

--insert fetch data to emphistorytable
if  ((@numberofchange >0 or @ispreviousrecordexist =0  ) and isnumeric(@RecID)=1)
begin
insert into [Record]
(
tableid, --tableid
V002, --EmployeeID,
V004,--Name
V005,--Initials
V006,--Designation
V007,--Group/Division
V011,--Package
V012,--ChkPackage
V010,--OthersSchedule_Leave
V009, --Annual_Leave/Yr
V014,--Monday
V015,--Tuesday
V016,--Wednesday
V017,--Thursday
V018,--Friday
V019,--Saturday
V020,--Sunday
V008,--Work_Hrs/Week
V021,--Employee  -- foreignkey
EnteredBy,
IsActive,
dateTimeRecorded ,DateAdded , DateUpdated,
 V003 --date
)
values
 (
 '2670',  --employeehistory table
 @EmployeeID ,
 @Name ,
 @Initial ,
 @Designation ,
 @GroupDivision,
 @Package ,
	@ChkPackage,
 @OthersSchedule_Leave,
 @Annual_Leave_Yr,
 @Monday ,
 @Tuesday,
 @Wednesday ,
 @Thursday ,
 @Friday,
 @Saturday ,
 @Sunday ,
 @Work_Hrs_Week,
 @RecID ,  --foregin key
 @EnteredBy,
 @IsActive,
 getdate(),
 getdate(),
 getdate()
 --,getdate()--
 --CONVERT( VARCHAR(20), getdate(), 120 )
 ,CONVERT( VARCHAR(20), getdate(), 103 )

)
end
RETURN @@ERROR


SELECT		CONVERT( VARCHAR(20), getdate(), 103 )