﻿
CREATE PROCEDURE [dbo].[Audit_Summary]

(
	@nAccountID int,
    @sTableName varchar(128)=NULL,
    @dDateFrom datetime = NULL,
	@dDateTo datetime = NULL,
	@nTableID int=NULL,
	@sTextSearch varchar(MAX) = NULL,
	@nUserID int = NULL,
    @nStartRow int = 1,
	@nMaxRows int = 20000000 

)

AS
BEGIN TRY
			--KG 13/11/17 - Re-written to be faster
			--DECLARE @sSQL varchar(MAX)
			--DECLARE @sSelectCount varchar(MAX)
			--DECLARE @sWhere nvarchar(MAX)=' AND [Audit].TableName <>''Payment'' AND [Audit].AccountID='+ +CAST(@nAccountID AS varchar)

			--IF @sTableName IS NOT NULL 
			--	SET @sWhere=@sWhere + ' AND [Audit].TableName=''' + @sTableName + '''' 

			--IF @dDateFrom IS NOT NULL AND @dDateTo IS NOT NULL
			--	SET @sWhere=@sWhere + '  AND [Audit].DateAdded>= '''+CONVERT(varchar(30), @dDateFrom, 103)+''' AND  [Audit].DateAdded<='''+CONVERT(varchar(30), @dDateTo, 103)	+ ''' '

			--IF @nUserID IS NOT NULL 
			--	SET @sWhere = @sWhere + ' AND  [Audit].UserID='+CAST(@nUserID AS varchar)

			--IF @nTableID IS NOT NULL 
			--	SET @sWhere = @sWhere + ' AND  [Audit].TableID='+CAST(@nTableID AS varchar)

			--IF @sTextSearch IS NOT NULL 
			--	SET @sWhere = @sWhere + ' AND  ([Audit].OldValue LIKE ''%'+@sTextSearch + '%'' OR [Audit].NewValue LIKE ''%'+@sTextSearch + '%'')'

 
			--  SELECT @sSQL = 'SELECT * FROM ( SELECT [Audit].TableName,[Audit].DateAdded,
			--			[User].FirstName + '' '' + [User].LastName as [User],
			--			dbo.fnListChangesCommon([Audit].TableName,convert(int, [Audit].PrimaryKeyValue), 
			--			[Audit].DateAdded) as ColumnList,(SELECT TableName FROM [Table] WHERE TableID= [Audit].TableID) as [TableName2],
			--			 [Audit].PrimaryKeyValue,[Audit].[ChangeReason] as ResonForChange,ROW_NUMBER() 
			--			OVER(ORDER BY [Audit].DateAdded DESC) as RowNum 
			--			FROM [Audit]
			--			JOIN [User] ON [User].UserID = [Audit].UserID
			--			WHERE  FieldName != ''DateUpdated'' AND FieldName != ''LastUpdatedUserID'' AND  dbo.fnListChangesCommon([Audit].TableName,convert(int, [Audit].PrimaryKeyValue),  [Audit].DateAdded)!=''''  '
			
			--	SELECT @sSQL= @sSQL + @sWhere
	
			--	SELECT @sSQL=@sSQL + ' GROUP BY [Audit].TableName,[Audit].DateAdded,
			--				  [User].FirstName + '' '' + [User].LastName,
			--				  dbo.fnListChangesCommon([Audit].TableName,convert(int, [Audit].PrimaryKeyValue), [Audit].DateAdded),
			--				  [Audit].TableID,[Audit].PrimaryKeyValue,[Audit].[ChangeReason]
			--				  ) AS AuditInfo
			--				  WHERE RowNum>='+ CONVERT(nvarchar(10), @nStartRow)+' AND RowNum<('+ CONVERT(nvarchar(10), @nStartRow)+'+'+ CONVERT(nvarchar(10), @nMaxRows)+')'

                
            
			--			--now we need to get TotalRows number
            
			--			SELECT @sSelectCount=' SELECT MAX(RowNum) AS TotalRows FROM ( SELECT [Audit].DateAdded,
			--			[User].FirstName + '' '' + [User].LastName as [User],
			--			dbo.fnListChangesCommon([Audit].TableName,convert(int, [Audit].PrimaryKeyValue), [Audit].DateAdded) as Change,
			--			ROW_NUMBER() 
			--			OVER(ORDER BY [Audit].DateAdded DESC) as RowNum 
			--			FROM [Audit]
			--			JOIN [User] ON [User].UserID = [Audit].UserID
			--			WHERE FieldName != ''DateUpdated'' AND FieldName != ''LastUpdatedUserID''  AND  dbo.fnListChangesCommon([Audit].TableName,convert(int, [Audit].PrimaryKeyValue),  [Audit].DateAdded)!='''' '
            
			--			SELECT @sSelectCount= @sSelectCount + @sWhere
            
			--			SELECT @sSelectCount= @sSelectCount + ' ' + 'GROUP BY [Audit].DateAdded,
			--				  [User].FirstName + '' '' + [User].LastName,
			--				  dbo.fnListChangesCommon([Audit].TableName,convert(int, [Audit].PrimaryKeyValue), [Audit].DateAdded)
			--				  ) AS AuditInfo'
            
			--			PRINT @sSQL
			--			PRINT '--'
			--			 PRINT @sSelectCount   
 
			-- EXEC (@sSQL)

			-- EXEC (@sSelectCount)
			SELECT * FROM (
				SELECT 
				  Results.TableName,
				  Results.DateAdded,
				  [User].FirstName + ' ' + [User].LastName 'User',
				  Results.TableID,
				  [Table].TableName 'TableName2',
				  Results.PrimaryKeyValue,
				  Results.ChangeReason 'ResonForChange',
				  STUFF((
					SELECT ', ' + CASE WHEN [Audit].TableName = 'Record' AND [Column].DisplayName IS NOT NULL THEN [Column].DisplayName ELSE [Audit].FieldName END
					FROM [Audit] 
					LEFT JOIN [Column] ON [Audit].TableID = [Column].TableID AND [Audit].FieldName = [Column].SystemName
					WHERE ([Audit].[TableID] = Results.TableID 
							AND [Audit].DateAdded = Results.DateAdded
							AND [Audit].PrimaryKeyValue = Results.PrimaryKeyValue
							AND [Audit].FieldName != 'DateUpdated' 
							AND [Audit].FieldName != 'LastUpdatedUserID'	
							) 
					FOR XML PATH(''),TYPE).value('(./text())[1]','varchar(MAX)')
				  ,1,2,'') AS ColumnList,
				  ROW_NUMBER() OVER(ORDER BY Results.DateAdded DESC) as RowNum 
				FROM
				[Audit] Results
				LEFT JOIN [Table] ON Results.TableID = [Table].TableID
				LEFT JOIN [User] ON Results.UserID = [User].UserID
				WHERE Results.FieldName != 'DateUpdated' 
					AND Results.FieldName != 'LastUpdatedUserID' 
					AND Results.AccountID=@nAccountID
					AND (Results.TableName IN (SELECT [Txt_Value] FROM DBO.fn_ParseText2Table(@sTableName, ',')) OR @sTableName IS NULL)
					AND (Results.TableID = @nTableID OR @nTableID IS NULL)
					AND (Results.UserID = @nUserID OR @nUserID IS NULL)
					AND (Results.DateAdded >= @dDateFrom OR @dDateFrom IS NULL)
					AND (Results.DateAdded <= @dDateTo OR @dDateTo IS NULL)
					AND  ((Results.OldValue LIKE '%'+@sTextSearch + '%' OR Results.NewValue LIKE '%'+@sTextSearch + '%') OR @sTextSearch IS NULL)
				GROUP BY 
					  Results.TableName,
					  Results.DateAdded,
					  [User].FirstName + ' ' + [User].LastName,
					  Results.TableID,
					  [Table].TableName,
					  Results.PrimaryKeyValue,
					  Results.ChangeReason
		) Summary
		WHERE (RowNum >= @nStartRow AND RowNum <= @nStartRow + @nMaxRows)
		ORDER BY DateAdded DESC
			  
		--SELECT TotalRows= @@ROWCOUNT


			SELECT MAX(RowNum) AS TotalRows FROM (
				SELECT 
				  Results.TableName,
				  Results.DateAdded,
				  [User].FirstName + ' ' + [User].LastName 'User',
				  Results.TableID,
				  [Table].TableName 'TableName2',
				  Results.PrimaryKeyValue,
				  Results.ChangeReason 'ResonForChange',
				  STUFF((
					SELECT ', ' + CASE WHEN [Audit].TableName = 'Record' AND [Column].DisplayName IS NOT NULL THEN [Column].DisplayName ELSE [Audit].FieldName END
					FROM [Audit] 
					LEFT JOIN [Column] ON [Audit].TableID = [Column].TableID AND [Audit].FieldName = [Column].SystemName
					WHERE ([Audit].[TableID] = Results.TableID 
							AND [Audit].DateAdded = Results.DateAdded
							AND [Audit].PrimaryKeyValue = Results.PrimaryKeyValue
							AND [Audit].FieldName != 'DateUpdated' 
							AND [Audit].FieldName != 'LastUpdatedUserID'	
							) 
					FOR XML PATH(''),TYPE).value('(./text())[1]','varchar(MAX)')
				  ,1,2,'') AS ColumnList,
				  ROW_NUMBER() OVER(ORDER BY Results.DateAdded DESC) as RowNum 
				FROM
				[Audit] Results
				LEFT JOIN [Table] ON Results.TableID = [Table].TableID
				LEFT JOIN [User] ON Results.UserID = [User].UserID
				WHERE Results.FieldName != 'DateUpdated' 
					AND Results.FieldName != 'LastUpdatedUserID' 
					AND Results.AccountID=@nAccountID
					AND (Results.TableName IN (SELECT [Txt_Value] FROM DBO.fn_ParseText2Table(@sTableName, ',')) OR @sTableName IS NULL)
					AND (Results.TableID = @nTableID OR @nTableID IS NULL)
					AND (Results.UserID = @nUserID OR @nUserID IS NULL)
					AND (Results.DateAdded >= @dDateFrom OR @dDateFrom IS NULL)
					AND (Results.DateAdded <= @dDateTo OR @dDateTo IS NULL)
					AND  ((Results.OldValue LIKE '%'+@sTextSearch + '%' OR Results.NewValue LIKE '%'+@sTextSearch + '%') OR @sTextSearch IS NULL)
				GROUP BY 
					  Results.TableName,
					  Results.DateAdded,
					  [User].FirstName + ' ' + [User].LastName,
					  Results.TableID,
					  [Table].TableName,
					  Results.PrimaryKeyValue,
					  Results.ChangeReason
		) Summary
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Audit_Summary', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
