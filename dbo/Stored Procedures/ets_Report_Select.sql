﻿
CREATE PROCEDURE [dbo].[ets_Report_Select]
(
	@nReportID int = NULL,
	@nAccountID int = NULL,
	@nTableID int = NULL,
	@nGraphID int = NULL,
	@sReportText varchar(MAX) = NULL,
	@nReportDates int = NULL,
	@nReportDatesBefore int = NULL,
	@dStartDate datetime = NULL,
	@dEndDate datetime = NULL,
	@dDateAdded datetime = NULL,
	@dDateUpdated datetime = NULL,
	@nUserID int = NULL,

	@sOrder nvarchar(200) = "ReportID", 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows

	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)

	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nReportID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Report].ReportID = '+ CAST(@nReportID AS nvarchar)
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Report].AccountID = '+ CAST(@nAccountID AS nvarchar)
	IF @nTableID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND EXISTS (SELECT ReportItemID FROM [ReportItem] WHERE [ReportItem].[ReportID] = [Report].[ReportID] AND [ObjectID] = '+CAST(@nTableID AS nvarchar) + ' AND ItemType = 1)'
	IF @nGraphID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND EXISTS (SELECT ReportItemID FROM [ReportItem] WHERE [ReportItem].[ReportID] = [Report].[ReportID] AND [ObjectID] = '+CAST(@nGraphID AS nvarchar) + ' AND ItemType = 2)'
	IF @sReportText IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ([Report].ReportName LIKE '+'''%' + @sReportText + '%''' + ' OR [Report].ReportDescription LIKE '+'''%' + @sReportText + '%'')'
	IF @nReportDates IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Report].ReportDates = ' + CAST(@nReportDates AS nvarchar)
	IF @nReportDatesBefore IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Report].ReportDatesBefore = ' + CAST(@nReportDatesBefore AS nvarchar)
	IF @dStartDate IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Report].StartDate >= ''' + CONVERT(varchar(30), @dStartDate, 120) + ''''
	IF @dEndDate IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Report].EndDate <= ''' + CONVERT(varchar(30), @dEndDate, 120) + ''''
	IF @dDateAdded IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Report].DateAdded = '+CAST(@dDateAdded AS nvarchar)
	IF @dDateUpdated IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Report].DateUpdated = '+CAST(@dDateUpdated AS nvarchar)
	IF @nUserID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Report].UserID = '+CAST(@nUserID AS nvarchar)
	
	/*RP MODIFIED TICKET 4513 - Add Days Column*/
	SET @sSelect = 'SELECT * FROM 
	(SELECT [Report].ReportID, [Report].AccountID, [Report].ReportName, [Report].ReportDescription,
						  [Report].ReportDates, [Report].ReportDatesBefore, [Report].StartDate,  [Report].EndDate,
						  [Report].DateAdded, [Report].DateUpdated, [Report].UserID, [Report].[DaysFrom],[Report].[DaysTo], ([User].FirstName + '' '' + [User].LastName) AS FullName,
						  ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum 
						  FROM [Report] LEFT JOIN
						  [User] ON [Report].UserID = [User].UserID ' + @sWhere + ') as ReportInfo'

	SET @sSelectCount = REPLACE(@sSelect, 'SELECT * FROM', 'SELECT COUNT(*) AS TotalRows FROM')
	SELECT @sSelect = @sSelect + ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)

 
	SET ROWCOUNT @nMaxRows
	PRINT @sSelect
	EXEC (@sSelect)
	SET ROWCOUNT 0

	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Report_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH


