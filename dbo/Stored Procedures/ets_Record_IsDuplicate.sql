﻿

CREATE PROCEDURE [dbo].[ets_Record_IsDuplicate]
(
	@TableID int,
	@DateTimeRecorded datetime,
	@V001 varchar(MAX) = NULL,
	@V002 varchar(MAX) = NULL,
	@V003 varchar(MAX) = NULL,
	@V004 varchar(MAX) = NULL,
	@V005 varchar(MAX) = NULL,
	@V006 varchar(MAX) = NULL,
	@V007 varchar(MAX) = NULL,
	@V008 varchar(MAX) = NULL,
	@V009 varchar(MAX) = NULL,
	@V010 varchar(MAX) = NULL,
	@V011 varchar(MAX) = NULL,
	@V012 varchar(MAX) = NULL,
	@V013 varchar(MAX) = NULL,
	@V014 varchar(MAX) = NULL,
	@V015 varchar(MAX) = NULL,
	@V016 varchar(MAX) = NULL,
	@V017 varchar(MAX) = NULL,
	@V018 varchar(MAX) = NULL,
	@V019 varchar(MAX) = NULL,
	@V020 varchar(MAX) = NULL,
	@V021 varchar(MAX) = NULL,
	@V022 varchar(MAX) = NULL,
	@V023 varchar(MAX) = NULL,
	@V024 varchar(MAX) = NULL,
	@V025 varchar(MAX) = NULL,
	@V026 varchar(MAX) = NULL,
	@V027 varchar(MAX) = NULL,
	@V028 varchar(MAX) = NULL,
	@V029 varchar(MAX) = NULL,
	@V030 varchar(MAX) = NULL,
	@V031 varchar(MAX) = NULL,
	@V032 varchar(MAX) = NULL,
	@V033 varchar(MAX) = NULL,
	@V034 varchar(MAX) = NULL,
	@V035 varchar(MAX) = NULL,
	@V036 varchar(MAX) = NULL,
	@V037 varchar(MAX) = NULL,
	@V038 varchar(MAX) = NULL,
	@V039 varchar(MAX) = NULL,
	@V040 varchar(MAX) = NULL,
	@V041 varchar(MAX) = NULL,
	@V042 varchar(MAX) = NULL,
	@V043 varchar(MAX) = NULL,
	@V044 varchar(MAX) = NULL,
	@V045 varchar(MAX) = NULL,
	@V046 varchar(MAX) = NULL,
	@V047 varchar(MAX) = NULL,
	@V048 varchar(MAX) = NULL,
	@V049 varchar(MAX) = NULL,
	@V050 varchar(MAX) = NULL
)
AS

--DECLARE @sSQL varchar(MAX)

--SELECT @sSQL= 'SELECT * from [Record] WHERE IsActive=1 AND 
--			LocationID =' +  CAST(@LocationID as varchar)  + 
--            'AND [TableID] =' +  CAST(@TableID as varchar)  +
--            'AND [DateTimeRecorded] =''' + CONVERT(varchar(30), @DateTimeRecorded, 120) + '''' 
            
            SELECT * from [Record] WHERE IsActive=1  
            AND [TableID] =@TableID 
            AND [DateTimeRecorded]= @DateTimeRecorded
          
           
--PRINT @sSQL
--EXEC (@sSQL)
