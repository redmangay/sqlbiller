﻿

CREATE PROCEDURE [dbo].[ets_Dashboard_Select]
(
	@nAccountID int  = NULL,
	@sDocumentText varchar(250)  = NULL,
	@sDocumentIDs varchar(MAX)  = NULL,
	@sOrder nvarchar(200) = '[Document].DocumentText ASC', 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	EXEC ets_Dashboard_Select @nAccountID=24800,@sDocumentText='as'
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1 AND [Document].ForDashBoard=1 '
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Document].AccountID = '+CAST(@nAccountID AS NVARCHAR)
	IF @sDocumentText IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Document].DocumentText LIKE '+'''%' + @sDocumentText + '%''' 
	IF @sDocumentIDs IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Document].DocumentID IN (' + @sDocumentIDs + ')' 

	SET @sSelect = 'SELECT * FROM 
	(SELECT [Document].DocumentID, [Document].AccountID, [Document].DocumentText, [Document].DocumentTypeID, [Document].UniqueName, [Document].FileTitle, 
						  [Document].DocumentDate, [Document].DateAdded, [Document].DateUpdated, [Document].UserID, ([User].FirstName +'' '' + [User].LastName) AS FullName,[Document].TableID, 
						  DocumentType.DocumentTypeName,[Document].ReportHTML,[Document].IsReportPublic,[Document].DocumentDescription,[Document].DocumentEndDate, 
						  ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum 
						  FROM [Document] LEFT JOIN
						  DocumentType ON [Document].DocumentTypeID = DocumentType.DocumentTypeID LEFT JOIN
						  [User] ON [Document].UserID = [User].UserID ' + @sWhere + ') as DocumentInfo'

	SET @sSelectCount=REPLACE(@sSelect, 'SELECT * FROM', 'SELECT COUNT(*) AS TotalRows FROM')
	SELECT @sSelect= @sSelect + ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)

 
	 SET ROWCOUNT @nMaxRows
	--PRINT @sSelect
	EXEC (@sSelect)
	SET ROWCOUNT 0

	--PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Dashboard_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
