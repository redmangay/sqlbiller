﻿

CREATE PROCEDURE [dbo].[dbg_Document_BestFittingDash]
(
	  
	 @nUserRoleID INT,
	 @sDocumentText varchar(250) = NULL
)
AS

/*

EXEC dbg_Document_BestFittingDash 702, 'Default 5'

*/

BEGIN TRY
	
	DECLARE @nDashBoardDocumentID INT =NULL
	DECLARE @nUserID INT
	DECLARE @nAccountID INT
	
	-- @sDocumentText is Dahsboard name
	
	-- populate @nUserID, @nAccountID and @nDashBoardDocumentID (IF any)
	SELECT @nDashBoardDocumentID=DashBoardDocumentID,@nAccountID=AccountID,
	@nUserID=UserID FROM UserRole WHERE UserRoleID=@nUserRoleID
	
	
	IF @sDocumentText IS NULL
		BEGIN
			IF @nDashBoardDocumentID IS NOT NULL 
				SELECT * FROM Document WHERE DocumentID=@nDashBoardDocumentID;
			ELSE			
				BEGIN
					--find default dashboard for that user or his own created dashboard
					SELECT TOP 1
					* 
					FROM [dbo].[Document]
					WHERE ForDashBoard=1 AND AccountID = @nAccountID 
					AND (UserID IS NULL or UserID = @nUserID)
					ORDER BY  userID DESC 
				END
		END
	ELSE
		BEGIN
			-- @nDashBoardDocumentID does not matter now as we  have @sDocumentText
			SELECT TOP 1
			* 
			FROM [dbo].[Document]
			WHERE ForDashBoard=1 AND AccountID = @nAccountID 
			AND DocumentText=@sDocumentText
			ORDER BY  userID DESC
		END	
	
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_Document_BestFittingDash', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
