﻿

CREATE PROCEDURE [dbo].[spGetValueFromRelatedTable]
(
	@nParentRecordID int, -- IF not known call spGetParentRecordID
	@nRequiredTableID int, 
	@sRequiredColumnName varchar(MAX) -- e.g. V002
)
AS
/*
-- Get a value from another table, currently works from parent or sibling table 
-- pass in tableid and other table id and other column id
EXEC spGetValueFromRelatedTable 927364,2693,'V003'
*/
BEGIN TRY
	SET CONCAT_NULL_YIELDS_NULL OFF
	DECLARE @SQL varchar(MAX)
	DECLARE @nParentTableID int

	SELECT 	@nParentTableID = R.TableID 
		FROM [Record] R 
		WHERE R.RecordID = @nParentRecordID

	PRINT '@nParentTableID: ' + CAST(@nParentTableID AS varchar)

	IF @nRequiredTableID = @nParentTableID
	BEGIN
		PRINT 'Getting value from Parent Table:'
		SELECT @SQL = 'SELECT ' +  @sRequiredColumnName
				+ ' FROM [Record]  '
				+ ' WHERE RecordID = ' + cast(@nParentRecordID as varchar)
	END
	ELSE
	BEGIN
		PRINT 'Getting value from Sibling Table:'
		SELECT @SQL = 'SELECT TOP 1 Child.' +  @sRequiredColumnName
				+ ' FROM [Record] Child ' 
				+ ' JOIN [Record] Parent ON CAST(Parent.'  + ParentCol.SystemName + '  as varchar) = Child.' + ChildCol.SystemName
				+ ' WHERE Child.IsActive=1 AND Parent.IsActive=1 AND  Parent.TableID = ' + cast(ParentCol.TableID as varchar)
				+ ' AND Child.TableID = ' + CAST(@nRequiredTableID AS varchar)
				+ ' AND Parent.RecordID = ' + CAST(@nParentRecordID AS varchar)
	--SELECT ChildCol.*
			FROM [Record] ParentRec
			JOIN [Table] ParentTable ON ParentTable.TableID = ParentRec.TableID
			-- JOIN [Table] ChildTable ON ChildTable.TableID = @nRequiredTableID
			JOIN [Column] ChildCol ON ChildCol.TableID = @nRequiredTableID
				AND ChildCol.TableTableID = ParentTable.TableID 
			JOIN [Column] ParentCol ON ParentCol.ColumnID  = ChildCol.LinkedParentColumnID 
			--JOIN [Column] ParentCol ON ParentCol.ColumnID = C.LinkedParentColumnID
			WHERE ParentRec.RecordID = @nParentRecordID
	END
	PRINT @SQL
	EXEC (@SQL)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spGetValueFromRelatedTable', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
