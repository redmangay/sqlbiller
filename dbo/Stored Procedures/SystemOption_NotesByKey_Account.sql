﻿

CREATE PROCEDURE [dbo].[SystemOption_NotesByKey_Account]
(
	@sOptionKey nvarchar(200),
	@nAccountID INT=NULL,
	@nTableID INT=NULL,
	@sOptionNotes nvarchar(1000) output

)
AS

BEGIN TRY

	IF @nAccountID IS NULL AND @nTableID IS NULL
	SELECT @sOptionNotes =  OptionNotes from SystemOption
	where OptionKey=@sOptionKey AND AccountID IS NULL
	AND TableID IS NULL

	IF @nAccountID IS NOT NULL 
	SELECT @sOptionNotes =  OptionNotes from SystemOption
	where OptionKey=@sOptionKey AND AccountID =@nAccountID


	IF  @nTableID IS NOT NULL
	SELECT @sOptionNotes =  OptionNotes from SystemOption
	WHERE OptionKey=@sOptionKey AND TableID =@nTableID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('SystemOption_NotesByKey_Account', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
