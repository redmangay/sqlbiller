﻿
CREATE PROCEDURE [dbo].[dev_GetTableSQL2]
(
     @TableID int
)	
AS
	
SET NOCOUNT ON;

DECLARE @TableName varchar(MAX), @SQL varchar(MAX)

IF EXISTS(SELECT TOP 1 TableID FROM [Table] WHERE TableID =@TableID AND [Table].IsActive = 1)
	SELECT @TableName = TableName FROM [Table] WHERE  TableID = @TableID 
ELSE
	PRINT 'Warning: Table  with id  ' + CAST(@TableID AS varchar(20)) + ' does not exist or 

is not active'

PRINT 'Displaying Useful SQL for table:
Table ID:  ' + CAST(@TableID AS varchar) + '
Tablename: ' + @TableName + '
--------------------------------------------------------------------'

SET	@SQL = 'SELECT			 ['+@TableName+'].[TableID]'
PRINT '-- Full Version:'
SELECT	@SQL = @SQL + '
				,['+@TableName+'].[' + [SystemName] + ']	AS	[' + 

[DisplayName] + ']'
					FROM	[Column] 
					WHERE	[TableID] = @TableID 
					AND		[IsStandard] != 1
					AND		SystemName != 'RecordID'
					ORDER BY [DisplayOrder]

SET		@SQL = SUBSTRING(@SQL, 1, LEN(@SQL)) + '
				,['+@TableName+'].[DateAdded]
	FROM		[Record] ['+@TableName+']
	WHERE		['+@TableName+'].[TableID] = ' + CAST(@TableID as varchar(20)) + '
	AND			['+@TableName+'].[IsActive] = 1'
		
PRINT	@SQL
PRINT	CHAR(13)
PRINT	'-- Short Version:'
SET		@SQL = 'SELECT			 R.[TableID]'

SELECT	@SQL = @SQL + '
				,R.[' + [SystemName] + ']	AS	[' + 

[DisplayName] + ']'
					FROM	[Column] 
					WHERE	[TableID] = @TableID 
					AND		[IsStandard] != 1
					AND		SystemName != '[RecordID]'
					ORDER BY [DisplayOrder]

SET		@SQL = SUBSTRING(@SQL, 1, LEN(@SQL)) + '
				,['+@TableName+'].[DateAdded]
	FROM		[Record] R
	WHERE		R.[TableID] = ' + CAST(@TableID as varchar(20)) + '
	AND			R.[IsActive] = 1'
		
PRINT	@SQL




--INSERT
	
SET		@SQL = '
INSERT INTO	[Record] /* ' + @TableName + ' */
	(		
			 [TableID],[EnteredBy],[IsActive]'

SELECT	@SQL = @SQL + '
			,[' + [Column].[SystemName] + ']		/* ' + [Column].[DisplayName] + ' */'
	FROM		[Column] 
	WHERE		[TableID] = @TableID 
	AND			[IsStandard] != 1
	AND			SystemName != '[RecordID]'
	ORDER BY	[Column].[DisplayOrder]

		
SET				@SQL = SUBSTRING(@SQL, 1, LEN(@SQL)) + '
	)'

SET		@SQL = @SQL  + '
	SELECT ' + CAST(@TableID AS varchar(20)) + ', 61, 1,'

SELECT	@SQL = @SQL + '
			,@' + REPLACE([Column].[DisplayName], ' ', '')  + '		/* ' + [Column].[DisplayName] + ' */'
	FROM		[Column] 
	WHERE		[TableID] = @TableID 
	AND			[IsStandard] != 1
	AND			SystemName != '[RecordID]'
	ORDER BY	[Column].[DisplayOrder]

		
SET				@SQL = @SQL + '
		)'

PRINT	@SQL


-- UPDATE

SET		@SQL = '
UPDATE [' + @TableName + '] SET '

SELECT	@SQL = @SQL + '
			,[' + [Column].[SystemName] + ']	 = @' + REPLACE([Column].[DisplayName], ' ', '') 
	FROM		[Column] 
	WHERE		[TableID] = @TableID 
	AND			[IsStandard] != 1
	AND			SystemName != '[RecordID]'
	ORDER BY	[Column].[DisplayOrder]

		
SET				@SQL = @SQL + '
	WHERE	[RecordID] = @RecordID'



PRINT	@SQL



-- DELETE
SET		@SQL ='
-- Use with caution
-- DELETE
-- 		FROM	[Record] [' + @TableName + ']
-- 		WHERE	[TableID] = ' + CAST(@TableID AS varchar(20)) 
	
PRINT	@SQL




