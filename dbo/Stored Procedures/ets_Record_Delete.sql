﻿

CREATE PROCEDURE [dbo].[ets_Record_Delete]
(
            @sRecordIDs varchar(MAX),
            @nUserID int,	
            @nTableID int,
            @bDeleteAll bit = 0,
            @bPermanentDelete bit = 0 ,
            @sDeleteReason varchar(MAX) = NULL,
			@ParentIDChecker bit = 1, 									 
			@TextSearch varchar(MAX) =''
					
)
/*
REVISION HISTORY
-- TEST

EXEC [dbo].[ets_Record_Delete] @sRecordIDs= '1356054',
@nUserID= '25440' ,@nTableID= '4214' ,@bDeleteAll=0,@bPermanentDelete=0,@sDeleteReason='red test'

-- END TEST


-- THIS SP FOR DELETE VIRTUALLY AND PERMANENTLY
-- @ParentIDChecker -- For parent records ID flag: this will become zero when loop for DELETE Children. 
					-- Need this as 1 to be able to get the recordids that were Selected by the user and return their status: Isactive=0 or isactive=1
-- @TextSearch -- This is either filter IF the page is Child table (i.e. Child Table RecordList in RecordDetail page) and filter IF the user has a search (filter) 

Red 01-Sep-17:	Get all recordids IF DELETE ALL, not just the Selected records in the UI as the parent recordids
				Removed flag IF Orphan child records... this isnt needed
Red 03-Sep-17:	Get all ParentRecordIDs with Normal and Advanced Search filter
Red 16-Sep-17:	Created a function for ExtraWhere clause
Red 09-Nov-17:	UPDATE the Prevent DELETE section into like View Filter in C#
Jon 10-Nov-17:  Improved CATCH info (logging SQL)

*/

AS
BEGIN TRY
 
            DECLARE @Error int = -1, @Count int, @nRowCount int
            DECLARE @UPDATE varchar(MAX)
            DECLARE @DELETE varchar(MAX)       
            DECLARE @WHERE varchar(MAX) 
            DECLARE @SQL varchar(MAX)

            ----------------------------------------------------------------
			-- Set the main SQL
	        ----------------------------------------------------------------
			IF @bPermanentDelete = 0
				BEGIN
					SET @SQL = 'UPDATE [Record] SET IsActive=0, LastUpdatedUserID = '+ CAST(@nUserID as varchar(MAX)) +' , DateUpdated= getdate(), DeleteReason = ''' + @sDeleteReason +''''
					SET @WHERE = 'WHERE IsActive=1'
				END
			ELSE 
				BEGIN 
					SET @SQL = 'DELETE FROM [Record]'
					SET @WHERE = 'WHERE IsActive=0'
				END

			IF @bDeleteAll = 0
				SET @WHERE = @WHERE + ' AND [RecordID] IN (SELECT Int_Value FROM DBO.fn_ParseText2Table(''' + @sRecordIDs + ''','',''))' + ' ' + @TextSearch
			ELSE 
				SET @WHERE = @WHERE + ' AND [TableID] = ' + CAST(@nTableID as varchar(20)) + ' ' + @TextSearch

		
            ----------------------------------------------------------------
			-- Process the children            
            ----------------------------------------------------------------
			DECLARE @DeleteAction varchar(50) = ''
			DECLARE @ParentRecordID varchar(MAX) =''
			DECLARE @WithChild bit
			DECLARE @SQLInsertSelectedRecord varchar(MAX)
			DECLARE @ParentTableSelectedRecord TABLE ([Counter] int identity(1,1), ParentRecordID int)

			-- lets get all the Selected records (original Selected records)
			IF (@ParentIDChecker = 1)
			BEGIN
				SELECT @SQLInsertSelectedRecord = 'SELECT [RecordID] FROM [Record] ' + ' '+ @WHERE
				INSERT INTO @ParentTableSelectedRecord EXEC(@SQLInsertSelectedRecord);
			END
			-- we have the recordids now
			
			SELECT @DeleteAction = [DeleteAction] FROM [Table] WHERE TableID = @nTableID


			IF @DeleteAction IS NOT NULL -- NULL means do nothing with the child records.. ignore them
			BEGIN

				-- Find all of the child tables for the current table	
				DECLARE @ChildRecordIDs nvarchar(MAX)			
				DECLARE @ChildTables TABLE ([Counter] int identity(1,1), ChildTableID int, ChildSystemName varchar(10))
				INSERT INTO @ChildTables (ChildTableID, ChildSystemName)
					SELECT C.[TableID], C.SystemName
						FROM [Column] C INNER JOIN [Table] T ON C.TableID = T.TableID
						WHERE C.[TableTableID] IS NOT NULL
						AND C.[TableTableID] = @nTableID
						AND T.IsActive=1

					DECLARE @Counter int = 1
					-- lets exclude Recordids with strict parent
					IF @DeleteAction = 'Prevent deletion'
					BEGIN
						
							DECLARE @PreventDeleteFilter varchar (MAX)
							SELECT @PreventDeleteFilter = PreventDeleteFilter FROM [Table] WHERE IsActive=1 AND TableID = @nTableID
							IF @PreventDeleteFilter IS NOT NULL
							BEGIN

							SELECT @WHERE = @WHERE + ' AND [RecordID] NOT IN (SELECT Record.RecordID FROM [Record] WHERE IsActive=1' +  @PreventDeleteFilter + ')'

							PRINT @where
							END

						WHILE EXISTS(SELECT * FROM @ChildTables WHERE [Counter] >= @Counter)
						BEGIN

							SELECT @WHERE = @WHERE + ' AND [RecordID] NOT IN (SELECT ISNULL(' + ChildSystemName + ',0) FROM [Record] WHERE IsActive=1 AND [TableID] =  '+ CONVERT(varchar(10),ChildTableID) +')' 
								FROM @ChildTables WHERE [Counter] = @Counter 								
							SET @Counter = @Counter + 1
						END 
						PRINT @where
						
					END
					-- excluded
					
						-- get the parent recordids
						DECLARE @SQLInsertParentIDs varchar(MAX)
						DECLARE @ParentTableRecordIDs TABLE ([Counter] int identity(1,1), ParentRecordID int)

						SELECT @SQLInsertParentIDs = 'SELECT [RecordID] FROM [Record]' + ' ' + @WHERE
						INSERT INTO @ParentTableRecordIDs (ParentRecordID) EXEC(@SQLInsertParentIDs);
						-- we have the parent recordids

						-- lets get the child recordids
						DECLARE @ChildTableRecordIDs_Temp TABLE ([Counter] int identity(1,1), ChildRecordID INT)
						DECLARE @ChildTableRecordIDs TABLE ([Counter] int identity(1,1), ChildRecordIDs NVARCHAR(MAX), ChildTableID int)
						
						DECLARE @ParentCounter int = 1						
						DECLARE @ParenRecordID varchar(MAX)
						DECLARE @ChildSystemName varchar(MAX)
						DECLARE @ChildTableID varchar(50)
						DECLARE @SQLInsert varchar(MAX)				
						DECLARE @CountParentRow int
						
						DECLARE @ChildTableIDToDelete int

						WHILE EXISTS(SELECT * FROM @ParentTableRecordIDs WHERE [Counter] >= @ParentCounter)
						BEGIN
							SELECT @ParenRecordID = ParentRecordID FROM @ParentTableRecordIDs WHERE [Counter] =@ParentCounter
							DECLARE @ChildTableRecordIDsCounter int = 1
							WHILE EXISTS(SELECT * FROM @ChildTables WHERE [Counter] >= @ChildTableRecordIDsCounter)
							BEGIN
								
								SELECT @ChildTableIDToDelete = ChildTableID FROM @ChildTables WHERE [COUNTER] = @ChildTableRecordIDsCounter	
								DELETE FROM @ChildTableRecordIDs_Temp -- RESET TABLE CONTAINER
								
								SELECT @SQLInsert = 'SELECT [RecordID] FROM [Record] WHERE IsActive=1 AND ' + ChildSystemName + ' = ''' + @ParenRecordID + ''' AND [TableID] = '+ CONVERT(varchar(10),ChildTableID) +''
								FROM @ChildTables WHERE [COUNTER] = @ChildTableRecordIDsCounter
								
								INSERT INTO @ChildTableRecordIDs_Temp (ChildRecordID) EXEC(@SQLInsert);
								
								SELECT  @ChildRecordIDs = LEFT(ChildRecordIDs, LEN(ChildRecordIDs) - 1) 
								FROM ( SELECT CONVERT(varchar(50), ChildRecordID) + ',' FROM @ChildTableRecordIDs_Temp FOR XML PATH ('') ) C (ChildRecordIDs)
							
								IF @ChildRecordIDs IS NOT NULL 
								BEGIN
									INSERT INTO @ChildTableRecordIDs (ChildRecordIDs, ChildTableID)
									SELECT @ChildRecordIDs, ChildTableID	
									FROM @ChildTables WHERE [COUNTER] = @ChildTableRecordIDsCounter	
															
								END
								SET @ChildTableRecordIDsCounter = @ChildTableRecordIDsCounter + 1
							END
							SET @ParentCounter = @ParentCounter + 1
						END
						-- we have the child recordids

					-- lets kill them all ha ha ha ha!
					SELECT @SQL = @SQL + ' ' + @WHERE
					--PRINT @SQL
					 EXEC(@SQL);
					 SELECT @nRowCount =  @@ROWCOUNT -- lets get the count of the affected	parent
					-- killed
					
					IF @DeleteAction = 'DELETE children records' -- cascade DELETE
					BEGIN						
						--lets send the children to the killer ha ha ha ha	
						WHILE EXISTS(SELECT * FROM @ChildTableRecordIDs WHERE [Counter] >= @Counter)
						BEGIN
						SELECT @ChildTableIDToDelete = ChildTableID, @ChildRecordIDs = ChildRecordIDs  FROM @ChildTableRecordIDs WHERE [Counter] = @Counter
						
						IF @ParentIDChecker=1 
						BEGIN
							SET @WithChild = 1
						END

						EXEC ets_Record_Delete	 
								 @ChildRecordIDs
								,@nUserID
								,@ChildTableIDToDelete
								,0
								,0
								,@sDeleteReason
								,0
								,''
																
						SET @Counter = @Counter + 1
						
						END
					END					
			END
			ELSE
			BEGIN				
				SELECT @SQL = @SQL + ' ' + @WHERE
				EXEC(@SQL);
			END		
		
			DECLARE @DeletedAndNotTableRecordIDs TABLE 
					([Counter] int identity(1,1)
					,DeletedRecordIDs NVARCHAR(MAX)
					,NotDeletedRecordIDs NVARCHAR(MAX)
					,RecordIDs nvarchar(MAX),WithChild bit,DeleteAction varchar(50) )

					-- lets get the Selected records and return to show for notification	
					IF @ParentIDChecker = 1
					BEGIN
						IF @bDeleteAll = 1
						BEGIN
							INSERT INTO @DeletedAndNotTableRecordIDs (DeletedRecordIDs,NotDeletedRecordIDs,RecordIDs,WithChild,DeleteAction)
							SELECT @nRowCount  --Deleted	
							,(SELECT COUNT(ParentRecordID) - @nRowCount FROM @ParentTableSelectedRecord) -- Not Deleted
							,(SELECT LEFT(ChildRecordIDs, LEN(ChildRecordIDs) - 1) 
								FROM ( SELECT CONVERT(varchar(50), RecordID) + ',' FROM Record WHERE IsActive=0 AND TableID = @nTableID FOR XML PATH ('') ) C (ChildRecordIDs))
							,isnull(@WithChild,0)
							,@DeleteAction -- Defined DELETE Action 

						END
						ELSE
						BEGIN
							INSERT INTO @DeletedAndNotTableRecordIDs (DeletedRecordIDs,NotDeletedRecordIDs,RecordIDs,WithChild,DeleteAction)
							SELECT	(SELECT COUNT(RecordID) FROM RECORD WHERE IsActive=0 AND RECORDID IN 
							(SELECT Int_Value FROM DBO.fn_ParseText2Table( @sRecordIDs,','))) -- Deleted
							,(SELECT COUNT(RecordID) FROM RECORD WHERE IsActive=1 AND RECORDID IN 
							(SELECT Int_Value FROM DBO.fn_ParseText2Table( @sRecordIDs,','))) -- Not Deleted
							,@sRecordIDs
							,isnull(@WithChild,0)
							,@DeleteAction -- Defined DELETE Action  

						END

						SELECT * FROM @DeletedAndNotTableRecordIDs -- Killed information
						
					END
					
					
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Record_Delete', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'SQL: ' + ISNULL(@SQL,'')) -- JB 10/11/17 added @SQL 
END CATCH
