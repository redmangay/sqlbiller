﻿CREATE procedure  [dbo].[usr_rptMAILTOMailingList] 
 
as
SELECT   Client.Company, ClientContact.Title, ClientContact.Name, Client.Address1, Client.Address2, Client.Suburb, Client.State, Client.PostCode, Client.Country, ClientContact.MailOuts
FROM            Account24918.vSubsidiary AS Client RIGHT OUTER JOIN
                         Account24918.[vClient Contact] AS ClientContact ON Client.ClientID = ClientContact.ClientID
                         where ClientContact.MailOuts ='True'
ORDER BY Client.Company
