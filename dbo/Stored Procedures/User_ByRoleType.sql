﻿

CREATE PROCEDURE [dbo].[User_ByRoleType]
(
	@sRoleType char(1)
)
AS

BEGIN TRY
	SET NOCOUNT ON;

	SELECT     [User].*
FROM         [User] INNER JOIN
                      UserRole ON [User].UserID = UserRole.UserID INNER JOIN
                      Role ON UserRole.RoleID = Role.RoleID
                      WHERe RoleType=@sRoleType

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('User_ByRoleType', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
