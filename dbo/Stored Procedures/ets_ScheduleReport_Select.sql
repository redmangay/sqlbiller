﻿
 
CREATE PROCEDURE [dbo].[ets_ScheduleReport_Select]
(
	@nAccountID int,
	@nMainDocumentID int  = NULL,
	@sFrequency char(1)  = NULL,
	@sFrequencyWhen varchar(20)  = NULL,
	@dDateAdded datetime  = NULL,
	@dDateUpdated datetime  = NULL,
	@sOrder nvarchar(200) = ScheduleReportID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	EXEC ets_ScheduleReport_Select @nAccountID=13
	
	---------------------------------------------------------------*/
 
AS

BEGIN TRY
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'

	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND AccountID = '+CAST(@nAccountID AS NVARCHAR)

	IF @nMainDocumentID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND MainDocumentID = '+CAST(@nMainDocumentID AS NVARCHAR)
	IF @sFrequency IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Frequency LIKE '+'''' + @sFrequency + ''''
	IF @sFrequencyWhen IS NOT NULL 
		SET @sWhere = @sWhere + ' AND FrequencyWhen LIKE '+'''' + @sFrequencyWhen + ''''
	IF @dDateAdded IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DateAdded = '+CAST(@dDateAdded AS NVARCHAR)
	IF @dDateUpdated IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DateUpdated = '+CAST(@dDateUpdated AS NVARCHAR)
	SET @sSelect = 'SELECT * FROM 
	(SELECT [ScheduleReport].ScheduleReportID,MainDocumentID,Frequency,FrequencyWhen,
	[ScheduleReport].DateAdded,[ScheduleReport].DateUpdated,[ScheduleReport].ReportPeriod,
	LastCreatedDate,ReportPeriodUnit,Emails,DocumentText,AccountID, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum FROM [ScheduleReport] 
	INNER JOIN Document on ScheduleReport.MainDocumentID=Document.DocumentID ' + @sWhere + ') as UserInfo'
	SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [ScheduleReport] INNER JOIN Document on ScheduleReport.MainDocumentID=Document.DocumentID ' + @sWhere 
 
	-- Extend WHERE to include paging:
	SET @sWhere = @sWhere + ' AND RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	SET @sOrder = ' ORDER BY ' + @sOrder
	SET @sSelect = @sSelect + @sWhere + @sOrder
 
 
	PRINT @sSelect 
	EXEC (@sSelect)
	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_ScheduleReport_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
