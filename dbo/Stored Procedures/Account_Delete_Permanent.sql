﻿CREATE  PROCEDURE [dbo].[Account_Delete_Permanent]
(
      @nAccountID int
)

      /*----------------------------------------------------------------

      exec [Account_Delete_Permanent] @nAccountID=26119
	  SELECT * FROM [Account] ORDER BY AccountID DESC
      ---------------------------------------------------------------*/

AS
	/* Modification History
	JB	07 Nov 2018	:	Fixed bugs
	*/

BEGIN TRY
--	BEGIN TRANSACTION --beginning a transaction..
		DECLARE @nAdminID int
		SELECT @nAdminID = UserID FROM [User] WHERE Email = 'info@dbgurus.com.au'

		UPDATE [Record] SET [LastUpdatedUserID] = @nAdminID 
			WHERE [LastUpdatedUserID] IN (
				SELECT [User].UserID FROM [User] 
				JOIN UserRole ON UserRole.UserID=[User].UserID 
				WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1)
		UPDATE [Record] SET [EnteredBy] = @nAdminID WHERE [EnteredBy] IN (SELECT [User].UserID FROM [User] JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1)
		UPDATE [Record] SET [OwnerUserID] = @nAdminID WHERE [OwnerUserID] IN (SELECT [User].UserID FROM [User] JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1)
		UPDATE [OfflineTask] SET [AddedByUserID] = @nAdminID WHERE [AddedByUserID] IN (SELECT [User].UserID FROM [User] JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1)

		UPDATE [Menu] 
			SET [TableID] = NULL,
				[DocumentID] = NULL,
				[DocumentTypeID] = NULL
			WHERE AccountID = @nAccountID 

		UPDATE [Role] 
			SET [OwnerUserID] = NULL
				,[DashboardTableID] = NULL 
				WHERE [AccountID] = @nAccountID

		-- Document Update
		UPDATE [Document] SET [UserID] = @nAdminID WHERE AccountID = @nAccountID
		UPDATE [Document] SET [UserID] = @nAdminID WHERE UserID IN (SELECT [User].UserID FROM [User] INNER JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1)

		UPDATE [Column] SET 
			[DataRetrieverID] = NULL,
			[LinkedParentColumnID] = NULL, 
			[TableTabID] = NULL,
			[MapPinHoverColumnID] = NULL,
			[LastUpdatedUserID] = @nAdminID
			FROM [Column]
			JOIN [Table] ON [Table].TableID = [Column].TableID 
			WHERE [Table].AccountID = @nAccountID

		UPDATE [Column] SET [LinkedParentColumnID] = NULL 
			WHERE [LinkedParentColumnID] IN (SELECT ColumnID FROM [Column] WHERE TableID IN (SELECT TableID FROM [Table] WHERE AccountID = @nAccountID))
		
		UPDATE [Column] SET [LastUpdatedUserID] = @nAdminID WHERE [LastUpdatedUserID] IN (SELECT [User].UserID FROM [User] INNER JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID)
		-- Old
		--UPDATE [User] SET DashBoardDocumentID = NULL, [DataScopeColumnID]=NULL FROM [User] INNER JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1

		-- Old?
		--UPDATE [ScheduledTask] SET EmailLogID = null WHERE AccountID = @nAccountID 

		UPDATE [Table] SET [FilterColumnID] = null,
			[AddUserPasswordColumnID] = null,
			[AddUserUserColumnID] = null,
			[SortColumnID] = NULL,
			[ValidateColumnID1] = NULL,
			[ValidateColumnID2] = NULL,
			[DataUpdateUniqueColumnID] = NULL,
			[UniqueColumnID] = NULL,
			[UniqueColumnID2] = NULL,
			[GraphSeriesColumnID] = NULL,
			[GraphXAxisColumnID] = NULL,
			GraphDefaultYAxisColumnID = NULL,
			ArchiveDateColumnID=null
			WHERE AccountID = @nAccountID 

		UPDATE [Account] SET 
		--	DisplayTableID = null,
			[MapDefaultTableID] = null, 
			[DefaultGraphOptionID] = null
			WHERE AccountID = @nAccountID

		-- Account Invite
		DELETE FROM [AccountInvite] WHERE AccountID = @nAccountID
			OR [InvitedAccountID] = @nAccountID
			OR [InvitedUserID] IN (SELECT U.UserID FROM [User] U JOIN UserRole UR ON UR.UserID=U.UserID WHERE AccountID = @nAccountID)
			OR [InviterUserID] IN (SELECT U.UserID FROM [User] U JOIN UserRole UR ON UR.UserID=U.UserID WHERE AccountID = @nAccountID)

		-- View
		DELETE FROM [ViewItem] WHERE [ViewID] IN (SELECT [ViewID] FROM [View] V JOIN [Table] T ON T.TableID = V.TableID WHERE [AccountID] = @nAccountID)
		DELETE FROM [View] WHERE [TableID] IN (SELECT [TableID] FROM [Table] WHERE [AccountID] = @nAccountID)
		DELETE FROM [View] WHERE [UserID] IN ( SELECT U.UserID FROM [User] U JOIN UserRole UR ON UR.UserID=U.UserID WHERE AccountID = @nAccountID AND IsPrimaryAccount=1 )

		-- RoleTable
		DELETE FROM [RoleTable] WHERE [ViewsDefaultFromUserID] IN ( SELECT U.UserID FROM [User] U JOIN UserRole UR ON UR.UserID=U.UserID WHERE AccountID = @nAccountID AND IsPrimaryAccount=1 )
								OR [RoleID] IN (SELECT [RoleID] FROM [Role] WHERE AccountID = @nAccountID)

		-- GraphDefinition
		DELETE FROM [GraphDefinition] WHERE [AccountID] = @nAccountID

		-- ExportTemplate
		DELETE FROM [ExportTemplateItem] WHERE ExportTemplateID IN (SELECT ExportTemplateID FROM [ExportTemplate] ET JOIN [Table] T ON T.[TableID] = ET.[TableID] WHERE [AccountID] = @nAccountID)
		DELETE FROM [ExportTemplate] WHERE [TableID] IN (SELECT [TableID] FROM [Table] WHERE [AccountID] = @nAccountID)

		-- ImportTemplate
		DELETE FROM [ImportTemplateItem] WHERE ImportTemplateID IN (SELECT ImportTemplateID FROM [ImportTemplate] ET JOIN [Table] T ON T.[TableID] = ET.[TableID] WHERE [AccountID] = @nAccountID)
		DELETE FROM [ImportTemplate] WHERE [TableID] IN (SELECT [TableID] FROM [Table] WHERE [AccountID] = @nAccountID)

		-- Service
		DELETE FROM [Service] WHERE [TableID] IN (SELECT [TableID] FROM [Table] WHERE [AccountID] = @nAccountID)
        DELETE FROM [Service] WHERE TableChildID IN (SELECT TableChildID FROM TableChild
			WHERE ParentTableID IN (SELECT [TableID] FROM [Table] WHERE [AccountID] = @nAccountID) )
		DELETE FROM [Service] WHERE TableChildID IN (SELECT TableChildID FROM TableChild
			WHERE ChildTableID IN (SELECT [TableID] FROM [Table] WHERE [AccountID] = @nAccountID) )
		DELETE FROM [Service] WHERE [AccountID] = @nAccountID

		DELETE FROM [OfflineTaskLog] WHERE [OfflineTaskID] IN (SELECT [OfflineTaskID] FROM [OfflineTask] WHERE [AccountID]= @nAccountID)
		DELETE FROM [OfflineTask] WHERE [AccountID]= @nAccountID

		DELETE FROM [Upload] 
			WHERE TableID IN (SELECT TableID FROM [Table] WHERE AccountID = @nAccountID)
			OR [DocumentTypeID] IN (SELECT [DocumentTypeID] FROM [DocumentType] WHERE AccountID = @nAccountID)

		DELETE FROM [SystemOption] 
			WHERE AccountID = @nAccountID 
			OR [TableID] IN (SELECT TableID FROM [Table] WHERE AccountID = @nAccountID)

		-- The rest should cascade delete
		DELETE FROM DataReminderUser WHERE UserID IN (SELECT [User].UserID FROM [User] INNER JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1)
		DELETE FROM FormSetGroup WHERE ParentTableID IN (SELECT TableID FROM [Table] WHERE AccountID = @nAccountID)

		PRINT 'TableTab Delete: --------------------------------------------------------------------------------------------------'
		DELETE FROM [Conditions] WHERE [TableTabID] IN (SELECT [TableTabID] FROM [TableTab] TT JOIN [Table] T ON T.TableID = TT.TableID  WHERE AccountID = @nAccountID)
		DELETE FROM [TableTab] WHERE [TableID] IN (SELECT [TableID] FROM [Table] WHERE [AccountID] = @nAccountID)
	
		DELETE FROM [TableChild] WHERE ParentTableID IN (SELECT TableID FROM [Table] WHERE AccountID = @nAccountID)
		DELETE FROM [EmailLog] WHERE AccountID = @nAccountID
		DELETE FROM DocTemplate WHERE DataRetrieverID in (select DataRetrieverID FROM [DataRetriever] WHERE TableID IN (SELECT TableID FROM [Table] WHERE AccountID = @nAccountID))
		DELETE FROM [DataRetriever] WHERE TableID IN (SELECT TableID FROM [Table] WHERE AccountID = @nAccountID)
		DELETE FROM [ScheduleReport] WHERE MainDocumentID IN (SELECT DocumentID FROM Document WHERE AccountID = @nAccountID)

		PRINT 'Report Delete: --------------------------------------------------------------------------------------------------'
		DELETE ReportTableItem WHERE ColumnID IN ( SELECT ColumnID FROM [Column] C JOIN [Table] T ON C.TableID=T.TableID
			WHERE T.AccountID=@nAccountID)
		DELETE ReportTableItem WHERE ReportItemID IN (SELECT ReportItemID FROM ReportItem RI JOIN Report R ON 
														RI.ReportID=R.ReportID WHERE R.AccountID=@nAccountID)

		DELETE ReportRecipient WHERE ReportID IN (SELECT ReportID FROM [Report] WHERE AccountID=@nAccountID)
		DELETE ReportItemSortOrder WHERE SortColumnID IN ( SELECT ColumnID FROM [Column] C JOIN [Table] T ON C.TableID=T.TableID
															WHERE T.AccountID=@nAccountID)
		DELETE ReportItemSortOrder WHERE ReportItemID IN (SELECT ReportItemID FROM ReportItem RI JOIN Report R ON RI.ReportID=R.ReportID 
															WHERE R.AccountID=@nAccountID)
		DELETE ReportItemFilter WHERE FilterColumnID IN ( SELECT ColumnID FROM [Column] C JOIN [Table] T ON C.TableID=T.TableID
															WHERE T.AccountID=@nAccountID)
		DELETE ReportItemFilter WHERE ReportItemID IN (SELECT ReportItemID FROM ReportItem RI JOIN Report R ON RI.ReportID=R.ReportID 
															WHERE R.AccountID=@nAccountID)
		DELETE ReportItem WHERE ReportID IN (SELECT ReportID FROM [Report] WHERE AccountID=@nAccountID)
		DELETE Report  WHERE AccountID=@nAccountID

		PRINT 'Document Delete: --------------------------------------------------------------------------------------------------'
		DELETE FROM [Menu] WHERE [DocumentTypeID] in (SELECT [DocumentTypeID] FROM [Document] WHERE AccountID = @nAccountID)
		DELETE FROM [Conditions] WHERE [DocumentSectionID] IN (SELECT [DocumentSectionID] FROM [DocumentSection] DS JOIN [Document] D ON DS.DocumentID = D.DocumentID WHERE AccountID = @nAccountID)

		DELETE FROM [DocumentSection] WHERE DocumentID IN (SELECT DocumentID FROM Document WHERE AccountID = @nAccountID OR UserID IN (SELECT UserID FROM UserRole WHERE AccountID = @nAccountID))

		DELETE FROM [UserRole] WHERE [DashBoardDocumentID] IN (SELECT [DocumentID] FROM [Document] WHERE AccountID = @nAccountID)
			OR [DataScopeColumnID] in (SELECT [ColumnID] FROM [Column] C JOIN [Table]  T ON T.TableID = C.TableID WHERE T.AccountID = @nAccountID)

		DELETE FROM [Document] WHERE AccountID = @nAccountID -- this looks wrong: OR UserID IN (SELECT UserID FROM UserRole WHERE AccountID = @nAccountID)

		DELETE FROM [MonitorScheduleUser] WHERE UserID IN (SELECT [User].UserID FROM [User] INNER JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1)
		DELETE FROM [MonitorSchedule] WHERE AccountID = @nAccountID
		PRINT '[MonitorSchedule]'
		DELETE FROM [DocumentType] WHERE AccountID = @nAccountID
		PRINT '[DocumentType]'
		DELETE FROM [VisitorLog] WHERE UserID IN (SELECT [User].UserID FROM [User] INNER JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1)
		PRINT '[VisitorLog]'
		-- UPDATE [Account] SET GraphDefaultColumnID = null WHERE AccountID = @nAccountID
		DELETE FROM [UserFolder] 
			WHERE FolderID IN (SELECT FolderID FROM [Folder] WHERE AccountID = @nAccountID)
			OR UserID IN(SELECT [User].UserID FROM [User] INNER JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1)
		PRINT '[UserFolder]'

		DELETE FROM [Folder] WHERE AccountID = @nAccountID
		DELETE FROM [File] WHERE AccountID = @nAccountID
		PRINT '[File]'
		DELETE FROM [InComingEmail] WHERE InComingEmailID IN (   SELECT     InComingEmail.InComingEmailID
							FROM         InComingEmail INNER JOIN
							  [User] ON InComingEmail.EmailFrom = [User].Email
							  INNER JOIN UserRole ON UserRole.UserID=[User].UserID
							  WHERE AccountID=@nAccountID)
		PRINT 'InComingEmail'



		DELETE FROM [DataReminderUser] WHERE UserID IN (SELECT [User].UserID FROM [User] INNER JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1) 
		PRINT '[DataReminderUser]'


		DELETE FROM [Conditions] WHERE [ConditionColumnID] IN (SELECT ColumnID FROM [Column] JOIN [Table] ON [Table].TableID = [Column].TableID WHERE [Table].AccountID = @nAccountID)
		DELETE FROM [Conditions] WHERE [ColumnID]  IN (SELECT ColumnID FROM [Column] JOIN [Table] ON [Table].TableID = [Column].TableID WHERE [Table].AccountID = @nAccountID)

		DELETE FROM [ColumnColour] WHERE [ControllingColumnID] IN (SELECT ColumnID FROM [Column] JOIN [Table] ON [Table].TableID = [Column].TableID WHERE [Table].AccountID = @nAccountID)

		DELETE FROM [Condition] WHERE [ColumnID] IN (SELECT ColumnID FROM [Column] JOIN [Table] ON [Table].TableID = [Column].TableID WHERE [Table].AccountID = @nAccountID)
		DELETE FROM [Condition] WHERE [CheckColumnID] IN (SELECT ColumnID FROM [Column] JOIN [Table] ON [Table].TableID = [Column].TableID WHERE [Table].AccountID = @nAccountID)

		DELETE FROM [DataReminderUser] WHERE DataReminderID IN 
			(SELECT DataReminderID FROM [DataReminder] JOIN [Column] ON [Column].ColumnID = [DataReminder].ColumnID JOIN [Table] ON [Table].TableID = [Column].TableID WHERE [Table].AccountID = @nAccountID)

		DELETE FROM [DataReminder] WHERE ColumnID IN (SELECT ColumnID FROM [Column] JOIN [Table] ON [Table].TableID = [Column].TableID WHERE [Table].AccountID = @nAccountID)
		PRINT '[DataReminder]'


		DELETE FROM [ScheduledTaskColumn] 
			WHERE ColumnID IN (SELECT ColumnID FROM [Column] JOIN [Table] ON [Table].TableID = [Column].TableID WHERE [Table].AccountID = @nAccountID)
		PRINT '[ScheduledTaskColumn]'

		DELETE FROM [ScheduledTask] WHERE AccountID = @nAccountID
		PRINT '[ScheduledTask]'

		-- Message 
		DELETE FROM [ScheduledTask] WHERE MessageID IN (SELECT MessageID FROM [Message] WHERE [AccountID] = @nAccountID)
		DELETE FROM [Message] WHERE [AccountID] = @nAccountID


		DELETE FROM [GraphOptionDetail] WHERE ColumnID IN (SELECT ColumnID FROM [Column] JOIN [Table] ON [Table].TableID = [Column].TableID WHERE [Table].AccountID = @nAccountID)
		DELETE FROM [Column] WHERE TableID IN (SELECT TableID FROM [Table] WHERE AccountID = @nAccountID)
		PRINT '[Column]'

		DELETE FROM [Upload] 
			WHERE TableID IN (SELECT TableID FROM [Table] WHERE AccountID = @nAccountID)
		PRINT '[Upload]'

		--DELETE FROM [UserTable] WHERE TableID IN (SELECT TableID FROM [Table] WHERE AccountID = @nAccountID)
		--PRINT '[UserTable]'
		--DELETE FROM [UserTable] WHERE UserID IN(SELECT [User].UserID FROM [User] INNER JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1)
		--PRINT '[UserTable]'

		DELETE FROM [Record] WHERE TableID IN (SELECT TableID FROM [Table] WHERE AccountID = @nAccountID)
		PRINT '[Record]'

		DELETE FROM [Record] WHERE BatchID in (SELECT BatchID FROM [Batch] 
			WHERE AccountID = @nAccountID
			OR TableID IN (SELECT TableID FROM [Table] WHERE AccountID = @nAccountID)
			OR UserIDUploaded IN(SELECT [User].UserID FROM [User] INNER JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1)
			)

		-- Must be after Record
		DELETE FROM [Batch] 
			WHERE AccountID = @nAccountID
			OR TableID IN (SELECT TableID FROM [Table] WHERE AccountID = @nAccountID)
			OR UserIDUploaded IN(SELECT [User].UserID FROM [User] INNER JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1)
		PRINT 'Batch'

		DELETE FROM [TempRecord] WHERE AccountID = @nAccountID
		PRINT 'TempRecord'
		DELETE FROM [TableUser] WHERE UserID IN (SELECT [User].UserID FROM [User] INNER JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1)
		PRINT 'TableUser'

		DELETE FROM [LinkedUser] WHERE AccountID = @nAccountID
			OR UserID IN(SELECT [User].UserID FROM [User] INNER JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1)
		PRINT '[LinkedUser]'

		DELETE FROM [UserContent] WHERE UserID IN(SELECT [User].UserID FROM [User] INNER JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1)
		PRINT '[UserContent]'

		DELETE FROM [User] WHERE UserID IN(SELECT [User].UserID FROM [User] INNER JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  AccountID = @nAccountID AND IsPrimaryAccount=1)
		PRINT '[User]'
		DELETE FROM Usage WHERE AccountID = @nAccountID
		PRINT 'Usage'
		DELETE FROM [Table] WHERE AccountID = @nAccountID
		PRINT '[Table]'
		DELETE FROM [Menu] WHERE AccountID = @nAccountID
		PRINT 'Menu'
		DELETE FROM [Audit] WHERE AccountID = @nAccountID
		PRINT 'Audit'
		DELETE FROM GraphOptionDetail WHERE GraphOptionID IN (SELECT GraphOptionID FROM GraphOption WHERE AccountID = @nAccountID)
		DELETE FROM GraphOption WHERE AccountID = @nAccountID
		OR [GraphOptionID] IN (SELECT DefaultGraphOptionID FROM Account WHERE AccountID = @nAccountID)
		PRINT 'GraphOption'

		DELETE FROM [Invoice] WHERE AccountID = @nAccountID
		PRINT 'Invoice'

		DELETE FROM Terminology WHERE AccountID = @nAccountID
		PRINT 'Terminology'

		DELETE FROM GraphOption WHERE AccountID = @nAccountID
		PRINT 'GraphOption'

		DELETE FROM UserRole WHERE AccountID = @nAccountID
		PRINT 'UserRole'

		DELETE FROM dbo.WorkFlowSection WHERE WorkFlowID IN (SELECT WorkFlowID FROM WorkFlow WHERE AccountID = @nAccountID) 

		DELETE FROM WorkFlow WHERE AccountID = @nAccountID
		PRINT 'WorkFlow'

		DELETE FROM [Content] WHERE AccountID = @nAccountID

		-- Role
		DELETE FROM [Role] WHERE AccountID = @nAccountID

		DELETE FROM [Account] WHERE AccountID = @nAccountID
		PRINT 'Account'

			 
--	COMMIT TRANSACTION --finally, Commit the transaction as all Success.
	PRINT 'OK'
END TRY

BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Account_Delete_Permanent', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
