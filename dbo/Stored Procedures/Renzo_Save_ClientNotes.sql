﻿CREATE PROCEDURE [dbo].[Renzo_Save_ClientNotes] 
(
	@RecordID  int,		/*	Record ID passed from the table on execute, base your solution on this value	*/
	@UserID int,
	@Return varchar(max) output
	, @Context varchar(max) = null
)
AS

/*

EXEC [Renzo_Save_ClientNotes] @RecordID = '2717569',@UserID = '25197',@Return=''

*/
BEGIN
	SET DATEFORMAT dmy;
	DECLARE @FollowUpBy varchar(10)

	SELECT @FollowUpBy = V011 FROM [Record] WHERE RecordID = @RecordID
    
    /* lets update the follow up date by days calculation */
	IF (@FollowUpBy = '1')
	BEGIN
	    UPDATE [Client Notes]
		  SET [Client Notes].V008 = [Client Notes].[V013]--CONVERT(varchar(10),DATEADD(DAY,CAST([Job Notes].V010 AS INT),CONVERT(DATE,[Job Notes].V003,103)),103)
	    FROM [Record] [Client Notes]
	    WHERE [Client Notes].RecordID = @RecordID
		  AND [Client Notes].TableID = 2655
		  AND [Client Notes].IsActive = 1
	END
	

	RETURN @@ERROR;
END;
