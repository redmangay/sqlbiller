﻿
 
CREATE PROCEDURE [dbo].[dbg_User_Validation]
(
	@nBatchID INT
)
	/*----------------------------------------------------------------
	DECLARE @nNumberImported int
	EXEC [dbg_Users_Import] 3425, @nNumberImported out
	PRINT cast(@nNumberImported as varchar)
	---------------------------------------------------------------*/
 
AS
 
BEGIN TRY
	BEGIN TRANSACTION --beginning a transaction..


	DECLARE Records_Cursor CURSOR FOR
	SELECT RecordID,V003 FROM TempRecord WHERE V003 IS NOT NULL
	AND TempRecord.BatchID=@nBatchID

	DECLARE @sV003 varchar(MAX)
	DECLARE @nRecordID INT

	DECLARE @nCounter INT=0;

	OPEN Records_Cursor;
	FETCH NEXT FROM Records_Cursor INTO @nRecordID,@sV003;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
	  
		  DECLARE @sEmailUX nvarchar(200)=NULL
		  SELECT @sEmailUX=Email FROM [User] WHERE Email=@sV003
		
			IF @sEmailUX IS NOT NULL
				BEGIN
					DECLARE @sRejectReason varchar(MAX)=NULL
				
					SELECT @sRejectReason=RejectReason FROM TempRecord WHERE RecordID=@nRecordID
				
					IF @sRejectReason IS NULL
						UPDATE TempRecord SET RejectReason='DUPLICATE:Email-' + @sEmailUX  WHERE RecordID=@nRecordID
				
					IF @sRejectReason IS NOT NULL
						UPDATE TempRecord SET @sRejectReason=RejectReason + ' DUPLICATE:Email-' + @sEmailUX  WHERE RecordID=@nRecordID
					
					SET @nCounter=@nCounter+1		
			
				END
	  
	  
		  FETCH NEXT FROM Records_Cursor INTO @nRecordID,@sV003;
	   END;
	CLOSE Records_Cursor;
	DEALLOCATE Records_Cursor;



	COMMIT TRANSACTION --finally, Commit the transaction as all Success.
END TRY
BEGIN CATCH			   
	ROLLBACK TRANSACTION --RollBack Transaction as some where error happend.
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_User_Validation', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
