﻿CREATE procedure [dbo].[usr_rpt_ChargeAbility]

(
 @Group  varchar(max) =null,
 @Employee varchar(max)=null,
 @startdate date ='2015-10-02',
 @enddate date ='2016-03-09'

)
as
 
 set dateformat dmy
        exec usr_rpt_EmployeeHistroy
    
    SELECT  EmployeeHistroy.EmployeeID AS Employee, EmployeeHistroy.[Group/Division] AS GroupDivision, 
    sum(case when (cast (Administration.Code_Status as int)) ='2' then (cast(TimeSheet.Hours as money)) else 0 end ) as TotalHrsScheduledLeave,
    sum(case when (cast( Administration.Code_Status as int)) ='3' then (cast(TimeSheet.Hours as money))  else 0 end) as TotalHrsOtherLeave, 
    Sum(cast(TimeSheet.[$_Amount] as money)) AS AdminTimeSheetDollar,
  EmployeeHistroy.Name,  Sum(cast([TimeSheet].[Total] as money)) AS TimeSheetDollar, Sum(cast(TimeSheet.Hours as money)) AS TotalHrsWorked 
    
    into #tempdata 
    FROM 
    (Account24918.vTimeSheet TimeSheet INNER JOIN ((Account24918.vPNEmployeeIDTimeSheetID PNEmployeeIDTimeSheetID INNER JOIN ##EmployeeHistroy qryEmployeeHistory ON 
    PNEmployeeIDTimeSheetID.EmployeeID = qryEmployeeHistory.EmployeeID) INNER JOIN 
   Account24918.vEmployeeHistroy EmployeeHistroy ON (qryEmployeeHistory.EmployeeID = EmployeeHistroy.EmployeeID) 
    AND (qryEmployeeHistory.MaxOfDate = EmployeeHistroy.Date)) ON TimeSheet.TimeSheetID = PNEmployeeIDTimeSheetID.TimeSheetID) 
     LEFT JOIN Account24918.vProject Project ON PNEmployeeIDTimeSheetID.ProjectNumber = Project.ProjectNumber 
     left JOIN Account24918.vAdministration Administration ON PNEmployeeIDTimeSheetID.ProjectNumber = Administration.Code
    --WHERE 
    --(((TimeSheet.Date)) >=@startdate  AND  ((TimeSheet.Date)) <= @enddate)
	
    
 
   
    
    GROUP BY
    EmployeeHistroy.[Group/Division], EmployeeHistroy.Name, EmployeeHistroy.EmployeeID 
    
       select 
       
         Employee, 
         GroupDivision, 
         TotalHrsScheduledLeave,
        TotalHrsOtherLeave,
         AdminTimeSheetDollar,
  Name,  
  TimeSheetDollar, 
   TotalHrsWorked -(TotalHrsScheduledLeave + TotalHrsOtherLeave) as TotalHrWorked
        
       
       
        from #tempdata

        order by name

