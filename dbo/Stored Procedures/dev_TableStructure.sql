﻿

CREATE PROCEDURE [dbo].[dev_TableStructure]
(
	@TableID int
)
AS

BEGIN TRY

	SELECT	T.TableID, T.TableName, C.[SystemName], C.[DisplayName], C.ColumnType, 
		CASE 
			WHEN C.DropDownType = 'tabledd' THEN 'Table' 
			WHEN C.DropDownType = 'values' THEN 'Values Only' 
			WHEN C.DropDownType = 'value_text' THEN 'Values and Text' 
			WHEN C.DropDownType = 'value_text' THEN 'Values and Images' 
			WHEN C.DropDownType = 'table' THEN 'Linked' 
			ELSE '' 
		END AS DropDownType, 
		
		C.TableTableID as [Dropdown Table ID]
		FROM			[Table] T
		JOIN			[Column] C ON	                C.TableID = T.TableID
											AND			(C.IsStandard = 0
											OR			C.SystemName = 'RecordID')
		WHERE			T.TableID = @TableID
		ORDER BY		C.[DisplayOrder]

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dev_TableStructure', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
