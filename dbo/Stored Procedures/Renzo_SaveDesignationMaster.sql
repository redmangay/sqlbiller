﻿CREATE PROCEDURE [dbo].[Renzo_SaveDesignationMaster]
(
	@RecordID  int,		/*	Record ID passed from the table on execute, base your solution on this value	*/
	@UserID int,
	@Return varchar(max) output
	, @Context varchar(max) = null
)
AS

	Declare @MasterRate money
	Declare @Rate money
	DECLARE @MasterStatus varchar(max)
	
	
	SELECT @MasterRate = V002, @MasterStatus = V004 FROM [RECORD]
			WHERE RECORDID = @RecordID
--			EXEC [Renzo_SaveDesignationMaster] '1828152','',''
	
		DECLARE @RecordID_RateUsed int
		
		DECLARE Designation_Master CURSOR FOR
		SELECT [RecordID] FROM [RECORD] WHERE V007 = @RecordID AND TABLEID = 2477 ORDER BY [RecordID];
	

		OPEN Designation_Master;
		FETCH NEXT FROM Designation_Master
		INTO @RecordID_RateUsed

		WHILE @@FETCH_STATUS = 0
		   BEGIN
		   
			/* lets get the rate used in the Job */
			SELECT @Rate= V002 FROM [RECORD]
			WHERE RECORDID = @RecordID_RateUsed
								
			IF @MasterRate = @Rate
			BEGIN
					--SELECT @MasterRate,@Rate
					UPDATE [Record] SET V004 = 'No'
					WHERE [RecordID] = @RecordID_RateUsed --AND V004 = 'No'
			END
			ELSE 
			BEGIN
					UPDATE [Record] SET V004 = 'Yes'
					WHERE [RecordID] = @RecordID_RateUsed --AND V004 = 'No'
			END
		   
		   
		  FETCH NEXT FROM Designation_Master
		  INTO @RecordID_RateUsed
			  
		   END;
		

		CLOSE Designation_Master;
		DEALLOCATE Designation_Master;

/*======================================*/

/* lets update the status */
/* lets get the last status from Designation Rate table 
    as the basis if the Master status is changed  */

DECLARE @DesignationStatus varchar(max) = '';
SELECT @DesignationStatus = V008 
    FROM [Record]
	   WHERE RecordID = (SELECT MAX(RecordID) FROM [Record]  WHERE TableID = 2477)

/* lets match the status to the Master status */
/* if not matched; lets update all */
IF (@MasterStatus <> @DesignationStatus)
BEGIN
    /* lets update the Designation Rate status */
    UPDATE Record
	   SET V008 = @MasterStatus
		  WHERE V007 = @RecordID
			 AND TableID = 2477
			 

    /* lets update the Designation Type status */
  

END

DECLARE @DesignationTypeStatus varchar(max) = '';
SELECT @DesignationTypeStatus = V002 
    FROM [Record]
	    WHERE RecordID = (SELECT V003 FROM [Record] WHERE RecordID =  @RecordID)
	     AND TableID = 2726

IF (@MasterStatus <> @DesignationTypeStatus)
BEGIN
  UPDATE Record
	   SET V002 = @MasterStatus
		  WHERE RecordID = (SELECT V003 FROM [Record] WHERE RecordID =  @RecordID)
			 AND TableID = 2726
END

/*======================================*/


--IF NOT EXISTS(SELECT * FROM Record WHERE V007 = CAST(@RecordID AS varchar))
--BEGIN
--EXEC [dbo].[Renzo_SaveDesignationMaster_Add] @RecordID = @RecordID, @UserID = @UserID, @Return = ''
--END



RETURN @@ERROR;