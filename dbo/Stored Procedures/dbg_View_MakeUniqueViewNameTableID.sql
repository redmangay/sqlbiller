﻿ 
 
 CREATE PROCEDURE [dbo].[dbg_View_MakeUniqueViewNameTableID]

	/*----------------------------------------------------------------
	
SELECT TableID, ViewName, count(*) as qty 
 FROM [View] 
 GROUP BY TableID, ViewName HAVING count(*)> 1
 

SELECT * FROM [View] WHERE TableID=2385 AND ViewName='1st ED Arrival Summary'
 
EXEC [dbo].[dbg_View_MakeUniqueViewNameTableID]
	---------------------------------------------------------------*/
 
AS
 
BEGIN TRY  
	DECLARE Cur_UniqueViewNameTableID CURSOR FOR
	SELECT TableID, ViewName, count(*) as Qty 
	FROM [View] 
	 GROUP BY TableID, ViewName HAVING count(*)> 1;
 
	DECLARE @nTableID INT;
	DECLARE @sViewName varchar(500);
	DECLARE @nQty INT;
	DECLARE @sNewViewName varchar(500);

	OPEN Cur_UniqueViewNameTableID;
	FETCH NEXT FROM Cur_UniqueViewNameTableID INTO @nTableID,@sViewName,@nQty;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
	  
	  
			DECLARE Cur_ViewID CURSOR FOR
			SELECT ViewID FROM [View] WHERE TableID=@nTableID AND ViewName=@sViewName;
			DECLARE @nViewID INT;
		
			OPEN Cur_ViewID;
			FETCH NEXT FROM Cur_ViewID INTO @nViewID;
			WHILE @@FETCH_STATUS = 0
			   BEGIN
			  
				  SELECT @sNewViewName=dbo.fnNextAvailableViewName(@sViewName,@nTableID,@nViewID);
			  
				  IF @sViewName<>@sNewViewName
				  UPDATE [View]
				  SET ViewName=@sNewViewName
				  WHERE ViewID=@nViewID
			 			  
				  FETCH NEXT FROM Cur_ViewID INTO @nViewID;
			   END;
			CLOSE Cur_ViewID;
			DEALLOCATE Cur_ViewID;  
	 
		  FETCH NEXT FROM Cur_UniqueViewNameTableID INTO @nTableID,@sViewName,@nQty;
	   END;
	CLOSE Cur_UniqueViewNameTableID;
	DEALLOCATE Cur_UniqueViewNameTableID;
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_View_MakeUniqueViewNameTableID', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
