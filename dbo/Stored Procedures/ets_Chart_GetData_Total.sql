﻿

CREATE PROCEDURE [dbo].[ets_Chart_GetData_Total]
       @nTableID int,
	   @sGraphXAxisColumnID varchar(MAX),
       @sValueFields varchar(MAX),
       @dStartDate datetime,
       @dEndDate datetime,
       @sPeriod varchar(20),
	   @GraphSeriesColumnID varchar(MAX),
	   @GraphSeriesID varchar(MAX),
	   @ForceAverage bit,
       @bPercent bit = 0,
	   @nMaxRows int = NULL
AS

BEGIN TRY
    SET NOCOUNT ON;
    --SET DATEFORMAT dmy  
    DECLARE @Sql varchar(MAX)       
    DECLARE @sDateTime varchar(500)
    DECLARE @sSeries varchar(MAX)

	SET  @sSeries = 'CAST(dbo.RemoveNonNumericChar(' + @sValueFields + ') AS decimal(18,4)) '

	SET @sDateTime = 'CAST(CAST((SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')) AS date) AS datetime)'

	SET @sql =  ' SELECT ' + @sDateTime + ' AS [' + @sGraphXAxisColumnID + '], ' + @sSeries + 'AS [' + @sValueFields + ']' + ', [Record].RecordID AS RecordID' +
				' FROM [Record]' +
				' WHERE IsActive=1 AND CAST((SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')) AS datetime) >= ''' + CONVERT(varchar(20), @dStartDate) + '''' +
				' AND CAST((SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')) AS datetime) <= ''' + CONVERT(varchar(20), @dEndDate) + '''' +
				' AND TableID = ' + CAST(@nTableID AS Varchar(20)) +
				CASE
					WHEN @GraphSeriesColumnID IS NULL THEN ''
					WHEN @GraphSeriesID IS NULL THEN ''
					WHEN LEN(@GraphSeriesColumnID) = 0 THEN ''
					WHEN LEN(@GraphSeriesID) = 0 THEN ''
					ELSE ' AND [' + @GraphSeriesColumnID + '] = ''' + @GraphSeriesID + ''''
				END +
				' AND ISNUMERIC(dbo.RemoveNonNumericChar(' + @sValueFields + ')) = 1'

	SET @sql =  'SELECT [' + @sGraphXAxisColumnID + '], SUM([' + @sValueFields + ']) AS ' + 
				CASE WHEN LEN(@GraphSeriesID) = 0 THEN 'Total,' ELSE ''''+ @GraphSeriesID +''',' END +  
				' MAX(RecordID) AS RecordID FROM ('+ @sql +') AS Main' + 
				' GROUP BY  Main.[' + @sGraphXAxisColumnID + ']' +
				' ORDER BY  Main.[' + @sGraphXAxisColumnID + ']'

	PRINT @sql
    EXEC(@sql)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Chart_GetData_Total', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
