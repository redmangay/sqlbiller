﻿
CREATE PROCEDURE [dbo].[ets_Report_Update]
(
	@nReportID int output,
	@nAccountID int,
	@sReportName varchar(250),
	@sReportDescription varchar(MAX)=NULL,
	@nReportDates int,
	@nReportDatesBefore int,
	@dStartDate datetime = NULL,
	@dEndDate datetime = NULL,
	@nUserID int = NULL,
	@nDaysFrom int = NULL, --RP Added Ticket 4513
	@nDaysTo int = NULL 
)
AS

BEGIN TRY


    SET  @sReportName=dbo.fnNextAvailableReportName(@sReportName,@nAccountID,@nReportID)

	UPDATE [Report]
	SET 
		AccountID = @nAccountID,
		ReportName = @sReportName,
		ReportDescription = @sReportDescription,
		ReportDates = @nReportDates,
		ReportDatesBefore = @nReportDatesBefore,
		StartDate = @dStartDate,
		EndDate = @dEndDate,
		UserID = @nUserID,
		[DaysFrom] = @nDaysFrom, --RP Added Ticket 4513
		[DaysTo] = @nDaysTo 
	WHERE ReportID = @nReportID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Report_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH


