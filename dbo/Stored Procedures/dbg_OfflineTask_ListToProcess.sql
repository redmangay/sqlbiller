﻿
 
CREATE PROCEDURE [dbo].[dbg_OfflineTask_ListToProcess]

AS
 
BEGIN try

	SELECT OT.*,U.FirstName,U.LastName,U.Email 
		FROM OfflineTask OT INNER JOIN Account A
		ON OT.AccountID=A.AccountID INNER JOIN [User] U
		ON OT.AddedByUserID=U.UserID
		WHERE A.IsActive=1 AND U.IsActive=1 AND OT.ActuallyRun IS NULL
		AND OT.ScheduledToRun IS NOT NULL AND OT.ScheduledToRun<GETDATE()
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_OfflineTask_ListToProcess', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
