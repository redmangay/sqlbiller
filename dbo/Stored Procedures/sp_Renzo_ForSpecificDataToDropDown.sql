﻿CREATE PROCEDURE [dbo].[sp_Renzo_ForSpecificDataToDropDown] -- Two tables renzo

/*
HISTORY:
03-03-2017: RED - For population of Disbursement Predictive Item dropdown.

exec [dbo].[sp_Renzo_ForSpecificDataToDropDown] '2706765','','','','','','',''

*/

(
	@RecordID  int,		/*	Record ID passed from the table on execute, base your solution on this value	*/
	@TableIDOne int=null,
	@TableIDTwo int=null,	
	@ColumnIDOne int =null,
	@ColumnIDTwo int = null,
	@swhere VARCHAR(1000) = null,
	@Search varchar(50)=null,
	@UserID int = null
)
AS

SELECT			[TableOne].[RecordID],[TableTwo].V001 +  ISNULL(' ' + [TableONe].[V003],'') AS DisplayText
FROM			[Record] [TableONe]
JOIN			[Record] [TableTwo] ON				1 = 1
													AND		[TableTwo].RecordID = [TableONe].V001
													AND		[TableTwo].IsActive = 1
													AND		[TableTwo].TableID = 2728  -- Disbursement Type
													AND		[TableTwo].V001 LIKE '%'+@Search+'%'
WHERE			1 = 1
AND				[TableONe].V004 = @RecordID
AND				[TableONe].IsActive = 1
AND				[TableONe].TableID = 2478  -- Disbursement Rate
AND				[TableONe].IsActive = 1

--GROUP BY [TableTwo].[RecordID],[TableTwo].V001 
ORDER BY [TableOne].[RecordID]


RETURN @@ERROR;

