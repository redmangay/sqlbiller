﻿
-- =============================================
-- Author:		<Author, Manoj Sharma>
-- Create date: <Create Date, 19 Sep 2016>
-- Description:	<Used to Update Job tite against matching job number in job number table>
-- =============================================

create PROCEDURE [dbo].[Renzo_SaveBill & Payment Activity_03_feb_2017]
(
	@RecordID  int,		/*	Record ID passed from the table on execute, base your solution on this value	*/
	@UserID int,
	@Return varchar(max) output
)
AS




UPDATE		[Bill & Payment Activity]
			
SET			[Bill & Payment Activity].V039	 =	CAST(ISNULL([Job Number].V003,'') AS VARCHAR(MAX)) 

FROM		[Record] [Bill & Payment Activity] 

LEFT JOIN	Record [Job Number]  ON					1=1

										AND		[Job Number].TableID = '2475'		/*	Job Information	*/
										AND		[Job Number].IsActive = 1
										AND		CAST( [Job Number].RecordID AS VARCHAR(MAX)) = [Bill & Payment Activity].V002		/*	Job Number	*/
								
	
WHERE		 1=1
AND		[Bill & Payment Activity].RecordID =@RecordID
AND     [Bill & Payment Activity].TableID = '2711'		/*	Bill & Payment Activity	*/
AND		[Bill & Payment Activity].IsActive = 1






RETURN @@ERROR;