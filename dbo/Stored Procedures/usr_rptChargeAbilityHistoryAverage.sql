﻿
/*
    Author: Red Mangay
    Date:	  19/06/2018

*/

CREATE PROCEDURE [dbo].[usr_rptChargeAbilityHistoryAverage]
AS

/*

EXEC [usr_rptChargeAbilityHistoryAverage]


*/
BEGIN TRY
	SET DATEFORMAT dmy;
	 DECLARE @ChgAverage int = 6
	 DECLARE @SQLGetAverage varchar(max) = ''

	 SELECT  @ChgAverage = V035 FROM RECORD
	 WHERE TABLEID = 2743
	 AND IsActive = 1

	DECLARE @EmployeeList TAble ([Counter] int identity(1,1), RecordID int)
	
	INSERT INTO @EmployeeList
	SELECT RecordID  FROM Record
	WHERE TableID = 2669
	AND IsActive = 1
	AND V027 = 1
	AND V002 = 'False'
	--AND RecordID = 15155012 --IN ('12911957',' 12911962')

	DECLARE @Counterx int = 1
	DECLARE @Counter int = 1
	WHILE EXISTS(SELECT * FROM @EmployeeList WHERE [Counter] >= @Counter )
	BEGIN
	    DECLARE @EmployeeID int
	    SELECT @EmployeeID = RecordID FROM @EmployeeList WHERE [Counter] = @Counter

	    DECLARE @ChgHistory Table ([Counter] int identity(1,1),RecordID int, Chg varchar(100), ChgDate date)
	   
	    INSERT INTO @ChgHistory
		  SELECT RecordID, V023, V005  FROM RECORD
		  WHERE TABLEID = 2651
		  AND ISACTIVE = 1
		  AND V024 = @EmployeeID
		  ORDER BY RecordID
	   
	   
	   WHILE EXISTS(SELECT * FROM @ChgHistory WHERE [Counter] >= @Counterx )
	   BEGIN
		   DECLARE @Chg Table (ChgAvg varchar(100))
		  
		   DECLARE @ChgRecordID int
		   DECLARE @ChgDate date 
		   DECLARE @Avg varchar(100) 

		    SELECT @ChgDate = CAST(ChgDate as date), @ChgRecordID = RecordID FROM @ChgHistory WHERE [Counter] = @Counterx

			  --IF (@ChgAverage = 2	 )
			  --BEGIN
			  
			  SET @SQLGetAverage ='
			  SELECT  SUM(CAST(Chg as money))/Count([Chargeability History].V005)
				FROM  
				    (SELECT top '+ CAST(@ChgAverage as varchar) + ' V023 as Chg ,  [Chargeability History].V005 
					   FROM RECORD [Chargeability History]  	   
					   WHERE [Chargeability History].TABLEID = 2651
					   AND [Chargeability History].ISACTIVE = 1	  
					   AND [Chargeability History].V024 = ' + CAST(@EmployeeID as varchar) + '
					   AND CAST([Chargeability History].V005 AS DATE) <= ''' + cast( @ChgDate as varchar) + '''
					   ORDER BY CAST([Chargeability History].V005 AS DATE) desc
			
				) [Chargeability History]'

				--PRINT @SQLGetAverage
				INSERT INTO @Chg EXEC(@SQLGetAverage);
				
				UPDATE Record
				SET V025 = (SELECT ChgAvg FROM @Chg)
				WHERE TableID = 2651 
				AND RecordID = @ChgRecordID
				AND IsActive = 1
				AND V024 = @EmployeeID
				--SELECT @ChgRecordID, @Avg
				DELETE FROM @Chg
			  --END

			 --  ELSE IF (@ChgAverage = 3	 )
			 -- BEGIN


			 -- SELECT @Avg= SUM(CAST(Chg as money))/Count(RecordID)
				--FROM  
				--    (SELECT top 6 V023 as Chg ,  RecordID 
				--	   FROM RECORD [Chargeability History]  	   
				--	   WHERE [Chargeability History].TABLEID = 2651
				--	   AND [Chargeability History].ISACTIVE = 1	  
				--	   AND [Chargeability History].V024 = @EmployeeID
				--	   AND [Chargeability History].RecordID <= @ChgRecordID
				--	   ORDER BY [Chargeability History].RecordID desc
			
				--) [Chargeability History]

				--UPDATE Record
				--SET V025 = @Avg
				--WHERE TableID = 2651 
				--AND RecordID = @ChgRecordID
				--AND IsActive = 1
				--AND V024 = @EmployeeID
				----SELECT @ChgRecordID, @Avg
				   
			 -- END


			 --ELSE IF (@ChgAverage = 4	 )
			 -- BEGIN


			 -- SELECT @Avg= SUM(CAST(Chg as money))/Count(RecordID)
				--FROM  
				--    (SELECT top 6 V023 as Chg ,  RecordID 
				--	   FROM RECORD [Chargeability History]  	   
				--	   WHERE [Chargeability History].TABLEID = 2651
				--	   AND [Chargeability History].ISACTIVE = 1	  
				--	   AND [Chargeability History].V024 = @EmployeeID
				--	   AND [Chargeability History].RecordID <= @ChgRecordID
				--	   ORDER BY [Chargeability History].RecordID desc
			
				--) [Chargeability History]

				--UPDATE Record
				--SET V025 = @Avg
				--WHERE TableID = 2651 
				--AND RecordID = @ChgRecordID
				--AND IsActive = 1
				--AND V024 = @EmployeeID
				----SELECT @ChgRecordID, @Avg
				   
			 -- END


		  --ELSE IF (@ChgAverage = 5	 )
			 -- BEGIN


			 -- SELECT @Avg= SUM(CAST(Chg as money))/Count(RecordID)
				--FROM  
				--    (SELECT top 6 V023 as Chg ,  RecordID 
				--	   FROM RECORD [Chargeability History]  	   
				--	   WHERE [Chargeability History].TABLEID = 2651
				--	   AND [Chargeability History].ISACTIVE = 1	  
				--	   AND [Chargeability History].V024 = @EmployeeID
				--	   AND [Chargeability History].RecordID <= @ChgRecordID
				--	   ORDER BY [Chargeability History].RecordID desc
			
				--) [Chargeability History]

				--UPDATE Record
				--SET V025 = @Avg
				--WHERE TableID = 2651 
				--AND RecordID = @ChgRecordID
				--AND IsActive = 1
				--AND V024 = @EmployeeID
				----SELECT @ChgRecordID, @Avg
				   
			 -- END


			 --ELSE IF (@ChgAverage = 6	 )
			 -- BEGIN


			 -- SELECT @Avg= SUM(CAST(Chg as money))/Count(RecordID)
				--FROM  
				--    (SELECT top 6 V023 as Chg ,  RecordID 
				--	   FROM RECORD [Chargeability History]  	   
				--	   WHERE [Chargeability History].TABLEID = 2651
				--	   AND [Chargeability History].ISACTIVE = 1	  
				--	   AND [Chargeability History].V024 = @EmployeeID
				--	   AND [Chargeability History].RecordID <= @ChgRecordID
				--	   ORDER BY [Chargeability History].RecordID desc
			
				--) [Chargeability History]

				--UPDATE Record
				--SET V025 = @Avg
				--WHERE TableID = 2651 
				--AND RecordID = @ChgRecordID
				--AND IsActive = 1
				--AND V024 = @EmployeeID
				----SELECT @ChgRecordID, @Avg
				   
			 -- END


			 -- ELSE IF (@ChgAverage = 7	 )
			 -- BEGIN


			 -- SELECT @Avg= SUM(CAST(Chg as money))/Count(RecordID)
				--FROM  
				--    (SELECT top 6 V023 as Chg ,  RecordID 
				--	   FROM RECORD [Chargeability History]  	   
				--	   WHERE [Chargeability History].TABLEID = 2651
				--	   AND [Chargeability History].ISACTIVE = 1	  
				--	   AND [Chargeability History].V024 = @EmployeeID
				--	   AND [Chargeability History].RecordID <= @ChgRecordID
				--	   ORDER BY [Chargeability History].RecordID desc
			
				--) [Chargeability History]

				--UPDATE Record
				--SET V025 = @Avg
				--WHERE TableID = 2651 
				--AND RecordID = @ChgRecordID
				--AND IsActive = 1
				--AND V024 = @EmployeeID
				----SELECT @ChgRecordID, @Avg
				   
			 -- END


			 --  ELSE IF (@ChgAverage = 8	 )
			 -- BEGIN


			 -- SELECT @Avg= SUM(CAST(Chg as money))/Count(RecordID)
				--FROM  
				--    (SELECT top 6 V023 as Chg ,  RecordID 
				--	   FROM RECORD [Chargeability History]  	   
				--	   WHERE [Chargeability History].TABLEID = 2651
				--	   AND [Chargeability History].ISACTIVE = 1	  
				--	   AND [Chargeability History].V024 = @EmployeeID
				--	   AND [Chargeability History].RecordID <= @ChgRecordID
				--	   ORDER BY [Chargeability History].RecordID desc
			
				--) [Chargeability History]

				--UPDATE Record
				--SET V025 = @Avg
				--WHERE TableID = 2651 
				--AND RecordID = @ChgRecordID
				--AND IsActive = 1
				--AND V024 = @EmployeeID
				----SELECT @ChgRecordID, @Avg
				   
			 -- END

			 -- ELSE IF (@ChgAverage = 9	 )
			 -- BEGIN


			 -- SELECT @Avg= SUM(CAST(Chg as money))/Count(RecordID)
				--FROM  
				--    (SELECT top 6 V023 as Chg ,  RecordID 
				--	   FROM RECORD [Chargeability History]  	   
				--	   WHERE [Chargeability History].TABLEID = 2651
				--	   AND [Chargeability History].ISACTIVE = 1	  
				--	   AND [Chargeability History].V024 = @EmployeeID
				--	   AND [Chargeability History].RecordID <= @ChgRecordID
				--	   ORDER BY [Chargeability History].RecordID desc
			
				--) [Chargeability History]

				--UPDATE Record
				--SET V025 = @Avg
				--WHERE TableID = 2651 
				--AND RecordID = @ChgRecordID
				--AND IsActive = 1
				--AND V024 = @EmployeeID
				----SELECT @ChgRecordID, @Avg
				   
			 -- END

			 --ELSE IF (@ChgAverage = 10	 )
			 -- BEGIN


			 -- SELECT @Avg= SUM(CAST(Chg as money))/Count(RecordID)
				--FROM  
				--    (SELECT top 6 V023 as Chg ,  RecordID 
				--	   FROM RECORD [Chargeability History]  	   
				--	   WHERE [Chargeability History].TABLEID = 2651
				--	   AND [Chargeability History].ISACTIVE = 1	  
				--	   AND [Chargeability History].V024 = @EmployeeID
				--	   AND [Chargeability History].RecordID <= @ChgRecordID
				--	   ORDER BY [Chargeability History].RecordID desc
			
				--) [Chargeability History]

				--UPDATE Record
				--SET V025 = @Avg
				--WHERE TableID = 2651 
				--AND RecordID = @ChgRecordID
				--AND IsActive = 1
				--AND V024 = @EmployeeID
				----SELECT @ChgRecordID, @Avg
				   
			 -- END

			 -- ELSE IF (@ChgAverage = 11	 )
			 -- BEGIN


			 -- SELECT @Avg= SUM(CAST(Chg as money))/Count(RecordID)
				--FROM  
				--    (SELECT top 6 V023 as Chg ,  RecordID 
				--	   FROM RECORD [Chargeability History]  	   
				--	   WHERE [Chargeability History].TABLEID = 2651
				--	   AND [Chargeability History].ISACTIVE = 1	  
				--	   AND [Chargeability History].V024 = @EmployeeID
				--	   AND [Chargeability History].RecordID <= @ChgRecordID
				--	   ORDER BY [Chargeability History].RecordID desc
			
				--) [Chargeability History]

				--UPDATE Record
				--SET V025 = @Avg
				--WHERE TableID = 2651 
				--AND RecordID = @ChgRecordID
				--AND IsActive = 1
				--AND V024 = @EmployeeID
				----SELECT @ChgRecordID, @Avg
				   
			 -- END

			 -- ELSE --IF (@ChgAverage = 12	 )
			 -- BEGIN


			 -- SELECT @Avg= SUM(CAST(Chg as money))/Count(RecordID)
				--FROM  
				--    (SELECT top 6 V023 as Chg ,  RecordID 
				--	   FROM RECORD [Chargeability History]  	   
				--	   WHERE [Chargeability History].TABLEID = 2651
				--	   AND [Chargeability History].ISACTIVE = 1	  
				--	   AND [Chargeability History].V024 = @EmployeeID
				--	   AND [Chargeability History].RecordID <= @ChgRecordID
				--	   ORDER BY [Chargeability History].RecordID desc
			
				--) [Chargeability History]

				--UPDATE Record
				--SET V025 = @Avg
				--WHERE TableID = 2651 
				--AND RecordID = @ChgRecordID
				--AND IsActive = 1
				--AND V024 = @EmployeeID
				----SELECT @ChgRecordID, @Avg
				   
			 -- END

	   SET @Counterx = @Counterx + 1
	   END
	 
	SET @Counter = @Counter + 1
	END


	/* Lets update the Column */
	UPDATE [Column]
	SET DisplayName = 'Avg: ' + @ChgAverage
	,DisplayTextSummary = 'Avg: ' + @ChgAverage
	,DisplayTextDetail = 'Avg: ' + @ChgAverage
	WHERE TableID = 2651
	AND SystemName = 'V025'
	AND DisplayTextSummary like '%Avg:%'

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('usr_rptChargeAbilityHistoryAverage', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH






















/*
== Old Script
SELECT ChargeabilityHistory.StartDate AS [Start Date], 
ChargeabilityHistory.EndDate AS [End Date], 
ChargeabilityHistory.AvgHrsPerWeek, 
ChargeabilityHistory.HrsScheduledLeave, 
ChargeabilityHistory.HrsOtherLeave,
ChargeabilityHistory.TimeSheet AS [$TimeSheet], 
ChargeabilityHistory.ChargedToJobs AS [$Jobs], 
ChargeabilityHistory.ChargedToJobsPercent,
ChargeabilityHistory.Billed AS [$Billed], 
ChargeabilityHistory.Adjustments AS [$Adjustments],
ChargeabilityHistory.TotalBilled AS [$Billed Total], 
ChargeabilityHistory.JobProdPercent, 
ChargeabilityHistory.Chargeability AS Chg,
ChargeabilityHistory.EmployeeID,
ChargeabilityHistory.ExportedTime, 
ChargeabilityHistory.ExportedDate
FROM   Account24918.vChargeabilityHistory ChargeabilityHistory;

	 */