﻿

CREATE PROCEDURE [dbo].[Column_Audit_Detail]

(
      @nColumnID int,
      @UpdateDate datetime
)

AS

BEGIN TRY
--EXEC Column_Audit_Detail 1133,'2012-03-18 21:20:34.483'
      -- DECLARE @nColumnID int
      -- SET @nColumnID = 2152
      SELECT [Audit].DateAdded,
            [User].FirstName + ' ' + [User].LastName as [User],
             FieldName,
            OldValue,
            NewValue
            FROM [Audit]
            JOIN [User] ON [User].UserID = [Audit].UserID
            WHERE TableName='Column'
            AND FieldName != 'DateUpdated'
			AND FieldName != 'LastUpdatedUserID'
            AND PrimaryKeyValue = @nColumnID
            AND [Audit].DateAdded = @UpdateDate
            ORDER BY [Audit].DateAdded
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Column_Audit_Detail', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH 
