﻿

CREATE PROCEDURE [dbo].[ets_Column_NextSystemName]
(
	@nTableID  int
)
AS
-- EXEC [ets_Column_NextSystemName] 1213
BEGIN TRY

	DECLARE @sLastName varchar(50)
	DECLARE @sNextName varchar(50)

	SELECT TOP 1 @sLastName=SystemName 
		FROM [Column] WHERE IsStandard=0 AND TableID=@nTableID
		ORDER BY SystemName DESC

	IF @sLastName is NULL
	BEGIN
	  SET @sNextName='V001'
		SELECT @sNextName
	  RETURN
	END
	PRINT @sLastName

	--IF @sLastName='V100'
	--BEGIN
	--	SELECT 'NO'
	--	RETURN
	--END

	DECLARE @iColumnNumber int 
	SET @iColumnNumber =Convert(int, SUBSTRING(@sLastName,2,3))
	--PRINT @iColumnNumber
	SET @iColumnNumber=@iColumnNumber+1
	IF @iColumnNumber<501
		BEGIN
			IF @iColumnNumber<10
				 SET @sNextName='V00' + Convert(varchar, @iColumnNumber)
			ELSE IF @iColumnNumber >9 and @iColumnNumber<100
				SET @sNextName='V0' + Convert(varchar, @iColumnNumber)
			ELSE IF @iColumnNumber >99 and @iColumnNumber<501
				SET @sNextName='V' + Convert(varchar, @iColumnNumber)	
		END
	ELSE
	  SET @sNextName='NO'
  
	   IF @sNextName='V0100' 
		 SET @sNextName='V100'
  
	  IF @sNextName='NO'
	  BEGIN
		
			DECLARE @intFlag INT
			DECLARE @sFreeNextName varchar(50)

			SET @intFlag = 1
			WHILE (@intFlag <=500)
			BEGIN

				IF @intFlag<10
						 SET @sNextName='V00' + Convert(varchar, @intFlag)
					ELSE IF @intFlag>9 and @intFlag<100
						SET @sNextName='V0' + Convert(varchar, @intFlag)
						ELSE IF @intFlag>99 and @intFlag<501
						SET @sNextName='V' + Convert(varchar, @intFlag)
			
				IF @sNextName='V0100' 
				 SET @sNextName='V100'
				SET @sFreeNextName=NULL
				SELECT @sFreeNextName=SystemName FROM [Column] WHERE SystemName=@sNextName AND TableID=@nTableID
			
				IF @sFreeNextName IS NULL	
				BEGIN
					PRINT @sNextName
					DECLARE @sSQL varchar(100);
					SET @sSQL= 'UPDATE Record SET ' + @sNextName + '=NULL WHERE TableID=' +  Convert(varchar, @nTableID)
					EXEC (@sSQL)
					BREAK
				END
				SET @intFlag = @intFlag + 1
			END
			
	  END
  
	 IF @sLastName='V500' AND @sNextName='V500'
		SET @sNextName='NO'
	
	  SELECT @sNextName
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Column_NextSystemName', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
