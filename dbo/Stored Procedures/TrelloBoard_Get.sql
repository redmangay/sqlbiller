﻿

-- =============================================
-- Author:		DH
-- Create date: 15 Aug 2017
-- Description:	Get board info
-- =============================================
CREATE PROCEDURE [dbo].[TrelloBoard_Get] 
	@RefId varchar(50)
AS

BEGIN TRY
	SET NOCOUNT ON;    
	
	SELECT	RefId As Id,
			[Name],
			ShortLink,
			DateLastActivity,
			CompanyID
	FROM	TrelloBoard
	WHERE	RefId = @RefId
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('TrelloBoard_Get', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
