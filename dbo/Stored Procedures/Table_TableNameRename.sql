﻿

CREATE PROCEDURE [dbo].[Table_TableNameRename]
(
	@TableID int,
	@OldName varchar(MAX),
	@NewName varchar(MAX)
	
)
AS 

BEGIN TRY
	-- EXEC [Table_TableNameRename] 2720,'Dept','Department'
	DECLARE @AccountID int

	SELECT @AccountID=AccountID FROM [Table] WHERE TableID=@TableID


	--[Column].[MapPopup]



	UPDATE C
		SET C.[MapPopup] = REPLACE(C.[MapPopup], '[' +@OldName +':',
		'[' +@NewName +':') 
		FROM [Column] AS C INNER JOIN [Table] AS T ON C.TableID=T.TableID 
		WHERE T.AccountID = @AccountID AND CHARINDEX('[' +@OldName +':',C.[MapPopup])>0


	--[DataReminder].[ReminderContent]


	UPDATE DR
		SET DR.[ReminderContent] = REPLACE(DR.[ReminderContent], '[' +@OldName +':' ,
		'[' +@NewName +':' ) 
		FROM [DataReminder] AS DR INNER JOIN [Column] AS C ON DR.ColumnID=C.ColumnID  
		INNER JOIN [Table] T ON C.TableID=T.TableID
		WHERE T.AccountID = @AccountID AND CHARINDEX('[' +@OldName +':',DR.[ReminderContent])>0


	-- [Table].[HeaderName]


	UPDATE [Table]  
		SET [HeaderName] = REPLACE([HeaderName] , '[' +@OldName +':' ,
		'[' +@NewName +':') 
		WHERE AccountID = @AccountID AND CHARINDEX('[' +@OldName +':',[HeaderName])>0


	

	--for calendar display text DocumentSection.Details


	UPDATE DS
		SET DS.Details=REPLACE(DS.Details ,'[' + @OldName + ':' ,
		'[' + @NewName + ':' ) 
		FROM DocumentSection AS DS INNER JOIN Document D ON DS.DocumentID=D.DocumentID   
		WHERE DocumentSectionTypeID=11 AND D.AccountID=@AccountID
		AND CHARINDEX('[' +@OldName +':',DS.Details)>0
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Table_TableNameRename', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
