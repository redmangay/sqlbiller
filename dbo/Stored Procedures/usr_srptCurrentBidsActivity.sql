﻿CREATE procedure [dbo].[usr_srptCurrentBidsActivity]
(
@Group varchar(max) =null,
@Controller varchar(max) =null,
@From date =null,
@To date  =null,
@filter varchar(50) ='0'



)
as
SELECT Client.Company,
 Project.ProjectNumber,
  Project.ProjectTitle, Project.Date AS ProjectDate, Project.[Group/Division],
  Project.[Status],
isnull(dbo.ConvertToGroupCurrency(cast(ISNULL(Value, 0) as money),cast(ISNULL(CurrencyRate, 0) as money), cast(ISNULL(RptCurrencyRate, 0) as money)),0) as ProjectValue
 , Project.EmployeeID AS Controller,
 case when cast(ISNULL(CurrencyRate, 0) as money)=0 then '' 
      when [FixedPrice] ='False' then '' 
      else 'C' end as IsConverted
      into #rptCurrentBidsActivity
 FROM Account24918.vSubsidiary Client   INNER JOIN Account24918.vProject Project ON Client.ClientID = Project.ClientID


SELECT Sum(rptCurrentBidsActivity.ProjectValue) AS JobValue,
 Count(*) AS Bids, 
 Name,
 IsNull(ReportingCurrency,'') as ReportingCurrency,
  rptCurrentBidsActivity.[Group/Division]
FROM  #rptCurrentBidsActivity rptCurrentBidsActivity
 INNER JOIN  Account24918.vEmployee_List e ON (rptCurrentBidsActivity.Controller = e.EmployeeID) 
 LEFT JOIN Account24918.vList_Of_Group_Division [List_Of_Group/Division] ON rptCurrentBidsActivity.[Group/Division] = [List_Of_Group/Division].[Group/Division]
where 

(rptCurrentBidsActivity.Status ='1'

and
--( (@group ='All') or (rptCurrentBidsActivity.[Group/Division] =@group))
--and
--( (@controller ='All') or (name =@controller))
( ((@filter ='0') and ((@group ='All') or (rptCurrentBidsActivity.[Group/Division] =@group)))  OR ( ( (@filter ='1') AND (name =@controller))) )

)


GROUP BY Name, rptCurrentBidsActivity.[Group/Division],IsNull(ReportingCurrency,'')
ORDER BY Name, rptCurrentBidsActivity.[Group/Division];
