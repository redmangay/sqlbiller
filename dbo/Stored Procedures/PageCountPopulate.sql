﻿
 
CREATE PROCEDURE [dbo].[PageCountPopulate]

	/*----------------------------------------------------------------
	EXEC PageCountPopulate
	---------------------------------------------------------------*/
 
AS

BEGIN TRY
	DECLARE @dateMax datetime

	SELECT @dateMax=MAX([Date]) FROM [PageCount]

	IF @dateMax is NULL
		SET @dateMax=DATEADD(day,-1000,getdate())

	--PRINT cast(@dateMax AS varchar(30))


	INSERT INTO [PageCount]([Date],PageURL,[Count])
	SELECT cast (DateAdded as date),PageURL,COUNT(*) FROM VisitorLog 
	WHERE cast (DateAdded as date)< cast (DATEADD(DAY,0, Getdate()) as date) 
	AND cast (DateAdded as date)>cast ( @dateMax as date)
	GROUP BY cast (DateAdded as date) ,PageURL
	ORDER by cast (DateAdded as date)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('PageCountPopulate', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
