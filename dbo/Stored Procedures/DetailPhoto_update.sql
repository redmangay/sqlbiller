﻿

CREATE PROCEDURE [dbo].[DetailPhoto_update]
(
	@Filename varchar(100),
	@TableID int,
	@Size int,
	@ColumnID int
)
AS

BEGIN TRY
	UPDATE DetailPhoto
		SET [Filename] = @Filename,	
			[Size] = @Size
		WHERE TableID = @TableID 
		AND ColumnID = @ColumnID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('DetailPhoto_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
