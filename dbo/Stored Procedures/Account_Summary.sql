﻿

 
CREATE PROCEDURE [dbo].[Account_Summary]
(
	@nAccountID int  = NULL,
	@sAccountName nvarchar(100)  = NULL,
	@sName nvarchar(50)  = NULL,
	@sEmail nvarchar(200)  = NULL,	
	@sPhoneNumber varchar(20)  = NULL,
	@bIsActive bit  = NULL,
	@dDateAdded datetime  = NULL,
	@dDateUpdated datetime  = NULL,
	@sOrder nvarchar(200) = AccountID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
	
)
	/*----------------------------------------------------------------
	EXEC [dbo].[Account_Summary]
	---------------------------------------------------------------*/
 
AS

BEGIN TRY

	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND AccountID = '+CAST(@nAccountID AS NVARCHAR)
	IF @sAccountName IS NOT NULL 
		SET @sWhere = @sWhere + ' AND AccountName LIKE '+'''%' + @sAccountName + '%'''
	IF @sName IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Name LIKE '+'''%' + @sName + '%'''
	IF @sPhoneNumber IS NOT NULL 
		SET @sWhere = @sWhere + ' AND PhoneNumber LIKE '+'''%' + @sPhoneNumber + '%'''
	IF @sEmail IS NOT NULL 
		SET @sWhere = @sWhere + ' AND AccEmail LIKE '+'''%' + @sEmail + '%'''
	IF @bIsActive IS NOT NULL 
		SET @sWhere = @sWhere + ' AND IsActive = '+CAST(@bIsActive AS NVARCHAR)

	IF @dDateAdded IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DateAdded = '+CAST(@dDateAdded AS NVARCHAR)
	IF @dDateUpdated IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DateUpdated = '+CAST(@dDateUpdated AS NVARCHAR)
	SET @sSelect = 'SELECT * FROM(SELECT AccFirst.*,ROW_NUMBER() OVER(ORDER BY '+ @sOrder +' ) as RowNum FROM
	(SELECT AccountID,AccountTypeID, AccountName,ExpiryDate,SMSCount, 
	(SELECT Top 1 FirstName from [User] INNER JOIN UserRole ON
	[User].UserID=UserRole.UserID WHERE [UserRole].accountid=Account.AccountID
	 AND [UserRole].IsAccountHolder=1 ORDER BY [User].UserID DESC) FirstName, 
	 (SELECT Top 1 LastName from [User] INNER JOIN UserRole ON
	[User].UserID=UserRole.UserID  WHERE [UserRole].accountid=Account.AccountID
	 AND [UserRole].IsAccountHolder=1 ORDER BY [User].UserID DESC) as LastName, 
	 (SELECT Top 1 PhoneNumber from [User] INNER JOIN UserRole ON
	[User].UserID=UserRole.UserID  WHERE [UserRole].accountid=Account.AccountID
	 AND [UserRole].IsAccountHolder=1 ORDER BY [User].UserID DESC) as PhoneNumber, DateAdded, 
	(SELECT Top 1 Email from [User] INNER JOIN UserRole ON
	[User].UserID=UserRole.UserID  WHERE [UserRole].accountid=Account.AccountID
	 AND [UserRole].IsAccountHolder=1 ORDER BY [User].UserID DESC) As AccEmail,
	 (SELECT Top 1 FirstName from [User] INNER JOIN UserRole ON
	[User].UserID=UserRole.UserID WHERE [UserRole].accountid=Account.AccountID
	 AND [UserRole].IsAccountHolder=1 ORDER BY [User].UserID DESC) + '' '' + 
	 (SELECT Top 1 LastName from [User] INNER JOIN UserRole ON
	[User].UserID=UserRole.UserID WHERE [UserRole].accountid=Account.AccountID
	 AND [UserRole].IsAccountHolder=1 ORDER BY [User].UserID DESC) as  Name,
	 (SELECT SUM(SignedInCount)  from Usage WHERE Usage.accountid=Account.AccountID) as  SignedInCount,
	  (SELECT SUM(UploadedCount)  from Usage WHERE Usage.accountid=Account.AccountID) as  UploadedCount,
	  (SELECT COUNT(VisitorLogID) from VisitorLog VL JOIN [UserRole] UVL ON UVL.UserID = VL.UserID WHERE UVL.AccountID=Account.AccountID) as  LoginCount,
	DateUpdated, IsActive,CreatedByWizard  FROM [Account])  AS AccFirst ' + @sWhere + ') as AccountInfo '

	SET @sSelectCount=REPLACE(@sSelect, 'SELECT * FROM(SELECT', 'SELECT COUNT(*) AS TotalRows FROM (SELECT')
	SET @sSelect = @sSelect + ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)

	--SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [Account] ' + @sWhere 
 
 
	EXEC (@sSelect)
	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Account_Summary', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
