﻿

CREATE PROCEDURE [dbo].[Usage_Select]
(
	@nUsageID int  = NULL,
	@nAccountID int  = NULL,
	@dDate datetime  = NULL,
	@bSignedInCount bit  = NULL,
	@bUploadedCount bit  = NULL,
	@dDateFrom datetime = NULL,
	@dDateTo datetime = NULL,
	@sOrder nvarchar(200) = UsageID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	EXEC [Usage_Select] @nAccountID= 13
	---------------------------------------------------------------*/
 
AS

BEGIN TRY
 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nUsageID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND UsageID = '+CAST(@nUsageID AS NVARCHAR)
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Usage].AccountID = '+CAST(@nAccountID AS NVARCHAR)
	IF @dDate IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Date = '+CAST(@dDate AS NVARCHAR)
	IF @bSignedInCount IS NOT NULL 
		SET @sWhere = @sWhere + ' AND SignedInCount >0 '
	IF @bUploadedCount IS NOT NULL 
		SET @sWhere = @sWhere + ' AND UploadedCount>0 '

	IF @dDateFrom IS NOT NULL 
			SELECT @sWhere = @sWhere + ' AND Date >= ''' + CONVERT(varchar(30), @dDateFrom, 120) + ''''

	IF @dDateTo IS NOT NULL 
			SELECT @sWhere = @sWhere + ' AND Date <=  ''' + CONVERT(varchar(30), @dDateTo, 120) + ''''
	

	SET @sSelect = 'SELECT * FROM 
	(SELECT [Usage].UsageID,[Usage].AccountID,[Usage].Date,SignedInCount,UploadedCount,AccountName, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum 
	FROM [Usage] INNER JOIN [Account] ON Account.AccountID=Usage.AccountID ' + @sWhere + ') as UsageInfo'
	 + ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	SET @sSelectCount = 'SELECT COUNT(*) FROM 
	(SELECT [Usage].*,(SELECT AccountName FROM Account WHERE Account.AccountID=[Usage].AccountID) AS AccountName FROM [Usage]' + @sWhere + ') as UsageInfo'
 
	-- Extend WHERE to include paging:

 
	EXEC (@sSelect)
	PRINT @sSelect

	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Usage_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
