﻿
 
CREATE PROCEDURE [dbo].[dbg_ImportTemplate_Detail]
(
	@nImportTemplateID int 
)
	/*----------------------------------------------------------------
	
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SELECT * FROM ImportTemplate
	WHERE ImportTemplateID = @nImportTemplateID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_ImportTemplate_Detail', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
