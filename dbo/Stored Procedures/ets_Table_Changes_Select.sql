﻿
 
CREATE PROCEDURE [dbo].[ets_Table_Changes_Select]
(
	@nTableID int ,
	@sOrder nvarchar(200) = 'ChangeID DESC', 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
/*
ets_Table_Changes_Select @nTableID=153
*/	
 
AS

BEGIN TRY
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)



	SET @sSelect = 'SELECT * FROM (SELECT     Table_Changes.ChangeID,Table_Changes.DateUpdated as [Date Changed], 
	IsPublic as [Public], IsImportPositional [Positional],
	 Table_Changes.IsActive as [Active],  PinImage as [Pin Image], 
	 Convert(decimal(10,2),MaxTimeBetweenRecords) as [MAX time], MaxTimeBetweenRecordsUnit as [Unit], 
   
	  ROW_NUMBER() OVER(ORDER BY '+@sOrder+') as RowNum 
	FROM         Table_Changes LEFT JOIN
	  Menu ON Menu.MenuID = Table_Changes.MenuID
	 WHERE  Table_Changes.TableID='+CAST(@nTableID AS NVARCHAR)+') as TableChangeInfo
	 WHERE  RowNum >=' + CONVERT(nvarchar(10), @nStartRow)


	SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [Table_Changes] WHERE TableID=' + +CAST(@nTableID AS NVARCHAR) 


	PRINT @sSelect 
	EXEC (@sSelect)

	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Table_Changes_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH                     
