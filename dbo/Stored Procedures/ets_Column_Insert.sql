﻿


CREATE PROCEDURE [dbo].[ets_Column_Insert]
(
	@nNewID int output,
	@nTableID int ,
	@sSystemName varchar(50) ,
	@nDisplayOrder int,
	@sDisplayTextSummary varchar(100)  = NULL,
	@sDisplayTextDetail varchar(255)  = NULL,
	@nGraphTypeID int  = NULL,
	@sValidationOnWarning nvarchar(4000)  = NULL,
	@sValidationOnEntry nvarchar(4000)  = NULL,	
	@bIsStandard bit =0,
	@sDisplayName varchar(100),
	@sGraphLabel varchar(100)=NULL,
	@sConstant varchar(MAX)=NULL,
	@sCalculation varchar(4000)=NULL,
	@bShowTotal bit=0,
	@bIgnoreSymbols bit=0,
	@nIgnoreSymbolMode int=NULL,
	@nIgnoreSymbolConstant decimal(18, 10)=NULL,
	@sNotes nvarchar(MAX)=NULL,
	@bIsRound bit=0,
	@nRoundNumber int=2,
	@bCheckUnlikelyValue bit =0,
	@sDropdownValues varchar(MAX)=NULL,
	@sImportance char(1)=NULL,
	@sAlignment varchar(25)=NULL,
	@nNumberType int=NULL,
	@sDefaultValue varchar(MAX)=NULL,
	@nAvgColumnID int=NULL,
	@nAvgNumberOfRecords int=NULL,
	@dShowGraphExceedance decimal(18, 10)=NULL,
	@dShowGraphWarning decimal(18, 10)= NULL,
	@nFlatLineNumber int=NULL,
	@dMaxValueAt decimal(18, 10)= NULL,
	@nDefaultGraphDefinitionID int = NULL,
	@nTextWidth int=NULL,
	@nTextHeight int=NULL,
	@sColumnType varchar(15)='text',
	@sDropDownType varchar(15)=NULL,
	@nTableTableID int=NULL,
	@sDisplayColumn varchar(MAX)= NULL,
	@bDisplayRight bit=0,
	@sSummaryCellBackColor varchar(50)=NULL,
	@nParentColumnID int=NULL,
	@sTextType varchar(25) =NULL,
	@sRegEx varchar(MAX)=NULL,
	@sDateCalculationType varchar(15)=NULL,
	@nLinkedParentColumnID int=NULL,
	@nDataRetrieverID int=NULL,
	@bVerticalList bit =NULL,
	@bQuickAddLink bit =NULL,
	@nTableTabID int=NULL,
	@sViewName varchar(50)= NULL,
	@sDefaultType varchar(50)=NULL,
	@nDefaultColumnID int=NULL,
	@sShowViewLink varchar(10)=NULL,
	@nFilterParentColumnID int=NULL,
	@nFilterOtherColumnID int=NULL,
	@sFilterValue varchar(MAX)=NULL,
	@nMapPinHoverColumnID INT = NULL,
	@nCompareColumnID int=NULL,
	@sCompareOperator varchar(50)=NULL,
	@sMapPopup varchar(MAX)= NULL,
	@nTrafficLightColumnID INT=NULL,
	@sTrafficLightValues varchar(MAX)= NULL,
	@nDefaultRelatedTableID int=NULL,
	@bDefaultUpdateValues bit = 0,
	@bValidationCanIgnore bit=NULL,
	@bImageOnSummary bit=NULL,
	@bAllowCopy bit=NULL,
	@sValidationOnExceedance nvarchar(4000)  = NULL,
	@bColourCells bit = 0,
	@sButtonInfo varchar(1000)='',
	@sFilterOperator varchar(25)=NULL,
	@bIsReadOnly bit=NULL,
	@dShowGraphWarningMin decimal(18, 10)= NULL,
	@dShowGraphExceedanceMin decimal(18, 10)=NULL,
	@nLastUpdatedUserID INT = NULL,
	@bHideTimeSecond bit = NULL,
	@ColSpan tinyint =NULL,@RowSpan tinyint =NULL,
	@nFilterParentColumnID2 int=NULL,
	@nFilterOtherColumnID2 int=NULL,
	@sFilterValue2 varchar(MAX)=NULL,
	@sFilterOperator2 varchar(25)=NULL,
	/*RP Added Ticket 4610*/
	@ShowAllValuesOnSearch bit=NULL,
	/*End Modification*/
	@bCalculationNullAsZero bit = NULL, @nChildTableID int=null, @bEditableOnSummary bit=null,
	@sRolesWhoCanIgnoreValidation varchar(MAX)=NULL,
	@sSqlRetrieval varchar(MAX)=NULL,
	@nTriggerColumnID int=NULL,
	@sSQLRetrievalOption varchar(MAX)=NULL
)
	/*----------------------------------------------------------------
			EXEC ets_Column_Insert  NULL, NULL,'LocationID',0,'Location','','Location','Location',NULL,NULL,NULL

	---------------------------------------------------------------*/
 
AS

BEGIN TRY
	DECLARE @bAllowCopyRecords bit

	SELECT @bAllowCopyRecords=AllowCopyRecords FROM [Table] WHERE TableID=@nTableID

	IF @bAllowCopyRecords IS NOT NULL AND @bAllowCopyRecords=1
	BEGIN

		IF @bIsStandard =0 AND @bAllowCopy IS NULL
			SET @bAllowCopy=1

		IF @bIsStandard =1 AND @bAllowCopy IS NULL
			SET @bAllowCopy=NULL

	END 

	INSERT INTO [Column]
	(
		TableID, 
		SystemName, 
		DisplayOrder, 
		DisplayTextSummary, 
		DisplayTextDetail,
		GraphTypeID, 
		ValidationOnWarning, 
		ValidationOnEntry ,
		IsStandard,DisplayName,
		Constant,Calculation,
		ShowTotal,IgnoreSymbols,
		IgnoreSymbolMode,IgnoreSymbolConstant,
		Notes,IsRound,
		RoundNumber,CheckUnlikelyValue,GraphLabel,
		DropdownValues,Importance,Alignment,
		NumberType,DefaultValue,AvgColumnID,
		AvgNumberOfRecords,
		ShowGraphExceedance,ShowGraphWarning,FlatLineNumber,MaxValueAt,DefaultGraphDefinitionID,
		TextWidth,TextHeight ,ColumnType,DropDownType,TableTableID,
		DisplayColumn,DisplayRight ,SummaryCellBackColor,ParentColumnID,
		TextType,RegEx,DateCalculationType,
		LinkedParentColumnID,DataRetrieverID,
		VerticalList,QuickAddLink,TableTabID,ViewName,
		DefaultType,DefaultColumnID,ShowViewLink,FilterParentColumnID,
		FilterOtherColumnID,FilterValue,MapPinHoverColumnID,
		CompareColumnID,CompareOperator,MapPopup,
		TrafficLightColumnID,TrafficLightValues,DefaultRelatedTableID,
		DefaultUpdateValues,ValidationCanIgnore,ImageOnSummary,AllowCopy,
		ValidationOnExceedance,ColourCells,ButtonInfo,
		FilterOperator,IsReadOnly,ShowGraphWarningMin,ShowGraphExceedanceMin,LastUpdatedUserID,HideTimeSecond,ColSpan,RowSpan,
		FilterParentColumnID2,FilterOtherColumnID2,FilterValue2, FilterOperator2, 
		/*RP Added Ticket 4310*/
		ShowAllValuesOnSearch,
		/*End Modification*/
		CalculationNullAsZero,ChildTableID,EditableOnSummary,
		RolesWhoCanIgnoreValidation,
		SqlRetrieval,
		TriggerColumnID,
		SQLRetrievalOption
	) VALUES (
		@nTableID,
		@sSystemName,
		@nDisplayOrder,
		replace(replace(@sDisplayTextSummary,'[','('),']',')'),
		@sDisplayTextDetail,
		@nGraphTypeID,
		@sValidationOnWarning,
		@sValidationOnEntry,
		@bIsStandard,@sDisplayName,
		@sConstant,@sCalculation,
		@bShowTotal,@bIgnoreSymbols,
		@nIgnoreSymbolMode,@nIgnoreSymbolConstant,
		@sNotes,@bIsRound,@nRoundNumber,@bCheckUnlikelyValue,
		@sGraphLabel,@sDropdownValues,@sImportance,@sAlignment,
		@nNumberType,@sDefaultValue,@nAvgColumnID,
		@nAvgNumberOfRecords,
		@dShowGraphExceedance,@dShowGraphWarning,@nFlatLineNumber,@dMaxValueAt,@nDefaultGraphDefinitionID,
		@nTextWidth,@nTextHeight ,@sColumnType,@sDropDownType,@nTableTableID,
		@sDisplayColumn,@bDisplayRight,@sSummaryCellBackColor ,@nParentColumnID,
		@sTextType,@sRegEx,
		@sDateCalculationType,@nLinkedParentColumnID,
		@nDataRetrieverID,@bVerticalList,
		@bQuickAddLink,@nTableTabID,@sViewName,
		@sDefaultType,@nDefaultColumnID,@sShowViewLink,@nFilterParentColumnID,
		@nFilterOtherColumnID,@sFilterValue,@nMapPinHoverColumnID,
		@nCompareColumnID,@sCompareOperator,@sMapPopup,
		@nTrafficLightColumnID,@sTrafficLightValues,@nDefaultRelatedTableID,
		@bDefaultUpdateValues,@bValidationCanIgnore,@bImageOnSummary,@bAllowCopy,
		@sValidationOnExceedance,@bColourCells,@sButtonInfo,
		@sFilterOperator,@bIsReadOnly,@dShowGraphWarningMin,@dShowGraphExceedanceMin, @nLastUpdatedUserID,@bHideTimeSecond,@ColSpan,@RowSpan,
		@nFilterParentColumnID2,@nFilterOtherColumnID2,@sFilterValue2, @sFilterOperator2,
		/*RP Added Ticket 4310*/
		@ShowAllValuesOnSearch,
		/*End Modification*/
		@bCalculationNullAsZero,@nChildTableID,@bEditableOnSummary,
		@sRolesWhoCanIgnoreValidation,
		@sSqlRetrieval,
		@nTriggerColumnID,
		@sSQLRetrievalOption
	)
	--SELECT @nNewID = @@IDENTITY
	SELECT @nNewID = SCOPE_IDENTITY()

	EXEC [dbo].[Column_Fix_NullCoordinate] @nTableID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
	 '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
	 '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
	 '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Column_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH



