﻿CREATE PROCEDURE		[dbo].[renzo_SaveDesignationRate]
(
	@RecordID  int,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output
    , @Context varchar(max) = null
)
AS
BEGIN

	/*
					Reset the Job Rates to the Master Rates

					DECLARE		@Return		VARCHAR(MAX)
					EXEC		[renzo_SaveDesignationRate] @RecordID = 2072146, @Return = @Return

					EXEC		[dbo].[dev_GetAccountSQL] @TableID = 2477, @TableName = 'Designation Rate', @AccountID = '24918'
					EXEC		[dbo].[dev_GetAccountSQL] @TableID = 2475, @TableName = 'Job Information', @AccountID = '24918'
					EXEC		[dbo].[dev_GetAccountSQL] @TableID = 2712, @TableName = 'Designation Master', @AccountID = '24918'

					EXEC		[dbo].[dev_GetAccountSQL] @AccountID = '24918'		/*	buffalobiller1 */

					UPDATE		[Table] 
					SET			[AddRecordSP] = 'renzo_SaveDesignationRate'
								,[SPSaveRecord]  = 'renzo_SaveDesignationRate'
					--SELECT		*
					FROM		[Table]
					WHERE		[TableID] = 2477
	*/

	DECLARE			@JobRecordID		INT

	SELECT			[Designation Master].[TableID]
					,[Designation Master].RecordID	AS				[Record ID]
					,[Designation Master].V002		AS				[Rate ($hr)]
					,[Designation Master].IsActive
		INTO		#DesignationMaster
		FROM		[Record] [Designation Master]
		WHERE		[Designation Master].TableID = '2712'		/*	Designation Master	*/
		AND			[Designation Master].IsActive = 1

	/*	Update the job to be Special; if the mater rate doesnt not match the new rate. 	*/
	
	UPDATE			[Record]	/*	Designation Rate	*/
		SET			V004	= 'Yes'	/*	[Is Special]	*/
		FROM		[Record] [Designation Rate]
		LEFT JOIN	#DesignationMaster [Designation Master] ON		[Designation Master].TableID = '2712'		/*	Designation Master	*/
															AND		[Designation Master].IsActive = 1
															AND		[Designation Master].[Record ID] = [Designation Rate].V007		/*	V007	*/
		WHERE		[Designation Rate].TableID = '2477'		/*	Designation Rate	*/
		AND			[Designation Rate].IsActive = 1
		AND			ISNUMERIC( ISNULL( [Designation Rate].V007, 0 ) ) = 1
		AND			[Designation Rate].RecordID = @RecordID
		AND			(	[Designation Master].[Rate ($hr)] <> [Designation Rate].V002
					OR	[Designation Rate].V007 IS NULL
					)
		
	DROP TABLE		#DesignationMaster;

	/*	Update the job to be Special rather than Default	*/
	
	SELECT			@JobRecordID = [Designation Rate].V003	/*	[Job No]	*/
		FROM		[Record] [Designation Rate]
		WHERE		[Designation Rate].TableID = '2477'		/*	Designation Rate	*/
		AND			[Designation Rate].RecordID = @RecordID
		AND			[Designation Rate].IsActive = 1

	UPDATE			[Record]	/*	Disbursement Rate	*/
		SET			V012 = 'TRUE'	/*	[Special Rates of Charge]	*/
		FROM		[Record] [Job Information]
		WHERE		[Job Information].TableID = '2475'		/*	Job Information	*/
		AND			[Job Information].IsActive = 1
		AND			[Job Information].RecordID = @JobRecordID

END
