﻿
--------------------------------------------------------------------------------------
-- PLEASE DO NOT CHANGE THE SCRIPT ABOVE - Simply set @Sequence & @Description
-- and paste your script in BELOW:
--------------------------------------------------------------------------------------

/* ============================================================================================================== === == */



--IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'ets_TableUser_Select' AND ROUTINE_TYPE = 'PROCEDURE')  
--BEGIN
	CREATE PROCEDURE [dbo].[ets_TableUser_Select]
	(
		@nTableUserID int  = NULL,
		@nTableID int  = NULL,
		@nUserID int  = NULL,
		@bLateWarningEmail bit  = NULL,
		@bLateWarningSMS bit  = NULL,
		@bUploadEmail bit=NULL ,
		@bUploadSMS bit=NULL ,
		@bUploadWarningEmail bit=NULL,
		@bUploadWarningSMS bit=NULL,
		@bAddDataEmail bit=NULL ,
		@bAddDataSMS bit=NULL,
		@bExceedanceEmail bit=NULL ,
		@bExceedanceSMS bit=NULL 
	)
		
	 
	 /*
	 
	 exec [dbo].[ets_TableUser_Select] @nTableID= 4245
	 
	 */

	AS

	BEGIN TRY 
		SET NOCOUNT ON
		SET CONCAT_NULL_YIELDS_NULL OFF
		-- DECLARE local variables
		DECLARE @sSelect nvarchar(MAX)
		DECLARE @sSelectCount nvarchar(MAX)
		DECLARE @sWhere nvarchar(MAX)
		-- Build WHERE clause
		/*RP MODIFIED Ticket 3427 - EMD Support*/
		--SET @sWhere = ' WHERE 1=1 '
		SET @sWhere = ' WHERE 1=1 AND [User].IsActive = 1 '
		/*****END MODIFICATION ***********/
		IF @nTableUserID IS NOT NULL 
			SET @sWhere = @sWhere + ' AND TableUserID = '+CAST(@nTableUserID AS NVARCHAR)
		IF @nTableID IS NOT NULL 
			SET @sWhere = @sWhere + ' AND TableUser.TableID = '+CAST(@nTableID AS NVARCHAR)
		IF @nUserID IS NOT NULL 
			SET @sWhere = @sWhere + ' AND TableUser.UserID = '+CAST(@nUserID AS NVARCHAR)
		
		IF @bLateWarningEmail IS NOT NULL 
			SET @sWhere = @sWhere + ' AND LateWarningEmail = '+CAST(@bLateWarningEmail AS NVARCHAR)
		IF @bLateWarningSMS IS NOT NULL 
			SET @sWhere = @sWhere + ' AND LateWarningSMS = '+CAST(@bLateWarningSMS AS NVARCHAR)

		IF @bUploadEmail IS NOT NULL 
			SET @sWhere = @sWhere + ' AND TableUser.UploadEmail = '+CAST(@bUploadEmail AS NVARCHAR)
		IF @bUploadSMS IS NOT NULL 
			SET @sWhere = @sWhere + ' AND UploadSMS = '+CAST(@bUploadSMS AS NVARCHAR)
		
		IF @bUploadWarningEmail IS NOT NULL 
			SET @sWhere = @sWhere + ' AND UploadWarningEmail = '+CAST(@bUploadWarningEmail AS NVARCHAR)
		IF @bUploadWarningSMS IS NOT NULL 
			SET @sWhere = @sWhere + ' AND UploadWarningSMS = '+CAST(@bUploadWarningSMS AS NVARCHAR)



		IF @bAddDataEmail IS NOT NULL 
			SET @sWhere = @sWhere + ' AND AddDataEmail = '+CAST(@bAddDataEmail AS NVARCHAR)
		IF @bAddDataSMS IS NOT NULL 
			SET @sWhere = @sWhere + ' AND AddDataSMS = '+CAST(@bAddDataSMS AS NVARCHAR)

		IF @bExceedanceEmail IS NOT NULL 
			SET @sWhere = @sWhere + ' AND ExceedanceEmail = '+CAST(@bExceedanceEmail AS NVARCHAR)
		IF @bExceedanceSMS IS NOT NULL 
			SET @sWhere = @sWhere + ' AND ExceedanceSMS = '+CAST(@bExceedanceSMS AS NVARCHAR)

		
		SET @sSelect = 'SELECT   
									CASE WHEN RecipientOption = ''EOCR'' THEN ''Email on current record''
										 WHEN RecipientOption = ''POCR'' THEN ''Phone on current record''
										ELSE  ([User].FirstName + '' '' + [User].LastName) END AS UserName, TableUser.TableUserID, 
						   TableUser.TableID, TableUser.UserID, 
							TableUser.LateWarningEmail, TableUser.LateWarningSMS,  TableUser.UploadWarningEmail,TableUser.UploadWarningSMS, 
							TableUser.ExceedanceEmail, TableUser.ExceedanceSMS, 
							  [Table].TableName,[User].Email,[User].PhoneNumber,TableUser.UploadEmail, TableUser.UploadSMS,[User].FirstName ,[User].LastName 
							  ,TableUser.PhoneColumnID,TableUser.EmailColumnID,TableUser.RecipientOption
						   FROM         [TableUser] 
						   LEFT JOIN [User] ON TableUser.UserID = [User].UserID
							AND [User].IsActive = 1
						   LEFT JOIN [Table] ON [Table].TableID = TableUser.TableID 
							 AND [Table].IsActive = 1
							   ' + @sWhere
	PRINT @sSelect
	 
		EXEC (@sSelect)
		SET ROWCOUNT 0
	END TRY
	BEGIN CATCH
		DECLARE @ErrorTrack varchar(MAX) =
		 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
		 '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
		 '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
		 '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

		 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
			VALUES ('ets_TableUser_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
	END CATCH
--END
--GO


