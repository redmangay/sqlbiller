﻿	 
	   CREATE PROCEDURE [dbo].[ets_Record_Sync]

	   (
		  @TableIDToUpdate int -- = 4230
		  ,@RunDeleteCommand bit = 1 -- Pass 0 when checking and it will not delete the original records
		  ,@Debug bit = 0 /* Set to 1 when debugging */
	   )
	   AS

	   --------------------------------------------------------------------
	   /* MODIFICATION HISTORY
	   ====================
	   Red 15 Dec 2017 Created the script to sync the Live [Record] and Archive [Record]
	   For manual execution, (i.e. not in the Job), please use [dbo].[ets_Record_Sync_Run]

	   Red 2 Jan 2018 Updated after review from JB

	   JB 3 Jan 2018 Tidied it up, formatting etc. and removed 2ns TRY CATCH as unnecessary

	   Red 8 Jan 2019 Added other Columns of Record table, e.g. UniqueKey, to Archive

	   Jon 17 Dec 2019 Updated to process 50,000 records at a time

	   USAGE
	   =====
	   EXEC [dbo].[ets_Record_Sync_Run] 4230
	   EXEC [dbo].[ets_Record_Sync] 4230
	   EXEC [dbo].[ets_Record_Sync] @TableIDToUpdate=2683, @RunDeleteCommand=1, @Debug=0

	   */
	   --------------------------------------------------------------------

	   BEGIN TRY

	   DECLARE @ArchiveDatabase varchar(MAX)
	   DECLARE @Counter int= 1

	   --Lets get the Defined Archive Database...
	   SELECT @ArchiveDatabase = [ArchiveDatabase]
	   FROM [Table]
	   WHERE [TableID] = @TableIDToUpdate

	   ----------------------------------------------------------------------
	   -- UPDATE RECORDS
	   ----------------------------------------------------------------------
	   DECLARE @Queue TABLE
	   (
	   [Counter] int identity(1,1),
	   ColumnName varchar(30),
	   RealDate datetime
	   )

	   INSERT INTO @Queue (ColumnName)
	   SELECT SystemName
	   FROM [Column]    
	   WHERE [TableID] = @TableIDToUpdate -- THE CURRENT TABLE
	   AND SystemName <> 'RecordID'

	   DECLARE @sSQL varchar(MAX) = 'UPDATE Archive
	   SET '
	   WHILE EXISTS(SELECT * FROM @Queue WHERE [Counter] >= @Counter)
	   BEGIN
	   SELECT @sSQL = @sSQL +  ' Archive.' + ColumnName + ' = Live.' + ColumnName + ',  
	   '
	   FROM @Queue WHERE [Counter] = @Counter

	   SET @Counter = @Counter + 1
	   END

	   SELECT @sSQL = @sSQL +
		' Archive.DateUpdated = Live.DateUpdated,
	   Archive.Notes = Live.Notes,
		  Archive.ValidationResults = Live.ValidationResults,
		  Archive.LastUpdatedUserID =  Live.LastUpdatedUserID,
		  Archive.WarningResults =  Live.WarningResults,
		  Archive.ChangeReason = Live.ChangeReason,
		  Archive.OwnerUserID = Live.OwnerUserID,
		  Archive.ReminderSentDate = Live.ReminderSentDate,
		  Archive.DeleteReason = Live.DeleteReason,
		  Archive.BatchID = Live.BatchID,
		  Archive.UniqueKey =  Live.UniqueKey,
		  Archive.IsReadOnly = Live.IsReadOnly
	   FROM ['+ @ArchiveDatabase +'].[dbo].[Record] Archive
	   JOIN [Record] Live ON Live.RecordID = Archive.RecordID
	   WHERE Archive.TableID = '+ CAST(@TableIDToUpdate AS varchar(MAX)) + '
	   AND Archive.DateUpdated <> Live.DateUpdated'

	   PRINT @sSQL
	   IF @Debug = 0
	   EXEC (@sSQL);

	   ----------------------------------------------------------------------
	   -- ADD THE NEW ROWS
	   ----------------------------------------------------------------------
	   DELETE FROM @Queue -- initialise table

	   INSERT INTO @Queue (ColumnName)
	   SELECT SystemName
	   FROM [Column]    
	   WHERE [TableID] = @TableIDToUpdate -- THE CURRENT TABLE
	   --AND SystemName <> 'RecordID'

	   SET @sSQL = 'INSERT INTO ['+ @ArchiveDatabase +'].[dbo].[Record]
	   (
	   '
	   SET @Counter = 1

	   WHILE EXISTS(SELECT * FROM @Queue WHERE [Counter] >= @Counter)
	   BEGIN
	   SELECT @sSQL = @sSQL +  ColumnName + ',
	   '
	   FROM @Queue WHERE [Counter] = @Counter

	   SET @Counter = @Counter + 1
	   END

	   SELECT @sSQL = @sSQL +
	   'DateUpdated,
	   DateAdded,
	   Notes,
	   ValidationResults,
	   LastUpdatedUserID,
	   WarningResults,
	   ChangeReason,
	   OwnerUserID,
	   ReminderSentDate,
	   DeleteReason,
	   BatchID,
	   UniqueKey,
	   IsReadOnly
	   )
	   SELECT TOP 50000
	   '
	   SET @Counter = 1

	   WHILE EXISTS(SELECT * FROM @Queue WHERE [Counter] >= @Counter)
	   BEGIN
	   SELECT @sSQL = @sSQL +  ColumnName + ',
	   '
	   FROM @Queue WHERE [Counter] = @Counter

	   SET @Counter = @Counter + 1
	   END

	   SELECT @sSQL = @sSQL +
	   'DateUpdated,
	   DateAdded,
	   Notes,
		 ValidationResults,
		 LastUpdatedUserID,
		 WarningResults,
		 ChangeReason,
		 OwnerUserID,
		 ReminderSentDate,
		 DeleteReason,
		 BatchID,
		 UniqueKey,
		 IsReadOnly
	   FROM [Record] Live
	   WHERE TableID = '+ CAST(@TableIDToUpdate AS varchar(MAX)) +'
	   AND RecordID NOT IN (SELECT RecordID FROM ['+ @ArchiveDatabase +'].[dbo].[Record] WHERE TableID = '+ CAST(@TableIDToUpdate AS varchar(MAX)) +' )'
	   -- AND [RecordID] >
	   -- (SELECT ISNULL(MAX(RecordID),0) FROM ['+ @ArchiveDatabase +'].[dbo].[Record]
	   -- WHERE TableID = '+ CAST(@TableIDToUpdate AS varchar(MAX)) +' )' -- the last record ID of the Archive

	   PRINT @sSQL
	   IF @Debug = 0
	   EXEC (@sSQL);

	   ----------------------------------------------------------------------
	   -- DELETE EXPIRED RECORDS IN LIVE DATABASE - I.E. MORE THAN DEFINED ARCHIVE MONTHS
	   ----------------------------------------------------------------------
	   -- may be a automatic backup before running this

	   DECLARE @SQLDelete varchar(MAX), @DateFrom_RecordsDelete datetime, @ArchiveMonth int, @ArchiveDateColumnID int, @SystemName varchar(50)

	   SELECT @ArchiveMonth = [ArchiveMonths],
	   @ArchiveDateColumnID = [ArchiveDateColumnID]
	   FROM [Table]
	   WHERE TableID = @TableIDToUpdate

	   SELECT @SystemName = SystemName FROM [Column] WHERE ColumnID = @ArchiveDateColumnID AND TableID = @TableIDToUpdate

	   -- lets get the date months x ago...
	   SET @ArchiveMonth = @ArchiveMonth * -1
	   SELECT @DateFrom_RecordsDelete = dateadd(M, @ArchiveMonth , getdate())

	   IF @RunDeleteCommand=1 -- Added 5 Oct to allow us to proceed with more caution
	   BEGIN
	 
	 -- lets DELETE the records
	   SET @SQLDelete = '  
		  DELETE FROM [Record] WHERE TableID = '+ CAST(@TableIDToUpdate AS varchar(MAX)) +' AND  
		  CONVERT(DATE,' + @SystemName +', 103) < CONVERT(DATE, '''+ CAST(@DateFrom_RecordsDelete AS varchar) +''', 103)
		  AND RecordID IN (SELECT RecordID FROM ['+ @ArchiveDatabase +'].[dbo].[Record] WHERE TableID = '+ CAST(@TableIDToUpdate AS varchar(MAX)) +' )' -- ? 

	   PRINT @SQLDelete
	   IF @Debug = 0
	   EXEC (@SQLDelete);
	   END

	   -- Lets UPDATE the Last Archive Run...
	   IF @Debug = 0
	   UPDATE [Table] SET [ArchiveLastRun] = GETDATE() WHERE [TableID] = @TableIDToUpdate
	   ELSE
	   SELECT [ArchiveLastRun] FROM [Table] WHERE [TableID] = @TableIDToUpdate

	   DECLARE @RegularTaskLogID int
	   SELECT TOP 1 @RegularTaskLogID = [RegularTaskLogID]
	   FROM [RegularTaskLog] RTL
	   JOIN [dbo].[RegularTask] RT ON RT.RegularTaskID = RTL.RegularTaskID
	   WHERE [MethodName] = 'RunArchiveDatabaseRecordSync'
	   ORDER BY [RegularTaskLogID] DESC

		UPDATE [RegularTaskLog] SET [ResultMessage] = ISNULL([ResultMessage], '') + ' Table ID: ' + CAST(@TableIDToUpdate AS varchar(MAX))
	   WHERE [RegularTaskLogID] = @RegularTaskLogID

	   END TRY
	   BEGIN CATCH
	   DECLARE @ErrorTrack varchar(MAX) =
	   'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') +
		   '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') +
		   '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') +
		   '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	   INSERT INTO [ErrorLog]([Module], [ErrorMessage], [ErrorTrack], [ErrorTime], [Path])
	   VALUES ('ets_Record_Sync', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
	   END CATCH

