﻿

CREATE PROCEDURE [dbo].[ets_Menu_ByUser_AdvancedSecurity]
(
	@nRoleID INT,
	@bShowMenu bit = NULL		
)
AS

BEGIN TRY
	--KG 4/5/2017 Ticket 2562
	DECLARE @sSelect nvarchar(4000)

	SET @sSelect = 'SELECT     Distinct TableID 
			FROM         RoleTable
			WHERE     (RoleID = ' + CAST(@nRoleID AS NVARCHAR) + ') AND (RoleType<>6)'

	IF @bShowMenu IS NOT NULL
		SET @sSelect = @sSelect + ' AND ShowMenu =' + CAST(@bShowMenu AS NVARCHAR) 

	EXEC (@sSelect)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Menu_ByUser_AdvancedSecurity', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
