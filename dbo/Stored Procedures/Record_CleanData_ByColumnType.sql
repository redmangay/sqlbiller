﻿
 
CREATE PROCEDURE   [dbo].[Record_CleanData_ByColumnType]

	/*----------------------------------------------------------------
	EXEC [dbo].[Record_CleanData_ByColumnType]
	---------------------------------------------------------------*/
 
AS
 
BEGIN TRY


	DECLARE @sSQL varchar(MAX)=NULL
		SELECT @sSQL=COALESCE(@sSQL+'','','''')+'UPDATE [Record] SET '+ SystemName + '=FORMAT(CAST('+ SystemName + ' AS datetime),''dd/MM/yyyy HH:mm:ss'') WHERE TableID=' + CAST(TableID AS varchar)
	+ '  AND '+ SystemName + ' IS NOT NULL AND ISDATE('+ SystemName + ')=1; '
	FROM [Column] WHERE ColumnType='datetime' AND IsStandard=0 AND UPPER( SUBSTRING(SystemName,1,1))='V'

	DECLARE @sSQL2 varchar(MAX)=NULL
		SELECT @sSQL2=COALESCE(@sSQL2+'','','''')+ 'UPDATE [Record] SET '+ SystemName + '=FORMAT(CAST(''01/01/2000 ''+ '+ SystemName + ' AS datetime),''HH:mm:ss'') WHERE TableID=' + CAST(TableID AS varchar)
	+ '  AND '+ SystemName + ' IS NOT NULL AND ISDATE(''01/01/2000 ''+ '+ SystemName + ' )=1 ; '
	FROM [Column] WHERE ColumnType='time' AND IsStandard=0 AND UPPER( SUBSTRING(SystemName,1,1))='V'

	IF @sSQL2 IS NULL
	 SET @sSQL2=''
	IF @sSQL IS NULL
	 SET @sSQL=''

	SET @sSQL=@sSQL+@sSQL2;

	--PRINT @sSQL
	EXEC (@sSQL)


END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Record_CleanData_ByColumnType', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
