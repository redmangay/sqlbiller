﻿

CREATE PROCEDURE   [dbo].[spRefreshExportTemplateFields]
(
	@nExportTemplateID int
)
AS
/*
	Refreshes the order that fields are exported to match the order detail page
	Note that we do the fields on the left first, then the right
*/
BEGIN TRY
	DECLARE @t TABLE (DisplayOrder int identity(0,1), ID int, ExportHeaderName varchar(MAX))
	INSERT INTO @t (ID, ExportHeaderName)
		SELECT ExportTemplateItemID, C.DisplayName
			FROM dbo.ExportTemplateItem ETI 
			JOIN [Column] C ON C.ColumnID = ETI.ColumnID
			WHERE ETI.ExportTemplateID = @nExportTemplateID
			ORDER BY C.DisplayRight, C.DisplayOrder

	UPDATE ETI 
		SET ETI.ColumnIndex = T.DisplayOrder,
		ETI.ExportHeaderName = CASE WHEN T.ExportHeaderName IS NULL  
						THEN dbo.fnNextAvailableExportTemplateItemHeading(ETI.ExportHeaderName,ExportTemplateID,ExportTemplateItemID)
						 ELSE dbo.fnNextAvailableExportTemplateItemHeading(T.ExportHeaderName,ExportTemplateID,ExportTemplateItemID) END
		FROM ExportTemplateItem ETI 
		JOIN @t T ON T.ID = ETI.ExportTemplateItemID 
		WHERE ExportTemplateID = @nExportTemplateID
	
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spRefreshExportTemplateFields', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
