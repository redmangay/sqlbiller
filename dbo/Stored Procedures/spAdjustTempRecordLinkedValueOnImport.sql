﻿

CREATE PROCEDURE [dbo].[spAdjustTempRecordLinkedValueOnImport]
(
	@nBatchID int,
	@sSystemName varchar(10),
	@nTableTableID INT,
	@sParentSystemName varchar(10)
)
	/*----------------------------------------------------------------
	EXEC spAdjustTempRecordLinkedValueOnImport @nBatchID=4951,@sSystemName='V001',@nTableTableID=2929,@sParentSystemName='V004'
	---------------------------------------------------------------*/
 
AS

BEGIN TRY
	DECLARE @nTableID INT
	DECLARE @sDisplayName varchar(100)
	DECLARE @sSQL1 varchar(1000)
	DECLARE @sSQL2 varchar(1000)

	SELECT @nTableID=TableID FROM Batch WHERE BatchID=@nBatchID
	SELECT @sDisplayName=DisplayName FROM [Column] WHERE TableID=@nTableID AND SystemName=@sSystemName


	--To use e	equal instead of CHARINDEX when there is no ; in sample site
	-- SELECT TOP 1 @sParentSystemName FROM [Record] WHERE TableID=@nTableTableID AND IsActive=1 AND CHARINDEX(@sParentSystemName,';')>0
	--DECLARE @sJoinON varchar(250)='TR.'+@sSystemName+'=R.'+@sParentSystemName

	-- validation
	SELECT @sSQL1=
	'UPDATE TempRecord
		SET RejectReason=ISNULL(RejectReason,'''')+'' '' + '''+@sDisplayName+'(''+'+@sSystemName+'+'') not found.'' 
		WHERE BatchID='+CAST(@nBatchID AS varchar)+' AND '+@sSystemName+' IS NOT NULL AND RecordID NOT IN
			(SELECT TR.RecordID FROM TempRecord TR
				INNER JOIN Record R ON  CHARINDEX ('';'' + RTRIM(LTRIM(TR.'+@sSystemName+ '))' +' + '';'','';'' + RTRIM(LTRIM(R.'+@sParentSystemName+ '))' +' + '';'')>0
				WHERE R.TableID='+CAST(@nTableTableID AS varchar)+' AND R.IsActive=1   
				AND TR.BatchID='+CAST(@nBatchID AS varchar)+' AND TR.'+@sSystemName+' IS NOT NULL
				AND R.'+@sParentSystemName+' IS NOT NULL  AND LEN(R.'+@sParentSystemName+')>0)'

	PRINT @sSQL1
	EXEC (@sSQL1)

	-- convert TempRecord linked value
	SELECT @sSQL2=
	'UPDATE TR
		SET TR.'+@sSystemName+'=R.RecordID
		FROM TempRecord TR
		INNER JOIN [Record] R ON  CHARINDEX ('';'' + RTRIM(LTRIM(TR.'+@sSystemName+ '))' +' + '';'','';'' + RTRIM(LTRIM(R.'+@sParentSystemName+ '))' +' + '';'')>0
		WHERE R.TableID='+CAST(@nTableTableID AS varchar)+' AND R.IsActive=1 AND  R.'+@sParentSystemName+' IS NOT NULL AND LEN(R.'+@sParentSystemName+')>0
		AND TR.BatchID='+CAST(@nBatchID AS varchar)+' AND TR.'+@sSystemName+' IS NOT NULL'
	PRINT @sSQL2
	EXEC (@sSQL2)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spAdjustTempRecordLinkedValueOnImport', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
