﻿-- =============================================
-- Author:		Barry Matutina
-- Create date: 09/06/2016
-- Description:	Updates Job Number
-- =============================================
CREATE PROCEDURE [dbo].[Renzo_JobInformationGetLastJobNumber] 
	
	@RecordID  int,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output
AS

BEGIN
	
	DECLARE			 @LASTJOB				VARCHAR(MAX)
					,@LastJobRecordId		INT

	/*	Get the most recent record	*/
	 
	SELECT			@LastJobRecordId = MAX([Job Information].RecordID) 
		FROM		[Record] [Job Information] WITH (NOLOCK)
		WHERE		[Job Information].TableID = '2475'		/*	Job Information	*/
		AND			[Job Information].IsActive = 1

	/*	Get the last job number	*/

	SELECT			@LASTJOB =[Job Information].V001
		FROM		[Record] [Job Information] WITH (NOLOCK)
		WHERE		[Job Information].RecordID = @LastJobRecordId 
		AND			[Job Information].TableID = '2475'		/*	Job Information	*/
		AND			[Job Information].IsActive = 1

	/*	Update the last job number	*/

	UPDATE	    [Job Information] 			/*	[Job Information]	*/
	SET			V001 = @LASTJOB
	FROM		[Record] [Job Information]  
	WHERE		[Job Information].RecordID = @RecordID	
	AND			[Job Information].TableID = '2475'		/*	Job Information	*/
	AND			[Job Information].IsActive = 1	
	
	UPDATE	    [SystemOption] 			/*	[Job Information]	*/
	SET			V001 = @LASTJOB
	FROM		[Record] [SystemOption]  
	WHERE		[SystemOption].RecordID = @RecordID	
	AND			[SystemOption].TableID = '2379'		/*	Job Information	*/
	AND			[SystemOption].IsActive = 1	
	
	
	RETURN 	@@ERROR	/*	Mandatory	*/
END
