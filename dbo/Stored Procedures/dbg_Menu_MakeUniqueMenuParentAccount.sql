﻿ 

CREATE PROCEDURE [dbo].[dbg_Menu_MakeUniqueMenuParentAccount]

	/*----------------------------------------------------------------
	
SELECT AccountID,ParentMenuID, Menu, count(*) as qty 
 FROM [Menu] WHERE IsActive=1 AND Menu<>'---'
 GROUP BY AccountID,ParentMenuID, Menu HAVING count(*)> 1


EXEC [dbo].[dbg_Menu_MakeUniqueMenuParentAccount]
	---------------------------------------------------------------*/
 
AS
 
BEGIN TRY  
	DECLARE Cur_UniqueMenuParentAccountID CURSOR FOR
	SELECT AccountID,ParentMenuID, Menu, count(*) as qty 
	 FROM [Menu] WHERE IsActive=1 AND Menu<>'---'
	 GROUP BY AccountID,ParentMenuID, Menu HAVING count(*)> 1
 
	DECLARE @nAccountID INT;
	DECLARE @nParentMenuID INT;
	DECLARE @sMenu NVARCHAR(255);

	DECLARE @nQty INT;
	DECLARE @sNewMenu NVARCHAR(255);
	DECLARE @myParm INT=0 ;-- @nParentMenuID IS NULL

	OPEN Cur_UniqueMenuParentAccountID;
	FETCH NEXT FROM Cur_UniqueMenuParentAccountID INTO @nAccountID,@nParentMenuID,@sMenu,@nQty;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
		
	
			IF @nParentMenuID IS NOT NULL
				SET @myParm=1;
			ELSE
				SET @myParm=0;
	  
	  
			DECLARE Cur_MenuID CURSOR FOR
			SELECT MenuID FROM [Menu] WHERE IsActive=1 AND Menu<>'---' AND AccountID=@nAccountID AND 
			((@myParm=0 AND ParentMenuID IS NULL ) OR (@myParm=1 AND ParentMenuID=@nParentMenuID )) AND Menu=@sMenu;
			DECLARE @nMenuID INT;
		
			OPEN Cur_MenuID;
			FETCH NEXT FROM Cur_MenuID INTO @nMenuID;
			WHILE @@FETCH_STATUS = 0
			   BEGIN
			  
				  --@sMenu nvarchar(255),@nAccountID int,@nParentMenuID int=NULL,@nMenuID int=NULL
				  SELECT @sNewMenu=dbo.fnNextAvailableMenu(@sMenu,@nAccountID,@nParentMenuID,@nMenuID);
			  
				  IF @sMenu<>@sNewMenu
					  UPDATE [Menu]
					  SET Menu=@sNewMenu
					  WHERE MenuID=@nMenuID
			 			  
				  FETCH NEXT FROM Cur_MenuID INTO @nMenuID;
			   END;
			CLOSE Cur_MenuID;
			DEALLOCATE Cur_MenuID;  
	 
		  FETCH NEXT FROM Cur_UniqueMenuParentAccountID INTO @nAccountID,@nParentMenuID,@sMenu,@nQty;
	   END;
	CLOSE Cur_UniqueMenuParentAccountID;
	DEALLOCATE Cur_UniqueMenuParentAccountID;


	UPDATE Menu SET ShowOnMenu=0 WHERE ParentMenuID IS NULL AND ShowOnMenu=1 AND Menu='--None--'
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_Menu_MakeUniqueMenuParentAccount', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
