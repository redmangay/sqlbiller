﻿
 
CREATE PROCEDURE   [dbo].[Record_UniqueKey_Update]

AS
 
BEGIN TRY
   -- 'datetime','time' columns data has been Updated

	DECLARE T_Cursor CURSOR FOR
	SELECT DISTINCT X.TableID FROM
			(SELECT  T.TableID	FROM [Table] T 
				JOIN      [Column] C1 ON C1.ColumnID = T.UniqueColumnID
					WHERE C1.ColumnType in ('datetime','time')
		UNION
			SELECT  T.TableID	FROM [Table] T 
				JOIN      [Column] C1 ON C1.ColumnID = T.UniqueColumnID2
					WHERE C1.ColumnType in ('datetime','time')
			) X

	DECLARE @nTableID INT;
	OPEN T_Cursor;
	FETCH NEXT FROM T_Cursor INTO @nTableID;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
			  -- UPDATE only Not NULL UniqueKey, no need to add UniqueKey - this can lead this loop timeout
			  IF EXISTS(SELECT TOP 1 RecordID FROM [Record] WHERE TableID = @nTableID AND UniqueKey IS NOT NULL)
				begin
					EXEC [Record_Set_UniqueKey] @sUpdateRecord='R', @nTableID=@nTableID
					PRINT ' UniqueKey has been Updated for TableID=' + CAST(@nTableID AS varchar(10))
				end
					
	   		  
			  FETCH NEXT FROM T_Cursor INTO @nTableID;
	   END;
	CLOSE T_Cursor;
	DEALLOCATE T_Cursor;
 END TRY
 BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Record_UniqueKey_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
