﻿

CREATE PROCEDURE [dbo].[dbg_View_ColumnIndexReset]
(
        @ViewID int	
)
AS

BEGIN TRY
	DECLARE @tViewItem TABLE
		(
		ID INT IDENTITY(1,1),
		ViewItemID INT
		)
	INSERT INTO @tViewItem (ViewItemID)
		SELECT ViewItemID FROM ViewItem WHERE ViewID=@ViewID ORDER BY ColumnIndex
		DECLARE @counter int
	SET @counter=1
	WHILE EXISTS(SELECT * FROM @tViewItem WHERE ID>@counter)
		BEGIN
			UPDATE ViewItem SET ColumnIndex=@counter
				WHERE ViewItemID IN (SELECT ViewItemID FROM @tViewItem WHERE ID=@counter)
					SET @counter=@counter+1
		END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_View_ColumnIndexReset', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
