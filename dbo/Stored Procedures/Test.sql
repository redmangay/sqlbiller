﻿

CREATE PROCEDURE [dbo].[Test]
(
	@nSampleTypeID int,
	@nEnteredBy int = NULL,
	@bIsActive bit = NULL,
	@bHasWarningResult bit = NULL,
	@sSampleSites varchar(MAX) = NULL,
	@dDateFrom datetime = NULL,
	@dDateTo datetime = NULL,
	@sFileName nvarchar(1000)=NULL 
)

/*
EXEC [ets_Sample_List_BulkExport] @nSampleTypeID=134,@bHasValidationResult=0,@sFileName='E:\DBG\SQLExport\1212BSFLog.csv'
*/
AS
BEGIN TRY
	SET NOCOUNT ON;

	DECLARE @tDisplayColumns TABLE
	(
		ID int identity(1,1),
		SampleColumnID int,
		SystemName varchar(50),
		DisplayText varchar(50)
	)

	INSERT INTO @tDisplayColumns (SampleColumnID, DisplayText, SystemName)
		SELECT SampleColumnID, NameOnExport, SystemName
			FROM SampleColumn 
			WHERE SampleTypeID = @nSampleTypeID 
			AND NameOnExport IS NOT NULL AND LEN(NameOnExport) > 0
		ORDER BY DisplayOrder


	DECLARE @sSQL varchar(MAX)
	DECLARE @sSelectCount varchar(MAX)
	DECLARE @counter int
	SET @counter = 1
		
	DECLARE @sColumns varchar(MAX)='SELECT  '
  
	IF @bHasWarningResult is not NULL
      IF @bHasWarningResult =1
        SET @sColumns= 'SELECT ''""Warning""'',' 
        
 
	DECLARE @DBName varchar(50)
	SELECT @DBName=DB_NAME() 

  -- Get the sOrder IF it is NULL
	WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID >= @counter)
	BEGIN
		SELECT @sColumns= @sColumns + '''""' + DisplayText  + '""''' + ',' FROM @tDisplayColumns WHERE ID = @counter
		--SET @sColumns=@sColumns + @sTempColumn	
		 SET @counter = @counter + 1
	END 
	
	
	
	SELECT @sColumns = LEFT(@sColumns, LEN(@sColumns)-1) 
	SET @sColumns= @sColumns + ' UNION ALL '
	
	PRINT @sColumns
 -- SELECT @sSQL = 'SELECT * FROM (SELECT SampleInfo.*,ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ' ) as RowNum FROM (SELECT '
	SELECT @sSQL = 'SELECT  * FROM ( SELECT '

     IF @bHasWarningResult is not NULL
      IF @bHasWarningResult =1
      SELECT @sSQL = @sSQL + ' ''""'' + convert(varchar(MAX),'+@DBName+'.dbo.Sample.WarningResults) + ''""'' as [Warning], ' 

    SET @counter = 1
	WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID >= @counter)
	BEGIN
		SELECT @sSQL = @sSQL + ' ''""'' + convert(varchar(MAX),'+@DBName+'.dbo.Sample.' + SystemName + ') + ''""''  AS [' + DisplayText + '], ' 
			FROM @tDisplayColumns 
			WHERE ID = @counter
		--PRINT @sSQL
		SET @counter = @counter + 1	
	END 

	SELECT @sSQL = LEFT(@sSQL, LEN(@sSQL)-1) 
	
	SELECT @sSQL = @sSQL + ' FROM '+@DBName+'.dbo.[Sample]'

	--IF PATINDEX('%Sample.SampleSiteID%', @sSQL) > 0
	BEGIN
		SET @sSQL = REPLACE(@sSQL, @DBName+'.dbo.Sample.SampleSiteID', @DBName+'.dbo.SampleSite.SampleSiteName')
		SET @sSQL = REPLACE(@sSQL, 'FROM '+@DBName+'.dbo.[Sample]', 'FROM '+@DBName+'.dbo.[Sample] JOIN '+@DBName+'.dbo.SampleSite ON '+@DBName+'.dbo.Sample.SampleSiteID = '+@DBName+'.dbo.SampleSite.SampleSiteID')
	END	

	IF PATINDEX('%Sample.SampleTypeID%', @sSQL) > 0
	BEGIN
		SET @sSQL = REPLACE(@sSQL, @DBName+'.dbo.Sample.SampleTypeID', @DBName+'.dbo.SampleType.SampleTypeName')
		SET @sSQL = REPLACE(@sSQL, 'FROM '+@DBName+'.dbo.[Sample]', 'FROM '+@DBName+'.dbo.[Sample] JOIN '+@DBName+'.dbo.SampleType ON '+@DBName+'.dbo.Sample.SampleTypeID = '+@DBName+'.dbo.SampleType.SampleTypeID')
	END	

	IF PATINDEX('%Sample.EnteredBy%', @sSQL) > 0
	BEGIN
		SET @sSQL = REPLACE(@sSQL, @DBName+'.dbo.Sample.EnteredBy', @DBName+'.dbo.[User].FirstName')
		SET @sSQL = REPLACE(@sSQL, 'FROM '+@DBName+'.dbo.[Sample]', 'FROM '+@DBName+'.dbo.[Sample] JOIN '+@DBName+'.dbo.[User] ON '+@DBName+'.dbo.Sample.EnteredBy = '+@DBName+'.dbo.[User].UserID')
	END	

	-- ADD IN THE WHERE CRITERIA
	SELECT @sSQL = @sSQL + ' WHERE '+@DBName+'.dbo.Sample.SampleTypeID = ' + CAST(@nSampleTypeID as varchar)
	SELECT @sSQL = @sSQL + ' AND '+@DBName+'.dbo.SampleSite.IsActive=1  ' 

	IF @sSampleSites IS NOT NULL AND LEN(@sSampleSites) > 0
		SELECT @sSQL = @sSQL + ' AND '+@DBName+'.dbo.Sample.SampleSiteID IN (' + @sSampleSites + ')' 

	IF @dDateFrom IS NOT NULL 
		SELECT @sSQL = @sSQL + ' AND '+@DBName+'.dbo.Sample.DateTimeSampled >= ''' + CONVERT(varchar(30), @dDateFrom, 120) + ''''

	IF @dDateTo IS NOT NULL 
		SELECT @sSQL = @sSQL + ' AND '+@DBName+'.dbo.Sample.DateTimeSampled <= ''' + CONVERT(varchar(30), @dDateTo, 120) + ''''

	IF @nEnteredBy IS NOT NULL 
		SELECT @sSQL = @sSQL + ' AND '+@DBName+'.dbo.Sample.EnteredBy = ' + CAST(@nEnteredBy AS varchar)

	 IF @bHasWarningResult IS NOT NULL
     BEGIN
		 IF @bHasWarningResult =0 
		 SELECT @sSQL = @sSQL + ' AND '+@DBName+'.dbo.Sample.WarningResults is  NULL ' 
	     
		 IF @bHasWarningResult =1 
		 SELECT @sSQL = @sSQL + ' AND '+@DBName+'.dbo.Sample.WarningResults is not NULL ' 
     END
	
	
	IF @bIsActive IS NOT NULL 
		SELECT @sSQL = @sSQL + ' AND '+@DBName+'.dbo.Sample.IsActive = ' + CAST(@bIsActive AS varchar)
    
    --SELECT @sSelectCount= @sSQL + ') as SampleInfo) as SampleFinalInfo'
	--SELECT @sSQL= @sSQL + ') as SampleInfo) as SampleFinalInfo WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	--SELECT @sSQL= @sSQL + ' ORDER BY ' + @sOrder  + ') AS SampleInfo'
	SELECT @sSQL= @sSQL + ' ) AS SampleInfo'
    --SET NOCOUNT OFF;
    
    --SET @sSelectCount=REPLACE(@sSelectCount, 'SELECT * FROM', 'SELECT COUNT(*) AS TotalRows FROM')
    
    
    SET ROWCOUNT 2147483647
	PRINT @sSQL

	--SET @sSQL=@sColumns + @sSQL
	--DECLARE @cmd varchar(2048) 
	--SET @cmd='bcp "'+ @sSQL +'" queryout ' + @sFileName + ' -c -t"," -S' + @@ServerName + ' -T'
	--PRINT @cmd
	-- EXEC master.dbo.xp_cmdshell  @cmd

	
	SET ROWCOUNT 0
 

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Test', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
