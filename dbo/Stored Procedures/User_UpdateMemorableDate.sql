﻿

CREATE Procedure [dbo].[User_UpdateMemorableDate]
@dMemDate nvarchar(200),
@nUserID int

AS
BEGIN TRY
	DECLARE @Salt VARBINARY(8) = CRYPT_GEN_RANDOM(8)

		UPDATE dbo.[User]
		SET 	[MemorableDate] = HASHBYTES('SHA2_512', @dMemDate + cast(@Salt as nvarchar)),
				[SaltMemDate] = @Salt
		WHERE [UserID] = @nUserID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('User_UpdateMemorableDate', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
