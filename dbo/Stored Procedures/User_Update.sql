﻿


CREATE PROCEDURE [dbo].[User_Update]
(
	@nUserID int,
	@sFirstName nvarchar(50)  = NULL,
	@sLastName nvarchar(50)  = NULL,
	@sPhoneNumber varchar(20)  = NULL,
	@sEmail nvarchar(200),
	@bIsActive bit,
	@SecondFactorAuthentication nvarchar(200) = NULL,
	@sProfilePicture varchar(MAX)=NULL,
	@sAttributes varchar(max) =null,
	@IsEmailConfirmed bit=null
)
AS
BEGIN TRY

	UPDATE [User]
	SET 
		FirstName = @sFirstName,
		LastName = @sLastName,
		PhoneNumber = @sPhoneNumber,
		Email = @sEmail,
		IsActive = @bIsActive,
		SecondFactorAuthentication = @SecondFactorAuthentication,
		ProfilePicture=@sProfilePicture,
		Attributes=@sAttributes,
		IsEmailConfirmed=@IsEmailConfirmed

	WHERE UserID = @nUserID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('User_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
