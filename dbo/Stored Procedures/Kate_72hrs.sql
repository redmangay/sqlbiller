﻿CREATE PROCEDURE [dbo].[Kate_72hrs]
(
	@RecordID int,
	@UserID int,
	@Return varchar(max) output
)
AS
-- 
-- Copy from the Notification table
BEGIN

	UPDATE [Record] 
		SET V086= N.V015						-- * ED1 Arrival Time (V015 is correct for Notifications)
		FROM [Record]
		JOIN [Record] N ON N.TableID = 2454		-- Join to Notification Form (2454 is correct for Notifications)
			AND N.RecordID = [Record].V020		-- * on Trauma Record Number
		WHERE [Record].RecordID = @RecordID
END
