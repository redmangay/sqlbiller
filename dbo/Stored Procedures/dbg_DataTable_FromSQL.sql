﻿

CREATE PROCEDURE [dbo].[dbg_DataTable_FromSQL]
(
	@sSQL varchar(MAX)
)
AS
 


/*
EXEC [dbo].[dbg_DataTable_FromSQL] 'SELECT * FROM [User]'
*/
BEGIN TRY
	SET NOCOUNT ON;
	SET DATEFORMAT dmy;

	-- Following line added by JB on 19 July 2016
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- PRINT @sSQL
	EXEC (@sSQL);
	
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') +
	'. SQL:'        + ISNULL(@sSQL, '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_DataTable_FromSQL', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
