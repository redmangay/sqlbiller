﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE usr_srptInvoicePayment 
AS
BEGIN
SELECT 
[Account24918].[vrptInvoiceAndPayment].GroupDivision, 
Sum(CAST(([Account24918].[vrptInvoiceAndPayment].[Invoice]) AS DECIMAL(14, 2))) AS sInvoice, 
Sum(CAST(([Account24918].[vrptInvoiceAndPayment].[InvoicedTax]) AS DECIMAL(14, 2))) AS sInvoiceTax ,
Sum(CAST(([Account24918].[vrptInvoiceAndPayment].[Invoice]) AS DECIMAL(14, 2))-CAST(([Account24918].[vrptInvoiceAndPayment].[InvoicedTax]) AS DECIMAL(14, 2))) AS sInvoiceNett ,
Sum(CAST(([Account24918].[vrptInvoiceAndPayment].[Credit]) AS DECIMAL(14, 2))) AS sCredit, 
Sum(CAST(([Account24918].[vrptInvoiceAndPayment].[CreditTax]) AS DECIMAL(14, 2))) AS sCreditTax ,
Sum(CAST(([Account24918].[vrptInvoiceAndPayment].[Credit]) AS DECIMAL(14, 2))-CAST(([Account24918].[vrptInvoiceAndPayment].[CreditTax]) AS DECIMAL(14, 2))) AS sCreditNett ,
Sum(CAST(([Account24918].[vrptInvoiceAndPayment].[BadDebt])AS DECIMAL(14, 2))) AS sBadDebt, 
Sum(CAST(([Account24918].[vrptInvoiceAndPayment].[BadDebtTax])AS DECIMAL(14, 2))) AS sBadDebtTax, 
Sum(CAST(([Account24918].[vrptInvoiceAndPayment].[BadDebt]) AS DECIMAL(14, 2))-CAST(([Account24918].[vrptInvoiceAndPayment].[BadDebtTax]) AS DECIMAL(14, 2))) AS sBadDebtNett ,
Sum(CAST(([Account24918].[vrptInvoiceAndPayment].[Payment])AS DECIMAL(14, 2))) AS sPayment 
--CASE WHEN ([Account24918].[vList_Of_Group_Division].[ReportingCurrency]!=0) THEN NULL ELSE [ReportingCurrency] 
--END AS GroupCurrency
 FROM [Account24918].[vrptInvoiceAndPayment] 
LEFT JOIN [Account24918].[vList_Of_Group_Division] ON
 [Account24918].[vrptInvoiceAndPayment].GroupDivision = 
 [Account24918].[vList_Of_Group_Division].[Group/Division]
--WHERE (((rptInvoiceAndPayment.GUID)=getInvPayGUID())) 
GROUP BY [Account24918].[vrptInvoiceAndPayment].GroupDivision
--IIf(IsNull([ReportingCurrency])," ",[ReportingCurrency]) 
ORDER BY [Account24918].[vrptInvoiceAndPayment].GroupDivision
END
