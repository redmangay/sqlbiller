﻿

CREATE PROCEDURE [dbo].[SystemOption_Select]
(
	@sOptionKey nvarchar(200)  = NULL,
	@nAccountID int = NULL,
	@nTableID int = NULL,
	@sOptionValue nvarchar(1000)  = NULL,
	@sOptionNotes ntext  = NULL,
	@dDateAdded datetime  = NULL,
	@dDateUpdated datetime  = NULL,	
	@sOrder nvarchar(200) = OptionKey, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	EXEC [SystemOption_Select] @nAccountID=24800
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'


	IF @sOptionKey IS NOT NULL 
		SET @sWhere = @sWhere + ' AND OptionKey LIKE '+'''%' + @sOptionKey + '%'''
	IF @sOptionValue IS NOT NULL 
		SET @sWhere = @sWhere + ' AND OptionValue LIKE '+'''%' + @sOptionValue + '%'''
	IF @dDateAdded IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DateAdded = '+CAST(@dDateAdded AS NVARCHAR)
	IF @dDateUpdated IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DateUpdated = '+CAST(@dDateUpdated AS NVARCHAR)
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND	AccountID = '+CAST(@nAccountID AS NVARCHAR)
	IF @nTableID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND TableID = '+CAST(@nTableID AS NVARCHAR)

	SET @sSelect = 'SELECT  * FROM 
	(SELECT S.*,A.AccountName,T.TableName, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum 
	FROM [SystemOption] S LEFT JOIN [Account] A ON S.AccountID=A.AccountID
	LEFT JOIN [Table] T ON S.TableID=T.TableID) as UserInfo'
	+ @sWhere + ' AND RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)

	SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [SystemOption] ' + @sWhere 
 
	-- Extend WHERE to include paging:
	--SET @sWhere = @sWhere + ' AND RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	--SET @sOrder = ' ORDER BY ' + @sOrder
	--SET @sSelect = @sSelect + @sWhere + @sOrder

	PRINT (@sSelect) 
	EXEC (@sSelect)
	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('SystemOption_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
