﻿

CREATE PROCEDURE [dbo].[Audit_Insert]
(
   @sTableName varchar(128),
   @nTableID int = NULL,
   @sPrimaryKeyValue varchar(1000),
   @sFieldName varchar(128),
   @sOldValue varchar(MAX) = NULL,
   @sNewValue varchar(MAX) =NULL,
   @dDateAdded datetime,
   @sUserID int,
   @nAccountID int,
   @sChangeReason varchar(MAX) = NULL
)
AS

BEGIN TRY
	INSERT INTO [dbo].[Audit]
           ([TableName]
           ,[TableID]
           ,[PrimaryKeyValue]
           ,[FieldName]
           ,[OldValue]
           ,[NewValue]
           ,[DateAdded]
           ,[UserID]
           ,[AccountID]
           ,[ChangeReason])
     VALUES
	 (
		   @sTableName,
		   @nTableID,
		   @sPrimaryKeyValue,
		   @sFieldName,
		   @sOldValue,
		   @sNewValue,
		   @dDateAdded,
		   @sUserID,
		   @nAccountID,
		   @sChangeReason	 
	 )
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Audit_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
