﻿
--------------------------------------------------------------------------------------
-- PLEASE DO NOT CHANGE THE SCRIPT ABOVE - Simply set @Sequence & @Description
-- and paste your script in BELOW:
--------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- ets_Record_List
-------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[ets_Record_List]
(
	@nTableID int,
	@nEnteredBy int = null,
	@bIsActive bit = null,
	@bHasWarningResults bit = null,
	@dDateFrom datetime = NULL,
	@dDateTo datetime = NULL,
	@sOrder nvarchar(200)=null , 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647,
	@sType varchar(25) = null,
	@sNumericSearch varchar(MAX) = null,
	@sTextSearch varchar(MAX) = null,
	@dDateAddedFrom datetime = NULL,
	@dDateAddedTo datetime = NULL,
	@sParentColumnSortSQL varchar(MAX) = null,
	@sHeaderSQL varchar(MAX) = null,
	@sViewName varchar(50) = null,
	@nViewID INT =NULL,
	@sColumnIDs varchar(MAX) = null, /* Added 5 Dec 16 to allow us to choose columns to select */
	@sReturnSQL VARCHAR(MAX)=null OUTPUT,
	@sReturnHeaderSQL VARCHAR(MAX)=null OUTPUT,
	@bReturnIsActive bit = 0,  /* Added 12 Dec 17 JB : Set to 1 if you need to return the IsActive column */
	@bUseArchiveDatabase bit = null /* Added 04 Jan 18 Red : basis for using Archive Database */
)

/*
REVISION HISTORY
----------------
01 Dec 2015: JB Added view filter 
19 Jul 2016: JB Added SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; and removed NOLOCK hint
13 Sep 2016: MR Added some think like @sType='SQLOnly' OR @sType='view' so that we can get SQL Output when @sType='view'
17 Oct 2016: MR Updated for the removal of NameOnImport,NameOnExport etc
02 Nov 2016: MR used only date part in DateAdded.
05 Dec 2016: JB added @sColumnIDs so we can pass in which columns to return
13 Dec 2016: JA Added DeletedBy Column.
03 Feb 2017: Jarrod made updates for the Export Bulk Data  
30 Mar 2017: Alex commented out a few lines related to ColourCells as that is now being done in the C# code
30 Mar 2017: JB Instead of using [RemoveSpecialChars] we are replacing square brackets with round brackets
31 Mar 2017: JB added a fix for square brackets in the sort order
04 Apr 2017: JB previous 2 changes backed out as they caused problems elsewhere
16 May 2017: Red added a script to process the Grand Total for RecordList page (replace for Page Total)
04 Jul 2017: Using dbgNum instead of RemoveSpecialChars_IngnoreSymbols
10 Jul 2017: Kath replaced DisplayText varchar(50) with DisplayText varchar(100)
26 Jul 2017: Kath added script to split datetime to separate date and time when exporting large data
05 Sep 2017: JB addee TRY/CATCH
26 Sep 2017: Alex added "report" type
04 Oct 2017: JB changed the system names to have an underscore e.g. _EnteredBy. Problem was client has same field names
09 Nov 2017:MR Show _Deleted,_DeleteReason,_EnteredBy and _DeletedBy
17 Nov 2017: Red updated export _EnteredBy field in the @SELECT and @FROM; this should not show in the export output unless config in C# Export Template
12 Dec 2017: JB added a new parameter @bReturnIsActive on behalf of Lan to bring back the IsActive column
04 Jan 2018: Red added scripts for Archive Database... all added scripts are related to @ArchiveDatabase and @bUseArchiveDatabase 
16 Feb 2018: JB improved the error message info to show the SQL run, the TableID, ViewID etc.
29 May 2019: JB changed DisplayText[1]', 'varchar(50)' --> DisplayText[1]', 'varchar(100)' to match the length of Column.DislayName
18 Jun 2019: JB changed the [Module] on the ErrorLog because it was missing sometimes
22 Aug 2019: MR Added DBGDateUpdated & DBGDateAdded
03 Nov 2019: Added REPLACE(  ETI.ExportHeaderName,' ','_')
25 Nov 2019: JB changed the logic of using @sColumnIDs

TEST CASES
----------
exec [ets_Record_List] @nTableID=1847,@sTextSearch='Record.V001 like ''%AB%'''
exec [ets_Record_List] @nTableID=1847,@sTextSearch='Record.V001 like ''%AB%'''
exec [ets_Record_List] @nTableID=5319, @sType='SQLOnly'
*/
AS
BEGIN TRY
	SET DATEFORMAT dmy;
	-- JB added this 19 July 2016
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SET NOCOUNT ON;
	DECLARE @tDisplayColumns TABLE
	(
		ID int identity(1,1),
		ColumnID int,
		SystemName varchar(50),
		DisplayText varchar(100),
		FieldsToShow varchar(MAX),
		ParentTableID int,
		ParentJoinColumnName varchar(254), -- RecordID
		ChildJoinColumnName varchar(254), -- V001 / V499
		ShowViewLink varchar(10),
		ColumnType varchar(20),
		ColourCells bit
	)

	DECLARE @nColumnIDRecordID INT


	SET @sReturnSQL=''
	SET @sReturnHeaderSQL=''
	SELECT @nColumnIDRecordID=ColumnID FROM [Column]
		WHERE TableID = @nTableID 
		AND SystemName='RecordID'

	DECLARE @sColumns varchar(MAX)='SELECT  '
	IF @sType is not null 
	BEGIN
		IF @sType LIKE 'export%' OR @sType='SQLOnly' OR @sType='report' -- From 30/10/2015 we will have export and exportbulk
		BEGIN
			IF @sHeaderSQL IS NULL
			BEGIN
				IF @sType LIKE 'export%' -- then we are going to use the ExportTemplate
				BEGIN
					/* Original code */
					DECLARE @nExportTemplateID INT
					SELECT TOP 1 @nExportTemplateID=ExportTemplateID FROM ExportTemplate WHERE TableID=@nTableID ORDER BY ExportTemplateID DESC
					INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName, FieldsToShow, ParentTableID, ParentJoinColumnName,ChildJoinColumnName,ColumnType)
							SELECT C.ColumnID,REPLACE(  ETI.ExportHeaderName,' ','_'), C.SystemName,
								dbo.fnReplaceDisplayColumns(C.DisplayColumn, C.[TableTableID],C.ColumnID) as [FieldsToShow],
								C.[TableTableID] AS  [ParentTableID], 
								PC.SystemName as [ParentJoinColumnName], 
								C.SystemName as [ChildJoinColumnName],
								C.ColumnType 
								FROM [Column] C
									LEFT OUTER JOIN [Column] PC ON PC.[ColumnID] = C.[LinkedParentColumnID]
									JOIN ExportTemplateItem ETI ON ETI.ColumnID=C.ColumnID
									WHERE C.TableID = @nTableID  AND ETI.ExportTemplateID=@nExportTemplateID							
									ORDER BY ETI.ColumnIndex
				END
				ELSE -- NOT using ExportTemplate
				BEGIN

					/* We are returning a particular set of columns */
					INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName, FieldsToShow, ParentTableID, ParentJoinColumnName,ChildJoinColumnName,ColumnType)
							SELECT C.ColumnID, C.DisplayName, C.SystemName,
								dbo.fnReplaceDisplayColumns(C.DisplayColumn, C.[TableTableID],C.ColumnID) as [FieldsToShow],
								C.[TableTableID] AS  [ParentTableID], 
								PC.SystemName as [ParentJoinColumnName], 
								C.SystemName as [ChildJoinColumnName],
								C.ColumnType 
								FROM [Column] C
									LEFT OUTER JOIN [Column] PC ON PC.[ColumnID] = C.[LinkedParentColumnID]
									WHERE C.TableID = @nTableID  
									AND (	@sColumnIDs IS NULL /* Not using ColumnIDs or if we are: */
											OR (C.ColumnID IN (SELECT Int_Value FROM DBO.fn_ParseText2Table(@sColumnIDs,',')))
										)
									ORDER BY C.DisplayOrder 
				END
			END -- @sHeaderSQL IS NULL

			IF @sHeaderSQL IS NOT NULL
			BEGIN
				DECLARE @exportXML XML
				SET @exportXML=@sHeaderSQL
				DECLARE @tExportTable TABLE
				(
					xmlColumn XML
				)
				 
				INSERT INTO @tExportTable SELECT @exportXML

				INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName, FieldsToShow,
					ParentTableID, ParentJoinColumnName,ChildJoinColumnName, ShowViewLink, ColumnType)
					SELECT	N.C.value('ColumnID[1]', 'int') ColumnID,
							N.C.value('DisplayText[1]', 'varchar(100)') DisplayText,
							N.C.value('SystemName[1]', 'varchar(50)') SystemName,
							N.C.value('FieldsToShow[1]', 'varchar(max)') FieldsToShow,
							N.C.value('ParentTableID[1]', 'int') ParentTableID,
							N.C.value('ParentJoinColumnName[1]', 'varchar(254)') ParentJoinColumnName,
							N.C.value('ChildJoinColumnName[1]', 'varchar(254)') ChildJoinColumnName,
							N.C.value('ShowViewLink[1]', 'varchar(10)') ShowViewLink,
							N.C.value('ColumnType[1]', 'varchar(20)') ColumnType
					FROM @tExportTable
					CROSS APPLY xmlColumn.nodes('//Records') N(C)

			END -- @sHeaderSQL IS NOT NULL
			UPDATE	@tDisplayColumns SET FieldsToShow=NULL,ParentTableID=NULL,ParentJoinColumnName=NULL,ChildJoinColumnName=NULL,
					ShowViewLink=NULL,ColumnType=NULL WHERE ParentTableID=0
			
			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
				VALUES (@nColumnIDRecordID,'DBGSystemRecordID','RecordID')
		END -- @sType='export'

		IF @sType='view'  -- OR @sType='SQLOnly' for testing only
		BEGIN
			--print 'Debug View'

			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName, FieldsToShow,
					ParentTableID, ParentJoinColumnName,ChildJoinColumnName, ShowViewLink, ColumnType,ColourCells)
					SELECT VI.ColumnID, ISNULL(C.DisplayTextSummary,C.DisplayName), C.SystemName,
						dbo.fnReplaceDisplayColumns(C.DisplayColumn, C.[TableTableID],C.ColumnID) as [FieldsToShow],
						C.[TableTableID] AS  [ParentTableID], 
						PC.SystemName as [ParentJoinColumnName], 
						C.SystemName as [ChildJoinColumnName],
						C.ShowViewLink,
						C.ColumnType,C.ColourCells
						FROM [ViewItem] VI
							INNER JOIN  [Column] C ON VI.ColumnID=C.ColumnID
							LEFT OUTER JOIN   [Column] PC ON     PC.[ColumnID] = C.[LinkedParentColumnID]
							WHERE ViewID = @nViewID			
							ORDER BY ColumnIndex
				
			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
				VALUES (@nColumnIDRecordID,'DBGSystemRecordID','RecordID')
		END -- @sType='view'
				
		IF @sType='graph' 
		BEGIN
			--print 'Debug Graph'

			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
				SELECT ColumnID, GraphLabel, SystemName
					FROM [Column]
					WHERE TableID = @nTableID 
					AND GraphLabel IS NOT NULL AND LEN(GraphLabel) > 0
					ORDER BY DisplayOrder
						INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
						VALUES (@nColumnIDRecordID,'DBGSystemRecordID','RecordID')
		END -- @sType='graph' 

		--IF @sType='mobile' 
		--BEGIN
		--	INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName, FieldsToShow, ParentTableID, ParentJoinColumnName,ChildJoinColumnName, ShowViewLink, ColumnType)
		--			SELECT C.ColumnID, C.MobileName, C.SystemName,
		--				dbo.fnReplaceDisplayColumns(C.DisplayColumn, C.[TableTableID],C.ColumnID) as [FieldsToShow],
		--				C.[TableTableID] AS  [ParentTableID], 
		--				PC.SystemName as [ParentJoinColumnName], 
		--				C.SystemName as [ChildJoinColumnName],
		--				C.ShowViewLink,
		--				C.ColumnType
		--				FROM [Column] C
		--				LEFT OUTER JOIN [Column] PC ON PC.[ColumnID] = C.[LinkedParentColumnID]
		--				WHERE C.TableID = @nTableID 
		--				AND C.MobileName IS NOT NULL AND LEN(C.MobileName) > 0
		--				ORDER BY C.DisplayOrder


		--	INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
		--		VALUES (@nColumnIDRecordID,'DBGSystemRecordID','RecordID')
		--END -- @sType='mobile' 

		IF @sType='nonstandard' 
		BEGIN
			--print 'Debug Nonst'

			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName, FieldsToShow, ParentTableID, ParentJoinColumnName,ChildJoinColumnName, ShowViewLink, ColumnType)
					SELECT C.ColumnID, C.DisplayName, C.SystemName,
						dbo.fnReplaceDisplayColumns(C.DisplayColumn, C.[TableTableID],C.ColumnID) as [FieldsToShow],
						C.[TableTableID] AS  [ParentTableID], 
						PC.SystemName as [ParentJoinColumnName], 
						C.SystemName as [ChildJoinColumnName],
						C.ShowViewLink,
						C.ColumnType
						FROM [Column] C
						LEFT OUTER JOIN [Column] PC ON PC.[ColumnID] = C.[LinkedParentColumnID]
						WHERE C.TableID = @nTableID 
						AND C.IsStandard=0
						ORDER BY C.DisplayOrder

			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
				VALUES (@nColumnIDRecordID,'DBGSystemRecordID','RecordID')
				
		END -- @sType='nonstandard' 
		
		IF @sType='allcolumns' 
		BEGIN
			--print 'Debug Nonst'
			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName, FieldsToShow, ParentTableID, ParentJoinColumnName,ChildJoinColumnName, ShowViewLink, ColumnType)
					SELECT C.ColumnID, C.DisplayName, C.SystemName,
						dbo.fnReplaceDisplayColumns(C.DisplayColumn, C.[TableTableID],C.ColumnID) as [FieldsToShow],
						C.[TableTableID] AS  [ParentTableID], 
						PC.SystemName as [ParentJoinColumnName], 
						C.SystemName as [ChildJoinColumnName],
						C.ShowViewLink,
						C.ColumnType
						FROM [Column] C
						LEFT OUTER JOIN [Column] PC ON PC.[ColumnID] = C.[LinkedParentColumnID]
						WHERE C.TableID = @nTableID 
						ORDER BY C.DisplayOrder

			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
				VALUES (@nColumnIDRecordID,'DBGSystemRecordID','RecordID')

			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
				VALUES (@nColumnIDRecordID,'DBGDateUpdated','DateUpdated')

			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
				VALUES (@nColumnIDRecordID,'DBGDateAdded','DateAdded')
				
		END --  @sType='allcolumns'
		
		IF @sType='backup'  or @sType='displayname' 
		BEGIN
			--print 'Debug Backup'

			IF @sType='backup'
			BEGIN
				INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
					SELECT ColumnID, DisplayName, SystemName
						FROM [Column] 
						WHERE TableID = @nTableID 
						AND DisplayName IS NOT NULL AND LEN(DisplayName) > 0
					ORDER BY DisplayOrder
			END
			IF @sType='displayname' 
			BEGIN
				INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
					SELECT ColumnID, SystemName, SystemName
						FROM [Column] 
						WHERE TableID = @nTableID 
					ORDER BY DisplayOrder

			END
			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
				VALUES (@nColumnIDRecordID,'DBGSystemRecordID','RecordID')
			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
				VALUES (@nColumnIDRecordID,'DateAdded','DateAdded')
			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
				VALUES (@nColumnIDRecordID,'DateUpdated','DateUpdated')
			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
				VALUES (@nColumnIDRecordID,'RecordUserID','EnteredBy')
		END -- @sType='displayname'
		
		
	END -- @sType is not null 
	ELSE
	BEGIN -- @sType is null 
		IF @sViewName IS NULL
		BEGIN
			--print 'Debug ViewName'

			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
				SELECT ColumnID, DisplayTextSummary, SystemName
					FROM [Column]
					WHERE TableID = @nTableID 
					AND DisplayTextSummary IS NOT NULL AND LEN(DisplayTextSummary) > 0
					ORDER BY DisplayOrder
		END
		ELSE -- @sViewName IS not NULL
		BEGIN
			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
				SELECT ColumnID, DisplayTextSummary, SystemName
					FROM [Column]
					WHERE TableID = @nTableID 
					AND DisplayTextSummary IS NOT NULL AND LEN(DisplayTextSummary) > 0
					AND ViewName=@sViewName
					ORDER BY DisplayOrder
		END
		INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
			VALUES (@nColumnIDRecordID,'DBGSystemRecordID','RecordID')

	END -- end of @sType is  null 


	IF (@bReturnIsActive=1) /* They need the IsActive column returned */
		INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
			VALUES (@nColumnIDRecordID,'DBGSystemIsActive','IsActive')
	-- Debug
	--SELECT * FROM @tDisplayColumns

	----------------------------------------------
	-- Part II - the build
	----------------------------------------------
	DECLARE @SELECT varchar(MAX)
	DECLARE @FROM varchar(MAX)
	DECLARE @WHERE varchar(MAX)
	DECLARE @SQLMain varchar(MAX)
	
	DECLARE @counter int
	SET @counter = 1
		
  -- Set the sOrder if it is null (JB: not sure why we are looping?)
   WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID >= @counter)
	BEGIN
		IF @sOrder is null
			SELECT @sOrder= ' [' + DisplayText  + '] ASC' FROM @tDisplayColumns WHERE ID = @counter
		SET @counter = @counter + 1
	END 
	
	SELECT @SELECT = ''
	SELECT @FROM = ' FROM [Record] '

 	
	IF @bIsActive IS NOT NULL AND @bIsActive =0
		SELECT @SELECT = @SELECT + ' Record.DateUpdated as _Deleted, ' ,@sColumns=@sColumns + '''""' + '_Deleted'+ '""''' + ', '

	IF @bIsActive IS NOT NULL AND @bIsActive =0
		SELECT @SELECT = @SELECT + ' Record.DeleteReason as _DeleteReason, ' ,@sColumns=@sColumns + '''""' + '_DeleteReason'+ '""''' + ', '

	-- Red: if export, EnteredBy should configure at C# Export Template to show in the export output file
	IF @bIsActive = 1 AND @sType NOT LIKE 'export%'  
		SELECT @SELECT = @SELECT + ' Record.EnteredBy as _EnteredBy, ' ,@sColumns=@sColumns + '''""' + '_EnteredBy'+ '""''' + ', '
 
	IF @bIsActive IS NOT NULL AND @bIsActive =0
		SELECT @SELECT = @SELECT + ' Record.LastUpdatedUserID as _DeletedBy, ' ,@sColumns=@sColumns + '''""' + '_DeletedBy'+ '""''' + ', '


	IF (@bHasWarningResults IS NOT NULL AND @bHasWarningResults =1) OR (@sType = 'report')
		SELECT @SELECT = @SELECT + ' Record.WarningResults as Warning, ' ,@sColumns=@sColumns + '''""' + 'Warning'+ '""''' + ', '

	DECLARE @ParentAlias varchar(100), @ShowViewLink varchar(10)
    SET @counter = 1
	WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID = @counter)
	BEGIN
		-- Check to see if this column has a parent (which we will get data off)
		IF @sType='SQLOnly' OR @sType='view'
			BEGIN
				SELECT @sColumns = @sColumns + '''""' + DisplayText + '""''' + ', '
					FROM @tDisplayColumns 
					WHERE ID = @counter
			END

		IF EXISTS(SELECT 1 FROM @tDisplayColumns WHERE ID = @counter AND ParentTableID IS NOT NULL AND ColumnType='dropdown' AND ParentTableID<>-1)
		BEGIN
			SELECT @ParentAlias = 'P' + CAST(ColumnID as varchar) FROM @tDisplayColumns WHERE ID = @counter 
			-- SELECT  @ShowViewLink=ShowViewLink FROM @tDisplayColumns WHERE ID = @conter
			SELECT 
					-- build up the parent field anems
					--@SELECT = @SELECT + REPLACE(FieldsToShow, '[', @ParentAlias + '.[') + ' AS [' + DisplayText + '], '
					@SELECT = @SELECT + FieldsToShow  + ' AS [' + DisplayText + '], '
					--@SELECT = @SELECT + 'IIF('+ FieldsToShow + '='''',[Record].'+ChildJoinColumnName+' + ''(deleted)'','+ FieldsToShow +')' + ' AS [' + DisplayText + '], '
					-- when view link then we need to return the record ID too:  --CAST(ColumnID as varchar)
					+ CASE WHEN ((@sType='view' AND (ShowViewLink = 'Both' OR ShowViewLink='Summary')) OR @sType='allcolumns') THEN '[Record].' + ChildJoinColumnName + ' AS [**' + DisplayText + '_ID**],' ELSE '' END,

					-- Build the join condition and a isnumeric check (for bad data) when joining to RecordID
					@FROM = @FROM + ' LEFT OUTER JOIN [Record] ' + @ParentAlias + ' ON ' 
						+ CASE WHEN ParentJoinColumnName = 'RecordID' 
							THEN  @ParentAlias + '.' + ParentJoinColumnName + ' = CASE WHEN ISNUMERIC([Record].' + ChildJoinColumnName +')=1 THEN [Record].' + ChildJoinColumnName + ' ELSE -1 END '
							ELSE @ParentAlias + '.TableID = ' + cast(ParentTableID as varchar) 
							+ ' AND ' + @ParentAlias + '.' + ParentJoinColumnName + ' = [Record].' + ChildJoinColumnName 
							END						
					--+' AND ' + @ParentAlias + '.[IsActive] = 1' 
				
				FROM @tDisplayColumns WHERE ID = @counter AND ParentTableID IS NOT NULL
	
		END
		
		ELSE IF EXISTS(SELECT 1 FROM @tDisplayColumns WHERE ID = @counter AND ParentTableID IS NOT NULL AND ColumnType='dropdown' AND ParentTableID=-1)
		BEGIN
			SELECT @SELECT = @SELECT + ' [dbo].[fnGetSystemUserDisplayText] ('''+FieldsToShow+''',Record.' + SystemName + ') 
										AS [' + DisplayText + '], ' 
										+ CASE WHEN (@sType='allcolumns') THEN ' [Record].' + SystemName + ' AS [**' + DisplayText + '_ID**], ' ELSE '' END
				FROM @tDisplayColumns 
				WHERE ID = @counter
		END
		
		ELSE IF EXISTS(SELECT 1 FROM @tDisplayColumns WHERE ID = @counter AND ParentTableID IS NOT NULL AND ColumnType='listbox' AND ParentTableID<>-1)
		BEGIN
			SELECT @SELECT = @SELECT + ' STUFF ( ( SELECT '','' + A FROM (SELECT '+dbo.fnReplaceDisplayColumns_NoAlias(ColumnID)+' as A FROM Record X WHERE X.TableID='
			+CAST(ParentTableID AS VARCHAR)+' AND CHARINDEX('',''+CAST(X.RecordID AS VARCHAR)+'','','',''+ Record.' + SystemName 
			+ ' + '','')>0 ) as MainInfo FOR XML PATH('''') ),1,1,'''') 
				 AS [' + DisplayText + '], ' 
				FROM @tDisplayColumns 
				WHERE ID = @counter
		END
		--- ELSE
		ELSE

			SELECT @SELECT = @SELECT + 
				CASE WHEN DisplayText LIKE '%-Date' OR DisplayText LIKE '%(Date)' 
						THEN 'CASE WHEN ISDATE(Record.' + SystemName + ')=1 THEN CONVERT(varchar(15),CAST(Record.' + SystemName +' AS DATE),103) ELSE Record.' + SystemName + ' END'
					 WHEN DisplayText LIKE '%-Time' OR DisplayText LIKE '%(Time)' 
						THEN 'CASE WHEN ISDATE(Record.' + SystemName + ')=1 THEN CONVERT(varchar(15),CAST(Record.' + SystemName +' AS TIME),108) ELSE Record.' + SystemName + ' END' 
					 ELSE 'Record.' + SystemName END + ' AS [' + DisplayText + '], ' 
			FROM @tDisplayColumns 
			WHERE ID = @counter

	    
		--PRINT @SELECT
		SET @counter = @counter + 1
		IF @sOrder is null
			SELECT @sOrder= ' [' + DisplayText + ']' FROM @tDisplayColumns WHERE ID = @counter
	END 
	SELECT @SELECT = LEFT(@SELECT, LEN(@SELECT)-1) 
	
    IF @sParentColumnSortSQL IS NOT NULL
		SELECT @SELECT = @SELECT + ' ' + @sParentColumnSortSQL 
	
	--Make sure that FROM has all only required joins
	
	IF EXISTS (SELECT TOP 1 ColumnID FROM [Column] WHERE TableID=@nTableID AND TableTableID IS NOT NULL AND TableTableID>0 AND ColumnType='dropdown')
		BEGIN
			DECLARE @t2TableTable Table
			(
				ID int identity(1,1),
				ColumnID int,
				SystemName varchar(50),
				TableTableID INT				
			)

			INSERT INTO @t2TableTable(ColumnID,SystemName,TableTableID) 
				SELECT ColumnID,SystemName,TableTableID FROM [Column] 
					WHERE TableID=@nTableID AND TableTableID IS NOT NULL AND TableTableID>0 AND ColumnType='dropdown'
				
			DECLARE @counter2 int
			SET @counter2 = 1
			WHILE EXISTS(SELECT * FROM @t2TableTable WHERE ID >= @counter2)
				BEGIN
						DECLARE @n2TableTableID INT
						SELECT @ParentAlias = 'P' + CAST(ColumnID as varchar),@n2TableTableID=TableTableID FROM @t2TableTable WHERE ID = @counter2
						IF CHARINDEX(@ParentAlias,@SELECT,1)>0 AND CHARINDEX(@ParentAlias,@FROM,1)=0
							BEGIN
								-- parent
								SELECT
									@FROM = @FROM + ' LEFT OUTER JOIN [Record] ' + @ParentAlias + ' ON ' 
											+ @ParentAlias + '.RecordID = CASE WHEN ISNUMERIC([Record].' + SystemName +')=1 THEN [Record].' + SystemName + ' ELSE -1 END '
											+ ' AND ' + @ParentAlias + '.[IsActive] = 1' 
									FROM @t2TableTable WHERE ID = @counter2
							END	
							
							
						DECLARE @t3TableTable Table
							(
								ID int identity(1,1),
								ColumnID int,
								SystemName varchar(50),
								TableTableID INT				
							)
						
						INSERT INTO @t3TableTable(ColumnID,SystemName,TableTableID) 
							SELECT ColumnID,SystemName,TableTableID FROM [Column] 
							WHERE TableID=@n2TableTableID AND TableTableID IS NOT NULL AND TableTableID>0 AND ColumnType='dropdown'
						DECLARE @counter3 int
						SET @counter3 = 1
						WHILE EXISTS(SELECT * FROM @t3TableTable WHERE ID >= @counter3)
							BEGIN
								DECLARE @s3ParentAlias VARCHAR(25)
								SELECT @s3ParentAlias = 'P' + CAST(ColumnID as varchar) FROM @t3TableTable WHERE ID = @counter3
								IF CHARINDEX(@s3ParentAlias,@SELECT,1)>0 AND CHARINDEX(@s3ParentAlias,@FROM,1)=0
									BEGIN
										-- parent's parent
										SELECT
										@FROM = @FROM + ' LEFT OUTER JOIN [Record] ' + @s3ParentAlias + ' ON ' 
											+ @s3ParentAlias + '.RecordID = CASE WHEN ISNUMERIC('+@ParentAlias+'.' + SystemName +')=1 THEN '+@ParentAlias+'.' + SystemName + ' ELSE -1 END '
											+ ' AND ' + @s3ParentAlias + '.[IsActive] = 1' 
											FROM @t3TableTable WHERE ID = @counter3
									END
								SET @counter3 = @counter3 + 1
							END				
					SET @counter2 = @counter2 + 1
				END

		END

	IF PATINDEX('%Record.TableID%', @SELECT) > 0
	BEGIN
		SET @SELECT = REPLACE(@SELECT, 'Record.TableID', '[Table].TableName')
		SET @FROM = REPLACE(@FROM, 'FROM [Record]', 'FROM [Record] JOIN [Table] ON Record.TableID = [Table].TableID')
	END	

  IF PATINDEX('%Record.LastUpdatedUserID%', @SELECT) > 0
	BEGIN		
		SET @SELECT = REPLACE(@SELECT, 'Record.LastUpdatedUserID', 'LastUpdatedUser.FirstName + '' '' + LastUpdatedUser.LastName ')
	  SET @FROM = REPLACE(@FROM, 'FROM [Record]', 'FROM [Record] LEFT JOIN [User] LastUpdatedUser ON Record.LastUpdatedUserID = LastUpdatedUser.UserID')
	END

	-- Red: if not export
	IF PATINDEX('%Record.EnteredBy%', @SELECT) > 0 and @sType <> 'displayname'
	BEGIN	
		SET @SELECT = REPLACE(@SELECT, 'Record.EnteredBy', '[User].FirstName + '' '' + [User].LastName ')

		SET @FROM = REPLACE(@FROM, 'FROM [Record]', 'FROM [Record] JOIN [User] ON Record.EnteredBy = [User].UserID')
	END	

		-- ADD IN THE WHERE CRITERIA
	SELECT @WHERE = ' WHERE (Record.TableID = ' + CAST(@nTableID as varchar) + ') ' 
			
	IF @bIsActive IS NOT NULL 
		SELECT @WHERE = @WHERE + ' AND (Record.IsActive = ' + CAST(@bIsActive AS varchar) + ') '

	IF @dDateFrom IS NOT NULL 
		SELECT @WHERE = @WHERE + ' AND (Record.DateTimeRecorded >= ''' + CONVERT(varchar(30), @dDateFrom, 103) + ''') '

	IF @dDateTo IS NOT NULL 
		SELECT @WHERE = @WHERE + ' AND (Record.DateTimeRecorded <=  ''' + CONVERT(varchar(30), @dDateTo, 103) + ''') '

	IF @dDateAddedFrom IS NOT NULL 
		SELECT @WHERE = @WHERE + ' AND (CAST(Record.DateAdded AS DATE) >= CAST(''' + CONVERT(varchar(30), @dDateAddedFrom, 103) + '''  AS DATE)) '

	IF @dDateAddedTo IS NOT NULL 
		SELECT @WHERE = @WHERE + ' AND (CAST(Record.DateAdded AS DATE) <= CAST(  ''' + CONVERT(varchar(30), @dDateAddedTo, 103) + '''  AS DATE)) '

	IF @nEnteredBy IS NOT NULL 
		SELECT @WHERE = @WHERE + ' AND (Record.EnteredBy = ' + CAST(@nEnteredBy AS varchar) + ') '

	 IF @bHasWarningResults IS NOT null
     BEGIN
		 IF @bHasWarningResults =0 
			SELECT @WHERE = @WHERE + ' AND (Record.WarningResults IS NULL) ' 
	     
		 IF @bHasWarningResults =1 
			SELECT @WHERE = @WHERE + ' AND (Record.WarningResults IS NOT NULL) ' 
     END
	
	IF @sNumericSearch IS NOT null 
		SELECT @WHERE = @WHERE + ' AND ( ' + @sNumericSearch + ') AND ISNUMERIC(' + ') = 1'
		
	IF @sTextSearch IS NOT null
		SELECT @WHERE = @WHERE + ' AND ( ' + @sTextSearch + ')'
		
	-- Added 1 Dec 2015 JB
	IF @sType='view' AND @nViewID IS NOT NULL
	BEGIN
		DECLARE @sFixedFilter varchar(MAX)
		SELECT 	@sFixedFilter = [FixedFilter] FROM [View] WHERE ViewID = @nViewID
		IF @sFixedFilter IS NOT NULL
			SELECT @WHERE = @WHERE + ' ' + @sFixedFilter
	END	
	
		
	IF @sType LIKE 'exportbulk'
	BEGIN
		-- For ExportBulk we are going to run the SQK inro a temp table 
		-- and then convert that temp table into a comma delimated string
		SELECT @SQLMain = 'SELECT ' + @SELECT + ' INTO #ExportBulk ' + @FROM + ' ' + @WHERE  
		--PRINT @SQLMain
		
		SELECT @SQLMain = @SQLMain + '
		SET CONCAT_NULL_YIELDS_NULL OFF ;
		
		DECLARE @SQL		VARCHAR(MAX);
		DECLARE @S			VARCHAR(MAX);
		DECLARE	@Count		INT;

		SET		@Count = 0

		SELECT	@S = @S + QUOTENAME([COLUMN_NAME],''"'') + '','' 
				,@Count = @Count + 1  
		FROM	tempdb.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = OBJECT_NAME(OBJECT_ID(''tempdb..#ExportBulk''), db_id (''tempdb''));
						
		SET		@S = REPLACE(@S, '''''''', '''''''''''')
		
		SET		@S = LEFT(@S, LEN(@S)-1); 

		--PRINT	@S;

		SELECT	@SQL = ''SELECT 0 as [IsData], cast(0 as int) as ''''DBGSystemRecordID'''', '''''' + CAST( @Count AS VARCHAR(MAX) ) + '''''' as [ColumnCount], ' + CAST( @nTableID AS VARCHAR(MAX) ) + ' as [TableID], '''''' + @S + '''''' as [DataDump]
		UNION	SELECT 1, DBGSystemRecordID, 0, ' + CAST( @nTableID AS VARCHAR(MAX) ) + ', ''
		SELECT	@SQL = @SQL + '' 
				QUOTENAME(['' + [COLUMN_NAME] + ''], ''''"'''') + '''','''' + ''
		FROM	tempdb.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = OBJECT_NAME(OBJECT_ID(''tempdb..#ExportBulk''), db_id (''tempdb''));
		SELECT	@SQL = LEFT(@SQL,LEN(@SQL)-8) ;
		SELECT	@SQL = @SQL + '' FROM #ExportBulk;''
		
		--PRINT	@SQL;
		
		EXEC	(@SQL);'

		--PRINT @SQLMain

		EXEC (@SQLMain)
	END
	ELSE
	BEGIN
		-- Pull it all together

		--Red Begin
		DECLARE @tDisplayColumnTotals TABLE
		(
			ID int identity(1,1),
			ColumnID int,
			DisplayText varchar(100),
			Total money
		)

		DECLARE @tColumnTotals TABLE
		(
			ID int identity(1,1),
			Total money
		)		

		INSERT INTO @tDisplayColumnTotals (ColumnID, DisplayText)
			SELECT VItem.ColumnID, Col.DisplayName 
			FROM [ViewItem] VItem 
			INNER JOIN [Column] Col ON VItem.ColumnID = Col.ColumnID
			WHERE VItem.ShowTotal=1 AND VItem.ViewID = @nViewID	
		--Red End

		DECLARE @SQLCount varchar(MAX)
		DECLARE @SQLSum varchar(MAX) --Red

		SET DATEFORMAT DMY 
		SET @SELECT = 'SELECT * FROM (SELECT RecordInfo.*,ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ' ) as RowNum FROM (SELECT ' + @SELECT

		SELECT @SQLMain =  
			+ @SELECT + ' ' + @FROM + ' ' + @WHERE  + ') as RecordInfo) as RecordFinalInfo WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
    
	    SELECT @SQLCount= REPLACE(@SELECT, 'SELECT * FROM', 'SELECT COUNT([DBGSystemRecordID]) AS TotalRows FROM') + ' ' + @FROM + ' ' + @WHERE + ') as RecordInfo) as RecordFinalInfo'
		
		--Red Begin
		DECLARE @ArchiveDatabase varchar(max)
		SELECT @ArchiveDatabase = ArchiveDatabase FROM [Table]
		WHERE TableID = @nTableID

		DECLARE @CounterTotal int, @DisplayText varchar (50)
		SET @CounterTotal = 1

		WHILE @CounterTotal <= (SELECT COUNT(*) FROM @tDisplayColumnTotals)
		BEGIN
				
				SELECT @DisplayText = DisplayText  FROM @tDisplayColumnTotals WHERE ID = @CounterTotal
				-- JB 4 Jul 2017: SELECT @SQLSum = REPLACE(@SELECT, 'SELECT * FROM', 'SELECT ISNULL(SUM(CAST(CONVERT(VARCHAR(50), DBO.[RemoveSpecialChars_IngnoreSymbols](['+ @DisplayText +'])) AS MONEY)),0)  FROM') + ' ' + @FROM + ' ' + @WHERE + ') as RecordInfo) as RecordFinalInfo' 
				SELECT @SQLSum = REPLACE(@SELECT, 'SELECT * FROM', 'SELECT SUM(CAST(CONVERT(VARCHAR(50), ISNULL(DBO.[dbgNum](['+ @DisplayText +']),0)) AS MONEY)) FROM') + ' ' + @FROM + ' ' + @WHERE + ') as RecordInfo) as RecordFinalInfo' 
				IF @bUseArchiveDatabase = 1 AND @ArchiveDatabase <> ''
				BEGIN
					SET @SQLSum = REPLACE(@SQLSum, 'FROM [Record]', ' FROM ['+ @ArchiveDatabase +'].[dbo].[Record] ')	
				END
				INSERT INTO @tColumnTotals (Total) EXEC (@SQLSum);   
				UPDATE @tDisplayColumnTotals SET Total =(SELECT CD.Total FROM @tColumnTotals CD WHERE CD.ID = @CounterTotal) 
				WHERE ID = @CounterTotal 		
					
			SET @CounterTotal = @CounterTotal + 1
									
		END
	--Red End

		IF @sType='SQLOnly'
			SET @nMaxRows=1
		
	    SET ROWCOUNT @nMaxRows
	    
		--PRINT left(@SQLMain,8000)
		--PRINT substring(@SQLMain,8001,8000)

		--IF @sType<>'SQLOnly'

		--Red Ticket 3358 - Archive Old Data			
		IF @bUseArchiveDatabase = 1 AND @ArchiveDatabase <> ''
		BEGIN
			SET @SQLMain = REPLACE(@SQLMain, 'FROM [Record]', ' FROM ['+ @ArchiveDatabase +'].[dbo].[Record] ')	
			SET @SQLCount = REPLACE(@SQLCount, 'FROM [Record]', ' FROM ['+ @ArchiveDatabase +'].[dbo].[Record] ')	
		END
		--end Red

		EXEC (@SQLMain)
	
		IF @sType='SQLOnly'  OR @sType='view'
			BEGIN
				SELECT @sColumns= @sColumns + '''""' + 'RowNum'+ '""''' 
				SELECT @sReturnHeaderSQL=@sColumns

				--KG Ticket 2904 - 26/7/17 added SET DATEFORMAT dmy; so date and time converts correctly
				--SELECT @sReturnSQL=  @SQLMain
				SELECT @sReturnSQL=  'SET DATEFORMAT dmy; '+ @SQLMain
			END
			

		SET ROWCOUNT 0 
		--PRINT left(@SQLCount,8000)
		--PRINT substring(@SQLCount,8001,8000)
		EXEC (@SQLCount)

		--for C# SqlDataAdapter OUTPUT Param
		IF @sType='SQLOnly' OR @sType='view'
			SELECT TOP 1 @sReturnSQL AS [ReturnSQL],@sReturnHeaderSQL AS [ReturnHeaderSQL] --FROM DBO.[Account]

		--Red Begin
		IF @SQLSum <> ''
			SELECT ColumnID,DisplayText,Total FROM @tDisplayColumnTotals
		--Red End
	END

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') + ' 
	 @SQLMain: ' + ISNULL(@SQLMain, '') + '
	 @SQLCount: ' + ISNULL(@SQLCount, '') + '
	 @SQLSum: ' + ISNULL(@SQLSum, '') + '
	 @nTableID: ' + CAST(ISNULL(@nTableID,0) AS varchar(10)) + '
	 @sOrder: ''' + ISNULL(@sOrder,'') + '''
	 @nViewID: ' + CAST(ISNULL(@nViewID,0) AS varchar(10)) 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Record_List', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
