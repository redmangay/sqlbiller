﻿
/****** Object:  StoredProcedure [dbo].[ets_Chart_GetData_New]    Script Date: 2018-02-01 5:07:02 PM ******/
CREATE PROCEDURE [dbo].[ets_Chart_GetData_Count_Series]
       -- Add the parameters for the stored procedure here
       @nTableID int,
	   @sVXXX VARCHAR(50),
	   @GraphSeriesColumnID varchar(MAX)=NULL,
	   @GraphSeriesID varchar(MAX)=NULL
AS
/*
       PRINT SYSDATETIME()
       EXEC [ets_Chart_GetData_Count_Series]
              @nTableID=8425,@sVXXX='V006',@GraphSeriesColumnID='v001',@GraphSeriesID='a'
		EXEC [ets_Chart_GetData_Count_Series]
              @nTableID=8425,@sVXXX='V008',@GraphSeriesColumnID='v001',@GraphSeriesID='a'
	  EXEC [ets_Chart_GetData_Count_Series]
              @nTableID=8425,@sVXXX='V009',@GraphSeriesColumnID='v001',@GraphSeriesID='a'
              
*/

BEGIN TRY
    SET NOCOUNT ON;
--SET DATEFORMAT dmy  
--SELECT V006 FROM [Record] WHERE IsActive=1 AND TableID=8425
--exec [ets_Record_List] @nTableID=8424
-- SELECT V001,RecordID FROM [Record] WHERE IsActive=1 AND TableID=8424
--DECLARE  @nTableID int=8425
--DECLARE @sVXXX VARCHAR(50)='V006'
--DECLARE @DisplayColumn VARCHAR(50)
DECLARE @TableTableID INT
DECLARE @ColumnID INT
DECLARE @ParentVXXX VARCHAR(100)
DECLARE @SQL VARCHAR(MAX)
DECLARE @sSeries varchar(max)
DECLARE @sDropDownType varchar(255)
declare @sDropdownValues varchar(max)
SELECT @ColumnID=ColumnID,@TableTableID=TableTableID,@sDropDownType=DropDownType,@sDropdownValues=DropdownValues FROM [Column] WHERE TableID=@nTableID AND SystemName=@sVXXX

SELECT  @ParentVXXX=dbo.fnReplaceDisplayColumns_NoAlias(@ColumnID)

--PRINT @ParentVXXX
--PRINT CAST(@TableTableID AS VARCHAR)

SELECT @sSeries= CASE
				WHEN @GraphSeriesColumnID IS NULL THEN ''
				WHEN @GraphSeriesID IS NULL THEN ''
				WHEN LEN(@GraphSeriesColumnID) = 0 THEN ''
				WHEN LEN(@GraphSeriesID) = 0 THEN ''
				ELSE ' AND A.[' + @GraphSeriesColumnID + '] = ''' + @GraphSeriesID + ''''
			END 


IF @TableTableID IS NOT NULL
	SET @SQL='SELECT (SELECT '+@ParentVXXX+' FROM [Record] B WHERE B.TableID='+CAST(@TableTableID AS varchar)+' AND B.RecordID=CAST(CountTable.'+@sVXXX+' AS INT)) '+@sVXXX+'DisplayName,CountTable.PoltCount  AS [Count],CountTable.'+@sVXXX+' AS RecordID FROM (SELECT A.'+@sVXXX+', COUNT(A.'+@sVXXX+') AS PoltCount FROM [Record] A WHERE A.IsActive=1 AND A.TableID='+CAST(@nTableID AS VARCHAR)+ @sSeries + ' AND A.'+@sVXXX+' IS NOT NULL AND ISNUMERIC(A.'+@sVXXX+')=1 GROUP BY A.'+@sVXXX+') AS CountTable'

IF @TableTableID IS NULL AND @sDropDownType='values'
	SET @SQL='SELECT '+@sVXXX+' '+@sVXXX+'DisplayName,CountTable.PoltCount  AS [Count],-1 AS RecordID FROM (SELECT A.'+@sVXXX+', COUNT(A.'+@sVXXX+') AS PoltCount FROM [Record] A WHERE A.IsActive=1 AND A.TableID='+CAST(@nTableID AS VARCHAR)+ @sSeries + ' AND A.'+@sVXXX+' IS NOT NULL  GROUP BY A.'+@sVXXX+') AS CountTable'

IF @TableTableID IS NULL AND @sDropDownType='value_text' AND @sDropdownValues IS NOT NULL
	SET @SQL='SELECT RTRIM( LTRIM([dbo].[fnGetTextFromValue](CountTable.'+@sVXXX+','''+@sDropdownValues+'''))) '+@sVXXX+'DisplayName,CountTable.PoltCount  AS [Count],-1 AS RecordID FROM (SELECT A.'+@sVXXX+', COUNT(A.'+@sVXXX+') AS PoltCount FROM [Record] A WHERE A.IsActive=1 AND A.TableID='+CAST(@nTableID AS VARCHAR)+ @sSeries + ' AND A.'+@sVXXX+' IS NOT NULL  GROUP BY A.'+@sVXXX+') AS CountTable'

PRINT @SQL

EXEC (@SQL)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('[ets_Chart_GetData_Count', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

