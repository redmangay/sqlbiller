﻿

CREATE PROCEDURE   [dbo].[Record_Set_UniqueKey]
(
	@sUpdateRecord char(1) = 'R', -- (R)ecord or (T)empRecord
	@nTableID int = NULL,
	@nBatchID int = NULL,
	@nRecordID INT=NULL, @sRecordIDs varchar(MAX)=NULL
						
)
AS

BEGIN TRY
	SET DATEFORMAT dmy;

	IF @nTableID is NULL AND @nBatchID IS NOT NULL
		SELECT @nTableID=TableID from Batch WHERE BatchID=@nBatchID

	DECLARE @UniqueCol1 varchar(20), @UniqueCol2 varchar(20), @sTableID char(5), @sTableNameToUpdate varchar(20)
	DECLARE @ColType1 varchar(20),@ColType2 varchar(20),@ColIsStandard1 bit,@ColIsStandard2 bit
	SELECT @sTableID = RIGHT('0000' + CAST(@nTableID as varchar(5)),5)

	SELECT @UniqueCol1 = C1.SystemName,@ColType1=C1.ColumnType,@ColIsStandard1=C1.IsStandard
	FROM [Table] T 
		JOIN      [Column] C1 ON C1.ColumnID = T.UniqueColumnID
		WHERE T.TableID = @nTableID

	SELECT @UniqueCol2 = C2.SystemName,@ColType2=C2.ColumnType,@ColIsStandard2=C2.IsStandard
	FROM [Table] T 
		JOIN [Column] C2 ON C2.ColumnID = T.UniqueColumnID2
		WHERE T.TableID = @nTableID

	IF @sUpdateRecord LIKE 'T'
		IF @UniqueCol1 = 'RecordID'
			SET @UniqueCol1 = 'TempRecordID'
		IF @UniqueCol2 = 'RecordID'
			SET @UniqueCol2 = 'TempRecordID'

	DECLARE @SQL varchar(MAX) = '', @Where varchar(MAX) = ''

	IF @UniqueCol1 IS NOT NULL
	BEGIN
		IF @sUpdateRecord LIKE 'R' 
			BEGIN
				SET @sTableNameToUpdate = '[Record]'
				SET @Where = 'WHERE [TableID]=' + CAST(@nTableID AS varchar(10)) + ' AND IsActive=1'
				IF @nRecordID IS NOT NULL
					SET @Where= @Where+ ' AND RecordID='+ CAST(@nRecordID AS varchar(10))
				IF @nRecordID IS NULL AND @sRecordIDs IS NOT NULL
					SET @Where= @Where+ ' AND RecordID IN('+ @sRecordIDs + ')'
			END
		ELSE
			BEGIN
				SET @sTableNameToUpdate = '[TempRecord]'
				SET @Where = 'WHERE [BatchID]=' + CAST(@nBatchID AS varchar(10)) 
			END
		BEGIN
			IF @ColType1 LIKE 'date%'
				BEGIN
					SET @SQL = '
					UPDATE ' + @sTableNameToUpdate + '
						SET [UniqueKey] = ''' + @sTableID + '#''+FORMAT(CONVERT(Datetime,' + @UniqueCol1 + ',103), ''dd/MM/yyyy HH:mm:ss'')'
					SET @Where=@Where + ' AND (ISDATE(' + @UniqueCol1 + ')=1 OR ' + @UniqueCol1 + ' IS NULL)'
				END
			ELSE
				SET @SQL = '
					UPDATE ' + @sTableNameToUpdate + '
						SET [UniqueKey] = ''' + @sTableID + '#''+RTRIM(LEFT(ISNULL(' +  @UniqueCol1 + ',''''),99))'

			IF @ColType2 IS NOT NULL
			BEGIN
				IF @ColType2 LIKE 'date%'
					BEGIN
						SET @SQL = @SQL + ' + ''#'' + FORMAT(CONVERT(Datetime,' + @UniqueCol2 + ',103), ''dd/MM/yyyy HH:mm:ss'')'
						SET @Where=@Where + ' AND (ISDATE(' + @UniqueCol2 + ')=1 OR ' + @UniqueCol2 + ' IS NULL)'
					END
				
				ELSE
					SET @SQL = @SQL + ' + ''#'' + RTRIM(LEFT(ISNULL(' +  @UniqueCol2 + ',''''),99))'
			END

			SET @SQL = @SQL + @Where

			--IF @bOnlyBlank = 1
			--	SET @SQL = @SQL + '
			--		AND [UniqueKey] IS NULL'

			PRINT @SQL
			EXEC (@SQL)

		END
	END

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Record_Set_UniqueKey', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
