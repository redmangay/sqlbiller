﻿
 
CREATE PROCEDURE [dbo].[Content_Select]
(
	@nContentID int  = NULL,
	@sContentKey nvarchar(50)  = NULL,
	@sHeading nvarchar(500)  = NULL,
	@sContent ntext  = NULL,
	@sStoredProcedure nvarchar(200)  = NULL,
	@dDateAdded datetime  = NULL,
	@dDateUpdated datetime  = NULL,
	@sOrder nvarchar(200) = ContentKey, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647,
	@nAccountID int ,
	@bGlobal bit=0,
	@bOnlyTemplate bit=NULL,
	@bOnlyGlobal bit=NULL,
	@nContentTypeID INT=NULL

)
	/*----------------------------------------------------------------
	EXEC [Content_Select] @nAccountID=13,@bGlobal=1
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nContentID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ContentID = '+CAST(@nContentID AS NVARCHAR)
	
	IF @nContentTypeID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND C.ContentTypeID = '+CAST(@nContentTypeID AS NVARCHAR)


	
	IF @bGlobal=1
	BEGIN
		IF @bOnlyTemplate IS NULL AND @bOnlyGlobal IS NULL
			SET @sWhere = @sWhere + ' AND (AccountID = '+CAST(@nAccountID AS NVARCHAR) + ' OR AccountID IS NULL)'	
	
		IF @bOnlyTemplate IS NOT NULL AND @bOnlyGlobal IS NULL
			SET @sWhere = @sWhere + ' AND (AccountID IS NULL AND ForAllAccount=1) '	
	
	
		IF @bOnlyTemplate IS NULL AND @bOnlyGlobal IS NOT NULL
			SET @sWhere = @sWhere + ' AND (AccountID IS NULL AND ForAllAccount=0)'	
	
	
		IF @bOnlyTemplate IS NOT NULL AND @bOnlyGlobal IS NOT NULL
			SET @sWhere = @sWhere + ' AND (AccountID IS NULL )'	
	
		
	END
	IF @bGlobal=0
			SET @sWhere = @sWhere + ' AND (AccountID = '+CAST(@nAccountID AS NVARCHAR) + ' OR (AccountID IS NULL AND ForAllAccount=1 AND ContentKey not in(SELECT ContentKey FROM Content WHERE ContentKey=ContentKey AND AccountID = '+CAST(@nAccountID AS NVARCHAR) + ' )))'		



	IF @sContentKey IS NOT NULL 
		SET @sWhere = @sWhere + ' AND (ContentKey LIKE '+'''%' + @sContentKey + '%'' OR Heading LIKE '+'''%' + @sContentKey + '%'' OR Content LIKE '+'''%' + @sContentKey + '%'' )'
	IF @sHeading IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Heading LIKE '+'''%' + @sHeading + '%'''
	IF @sStoredProcedure IS NOT NULL 
		SET @sWhere = @sWhere + ' AND StoredProcedure LIKE '+'''%' + @sStoredProcedure + '%'''
	IF @dDateAdded IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DateAdded = '+CAST(@dDateAdded AS NVARCHAR)
	IF @dDateUpdated IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DateUpdated = '+CAST(@dDateUpdated AS NVARCHAR)
	SET @sSelect = 'SELECT * FROM 
						(SELECT C.*,CT.ContentTypeName, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum FROM [Content] C 
						LEFT JOIN [ContentType] CT ON C.ContentTypeID=CT.ContentTypeID ' + @sWhere + ')
						 as ContentInfo  Where RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [Content] C ' + @sWhere 
 
 
	EXEC (@sSelect)
	PRINT @sSelect

	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Content_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
