﻿/* Template to create an Add Stored Procedure */
------------------------------------------------
CREATE PROCEDURE [dbo].[Renzo_BillandPaymentActivity_Add]
(
	@RecordID  int ,
	@FormSetID int = null /* Optional */
)
AS
BEGIN
	-- Add your code here
	-- WHERE TableID = 1
	-- exec [Renzo_BillandPaymentActivity_Add] '2707345',''
	DECLARE			@JobNo			VARCHAR(MAX)
	--DECLARE			@InvoiceNo		VARCHAR(MAX)
	DECLARE			@Activity		VARCHAR(MAX)
	--DECLARE			@JobRecordID		VARCHAR(MAX)
	DECLARE			@JobValue		VARCHAR(MAX)
	DECLARE			@TotalInvoiceAmount		VARCHAR(MAX)

	/*	Get Job No	*/

	SELECT			@JobNo = [Bill & Payment Activity].V002				/*	[Job No]	*/
					--,@JobRecordID = [Bill & Payment Activity].V002		/*	[Job RecordID (Linked account)]	*/
					,@Activity = [Bill & Payment Activity].V029			/*	[Activity]		*/
		FROM		[Record] [Bill & Payment Activity]  WITH (NOLOCK)
		WHERE		[Bill & Payment Activity].RecordID = @RecordID

	/*	Set all unallocated invoices to the newly created interim invoice		*/

	SELECT @JobValue = V008 FROM [Record] WHERE RecordID = @JobNo
	
	SELECT @TotalInvoiceAmount = SUM(CAST(V005 AS MONEY)) FROM [RECORD] WHERE TABLEID ='2711' AND V002 = @JobNo

	IF					@Activity IN ( 'Invoice' )
				AND		ISNULL(@JobNo, '' ) <> ''
		BEGIN

		UPDATE			[Timesheet Details] 							/*	[Timesheet Details]	*/
			SET			V016 = CAST( @RecordID AS VARCHAR(MAX) )		/*	[Invoice Number]	*/
			FROM		[Record] [Timesheet Details]
			WHERE		[Timesheet Details].TableID = '2729'			/*	Timesheet Details	*/
			AND			[Timesheet Details].IsActive = 1
			AND			[Timesheet Details].V001 = @JobNo				/*	[Job No]			*/
			AND			ISNULL([Timesheet Details].V016, '') = ''		/*	[Invoice Number]	*/
			AND			[Timesheet Details].V022 <> '3' -- Hold

		-- LETS UPDATE TIMESHEET
		UPDATE [Timesheet]
		SET V034 = 'Invoiced'
		FROM	[Record] [Timesheet]
		WHERE RECORDID IN (
		SELECT V011 FROM [Record] [Timesheet Details]
		WHERE	[Timesheet Details].TableID = '2729'			/*	Timesheet Details	*/
			AND			[Timesheet Details].IsActive = 1
			AND			[Timesheet Details].V001 = @JobNo  
			AND			[Timesheet Details].V016 =  CAST( @RecordID AS VARCHAR(MAX) )
			)
			AND			[Timesheet].TABLEID = 2488
			AND ISNULL([Timesheet].V034,'') = ''

		-- LETS UPDATE DISBURSEMENT
		UPDATE [Disbursement]
		SET V021 = 'Invoiced'
		FROM [Record] [Disbursement]
		WHERE RECORDID IN (
		SELECT V010 FROM [Record] [Timesheet Details] 
		WHERE	[Timesheet Details].TableID = '2729'			/*	Timesheet Details	*/
			AND			[Timesheet Details].IsActive = 1
			AND			[Timesheet Details].V001 = @JobNo  
			AND			[Timesheet Details].V016 =  CAST( @RecordID AS VARCHAR(MAX) )
			)
			AND			[Disbursement].TABLEID = 2725
			AND ISNULL([Disbursement].V021,'') = ''
		

		UPDATE [Record]
			SET V036 = NULL
			WHERE [RecordID] = @RecordID	
		END

	IF CAST(@JobValue AS MONEY) <= CAST(@TotalInvoiceAmount AS MONEY)
	BEGIN
		UPDATE [RECORD] SET V062 = 'Yes' -- Complete and Invoiced
		WHERE [RecordID] = @RecordID
	END
	ELSE IF (@Activity = 'Bad Debt Entry')
	BEGIN
	   UPDATE [RECORD] SET V062 = 'Yes' -- Complete and Invoiced
		WHERE [RecordID] = @RecordID
	END
	ELSE
	BEGIN
		UPDATE [RECORD] SET V062 =  'No' -- Not Complete and Invoiced
		WHERE [RecordID] = @RecordID
	END
	--select @JobValue AS JOBVALUE, @TotalInvoiceAmount AS TOTALINVOICE

	RETURN @@ERROR
END
