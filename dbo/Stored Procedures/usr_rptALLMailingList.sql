﻿CREATE PROCEDURE [dbo].[usr_rptALLMailingList]
AS
BEGIN
    
    
SELECT  [Client Information].V001 as Company /* Company */
	  ,[Client Contact].V004 as Title /* Title */
	  ,[Client Contact].V005 + ' ' + [Client Contact].V006 as Name  /* Name */
	  ,[Client Information].V002 as Address1 /* Address 1 */
	  ,[Client Information].V020 as Address2 /* Address 2 */
	  ,[Client Information].V003 as Suburb /* Suburb */
	  ,[Client Information].V004 as [State] /* State */
	  ,[Client Information].V005 as [PostCode] /* Post Code */
	  ,[Client Information].V006 as Country /* Country */
	  ,[Client Contact].V003 as MailOuts /* MailOuts */

    FROM Record [Client Contact]
    JOIN Record [Client Information] ON [Client Information].TableID = 2474
    AND [Client Information].IsActive = 1
    AND [Client Information].RecordID = [Client Contact].V012

    WHERE [Client Contact].TableID = 2657
	   AND [Client Contact].IsActive = 1
	   ORDER BY  [Client Information].V001


END