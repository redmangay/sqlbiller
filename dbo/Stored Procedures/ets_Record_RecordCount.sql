﻿ 


CREATE PROCEDURE [dbo].[ets_Record_RecordCount]
(
	@sTableIDs varchar(MAX),
	@dDateFrom datetime,
	@dDateTo datetime,
	@sColumnID varchar(MAX) = 'Record.DateTimeRecorded'
)

-- EXEC ets_Record_RecordCount '-1,942','1/1/2000','1/1/2014'

AS

BEGIN TRY
	SET dateformat dmy
	DECLARE @sSQL varchar(MAX)

	SELECT @sSQL=
		CASE @sColumnID
			WHEN 'Record.DateTimeRecorded' THEN
				'SELECT COUNT(*) FROM Record WHERE IsActive=1 AND TableID IN ('+@sTableIDs+')
				AND Record.DateTimeRecorded >=''' + CONVERT(varchar(30), @dDateFrom, 120) +
				 ''' AND Record.DateTimeRecorded <= ''' + CONVERT(varchar(30), @dDateTo, 120) + ''''
			ELSE
				'SELECT COUNT(*) FROM Record WHERE IsActive=1 AND TableID IN ('+@sTableIDs+')
				AND CAST(CASE WHEN ISDATE(' + @sColumnID + ') > 0 THEN ' + @sColumnID + ' ELSE NULL END AS datetime) >=''' + CONVERT(varchar(30), @dDateFrom, 113) +
				 ''' AND CAST(CASE WHEN ISDATE(' + @sColumnID + ') > 0 THEN ' + @sColumnID + ' ELSE NULL END AS datetime) <= ''' + CONVERT(varchar(30), @dDateTo, 113) + ''''
		END

		PRINT @sSQL
		EXEC (@sSQL)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Record_RecordCount', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
