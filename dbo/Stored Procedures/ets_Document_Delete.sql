﻿

CREATE PROCEDURE   [dbo].[ets_Document_Delete]
(
	@nDocumentID int 
)
	/*----------------------------------------------------------------
	
	---------------------------------------------------------------*/
 
AS


BEGIN TRY

	UPDATE [UserRole] SET DashBoardDocumentID=NULL WHERE DashBoardDocumentID=@nDocumentID
		DELETE Conditions WHERE DocumentSectionID IN (SELECT DocumentSectionID FROM DocumentSection WHERE DocumentID=@nDocumentID)

	DELETE ScheduleReport WHERE MainDocumentID=@nDocumentID

	DELETE DocumentSection 
	WHERE DocumentID = @nDocumentID

	UPDATE Menu
	SET DocumentID=NULL WHERE DocumentID = @nDocumentID


	DELETE FROM [Document]
	WHERE DocumentID = @nDocumentID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Document_Delete', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
