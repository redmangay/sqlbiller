﻿

CREATE PROCEDURE   [dbo].[spRefreshImportTemplateFields]
(
	@nImportTemplateID int, @SortByFieldName bit=NULL
)
AS
/*
	Refreshes the order that fields are Imported to match the order detail page
	Note that we do the fields on the left first, then the right
*/
BEGIN TRY
	DECLARE @t TABLE (DisplayOrder int identity(0,1), ID int, ImportHeaderName varchar(MAX))


	IF @SortByFieldName is NULL
		BEGIN
			INSERT INTO @t (ID, ImportHeaderName)
				SELECT ImportTemplateItemID, C.DisplayName
					FROM dbo.ImportTemplateItem ETI 
					JOIN [Column] C ON C.ColumnID = ETI.ColumnID
					WHERE ETI.ImportTemplateID = @nImportTemplateID
					ORDER BY C.DisplayRight, C.DisplayOrder
		END
	IF @SortByFieldName IS NOT NULL
		BEGIN

			INSERT INTO @t (ID, ImportHeaderName)
				SELECT ImportTemplateItemID, C.DisplayName
					FROM dbo.ImportTemplateItem ETI 
					JOIN [Column] C ON C.ColumnID = ETI.ColumnID
					WHERE ETI.ImportTemplateID = @nImportTemplateID
					ORDER BY C.DisplayName

		END

	

			UPDATE ImportTemplateItem 
				SET ColumnIndex = T.DisplayOrder,
				ImportHeaderName = CASE WHEN T.ImportHeaderName IS NULL  
								THEN dbo.fnNextAvailableImportTemplateItemHeading(ITI.ImportHeaderName,ImportTemplateID,ImportTemplateItemID)
								 ELSE dbo.fnNextAvailableImportTemplateItemHeading(T.ImportHeaderName,ImportTemplateID,ImportTemplateItemID) END
				FROM ImportTemplateItem ITI
				JOIN @t T ON T.ID = ITI.ImportTemplateItemID 
				WHERE ImportTemplateID = @nImportTemplateID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spRefreshImportTemplateFields', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH	
