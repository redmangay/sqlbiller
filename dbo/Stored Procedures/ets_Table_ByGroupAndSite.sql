﻿

 
CREATE PROCEDURE [dbo].[ets_Table_ByGroupAndSite]
(
	@nAccountID int  = NULL,
	@nMenuID int  = NULL,
	@nLocationID int  = NULL,
	@nTableID int  = NULL
	
)
 
 -- ets_Table_ByGroupAndSite @nAccountID=13,@nTableID=109
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	--SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1 AND
						   [Table].IsActive=1 AND Menu.IsActive =1 AND Location.IsActive=1'
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Table].AccountID = '+CAST(@nAccountID AS NVARCHAR)
	IF @nMenuID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Menu.MenuID = '+CAST(@nMenuID AS NVARCHAR)
	IF @nLocationID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND LocationTable.LocationID = '+CAST(@nLocationID AS NVARCHAR)
	IF @nTableID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND LocationTable.TableID = '+CAST(@nTableID AS NVARCHAR)

	SET @sSelect = 'SELECT     DISTINCT LocationTable.TableID,[Table].TableName,Location.LocationID,Location.LocationName
                      
	FROM         LocationTable INNER JOIN
						  [Table] ON LocationTable.TableID = [Table].TableID INNER JOIN
						  Menu ON [Table].MenuID = Menu.MenuID INNER JOIN
						  Location ON LocationTable.LocationID = Location.LocationID ' + @sWhere 
 
 
	EXEC (@sSelect)
	SET ROWCOUNT 0
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Table_ByGroupAndSite', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
