﻿create PROCEDURE [dbo].[Hii_SavePatient_bu]
(
	@RecordID int,
	@UserID int,
	@Return varchar(max) output
)
AS
-- exec Hii_SavePatient @RecordID=539220, @UserID=0, @Return=''
BEGIN
	-- Check the initial conditions and get the values
	DECLARE @Condition varchar(max), @ID varchar(max), @PatientNumber varchar(max)
	SELECT @Condition = V048, @ID = V049
		FROM [Record] WHERE RecordID = @RecordID 
		AND ISNUMERIC(V049)=1
		AND V005 IS NULL
		AND V048 IS NOT NULL
		AND V049 IS NOT NULL
		
		print @Condition
		print @ID
	
	IF @Condition LIKE '%Achalasia%'
		UPDATE [Record] SET V005 = 'A' + @ID WHERE RecordID = @RecordID
	IF @Condition LIKE '%Bariatric%'
		UPDATE [Record] SET V005 = 'B' + @ID WHERE RecordID = @RecordID
	IF @Condition LIKE '%Hiatus hernia%'
		UPDATE [Record] SET V005 = 'HH' + @ID WHERE RecordID = @RecordID
	IF @Condition LIKE '%OG Cancer%'
		UPDATE [Record] SET V005 = 'OG' + @ID WHERE RecordID = @RecordID
	IF @Condition LIKE '%Liver and biliary tumours%'
		UPDATE [Record] SET V005 = 'LB' + @ID WHERE RecordID = @RecordID
	IF @Condition LIKE '%Pancreatic and periampullary tumours%'
		UPDATE [Record] SET V005 = 'PP' + @ID WHERE RecordID = @RecordID


END
