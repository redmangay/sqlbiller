﻿

CREATE PROCEDURE [dbo].[User_Select]
(
	@nUserID int  = NULL,
	@sFirstName nvarchar(50)  = NULL,
	@sLastName nvarchar(50)  = NULL,
	@sPhoneNumber varchar(20)  = NULL,
	@sEmail nvarchar(200)  = NULL,
	--@sPassword nvarchar(200)  = NULL, -- Red removed this for Ticket 3059
	@bIsActive bit  = NULL,
	@dDateAdded datetime  = NULL,
	@dDateUpdated datetime  = NULL,
	@nAccountID int  = NULL,
	@sOrder nvarchar(200) = FirstName, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
 
AS

BEGIN TRY

	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows

	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nUserID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND UserID = '+CAST(@nUserID AS NVARCHAR)
	IF @sFirstName IS NOT NULL 
		SET @sWhere = @sWhere + ' AND FirstName LIKE '+'''%' + @sFirstName + '%'''
	IF @sLastName IS NOT NULL 
		SET @sWhere = @sWhere + ' AND LastName LIKE '+'''%' + @sLastName + '%'''
	IF @sPhoneNumber IS NOT NULL 
		SET @sWhere = @sWhere + ' AND U.PhoneNumber LIKE '+'''%' + @sPhoneNumber + '%'''
	IF @sEmail IS NOT NULL 
		SET @sWhere = @sWhere + ' AND U.Email LIKE '+'''%' + @sEmail + '%'''
	--IF @sPassword IS NOT NULL  -- Red removed this for Ticket 3059
		--SET @sWhere = @sWhere + ' AND Password = '+'''' + @sPassword + '''' -- Red removed this for Ticket 3059
	IF @bIsActive IS NOT NULL 
		SET @sWhere = @sWhere + ' AND U.IsActive = '+CAST(@bIsActive AS NVARCHAR)
	IF @dDateAdded IS NOT NULL 
		SET @sWhere = @sWhere + ' AND U.DateAdded = '+CAST(@dDateAdded AS NVARCHAR)
	IF @dDateUpdated IS NOT NULL 
		SET @sWhere = @sWhere + ' AND U.DateUpdated = '+CAST(@dDateUpdated AS NVARCHAR)
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND UR.AccountID = '+CAST(@nAccountID AS NVARCHAR)


	SET @sSelect = 'SELECT * FROM 
		(SELECT U.UserID,  U.FirstName, U.LastName, U.PhoneNumber, U.Email, 
			U.IsActive, U.DateAdded, U.DateUpdated, UR.AccountID, UR.IsAccountHolder, 
			(U.FirstName + '' '' + U.LastName) as FullName,
			UR.IsAdvancedSecurity, U.LoginCount, U.LastLoginTime,
			UR.IsPrimaryAccount, R.[Role], 
			ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum 
			FROM [User] U 
			INNER JOIN UserRole UR ON UR.UserID = U.UserID 
			LEFT JOIN  [Role] R on R.RoleID = UR.RoleID ' 
		
			+ @sWhere + ') as UserInfo'
			+ ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
 
	 SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [User] u INNER JOIN UserRole ur ON
		 U.UserID=UR.UserID ' + @sWhere 
 

	PRINT  @sSelect
	EXEC (@sSelect)
	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('User_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
