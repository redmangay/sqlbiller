﻿      
CREATE Procedure [dbo].[usr_rptSundryDebtors]        
 @StartDate datetime  ,--='2010-04-05',   
 @EndDate  datetime , --='2015-04-09' 
 @daterange varchar(10) , --1
 @month varchar(10)=null , --1
 @year varchar(10) =null   

AS        
BEGIN   

/*

EXEC [dbo].[usr_rptSundryDebtors] '01/05/2018', '31/05/2018','1','05','2018'


*/

	SET dateformat dmy
    
	IF @daterange ='2'
	BEGIN
	--set @StartDate =CAST(cast(YEAR(GETDATE()) as varchar(10)) + '-' + @month + '-01' AS DATETIME)	

	--DECLARE @TODEDUCTMONTH VARCHAR(10)
	--SELECT @TODEDUCTMONTH =  MONTH(GETDATE()) - @month
	SET @StartDate =   DATEADD(month,CONVERT(INT,@Month)-1,DATEADD(year,CONVERT(INT,@Year)-1900,0)) --DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) -@TODEDUCTMONTH, 0)	
	SET @EndDate=  DATEADD(day,-1,DATEADD(month,CONVERT(INT,@Month),DATEADD(year,CONVERT(INT,@Year)-1900,0)))--DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@StartDate)+1,0))
	print @StartDate
	print @EndDate
	END

/* lets get the reporting style */
DECLARE @ReportStyle bit = 0

IF ((SELECT V005 FROM [RECORD] WHERE TABLEID = 2743) = 'Yes')
BEGIN
SET @ReportStyle = 1
END

DECLARE @SundryDebtorsTable TABLE
(
	[Counter]       INT IDENTITY(1, 1)
   , [JobRecordID]   INT
   , [Date]     Date
   , [JobNumber]     VARCHAR(MAX)
   , [Description]      VARCHAR(MAX)
   , [Debit]       MONEY
   , [Credit]       MONEY
   , [Balance]       MONEY
   , [Currency]      VARCHAR(MAX)
   , [Group Name]      VARCHAR(MAX)
   , [Beginning Balance]      VARCHAR(MAX)
   , [IsConverted]   VARCHAR(MAX)
);

DECLARE @SundryDebtorsTableBeginningBalance TABLE
(
	[Counter]       INT IDENTITY(1, 1) 
   , [CurrencyOrGroup]      VARCHAR(MAX)
   , [Beginning Balance]      VARCHAR(MAX)
);


DECLARE @SundryDebtorsTableFinal TABLE
(
	[Counter]       INT IDENTITY(1, 1)
   , [JobRecordID]   INT
   , [Date]     Date
   , [JobNumber]     VARCHAR(MAX)
   , [Description]      VARCHAR(MAX)
   , [Debit]       MONEY
   , [Credit]       MONEY
   , [Balance]       MONEY
   , [Currency]      VARCHAR(MAX)
     , [Group Name]      VARCHAR(MAX)
   , [Beginning Balance]      VARCHAR(MAX)
   , [IsConverted]   VARCHAR(MAX)
);


 INSERT INTO @SundryDebtorsTable(
	   JobRecordID
	   ,[Date]
	   ,JobNumber
	   ,[Description]
	   ,[Debit]
	   ,[Credit]
	   ,Currency
	   ,[Group Name]
	   ,IsConverted)
 SELECT [Job Information].RecordID
	   ,[Bill & Payment Activity].V004 AS Date
	   ,[Job Information].[V001] AS JobNumber
	   ,[Bill & Payment Activity].V015 AS Description
	   ,CASE 
		  WHEN [Bill & Payment Activity].V029 IN ('Invoice', 'Interim Invoice')
		  THEN Round(ISNULL(CAST(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V005,0) AS MONEY)),2) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),0),2) 
		  WHEN [Bill & Payment Activity].V029 IN ('Overpaid Entry')  
		  THEN Round(ISNULL(CAST(ABS(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V007,0) AS MONEY)),2)) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),0),2)  
		  ELSE 0
		  END AS [DEBIT]
	   ,CASE 
		  WHEN [Bill & Payment Activity].V029 IN ('Payment Entry', 'Bad Debt Entry')
		  THEN Round(ISNULL(CAST(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V007,0) AS MONEY)),2) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),0),2) 
		  WHEN [Bill & Payment Activity].V029 IN ('Interim Credit', 'Credit Entry')
		  THEN Round(ISNULL(CAST(ABS(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V005,0) AS MONEY)),2)) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),0),2) 
		  ELSE 0 
		  END AS [CREDIT]
	   ,ISNULL([Job Group].V004,'None')
	   ,[Job Group].V001
	   ,CASE WHEN [Job Currency Rates].V001 = [Job Group].V004 
		  THEN NULL 
		  ELSE CONVERT(VARCHAR,'C') 
		  END AS [Converted Symbol]
    FROM [Record] [Bill & Payment Activity] 
    INNER JOIN [Record] [Job Information] ON CAST([Job Information].[RecordID] AS VARCHAR) = [Bill & Payment Activity].[V002]
		  AND [Job Information].[IsActive] = 1
		  AND [Job Information].[TableID] = 2475
		  --and [Job Information].[v027] = '15154977'
		  --AND [Job Information].[V021] NOT IN ('Lost Bid')    
     LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
		  AND [Job Group].IsActive = 1
		  --AND [Job Group].V001 = 'Windtech (HK)'
		  AND CAST( [Job Group].RecordID AS VARCHAR(MAX)) = [Job Information].V027		/*	Currency	*/
	LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
		  AND [Group Currency Rates].IsActive = 1
		  AND CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/
	LEFT JOIN Record [Job Currency Rates] WITH (NOLOCK) on 		[Job Currency Rates].TableID = '2665'		/*	Currency Name	*/
		  AND [Job Currency Rates].IsActive = 1
		  AND CAST( [Job Currency Rates].RecordID AS VARCHAR(MAX)) = [Job Information].V034		/*	[Group Currency]	*/
    WHERE [Bill & Payment Activity].TableID = 2711
		  AND [Bill & Payment Activity].IsActive = 1
		  --AND CONVERT(DATE,[Bill & Payment Activity].V004) <= 'Nov 30 2018 11:59PM'
		  AND CONVERT(DATE,[Bill & Payment Activity].V004,103) <= CONVERT(DATE,@EndDate,103) 
    ORDER BY [Job Group].V001 --[Bill & Payment Activity].RecordID DESC
     
	  --SELECT * FROM @SundryDebtorsTable
   -- WHERE [DATE] < @StartDate
   -- GROUP BY CURRENCY 
   
   IF (@ReportStyle = 0)
   BEGIN
   
    INSERT INTO @SundryDebtorsTableBeginningBalance ([CurrencyOrGroup], [Beginning Balance])
    SELECT Currency,ISNULL(Sum(ISNULL(Debit,0))-SUM(ISNULL(CREDIT,0)),0) AS [Beginning Balance] FROM @SundryDebtorsTable
    WHERE convert(date,[DATE]) < convert(date,@StartDate)
    GROUP BY CURRENCY 
    END
    ELSE 
    BEGIN
      INSERT INTO @SundryDebtorsTableBeginningBalance ([CurrencyOrGroup], [Beginning Balance])
    SELECT [Group Name],ISNULL(Sum(ISNULL(Debit,0))-SUM(ISNULL(CREDIT,0)),0) AS [Beginning Balance] FROM @SundryDebtorsTable
    WHERE convert(date,[DATE]) < convert(date,@StartDate)
    GROUP BY [Group Name] 
    END
       
	  --SELECT [DATE],[Jobnumber],ISNULL(DEBIT,0) AS DEBIT,ISNULL(CREDIT,0) AS CREDIT,CURRENCY,[GROUP NAME] FROM @SundryDebtorsTable
   -- WHERE [DATE] < @StartDate AND [Currency] = 'AUD'
   ---- order by [GROUP nAME]
   ---- GROUP BY CURRENCY 

   --  SELECT  *  FROM @SundryDebtorsTable
    INSERT INTO @SundryDebtorsTableFinal(
    [JobRecordID]    
   , [Date]    
   , [JobNumber]     
   , [Description]      
   , [Debit]   
   , [Credit]        
   , [Balance]        
   , [Currency]      
   ,[Group Name]
   , [Beginning Balance]      
   , [IsConverted]  ) 
    SELECT   [JobRecordID]    
   , [Date]    
   , [JobNumber]     
   , [Description]      
   , [Debit]   
   , [Credit]        
   , [Balance]        
   , [Currency]    
   ,[Group Name]  
   , [Beginning Balance]      
   , [IsConverted]    FROM @SundryDebtorsTable
   WHERE [DATE] >= CONVERT(DATE,@StartDate,103) AND [DATE] <= CONVERT(DATE,@EndDate,103)
   ORDER BY CASE WHEN @ReportStyle = 0 then CURRENCY else [group name] end,[DATE] ASC


     

	--DECLARE @Counter INT = 1
	--WHILE EXISTS(SELECT * FROM @SundryDebtorsTableBeginningBalance WHERE [Counter] >= @Counter )
	--BEGIN
	--   DECLARE @Currency VARCHAR(MAX)
	--   SELECT @Currency 

	--   SET @Counter = @Counter + 1
	--END

	--SELECT * FROM @SundryDebtorsTableBeginningBalance
	 IF (@ReportStyle = 0)
	 BEGIN
	UPDATE SundryDebtorsTableFinal
	SET [Beginning Balance] = SundryDebtorsTableBeginningBalance.[Beginning Balance]
	FROM @SundryDebtorsTableFinal SundryDebtorsTableFinal
	INNER JOIN @SundryDebtorsTableBeginningBalance SundryDebtorsTableBeginningBalance ON 
	   SundryDebtorsTableBeginningBalance.[CurrencyOrGroup] = SundryDebtorsTableFinal.CURRENCY
	   END
	   ELSE
	   BEGIN

	   UPDATE SundryDebtorsTableFinal
	SET [Beginning Balance] = SundryDebtorsTableBeginningBalance.[Beginning Balance]
	FROM @SundryDebtorsTableFinal SundryDebtorsTableFinal
	INNER JOIN @SundryDebtorsTableBeginningBalance SundryDebtorsTableBeginningBalance ON 
	   SundryDebtorsTableBeginningBalance.[CurrencyOrGroup] = SundryDebtorsTableFinal.[GROUP nAME]
	   END


	
	DECLARE @Counter INT = 1
	DECLARE @Currency VARCHAR(MAX) = ''
	DECLARE @Currencyx VARCHAR(MAX) = ''
	DECLARE @BeginningBalance money = 0.00
	DECLARE @Balance money = 0.00
	DECLARE @Debit money = 0.00
	DECLARE @Credit money = 0.00
	WHILE EXISTS(SELECT * FROM @SundryDebtorsTableFinal WHERE [Counter]>= @Counter)
	BEGIN
	 
	   SELECT @BeginningBalance = ISNULL([Beginning Balance],0), @Currency =  CASE   WHEN @ReportStyle = 0 then [Currency] ELSE [GROUP NAME] END, @Debit = Debit,@Credit= Credit FROM @SundryDebtorsTableFinal WHERE [Counter] = @Counter
	   print 'currencyx :' + @Currencyx
	   print 'currencyx :' + @Currency
	   print 'begbal :' + cast(@BeginningBalance as varchar)
	   print 'debit :' + cast(@Debit as varchar)
	   print 'creidt :' + cast(@Credit as varchar)
	     IF (@Currencyx = @Currency)
		BEGIN
		SET @Balance = @Balance + @Debit - @Credit
		UPDATE @SundryDebtorsTableFinal
		SET Balance =  @Balance
		WHERE [Counter] = @Counter
		END
		ELSE 
		BEGIN
		SET @Balance = @BeginningBalance + @Debit - @Credit
		UPDATE @SundryDebtorsTableFinal
		SET Balance =  @Balance
		WHERE [Counter] = @Counter
		END
	   
	  
	   SET @Currencyx = @Currency
	   SET @Counter = @Counter + 1
	END

	


--SELECT V039 AS [Header] FROM RECORD WHERE TABLEID = 2743

	SELECT [Counter],
	JobRecordID,
	[Date],
	JobNumber,
	Description,
	ISNULL(Debit,0) AS Debit,
	ISNULL(Credit,0) AS Credit,
	ISNULL(Balance,0) AS Balance,
	Currency,
	[Group Name],
	ISNULL([Beginning Balance],0) AS [Beginning Balance], 
	IsConverted,
	CONVERT(DATE,@StartDate) AS StartDate,
	CONVERT(DATE,@EndDate) AS EndDate, 
	@ReportStyle AS [Report Style], 
	(SELECT V039  FROM RECORD WHERE TABLEID = 2743) AS [Header]
	FROM @SundryDebtorsTableFinal
	ORDER BY CURRENCY,[DATE]
END