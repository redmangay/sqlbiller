﻿

CREATE PROCEDURE [dbo].[ets_Copy_Conditions]
(
	@OldTableID int,
	@NewTableID int
)
AS
BEGIN TRY
	-- Logic:
	-- 1. Find out the name of Old.ColumnID
	-- 2. Find out the name of Old.HideColumID
	-- 3. Find out the new ColumnID using the name from 1 in the new table
	-- 4. Find out the new ColumnID using the name from 2 in the new table
	-- Create the new rows

	-- Created by Jon Bosker on 30 March 2017 

	DECLARE @SW TABLE
	(
		[Counter] int identity(1,1),
		OldColumnID int,
		OldConditionColumnID int, 
		[ConditionValue] nvarchar(250),
		[ConditionOperator] varchar(25),
		[DisplayOrder] int,
		[JoinOperator] varchar(10),
		ColumnID_SystemName varchar(10),
		ConditionColumnID_SystemName varchar(10),
		NewColumnID int,
		NewConditionColumnID int
	)

	INSERT INTO @SW (OldColumnID, OldConditionColumnID, ColumnID_SystemName, [ConditionValue], [ConditionOperator], [DisplayOrder], [JoinOperator])
		SELECT SW.ColumnID, SW.ConditionColumnID, C.SystemName, SW.[ConditionValue], SW.[ConditionOperator], SW.[DisplayOrder], SW.[JoinOperator]
			FROM [Conditions] SW
			JOIN  [Column] C ON C.ColumnID = SW.ColumnID 
			WHERE [TableID] = @OldTableID  -- 146 rows


	UPDATE SW SET ConditionColumnID_SystemName = C.SystemName 
		FROM @SW SW 
		JOIN [Column] C ON C.ColumnID = SW.OldConditionColumnID

	UPDATE SW SET NewColumnID = C.ColumnID 
		FROM @SW SW 
		JOIN [Column] C ON C.TableID = @NewTableID AND C.SystemName = SW.ColumnID_SystemName

	UPDATE SW SET NewConditionColumnID = C.ColumnID 
		FROM @SW SW 
		JOIN [Column] C ON C.TableID = @NewTableID AND C.SystemName = SW.ConditionColumnID_SystemName

	SELECT * FROM @SW


	SELECT SW.*
		FROM [Conditions] SW
		JOIN  [Column] C ON C.ColumnID = SW.ColumnID 
		WHERE [TableID] = 2395  -- 146 rows

	INSERT INTO [Conditions] (ColumnID, ConditionColumnID, ConditionValue, ConditionOperator, DisplayOrder, JoinOperator)
		SELECT NewColumnID, NewConditionColumnID, ConditionValue, ConditionOperator, DisplayOrder, JoinOperator
			FROM @SW

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Copy_Conditions', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
