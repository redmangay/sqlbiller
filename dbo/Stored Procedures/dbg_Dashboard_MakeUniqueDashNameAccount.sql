﻿ 

CREATE PROCEDURE [dbo].[dbg_Dashboard_MakeUniqueDashNameAccount]

	/*----------------------------------------------------------------
	
SELECT AccountID, DocumentText, count(*) as qty 
 FROM [Document] WHERE ForDashBoard=1
 GROUP BY AccountID, DocumentText HAVING count(*)> 1


EXEC [dbo].[dbg_Dashboard_MakeUniqueDashNameAccount]
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
  
	DECLARE Cur_UniqueDashNameAccountID CURSOR FOR
	SELECT AccountID, DocumentText, count(*) as qty 
	 FROM [Document] WHERE ForDashBoard=1
	 GROUP BY AccountID, DocumentText HAVING count(*)> 1
 
	DECLARE @nAccountID INT;
	DECLARE @sDocumentText varchar(250);
	DECLARE @nQty INT;
	DECLARE @sNewDocumentText varchar(250);

	OPEN Cur_UniqueDashNameAccountID;
	FETCH NEXT FROM Cur_UniqueDashNameAccountID INTO @nAccountID,@sDocumentText,@nQty;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
	  
	  
			DECLARE Cur_DocumentID CURSOR FOR
			SELECT DocumentID FROM [Document] WHERE ForDashBoard=1 AND AccountID=@nAccountID AND DocumentText=@sDocumentText;
			DECLARE @nDocumentID INT;
		
			OPEN Cur_DocumentID;
			FETCH NEXT FROM Cur_DocumentID INTO @nDocumentID;
			WHILE @@FETCH_STATUS = 0
			   BEGIN
			  
				  SELECT @sNewDocumentText=dbo.fnNextAvailableDashBoardName(@sDocumentText,@nAccountID,@nDocumentID);
			  
				  IF @sDocumentText<>@sNewDocumentText
					  UPDATE [Document]
					  SET DocumentText=@sNewDocumentText
					  WHERE DocumentID=@nDocumentID
			 			  
				  FETCH NEXT FROM Cur_DocumentID INTO @nDocumentID;
			   END;
			CLOSE Cur_DocumentID;
			DEALLOCATE Cur_DocumentID;  
	 
		  FETCH NEXT FROM Cur_UniqueDashNameAccountID INTO @nAccountID,@sDocumentText,@nQty;
	   END;
	CLOSE Cur_UniqueDashNameAccountID;
	DEALLOCATE Cur_UniqueDashNameAccountID;

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_Dashboard_MakeUniqueDashNameAccount', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
