﻿

CREATE PROCEDURE [dbo].[Renzo_PrintStatement]
(
	@ReportType  int = 2,
	@RecordID  int   
)
AS

--DECLARE @RecordIDx int = 2709836
 /*
 
    exec [dbo].[Renzo_PrintStatement] @RecordID =  16702755
 
 */


 DECLARE @JobInformationRecordID int

 SELECT @JobInformationRecordID = V002 FROM RECORD WHERE RECORDID = @RecordID


 DECLARE @Header varchar(max) = '(SELECT OptionValue FROM SYSTEMOPTION WHERE OPTIONKEY = ''FilesLocation'') + ''/UserFiles/AppFiles/'' + [Document Layout].V039  AS HeaderImage'

  IF @ReportType = 2
  BEGIN
  SET @Header = '(SELECT OptionValue FROM SYSTEMOPTION WHERE OPTIONKEY = ''FilesLocation'') + ''/UserFiles/AppFiles/'' + [Document Layout].V041  AS HeaderImage'
  END

 DECLARE @SQL varchar(max) = ''

 SET @SQL = 'SET DATEFORMAT dmy
 SELECT 
		cast([Bill & Payment Activity].[V004] as date) AS [Date]
		,[Job Information].[V001] AS [Job Number]
		,[Bill & Payment Activity].[V017] AS [Invoice No]
		,[Client Information].[V017] as [Client Name]  --[Office Information].[V001] AS [Client Name]
		,[Client Information].[V002] AS [Client Address1]
		,isnull([Client Information].[V015],'''') AS [Client Address2]
		,[Client Information].[V003] AS [Client Suburb]
		,CASE WHEN [Client Information].[V004] IS NULL THEN '''' ELSE [Client Information].[V004] + '' '' +  [Client Information].[V005] END AS [Client State]
		,[Client Information].[V006] AS [Country]
		,[Client Information].V019 as  [Attention Name]  --[Client Contact].V004 + '' '' + [Client Contact].V005 + '' '' + [Client Contact].V006 AS [Attention Name]
		,[Job Information].[V003] AS [Job Title]
		,ISNULL([Tax Table].[V001],'''') AS [Tax Name]
		,CAST(ISNULL([Bill & Payment Activity].[V005],0) as money)  AS [Invoice Amount]
		,CASE WHEN [Bill & Payment Activity].[V029] IN (''Bad Debt Entry'') THEN '''' ELSE cast(ISNULL([Bill & Payment Activity].[V007],0) as money) END AS [Payment Amount]
		,ISNULL(cast(ISNULL([Job Information].[V008],0.00) as money),''0.00'') AS [Job Value]
		,ISNULL([Job Currency].[V001],'''') AS [Job Currency]
		,		
		CASE WHEN ISNULL([Job Currency].[V001],'''') = ''(None)'' THEN '''' 
		 ELSE 
		ISNULL([Job Currency].[V003],ISNULL([Job Currency].[V001],'''')) END AS [Currency Symbol]
		,CASE WHEN [Job Information].[V009] = ''FALSE'' THEN ''Estimate'' ELSE '''' END AS [Estimate]
		--,CAST(ISNULL([Previous Bill & Payment Activity].[V005],0.00) as money) AS [Previous Invoice Amount]
		,CAST([Bill & Payment Activity].[V063] as money) AS [GST]
		,[Bill & Payment Activity].[V015] AS [Description]

		,/*CASE WHEN LEN(LTRIM(RTRIM([Bill & Payment Activity].[V028]))) = 0 THEN 0 ELSE */ 
		CASE  
			 WHEN CAST([Bill & Payment Activity].[V028] AS MONEY) = '''' THEN ''0'' ELSE
			 ISNULL([Bill & Payment Activity].[V028],''0'') 
		END
		
		AS [Percentage Complete]
		
		
		,CASE WHEN '+ CAST(@ReportType AS VARCHAR) +' = 2 THEN [Document Layout].V017 ELSE 		
		 CASE WHEN [Bill & Payment Activity].[V029] IN (''Invoice'') THEN [Document Layout].V015 ELSE [Document Layout].V011 END END AS Notes
		, [Job Group].V001 AS [Group]
		--,[Client Information].[V001]
		--,CAST([Job Information].[RecordID] AS VARCHAR)
		--,[Client Information].[V013]

		  ,CASE WHEN [Job Information].[V009] = ''TRUE'' THEN ''% Complete'' ELSE '''' END AS [% Complete],
		   CASE WHEN [Job Information].[V009] = ''TRUE'' THEN convert(money,ISNULL([Bill & Payment Activity].V028,0)) / cast(''100.00'' as money) ELSE '''' END AS [% Complete Value]
 , '+ @Header +'
 FROM [Record] [Bill & Payment Activity] WITH (NOLOCK) 
 JOIN [Record] [Job Information] ON [Job Information].[TableID] = 2475
			   AND [Job Information].[RecordID] = CAST([Bill & Payment Activity].[V002] as int)
			   AND [Job Information].[IsActive] = 1
			   AND [Job Information].[RecordID] = '+ CAST(@JobInformationRecordID AS VARCHAR) +'
			   
 --LEFT JOIN [Record] [Office Information] ON [Office Information].[TableID] = 2474
	--		   AND [Office Information].[RecordID] = CAST([Job Information].[V024] as int) 
	--		   AND [Office Information].[IsActive] = 1

 LEFT JOIN [Record] [Client Information] ON [Client Information].[TableID] = 2476
			   AND CAST([Client Information].[V013] as int) = [Job Information].[RecordID]
			   AND [Client Information].[IsActive] = 1

			   
 LEFT JOIN [Record] [Job Currency] ON [Job Currency].[TableID] = 2665
			   AND [Job Currency].[RecordID] = CAST([Job Information].[V034] as int) 
			   AND [Job Currency].[IsActive] = 1

 LEFT JOIN [Record] [Tax Table]	ON	1=1
				AND		[Tax Table].[RecordID] = CAST([Job Information].V042 as int)	
				AND		[Tax Table].TableID = 2696		/*	Tax Rates	*/
				AND		[Tax Table].IsActive = 1

 LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
				AND			[Job Group].IsActive = 1
				AND		[Job Group].RecordID = CAST([Job Information].V027 as int)		/*	Group	*/

LEFT JOIN Record [Document Layout] WITH (NOLOCK) ON [Document Layout].TableID = 2744	/*	Document Layout	*/
				AND			[Document Layout].IsActive = 1
				AND		CAST([Document Layout].V005 as int) = [Job Group].RecordID		/*	Group	*/

LEFT JOIN Record [Client Contact] WITH (NOLOCK) ON [Client Contact].TableID = 2657	/*	Document Layout	*/
				AND			[Client Contact].IsActive = 1
				AND		[Client Contact].RecordID = CAST([Client Information].V008 as int)		/*	Group	*/
			 								
 WHERE [Bill & Payment Activity].[TableID] = 2711	AND
	   [Bill & Payment Activity].[IsActive] = 1 
	  --and [Job Information].[V001] =  ''TD029-60''
	  -- AND [Bill & Payment Activity].[V002] = ''2705119''
	  	  
	  ORDER BY cast([Bill & Payment Activity].[V004] as date)' -- end quote

	  


	  EXEC(@SQL);
	  PRINT @SQL


	  ----------------
	 
	  ----------------

RETURN @@ERROR;