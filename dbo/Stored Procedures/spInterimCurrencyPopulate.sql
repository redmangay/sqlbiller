﻿-- =============================================
-- Author:		<Barry Matutina>
-- Create date: <09/03/2016>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE spInterimCurrencyPopulate 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT			[Interim Invoice Entry].[TableID]
				,[Interim Invoice Entry].RecordID	AS				[Record ID]
				,[Interim Invoice Entry].V001		AS				[Job Number]
				,[Interim Invoice Entry].V002		AS				[Date]
				,[Interim Invoice Entry].V003		AS				[Invoice Number]
				,[Interim Invoice Entry].V004		AS				[Invoice Amount]
				,[Interim Invoice Entry].V005		AS				[Description]
				,[Interim Invoice Entry].DateAdded
	FROM		[Record] [Interim Invoice Entry] WITH (NOLOCK)
	LEFT JOIN	Record [Invoice Number] ON					[Invoice Number].TableID = '2486'		/*	zOld Bill & Payment Activity	*/
											AND		[Invoice Number].IsActive = 1
											AND		CAST( [Invoice Number].RecordID AS VARCHAR(MAX)) = [Interim Invoice Entry].V003		/*	Invoice Number	*/
	LEFT JOIN	Record [Job Number] ON					[Job Number].TableID = '2486'		/*	zOld Bill & Payment Activity	*/
											AND		[Job Number].IsActive = 1
											AND		CAST( [Job Number].RecordID AS VARCHAR(MAX)) = [Interim Invoice Entry].V001		/*	Job Number	*/
	WHERE		[Interim Invoice Entry].TableID = '2481'		/*	Interim Invoice Entry	*/
	AND			[Interim Invoice Entry].IsActive = 1
END
