﻿
 
CREATE PROCEDURE [dbo].[dbg_Fix_MenuNoneIssue]
	/*----------------------------------------------------------------
	
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	DECLARE C_NoneAccount CURSOR FOR
	SELECT AccountID FROM Account WHERE AccountID NOT IN (SELECT DISTINCT AccountID FROM Menu WHERE IsActive=1 
	and Menu='--None--' and ParentMenuID is NULL);

	DECLARE @nAccountID INT;
	DECLARE @nCounter INT=0;
	OPEN C_NoneAccount;
	FETCH NEXT FROM C_NoneAccount INTO @nAccountID;
	WHILE @@FETCH_STATUS = 0
	   BEGIN	  
	  
		  EXEC ets_Menu_Insert NULL,'--None--',@nAccountID,0,1,NULL,NULL,NULL,NULL,NULL
		  FETCH NEXT FROM C_NoneAccount INTO @nAccountID;	
	          
	   END;
	CLOSE C_NoneAccount;
	DEALLOCATE C_NoneAccount;
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_Fix_MenuNoneIssue', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
