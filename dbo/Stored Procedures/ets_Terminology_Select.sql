﻿

CREATE PROCEDURE [dbo].[ets_Terminology_Select]
(
	@nAccountID int  = NULL,
	@sPageName varchar(300)  = NULL,
	@sInputText varchar(MAX)  = NULL,
	@sOutputText varchar(MAX)  = NULL,
	@sOrder nvarchar(200) = TerminologyID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------

	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(4000)
	DECLARE @sWhere nvarchar(4000)
	DECLARE @sSelectCount nvarchar(MAX)

	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND AccountID = '+CAST(@nAccountID AS NVARCHAR)

	IF @nAccountID IS  NULL 
		SET @sWhere = @sWhere + ' AND AccountID  IS NULL '
	
	IF @sPageName IS NOT NULL 
		SET @sWhere = @sWhere + ' AND PageName = '+'''' + @sPageName + ''''
	
	
	IF @sInputText IS NOT NULL 
		SET @sWhere = @sWhere + ' AND InputText LIKE '+'''%' + @sInputText + '%'''
	IF @sOutputText IS NOT NULL 
		SET @sWhere = @sWhere + ' AND OutputText LIKE '+'''%' + @sOutputText + '%'''
	SET @sSelect = 'SELECT * FROM 
	(SELECT [Terminology].*, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum FROM [Terminology]' + @sWhere + ') as UserInfo'
	+ ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow) 

 
	SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [Terminology] ' + @sWhere 


	EXEC (@sSelect)
	SET ROWCOUNT 0

	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Terminology_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
