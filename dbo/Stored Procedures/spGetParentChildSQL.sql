﻿


-----------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[spGetParentChildSQL]
(
 @nParentTableID int,
 @nChildTableID int
)
AS

BEGIN TRY 
 SELECT Parent.TableID as [ParentTableID] 
  ,Child.TableID as [ChildTableID]
  ,Child.[ColumnID] as ChildColumnID
  ,Parent.ColumnID AS	ParentColumnID
  ,Parent.SystemName as ParentCol
  ,Child.SystemName as ChildCol
  ,'FROM [Record] Parent JOIN [Record] Child ON Child.TableID = ' + CAST(Child.TableID as varchar)
   + ' AND Parent.' +  Parent.SystemName + ' = ' + 'Child.' + Child.SystemName AS	[FromClause]
  ,'WHERE Parent.TableID = ' + CAST(Parent.TableID as varchar) AS [WhereClause]
  FROM [Column] Parent 
  JOIN [Column] Child ON Child.TableID = @nChildTableID
  WHERE Parent.TableID = @nParentTableID
  AND Parent.[ColumnID] = Child.[LinkedParentColumnID]
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spGetParentChildSQL', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
