﻿CREATE PROCEDURE [dbo].[Renzo_PrintInvoiced_BilledDetails]
(
	@RecordID  int   		
	
)
AS

/*

EXEC [Renzo_PrintInvoiced_BilledDetails]  15244439


*/

BEGIN
 SET DATEFORMAT dmy
   DECLARE @TaxAmount money
   DECLARE @interimTaxAmount money
      /*Billed Details tab: Get the Interim Tax Amount */
	  SELECT @interimTaxAmount =  CAST(CONVERT(VARCHAR(20),ISNULL(V063,0),1) AS money)  FROM Record WHERE ISACTIVE=1 AND TableID='2711' AND V061= CAST(@RecordID as varchar)
	   /*Billed Details tab: Get the Tax Amount */
	  SELECT @TaxAmount =  ISNULL(CAST(CONVERT(VARCHAR(20),V063,1) AS money),0)  FROM Record WHERE ISACTIVE=1 AND TableID='2711' AND RECORDID= @RecordID 
	  SET @TaxAmount = @TaxAmount +  ISNULL(@interimTaxAmount,0)


 
 SELECT	  [Job Information].V009 as [Fixed Value],
		  [Job Information].V001 as [Job No],
		  [Job Information].V003 as [Job Title],
		  [Timesheet Detail].V009 as [Date],
		  [EmployeeList].V004 as [Initial],
		  [Timesheet Detail].V003 as [Details],
		  [Timesheet Detail].V004 as [X], 
		  [Timesheet Detail].V005 as [HrUnit], 
		  CAST([Timesheet Detail].V006 AS money) as [Rate] ,
		  CAST([Timesheet Detail].V007 AS money) as [Amount],
		  @TaxAmount as [Tax] 
	FROM Record [Timesheet Detail]
	    JOIN Record [Job Information] ON [Job Information].TableID = 2475
	    AND [Job Information].IsActive = 1
	    AND [Job Information].RecordID = [Timesheet Detail].V001
	    JOIN Record [EmployeeList] ON [EmployeeList].TableID = 2669
	    AND [EmployeeList].IsActive = 1
	    AND [EmployeeList].RecordID = [Timesheet Detail].V002
	WHERE [Timesheet Detail].TableID = 2729
	AND [Timesheet Detail].IsActive = 1
	AND [Timesheet Detail].V016 = @RecordID
	ORDER BY CAST([Timesheet Detail].V009 AS DATE) ASC

RETURN @@ERROR;

END