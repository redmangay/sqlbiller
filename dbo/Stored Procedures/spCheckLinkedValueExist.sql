﻿

CREATE PROCEDURE [dbo].[spCheckLinkedValueExist]
(
	@nBatchID int,
	@sSystemName varchar(10),
	@nTableTableID INT,
	@sParentSystemName varchar(10)
)
	/*----------------------------------------------------------------

	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	DECLARE @nTableID INT
	DECLARE @sDisplayName varchar(100)
	DECLARE @sSQL1 varchar(1000)
	DECLARE @sSQL2 varchar(1000)

	SELECT @nTableID=TableID FROM Batch WHERE BatchID=@nBatchID
	SELECT @sDisplayName=DisplayName FROM [Column] WHERE TableID=@nTableID AND SystemName=@sSystemName



	SELECT @sSQL1='UPDATE TempRecord
	SET RejectReason=ISNULL(RejectReason,'''')+'' '' + '''+@sDisplayName+'(''+'+@sSystemName+'+'') not found.'' WHERE BatchID='+CAST(@nBatchID AS varchar)+' AND '+@sSystemName+' IS NOT NULL AND RecordID NOT IN
	(SELECT TR.RecordID FROM TempRecord TR
	INNER JOIN Record R
	ON TR.'+@sSystemName+'=R.'+@sParentSystemName+'
	WHERE R.TableID='+CAST(@nTableTableID AS varchar)+' AND R.IsActive=1 and TR.'+@sSystemName+'=R.'+@sParentSystemName+'
	AND TR.BatchID='+CAST(@nBatchID AS varchar)+' AND TR.'+@sSystemName+' IS NOT NULL)'


	EXEC (@sSQL1)

	SELECT @sSQL2='UPDATE TR
	SET TR.'+@sSystemName+'=R.RecordID
	FROM TempRecord TR
	INNER JOIN Record R
	ON TR.'+@sSystemName+'=R.'+@sParentSystemName+'
	WHERE R.TableID='+CAST(@nTableTableID AS varchar)+' AND R.IsActive=1
	AND TR.BatchID='+CAST(@nBatchID AS varchar)+' AND TR.'+@sSystemName+' IS NOT NULL'

	EXEC (@sSQL2)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spCheckLinkedValueExist', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
