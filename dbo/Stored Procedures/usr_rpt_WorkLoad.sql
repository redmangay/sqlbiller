﻿CREATE proc [dbo].[usr_rpt_WorkLoad]
(
@Group varchar(max) =null,
@Controller varchar(max) =null,
@Flag  int=null,
@From date=null,
@To date=null,
@Group_Controller int =1



)
as


exec usr_rpt_CurrentJobListGroupCurrency

SELECT NEWID() as Guid,[Group/Division] AS GroupDivision, Name, Approved, Invoiced, Balanced
into #rptWorkLoad
 FROM ##CurrentJobListGroupCurrency
 --where (((v_CurrentJobListGroupCurrency.[Group/Division] =@group) or ( @group ='ALL')) and ((v_CurrentJobListGroupCurrency.Name = @controller )or (@controller='All') ))
 
 
 


 
if @flag =1
begin

 
 SELECT  Sum(rptCurrentJobsList.Approved) AS JobApproved,
  Sum(rptCurrentJobsList.Invoiced) AS JobInvoiced,
   Sum(rptCurrentJobsList.Balanced) AS JobBalanced, 
   Count(*) AS Jobs, 
   rptCurrentJobsList.GroupDivision,
   rptCurrentJobsList.Guid,
   
    rptCurrentJobsList.Name as Controller, 
   isnull(ReportingCurrency,'') AS GroupCurrency
FROM  #rptWorkLoad rptCurrentJobsList LEFT JOIN   Account24918.vList_Of_Group_Division [List_Of_Group/Division] ON rptCurrentJobsList.GroupDivision = [List_Of_Group/Division].[Group/Division]
where (((rptCurrentJobsList.GroupDivision =@group) or ( @group in('ALL','NULL'))) and ((rptCurrentJobsList.Name = @controller )or (@controller='NULL') ))
GROUP BY rptCurrentJobsList.GroupDivision,  rptCurrentJobsList.Name, 
isnull(ReportingCurrency,''),rptCurrentJobsList.Guid
ORDER BY rptCurrentJobsList.GroupDivision;
end

if @flag =2
begin

if @Group_Controller ='2'
begin
SELECT DISTINCT rptWorkLoad.GroupDivision, 1 AS id 	FROM  #rptWorkLoad rptWorkLoad 

end
	
else if @Group_Controller ='3'	
  begin
	SELECT        'NULL' AS GroupDivision, 0 AS id 	ORDER BY id, GroupDivision
	end
	else
	SELECT        'ALL' AS GroupDivision, 0 AS id 	ORDER BY id, GroupDivision

end

if @flag =3
begin


if @Group_Controller =3
begin
 SELECT DISTINCT rptWorkLoad.Name as Controller, 1 AS id
FROM  #rptWorkLoad rptWorkLoad

end
else
begin
SELECT        'NULL' AS Controller, 0 AS id
ORDER BY id, Controller
end

end


           
