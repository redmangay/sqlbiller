﻿

CREATE PROCEDURE [dbo].[ReportParameters]
    
    	@nRecordID int
AS

BEGIN TRY

/* 

exec [dbo].[ReportParameters] 1821173

*/
	
	DECLARE @TableID int 
	SELECT @TableID = TableID FROM Record WHERE RecordID =  @nRecordID

    DECLARE @Queue TABLE
	   (
		  [Counter] int identity(1,1),
		  ColumnName varchar(30),
		  DisplayName varchar(max)
	   )

  DECLARE @Counter int= 1

  INSERT INTO @Queue (ColumnName,DisplayName)
	   SELECT SystemName,DisplayName
	   FROM [Column]    
	   WHERE [TableID] = @TableID -- THE CURRENT TABLE
	   AND DisplayTextDetail IS NOT NULL

	   DECLARE @sSQL varchar(MAX) = 'SELECT
	   '
	   WHILE EXISTS(SELECT * FROM @Queue WHERE [Counter] >= @Counter)
		  BEGIN
		  SELECT @sSQL = @sSQL +  ' Record.' + ColumnName  + ' as [' + Replace(DisplayName,' ','_') + '],'
		  FROM @Queue WHERE [Counter] = @Counter

		  SET @Counter = @Counter + 1
	   END

	   SET @sSQL = @sSQL + 'XX'
	   SELECT @sSQL = REPLACE(@sSQL, ',XX','')

	   SELECT @sSQL = @sSQL +
		'
	   FROM [Record] Record	
	   WHERE Record.RecordID = '+ CAST(@nRecordID AS varchar(MAX)) + '
	  '

	   PRINT @sSQL
	   EXEC(@sSQL);


END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog]([Module], [ErrorMessage], [ErrorTrack], [ErrorTime], [Path]) 
		VALUES ('ReportParameters', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())

END CATCH

