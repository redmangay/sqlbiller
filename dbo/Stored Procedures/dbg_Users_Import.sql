﻿
 
CREATE PROCEDURE [dbo].[dbg_Users_Import]
(
	@nBatchID INT,@nNumberImported INT out
)
	/*----------------------------------------------------------------
	DECLARE @nNumberImported int
	EXEC [dbg_Users_Import] 3425, @nNumberImported out
	PRINT cast(@nNumberImported as varchar)
	---------------------------------------------------------------*/
 
AS
 
BEGIN TRY

	BEGIN TRANSACTION --beginning a transaction..

	DECLARE @nAccountID INT

	SELECT @nAccountID=AccountID FROM Batch WHERE BatchID=@nBatchID

	DECLARE Records_Cursor CURSOR FOR
		SELECT V001,V002,V003,V004,V005,V006 
			FROM Record 
			WHERE IsActive=1 AND V001 IS NOT NULL
			AND V002 IS NOT NULL AND V003 IS NOT NULL AND V005 IS NOT NULL
			AND Record.BatchID=@nBatchID

	DECLARE @sV001 varchar(MAX),@sV002 varchar(MAX),@sV003 varchar(MAX),
		@sV004 varchar(MAX),@sV005 varchar(MAX),@sV006 varchar(MAX);

	DECLARE @nCounter INT=0;

	OPEN Records_Cursor;
	FETCH NEXT FROM Records_Cursor INTO @sV001,@sV002,@sV003,@sV004,@sV005,@sV006;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
		  DECLARE @nRoleID INT =NULL
		  DECLARE @bIsSystemRole bit=NULL
		  DECLARE @bIsAdvancedSecurity bit=1
		  -- Process User Role
		  IF @sV006 IS NOT NULL AND LEN(@sV006)>0
			SELECT @nRoleID =RoleID,@bIsSystemRole=IsSystemRole FROM [Role] WHERE [Role]=@sV006 AND AccountID=@nAccountID
	
		  IF @bIsSystemRole IS NOT NULL AND @bIsSystemRole=1
			  SET @bIsAdvancedSecurity=0
		  
		  IF @nRoleID IS NULL
			SELECT @nRoleID =RoleID FROM [Role] WHERE [Role]='Read Only' AND AccountID=@nAccountID
		  
		  DECLARE @sEmailUX nvarchar(200)=NULL
		  SELECT @sEmailUX=Email FROM [User] WHERE Email=@sV003
		
			IF @nRoleID IS NOT NULL AND @sEmailUX IS NULL
				BEGIN
					INSERT INTO [User]( FirstName,LastName,Email,PhoneNumber --,[Password] -- Red removed this for Ticket 3059 (please see comment below for EXEC User_UpdatePassword)
					,IsActive) 
						VALUES (@sV001,@sV002,@sV003,@sV004 --, @sV005 -- Red removed this for Ticket 3059 (please see comment below for EXEC User_UpdatePassword)
						,1)
			
					DECLARE @nUserID INT
					SELECT @nUserID=@@IDENTITY

					-- UPDATE New user Password
					EXEC [User_UpdatePassword] @nUserID,@sV005 -- Red added this - Ticket 3059 

				
					INSERT INTO [UserRole](UserID,RoleID,AccountID,IsPrimaryAccount,IsAccountHolder,IsAdvancedSecurity)
						VALUES(@nUserID,@nRoleID,@nAccountID,1,0,@bIsAdvancedSecurity)	
					
					SET @nCounter=@nCounter+1		
			
				END
	  
	  
		  FETCH NEXT FROM Records_Cursor INTO @sV001,@sV002,@sV003,@sV004,@sV005,@sV006;
	   END;
	CLOSE Records_Cursor;
	DEALLOCATE Records_Cursor;

	SELECT @nNumberImported=@nCounter


	COMMIT TRANSACTION --finally, Commit the transaction as all Success.

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_Users_Import', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
