﻿

CREATE PROCEDURE [dbo].[ets_Column_Update]
(
	@nColumnID int ,
	@nTableID int ,
	@sSystemName varchar(50) ,
	@nDisplayOrder int ,
	@sDisplayTextSummary varchar(100)  = NULL,
	@sDisplayTextDetail varchar(255)  = NULL,
	@nGraphTypeID int  = NULL,
	@sValidationOnWarning nvarchar(4000)  = NULL,
	@sValidationOnEntry nvarchar(4000)  = NULL,
	@bIsStandard bit =0,
	@sDisplayName varchar(100),
	@sConstant varchar(MAX)=NULL,
	@sCalculation varchar(4000)=NULL,
	@bShowTotal bit=0,
	@bIgnoreSymbols bit=0,
	@nIgnoreSymbolMode int = NULL,
	@nIgnoreSymbolConstant decimal(18, 10) = NULL,
	@sNotes nvarchar(MAX)=NULL,
	@bIsRound bit=0,
	@nRoundNumber int=2,
	@bCheckUnlikelyValue bit=0,
	@sGraphLabel varchar(100)=NULL,
	@nLastUpdatedUserID int =NULL,
	@sDropdownValues varchar(MAX)=NULL,
	@sImportance char(1)=NULL,
	@sAlignment varchar(25)=NULL,
	@nNumberType int=NULL,
	@sDefaultValue varchar(MAX)=NULL,
	@nAvgColumnID int =NULL,
	@nAvgNumberOfRecords int=NULL,
	@dShowGraphExceedance decimal(18, 10)=NULL,
	@dShowGraphWarning decimal(18, 10)= NULL,
	@nFlatLineNumber int=NULL,
	@dMaxValueAt decimal(18, 10)= NULL,
	@nDefaultGraphDefinitionID int= NULL,
	@nTextWidth int=NULL,
	@nTextHeight int=NULL,
	@sColumnType varchar(15)='text',
	@sDropDownType varchar(15)=NULL,
	@nTableTableID int=NULL,
	@sDisplayColumn varchar(MAX)= NULL,
	@bDisplayRight bit=0,
	@sSummaryCellBackColor varchar(50)=NULL,
	@nFormVerticalPosition int=NULL,
	@nFormHorizontalPosition int=NULL,
	@nParentColumnID int=NULL,
	@sTextType varchar(25) =NULL,
	@sRegEx varchar(MAX)=NULL,
	@sDateCalculationType varchar(15)=NULL,
	@nLinkedParentColumnID int=NULL,
	@nDataRetrieverID int=NULL,
	@bVerticalList bit= NULL,
	@bQuickAddLink bit =NULL,
	@bCalculationIsActive bit =NULL,
	@nTableTabID int=NULL,
	@sViewName varchar(50)= NULL,
	@sDefaultType varchar(50)=NULL,
	@nDefaultColumnID int=NULL,
	@sShowViewLink varchar(10)=NULL,
	@nFilterParentColumnID int=NULL,
	@nFilterOtherColumnID int=NULL,
	@sFilterValue varchar(MAX)=NULL,
	@nMapPinHoverColumnID int=NULL,
	@nCompareColumnID int=NULL,
	@sCompareOperator varchar(50)=NULL,
	@sMapPopup varchar(MAX)= NULL,
	@nTrafficLightColumnID INT=NULL,
	@sTrafficLightValues varchar(MAX)= NULL,
	@nDefaultRelatedTableID int=NULL,
	@bDefaultUpdateValues bit=NULL,
	@bValidationCanIgnore bit=NULL,
	@bImageOnSummary bit=NULL,
	@bAllowCopy bit=NULL,
	@sValidationOnExceedance nvarchar(4000)  = NULL,
	@bColourCells bit =NULL,
	@sButtonInfo varchar(1000)= NULL,
	@sFilterOperator varchar(25)=NULL,
	@bIsReadOnly bit=NULL,
	@dShowGraphWarningMin decimal(18, 10)= NULL,
	@dShowGraphExceedanceMin decimal(18, 10)=NULL,
	@bHideTimeSecond bit = NULL,
	@ColSpan tinyint =NULL,@RowSpan tinyint =NULL,
	@nFilterParentColumnID2 int=NULL,
	@nFilterOtherColumnID2 int=NULL,
	@sFilterValue2 varchar(MAX)=NULL,
	@sFilterOperator2 varchar(25)=NULL,
	/*RP Added Ticket 4310*/
	@ShowAllValuesOnSearch bit=NULL,
	/*End Modification*/
	@bCalculationNullAsZero bit = NULL,@nChildTableID  int=null,@bEditableOnSummary bit=null,
	@sRolesWhoCanIgnoreValidation varchar(MAX)=NULL,
	@sSqlRetrieval varchar(MAX)=NULL,
	@nTriggerColumnID int=NULL,
	@sSQLRetrievalOption varchar(MAX)=NULL
)
	/*----------------------------------------------------------------
	-- Stored Procedure: ets_Column_Update
	-- Produced by: DB Gurus (SPGen) 
	-- Date: Dec  3 2010  6:34PM
	---------------------------------------------------------------*/
 
AS
BEGIN TRY
	DECLARE @nTableTabID_old int
	SELECT @nTableTabID_old=TableTabID FROM [Column] WHERE ColumnID=@nColumnID

	UPDATE [Column]
	SET 
		TableID = @nTableID,
		SystemName = @sSystemName ,
		DisplayOrder = @nDisplayOrder,
		DisplayTextSummary = replace(replace(@sDisplayTextSummary,'[','('),']',')'),
		DisplayTextDetail = @sDisplayTextDetail,
		GraphTypeID = @nGraphTypeID,
		ValidationOnWarning = @sValidationOnWarning,
		ValidationOnEntry = @sValidationOnEntry,
		IsStandard = @bIsStandard,
		DisplayName=@sDisplayName,
		Constant=@sConstant,
		Calculation=@sCalculation,	
		ShowTotal=@bShowTotal,
		IgnoreSymbols=@bIgnoreSymbols,
		IgnoreSymbolMode=@nIgnoreSymbolMode,
		IgnoreSymbolConstant=@nIgnoreSymbolConstant,
		Notes=@sNotes,
		IsRound=@bIsRound,
		RoundNumber=@nRoundNumber,
		CheckUnlikelyValue=@bCheckUnlikelyValue,
		GraphLabel=@sGraphLabel,
		LastUpdatedUserID=@nLastUpdatedUserID,
		DropdownValues=@sDropdownValues,
		Importance=@sImportance,
		Alignment=@sAlignment,
		NumberType=@nNumberType,
		DefaultValue=@sDefaultValue,
		AvgColumnID=@nAvgColumnID,
		AvgNumberOfRecords=@nAvgNumberOfRecords,
		ShowGraphExceedance=@dShowGraphExceedance,
		ShowGraphWarning=@dShowGraphWarning,
		FlatLineNumber=@nFlatLineNumber,
		MaxValueAt=@dMaxValueAt,
		DefaultGraphDefinitionID=@nDefaultGraphDefinitionID,
		TextWidth=@nTextWidth,
		TextHeight=@nTextHeight,
		ColumnType=@sColumnType,
		DropDownType=@sDropDownType,
		TableTableID =@nTableTableID,
		DisplayColumn=@sDisplayColumn,
		DisplayRight =@bDisplayRight,
		SummaryCellBackColor=@sSummaryCellBackColor,
		FormVerticalPosition=@nFormVerticalPosition,
		FormHorizontalPosition=@nFormHorizontalPosition,
		ParentColumnID=@nParentColumnID,
		TextType=@sTextType,
		RegEx=@sRegEx,
		DateCalculationType=@sDateCalculationType,
		LinkedParentColumnID=@nLinkedParentColumnID,
		DataRetrieverID=@nDataRetrieverID,
		VerticalList=@bVerticalList,
		QuickAddLink=@bQuickAddLink,
		CalculationIsActive=@bCalculationIsActive,
		TableTabID=@nTableTabID,
		ViewName=@sViewName,
		DefaultType=@sDefaultType,
		DefaultColumnID=@nDefaultColumnID,
		ShowViewLink=@sShowViewLink,
		FilterParentColumnID=@nFilterParentColumnID,
		FilterOtherColumnID=@nFilterOtherColumnID,
		FilterValue=@sFilterValue,
		MapPinHoverColumnID=@nMapPinHoverColumnID,
		CompareColumnID=@nCompareColumnID,
		CompareOperator=@sCompareOperator,
		MapPopup=@sMapPopup,
		TrafficLightColumnID=@nTrafficLightColumnID,
		TrafficLightValues=@sTrafficLightValues,
		DefaultRelatedTableID=@nDefaultRelatedTableID,
		DefaultUpdateValues=@bDefaultUpdateValues,
		ValidationCanIgnore=@bValidationCanIgnore,
		ImageOnSummary=@bImageOnSummary,
		AllowCopy=@bAllowCopy,
		ValidationOnExceedance=@sValidationOnExceedance,
		ColourCells=@bColourCells,
		ButtonInfo=@sButtonInfo,
		FilterOperator=@sFilterOperator,
		IsReadOnly=@bIsReadOnly,
		ShowGraphWarningMin=@dShowGraphWarningMin,
		ShowGraphExceedanceMin=@dShowGraphExceedanceMin,
		HideTimeSecond = @bHideTimeSecond,
		ColSpan=@ColSpan,
		RowSpan=@RowSpan,
		FilterParentColumnID2=@nFilterParentColumnID2,
		FilterOtherColumnID2=@nFilterOtherColumnID2,
		FilterValue2=@sFilterValue2,
		FilterOperator2=@sFilterOperator2,
		/*RP Added Ticket 4310*/
		ShowAllValuesOnSearch=@ShowAllValuesOnSearch,
		/*End Modification*/
		CalculationNullAsZero =	@bCalculationNullAsZero,
		ChildTableID =@nChildTableID,
		EditableOnSummary=@bEditableOnSummary,
		RolesWhoCanIgnoreValidation = @sRolesWhoCanIgnoreValidation,
		SqlRetrieval = @sSqlRetrieval,
		TriggerColumnID = @nTriggerColumnID,
		SQLRetrievalOption = @sSQLRetrievalOption

	WHERE ColumnID = @nColumnID
	IF (@nTableTabID_old  IS NULL AND @nTableTabID IS NOT NULL)OR (@nTableTabID_old  IS NOT NULL AND @nTableTabID IS NULL)  OR (@nTableTabID_old  IS NOT NULL AND @nTableTabID IS NOT NULL	AND @nTableTabID_old<>@nTableTabID)
		BEGIN
			UPDATE [Column] SET Xcoordinate=NULL,Ycoordinate=NULL WHERE ColumnID = @nColumnID
			EXEC dbo.Column_Fix_NullCoordinate @nTableID
		END
		
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
	 '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
	 '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
	 '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Column_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH



