﻿CREATE PROCEDURE [dbo].[usr_rptWorkLoad_25102018]
AS
BEGIN
SELECT
		Account24918.vProject.[Group/Division],
		Account24918.vProject.Controller,
		Account24918.vProject.ProjectNumber,
		  Account24918.vProject.ProjectTitle, 
	 Account24918.vProject.Status,
	 Account24918.vNotes.Date,
	  Account24918.vProject.Value, 
	  Account24918.vNotes.Subject, 
	  Account24918.vProject.FixedPrice
FROM Account24918.vProject 
LEFT JOIN Account24918.vNotes ON Account24918.vProject.ProjectNumber =Account24918.vNotes.ProjectNumber
--WHERE (((Account24918.vProject.Status)=2) AND 
--((Account24918.vNotes.Date)>=#1/1/2001# 
--And (Account24918.vNotes.Date)<=#1/19/2001#))
ORDER BY
		Account24918.vProject.[Group/Division], 
		Account24918.vProject.Controller,
		Account24918.vProject.ProjectNumber;

END
