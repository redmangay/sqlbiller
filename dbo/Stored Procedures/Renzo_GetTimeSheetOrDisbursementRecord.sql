﻿-- =============================================
-- Author:		<Red Mangay>
-- Create date:	<10/07/2018>
-- Description:	<Get the tableid of the orginal table ie timesheet or disbursement>
-- =============================================
/*

exec [GetTimeSheetTableIDFrom] 2713164

*/


CREATE PROCEDURE [dbo].[Renzo_GetTimeSheetOrDisbursementRecord]
(
	@RecordID int
	
)
AS
BEGIN
	
		

DECLARE @TimeSheetOrDisbursementRecordID int

SELECT @TimeSheetOrDisbursementRecordID = ISNULL(V010, V011) FROM RECORD
WHERE RECORDID = @RecordID

SELECT * FROM [RECORD]
WHERE RECORDID =  @TimeSheetOrDisbursementRecordID

END

