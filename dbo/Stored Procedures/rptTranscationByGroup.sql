﻿
CREATE PROCEDURE [dbo].[rptTranscationByGroup]
	@StartDate datetime=null
AS
BEGIN
	
	IF (SELECT object_id('TempDB..#tempTable1')) IS NOT NULL
BEGIN
    DROP TABLE #tempTable1
END

IF (SELECT object_id('TempDB..#tempTableGroup1')) IS NOT NULL
BEGIN
    DROP TABLE #tempTableGroup1
END

IF (SELECT object_id('TempDB..#tempTableSub')) IS NOT NULL
BEGIN
    DROP TABLE #tempTableSub
END

SELECT    p.[Group/Division] AS GD,
 CASE WHEN ISNULL(p.[Group/Division],'')='' or ISNULL(b.[Group Currency], '')=''
            THEN CAST(ISNULL(b.[Debtors Converted], 0) AS MONEY)
            ELSE CAST(ISNULL(b.[Rpt Debtors Converted], 0) AS MONEY) 
       END AS Debtors,
 
 ISNULL(b.[Group Currency], '') AS [Group Currency], CONVERT(Datetime, b.[Date], 103)  as [Date] , b.[Project Number]
into #tempTable1
FROM        
    Account24918.[vBilling Information] AS b INNER JOIN
                         Account24918.vProject AS p ON b.[Project Number] = p.ProjectNumber
WHERE        (b.[Project Number] IN
                             (SELECT        ProjectNumber
                               FROM            Account24918.vProject))
ORDER BY b.Date, b.[Project Number]

select SUM(Debtors) AS DBSum,GD into #tempTableSub
from #tempTable1 
group by GD

select SUM(Debtors) AS DBSum,GD into #tempTableGroup1 
from #tempTable1 
where Date >= @StartDate
group by GD

select distinct t1.GD , (t1.DBSum-ISNULL(t2.DbSum,0)) As Begining_Balance
FROM  #tempTableSub t1 LEFT JOIN #tempTableGroup1 t2                         
on t1.GD=t2.GD
left join
Account24918.vList_Of_Group_Division AS ListGD on
(ListGD.[Group/Division]=t1.GD or t1.GD='(None)')
where ListGD.OldYesNo='FALSE'
	
END

