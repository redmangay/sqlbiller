﻿create procedure [dbo].[usr_rptCurrentBidsActivity_22102018]
(
@Group varchar(max) =null,
@Controller varchar(max) =null,
@From date =null,
@To date  =null,
@OrderByColumn varchar(500) =1,
@filter varchar(50) ='0'

)
as

SELECT Client.Company,
 Project.ProjectNumber,
  Project.ProjectTitle, Project.Date AS ProjectDate, Project.[Group/Division],
  Project.[Status],
isnull(dbo.ConvertToGroupCurrency(cast(ISNULL(Value, 0) as money),cast(ISNULL(CurrencyRate, 0) as money), cast(ISNULL(RptCurrencyRate, 0) as money)),0) as ProjectValue
 , Project.EmployeeID AS Controller,
 case when cast(ISNULL(CurrencyRate, 0) as money)=0 then '' 
      when [FixedPrice] ='False' then '' 
      else 'C' end as IsConverted
      into #rptCurrentBidsActivity
 FROM Account24918.vSubsidiary Client   INNER JOIN Account24918.vProject Project ON Client.ClientID = Project.ClientID


if @OrderByColumn ='1'
begin
SELECT c.[Group/Division] as GroupDivision, 
sum(case when isnumeric(c.ProjectValue) =1 then c.ProjectValue else 0  end) as JobValue,
 Count(*) AS Bids,
 min(name) as name,
   IsNull(ReportingCurrency,'') as[ReportingCurrency]
 FROM #rptCurrentBidsActivity c 
  INNER JOIN  Account24918.vEmployee_List e ON (c.Controller = e.EmployeeID) 
  LEFT JOIN Account24918.vList_Of_Group_Division [List_Of_Group/Division] ON c.[Group/Division] = [List_Of_Group/Division].[Group/Division]
where
 (  convert(date, ProjectDate) between @From and @TO  and
 c.Status ='1'
and
( ((@filter ='0') and ((@group ='All') or (c.[Group/Division] =@group)))  OR ( ( (@filter ='1') AND (name =@controller))) )



)

group by  c.[Group/Division],
IsNull(ReportingCurrency,'')
order by convert(date, Min(ProjectDate))
end

else
begin
SELECT c.[Group/Division] as GroupDivision, 
sum(case when isnumeric(c.ProjectValue) =1 then c.ProjectValue else 0  end) as JobValue,
 Count(*) AS Bids,
 min(name) as name,
   IsNull(ReportingCurrency,'') as[ReportingCurrency]
 FROM #rptCurrentBidsActivity c 
  INNER JOIN  Account24918.vEmployee_List e ON (c.Controller = e.EmployeeID) 
  LEFT JOIN Account24918.vList_Of_Group_Division [List_Of_Group/Division] ON c.[Group/Division] = [List_Of_Group/Division].[Group/Division]
where
 (  convert(date, ProjectDate) between @From and @TO  and
 c.Status ='1'
and
( ((@filter ='0') and ((@group ='All') or (c.[Group/Division] =@group)))  OR ( ( (@filter ='1') AND (name =@controller))) )

)

group by  c.[Group/Division],
IsNull(ReportingCurrency,'')
order by Min(ProjectNumber)


end






