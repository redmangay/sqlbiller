﻿

CREATE PROCEDURE [dbo].[ContactUs_Email_Content]

(

      @sKey varchar(10) = NULL

)

AS

/*

      UNIT TESTING

      ============

      EXEC dbo.ContactUs_Email_Conetnt

      EXEC dbo.ContactUs_Email_Conetnt @sKey = 1

*/

BEGIN TRY

      DECLARE @nContactID int

      IF @sKey IS NULL

      BEGIN

            PRINT 'NULL'

            SELECT @nContactID = MAX(ContactID) FROM Contact

                  WHERE ContactID = 3

      END

      ELSE

            SELECT @nContactID = CAST(@sKey AS int)

 

      PRINT '@nContactID = ' + CAST(@nContactID  AS varchar)   

 

      SELECT * FROM Contact WHERE ContactID = @nContactID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ContactUs_Email_Content', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
