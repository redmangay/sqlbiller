﻿

CREATE PROCEDURE [dbo].[EMD_System_Column_Lookup_Refresh]

AS

BEGIN TRY
	TRUNCATE TABLE [EMD_System_Column_Lookup];

	INSERT INTO [dbo].[EMD_System_Column_Lookup]
           ([TableID]
           ,[Record ID]
           ,[Account Name]
           ,[Table Name]
           ,[ColumnID]
           ,[Column Display Name (Auto Populated)]
           ,[Column System Name (Auto Populated)]
           ,[NameOnImport (Auto Populated)]
           ,[TableID (Auto Populated)]
           ,[DateAdded])
		SELECT  dbo.[Table].TableID
		   ,dbo.Record.RecordID AS [Record ID]
		   ,dbo.Record.V001 AS [Account Name (auto populated)]
		   ,dbo.Record.V002 AS [Table Name (Auto Populated)]
		   ,dbo.Record.V003 AS [ColumnID (Auto Populated)]
		   ,dbo.Record.V004 AS [Column Display Name (Auto Populated)]
		   ,dbo.Record.V005 AS [Column System Name (Auto Populated)]
		   ,dbo.Record.V006 AS [NameOnImport (auto populated)]
		   ,dbo.Record.V007 AS [TableID (auto populated)]
		   ,dbo.Record.DateAdded
			FROM    dbo.[Table] INNER JOIN
					dbo.Record ON dbo.Record.IsActive = 1 AND dbo.Record.TableID = dbo.[Table].TableID
			WHERE   (dbo.[Table].IsActive = 1) 
					AND (dbo.[Table].AccountID = 24929) 
					AND (dbo.[Table].TableName = 'System Column Lookup')
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('EMD_System_Column_Lookup_Refresh', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
