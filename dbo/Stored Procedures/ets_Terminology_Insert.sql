﻿

CREATE PROCEDURE [dbo].[ets_Terminology_Insert]
(
	@nNewID int output,
	@nAccountID int  = NULL,
	@sPageName varchar(300)  = NULL,
	@sInputText varchar(MAX) ,
	@sOutputText varchar(MAX) 
)
	/*----------------------------------------------------------------
	
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	INSERT INTO Terminology
	(
		AccountID, 
		PageName, 
		InputText, 
		OutputText
	) VALUES (
		@nAccountID,
		@sPageName,
		@sInputText,
		@sOutputText
	)
	SELECT @nNewID = @@IDENTITY
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Terminology_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
