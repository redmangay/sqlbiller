﻿

CREATE PROCEDURE [dbo].[spGrantRightsToAllRoutines]
AS
/*
	Stored procedure to create 2 new database roles sp_viewer and sp_runner 
	and grant rights rights to those roles. After that all you need to do is
	give those roles to the database users who need them

	EXEC spGrantRightsToAllRoutines
*/

BEGIN TRY
	IF DATABASE_PRINCIPAL_ID('sp_runner') IS NULL
		CREATE ROLE [sp_runner]
	IF DATABASE_PRINCIPAL_ID('sp_viewer') IS NULL
		CREATE ROLE [sp_viewer]

	DECLARE @t TABLE (Name varchar(8000), id int identity(1,1) )

	INSERT INTO @t 
		SELECT SPECIFIC_SCHEMA  + '.[' + ROUTINE_NAME + ']' FROM INFORMATION_SCHEMA.ROUTINES 
		WHERE NOT (ROUTINE_TYPE = 'FUNCTION' AND DATA_TYPE = 'TABLE') -- this one causes an error

	DECLARE @sSQL varchar(8000), @counter int
	SET @counter = 1

	WHILE EXISTS(SELECT * FROM @t WHERE id >= @counter)
	BEGIN
		SELECT @sSQL='
					GRANT EXEC ON ' + [Name] + ' TO sp_runner;  
					GRANT VIEW DEFINITION ON ' + [Name] + ' TO sp_viewer;' 
					  FROM @t WHERE id = @counter
		--PRINT @sSQL
 		EXEC (@sSQL)  -- comment out this line to test it

		SET @counter = @counter + 1
	END 
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spGrantRightsToAllRoutines', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
