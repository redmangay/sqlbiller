﻿

CREATE PROCEDURE [dbo].[dbg_View_ResetViewItems]
(
	@nTableID int
)
	/*----------------------------------------------------------------
	EXEC [dbg_View_ResetViewItems] 2655
	---------------------------------------------------------------*/
 
AS

BEGIN TRY
	DECLARE Cur_View CURSOR FOR
	SELECT ViewID FROM [View] WHERE TableID=@nTableID;
	DECLARE @nViewID INT;
	OPEN Cur_View;
	FETCH NEXT FROM Cur_View INTO @nViewID;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
	  
			 IF @nViewID IS NOT NULL
			 BEGIN	 		
					-- insert new ViewItem when column has DisplayTextSummary
					INSERT INTO [ViewItem]
					   ([ViewID]
					   ,[ColumnID]
				  		   ,[SearchField]
					   ,[FilterField]
					   ,[Alignment]
					   ,[ShowTotal]
					   ,[SummaryCellBackColor]
					   ,[ColumnIndex])
		     
					SELECT @nViewID
						,ColumnID
						,null
						,1
						,Alignment
						,ShowTotal
						,SummaryCellBackColor
						,(SELECT ISNULL(MAX(ColumnIndex),-1) + 1 FROM ViewItem WHERE ViewID=@nViewID) 
						FROM [Column] 
						WHERE TableID=@nTableID 
						AND ColumnID NOT IN 
							(SELECT ColumnID FROM [ViewItem] WHERE ViewID=@nViewID)
						AND DisplayTextSummary IS NOT NULL AND LEN(DisplayTextSummary) > 0
						AND Systemname not in('IsActive','TableID') 
						AND ColumnType NOT IN ('staticcontent')
						ORDER BY DisplayOrder		
				
				END
	  
		  FETCH NEXT FROM Cur_View INTO @nViewID;
	   END;
	CLOSE Cur_View;
	DEALLOCATE Cur_View;
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_View_ResetViewItems', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
