﻿

CREATE PROCEDURE [dbo].[ets_Record_Count_New]
(
      @nTableID int,
      @sSeries varchar(MAX),
      -- @sValueFields varchar(MAX),
      @dStartDate datetime,
      @dEndDate datetime,
      @sPeriod varchar(20) = '' -- Possible values of @sPeriod: Year, Month, Week, Day, Hour
)
AS
/*
      PRINT SYSDATETIME()
      EXEC ets_Record_Count_New
            @nTableID=919
            ,@sSeries='2555' --LocationID
            ,@dStartDate='2012-12-13'
            ,@dEndDate='2012-12-13'
            ,@sPeriod='Day'
      PRINT SYSDATETIME()
      -- 3603515 1st run
      -- 2919922 2nd run : no IF
      -- 2529297 3rd run : using IN
*/
BEGIN TRY
    SET NOCOUNT ON;
    DECLARE @Sql varchar(MAX)       
    DECLARE @sDateTime varchar(100)
    DECLARE @sSeriesAvg varchar(MAX)
 
    --SET @dEndDate = DATEADD(Day, 1, @dEndDate)
    IF(LEN(@sPeriod) = 0) OR @sPeriod = 'Hour'  -- then we do not reduce the data
    BEGIN

		SET @sql =  ' SELECT COUNT(*)' +
                  ' FROM [Record]' +
                  ' WHERE IsActive=1 AND DateTimeRecorded BETWEEN ''' + CAST(@dStartDate AS Varchar) + ''' AND ''' +  CAST(@dEndDate AS Varchar) + '''' +
                  ' AND LocationID IN (' + @sSeries + ')' +
                  ' AND TableID = ' + CAST(@nTableID AS Varchar)
	END  
	ELSE -- we are ing to reduce the data
	BEGIN
		-- Convert the fields to averages
		IF @sPeriod = 'Year' SET @sDateTime = 'CAST(CONVERT(varchar(07),DateTimeRecorded,20) + ''-01'' AS datetime)'
		IF @sPeriod = 'Month' SET @sDateTime = 'CAST(CONVERT(varchar(10),DateTimeRecorded,20) AS datetime)'
		IF @sPeriod = 'Week' SET @sDateTime = 'CAST(CONVERT(varchar(10),DateTimeRecorded,20) AS datetime)'
		IF @sPeriod = 'Day' SET @sDateTime = 'CAST(CONVERT(varchar(13),DateTimeRecorded,20) + '':00'' AS datetime)'

		SET @sql =  '
			SELECT COUNT(DISTINCT ' + @sDateTime + ') FROM [Record]' +
                  ' WHERE IsActive=1 AND DateTimeRecorded BETWEEN ''' + CAST(@dStartDate AS Varchar) + ''' AND ''' +  CAST(@dEndDate AS Varchar) + '''' +
                  ' AND LocationID IN (' + @sSeries + ')' +
                  ' AND TableID = ' + CAST(@nTableID AS varchar)
                  --' GROUP BY ' + @sDateTime
	END
	PRINT @sql
	EXEC(@sql)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Record_Count_New', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
