﻿

CREATE PROCEDURE   [dbo].[ets_Record_List_BulkExport]
(
	@nTableID int,
	@nEnteredBy int = NULL,
	@bIsActive bit = NULL,
	@bHasWarningResult bit = NULL,
	@dDateFrom datetime = NULL,
	@dDateTo datetime = NULL,
	@sFileName nvarchar(1000)=NULL 
)

/*
EXEC [ets_Record_List_BulkExport] @nTableID=1169,@sFileName='E:\DBG\SQLExport\a1212BSFLog.csv'
*/
AS

BEGIN TRY
	SET NOCOUNT ON;

	DECLARE @tDisplayColumns TABLE
	(
		ID int identity(1,1),
		ColumnID int,
		SystemName varchar(50),
		DisplayText varchar(50)
	)

				DECLARE @nExportTemplateID INT
				SELECT TOP 1 @nExportTemplateID=ExportTemplateID FROM ExportTemplate WHERE TableID=@nTableID ORDER BY ExportTemplateID DESC
				--INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName, FieldsToShow, ParentTableID, ParentJoinColumnName,ChildJoinColumnName,ColumnType)
				--		SELECT C.ColumnID, ETI.ExportHeaderName, C.SystemName,
				--			dbo.fnReplaceDisplayColumns(C.DisplayColumn, C.[TableTableID],C.ColumnID) as [FieldsToShow],
				--			C.[TableTableID] AS  [ParentTableID], 
				--			PC.SystemName as [ParentJoinColumnName], 
				--			C.SystemName as [ChildJoinColumnName],
				--			C.ColumnType 
				--			FROM [Column] C
				--				LEFT OUTER JOIN [Column] PC ON PC.[ColumnID] = C.[LinkedParentColumnID]
				--				JOIN ExportTemplateItem ETI ON ETI.ColumnID=C.ColumnID
				--				WHERE C.TableID = @nTableID 	AND ETI.ExportTemplateID=@nExportTemplateID							
				--				ORDER BY ETI.ColumnIndex


	INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
			SELECT C.ColumnID, ETI.ExportHeaderName, SystemName
				FROM [Column] C JOIN ExportTemplateItem ETI ON ETI.ColumnID=C.ColumnID
				WHERE C.TableID = @nTableID 
			ORDER BY DisplayOrder


		DECLARE @sSQL varchar(MAX)
		DECLARE @sSelectCount varchar(MAX)
		DECLARE @counter int
		SET @counter = 1
		
	  DECLARE @sColumns varchar(MAX)='SELECT  '
  
	  IF @bHasWarningResult is not NULL
		  IF @bHasWarningResult =1
			SET @sColumns= 'SELECT ''""Warning""'',' 
        
 
 
	 DECLARE @DBName varchar(50)
	 SELECT @DBName='[' + DB_NAME()  + ']'

	  -- Get the sOrder IF it is NULL
	   WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID >= @counter)
		BEGIN
			SELECT @sColumns= @sColumns + '''""' + DisplayText  + '""''' + ',' FROM @tDisplayColumns WHERE ID = @counter
			--SET @sColumns=@sColumns + @sTempColumn	
			 SET @counter = @counter + 1
		END 
	
	
	
		SELECT @sColumns = LEFT(@sColumns, LEN(@sColumns)-1) 
		SET @sColumns= @sColumns + ' UNION ALL '
	
		PRINT @sColumns
	 -- SELECT @sSQL = 'SELECT * FROM (SELECT RecordInfo.*,ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ' ) as RowNum FROM (SELECT '
	  SELECT @sSQL = 'SELECT  * FROM ( SELECT '

		 IF @bHasWarningResult is not NULL
		  IF @bHasWarningResult =1
		  SELECT @sSQL = @sSQL + ' ''""'' + convert(varchar(MAX),'+@DBName+'.dbo.Record.WarningResults) + ''""'' as [Warning], ' 

		SET @counter = 1
		WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID >= @counter)
		BEGIN
			SELECT @sSQL = @sSQL + ' ''""'' + convert(varchar(MAX),'+@DBName+'.dbo.Record.' + SystemName + ') + ''""''  AS [' + DisplayText + '], ' 
				FROM @tDisplayColumns 
				WHERE ID = @counter
			--PRINT @sSQL
			SET @counter = @counter + 1	
		END 

		SELECT @sSQL = LEFT(@sSQL, LEN(@sSQL)-1) 
	
		SELECT @sSQL = @sSQL + ' FROM '+@DBName+'.dbo.[Record]'

	

		IF PATINDEX('%Record.TableID%', @sSQL) > 0
		BEGIN
			SET @sSQL = REPLACE(@sSQL, @DBName+'.dbo.Record.TableID', @DBName+'.dbo.[Table].TableName')
			SET @sSQL = REPLACE(@sSQL, 'FROM '+@DBName+'.dbo.[Record]', 'FROM '+@DBName+'.dbo.[Record] JOIN '+@DBName+'.dbo.[Table] ON '+@DBName+'.dbo.Record.TableID = '+@DBName+'.dbo.[Table].TableID')
		END	

		IF PATINDEX('%Record.EnteredBy%', @sSQL) > 0
		BEGIN
			SET @sSQL = REPLACE(@sSQL, @DBName+'.dbo.Record.EnteredBy', @DBName+'.dbo.[User].FirstName')
			SET @sSQL = REPLACE(@sSQL, 'FROM '+@DBName+'.dbo.[Record]', 'FROM '+@DBName+'.dbo.[Record] JOIN '+@DBName+'.dbo.[User] ON '+@DBName+'.dbo.Record.EnteredBy = '+@DBName+'.dbo.[User].UserID')
		END	

		-- ADD IN THE WHERE CRITERIA
		SELECT @sSQL = @sSQL + ' WHERE '+@DBName+'.dbo.Record.TableID = ' + CAST(@nTableID as varchar)


		IF @dDateFrom IS NOT NULL 
			SELECT @sSQL = @sSQL + ' AND '+@DBName+'.dbo.Record.DateTimeRecorded >= ''' + CONVERT(varchar(30), @dDateFrom, 120) + ''''

		IF @dDateTo IS NOT NULL 
			SELECT @sSQL = @sSQL + ' AND '+@DBName+'.dbo.Record.DateTimeRecorded <= ''' + CONVERT(varchar(30), @dDateTo, 120) + ''''

		IF @nEnteredBy IS NOT NULL 
			SELECT @sSQL = @sSQL + ' AND '+@DBName+'.dbo.Record.EnteredBy = ' + CAST(@nEnteredBy AS varchar)

		 IF @bHasWarningResult IS NOT NULL
		 BEGIN
			 IF @bHasWarningResult =0 
			 SELECT @sSQL = @sSQL + ' AND '+@DBName+'.dbo.Record.WarningResults is  NULL ' 
	     
			 IF @bHasWarningResult =1 
			 SELECT @sSQL = @sSQL + ' AND '+@DBName+'.dbo.Record.WarningResults is not NULL ' 
		 END
	
	
	
	
		IF @bIsActive IS NOT NULL 
			SELECT @sSQL = @sSQL + ' AND '+@DBName+'.dbo.Record.IsActive = ' + CAST(@bIsActive AS varchar)
    
		--SELECT @sSelectCount= @sSQL + ') as RecordInfo) as RecordFinalInfo'
		--SELECT @sSQL= @sSQL + ') as RecordInfo) as RecordFinalInfo WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
		--SELECT @sSQL= @sSQL + ' ORDER BY ' + @sOrder  + ') AS RecordInfo'
		SELECT @sSQL= @sSQL + ' ) AS RecordInfo'
		--SET NOCOUNT OFF;
    
		--SET @sSelectCount=REPLACE(@sSelectCount, 'SELECT * FROM', 'SELECT COUNT(*) AS TotalRows FROM')
    
    
		SET ROWCOUNT 2147483647
		PRINT @sSQL
	SET @sSQL=@sColumns + @sSQL
	DECLARE @cmd varchar(2048) 
	--SET @cmd='bcp "'+ @sSQL +'" queryout E:\DBG\SQLExport\BSFLog.csv -c -t"," -S' + @@ServerName + ' -T'
	SET @cmd='bcp "'+ @sSQL +'" queryout ' + @sFileName + ' -c -t"," -S' + @@ServerName + ' -T'

	PRINT @cmd

	EXEC master.dbo.xp_cmdshell  @cmd

	
	SET ROWCOUNT 0
 

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Record_List_BulkExport', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
