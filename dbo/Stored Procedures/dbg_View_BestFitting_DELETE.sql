﻿

CREATE PROCEDURE [dbo].[dbg_View_BestFitting_DELETE]
(
	 @UserID int=NULL, 
	 @ViewPageType varchar(10), 
	 @TableID int,
	 @ViewName varchar(500) = NULL
)
AS

BEGIN TRY
	
	SELECT TOP 1 	* 
	FROM [dbo].[View]
	WHERE TableID = @TableID 
		AND ViewPageType = @ViewPageType 
		AND (UserID IS NULL or UserID = @UserID)
	ORDER BY  userID DESC,
		CASE WHEN @ViewName IS NULL OR ViewName=@ViewName THEN 1 ELSE 0 END DESC
	
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_View_BestFitting_DELETE', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
