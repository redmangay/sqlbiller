﻿

CREATE PROCEDURE [dbo].[ets_GetListOfSamplesByOneSys]
(
	@sSystemName varchar(50),
	@nSampleTypeID int 
)
AS
/*
EXEC User_Details 2
*/
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @mySQL varchar(4000)
	
	set @mySQL='SELECT ' +  @sSystemName  + ',SampleID,WarningResults,ValidationResults FROM Sample
	 WHERE  SampleTypeID=' + convert(varchar,@nSampleTypeID );
    
    PRINT @mySQL
    
    EXEC (@mySQL)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_GetListOfSamplesByOneSys', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
