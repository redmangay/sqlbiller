﻿CREATE proc [dbo].[srptworkload]
as
SELECT Notes.ProjectNumber, Max(Notes.Date) AS MaxOfDate
into #JobMaxNoteDate
FROM Account24918.vNotes Notes INNER JOIN Account24918.vProject Project ON Notes.ProjectNumber = Project.ProjectNumber
WHERE (((Project.Status)='2'))
GROUP BY Notes.ProjectNumber
ORDER BY Notes.ProjectNumber;



SELECT   Project.[Group/Division] AS GroupDivision, 
Notes.ProjectNumber AS JobNo,
 Project.ProjectTitle,Notes.Date AS NoteDate,
  Notes.Initials,
   Employee_List.Name AS Controller, 
   Notes.Subject AS Subject, 
   Project.FixedPrice, 
   Project.Value as ProjectValue, 
    
      

   
   Client.Company
   into #tempdata
    FROM 
   Account24918.vClient Client
    INNER JOIN (((#JobMaxNoteDate JobMaxNoteDate INNER JOIN
    
     Account24918.vNotes Notes ON (JobMaxNoteDate.MaxOfDate = Notes.Date) AND (JobMaxNoteDate.ProjectNumber = Notes.ProjectNumber) )INNER JOIN
     Account24918.vProject Project ON Notes.ProjectNumber = Project.ProjectNumber) INNER JOIN 
     Account24918.vEmployee_List Employee_List ON Project.EmployeeID = Employee_List.EmployeeID) ON Client.ClientID = Project.ClientID


WHERE
((convert(date,(Notes.Date)) <='2015-05-14' )OR (convert(date,Notes.Date) >='2015-06-15')) 
    

SELECT  Project.[Group/Division] AS GroupDivision, 
Project.ProjectNumber AS JobNo,
 Project.ProjectTitle,Notes.Date AS NoteDate,
  Notes.Initials, Employee_List.Name AS Controller, Notes.Subject AS Subject, Project.FixedPrice, Project.Value  as ProjectValue, 
 
  Client.Company
  into #tempdata2
  FROM  Account24918.vSubsidiary Client   INNER JOIN 
   (( Account24918.vProject Project   LEFT JOIN
     Account24918.vNotes Notes ON Project.ProjectNumber = Notes.ProjectNumber)   INNER JOIN 
     Account24918.vEmployee_List Employee_List ON Project.EmployeeID = Employee_List.EmployeeID) ON Client.ClientID = Project.ClientID 


WHERE 
((((convert(date,Notes.Date)) >='2015-05-14' AND (convert(date,Notes.Date)) <= '2015-06-15'    ) 
AND (Project.Status) = '2') OR (((Notes.Date) Is Null) AND ((Project.Status)='2')));



 select dbo.fn_getTxtBilled(JobNo, ProjectValue) as gettxtbilled,* from #tempdata

UNION

 select dbo.fn_getTxtBilled(JobNo, ProjectValue) as gettxtbilled, * from #tempdata2
 