﻿

CREATE PROCEDURE   [dbo].[ets_TempRecordIntermediate_List]
(
	@nBatchID int,
	@nTableID int,
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
AS
/*

EXEC [ets_TempRecord_List] @nBatchID=-1,@nTableID=2930,@nImportTemplateID=75

*/
BEGIN TRY

	SET NOCOUNT ON;

		DECLARE @Where varchar(MAX)
		DECLARE @SQLMain varchar(MAX)
		DECLARE @SQLCount varchar(MAX)
		
		DECLARE @nSourceColumnsCnt int = 0
		SELECT @nSourceColumnsCnt = [SourceColumnsCount] FROM [Batch] WHERE [BatchID] = @nBatchID

		SET @Where = ' WHERE TempRecord.TableID = ' + CAST(@nTableID as varchar)
		SET @Where = @Where + ' AND TempRecord.BatchID = ' + CAST(@nBatchID as varchar)

		DECLARE @i int = 1
		DECLARE @sFieldsA varchar(MAX) = ''
		DECLARE @sFieldsB varchar(MAX) = ''

		WHILE @i <= 100 AND @i <= @nSourceColumnsCnt
		BEGIN
			SET @sFieldsA = @sFieldsA + 'V' + RIGHT('000' + RTRIM(CAST(@i AS char(3))), 3) + ' AS [' + RTRIM(CAST(@i AS char(3))) + '], '
			SET @sFieldsB = @sFieldsB + '[' + RTRIM(CAST(@i AS char(3))) + '], '
			SET @i = @i + 1
		END
		SET @sFieldsA = LEFT(@sFieldsA, LEN(@sFieldsA) - 1)
		SET @sFieldsB = LEFT(@sFieldsB, LEN(@sFieldsB) - 1)
		PRINT @sFieldsA
		PRINT @sFieldsB

		SET @SQLMain = 'WITH IntermediateRecords AS '+
		'( ' + 
		'    SELECT ' + @sFieldsA + ', RecordID as DBGSystemRecordID, ' +
		'    ROW_NUMBER() OVER(ORDER BY RecordID) AS RN ' + 
		'    FROM TempRecord ' + @Where + ' ' +
		') ' + 
		'SELECT ' + @sFieldsB + ', DBGSystemRecordID ' + 
		'FROM IntermediateRecords ' +
		'WHERE RN >= ' + CAST(@nStartRow AS varchar)

		SET @SQLCount = 'SELECT COUNT(*) FROM TempRecord' + @Where

		SET ROWCOUNT @nMaxRows

		PRINT LEFT(@SQLMain,8000)
		PRINT SUBSTRING(@SQLMain,8001,8000)
		EXEC (@SQLMain)
	
		SET ROWCOUNT 0 
		PRINT LEFT(@SQLCount,8000)
		PRINT SUBSTRING(@SQLCount,8001,8000)
		EXEC (@SQLCount)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_TempRecordIntermediate_List', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
