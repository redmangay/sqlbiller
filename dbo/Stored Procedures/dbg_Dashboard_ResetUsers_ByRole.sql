﻿

CREATE PROCEDURE [dbo].[dbg_Dashboard_ResetUsers_ByRole]
(
	 @RoleID INT
)
AS

BEGIN TRY
		
	DECLARE @AccountID INT
	DECLARE @DashboardDefaultFromUserID INT
	SELECT @AccountID=AccountID,@DashboardDefaultFromUserID=DashboardDefaultFromUserID
	 FROM [Role] WHERE RoleID=@RoleID
	
	IF @DashboardDefaultFromUserID IS NULL 
		SET @DashboardDefaultFromUserID=-1

	DECLARE @tDocument Table
			(
				ID int identity(1,1),
				DocumentID int
			)
	INSERT INTO @tDocument(DocumentID)
	SELECT DocumentID	FROM [dbo].[Document]
					WHERE ForDashBoard=1 AND AccountID = @AccountID 
					AND UserID <> @DashboardDefaultFromUserID and UserID is not NULL
					AND UserID IN (	SELECT U.UserID FROM [User] U INNER JOIN [UserRole] UR ON
					U.UserID=UR.UserID WHERE UR.AccountID=@AccountID AND UR.RoleID=@RoleID)

	

		DECLARE @counter int
			SET @counter = 1
			WHILE EXISTS(SELECT DocumentID FROM @tDocument WHERE ID >= @counter)
				BEGIN
					DECLARE @DashDocumentID INT
					SELECT @DashDocumentID=DocumentID FROM @tDocument WHERE ID=@counter

					EXEC DBO.ets_Document_Delete @DashDocumentID
					SET @counter = @counter + 1
				END 
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_Dashboard_ResetUsers_ByRole', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
