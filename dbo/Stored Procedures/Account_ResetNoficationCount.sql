﻿CREATE PROCEDURE [Account_ResetNoficationCount] 
	@bReturnValue bit OUTPUT
AS
BEGIN
-- Reset count only if first day of the month
	IF (DAY(GETDATE()) = 1)
	BEGIN
		UPDATE [Account] 
			SET SMSCount = 0, EmailCount=0 
		WHERE IsActive=1

		SELECT @bReturnValue = 1
	END
	ELSE
		SELECT @bReturnValue = 0
END
