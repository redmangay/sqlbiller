﻿CREATE procedure [dbo].[usr_rptContactActivity]
(
    @From	  date,
    @To	  date,
    @Notes  varchar(max) = '1'

)
AS
BEGIN

/*

[usr_rptContactActivity] '01/06/2000','28/05/2019','1'

*/
set dateformat dmy;
DECLARE @SQL VARCHAR(max) 
DECLARE @NotesBelongTo VARCHAR(MAX) = ''

 IF @Notes <> '1'
 BEGIN
 SET @NotesBelongTo = 'AND [Client Contact Notes].V007 ='''+ @Notes +''''
 END

SET @SQL = '
SELECT  [Client Information].V001 as ClientName		  /* ClientName */
	  ,[Client Contact Notes].V003 as [Date]		  /* Date */
	  ,CASE WHEN [Client Contact Notes].V005 = ''0'' THEN ''None'' 
	   WHEN [Client Contact Notes].V005 = ''1'' THEN ''Low'' 
	   WHEN [Client Contact Notes].V005 = ''2'' THEN ''Medium'' 
	   WHEN [Client Contact Notes].V005 = ''3'' THEN ''High'' End as [Priority]	  /* [Priority] */
	  ,[Employee].V004 as Initials	  /* Initial */
	  ,[Client Contact Notes].V008 as [FollowUpDate]		  /* [Follow-Up] */
	  ,[Client Contact Notes].V006 as [Subject]		  /* [Notes] */
	 

    FROM Record [Client Contact Notes]
    JOIN Record [Client Information] ON [Client Information].TableID = 2474
    AND [Client Information].IsActive = 1
    AND [Client Information].RecordID = [Client Contact Notes].V010

    JOIN Record [Employee] ON [Employee].TableID = 2669
    AND [Employee].IsActive = 1
    AND [Employee].V027 = ''1''
    AND [Employee].V002 = ''false''
    AND [Employee].RecordID = [Client Contact Notes].V007

    WHERE [Client Contact Notes].TableID = 2655
	   AND [Client Contact Notes].IsActive = 1
	   AND CONVERT(DATE,[Client Contact Notes].V003) >= CONVERT(date,'''+ CAST(@From as varchar) + ''')
	   AND CONVERT(DATE,[Client Contact Notes].V003) <= CONVERT(date,'''+ CAST(@To as varchar) +''')  
	   ' + @NotesBelongTo + '
	   ORDER BY  [Client Information].V001
	   '


EXEC(@SQL);
PRINT CAST(@SQL AS NTEXT)


/* Old Script from Prev programmer */


--set dateformat dmy
--SELECT Client.Company AS ClientName, Client_Notes.Follow_Up AS FollowUpDate, Client_Notes.Flag AS Priority, Client_Notes.Date, Client_Notes.Initials AS Initials, Client_Notes.Subject AS Subject, Employee_List.Name

--into #contactactivity
--FROM [Account24918].vSubsidiary as Client INNER JOIN ( [Account24918].[vClient_Notes]  as Client_Notes INNER JOIN [Account24918].[vEmployee_List]as Employee_List ON Client_Notes.Initials = Employee_List.Initials) ON Client.ClientID = Client_Notes.ClientID

-- WHERE (((Employee_List.OldYesNo)='False')  and (Employee_List.Name =@Notes or @Notes ='All'))
 
-- select * from #contactactivity where  #contactactivity.Date between  @From and @To
-- order by ClientName, #contactactivity.Date desc
 
 
 END
 
 
 