﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[srptTranscationByCurrency] 
	@StartDateSundryDebtors DateTime = null,
	@EndDateSundryDebtors DateTime = null,@Group_Currency varchar(7)=''
AS
BEGIN

IF OBJECT_ID('tempdb..#tempTableForDateRange') IS NOT NULL
begin
        drop table #tempTableForDateRange
end

SELECT CONVERT(Datetime, b.[Date], 103)  as [Date]  , b.[Project Number], b.Description, b.[Payment Amount Converted],
 b.[Invoice Amount Converted] AS ['Invoice Amount Converted'], CurrencyRate, 
 b.[Debtors Converted] AS ['Debtors Converted'], b.[Bad Debt Exp Converted], b.Type,
   p.RptCurrencyRate, b.[Rpt Debtors Converted] AS ['Rpt Debtors Converted'],  
  p.Currency, b.ID, b.[Group Currency] AS Group_Currency,(Select Symbol from [thedatabase_windtech].
[Account24918].[vCurrency Rates] where Currency=b.[Group Currency]) AS CurrencySymbol
  into #tempTableForDateRange
 
FROM [thedatabase_windtech].[Account24918].[vBilling Information] AS b, 
[thedatabase_windtech].[Account24918].[vProject] AS p
Where (b.[Project Number]=p.ProjectNumber) AND (b.Type<>7) 

Select * from #tempTableForDateRange
Where (Date BETWEEN @StartDateSundryDebtors And
@EndDateSundryDebtors ) 
AND (ISNULL(Group_Currency,'')=ISNULL(@Group_Currency,''))

ORDER BY Date, [Project Number]

END
