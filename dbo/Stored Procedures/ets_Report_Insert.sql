﻿
CREATE PROCEDURE [dbo].[ets_Report_Insert]
(
	@nNewID int output,
	@nAccountID int,
	@sReportName varchar(250) ,
	@sReportDescription varchar(MAX)=NULL,
	@nReportDates int,
	@nReportDatesBefore int,
	@dStartDate datetime = NULL,
	@dEndDate datetime = NULL,
	@nUserID int = NULL,
	@nDaysFrom int = NULL, -- RP Added Ticket 4513
	@nDaysTo int = NULL
)
AS

BEGIN TRY

	SET  @sReportName=dbo.fnNextAvailableReportName(@sReportName,@nAccountID,null)
	INSERT INTO [Report]
	(
		AccountID, 
		ReportName, 
		ReportDescription, 
		ReportDates,
		ReportDatesBefore,
		StartDate, 
		EndDate, 
		UserID,
		[DaysFrom],
		DaysTo
	) VALUES (
		@nAccountID,
		@sReportName,
		@sReportDescription,
		@nReportDates,
		@nReportDatesBefore,
		@dStartDate,
		@dEndDate,
		@nUserID,
		@nDaysFrom, --RP Added Ticket 4513
		@nDaysTo
	)
	SELECT @nNewID = @@IDENTITY
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
	 '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
	 '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
	 '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Report_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH


