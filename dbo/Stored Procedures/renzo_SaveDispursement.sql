﻿
CREATE PROCEDURE		[dbo].[renzo_SaveDispursement]
(
	@RecordID  int,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output
)
AS

/*
				Set/update the rate based on the dispursement based on the disbursement rate

				DECLARE			@Return		VARCHAR(MAX)
				EXEC			[renzo_SaveDispursement] @RecordID = 2072086, @Return = @Return

				EXEC		[dbo].[dev_GetAccountSQL] @TableID = 2725, @TableName = 'Disbursement', @AccountID = '24918'

				EXEC		[dbo].[dev_GetAccountSQL] @AccountID = '24918'		/*	buffalobiller1 */

*/

--				Set Rate
UPDATE			[Disbursement]
SET				[Disbursement].V005 = ISNULL([Disbursement Rate].V002, '0' )
FROM			[Record] [Disbursement]
JOIN			[Record] [Disbursement Rate] ON				1 = 1
													AND		[Disbursement Rate].RecordID = [Disbursement].V003
													AND		[Disbursement Rate].IsActive = 1
													AND		[Disbursement Rate].TableID = 2478
JOIN			[Record] [Disbursement Type] ON				1 = 1
													AND		[Disbursement Type].RecordID = [Disbursement Rate].V001
													AND		[Disbursement Type].IsActive = 1
													AND		[Disbursement Type].TableID = 2728
WHERE			1 = 1
AND				[Disbursement].[RecordID] = @RecordID
AND				[Disbursement].IsActive = 1
AND				[Disbursement].TableID = 2725
AND				[Disbursement].V005 IS NULL;

--				Set Details
UPDATE			[Disbursement]
SET				[Disbursement].V006 = ISNULL([Disbursement Type].V001, '' ) + CASE WHEN LEN( ISNULL([Disbursement Type].V001, '' )) > 0 AND LEN(ISNULL([Disbursement Rate].V003, '' )) > 0 THEN ' ' ELSE '' END + ISNULL([Disbursement Rate].V003, '' )

FROM			[Record] [Disbursement]
JOIN			[Record] [Disbursement Rate] ON				1 = 1
													AND		[Disbursement Rate].RecordID = [Disbursement].V003
													AND		[Disbursement Rate].IsActive = 1
													AND		[Disbursement Rate].TableID = 2478
JOIN			[Record] [Disbursement Type] ON				1 = 1
													AND		[Disbursement Type].RecordID = [Disbursement Rate].V001
													AND		[Disbursement Type].IsActive = 1
													AND		[Disbursement Type].TableID = 2728
WHERE			1 = 1
AND				[Disbursement].[RecordID] = @RecordID
AND				[Disbursement].IsActive = 1
AND				[Disbursement].TableID = 2725
AND				[Disbursement].V006 IS NULL;

RETURN @@ERROR;

