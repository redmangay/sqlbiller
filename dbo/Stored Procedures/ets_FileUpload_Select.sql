﻿
---------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[ets_FileUpload_Select]
(
@nAccountID int=NULL,
	@nTableID int  = NULL,
	@sUploadName varchar(100)  = NULL,
	@sLocation varchar(250)  = NULL,
	@sFilename varchar(250)  = NULL,
	@sOrder nvarchar(200) = 'FileUploadID DESC', 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	EXEC ets_Upload_Select @nAccountID=13, @nTableID=189
	---------------------------------------------------------------*/
AS
BEGIN TRY

	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(4000)
	DECLARE @sSelectCount nvarchar(MAX)

	DECLARE @sWhere nvarchar(4000)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nTableID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND u.TableID = '+CAST(@nTableID AS NVARCHAR)
	IF @nTableID IS NULL AND @nAccountID IS NOT NULL
	BEGIN
			SET @sWhere = @sWhere + ' AND AccountID = '+CAST(@nAccountID AS NVARCHAR)
	END
	
	IF @sUploadName IS NOT NULL 
		SET @sWhere = @sWhere + ' AND (UploadName LIKE '+'''%' + @sUploadName + '%'' OR Location LIKE '+'''%' + @sUploadName + '%'')'
	IF @sLocation IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Location LIKE '+'''%' + @sLocation + '%'''
	IF @sFilename IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Filename LIKE '+'''%' + @sFilename + '%'''
	SET @sSelect = 'SELECT * FROM 
	(SELECT u.FileUploadID as UploadID,u.TableID,u.UseMapping,u.UploadName,u.Location,u.Filename,st.AccountID , ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum 
	FROM FileUpload u INNER JOIN [Table] st ON u.TableID=st.TableID ' + @sWhere + ') as UI '
	 + ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)

	-- Extend WHERE to include paging:

	SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [FileUpload]  u INNER JOIN [Table] st ON u.TableID=st.TableID ' + @sWhere 
 
	EXEC (@sSelect)
	SET ROWCOUNT 0

	EXEC (@sSelectCount)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_FileUpload_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

