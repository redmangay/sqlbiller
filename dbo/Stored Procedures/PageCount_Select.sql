﻿
 
CREATE PROCEDURE [dbo].[PageCount_Select]
(
	@sPageURL varchar(1000)  = NULL,
	@dDateFrom datetime = NULL,
	@dDateTo datetime = NULL,
	@sOrder nvarchar(200) = visiteddate, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	EXEC PageCount_Select @nMaxRows=3
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'

	IF @sPageURL IS NOT NULL 
		SET @sWhere = @sWhere + ' AND PageURL LIKE '+'''%' + @sPageURL + '%'''

	IF @dDateFrom IS NOT NULL 
			SELECT @sWhere = @sWhere + ' AND visiteddate >= ''' + CONVERT(varchar(30), @dDateFrom, 120) + ''''

	IF @dDateTo IS NOT NULL 
			SELECT @sWhere = @sWhere + ' AND visiteddate <=  ''' + CONVERT(varchar(30), @dDateTo, 120) + ''''
	
	
	
	SET @sSelect = 'SELECT * FROM (SELECT visiteddate,PageURL,TotalCount,ROW_NUMBER()
	 OVER(ORDER BY ' + @sOrder + ' ) as RowNum  FROM 
	(SELECT cast ([DATE] as date)as visiteddate,PageURL,[Count] as TotalCount 
	FROM [PageCount] 
	UNION
	SELECT cast (DateAdded as date) as visiteddate ,PageURL,COUNT(*) as TotalCount   FROM VisitorLog
	where cast (DateAdded as date)=cast (GETDATE() as date)
	GROUP BY cast (DateAdded as date),PageURL) as OurTable ' + @sWhere + ' ) as TableMain' 
	  + ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
 
	-- Extend WHERE to include paging:

	SET @sSelectCount ='SELECT COUNT(*) AS TotalRows FROM (SELECT visiteddate,PageURL,TotalCount FROM 
	(SELECT cast ([DATE] as date)as visiteddate,PageURL,[Count] as TotalCount 
	FROM [PageCount] 
	UNION
	SELECT cast (DateAdded as date) as visiteddate ,PageURL,COUNT(*) as TotalCount   FROM VisitorLog
	where cast (DateAdded as date)=cast (GETDATE() as date)
	GROUP BY cast (DateAdded as date),PageURL) as OurTable ' + @sWhere + ' ) as TableMain'
 
	EXEC (@sSelect)
	PRINT @sSelect
	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('PageCount_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
