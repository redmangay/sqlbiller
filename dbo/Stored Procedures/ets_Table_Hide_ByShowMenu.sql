﻿

CREATE PROCEDURE [dbo].[ets_Table_Hide_ByShowMenu]
(
	@nRoleID INT	
)
AS

--EXEC [dbo].[ets_Table_Hide_ByShowMenu] 1623



BEGIN TRY


	SELECT     Distinct TableID 
		FROM         RoleTable
		WHERE     (RoleID = @nRoleID ) AND ShowMenu=0




END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Table_Hide_ByShowMenu', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
