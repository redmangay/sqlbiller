﻿

CREATE PROCEDURE [dbo].[ets_Recent_Uploads]
(
	@nAccountID int
)
AS
-- EXEC ets_Recent_Uploads @nAccountID=13

BEGIN TRY
	
	SELECT TOP 10 [Batch].BatchID,[Batch].TableID,TableName,[Batch].DateAdded,(SELECT     COUNT(*) AS Expr1
		FROM          TempRecord WHERE BatchID=Batch.BatchID and RejectReason is NULL and WarningReason is NULL) AS ValidCount,
		(SELECT     COUNT(*) AS Expr1
		FROM          TempRecord WHERE BatchID=Batch.BatchID and RejectReason is NULL and WarningReason is not NULL) AS WarningCount,                             
		(SELECT     COUNT(*) AS Expr1
		FROM          TempRecord WHERE BatchID=Batch.BatchID and RejectReason is not NULL) 
		AS NotValidCount                    FROM [Batch] INNER JOIN [User] ON Batch.UserIDUploaded = [User].UserID INNER JOIN [Table]
		ON Batch.TableID=[Table].TableID  WHERE  Batch.AccountID = @nAccountID

	
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Recent_Uploads', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
