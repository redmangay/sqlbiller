﻿

CREATE PROCEDURE [dbo].[ets_Record_Detail_Full]
(
	@nRecordID int,
	@nTableID int = NULL,
	@bUseArchiveDatabase bit = NULL
)
AS

/*
=========================
CHANGE HISTORY
=========================
Red 04 Jan 18: UPDATE the SP for Archive Database
Red 08 Jan 18: Added new param, nTableID, for archive database




===============
Testing
===============

EXEC [dbo].[ets_Record_Detail_Full] 1360176,4230,1



*/



BEGIN TRY

	DECLARE @SQLExec varchar(MAX)
	DECLARE @SELECT varchar(MAX) = 'SELECT * '
	DECLARE @FROM varchar(MAX) = 'FROM [Record]'
	DECLARE @WHERE varchar(MAX) =  ' WHERE RecordID=' + cast(@nRecordID as varchar)
	DECLARE @ArchiveDatabase varchar(MAX)




		IF @bUseArchiveDatabase =1 
		BEGIN
			SELECT @ArchiveDatabase = ArchiveDatabase FROM [Table]
			WHERE TableID = @nTableID

			IF @ArchiveDatabase <> ''
			BEGIN
				SET @FROM = 'FROM ['+ @ArchiveDatabase +'].[dbo].[Record]'
			END
		END

		SET @SQLExec = @SELECT + @FROM + @WHERE
		--PRINT @SQLExec
		EXEC (@SQLExec);
	
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Record_Detail_Full', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
