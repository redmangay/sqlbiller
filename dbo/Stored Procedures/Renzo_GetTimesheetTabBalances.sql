﻿-- =============================================
-- Author:		<Red Mangay>
-- Create date:	<28/11/2018>
-- Description:	<>
-- =============================================
/*

exec [Renzo_GetTimesheetTabBalances]  16671909

*/


CREATE PROCEDURE [dbo].[Renzo_GetTimesheetTabBalances]
(
	@RecordID int
	
)
AS
BEGIN
	
    DECLARE @TaxAmount money
    DECLARE @TaxName varchar(max)
    DECLARE @TotalAmount money
	DECLARE @TaxRate money

	
    SELECT  @TaxRate = (cast(J.V011 as money)/100)
	   FROM Record TD
	   JOIN REcord TS ON TS.TABLEID = 2488
	   AND TS.ISACTIVE = 1
	   AND TS.RECORDID = CAST(TD.V011 AS INT)
	   JOIN RECORD J ON J.TABLEID = 2475
	   AND J.ISACTIVE = 1
	   AND J.RECORDID = TD.V001
	   WHERE TD.ISACTIVE=1 AND TD.TableID='2729'  
		  AND CAST(TD.V001 as int) = @RecordID 
		  AND TD.V016 IS NULL
    
    /*Timehseet Details tab: Get the Tax Amount */
    SELECT @TaxAmount = Round(ISNULL(SUM(CAST(CONVERT(VARCHAR(20),V007,1) AS money)),0),2) *  @TaxRate
	   FROM Record 
		  WHERE ISACTIVE=1 AND TableID='2729' 
		  AND V001= CAST(@RecordID AS VARCHAR) 
		  AND V016 IS NULL	

      /*Timehseet Details tab: Get the Tax Name */
    SELECT @TaxName = V001 FROM RECORD WHERE RECORDID = (SELECT V042 FROM RECORD WHERE RECORDID =@RecordID)

      /*Timehseet Details tab: Get the Total */
    SELECT @TotalAmount = ISNULL(SUM(CAST(CONVERT(VARCHAR(20),V007,1) AS money)),0) --+ Round(ISNULL(SUM(CAST(CONVERT(VARCHAR(20),ISNULL(V012,0),1) AS money)),0),2)
    FROM Record 
	   WHERE ISACTIVE=1 
		  AND TableID='2729'  
			 AND V001=cast(@RecordID as varchar)
				AND V016 IS NULL 
    SET @TotalAmount = @TotalAmount + @TaxAmount
    
    SELECT @TaxName AS [Label], @TaxAmount AS [Amount]
    UNION ALL
    SELECT 'Total' AS [Label], @TotalAmount AS [Amount]





END

