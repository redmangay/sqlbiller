﻿

CREATE PROCEDURE   [dbo].[eContractor_Copy_Account] 
(
	@nTemplateAccountID INT,
	@nTargetAccountID INT,
	@nSignUpUserID INT
)
AS 
-- MODIFICATION HISTORY
-- 
-- 20 Sep 2018 JB: Various changes for OnTask
-- 25 Sep 2018 JB: Update the menu 
-- 14 May 2019 MR: Fixed ChecklistTemplate link
-- 15 May 2019 MR: Fixed ChecklistTemplate attachement link

BEGIN TRY

	DECLARE @StartTime datetime = GETDATE()
                
	---------------------------------------------------------------------------------------------
	-- Update the account record (it must have been created previously)
	---------------------------------------------------------------------------------------------   
	SET @nTemplateAccountID = @nTemplateAccountID -- testing
	DECLARE @TemplateAccHolderID int = [dbo].[fnGetAccountHolderUserID] (@nTemplateAccountID)
	DECLARE @tSQL varchar(MAX)=''

	UPDATE [Account] SET
			Logo=Template.Logo, 
			UseDefaultLogo=Template.UseDefaultLogo,
			MasterPage=Template.MasterPage 
		FROM [Account] 
		JOIN [Account] Template ON Template.AccountID = @nTemplateAccountID 
		WHERE [Account].AccountID = @nTargetAccountID


	---------------------------------------------------------------------------------------------
	-- Copy tables
	---------------------------------------------------------------------------------------------
	--SELECT * FROM [Table] WHERE AccountID = @nTemplateAccountID AND IsActive = 1
	PRINT 'Copy Table Performance Check:' + CAST(DATEDIFF(ms, @StartTime, GETDATE()) AS varchar(100))
	
	DECLARE @Cur_Table TABLE ([Counter] int IDENTITY(1,1), TableID INT, CopyDataWithTemplate bit null)

	INSERT INTO @Cur_Table (TableID,CopyDataWithTemplate)
			SELECT TableID,CopyDataWithTemplate--,[TableName]
				FROM [Table] 
				WHERE AccountID=@nTemplateAccountID
				AND [Table].IsActive = 1
				-- we want all tables -- AND [TableName] NOT LIKE '%template%'

	DECLARE @Counter int = 1,
			@nSourceTableID int ,
			@bCopyDataWithTemplate bit

	WHILE EXISTS(SELECT * FROM @Cur_Table WHERE [Counter] >= @Counter)
	BEGIN

		SELECT @nSourceTableID = TableID,@bCopyDataWithTemplate=CopyDataWithTemplate
			FROM @Cur_Table WHERE [Counter] = @Counter

		IF @bCopyDataWithTemplate is null
			SET @bCopyDataWithTemplate=0

	     EXEC ets_CopyTable @nSourceTableID, @nTargetAccountID,@nSignUpUserID,@bCopyDataWithTemplate--,@bCreateMenus=0

		PRINT 'Copy Table ' + CAST(@nSourceTableID AS varchar) + ' Performance Check:' + CAST(DATEDIFF(ms, @StartTime, GETDATE()) AS varchar(100))

		SET @Counter = @Counter + 1
	END 

	UPDATE Menu SET ShowOnMenu=0 WHERE Menu='--None--' AND AccountID=@nTargetAccountID


	---------------------------------------------------------------------------------------------
	-- Copy TableChild
	---------------------------------------------------------------------------------------------
	INSERT INTO [dbo].[TableChild]
		([ParentTableID],[ChildTableID],[Description],[DetailPageType],[ShowAddButton],[ShowEditButton]
           ,[DisplayOrder],[ConditionColumnID],[ConditionValue],[ConditionOperator],[FilterSQL],[DocumentID])
	
			SELECT NewParent.TableID as [ParentTableID], NewChild.TableID as [ChildTableID], TC.[Description], TC.[DetailPageType], TC.[ShowAddButton], TC.[ShowEditButton],
					TC.[DisplayOrder], TC.[ConditionColumnID], TC.[ConditionValue], TC.[ConditionOperator], TC.[FilterSQL], TC.[DocumentID]
					--C.SystemName, TC.* 
				FROM [TableChild] TC
				JOIN [Table] TemplateParent	ON TC.ParentTableID = TemplateParent.TableID 
				JOIN [Table] TemplateChild	ON TC.ChildTableID = TemplateChild.TableID 
				JOIN [Table] NewParent ON NewParent.SystemName = TemplateParent.SystemName AND NewParent.AccountID = @nTargetAccountID
				JOIN [Table] NewChild ON NewChild.SystemName = TemplateChild.SystemName AND NewChild.AccountID = @nTargetAccountID
				LEFT JOIN [Column] C ON C.ColumnID = TC.ConditionColumnID 
				LEFT JOIN [Column] NewC ON C.SystemName = C.SystemName AND NewC.TableID = NewParent.TableID
				WHERE TemplateParent.AccountID=@nTemplateAccountID 
				AND TemplateParent.IsActive=1 AND TemplateChild.IsActive=1
				AND TC.DetailPageType != 'not'
				AND TemplateParent.AccountID = @nTemplateAccountID 
				-- this is a mistake - we want the relationship with the children AND TemplateParent.TableName NOT LIKE '%TEMPLATE%'
	
	PRINT 'TableChild ALL DONE'
	
	/* Should we commment this out? 
	-- Commented out on 31 Oct 2018
	SELECT * FROM [TableChild] TC 
		JOIN [Table] T ON T.TableID = TC.ParentTableID 
		JOIN [Table] TemplateChild	ON TC.ChildTableID = TemplateChild.TableID 
		JOIN [Table] TemplateParent	ON TC.ParentTableID = TemplateParent.TableID 
		JOIN [Table] NewParent ON NewParent.SystemName = TemplateParent.SystemName AND NewParent.AccountID = @nTargetAccountID
		WHERE T.AccountID = @nTemplateAccountID AND t.IsActive = 1 -- 22
	*/
	---------------------------------------------------------------------------------------------
	-- Table Relationships
	---------------------------------------------------------------------------------------------
	PRINT 'Relations Performance Check:' + CAST(DATEDIFF(ms, @StartTime, GETDATE()) AS varchar(100))

	UPDATE [Column]
		SET TableTableID = NewTable.TableID, LinkedParentColumnID = NewTC.ColumnID
		-- SELECT [Column].TableTableID, NewTable.TableID AS [New TableTableID], [Column].LinkedParentColumnID, NewTC.ColumnID as [New LinkedParentColumnID], [Column].*
		FROM [Column]
		JOIN  [Table] T ON T.TableID = [Column].TableID 
		JOIN [Table] OldTable ON OldTable.TableID = [Column].TableTableID
		JOIN [Table] NewTable ON NewTable.SystemName = OldTable.SystemName AND NewTable.AccountID = @nTargetAccountID
		JOIN [Column] TC ON TC.ColumnID = [Column].LinkedParentColumnID 
		JOIN [Column] NewTC ON NewTC.SystemName = TC.SystemName AND NewTC.TableID = NewTable.TableID 
		WHERE T.AccountID = @nTargetAccountID
		AND [Column].TableTableID IS NOT NULL

    PRINT 'LinkedParentColumnID ALL DONE'
	--work on Conditions table

	---------------------------------------------------------------------------------------------
	-- Update the related data (applies to Ontask only)
	-- maybe we can make this generic one day!
	--------------------------------------------------------------------------------------------- 
	-- DECLARE @nTargetAccountID INT = 27341
	DECLARE @NewChecklistTemplate int, @NewChecklistTemplateItem int
	SELECT @NewChecklistTemplate = TableID FROM [Table] WHERE [SystemName] = 'Checklist Template' AND AccountID = @nTargetAccountID
	SELECT @NewChecklistTemplateItem = TableID FROM [Table] WHERE [SystemName] = 'Checklist Template Item' AND AccountID = @nTargetAccountID
	
	--v500 has related recordid
	UPDATE TI  SET TI.V001=(SELECT TT.RecordID FROM [Record] TT WHERE TT.V500=TI.V001 AND TT.TableID=@NewChecklistTemplate)
	FROM [Record] TI
	 WHERE TI.TableID=@NewChecklistTemplateItem AND TI.V001 IS NOT NULL

	  --link template attachment records 
	 DECLARE @NewAttachment int
	 SELECT @NewAttachment = TableID FROM [Table] WHERE [SystemName] = 'Attachment' AND AccountID = @nTargetAccountID

	 UPDATE TA  SET TA.V010=(SELECT TT.RecordID FROM [Record] TT WHERE TT.V500=TA.V010 AND TT.TableID=@NewChecklistTemplateItem)
	FROM [Record] TA
	 WHERE TA.TableID=@NewAttachment AND TA.V010 IS NOT NULL

	--Commented out on 14-May-2019 by MR
	--UPDATE R SET R.V001 = TR.RecordID
	--	FROM [Record] T 
	--	JOIN [Record] R ON R.V005 = T.V005
	--	JOIN [Record] TP ON T.V001 = TP.RecordID
	--	JOIN [Record] TR ON TR.V003 = TP.V003
	--	WHERE T.TableID= 6523
	--	AND R.TableID  = @NewChecklistTemplateItem /* Checklist Template Item */
	--	AND TP.TableID = 6522
	--	AND TR.TableID = @NewChecklistTemplate /* Checklist Template */

	---------------------------------------------------------------------------------------------
	-- Show When
	---------------------------------------------------------------------------------------------
	PRINT 'Show When Performance Check:' + CAST(DATEDIFF(ms, @StartTime, GETDATE()) AS varchar(100))

	--INSERT INTO Conditions(ColumnID,ConditionColumnID,ConditionValue,ConditionOperator,DisplayOrder,JoinOperator) 
	--	SELECT NewC.ColumnID, NewHC.ColumnID,
	--		SW.ConditionValue,SW.ConditionOperator, SW.DisplayOrder,SW.JoinOperator 
	--		FROM [Conditions] SW
	--		JOIN [Column] C ON C.ColumnID = SW.ColumnID 
	--		JOIN [Column] HC ON C.ColumnID = SW.ConditionColumnID 
	--		JOIN [Table] T ON T.TableID = C.TableID 
	--		JOIN [Table] NewTable ON NewTable.SystemName = T.SystemName AND NewTable.AccountID = @nTargetAccountID
	--		JOIN [Column] NewC ON NewC.SystemName = C.SystemName AND NewC.TableID = NewTable.TableID 
	--		JOIN [Column] NewHC ON NewHC.SystemName = HC.SystemName AND NewHC.TableID = NewTable.TableID
	--		WHERE T.AccountID = @nTargetAccountID

	INSERT INTO Conditions(ColumnID,ConditionColumnID,ConditionValue,ConditionOperator,DisplayOrder,JoinOperator) 
	SELECT NewC.ColumnID, NewHC.ColumnID,
			SW.ConditionValue,SW.ConditionOperator, SW.DisplayOrder,SW.JoinOperator 
			FROM [Conditions] SW
			JOIN [Column] C ON C.ColumnID = SW.ColumnID 
			LEFT JOIN [Column] HC ON HC.ColumnID = SW.ConditionColumnID 
			JOIN [Table] T ON T.TableID = C.TableID 
			JOIN [Table] NewTable ON NewTable.SystemName = T.SystemName 
			JOIN [Column] NewC ON NewC.SystemName = C.SystemName AND NewC.TableID = NewTable.TableID 
			JOIN [Column] NewHC ON NewHC.SystemName = HC.SystemName AND NewHC.TableID = NewTable.TableID
			WHERE T.AccountID = @nTemplateAccountID AND NewTable.AccountID = @nTargetAccountID
	
	INSERT INTO Conditions(ColumnID,ConditionColumnID,ConditionValue,ConditionOperator,DisplayOrder,JoinOperator) 
	SELECT NewC.ColumnID,null,
			SW.ConditionValue,SW.ConditionOperator, SW.DisplayOrder,SW.JoinOperator 
			FROM [Conditions] SW
			JOIN [Column] C ON C.ColumnID = SW.ColumnID 
			JOIN [Table] T ON T.TableID = C.TableID 
			JOIN [Table] NewTable ON NewTable.SystemName = T.SystemName 
			JOIN [Column] NewC ON NewC.SystemName = C.SystemName AND NewC.TableID = NewTable.TableID 
			WHERE SW.ConditionColumnID is null and T.AccountID = @nTemplateAccountID AND NewTable.AccountID = @nTargetAccountID

					   


			
	PRINT 'Conditions all DONE'
	-- lets work on Dashboard
   
	---------------------------------------------------------------------------------------------
	-- Dashboard
	---------------------------------------------------------------------------------------------
	INSERT INTO Document 
			(AccountID, DocumentText, DocumentTypeID, UniqueName, FileTitle, DocumentDate, DateAdded, DateUpdated, 
			 UserID, TableID, ReportHTML, IsReportPublic, DocumentDescription, DocumentEndDate, ForDashBoard, FolderID, Size)
		SELECT @nTargetAccountID,DocumentText, NULL, UniqueName, FileTitle, DocumentDate, DateAdded, DateUpdated, 
			@nSignUpUserID, NULL, ReportHTML, IsReportPublic, DocumentDescription, DocumentEndDate, ForDashBoard, NULL, Size 
			FROM Document 
			WHERE ForDashBoard=1 AND AccountID=@nTemplateAccountID AND Document.UserID=@TemplateAccHolderID                                     
   
  
   -- need to work with DocumentSection
	-- excluded DocumentSectionStyleID


	DECLARE @Cur_DocumentSection TABLE
	(
		[Counter_DocumentSection] int identity(1,1),
		DocumentSectionID INT,
		DocumentID INT NULL,
		ParentSectionID INT NULL, 
		DateAdded datetime null
	)

	INSERT INTO @Cur_DocumentSection (DocumentSectionID,DocumentID,ParentSectionID,DateAdded)
			SELECT DocumentSectionID,DocumentSection.DocumentID,ParentSectionID,Document.DateAdded
			FROM DocumentSection 
			INNER JOIN Document	ON DocumentSection.DocumentID=Document.DocumentID
			WHERE ForDashBoard=1 AND Document.AccountID=@nTemplateAccountID AND Document.UserID=@TemplateAccHolderID


	DECLARE @Counter_DocumentSection  int
	SET @Counter_DocumentSection = 1
	DECLARE @nSDocumentSectionID int;
	DECLARE @nSDocumentID INT;
	DECLARE @sSParentSectionID  INT;
	DECLARE @dDateAdded datetime;

	WHILE EXISTS(SELECT * FROM @Cur_DocumentSection WHERE [Counter_DocumentSection] >= @Counter_DocumentSection)
	BEGIN
		SELECT @nSDocumentSectionID=DocumentSectionID,@nSDocumentID=DocumentID,@sSParentSectionID=ParentSectionID,@dDateAdded=DateAdded
			FROM @Cur_DocumentSection WHERE [Counter_DocumentSection] = @Counter_DocumentSection
	
			-- why? - MR
			SELECT @nSDocumentID=DocumentID FROM Document WHERE AccountID=@nTargetAccountID
			AND DateAdded=@dDateAdded and ForDashBoard=1
		
			IF @sSParentSectionID IS NOT NULL
			BEGIN
				DECLARE @nPosition int
				SELECT @sSParentSectionID=DocumentID,@nPosition=Position FROM DocumentSection 
				WHERE DocumentSectionID=@sSParentSectionID
					
				SELECT @dDateAdded=DateAdded FROM Document WHERE DocumentID=@sSParentSectionID
			
				SELECT @sSParentSectionID=DocumentID FROM Document WHERE AccountID=@nTargetAccountID
				AND DateAdded=@dDateAdded
			
				IF @sSParentSectionID IS  NOT NULL
				SELECT @sSParentSectionID=DocumentSectionID FROM DocumentSection
				WHERE DocumentID=@sSParentSectionID AND Position=@nPosition
			END
		
			IF @nSDocumentID IS NOT NULL		
			INSERT INTO DocumentSection (DocumentID, DocumentSectionTypeID, Position, SectionName, [Content], Filter, Details, DateAdded, DateUpdated, DocumentSectionStyleID, 
				ValueFields, ParentSectionID, ColumnIndex)
				SELECT @nSDocumentID, DocumentSectionTypeID, Position, SectionName, [Content], Filter, Details, DateAdded, DateUpdated, NULL, 
				ValueFields, @sSParentSectionID, ColumnIndex
				FROM DocumentSection  WHERE DocumentSectionID=@nSDocumentSectionID
	    
				PRINT 'INSERT INTO DocumentSection EACH'
		
		
		SET @Counter_DocumentSection = @Counter_DocumentSection + 1
	END 
    


	PRINT 'Dash Performance Check:' + CAST(DATEDIFF(ms, @StartTime, GETDATE()) AS varchar(100))
	PRINT DATEDIFF(ms, @StartTime, GETDATE())
	 
	 --NO NEED Table
	 -- TableUser                   
	--DASHBOARD DATA
	
--declare  @nTemplateAccountID int=@nTemplateAccountID
--declare @nTargetAccountID int =27124
	DECLARE @Dash_Table TABLE
		(
			[Counter] int identity(1,1),
			sTableID INT,
			sTableName nvarchar(225),
			tTableID INT null,
			sColumnID int,
			sSystemName varchar(50),
			tColumnID INT NULL

		)

	INSERT INTO @Dash_Table (sTableID,sTableName,sColumnID,sSystemName)
			SELECT T.TableID,T.TableName,C.ColumnID,C.SystemName
				FROM [Table] T JOIN [Column] C ON T.TableID=C.TableID
				WHERE T.AccountID=@nTemplateAccountID AND T.IsActive=1


	DECLARE @Counter_DashTable  int
	SET @Counter_DashTable = 1
	WHILE EXISTS(SELECT * FROM @Dash_Table WHERE [Counter] >= @Counter_DashTable)
	BEGIN
		UPDATE @Dash_Table SET tTableID=(SELECT Top 1 TableID FROM [Table] WHERE AccountID=@nTargetAccountID and
		TableName=sTableName and IsActive=1)
		WHERE [Counter] = @Counter_DashTable

		UPDATE @Dash_Table SET tColumnID=(SELECT Top 1 ColumnID FROM [Table] T JOIN [Column] C ON T.TableID=C.TableID
		 WHERE T.AccountID=@nTargetAccountID and T.TableID=tTableID AND C.SystemName=sSystemName)
		WHERE [Counter] = @Counter_DashTable

		
	    --print CAST(@Counter_DashTable AS VARCHAR)

		SET @Counter_DashTable = @Counter_DashTable + 1
	END 

	SET @Counter_DashTable = 1
	DECLARE @sTableID INT
	DECLARE @tTableID INT
	DECLARE @sColumnID INT
	DECLARE @tColumnID INT

	WHILE EXISTS(SELECT * FROM @Dash_Table WHERE [Counter] >= @Counter_DashTable)
	BEGIN
		SELECT @sTableID=sTableID,@tTableID=tTableID,@sColumnID=sColumnID,@tColumnID=tColumnID FROM @Dash_Table
		WHERE [Counter] = @Counter_DashTable
	   	 	
		IF EXISTS( SELECT DS.DocumentSectionID FROM DocumentSection DS JOIN Document D ON D.DocumentID=DS.DocumentID
		WHERE D.AccountID=@nTargetAccountID AND DS.Details LIKE '%:'+CAST(@sTableID AS VARCHAR)+',%' )
			BEGIN
				--select DocumentSectionID,REPLACE(Details,':'+CAST(@sTableID AS varchar)+',',':'+CAST(@tTableID AS varchar)+',') from DocumentSection
				--WHERE DocumentSectionID IN ( SELECT DS.DocumentSectionID FROM DocumentSection DS JOIN Document D ON D.DocumentID=DS.DocumentID
				--WHERE D.AccountID=@nTargetAccountID AND DS.Details LIKE '%:'+CAST(@sTableID AS VARCHAR)+',%' )
				-- AND Details LIKE '%:'+CAST(@sTableID AS VARCHAR)+',%'
				UPDATE DocumentSection SET Details=REPLACE(Details,':'+CAST(@sTableID AS varchar)+',',':'+CAST(@tTableID AS varchar)+',') from DocumentSection
				WHERE DocumentSectionID IN ( SELECT DS.DocumentSectionID FROM DocumentSection DS JOIN Document D ON D.DocumentID=DS.DocumentID
				WHERE D.AccountID=@nTargetAccountID AND DS.Details LIKE '%:'+CAST(@sTableID AS VARCHAR)+',%' )
				 AND Details LIKE '%:'+CAST(@sTableID AS VARCHAR)+',%'

			END

		IF EXISTS( SELECT DS.DocumentSectionID FROM DocumentSection DS JOIN Document D ON D.DocumentID=DS.DocumentID
		WHERE D.AccountID=@nTargetAccountID AND DS.Details LIKE '%:'+CAST(@sColumnID AS VARCHAR)+',%' )
			BEGIN
				--select DocumentSectionID,REPLACE(Details,':'+CAST(@sColumnID AS varchar)+',',':'+CAST(@tColumnID AS varchar)+',') from DocumentSection
				--WHERE DocumentSectionID IN ( SELECT DS.DocumentSectionID FROM DocumentSection DS JOIN Document D ON D.DocumentID=DS.DocumentID
				--WHERE D.AccountID=@nTargetAccountID AND DS.Details LIKE '%:'+CAST(@sColumnID AS VARCHAR)+',%' )
				-- AND Details LIKE '%:'+CAST(@sColumnID AS VARCHAR)+',%'
				UPDATE DocumentSection SET Details=REPLACE(Details,':'+CAST(@sColumnID AS varchar)+',',':'+CAST(@tColumnID AS varchar)+',') from DocumentSection
				WHERE DocumentSectionID IN ( SELECT DS.DocumentSectionID FROM DocumentSection DS JOIN Document D ON D.DocumentID=DS.DocumentID
				WHERE D.AccountID=@nTargetAccountID AND DS.Details LIKE '%:'+CAST(@sColumnID AS VARCHAR)+',%' )
				 AND Details LIKE '%:'+CAST(@sColumnID AS VARCHAR)+',%'

			END


		SET @Counter_DashTable = @Counter_DashTable + 1
	END 

                     
	---------------------------------------------------------------------------------------------
	-- Service
	---------------------------------------------------------------------------------------------
	IF EXISTS( SELECT * FROM [Service] WHERE AccountID=@nTemplateAccountID)
	BEGIN
		INSERT INTO [Service] (AccountID,ColumnSystemName,IsServerCode,ProcessNotes,SameEventOrder,ServiceDefinition,ServiceJSON,ServiceType)
		SELECT  @nTargetAccountID,s.ColumnSystemName,s.IsServerCode,s.ProcessNotes,s.SameEventOrder,s.ServiceDefinition,s.ServiceJSON,s.ServiceType
		 FROM [Service] s WHERE AccountID=@nTemplateAccountID
	END

	------
	
	---------------------------------------------------------------------------------------------
	-- Terminology
	---------------------------------------------------------------------------------------------
	INSERT INTO Terminology (AccountID, PageName, InputText, OutputText)
		SELECT @nTargetAccountID,PageName, InputText, OutputText
		FROM Terminology WHERE AccountID=@nTemplateAccountID


	---------------------------------------------------------------------------------------------
	-- Update Menus
	---------------------------------------------------------------------------------------------
	DELETE FROM [Menu] WHERE ParentMenuID IN (SELECT MenuID FROM [Menu] WHERE [AccountID] = @nTargetAccountID)
	DELETE FROM [Menu] WHERE [AccountID] = @nTargetAccountID

	-- Copy the menus from the template account, fixing up the TableIDs as we go:
	INSERT INTO [Menu] ([Menu], [AccountID], [ShowOnMenu], [IsActive], [DisplayOrder], [ParentMenuID], [TableID])
		SELECT [Menu], @nTargetAccountID, [ShowOnMenu], [Menu].[IsActive], [Menu].[DisplayOrder], NULL, New.[TableID] 
			FROM [Menu]
			LEFT JOIN [Table] Old ON Old.TableID = [Menu].TableID 
			LEFT JOIN [Table] New ON New.SystemName = Old.SystemName AND New.AccountID = @nTargetAccountID
			WHERE [Menu].AccountID = @nTemplateAccountID AND [Menu].IsActive = 1

	-- Fix up the ParentMenuIDs:
	UPDATE [Menu] SET [ParentMenuID] = New.MenuID
	--SELECT [Menu].[MenuID], Template.[MenuID], Template.[ParentMenuID], TemplateParent.MenuID, New.MenuID
		FROM [Menu] 
		JOIN [Menu] Template ON Template.Menu = [Menu].Menu  
		JOIN [Menu] TemplateParent ON TemplateParent.MenuID = Template.ParentMenuID
		JOIN [Menu] New ON New.Menu = TemplateParent.Menu
		WHERE [Menu].AccountID = @nTargetAccountID
		AND Template.AccountID = @nTemplateAccountID
		and TemplateParent.AccountID = @nTemplateAccountID
		AND New.AccountID = @nTargetAccountID

	
	PRINT 'Final Performance Check:' + CAST(DATEDIFF(ms, @StartTime, GETDATE()) AS varchar(100))
	--COMMIT TRANSACTION --finally, Commit 

END TRY

BEGIN CATCH
	PRINT @@ERROR
 
  SELECT
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage;
 
	--ROLLBACK TRANSACTION --RollBack Transaction 
END CATCH

