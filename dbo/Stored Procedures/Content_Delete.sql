﻿

CREATE PROCEDURE [dbo].[Content_Delete]
(
	@nContentID  int
)
AS

BEGIN TRY
	BEGIN TRANSACTION --beginning a transaction..

		BEGIN
			
			DELETE FROM    Content 
			   WHERE ContentID=@nContentID
			 
			--DELETE FROM    Content 
			  -- WHERE MainContentID=@nContentID
			
		END

	COMMIT TRANSACTION --finally, Commit the transaction as all Success.

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Content_Delete', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
