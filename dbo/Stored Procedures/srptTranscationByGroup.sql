﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[srptTranscationByGroup]
	@StartDate DateTime = null,
	@EndDate DateTime = null,@Group_Divison varchar(100)=''
AS
BEGIN

IF OBJECT_ID('tempdb..#tempTableGroupForDateRange') IS NOT NULL
begin
        drop table #tempTableGroupForDateRange
end

SELECT p.[Group/Division] AS GD,CONVERT(Datetime, b.[Date], 103)  as [Date]  , b.[Project Number], b.Description, b.[Payment Amount Converted],
 b.[Invoice Amount Converted] AS ['Invoice Amount Converted'], CurrencyRate, 
 b.[Debtors Converted] AS ['Debtors Converted'], b.[Bad Debt Exp Converted], b.Type,
   p.RptCurrencyRate, b.[Rpt Debtors Converted] AS ['Rpt Debtors Converted'],  
  p.Currency, b.ID, b.[Group Currency] AS Group_Currency,(Select Symbol from [thedatabase_windtech].
[Account24918].[vCurrency Rates] where Currency=b.[Group Currency]) AS CurrencySymbol
  into #tempTableGroupForDateRange
 
FROM [thedatabase_windtech].[Account24918].[vBilling Information] AS b, 
[thedatabase_windtech].[Account24918].[vProject] AS p
Where (b.[Project Number]=p.ProjectNumber) AND (b.Type<>7) 

Select * from #tempTableGroupForDateRange
Where (Date BETWEEN @StartDate And @EndDate ) 
AND GD=@Group_Divison

ORDER BY Date, [Project Number]

END
