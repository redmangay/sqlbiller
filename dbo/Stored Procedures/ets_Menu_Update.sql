﻿

CREATE PROCEDURE [dbo].[ets_Menu_Update]
(
	@nMenuID int ,
	@sMenu nvarchar(255) ,
	@nAccountID int  ,
	@bShowOnMenu bit ,@bIsActive bit  = 1,
	@nParentMenuID int =NULL,
	@nTableID INT=NULL,
	@nDocumentID int=NULL,	
	@sExternalPageLink nvarchar(1000)=NULL,
	@bOpenInNewWindow bit=NULL,
	@nDocumentTypeID INT=NULL,
	@sMenuType varchar(10)=NULL,
	@bIsAdvancedSecurity bit = 0,
	@nReportID int = null-- Added by Red 04092019
)
	/*----------------------------------------------------------------
	-- Stored Procedure: ets_Menu_Update
	-- Produced by: DB Gurus (SPGen) 
	-- Date: Dec  3 2010  6:35PM
	---------------------------------------------------------------*/
 
AS
 
BEGIN TRY 
	SELECT @sMenu=dbo.fnNextAvailableMenu(@sMenu,@nAccountID,@nParentMenuID,@nMenuID)
	UPDATE Menu
	SET 
		Menu = @sMenu,
		AccountID = @nAccountID,
		ShowOnMenu = @bShowOnMenu,
		IsActive=@bIsActive,
		ParentMenuID=@nParentMenuID,
		TableID=@nTableID,
		DocumentID=@nDocumentID,
		ExternalPageLink=@sExternalPageLink,
		OpenInNewWindow=@bOpenInNewWindow,
		DocumentTypeID=@nDocumentTypeID,
		MenuType=@sMenuType,
		IsAdvancedSecurity = @bIsAdvancedSecurity,
		ReportID = @nReportID

	WHERE MenuID = @nMenuID
END TRY
-----------------------------------------------------------------
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Menu_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

