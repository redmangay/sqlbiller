﻿

CREATE PROCEDURE   [dbo].[ets_TempRecord_List]
(
	--@nAccountID int,
	@nBatchID int,
	@nTableID int,
--	@sSearch varchar(MAX) = NULL,
	--@nEnteredBy int = NULL,
	@bHasRejectReason bit = 0,
	@bHasWarningReason bit = NULL,
	@bIsActive bit = NULL,
	@dDateFrom date = NULL,
	@dDateTo date = NULL,
	@sOrder nvarchar(200)=NULL , 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647,
	@sTextSearch varchar(MAX) = NULL,
	@nImportTemplateID INT=NULL,
	@sType varchar(20)=NULL
)
AS
/*

EXEC [ets_TempRecord_List] @nBatchID=-1,@nTableID=2930,@nImportTemplateID=75

*/
BEGIN TRY

		SET DATEFORMAT dmy;
		SET NOCOUNT ON;

		DECLARE @tDisplayColumns TABLE
		(
			ID int identity(1,1),
			ColumnID int,
			SystemName varchar(50),
			DisplayText varchar(100),
			FieldsToShow varchar(MAX),
			ParentTableID int,
			ParentJoinColumnName varchar(254), -- RecordID
			ChildJoinColumnName varchar(254), -- V001 / V499
			ShowViewLink varchar(10),
			ColumnType varchar(20),
			ColourCells bit
		)
																

		DECLARE @nColumnIDRecordID INT

		SELECT @nColumnIDRecordID=ColumnID FROM [Column] 
			WHERE TableID = @nTableID AND SystemName='RecordID'

		IF @sType IS NULL AND @nImportTemplateID IS NULL
				SELECT @nImportTemplateID=DefaultImportTemplateID FROM [Table] WHERE TableID = @nTableID

		IF @sType IS NULL AND @nImportTemplateID IS NULL
			SELECT top 1 @nImportTemplateID=ImportTemplateID FROM [ImportTemplate] WHERE TableID = @nTableID ORDER BY ImportTemplateID DESC

			--INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName, FieldsToShow, ParentTableID, ParentJoinColumnName,ChildJoinColumnName, ShowViewLink, ColumnType)
			--					SELECT C.ColumnID, C.NameOnImport, C.SystemName,
			--						dbo.fnReplaceDisplayColumns(C.DisplayColumn, C.[TableTableID],C.ColumnID) as [FieldsToShow],
			--						C.[TableTableID] AS  [ParentTableID], 
			--						PC.SystemName as [ParentJoinColumnName], 
			--						C.SystemName as [ChildJoinColumnName],
			--						C.ShowViewLink,
			--						C.ColumnType
			--						FROM [Column] C 
			--						LEFT OUTER JOIN [Column] PC ON PC.[ColumnID] = C.[LinkedParentColumnID]
			--						WHERE C.TableID = @nTableID 
			--						AND C.NameOnImport IS NOT NULL AND LEN(C.NameOnImport) > 0 AND C.NameOnImport<>'EnteredBy'
			--						ORDER BY C.DisplayOrder


		--[dbo].[RemoveSpecialChars]

		IF @sType IS NULL AND @nImportTemplateID IS NOT NULL
			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName, FieldsToShow, ParentTableID, ParentJoinColumnName,ChildJoinColumnName, ShowViewLink, ColumnType)
								SELECT C.ColumnID,  [dbo].[RemoveSpecialChars](ITI.ImportHeaderName), C.SystemName,
									dbo.fnReplaceDisplayColumns(C.DisplayColumn, C.[TableTableID],C.ColumnID) as [FieldsToShow],
									C.[TableTableID] AS  [ParentTableID], 
									PC.SystemName as [ParentJoinColumnName], 
									C.SystemName as [ChildJoinColumnName],
									C.ShowViewLink,
									C.ColumnType
									FROM [Column] C INNER JOIN ImportTemplateItem ITI
									ON C.ColumnID=ITI.ColumnID
									LEFT OUTER JOIN [Column] PC ON C.[LinkedParentColumnID]=PC.[ColumnID] 
									WHERE C.TableID = @nTableID AND ITI.ImportTemplateID=@nImportTemplateID
									AND ITI.ImportHeaderName IS NOT NULL AND LEN(ITI.ImportHeaderName) > 0  
									AND ITI.ImportHeaderName<>'EnteredBy' AND C.SystemName<>'EnteredBy'
									ORDER BY ITI.ColumnIndex



			IF @sType IS NOT NULL AND @sType='system'
				INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
									SELECT C.ColumnID, C.SystemName, C.SystemName
										FROM [Column] C 
										WHERE C.TableID = @nTableID 
										AND C.SystemName NOT IN ('EnteredBy','IsActive','TableID','RecordID')
										ORDER BY C.DisplayOrder

		DECLARE @isRecordID int = 0
		SELECT @isRecordID = COUNT(*) FROM [ImportTemplateItem] ITI INNER JOIN [Column] C
			ON ITI.[ColumnID] = C.[ColumnID]
			WHERE ITI.ImportTemplateID = @nImportTemplateID AND C.SystemName = 'RecordID'
		IF @isRecordID > 0
			UPDATE @tDisplayColumns SET SystemName = 'TempRecordID' WHERE SystemName = 'RecordID'

		INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
		VALUES (@nColumnIDRecordID,'DBGSystemRecordID','RecordID')


			DECLARE @SELECT varchar(MAX)
			DECLARE @FROM varchar(MAX)
			DECLARE @WHERE varchar(MAX)
			DECLARE @SQLMain varchar(MAX)

			--DECLARE @sSelectCount varchar(MAX)
			DECLARE @counter int
			SET @counter = 1
		

		  -- Get the sOrder IF it is NULL
		   WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID >= @counter)
			BEGIN
		
				IF @sOrder is NULL
				 SELECT @sOrder= ' [' + DisplayText  + ']' FROM @tDisplayColumns WHERE ID = @counter
				 SET @counter = @counter + 1
			END 
	
		  --SELECT @SELECT = 'SELECT * FROM (SELECT RecordInfo.*,ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ' ) as RowNum FROM (SELECT '
  
		  SELECT @SELECT = ''
		  SELECT @FROM = ' FROM [TempRecord] '

		  IF @bHasRejectReason =1
			SELECT @SELECT = @SELECT + ' TempRecord.RejectReason as [Reject Reason] , ' 
		   IF @bHasWarningReason is not NULL
			  IF @bHasWarningReason =1
			  SELECT @SELECT = @SELECT + ' TempRecord.WarningReason as [Warning Reason], ' 
       
			DECLARE @ParentAlias varchar(100)
			SET @counter = 1
			WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID >= @counter)
			BEGIN
	   
			   -- Check to see IF this column has a parent (which we will get data off)
				IF EXISTS(SELECT 1 FROM @tDisplayColumns WHERE ID = @counter AND ParentTableID IS NOT NULL AND ColumnType='dropdown' AND ParentTableID>-1)
					BEGIN
						SELECT @ParentAlias = 'P' +  + CAST(ColumnID as varchar) FROM @tDisplayColumns WHERE ID = @counter
						SELECT 
								-- build up the parent field anems
								@SELECT = @SELECT + FieldsToShow + ' AS [' + DisplayText + '], '
								+'',
								-- Build the join condition and a isnumeric check (for bad data) when joining to RecordID
								@FROM = @FROM + ' LEFT OUTER JOIN [Record] ' + @ParentAlias + ' ON ' 
									+ CASE WHEN ParentJoinColumnName = 'RecordID' 
										THEN  @ParentAlias + '.' + ParentJoinColumnName + ' = CASE WHEN (ISNUMERIC([TempRecord].' + ChildJoinColumnName +')=1 AND CHARINDEX(''.'',[TempRecord].' + ChildJoinColumnName +')=0) THEN [TempRecord].' + ChildJoinColumnName + ' ELSE -1 END '
										ELSE @ParentAlias + '.TableID = ' + cast(ParentTableID as varchar) 
										+ ' AND ' + @ParentAlias + '.' + ParentJoinColumnName + ' = [TempRecord].' + ChildJoinColumnName 
										END						
								+ ' AND ' + @ParentAlias + '.[IsActive] = 1' 
				
							FROM @tDisplayColumns WHERE ID = @counter 

					END
				ELSE IF EXISTS(SELECT 1 FROM @tDisplayColumns WHERE ID = @counter AND ParentTableID IS NOT NULL AND ColumnType='dropdown' AND ParentTableID=-1)
				BEGIN
					SELECT @SELECT = @SELECT + ' [dbo].[fnGetSystemUserDisplayText] ('''+FieldsToShow+''',TempRecord.' + SystemName + ') 
												AS [' + DisplayText + '], ' 										
						FROM @tDisplayColumns 
						WHERE ID = @counter
				
				
				END
		
				ELSE IF EXISTS(SELECT 1 FROM @tDisplayColumns WHERE ID = @counter AND ParentTableID IS NOT NULL AND ColumnType='listbox' AND ParentTableID>-1)
				BEGIN
					SELECT @SELECT = @SELECT + ' STUFF ( ( SELECT '','' + A FROM (SELECT '+dbo.fnReplaceDisplayColumns_NoAlias(ColumnID)+' as A FROM TempRecord X WHERE X.TableID='
					+CAST(ParentTableID AS varchar)+' AND CHARINDEX('',''+CAST(X.RecordID AS varchar)+'','','',''+ TempRecord.' + SystemName 
					+ ' + '','')>0 ) as MainInfo FOR XML PATH('''') ),1,1,'''') 
						 AS [' + DisplayText + '], ' 
						FROM @tDisplayColumns 
						WHERE ID = @counter
				
				
				END
				ELSE
				BEGIN		
						SELECT @SELECT = @SELECT + 'TempRecord.' + SystemName + ' AS [' + DisplayText + '], ' 
						FROM @tDisplayColumns 
						WHERE ID = @counter
				END
				--PRINT @SELECT
				SET @counter = @counter + 1
				IF @sOrder is NULL
				 SELECT @sOrder= ' [' + DisplayText  + ']' FROM @tDisplayColumns WHERE ID = @counter
			END 

			SELECT @SELECT = LEFT(@SELECT, LEN(@SELECT)-1) 
	
	
		--Make sure that FROM has all only required joins
	
	IF EXISTS (SELECT TOP 1 ColumnID FROM [Column] WHERE TableID=@nTableID AND TableTableID IS NOT NULL AND TableTableID<>-1 AND ColumnType='dropdown')
		BEGIN
			DECLARE @t2TableTable Table
			(
				ID int identity(1,1),
				ColumnID int,
				SystemName varchar(50),
				TableTableID INT				
			)

			INSERT INTO @t2TableTable(ColumnID,SystemName,TableTableID) 
				SELECT ColumnID,SystemName,TableTableID FROM [Column] 
					WHERE TableID=@nTableID AND TableTableID IS NOT NULL AND TableTableID>-1 AND ColumnType='dropdown'
				
			DECLARE @counter2 int
			SET @counter2 = 1
			WHILE EXISTS(SELECT * FROM @t2TableTable WHERE ID >= @counter2)
				BEGIN
						DECLARE @n2TableTableID INT
						SELECT @ParentAlias = 'P' + CAST(ColumnID as varchar),@n2TableTableID=TableTableID FROM @t2TableTable WHERE ID = @counter2
						IF CHARINDEX(@ParentAlias,@SELECT,1)>0 AND CHARINDEX(@ParentAlias,@FROM,1)=0
							BEGIN
								-- parent
								SELECT
									@FROM = @FROM + ' LEFT OUTER JOIN [TempRecord] ' + @ParentAlias + ' ON ' 
											+ @ParentAlias + '.RecordID = CASE WHEN ISNUMERIC([TempRecord].' + SystemName +')=1 THEN [TempRecord].' + SystemName + ' ELSE -1 END '
											+ ' AND ' + @ParentAlias + '.[IsActive] = 1' 
									FROM @t2TableTable WHERE ID = @counter2
							END	
							
							
						DECLARE @t3TableTable Table
							(
								ID int identity(1,1),
								ColumnID int,
								SystemName varchar(50),
								TableTableID INT				
							)
						
						INSERT INTO @t3TableTable(ColumnID,SystemName,TableTableID) 
							SELECT ColumnID,SystemName,TableTableID 
								FROM [Column] 
								WHERE TableID=@n2TableTableID AND TableTableID IS NOT NULL AND TableTableID>0 AND ColumnType='dropdown'
						DECLARE @counter3 int
						SET @counter3 = 1
						WHILE EXISTS(SELECT * FROM @t3TableTable WHERE ID >= @counter3)
							BEGIN
								DECLARE @s3ParentAlias varchar(25)
								SELECT @s3ParentAlias = 'P' + CAST(ColumnID as varchar) FROM @t3TableTable WHERE ID = @counter3
								IF CHARINDEX(@s3ParentAlias,@SELECT,1)>0 AND CHARINDEX(@s3ParentAlias,@FROM,1)=0
									BEGIN
										-- parent's parent
										SELECT
										@FROM = @FROM + ' LEFT OUTER JOIN [TempRecord] ' + @s3ParentAlias + ' ON ' 
											+ @s3ParentAlias + '.RecordID = CASE WHEN ISNUMERIC('+@ParentAlias+'.' + SystemName +')=1 THEN '+@ParentAlias+'.' + SystemName + ' ELSE -1 END '
											+ ' AND ' + @s3ParentAlias + '.[IsActive] = 1' 
											FROM @t3TableTable WHERE ID = @counter3
									END
								SET @counter3 = @counter3 + 1
							END				
					SET @counter2 = @counter2 + 1
				END

		END



			IF PATINDEX('%TempRecord.TableID%', @SELECT) > 0
			BEGIN
				SET @SELECT = REPLACE(@SELECT, 'TempRecord.TableID', '[Table].TableName')
				SET @FROM = REPLACE(@FROM, 'FROM [TempRecord]', 'FROM [TempRecord] JOIN [Table] ON TempRecord.TableID = [Table].TableID')
			END	

			--IF PATINDEX('%Record.EnteredBy%', @SELECT) > 0
			--BEGIN
			--	SET @SELECT = REPLACE(@SELECT, 'Record.EnteredBy', '[User].FullName')
			--	SET @SELECT = REPLACE(@SELECT, 'FROM [Record]', 'FROM [Record] JOIN [User] ON Record.EnteredBy = [User].UserID')
			--END	

			-- ADD IN THE WHERE CRITERIA
			SELECT @WHERE = ' WHERE TempRecord.TableID = ' + CAST(@nTableID as varchar)
			SELECT @WHERE = @WHERE + ' AND TempRecord.BatchID = ' + CAST(@nBatchID as varchar)


			IF @bHasRejectReason =0 
				SELECT @WHERE = @WHERE + ' AND TempRecord.RejectReason is  NULL ' 
     
			IF @sTextSearch IS NOT NULL
			 SELECT @WHERE = @WHERE + @sTextSearch
     
			 IF @bHasRejectReason =1 
			 SELECT @WHERE = @WHERE + ' AND TempRecord.RejectReason is not NULL ' 
     
			 IF @bHasWarningReason IS NOT NULL
			 BEGIN
				 IF @bHasWarningReason =0 
				 SELECT @WHERE = @WHERE + ' AND TempRecord.WarningReason is  NULL ' 
	     
				 IF @bHasWarningReason =1 
				 SELECT @WHERE = @WHERE + ' AND TempRecord.WarningReason is not NULL ' 
			 END


			IF @dDateFrom IS NOT NULL 
				SELECT @WHERE = @WHERE + ' AND TempRecord.DateTimeRecorded >= ''' + CONVERT(varchar(10), @dDateFrom, 103) + ''''

			IF @dDateTo IS NOT NULL 
				SELECT @WHERE = @WHERE + ' AND TempRecord.DateTimeRecorded <= ''' + CONVERT(varchar(10), @dDateTo, 103) + ''''

			--IF @nEnteredBy IS NOT NULL 
			--	SELECT @SELECT = @SELECT + ' AND TempRecord.EnteredBy = ' + CAST(@nEnteredBy AS varchar)

			IF @bIsActive IS NOT NULL 
				SELECT @WHERE = @WHERE + ' AND TempRecord.IsActive = ' + CAST(@bIsActive AS varchar)
    

			DECLARE @SQLCount varchar(MAX)


			SET DATEFORMAT DMY
				SET @SELECT = 'SELECT * FROM (SELECT RecordInfo.*,ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ' ) as RowNum FROM (SELECT ' + @SELECT

				SELECT @SQLMain =  
					+ @SELECT + ' ' + @FROM + ' ' + @WHERE  + ') as RecordInfo) as RecordFinalInfo WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
    
				SELECT @SQLCount= REPLACE(@SELECT, 'SELECT * FROM', 'SELECT COUNT([DBGSystemRecordID]) AS TotalRows FROM') + ' ' + @FROM + ' ' + @WHERE + ') as RecordInfo) as RecordFinalInfo'
    
				SET ROWCOUNT @nMaxRows

				PRINT left(@SQLMain,8000)
				PRINT substring(@SQLMain,8001,8000)
				EXEC (@SQLMain)
	
				SET ROWCOUNT 0 
				PRINT left(@SQLCount,8000)
				PRINT substring(@SQLCount,8001,8000)
				EXEC (@SQLCount)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_TempRecord_List', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
