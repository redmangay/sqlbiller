﻿

CREATE PROCEDURE [dbo].[UserSearch_GetNewSCid]
(
@nNewID int output,
 @nUserID int,
 @nViewID int
)
AS

BEGIN TRY
	DECLARE @sSearchXML varchar(MAX)=NULL
	SELECT @sSearchXML=SearchXML FROM UserSearch WHERE UserID=@nUserID AND ViewID=@nViewID

	IF @sSearchXML IS NOT NULL
		BEGIN
			INSERT INTO [SearchCriteria]
				(
					SearchText
				) VALUES (
					@sSearchXML	
				)
				SELECT @nNewID = @@IDENTITY
		END
	ELSE
		SELECT @nNewID = -1
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('UserSearch_GetNewSCid', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
