﻿

CREATE PROCEDURE [dbo].[rrp_HazardByCategory]
(
	@AccountID int,
	@FromDate date, 
	@ToDate date
)
AS  

BEGIN TRY
-- Workaround: Define the columne in here where it will never be run:
	IF 1 = 2
		 BEGIN
		   -- These are the actual column names and types returned by the real proc
		   SELECT CAST('' AS nvarchar(20)) AS Title, 
				  CAST('' AS nvarchar(20)) AS [Hazard Type],
				  0 as [Count]
		 END;
	-- End Workaround

	DECLARE @sFromDate varchar(10), @sToDate varchar(10)
	SELECT @sFromDate = CONVERT( varchar(10), @FromDate, 103),
		@sToDate = CONVERT( varchar(10), @ToDate, 103)

	DECLARE @sSQL nvarchar(MAX)
	SET @sSQL = 'SET DATEFORMAT DMY;
		SELECT ''' + @sFromDate + ' to ' + CONVERT( varchar(10), @ToDate, 103) + ''' AS   Title
				,Account' + CAST(@AccountID as varchar) + '.vHazard.[Hazard Type]
				,COUNT(*) AS		[Count]
	FROM		Account' + CAST(@AccountID as varchar) + '.vHazard 
	WHERE		ISDATE([Date IdentIFied])=1 
				AND CAST([Date IdentIFied] as date) between ''' + @sFromDate + ''' and ''' + @sToDate + '''
	GROUP BY	Account' + CAST(@AccountID as varchar) + '.vHazard.[Hazard Type]
	ORDER BY	Account' + CAST(@AccountID as varchar) + '.vHazard.[Hazard Type]'

	PRINT (@sSQL)	
	EXEC (@sSQL)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('rrp_HazardByCategory', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
