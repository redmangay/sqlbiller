﻿

CREATE PROCEDURE [dbo].[ets_Record_Avg]
(
	@nTableID int ,
	@nColumnID INT,
	@dAvgDateFrom datetime,
	@dAvgDateTo datetime
)
AS
/*
EXEC ets_Record_Avg 186,'V001','05/22/2012'
*/
BEGIN TRY
	DECLARE @sSelect nvarchar(MAX)

	DECLARE @sSystemName AS varchar(10)

	SELECT @sSystemName=SystemName FROM [Column] WHERE ColumnID=@nColumnID

	SET @sSelect ='SELECT Avg(CONVERT(decimal(18,10),' + +CAST(@sSystemName AS varchar) + ')) FROM Record 
	WHERE IsActive=1 AND TableID=' + +CAST(@nTableID AS varchar) + 
	' AND Record.DateTimeRecorded >= ''' + CONVERT(varchar(30), @dAvgDateFrom, 120) + '''' +
	' AND Record.DateTimeRecorded <= ''' + CONVERT(varchar(30), @dAvgDateTo, 120) + ''''


		PRINT @sSelect
		EXEC (@sSelect)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Record_Avg', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
