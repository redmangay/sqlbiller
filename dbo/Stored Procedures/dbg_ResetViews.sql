﻿

CREATE PROCEDURE [dbo].[dbg_ResetViews]
(
	@nViewID int
)
	/*----------------------------------------------------------------
	EXEC [dbg_ResetViews] 29
	---------------------------------------------------------------*/
 
AS

BEGIN TRY
	BEGIN TRANSACTION --beginning a transaction..
          
		 
		DECLARE @nTableID INT		
		DECLARE @sViewPageType varchar(250)
		
		SELECT @nTableID=TableID,@sViewPageType=ViewPageType	
		FROM [View] WHERE ViewID=@nViewID

		UPDATE [View] SET UserID=NULL WHERE ViewID=@nViewID
		
		DELETE ViewItem WHERE ViewID IN
		(SELECT ViewID FROM [View]
		WHERE TableID=@nTableID AND ViewPageType=@sViewPageType
		 AND ViewID<>@nViewID )
		 
		 DELETE [View] WHERE 
		 ViewID IN
		(SELECT ViewID FROM [View]
		WHERE TableID=@nTableID AND ViewPageType=@sViewPageType
		 AND ViewID<>@nViewID )

	
		
		PRINT 'Commit'
        COMMIT TRANSACTION --finally, Commit 

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_ResetViews', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
