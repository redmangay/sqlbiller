﻿
--
-- PARAMETERS:
--		@ReportType INT 
--			* Same as the options in MS Access
--			* 1 - Select Job Number
--			* 2 - Current Jobs
--			* 3 - Jobs with Invoice Dated
--			* 4 - Completed Jobs
--		@ProjectNumbers NVARCHAR(MAX)
--			* List of included projects. Needed for ReportType 1
--			* If more one projects, separate it with a comma (e.g. 'WC718-01','WC719-01')
--
--		@StartDate DATETIME
--			* Starting date of report. Needed for ReportType 3 and 4
--
--		@EndDate DATETIME
--			* Starting date of report. Needed for ReportType 3 and 4
--
--		@GroupDivision NVARCHAR(255)
--			* Optional. If NULL, ALL Group/Divion will be included
--			* Display selected Group/Division only. Used in ReportType 2-4
--
--		@Controller NVARCHAR(255)
--			* Optional. If NULL, ALL Controller will be included
--			* Display selected Controller only. Used in ReportType 2-4
--
CREATE PROCEDURE [dbo].[usr_JobPerformanceReport]
	  @ReportType		INT
	, @ProjectNumbers	NVARCHAR(MAX)
	, @StartDate		DATETIME =null
	, @EndDate			DATETIME =null
	, @GroupDivision	NVARCHAR(255) =null
	, @Controller		NVARCHAR(255) =null
AS
BEGIN
set dateformat dmy

if @ReportType <> 1
begin
exec usr_rpt_JPInvoicedUnBilledCost
exec usr_rpt_JPInvoicedUnBilledTime
end


	DECLARE @BaseBilledTimeSQL NVARCHAR(MAX)
	DECLARE @SQL NVARCHAR(MAX)





	
SELECT DISTINCT BillingInformation.[Project Number]
		, Max(cast(BillingInformation.Date as date)) AS MaxOfDate

		into #JPCompletedByMaxInvoiceDate
	FROM Account24918.[vBilling Information] BillingInformation
		LEFT JOIN Account24918.vProject Project ON BillingInformation.[Project Number] = Project.ProjectNumber
	GROUP BY BillingInformation.[Project Number]
		, Project.Status
		, BillingInformation.[Invoice Amount]
	HAVING  (Project.Status='4' OR Project.Status='5' OR Project.Status='6')
		AND (BillingInformation.[Invoice Amount] IS NOT NULL)








	-- BilledTime_Insert temp tables
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledTimeInsert1' )) DROP TABLE #TempBilledTimeInsert1
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledTimeInsert2' )) DROP TABLE #TempBilledTimeInsert2
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledTimeInsert3' )) DROP TABLE #TempBilledTimeInsert3
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledTimeInsert4' )) DROP TABLE #TempBilledTimeInsert4

	-- BilledCost_Insert temp tables
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledCostInsert' )) DROP TABLE #TempBilledCostInsert
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledCostInsert1' )) DROP TABLE #TempBilledCostInsert1
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledCostInsert2' )) DROP TABLE #TempBilledCostInsert2
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledCostInsert3' )) DROP TABLE #TempBilledCostInsert3
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledCostInsert4' )) DROP TABLE #TempBilledCostInsert4

	-- UnBilledTime_Insert temp tables
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempUnBilledTimeInsert' )) DROP TABLE #TempUnBilledTimeInsert
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempUnBilledTimeInsert1' )) DROP TABLE #TempUnBilledTimeInsert1
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempUnBilledTimeInsert2' )) DROP TABLE #TempUnBilledTimeInsert2
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempUnBilledTimeInsert3' )) DROP TABLE #TempUnBilledTimeInsert3

	-- UnBilledCost_Insert temp tables
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempUnBilledCostInsert' )) DROP TABLE #TempUnBilledCostInsert
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempUnBilledCostInsert1' )) DROP TABLE #TempUnBilledCostInsert1
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempUnBilledCostInsert2' )) DROP TABLE #TempUnBilledCostInsert2
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempUnBilledCostInsert3' )) DROP TABLE #TempUnBilledCostInsert3

	IF (@ReportType=1)
		BEGIN
			-- In order to process multiple project numbers, we need to use global temp table.
			IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##TempBilledTimeInsert' )) DROP TABLE ##TempBilledTimeInsert
			IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##TempBilledCostInsert' )) DROP TABLE ##TempBilledCostInsert
			IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##TempUnBilledTimeInsert' )) DROP TABLE ##TempUnBilledTimeInsert
			IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##TempUnBilledCostInsert' )) DROP TABLE ##TempUnBilledCostInsert

			-- BilledTime_Insert (mdlJobPerformance.OPTION_RANGE) 
			SET @SQL = 'SELECT DISTINCT Project.[Group/Division] AS GroupDivision
				, TimeSheet.Date AS TimeDate
				, Employee_List.Initials AS EInitials
				, TimeSheet.Details
				, EmployeeHistroy.Name AS EName
				, CASE WHEN TimeSheet.[Status]=''5'' THEN ISNULL(cast(TimeSheet.Total as money),0) ELSE ISNULL(cast(TimeSheet.[$_Amount] as money),0) END AS TimeSheet
				, ISNULL(cast(TimeSheet.[$_Amount] as money),0) AS BilledDoller
				, CASE WHEN TimeSheet.STATUS=''0'' THEN ''By Rate'' ELSE
					CASE WHEN TimeSheet.STATUS=''1'' THEN ''Hold'' ELSE
						CASE WHEN TimeSheet.STATUS=''3'' THEN ''Fixed'' ELSE
							CASE WHEN TimeSheet.STATUS=''4'' THEN ''No Charge'' ELSE
								CASE WHEN TimeSheet.STATUS=''5'' THEN ''Write-off'' ELSE
									CASE WHEN TimeSheet.STATUS=''6'' THEN ''Adjusted'' ELSE
										''NONE''
									END
								END
							END
						END
					END
				  END AS BilledStatus
				, PNEmployeeIDTimeSheetID.TimeSheetID
				, BillingInformation.ID AS BillingID
				, BillingInformation.[Invoice No]
				, BillingInformation.[Invoice Amount Converted] AS InvoiceAmount
				, BillingInformation.Date AS InvoiceDate
				, BillingInformation.[Tax Name]
				, BillingInformation.[Tax Amount]
				, BillingInformation.[Tax Rate]
				, Project.ProjectNumber AS JobNo
				, Project.ProjectTitle
				, CASE WHEN ISNULL(Project.CurrencyRate,'''')='''' THEN CAST(Project.Value AS MONEY) ELSE 
					CAST(CASE WHEN ISNUMERIC(Project.Value)=1 THEN CAST(Project.Value AS MONEY)/CAST(Project.CurrencyRate AS FLOAT) ELSE CAST(Project.Value AS MONEY) END AS VARCHAR(255))
				  END AS ProjectValue
				, CASE WHEN ISNULL(Project.CurrencyRate,'''')='''' THEN ''1'' ELSE Project.CurrencyRate END AS ExchangeRate
				, CASE WHEN cast(TimeSheet.[$_Amount] as money)=''0'' THEN 0 ELSE (cast (TimeSheet.[$_Billed] as money)/cast( TimeSheet.[$_Amount] as money)) * 100 END AS PercentBilled
				, CASE WHEN cast(TimeSheet.[$_Amount] as money)=''0'' THEN 0 ELSE ((cast(TimeSheet.[$_Amount] as money)- cast(TimeSheet.[$_Billed] as money))/ cast(TimeSheet.[$_Amount] as money)) * 100 END AS PercentUnBilled
				, 0 AS [Convert]
				, CAST(0 AS MONEY) AS InterimInvoiceAmount
				, CAST(0 AS MONEY) AS InterimInvoiceTaxAmount
				, CAST(0 AS FLOAT) AS Rate 
			INTO ##TempBilledTimeInsert
			FROM (Account24918.vEmployee_List Employee_List INNER JOIN (Account24918.vProject Project RIGHT JOIN ((Account24918.vPNEmployeeIDTimeSheetID PNEmployeeIDTimeSheetID INNER JOIN Account24918.vTimeSheet TimeSheet ON PNEmployeeIDTimeSheetID.TimeSheetID = TimeSheet.TimeSheetID)
				INNER JOIN Account24918.[vBilling Information] BillingInformation ON (PNEmployeeIDTimeSheetID.ProjectNumber = BillingInformation.[Project Number]) AND (TimeSheet.InvoiceNo = BillingInformation.[Invoice No]))
					ON Project.ProjectNumber = BillingInformation.[Project Number]) ON Employee_List.EmployeeID = PNEmployeeIDTimeSheetID.EmployeeID)
				LEFT JOIN Account24918.vEmployeeHistroy EmployeeHistroy ON Project.EmployeeID = EmployeeHistroy.EmployeeID	
	
			WHERE (TimeSheet.Billed=''True'')
				AND (BillingInformation.Type=''1'')
				AND (BillingInformation.[Project Number] IN (''' + @ProjectNumbers + '''))
			'
			EXEC SP_EXECUTESQL @SQL

			SELECT * 
			INTO #TempBilledTimeInsert1
			FROM ##TempBilledTimeInsert

			-- BilledCost_Insert
			SET @SQL = '
			
			SELECT DISTINCT Project.[Group/Division] as GroupDivision
				, TimeSheet_Cost.Date as TimeDate
				, Employee_List.Initials AS EInitials
				, TimeSheet_Cost.Details
				, EmployeeHistroy.Name as EName
				, CASE WHEN TimeSheet_Cost.STATUS=''5'' THEN 
					CASE WHEN TimeSheet_Cost.Flag=''1'' THEN
						ISNULL(cast (TimeSheet_Cost.Dollar_Amount as money),0) 
					ELSE ISNULL(cast(TimeSheet_Cost.Units as money),0)*ISNULL( cast(TimeSheet_Cost.Rate as money),0)
					END
				  ELSE
					ISNULL(cast(TimeSheet_Cost.[$_Amount] as money),0) 
				  END AS TimeSheet
				, ISNULL(cast(TimeSheet_Cost.[$_Amount] as money),0) AS BilledDoller
				, CASE WHEN TimeSheet_Cost.STATUS=''0'' THEN ''By Rate'' ELSE
					CASE WHEN TimeSheet_Cost.STATUS=''1'' THEN ''Hold'' ELSE
						CASE WHEN TimeSheet_Cost.STATUS=''3'' THEN ''Fixed'' ELSE
							CASE WHEN TimeSheet_Cost.STATUS=''4'' THEN ''No Charge'' ELSE
								CASE WHEN TimeSheet_Cost.STATUS=''5'' THEN ''Write-off'' ELSE
									CASE WHEN TimeSheet_Cost.STATUS=''6'' THEN ''Adjusted'' ELSE
										''NONE''
									END
								END
							END
						END
					END
				  END AS BilledStatus
				, PNEmployeeIDCostID.CostID
				, BillingInformation.ID AS BillingID
				, BillingInformation.[Invoice No]
				, BillingInformation.[Invoice Amount Converted] as InvoiceAmount
				, BillingInformation.Date AS InvoiceDate
				, BillingInformation.[Tax Name]
				, BillingInformation.[Tax Amount] as [Tax Amount]
				, BillingInformation.[Tax Rate] as TaxRate
				, Project.ProjectNumber as JobNo
				, Project.ProjectTitle
				, CASE WHEN ISNULL(Project.CurrencyRate,'''')='''' THEN CAST(Project.Value AS MONEY) ELSE 
					CAST(CASE WHEN ISNUMERIC(Project.Value)=1 THEN CAST(Project.Value AS MONEY)/CAST(Project.CurrencyRate AS FLOAT) ELSE CAST(Project.Value AS MONEY) END AS VARCHAR(255))
					END AS ProjectValue
				, CASE WHEN ISNULL(Project.CurrencyRate,'''')='''' THEN ''1'' ELSE Project.CurrencyRate END AS ExchangeRate
				, CASE WHEN cast(TimeSheet_Cost.[$_Amount] as money)=0 THEN 0 ELSE 100 END AS PercentBilled
				, 0 AS [Convert]
				, CAST(0 AS MONEY) AS InterimInvoiceAmount
				, CAST(0 AS MONEY) AS InterimInvoiceTaxAmount
				, CAST(0 AS FLOAT) AS Rate 
			INTO ##TempBilledCostInsert
			FROM (Account24918.vEmployee_List Employee_List INNER JOIN ((Account24918.vPNEmployeeIDCostID PNEmployeeIDCostID INNER JOIN (Account24918.vTimeSheet_Cost TimeSheet_Cost INNER JOIN Account24918.[vBilling Information] BillingInformation ON TimeSheet_Cost.InvoiceNo = BillingInformation.[Invoice No]) ON (TimeSheet_Cost.CostID = PNEmployeeIDCostID.CostID) AND (PNEmployeeIDCostID.ProjectNumber = BillingInformation.[Project Number]))
				LEFT JOIN Account24918.vProject Project ON BillingInformation.[Project Number] = Project.ProjectNumber) ON Employee_List.EmployeeID = PNEmployeeIDCostID.EmployeeID)
				LEFT JOIN Account24918.vEmployeeHistroy EmployeeHistroy  ON Project.EmployeeID = EmployeeHistroy.EmployeeID
			WHERE (TimeSheet_Cost.Billed=''True'')
				AND (BillingInformation.Type=''1'')
				AND (BillingInformation.[Project Number] IN (''' + @ProjectNumbers + '''))
			'
			EXEC SP_EXECUTESQL @SQL

			SELECT * 
			INTO #TempBilledCostInsert1
			FROM ##TempBilledCostInsert

			-- UnBilledTime_Insert
			SET @SQL = '
			SELECT DISTINCT Project.[Group/Division] AS GroupDivision
				, TimeSheet.Date AS TimeDate
				, Employee_List.Initials AS EInitials
				, TimeSheet.Details, EmployeeHistroy.Name  AS EName
				, CASE WHEN TimeSheet.[Status]=''5'' THEN ISNULL(cast(TimeSheet.Total as money),0) ELSE ISNULL(cast(TimeSheet.[$_Amount] as money),0) END AS TimeSheet
				, ISNULL(cast(TimeSheet.[$_Amount] as money),0) AS BilledDoller
				, CASE WHEN TimeSheet.STATUS=''0'' THEN ''By Rate'' ELSE
					CASE WHEN TimeSheet.STATUS=''1'' THEN ''Hold'' ELSE
						CASE WHEN TimeSheet.STATUS=''3'' THEN ''Fixed'' ELSE
							CASE WHEN TimeSheet.STATUS=''4'' THEN ''No Charge'' ELSE
								CASE WHEN TimeSheet.STATUS=''5'' THEN ''Write-off'' ELSE
									CASE WHEN TimeSheet.STATUS=''6'' THEN ''Adjusted'' ELSE
										''NONE''
									END
								END
							END
						END
					END
				  END AS BilledStatus
				, PNEmployeeIDTimeSheetID.TimeSheetID
				, Project.ProjectTitle
				, Project.ProjectNumber AS JobNo
				, CAST(ISNULL(Project.Value,''0'') AS MONEY) AS ProjectValue
				, 0 AS [Convert]
				, CAST(0 AS FLOAT) AS Rate 
			INTO ##TempUnBilledTimeInsert
			FROM Account24918.vProject Project INNER JOIN ((Account24918.vEmployee_List Employee_List INNER JOIN Account24918.vEmployeeHistroy EmployeeHistroy  ON Employee_List.EmployeeID = EmployeeHistroy.EmployeeID)
				INNER JOIN (Account24918.vPNEmployeeIDTimeSheetID PNEmployeeIDTimeSheetID INNER JOIN Account24918.vTimeSheet TimeSheet ON PNEmployeeIDTimeSheetID.TimeSheetID = TimeSheet.TimeSheetID)
					ON Employee_List.EmployeeID = PNEmployeeIDTimeSheetID.EmployeeID) ON Project.ProjectNumber = PNEmployeeIDTimeSheetID.ProjectNumber
			WHERE (TimeSheet.Billed=''False'')
				AND (Project.ProjectNumber IN (''' + @ProjectNumbers + '''))
			'
			EXEC SP_EXECUTESQL @SQL

			SELECT * 
			INTO #TempUnBilledTimeInsert1
			FROM ##TempUnBilledTimeInsert

			-- UnBilledCost_Insert
			SET @SQL = '
			SELECT DISTINCT Project.[Group/Division] AS GroupDivision
				, TimeSheet_Cost.Date AS TimeDate
				, Employee_List.Initials AS EInitials
				, TimeSheet_Cost.Details
				, EmployeeHistroy.Name AS EName
				, CASE WHEN TimeSheet_Cost.STATUS=''5'' THEN 
					CASE WHEN TimeSheet_Cost.Flag=''1'' THEN
						ISNULL(cast(TimeSheet_Cost.Dollar_Amount as money),0) 
					ELSE ISNULL(cast(TimeSheet_Cost.Units as money),0)*ISNULL(cast(TimeSheet_Cost.Rate as money),0)
					END
				  ELSE
					ISNULL(cast(TimeSheet_Cost.[$_Amount] as money),0) 
				  END AS TimeSheet
				, ISNULL(cast(TimeSheet_Cost.[$_Amount] as money),0) AS BilledDoller
				, CASE WHEN TimeSheet_Cost.STATUS=''0'' THEN ''By Rate'' ELSE
					CASE WHEN TimeSheet_Cost.STATUS=''1'' THEN ''Hold'' ELSE
						CASE WHEN TimeSheet_Cost.STATUS=''3'' THEN ''Fixed'' ELSE
							CASE WHEN TimeSheet_Cost.STATUS=''4'' THEN ''No Charge'' ELSE
								CASE WHEN TimeSheet_Cost.STATUS=''5'' THEN ''Write-off'' ELSE
									CASE WHEN TimeSheet_Cost.STATUS=''6'' THEN ''Adjusted'' ELSE
										''NONE''
									END
								END
							END
						END
					END
				  END AS BilledStatus
				, PNEmployeeIDCostID.CostID
				, Project.ProjectTitle
				, Project.ProjectNumber AS JobNo
				, CAST(ISNULL(Project.Value,''0'') AS MONEY) AS ProjectValue
				, 0 AS [Convert]
				, CAST(0 AS FLOAT) AS Rate 
			INTO ##TempUnBilledCostInsert
			FROM Account24918.vProject Project INNER JOIN ((Account24918.vTimeSheet_Cost TimeSheet_Cost INNER JOIN (Account24918.vEmployee_List Employee_List INNER JOIN Account24918.vPNEmployeeIDCostID PNEmployeeIDCostID ON Employee_List.EmployeeID = PNEmployeeIDCostID.EmployeeID) ON TimeSheet_Cost.CostID = PNEmployeeIDCostID.CostID)
				INNER JOIN Account24918.vEmployeeHistroy  EmployeeHistroy ON Employee_List.EmployeeID = EmployeeHistroy.EmployeeID) ON Project.ProjectNumber = PNEmployeeIDCostID.ProjectNumber
			WHERE (TimeSheet_Cost.Billed=''False'')			AND (Project.ProjectNumber IN (''' + @ProjectNumbers + '''))
			'
			EXEC SP_EXECUTESQL @SQL

			SELECT * 
			INTO #TempUnBilledCostInsert1
			FROM ##TempUnBilledCostInsert

			-- Global temp table
			IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##TempBilledTimeInsert' )) DROP TABLE ##TempBilledTimeInsert
			IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##TempBilledCostInsert' )) DROP TABLE ##TempBilledCostInsert
			IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##TempUnBilledTimeInsert' )) DROP TABLE ##TempUnBilledTimeInsert
			IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##TempUnBilledCostInsert' )) DROP TABLE ##TempUnBilledCostInsert
		END
	ELSE IF (@ReportType=2)
		BEGIN

		
			SELECT DISTINCT Project.[Group/Division] AS GroupDivision
				, TimeSheet.Date AS TimeDate
				, Employee_List.Initials AS EInitials
				, TimeSheet.Details
				, EmployeeHistroy.Name AS EName
				, CASE WHEN TimeSheet.[Status]='5' THEN ISNULL(cast(TimeSheet.Total as money),0) ELSE ISNULL(cast(TimeSheet.[$_Amount] as money),0) END AS TimeSheet
				, ISNULL(cast(TimeSheet.[$_Amount] as money),0) AS BilledDoller
				, CASE WHEN TimeSheet.STATUS='0' THEN 'By Rate' ELSE
					CASE WHEN TimeSheet.STATUS='1' THEN 'Hold' ELSE
						CASE WHEN TimeSheet.STATUS='3' THEN 'Fixed' ELSE
							CASE WHEN TimeSheet.STATUS='4' THEN 'No Charge' ELSE
								CASE WHEN TimeSheet.STATUS='5' THEN 'Write-off' ELSE
									CASE WHEN TimeSheet.STATUS='6' THEN 'Adjusted' ELSE
										'NONE'
									END
								END
							END
						END
					END
				  END AS BilledStatus
				, PNEmployeeIDTimeSheetID.TimeSheetID
				, BillingInformation.ID AS BillingID
				, BillingInformation.[Invoice No]
				, BillingInformation.[Invoice Amount Converted] AS InvoiceAmount
				, BillingInformation.Date AS InvoiceDate
				, BillingInformation.[Tax Name]
				, BillingInformation.[Tax Amount]
				, BillingInformation.[Tax Rate]
				, Project.ProjectNumber AS JobNo
				, Project.ProjectTitle
				, CASE WHEN ISNULL(cast(Project.CurrencyRate as money),0)=0 THEN CAST(Project.Value AS MONEY) ELSE 
					CAST(CASE WHEN ISNUMERIC(Project.Value)=1 THEN CAST(Project.Value AS MONEY)/CAST(Project.CurrencyRate AS FLOAT) ELSE CAST(Project.Value AS MONEY) END AS VARCHAR(255))
				  END AS ProjectValue
				, CASE WHEN ISNULL(Project.CurrencyRate,'')='' THEN '1' ELSE Project.CurrencyRate END AS ExchangeRate
				, CASE WHEN cast(TimeSheet.[$_Amount] as money)=0 THEN 0 ELSE (cast(TimeSheet.[$_Billed] as money)/ cast(TimeSheet.[$_Amount] as money)) * 100 END AS PercentBilled
			    , CASE WHEN cast(TimeSheet.[$_Amount] as money)=0 THEN 0 ELSE ((cast(TimeSheet.[$_Amount] as money)- cast(TimeSheet.[$_Billed] as money))/ cast(TimeSheet.[$_Amount] as money)) * 100 END AS PercentUnBilled
				, 0 AS [Convert]
				, CAST(0 AS MONEY) AS InterimInvoiceAmount
				, CAST(0 AS MONEY) AS InterimInvoiceTaxAmount
				, CAST(0 AS FLOAT) AS Rate 
			INTO #TempBilledTimeInsert2
			FROM (Account24918.vEmployee_List Employee_List INNER JOIN (Account24918.vProject Project RIGHT JOIN ((Account24918.vPNEmployeeIDTimeSheetID PNEmployeeIDTimeSheetID INNER JOIN Account24918.vTimeSheet TimeSheet ON PNEmployeeIDTimeSheetID.TimeSheetID = TimeSheet.TimeSheetID)
				INNER JOIN Account24918.[vBilling Information] BillingInformation ON (PNEmployeeIDTimeSheetID.ProjectNumber = BillingInformation.[Project Number]) AND (TimeSheet.InvoiceNo = BillingInformation.[Invoice No])) ON Project.ProjectNumber = BillingInformation.[Project Number]) ON Employee_List.EmployeeID = PNEmployeeIDTimeSheetID.EmployeeID)
				LEFT JOIN Account24918.vEmployeeHistroy EmployeeHistroy ON Project.EmployeeID = EmployeeHistroy.EmployeeID 
			WHERE (Project.ProjectNumber<>'Default')
				AND (Project.Status='2')
				AND (TimeSheet.Billed='1')
				AND (BillingInformation.Type='1')
				AND (Project.[Group/Division] LIKE CASE WHEN @GroupDivision IS NULL THEN '%' ELSE @GroupDivision END)
				AND (Project.Controller LIKE CASE WHEN @Controller IS NULL THEN '%' ELSE @Controller END)

			-- BilledCost_Insert
			SELECT DISTINCT Project.[Group/Division] as GroupDivision
				, TimeSheet_Cost.Date as TimeDate
				, Employee_List.Initials AS EInitials
				, TimeSheet_Cost.Details
				, EmployeeHistroy.Name as EName
				, CASE WHEN TimeSheet_Cost.STATUS='5' THEN 
					CASE WHEN TimeSheet_Cost.Flag='1' THEN
						ISNULL(cast(TimeSheet_Cost.Dollar_Amount as money),0) 
					ELSE ISNULL(cast(TimeSheet_Cost.Units as money),0)*ISNULL(cast(TimeSheet_Cost.Rate as money),0)
					END
				  ELSE
					ISNULL(cast(TimeSheet_Cost.[$_Amount] as money),0) 
				  END AS TimeSheet
				, ISNULL(cast(TimeSheet_Cost.[$_Amount] as money),0) AS BilledDoller
				, CASE WHEN TimeSheet_Cost.STATUS='0' THEN 'By Rate' ELSE
					CASE WHEN TimeSheet_Cost.STATUS='1' THEN 'Hold' ELSE
						CASE WHEN TimeSheet_Cost.STATUS='3' THEN 'Fixed' ELSE
							CASE WHEN TimeSheet_Cost.STATUS='4' THEN 'No Charge' ELSE
								CASE WHEN TimeSheet_Cost.STATUS='5' THEN 'Write-off' ELSE
									CASE WHEN TimeSheet_Cost.STATUS='6' THEN 'Adjusted' ELSE
										'NONE'
									END
								END
							END
						END
					END
				  END AS BilledStatus
				, PNEmployeeIDCostID.CostID
				, BillingInformation.ID AS BillingID
				, BillingInformation.[Invoice No]
				, BillingInformation.[Invoice Amount Converted] as InvoiceAmount
				, BillingInformation.Date AS InvoiceDate
				, BillingInformation.[Tax Name]
				, BillingInformation.[Tax Amount] as [Tax Amount]
				, BillingInformation.[Tax Rate] as TaxRate
				, Project.ProjectNumber as JobNo
				, Project.ProjectTitle
				, CASE WHEN ISNULL(cast (Project.CurrencyRate as money),0)=0 THEN CAST(Project.Value AS MONEY) ELSE 
					CAST(CASE WHEN ISNUMERIC(Project.Value)=1 THEN CAST(Project.Value AS MONEY)/CAST(Project.CurrencyRate AS FLOAT) ELSE CAST(Project.Value AS MONEY) END AS VARCHAR(255))
				  END AS ProjectValue
				, CASE WHEN ISNULL(Project.CurrencyRate,'')='' THEN '1' ELSE Project.CurrencyRate END AS ExchangeRate
				, CASE WHEN cast(TimeSheet_Cost.[$_Amount] as money)=0 THEN 0 ELSE 100 END AS PercentBilled
				, 0 AS [Convert]
				, CAST(0 AS MONEY) AS InterimInvoiceAmount
				, CAST(0 AS MONEY) AS InterimInvoiceTaxAmount
				, CAST(0 AS FLOAT) AS Rate 
			INTO #TempBilledCostInsert2
			FROM (Account24918.vEmployee_List Employee_List INNER JOIN ((Account24918.vPNEmployeeIDCostID PNEmployeeIDCostID INNER JOIN (Account24918.vTimeSheet_Cost TimeSheet_Cost INNER JOIN Account24918.[vBilling Information] BillingInformation ON TimeSheet_Cost.InvoiceNo = BillingInformation.[Invoice No])ON (TimeSheet_Cost.CostID = PNEmployeeIDCostID.CostID) AND (PNEmployeeIDCostID.ProjectNumber = BillingInformation.[Project Number]))
				LEFT JOIN Account24918.vProject Project ON BillingInformation.[Project Number] = Project.ProjectNumber) ON Employee_List.EmployeeID = PNEmployeeIDCostID.EmployeeID)
				LEFT JOIN Account24918.vEmployeeHistroy EmployeeHistroy ON Project.EmployeeID = EmployeeHistroy.EmployeeID
			WHERE (Project.ProjectNumber<>'Default')
				AND (Project.Status='2')
				AND (TimeSheet_Cost.Billed='1')
				AND (BillingInformation.Type='1')
				AND (Project.[Group/Division] LIKE CASE WHEN @GroupDivision IS NULL THEN '%' ELSE @GroupDivision END)
				AND (Project.Controller LIKE CASE WHEN @Controller IS NULL THEN '%' ELSE @Controller END)
				
			-- UnBilledTime_Insert
			
			
			SELECT DISTINCT Project.[Group/Division] AS GroupDivision
				, TimeSheet.Date AS TimeDate
				, Employee_List.Initials AS EInitials
				, TimeSheet.Details
				, EmployeeHistroy.Name AS EName
				, CASE WHEN TimeSheet.[Status]='5' THEN ISNULL((TimeSheet.Total ),'0') ELSE ISNULL((TimeSheet.[$_Amount]),'0') END AS TimeSheet
				, ISNULL((TimeSheet.[$_Amount] ),'0') AS BilledDoller
				, CASE WHEN TimeSheet.STATUS='0' THEN 'By Rate' ELSE
					CASE WHEN TimeSheet.STATUS='1' THEN 'Hold' ELSE
						CASE WHEN TimeSheet.STATUS='3' THEN 'Fixed' ELSE
							CASE WHEN TimeSheet.STATUS='4' THEN 'No Charge' ELSE
								CASE WHEN TimeSheet.STATUS='5' THEN 'Write-off' ELSE
									CASE WHEN TimeSheet.STATUS='6' THEN 'Adjusted' ELSE
										'NONE'
									END
								END
							END
						END
					END
				  END AS BilledStatus
				, PNEmployeeIDTimeSheetID.TimeSheetID
				, Project.ProjectTitle
				, Project.ProjectNumber AS JobNo
				, CAST(ISNULL(Project.Value,0) AS MONEY) AS ProjectValue
				, 0 AS [Convert]
				, CAST(0 AS money) AS Rate 
			INTO #TempUnBilledTimeInsert2
			FROM Account24918.vProject Project INNER JOIN ((Account24918.vEmployee_List Employee_List INNER JOIN Account24918.vEmployeeHistroy EmployeeHistroy ON Employee_List.EmployeeID = EmployeeHistroy.EmployeeID)
				INNER JOIN (Account24918.vPNEmployeeIDTimeSheetID PNEmployeeIDTimeSheetID INNER JOIN Account24918.vTimeSheet TimeSheet ON PNEmployeeIDTimeSheetID.TimeSheetID = TimeSheet.TimeSheetID)
					ON Employee_List.EmployeeID = PNEmployeeIDTimeSheetID.EmployeeID) ON Project.ProjectNumber = PNEmployeeIDTimeSheetID.ProjectNumber
			WHERE (TimeSheet.Billed='0')
				AND (Project.ProjectNumber<>'Default' AND Project.Status='2')
				AND (Project.[Group/Division] LIKE CASE WHEN @GroupDivision IS NULL THEN '%' ELSE @GroupDivision END)
				AND (Project.Controller LIKE CASE WHEN @Controller IS NULL THEN '%' ELSE @Controller END)
				
			-- UnBilledCost_Insert
			SELECT DISTINCT Project.[Group/Division] as GroupDivision
				, TimeSheet_Cost.Date as TimeDate
				, Employee_List.Initials AS EInitials
				, TimeSheet_Cost.Details
				, EmployeeHistroy.Name AS EName
				--, iif(TimeSheet_Cost.STATUS=5, IIF(Flag, nz([Dollar_Amount], 0),  nz(Units, 0)*nz(Rate, 0)), nz(TimeSheet_Cost.[$_Amount], 0)) AS TimeSheet
				, CASE WHEN TimeSheet_Cost.STATUS='5' THEN 
					CASE WHEN TimeSheet_Cost.Flag='1' THEN
						ISNULL(TimeSheet_Cost.Dollar_Amount,'0') 
					ELSE ISNULL(cast(TimeSheet_Cost.Units as money),0)*ISNULL(cast(TimeSheet_Cost.Rate as money),0)
					END
				  ELSE
					ISNULL(TimeSheet_Cost.[$_Amount],'0') 
				  END AS TimeSheet
				, ISNULL(TimeSheet_Cost.[$_Amount],'0') AS BilledDoller
				, CASE WHEN TimeSheet_Cost.STATUS='0' THEN 'By Rate' ELSE
					CASE WHEN TimeSheet_Cost.STATUS='1' THEN 'Hold' ELSE
						CASE WHEN TimeSheet_Cost.STATUS='3' THEN 'Fixed' ELSE
							CASE WHEN TimeSheet_Cost.STATUS='4' THEN 'No Charge' ELSE
								CASE WHEN TimeSheet_Cost.STATUS='5' THEN 'Write-off' ELSE
									CASE WHEN TimeSheet_Cost.STATUS='6' THEN 'Adjusted' ELSE
										'NONE'
									END
								END
							END
						END
					END
				  END AS BilledStatus
				, PNEmployeeIDCostID.CostID
				, Project.ProjectTitle
				, Project.ProjectNumber as JobNo
				, CAST(ISNULL(Project.Value,'0') AS MONEY) AS ProjectValue
				, 0 AS [Convert]
				, CAST(0 AS FLOAT) AS Rate 
			INTO #TempUnBilledCostInsert2
			FROM Account24918.vProject Project INNER JOIN ((Account24918.vTimeSheet_Cost TimeSheet_Cost INNER JOIN (Account24918.vEmployee_List Employee_List INNER JOIN Account24918.vPNEmployeeIDCostID PNEmployeeIDCostID ON Employee_List.EmployeeID = PNEmployeeIDCostID.EmployeeID) ON TimeSheet_Cost.CostID = PNEmployeeIDCostID.CostID)
				INNER JOIN Account24918.vEmployeeHistroy EmployeeHistroy ON Employee_List.EmployeeID = EmployeeHistroy.EmployeeID) ON Project.ProjectNumber = PNEmployeeIDCostID.ProjectNumber
			WHERE (TimeSheet_Cost.Billed='0')
				AND (Project.ProjectNumber<>'Default' AND Project.Status='2')
				AND (Project.[Group/Division] LIKE CASE WHEN @GroupDivision IS NULL THEN '%' ELSE @GroupDivision END)
				AND (Project.Controller LIKE CASE WHEN @Controller IS NULL THEN '%' ELSE @Controller END)
		END
	ELSE IF (@ReportType=3)
		-- BilledTime_Insert
		BEGIN
			SELECT DISTINCT Project.[Group/Division] AS GroupDivision
				, TimeSheet.Date as TimeDate
				, Employee_List.Initials AS EInitials
				, TimeSheet.Details, EmployeeHistroy.Name AS EName
				, CASE WHEN TimeSheet.[Status]='5' THEN ISNULL(TimeSheet.Total,'0') ELSE ISNULL(TimeSheet.[$_Amount],'0') END AS TimeSheet
				, ISNULL(TimeSheet.[$_Amount],'0') AS BilledDoller
				, CASE WHEN TimeSheet.STATUS='0' THEN 'By Rate' ELSE
					CASE WHEN TimeSheet.STATUS='1' THEN 'Hold' ELSE
						CASE WHEN TimeSheet.STATUS='3' THEN 'Fixed' ELSE
							CASE WHEN TimeSheet.STATUS='4' THEN 'No Charge' ELSE
								CASE WHEN TimeSheet.STATUS='5' THEN 'Write-off' ELSE
									CASE WHEN TimeSheet.STATUS='6' THEN 'Adjusted' ELSE
										'NONE'
									END
								END
							END
						END
					END
				  END AS BilledStatus
				, PNEmployeeIDTimeSheetID.TimeSheetID
				, BillingInformation.ID AS BillingID
				, BillingInformation.[Invoice No]
				, BillingInformation.[Invoice Amount Converted] as InvoiceAmount
				, BillingInformation.Date AS InvoiceDate
				, BillingInformation.[Tax Name]
				, BillingInformation.[Tax Amount]
				, BillingInformation.[Tax Rate]
				, Project.ProjectNumber as JobNo
				, Project.ProjectTitle
				, CASE WHEN ISNULL(Project.CurrencyRate,'')='' THEN CAST(Project.Value AS MONEY) ELSE 
					CAST(CASE WHEN ISNUMERIC(Project.Value)=1 THEN CAST(Project.Value AS MONEY)/CAST(Project.CurrencyRate AS FLOAT) ELSE CAST(Project.Value AS MONEY) END AS VARCHAR(255))
				  END AS ProjectValue
				, CASE WHEN ISNULL(Project.CurrencyRate,'')='' THEN '1' ELSE Project.CurrencyRate END AS ExchangeRate
				, CASE WHEN cast(TimeSheet.[$_Amount] as money)=0 THEN 0 ELSE (cast(TimeSheet.[$_Billed] as money)/ cast(TimeSheet.[$_Amount] as money)) * 100 END AS PercentBilled
				, CASE WHEN cast(TimeSheet.[$_Amount] as money)=0 THEN 0 ELSE ((cast(TimeSheet.[$_Amount] as money)- cast(TimeSheet.[$_Billed] as money))/ cast(TimeSheet.[$_Amount] as money)) * 100 END AS PercentUnBilled
				, 0 AS [Convert]
				, CAST(0 AS MONEY) AS InterimInvoiceAmount
				, CAST(0 AS MONEY) AS InterimInvoiceTaxAmount
				, CAST(0 AS FLOAT) AS Rate 
			INTO #TempBilledTimeInsert3
			FROM (Account24918.vEmployee_List Employee_List INNER JOIN (Account24918.vProject Project RIGHT JOIN ((Account24918.vPNEmployeeIDTimeSheetID PNEmployeeIDTimeSheetID INNER JOIN Account24918.vTimeSheet TimeSheet ON PNEmployeeIDTimeSheetID.TimeSheetID = TimeSheet.TimeSheetID)
				INNER JOIN Account24918.[vBilling Information]  BillingInformation ON (PNEmployeeIDTimeSheetID.ProjectNumber = BillingInformation.[Project Number]) AND (TimeSheet.InvoiceNo = BillingInformation.[Invoice No])) ON Project.ProjectNumber = BillingInformation.[Project Number]) ON Employee_List.EmployeeID = PNEmployeeIDTimeSheetID.EmployeeID)
				LEFT JOIN Account24918.vEmployeeHistroy EmployeeHistroy ON Project.EmployeeID = EmployeeHistroy.EmployeeID 
			WHERE (Project.ProjectNumber<>'Default')
				AND (Project.Status<>'1' AND Project.Status<>'3')
				AND (TimeSheet.Billed='1')
				AND (BillingInformation.Type='1')
				--AND ((BillingInformation.Date)>=@StartDate AND (BillingInformation.Date)<=@EndDate) and isdate(BillingInformation.Date) =1
				AND (Project.[Group/Division] LIKE CASE WHEN @GroupDivision IS NULL THEN '%' ELSE @GroupDivision END)
				AND (Project.Controller LIKE CASE WHEN @Controller IS NULL THEN '%' ELSE @Controller END)

			-- BilledCost_Insert
			SELECT DISTINCT Project.[Group/Division] as GroupDivision
				, TimeSheet_Cost.Date as TimeDate
				, Employee_List.Initials AS EInitials
				, TimeSheet_Cost.Details
				, EmployeeHistroy.Name as EName
				, CASE WHEN TimeSheet_Cost.STATUS='5' THEN 
					CASE WHEN TimeSheet_Cost.Flag='1' THEN
						ISNULL(TimeSheet_Cost.Dollar_Amount,'0') 
					ELSE ISNULL(cast(TimeSheet_Cost.Units as money),0)*ISNULL(cast(TimeSheet_Cost.Rate as money),0)
					END
				  ELSE
					ISNULL(TimeSheet_Cost.[$_Amount],'0') 
				  END AS TimeSheet
				, ISNULL(TimeSheet_Cost.[$_Amount],'0') AS BilledDoller
				, CASE WHEN TimeSheet_Cost.STATUS='0' THEN 'By Rate' ELSE
					CASE WHEN TimeSheet_Cost.STATUS='1' THEN 'Hold' ELSE
						CASE WHEN TimeSheet_Cost.STATUS='3' THEN 'Fixed' ELSE
							CASE WHEN TimeSheet_Cost.STATUS='4' THEN 'No Charge' ELSE
								CASE WHEN TimeSheet_Cost.STATUS='5' THEN 'Write-off' ELSE
									CASE WHEN TimeSheet_Cost.STATUS='6' THEN 'Adjusted' ELSE
										'NONE'
									END
								END
							END
						END
					END
				  END AS BilledStatus
				, PNEmployeeIDCostID.CostID
				, BillingInformation.ID AS BillingID
				, BillingInformation.[Invoice No]
				, BillingInformation.[Invoice Amount Converted] as InvoiceAmount
				, BillingInformation.Date AS InvoiceDate
				, BillingInformation.[Tax Name]
				, BillingInformation.[Tax Amount] as [Tax Amount]
				, BillingInformation.[Tax Rate] as TaxRate
				, Project.ProjectNumber as JobNo
				, Project.ProjectTitle
				, CASE WHEN ISNULL(Project.CurrencyRate,'')='' THEN CAST(Project.Value AS MONEY) ELSE 
					CAST(CASE WHEN ISNUMERIC(Project.Value)=1 THEN CAST(Project.Value AS MONEY)/CAST(Project.CurrencyRate AS FLOAT) ELSE CAST(Project.Value AS MONEY) END AS VARCHAR(255))
					END AS ProjectValue
				, CASE WHEN ISNULL(Project.CurrencyRate,'')='' THEN '1' ELSE Project.CurrencyRate END AS ExchangeRate
				, CASE WHEN cast(TimeSheet_Cost.[$_Amount] as money)=0 THEN 0 ELSE 100 END AS PercentBilled
				, 0 AS [Convert]
				, CAST(0 AS MONEY) AS InterimInvoiceAmount
				, CAST(0 AS MONEY) AS InterimInvoiceTaxAmount
				, CAST(0 AS FLOAT) AS Rate 
			INTO #TempBilledCostInsert3
			FROM (Account24918.vEmployee_List Employee_List INNER JOIN ((Account24918.vPNEmployeeIDCostID PNEmployeeIDCostID INNER JOIN (Account24918.vTimeSheet_Cost  TimeSheet_Cost INNER JOIN Account24918.[vBilling Information] BillingInformation ON TimeSheet_Cost.InvoiceNo = BillingInformation.[Invoice No]) ON (TimeSheet_Cost.CostID = PNEmployeeIDCostID.CostID) AND (PNEmployeeIDCostID.ProjectNumber = BillingInformation.[Project Number]))
				LEFT JOIN Account24918.vProject Project ON BillingInformation.[Project Number] = Project.ProjectNumber) ON Employee_List.EmployeeID = PNEmployeeIDCostID.EmployeeID)
				LEFT JOIN Account24918.vEmployeeHistroy EmployeeHistroy ON Project.EmployeeID = EmployeeHistroy.EmployeeID 
			WHERE (Project.ProjectNumber<>'Default')
				AND (Project.Status<>'2' AND Project.Status<>'3')
				AND (TimeSheet_Cost.Billed='1')
				AND (BillingInformation.Type='1')
				--AND ((BillingInformation.date)>=@StartDate AND (BillingInformation.date)<=@EndDate) and isdate(BillingInformation.Date) =1
				AND (Project.[Group/Division] LIKE CASE WHEN @GroupDivision IS NULL THEN '%' ELSE @GroupDivision END)
				AND (Project.Controller LIKE CASE WHEN @Controller IS NULL THEN '%' ELSE @Controller END)

			-- UnBilledTime_Insert
			
			SELECT UnBilledTime.GroupDivision,
UnBilledTime.TimeDate,
UnBilledTime.EInitials,
UnBilledTime.Details,
UnBilledTime.EName,
UnBilledTime.TimeSheet,
UnBilledTime.BilledDoller,
UnBilledTime.BilledStatus,
UnBilledTime.TimeSheetID,
UnBilledTime.ProjectTitle,
UnBilledTime.JobNo,
UnBilledTime.ProjectValue 
				, 0 AS [Convert]
				, CAST(0 AS FLOAT) AS Rate 
			INTO #TempUnBilledTimeInsert3
			FROM (##JPInvoicedUnBilledTime UnBilledTime inner JOIN Account24918.[vBilling Information] BillingInformation ON UnBilledTime.JobNo = BillingInformation.[Project Number])
				LEFT JOIN Account24918.vProject Project ON BillingInformation.[Project Number] = Project.ProjectNumber
			WHERE (Project.ProjectNumber<>'Default')
				AND (Project.Status<>'1' AND Project.Status<>'3')
				AND (BillingInformation.Date>=@StartDate AND BillingInformation.Date<=@EndDate) and isdate(BillingInformation.Date) =1
				AND (Project.[Group/Division] LIKE CASE WHEN @GroupDivision IS NULL THEN '%' ELSE @GroupDivision END)
				AND (Project.Controller LIKE CASE WHEN @Controller IS NULL THEN '%' ELSE @Controller END)

			-- UnBilledCost_Insert
			SELECT 

			unbilledcost.GroupDivision,
unbilledcost.TimeDate,
unbilledcost.EInitials,
unbilledcost.Details,
unbilledcost.EName,
unbilledcost.TimeSheet,
unbilledcost.BilledDoller,
unbilledcost.BilledStatus,
unbilledcost.CostID,
unbilledcost.ProjectTitle,
unbilledcost.JobNo,
unbilledcost.ProjectValue,
			 0 AS [Convert]
				, CAST(0 AS FLOAT) AS Rate 
			INTO #TempUnBilledCostInsert3
			FROM (##JPInvoicedUnBilledCost unbilledcost INNER JOIN Account24918.[vBilling Information] BillingInformation ON unbilledcost.JobNo = BillingInformation.[Project Number])
				LEFT JOIN  Account24918.vProject Project ON BillingInformation.[Project Number] = Project.ProjectNumber
			WHERE (Project.ProjectNumber<>'Default')
				AND (Project.Status<>'1' AND Project.Status<>'3')
			--	AND ((BillingInformation.Date)>=@StartDate AND (BillingInformation.Date)<=@EndDate) and isdate(BillingInformation.Date) =1
				AND (Project.[Group/Division] LIKE CASE WHEN @GroupDivision IS NULL THEN '%' ELSE @GroupDivision END)
				AND (Project.Controller LIKE CASE WHEN @Controller IS NULL THEN '%' ELSE @Controller END)

		END 
	ELSE IF (@ReportType=4)
		BEGIN
			SELECT DISTINCT Project.[Group/Division] as GroupDivision
				, TimeSheet.Date AS TimeDate
				, Employee_List.Initials AS EInitials
				, TimeSheet.Details
				, EmployeeHistroy.Name AS EName
				, CASE WHEN TimeSheet.[Status]='5' THEN ISNULL(TimeSheet.Total,'0') ELSE ISNULL(TimeSheet.[$_Amount],'0') END AS TimeSheet
				, ISNULL(TimeSheet.[$_Amount],'0') AS BilledDoller
				, CASE WHEN TimeSheet.STATUS='0' THEN 'By Rate' ELSE
					CASE WHEN TimeSheet.STATUS='1' THEN 'Hold' ELSE
						CASE WHEN TimeSheet.STATUS='3' THEN 'Fixed' ELSE
							CASE WHEN TimeSheet.STATUS='4' THEN 'No Charge' ELSE
								CASE WHEN TimeSheet.STATUS='5' THEN 'Write-off' ELSE
									CASE WHEN TimeSheet.STATUS='6' THEN 'Adjusted' ELSE
										'NONE'
									END
								END
							END
						END
					END
				  END AS BilledStatus
				, PNEmployeeIDTimeSheetID.TimeSheetID
				, BillingInformation.ID AS BillingID, BillingInformation.[Invoice No], BillingInformation.[Invoice Amount Converted] as InvoiceAmount
				, BillingInformation.Date AS InvoiceDate, BillingInformation.[Tax Name], BillingInformation.[Tax Amount], BillingInformation.[Tax Rate], Project.ProjectNumber as JobNo
				, Project.ProjectTitle
				, CASE WHEN ISNULL(Project.CurrencyRate,'')='' THEN CAST(Project.Value AS MONEY) ELSE 
					CAST(CASE WHEN ISNUMERIC(Project.Value)=1 THEN CAST(Project.Value AS MONEY)/CAST(Project.CurrencyRate AS FLOAT) ELSE CAST(Project.Value AS MONEY) END AS VARCHAR(255))
				  END AS ProjectValue
				, CASE WHEN ISNULL(Project.CurrencyRate,'')='' THEN '1' ELSE Project.CurrencyRate END AS ExchangeRate
				, CASE WHEN cast(TimeSheet.[$_Amount] as money)=0 THEN 0 ELSE (cast(TimeSheet.[$_Billed] as money)/ cast(TimeSheet.[$_Amount] as money)) * 100 END AS PercentBilled
				, CASE WHEN cast(TimeSheet.[$_Amount] as money)=0 THEN 0 ELSE ((cast(TimeSheet.[$_Amount] as money)- cast(TimeSheet.[$_Billed] as money))/cast(TimeSheet.[$_Amount] as money)) * 100 END AS PercentUnBilled
				, 0 AS [Convert]
				, CAST(0 AS MONEY) AS InterimInvoiceAmount
				, CAST(0 AS MONEY) AS InterimInvoiceTaxAmount
				, CAST(0 AS FLOAT) AS Rate 
			INTO #TempBilledTimeInsert4
			FROM (Account24918.vEmployee_List Employee_List INNER JOIN ((Account24918.vProject Project RIGHT JOIN ((Account24918.vPNEmployeeIDTimeSheetID PNEmployeeIDTimeSheetID INNER JOIN Account24918.vTimeSheet TimeSheet ON PNEmployeeIDTimeSheetID.TimeSheetID = TimeSheet.TimeSheetID)
				INNER JOIN Account24918.[vBilling Information] BillingInformation ON (TimeSheet.InvoiceNo = BillingInformation.[Invoice No]) AND (PNEmployeeIDTimeSheetID.ProjectNumber = BillingInformation.[Project Number])) ON Project.ProjectNumber = BillingInformation.[Project Number])
				LEFT JOIN Account24918.vEmployeeHistroy EmployeeHistroy ON Project.EmployeeID = EmployeeHistroy.EmployeeID) ON Employee_List.EmployeeID = PNEmployeeIDTimeSheetID.EmployeeID)
				LEFT JOIN #JPCompletedByMaxInvoiceDate ON Project.ProjectNumber = #JPCompletedByMaxInvoiceDate.[Project Number]			
			WHERE (Project.ProjectNumber<>'Default')
				AND (Project.Status IN ('4','5','6'))
				AND (TimeSheet.Billed='1')
				AND (BillingInformation.Type='1')
				AND (#JPCompletedByMaxInvoiceDate.MaxOfDate>=@StartDate AND #JPCompletedByMaxInvoiceDate.MaxOfDate<=@EndDate)
				AND (Project.[Group/Division] LIKE CASE WHEN @GroupDivision IS NULL THEN '%' ELSE @GroupDivision END)
				AND (Project.Controller LIKE CASE WHEN @Controller IS NULL THEN '%' ELSE @Controller END)

			-- BilledCost_Insert
			SELECT DISTINCT Project.[Group/Division] as GroupDivision
				, TimeSheet_Cost.Date as TimeDate
				, Employee_List.Initials AS EInitials
				, TimeSheet_Cost.Details, EmployeeHistroy.Name as EName
				, CASE WHEN TimeSheet_Cost.STATUS='5' THEN 
					CASE WHEN TimeSheet_Cost.Flag='1' THEN
						ISNULL(TimeSheet_Cost.Dollar_Amount,'0') 
					ELSE ISNULL(cast(TimeSheet_Cost.Units as money),0)*ISNULL( cast(TimeSheet_Cost.Rate as money),0)
					END
				  ELSE
					ISNULL(TimeSheet_Cost.[$_Amount],'0') 
				  END AS TimeSheet
				, ISNULL(TimeSheet_Cost.[$_Amount],'0') AS BilledDoller
				, CASE WHEN TimeSheet_Cost.STATUS='0' THEN 'By Rate' ELSE
					CASE WHEN TimeSheet_Cost.STATUS='1' THEN 'Hold' ELSE
						CASE WHEN TimeSheet_Cost.STATUS='3' THEN 'Fixed' ELSE
							CASE WHEN TimeSheet_Cost.STATUS='4' THEN 'No Charge' ELSE
								CASE WHEN TimeSheet_Cost.STATUS='5' THEN 'Write-off' ELSE
									CASE WHEN TimeSheet_Cost.STATUS='6' THEN 'Adjusted' ELSE
										'NONE'
									END
								END
							END
						END
					END
				  END AS BilledStatus
				, PNEmployeeIDCostID.CostID
				, BillingInformation.ID AS BillingID
				, BillingInformation.[Invoice No]
				, BillingInformation.[Invoice Amount Converted] as InvoiceAmount
				, BillingInformation.Date AS InvoiceDate
				, BillingInformation.[Tax Name]
				, BillingInformation.[Tax Amount] as [Tax Amount]
				, BillingInformation.[Tax Rate] as TaxRate, Project.ProjectNumber as JobNo
				, Project.ProjectTitle
				, CASE WHEN ISNULL(Project.CurrencyRate,'')='' THEN CAST(Project.Value AS MONEY) ELSE 
					CAST(CASE WHEN ISNUMERIC(Project.Value)=1 THEN CAST(Project.Value AS MONEY)/CAST(Project.CurrencyRate AS FLOAT) ELSE CAST(Project.Value AS MONEY) END AS VARCHAR(255))
				  END AS ProjectValue
				, CASE WHEN ISNULL(Project.CurrencyRate,'')='' THEN '1' ELSE Project.CurrencyRate END AS ExchangeRate
				, CASE WHEN cast(TimeSheet_Cost.[$_Amount] as money)=0 THEN 0 ELSE 100 END AS PercentBilled
				, 0 AS [Convert]
				, CAST(0 AS MONEY) AS InterimInvoiceAmount
				, CAST(0 AS MONEY) AS InterimInvoiceTaxAmount
				, CAST(0 AS FLOAT) AS Rate
			INTO #TempBilledCostInsert4
			FROM (Account24918.vEmployee_List Employee_List INNER JOIN (((Account24918.vPNEmployeeIDCostID PNEmployeeIDCostID INNER JOIN (Account24918.vTimeSheet_Cost TimeSheet_Cost INNER JOIN Account24918.[vBilling Information] BillingInformation ON TimeSheet_Cost.InvoiceNo = BillingInformation.[Invoice No]) ON (TimeSheet_Cost.CostID = PNEmployeeIDCostID.CostID) AND (PNEmployeeIDCostID.ProjectNumber = BillingInformation.[Project Number]))
				LEFT JOIN Account24918.vProject Project ON BillingInformation.[Project Number] = Project.ProjectNumber) LEFT JOIN Account24918.vEmployeeHistroy EmployeeHistroy ON Project.EmployeeID = EmployeeHistroy.EmployeeID) ON Employee_List.EmployeeID = PNEmployeeIDCostID.EmployeeID)
				LEFT JOIN #JPCompletedByMaxInvoiceDate ON Project.ProjectNumber = #JPCompletedByMaxInvoiceDate.[Project Number] 
			WHERE (Project.ProjectNumber<>'Default')
				AND (Project.Status IN ('4','5','6'))
				AND (TimeSheet_Cost.Billed='1')
				AND (BillingInformation.Type='1')
				AND (#JPCompletedByMaxInvoiceDate.MaxOfDate>=@StartDate AND #JPCompletedByMaxInvoiceDate.MaxOfDate<=@EndDate)
				AND (Project.[Group/Division] LIKE CASE WHEN @GroupDivision IS NULL THEN '%' ELSE @GroupDivision END)
				AND (Project.Controller LIKE CASE WHEN @Controller IS NULL THEN '%' ELSE @Controller END)

			-- UnBilledTime_Insert
			-- UnBilledCost_Insert
		END

	IF (@ReportType=1)
		BEGIN
			-- BilledTime_Insert update
			UPDATE #TempBilledTimeInsert1
			SET [Convert] = CASE WHEN LEN(dbo.getGroupReportingCurrency(GroupDivision))>1 THEN 1 ELSE 0 END
				, InterimInvoiceAmount = ISNULL((SELECT SUM(Amount) FROM v_InterimInvoicesAmount WHERE FullInvoiceId=#TempBilledTimeInsert1.BillingID),0)
				, InterimInvoiceTaxAmount = ISNULL((SELECT SUM(Tax) FROM v_InterimInvoicesAmount WHERE FullInvoiceId=#TempBilledTimeInsert1.BillingID),0)
				, Rate = dbo.getJobReportingRate(JobNo)

			-- BilledCost_Insert update
			UPDATE #TempBilledCostInsert1
			SET [Convert] = CASE WHEN LEN(dbo.getGroupReportingCurrency(GroupDivision))>1 THEN 1 ELSE 0 END
				, InterimInvoiceAmount = ISNULL((SELECT SUM(Amount) FROM v_InterimInvoicesAmount WHERE FullInvoiceId=#TempBilledCostInsert1.BillingID),0)
				, InterimInvoiceTaxAmount = ISNULL((SELECT SUM(Tax) FROM v_InterimInvoicesAmount WHERE FullInvoiceId=#TempBilledCostInsert1.BillingID),0)
				, Rate = dbo.getJobReportingRate(JobNo)

			-- UnBilledTime_Insert update
			UPDATE #TempUnBilledTimeInsert1
			SET [Convert] = CASE WHEN LEN(dbo.getGroupReportingCurrency(GroupDivision))>1 THEN 1 ELSE 0 END
				, Rate = dbo.getJobReportingRate(JobNo)

			-- UnBilledCost_Insert update
			UPDATE #TempUnBilledCostInsert1
			SET [Convert] = CASE WHEN LEN(dbo.getGroupReportingCurrency(GroupDivision))>1 THEN 1 ELSE 0 END
				, Rate = dbo.getJobReportingRate(JobNo)

			SELECT GroupDivision
				, TimeDate
				, EInitials AS Initials
				, Details AS TimeDetails
				, EName AS ControllerName
				, 1 AS Billed
				, BilledStatus
				, [Invoice No] 
				, InvoiceDate
				, [Tax Name]
				,  JobNo
				, ProjectTitle
				, PercentBilled
				, PercentUnBilled
				, 1 AS IsTime
				, CASE WHEN [Convert]=1 THEN TimeSheet*Rate ELSE TimeSheet END AS TimeSheet
				, CASE WHEN [Convert]=1 THEN BilledDoller*Rate ELSE BilledDoller END AS BilledDoller
				, CASE WHEN [Convert]=1 THEN (InvoiceAmount+InterimInvoiceAmount)*Rate ELSE (InvoiceAmount+InterimInvoiceAmount) END AS InvoiceAmount
				, CASE WHEN [Convert]=1 THEN ([Tax Amount]+InterimInvoiceTaxAmount)*Rate ELSE ([Tax Amount]+InterimInvoiceTaxAmount) END AS TaxAmount
				, CASE WHEN [Convert]=1 THEN ProjectValue*Rate ELSE ProjectValue END AS JobValue
				, CASE WHEN [Convert]=1 THEN ExchangeRate*Rate ELSE ExchangeRate END AS ExchangeRate
				, GETDATE() AS RecordedDate
			FROM  #TempBilledTimeInsert1
			UNION
			SELECT GroupDivision
				, TimeDate
				, EInitials AS Initials
				, Details AS TimeDetails
				, EName AS ControllerName
				, 1 AS Billed
				, BilledStatus
				, [Invoice No] 
				, InvoiceDate
				,  [Tax Name]
				,  JobNo
				, ProjectTitle
				, PercentBilled
				, 0 AS PercentUnBilled
				, 0 AS IsTime
				, CASE WHEN [Convert]=1 THEN TimeSheet*Rate ELSE TimeSheet END AS TimeSheet
				, CASE WHEN [Convert]=1 THEN BilledDoller*Rate ELSE BilledDoller END AS BilledDoller
				, CASE WHEN [Convert]=1 THEN (InvoiceAmount+InterimInvoiceAmount)*Rate ELSE (InvoiceAmount+InterimInvoiceAmount) END AS InvoiceAmount
				, CASE WHEN [Convert]=1 THEN ([Tax Amount]+InterimInvoiceTaxAmount)*Rate ELSE ([Tax Amount]+InterimInvoiceTaxAmount) END AS TaxAmount
				, CASE WHEN [Convert]=1 THEN ProjectValue*Rate ELSE ProjectValue END AS JobValue
				, ExchangeRate
				, GETDATE() AS RecordedDate
			FROM  #TempBilledCostInsert1
			UNION ALL
			SELECT GroupDivision
				, TimeDate
				, EInitials AS Initials
				, Details AS TimeDetails
				, EName AS ControllerName
				, 0 AS Billed
				, BilledStatus
				, NULL AS [Invoice No] 
				, NULL AS InvoiceDate
				, NULL AS [Tax Name]
				, JobNo
				, ProjectTitle
				, 0 AS PercentBilled
				, 0 AS PercentUnBilled
				, 1 AS IsTime
				, CASE WHEN [Convert]=1 THEN TimeSheet*Rate ELSE TimeSheet END AS TimeSheet
				, CASE WHEN [Convert]=1 THEN BilledDoller*Rate ELSE BilledDoller END AS BilledDoller
				, 0 AS InvoiceAmount
				, 0 AS [Tax Amount]
				, CASE WHEN [Convert]=1 THEN ProjectValue*Rate ELSE ProjectValue END AS JobValue
				, 0 AS ExchangeRate
				, GETDATE() AS RecordedDate
			FROM #TempUnBilledTimeInsert1
			UNION ALL
			SELECT GroupDivision
				, TimeDate
				, EInitials AS Initials
				, Details AS TimeDetails
				, EName AS ControllerName
				, 0 AS Billed
				, BilledStatus
				, NULL AS [Invoice No] 
				, NULL AS InvoiceDate
				, NULL AS [Tax Name]
				, JobNo
				, ProjectTitle
				, 0 AS PercentBilled
				, 0 AS PercentUnBilled
				, 0 AS IsTime
				, CASE WHEN [Convert]=1 THEN TimeSheet*Rate ELSE TimeSheet END AS TimeSheet
				, CASE WHEN [Convert]=1 THEN BilledDoller*Rate ELSE BilledDoller END AS BilledDoller
				, 0 AS InvoiceAmount
				, 0 AS [Tax Amount]
				, CASE WHEN [Convert]=1 THEN ProjectValue*Rate ELSE ProjectValue END AS JobValue
				, 0 AS ExchangeRate
				, GETDATE() AS RecordedDate
			FROM #TempUnBilledCostInsert1
		END
	ELSE IF (@ReportType=2)
		BEGIN
			-- BilledTime_Insert update
			UPDATE #TempBilledTimeInsert2
			SET [Convert] = CASE WHEN LEN(dbo.getGroupReportingCurrency(GroupDivision))>1 THEN 1 ELSE 0 END
				, InterimInvoiceAmount = ISNULL((SELECT SUM(Amount) FROM v_InterimInvoicesAmount WHERE FullInvoiceId=#TempBilledTimeInsert2.BillingID),0)
				, InterimInvoiceTaxAmount = ISNULL((SELECT SUM(Tax) FROM v_InterimInvoicesAmount WHERE FullInvoiceId=#TempBilledTimeInsert2.BillingID),0)
				, Rate = dbo.getJobReportingRate(JobNo)
			
			-- BilledCost_Insert update
			UPDATE #TempBilledCostInsert2
			SET [Convert] = CASE WHEN LEN(dbo.getGroupReportingCurrency(GroupDivision))>1 THEN 1 ELSE 0 END
				, InterimInvoiceAmount = ISNULL((SELECT SUM(Amount) FROM v_InterimInvoicesAmount WHERE FullInvoiceId=#TempBilledCostInsert2.BillingID),0)
				, InterimInvoiceTaxAmount = ISNULL((SELECT SUM(Tax) FROM v_InterimInvoicesAmount WHERE FullInvoiceId=#TempBilledCostInsert2.BillingID),0)
				, Rate = dbo.getJobReportingRate(JobNo)

			-- UnBilledTime_Insert update
			UPDATE #TempUnBilledTimeInsert2
			SET [Convert] = CASE WHEN LEN(dbo.getGroupReportingCurrency(GroupDivision))>1 THEN 1 ELSE 0 END
				, Rate = dbo.getJobReportingRate(JobNo)

			-- UnBilledCost_Insert update
			UPDATE #TempUnBilledCostInsert2
			SET [Convert] = CASE WHEN LEN(dbo.getGroupReportingCurrency(GroupDivision))>1 THEN 1 ELSE 0 END
				, Rate = dbo.getJobReportingRate(JobNo)

			SELECT GroupDivision
				, TimeDate
				, EInitials AS Initials
				, Details AS TimeDetails
				, EName AS ControllerName
				, 1 AS Billed
				, BilledStatus
				, [Invoice No] 
				, [InvoiceDate]
				, [Tax Name]
				,  JobNo
				, ProjectTitle
				, PercentBilled
				, PercentUnBilled
				, 1 AS IsTime
				, CASE WHEN [Convert]=1 THEN TimeSheet*Rate ELSE TimeSheet END AS TimeSheet
				, CASE WHEN [Convert]=1 THEN BilledDoller*Rate ELSE BilledDoller END AS BilledDoller
				, CASE WHEN [Convert]=1 THEN (InvoiceAmount+InterimInvoiceAmount)*Rate ELSE (InvoiceAmount+InterimInvoiceAmount) END AS InvoiceAmount
				, CASE WHEN [Convert]=1 THEN ([Tax Amount]+InterimInvoiceTaxAmount)*Rate ELSE ([Tax Amount]+InterimInvoiceTaxAmount) END AS TaxAmount
				, CASE WHEN [Convert]=1 THEN ProjectValue*Rate ELSE ProjectValue END AS JobValue
				, CASE WHEN [Convert]=1 THEN ExchangeRate*Rate ELSE ExchangeRate END AS ExchangeRate
				, GETDATE() AS RecordedDate
			FROM  #TempBilledTimeInsert2
			UNION ALL
			SELECT GroupDivision
				, TimeDate
				, EInitials AS Initials
				, Details AS TimeDetails
				, EName AS ControllerName
				, 1 AS Billed
				, BilledStatus
				, [Invoice No] 
				, InvoiceDate
				, [Tax Name]
				,  JobNo
				, ProjectTitle
				, PercentBilled
				, 0 AS PercentUnBilled
				, 0 AS IsTime
				, CASE WHEN [Convert]=1 THEN TimeSheet*Rate ELSE TimeSheet END AS TimeSheet
				, CASE WHEN [Convert]=1 THEN BilledDoller*Rate ELSE BilledDoller END AS BilledDoller
				, CASE WHEN [Convert]=1 THEN (InvoiceAmount+InterimInvoiceAmount)*Rate ELSE (InvoiceAmount+InterimInvoiceAmount) END AS InvoiceAmount
				, CASE WHEN [Convert]=1 THEN ([Tax Amount]+InterimInvoiceTaxAmount)*Rate ELSE ([Tax Amount]+InterimInvoiceTaxAmount) END AS TaxAmount
				, CASE WHEN [Convert]=1 THEN ProjectValue*Rate ELSE ProjectValue END AS JobValue
				, ExchangeRate
				, GETDATE() AS RecordedDate
			FROM  #TempBilledCostInsert2
			UNION ALL
			SELECT GroupDivision
				, TimeDate
				, EInitials AS Initials
				, Details AS TimeDetails
				, EName AS ControllerName
				, 0 AS Billed
				, BilledStatus
				, NULL AS [Invoice No] 
				, NULL AS InvoiceDate
				, NULL AS [Tax Name]
				, JobNo
				, ProjectTitle
				, 0 AS PercentBilled
				, 0 AS PercentUnBilled
				, 1 AS IsTime
				, CASE WHEN [Convert]=1 THEN TimeSheet*Rate ELSE TimeSheet END AS TimeSheet
				, CASE WHEN [Convert]=1 THEN BilledDoller*Rate ELSE BilledDoller END AS BilledDoller
				, 0 AS InvoiceAmount
				, 0 AS [Tax Amount]
				, CASE WHEN [Convert]=1 THEN ProjectValue*Rate ELSE ProjectValue END AS JobValue
				, 0 AS ExchangeRate
				, GETDATE() AS RecordedDate
			FROM #TempUnBilledTimeInsert2
			UNION ALL
			SELECT GroupDivision
				, TimeDate
				, EInitials AS Initials
				, Details AS TimeDetails
				, EName AS ControllerName
				, 0 AS Billed
				, BilledStatus
				, NULL AS [Invoice No] 
				, NULL AS InvoiceDate
				, NULL AS [Tax Name]
				, JobNo
				, ProjectTitle
				, 0 AS PercentBilled
				, 0 AS PercentUnBilled
				, 0 AS IsTime
				, CASE WHEN [Convert]=1 THEN TimeSheet*Rate ELSE TimeSheet END AS TimeSheet
				, CASE WHEN [Convert]=1 THEN BilledDoller*Rate ELSE BilledDoller END AS BilledDoller
				, 0 AS [Invoice Amount]
				, 0 AS TaxAmount
				, CASE WHEN [Convert]=1 THEN ProjectValue*Rate ELSE ProjectValue END AS JobValue
				, 0 AS ExchangeRate
				, GETDATE() AS RecordedDate
			FROM #TempUnBilledCostInsert2
		END
	ELSE IF (@ReportType=3)
		BEGIN
			-- BilledTime_Insert Update
			UPDATE #TempBilledTimeInsert3
			SET [Convert] = CASE WHEN LEN(dbo.getGroupReportingCurrency(GroupDivision))>1 THEN 1 ELSE 0 END
				, InterimInvoiceAmount = ISNULL((SELECT SUM(Amount) FROM v_InterimInvoicesAmount WHERE FullInvoiceId=#TempBilledTimeInsert3.BillingID),0)
				, InterimInvoiceTaxAmount = ISNULL((SELECT SUM(Tax) FROM v_InterimInvoicesAmount WHERE FullInvoiceId=#TempBilledTimeInsert3.BillingID),0)
				, Rate = dbo.getJobReportingRate(JobNo)

			-- BilledCost_Insert Update
			UPDATE #TempBilledCostInsert3
			SET [Convert] = CASE WHEN LEN(dbo.getGroupReportingCurrency(GroupDivision))>1 THEN 1 ELSE 0 END
				, InterimInvoiceAmount = ISNULL((SELECT SUM(Amount) FROM v_InterimInvoicesAmount WHERE FullInvoiceId=#TempBilledCostInsert3.BillingID),0)
				, InterimInvoiceTaxAmount = ISNULL((SELECT SUM(Tax) FROM v_InterimInvoicesAmount WHERE FullInvoiceId=#TempBilledCostInsert3.BillingID),0)
				, Rate = dbo.getJobReportingRate(JobNo)

			-- UnBilledTime_Insert update
			UPDATE #TempUnBilledTimeInsert3
			SET [Convert] = CASE WHEN LEN(dbo.getGroupReportingCurrency(GroupDivision))>1 THEN 1 ELSE 0 END
				, Rate = dbo.getJobReportingRate(JobNo)

			-- UnBilledCost_Insert update
			UPDATE #TempUnBilledCostInsert3
			SET [Convert] = CASE WHEN LEN(dbo.getGroupReportingCurrency(GroupDivision))>1 THEN 1 ELSE 0 END
				, Rate = dbo.getJobReportingRate(JobNo)

			SELECT GroupDivision
				, TimeDate
				, EInitials AS Initials
				, Details AS TimeDetails
				, EName AS ControllerName
				, 1 AS Billed
				, BilledStatus
				, [Invoice No] 
				, [InvoiceDate]
				, [Tax Name]
				, JobNo
				, ProjectTitle
				, PercentBilled
				, PercentUnBilled
				, 1 AS IsTime
				, CASE WHEN [Convert]='1' THEN cast(TimeSheet as money)* cast(Rate as money) ELSE cast(TimeSheet as money) END AS TimeSheet
				, CASE WHEN [Convert]='1' THEN cast(BilledDoller as money)* cast(Rate as money) ELSE cast(BilledDoller as money) END AS BilledDoller
				, CASE WHEN [Convert]='1' THEN (cast(InvoiceAmount as money)+cast(InterimInvoiceAmount as money))*cast(Rate as money) ELSE (cast(InvoiceAmount as money)+cast(InterimInvoiceAmount  as money)) END AS InvoiceAmount
				, CASE WHEN [Convert]='1' THEN (cast([Tax Amount] as money) +cast(InterimInvoiceTaxAmount as money))*cast(Rate as money) ELSE (cast([Tax Amount] as money)+cast(InterimInvoiceTaxAmount as money)) END AS TaxAmount
				, CASE WHEN [Convert]='1' THEN cast(ProjectValue as money)* cast(Rate as money) ELSE cast(ProjectValue as money) END AS JobValue
				, CASE WHEN [Convert]='1' THEN cast(ExchangeRate as money)* cast(Rate as money) ELSE cast(ExchangeRate as money) END AS ExchangeRate
				, GETDATE() AS RecordedDate
			FROM  #TempBilledTimeInsert3
			UNION ALL
			SELECT GroupDivision
				, TimeDate
				, EInitials AS Initials
				, Details AS TimeDetails
				, EName AS ControllerName
				, 1 AS Billed
				, BilledStatus
				, [Invoice No] 
				, InvoiceDate
				, [Tax Name]
				, JobNo
				, ProjectTitle
				, PercentBilled
				, 0 AS PercentUnBilled
				, 0 AS IsTime
				, CASE WHEN [Convert]='1' THEN cast(TimeSheet as money)*cast(Rate as money) ELSE cast(TimeSheet as money) END AS TimeSheet
				, CASE WHEN [Convert]='1' THEN cast(BilledDoller as money)*cast(Rate as money) ELSE cast(BilledDoller as money) END AS BilledDoller
				, CASE WHEN [Convert]='1' THEN (cast(InvoiceAmount as money)+cast(InterimInvoiceAmount as money))*cast(Rate as money) ELSE (cast(InvoiceAmount as money)+cast(InterimInvoiceAmount as money)) END AS InvoiceAmount
				, CASE WHEN [Convert]='1' THEN (cast([Tax Amount] as money)+cast(InterimInvoiceTaxAmount as money))* cast(Rate as money) ELSE (cast([Tax Amount] as money)+cast(InterimInvoiceTaxAmount as money)) END AS TaxAmount
				, CASE WHEN [Convert]='1' THEN cast(ProjectValue as money)*cast(Rate as money) ELSE cast(ProjectValue as money) END AS JobValue
				, ExchangeRate
				, GETDATE() AS RecordedDate
			FROM  #TempBilledCostInsert3
			UNION ALL
			SELECT GroupDivision
				, TimeDate
				, EInitials AS Initials
				, Details AS TimeDetails
				, EName AS ControllerName
				, 0 AS Billed
				, BilledStatus
				, NULL AS [Invoice No] 
				, NULL AS InvoiceDate
				, NULL AS [Tax Name]
				, JobNo
				, ProjectTitle
				, 0 AS PercentBilled
				, 0 AS PercentUnBilled
				, 1 AS IsTime
				, CASE WHEN [Convert]='1' THEN cast(TimeSheet as money)* cast(Rate as money) ELSE cast(TimeSheet as money) END AS TimeSheet
				, CASE WHEN [Convert]='1' THEN cast(BilledDoller as money)* cast(Rate as money) ELSE cast(BilledDoller as money) END AS BilledDoller
				, 0 AS InvoiceAmount
				, 0 AS [Tax Amount]
				, CASE WHEN [Convert]='1' THEN cast(ProjectValue as money)* cast(Rate as money) ELSE cast(ProjectValue as money) END AS JobValue
				, 0 AS ExchangeRate
				, GETDATE() AS RecordedDate
			FROM #TempUnBilledTimeInsert3
			UNION ALL
			SELECT GroupDivision
				, TimeDate
				, EInitials AS Initials
				, Details AS TimeDetails
				, EName AS ControllerName
				, 0 AS Billed
				, BilledStatus
				, NULL AS [Invoice No] 
				, NULL AS InvoiceDate
				, NULL AS [Tax Name]
				, JobNo
				, ProjectTitle
				, 0 AS PercentBilled
				, 0 AS PercentUnBilled
				, 0 AS IsTime
				, CASE WHEN [Convert]='1' THEN cast(TimeSheet as money)* cast(Rate as money) ELSE cast(TimeSheet as money) END AS TimeSheet
				, CASE WHEN [Convert]='1' THEN cast(BilledDoller as money)*cast(Rate as money) ELSE cast(BilledDoller as money) END AS BilledDoller
				, 0 AS InvoiceAmount
				, 0 AS [Tax Amount]
				, CASE WHEN [Convert]='1' THEN cast(ProjectValue as money)* cast(Rate as money) ELSE cast(ProjectValue as money) END AS JobValue
				, 0 AS ExchangeRate
				, GETDATE() AS RecordedDate
			FROM #TempUnBilledCostInsert3

		END 
	ELSE IF (@ReportType=4)
		BEGIN
			-- BilledTime_Insert Update
			UPDATE #TempBilledTimeInsert4
			SET [Convert] = CASE WHEN LEN(dbo.getGroupReportingCurrency(GroupDivision))>1 THEN 1 ELSE 0 END
				, InterimInvoiceAmount = ISNULL((SELECT SUM(Amount) FROM v_InterimInvoicesAmount WHERE FullInvoiceId=#TempBilledTimeInsert4.BillingID),0)
				, InterimInvoiceTaxAmount = ISNULL((SELECT SUM(Tax) FROM v_InterimInvoicesAmount WHERE FullInvoiceId=#TempBilledTimeInsert4.BillingID),0)
				, Rate = dbo.getJobReportingRate(JobNo)

			-- BilledCost_Insert Update
			UPDATE #TempBilledCostInsert4
			SET [Convert] = CASE WHEN LEN(dbo.getGroupReportingCurrency(GroupDivision))>1 THEN 1 ELSE 0 END
				, InterimInvoiceAmount = ISNULL((SELECT SUM(Amount) FROM v_InterimInvoicesAmount WHERE FullInvoiceId=#TempBilledCostInsert4.BillingID),0)
				, InterimInvoiceTaxAmount = ISNULL((SELECT SUM(Tax) FROM v_InterimInvoicesAmount WHERE FullInvoiceId=#TempBilledCostInsert4.BillingID),0)
				, Rate = dbo.getJobReportingRate(JobNo)

			-- UnBilledTime_Insert update
			-- UnBilledCost_Insert update

			SELECT GroupDivision
				, TimeDate
				, EInitials AS Initials
				, Details AS TimeDetails
				, EName AS ControllerName
				, 1 AS Billed
				, BilledStatus
				, [Invoice No] 
				, InvoiceDate
				, [Tax Name]
				,  JobNo
				, ProjectTitle
				, PercentBilled
				, PercentUnBilled
				, 1 AS IsTime
				, CASE WHEN [Convert]='1' THEN cast(TimeSheet as money)* cast(Rate as money) ELSE TimeSheet END AS TimeSheet
				, CASE WHEN [Convert]='1' THEN cast(BilledDoller as money)*cast(Rate as money) ELSE BilledDoller END AS BilledDoller
				, CASE WHEN [Convert]='1' THEN (cast(InvoiceAmount as money)+cast(InterimInvoiceAmount as money))*cast( Rate as money) ELSE (cast(InvoiceAmount as money)+cast(InterimInvoiceAmount as money)) END AS InvoiceAmount
				, CASE WHEN [Convert]='1' THEN (cast([Tax Amount] as money)+cast(InterimInvoiceTaxAmount as money))*cast(Rate as money) ELSE (cast([Tax Amount] as money)+cast(InterimInvoiceTaxAmount as money)) END AS TaxAmount
				, CASE WHEN [Convert]='1' THEN cast(ProjectValue as money)* cast(Rate as money) ELSE cast(ProjectValue as money) END AS JobValue
				, CASE WHEN [Convert]='1' THEN cast(ExchangeRate as money)* cast(Rate as money) ELSE cast(ExchangeRate as money) END AS ExchangeRate
				, GETDATE() AS RecordedDate
			FROM  #TempBilledTimeInsert4
			UNION ALL
			SELECT GroupDivision
				, TimeDate
				, EInitials AS Initials
				, Details AS TimeDetails
				, EName AS ControllerName
				, 1 AS Billed
				, BilledStatus
				, [Invoice No] 
				, InvoiceDate
				, [Tax Name]
				,  JobNo
				, ProjectTitle
				, PercentBilled
				, 0 AS PercentUnBilled
				, 0 AS IsTime
				, CASE WHEN [Convert]='1' THEN cast(TimeSheet as money)* cast(Rate as money) ELSE TimeSheet END AS TimeSheet
				, CASE WHEN [Convert]='1' THEN cast(BilledDoller as money)* cast(Rate as money) ELSE BilledDoller END AS BilledDoller
				, CASE WHEN [Convert]='1' THEN (cast(InvoiceAmount as money)+ cast(InterimInvoiceAmount as money))*cast(Rate as money) ELSE (cast(InvoiceAmount as money)+ cast(InterimInvoiceAmount as money)) END AS InvoiceAmount
				, CASE WHEN [Convert]='1' THEN (cast([Tax Amount] as money)+ cast(InterimInvoiceTaxAmount as money))* cast(Rate as money) ELSE (cast([Tax Amount] as money)+ cast(InterimInvoiceTaxAmount as money)) END AS TaxAmount
				, CASE WHEN [Convert]='1' THEN ProjectValue*Rate ELSE ProjectValue END AS JobValue
				, ExchangeRate
				, GETDATE() AS RecordedDate
			FROM  #TempBilledCostInsert4
		END 
	ELSE
		SELECT 0

	-- BilledTime_Insert temp tables
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledTimeInsert1' )) DROP TABLE #TempBilledTimeInsert1
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledTimeInsert2' )) DROP TABLE #TempBilledTimeInsert2
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledTimeInsert3' )) DROP TABLE #TempBilledTimeInsert3
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledTimeInsert4' )) DROP TABLE #TempBilledTimeInsert4

	-- BilledCost_Insert temp tables
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledCostInsert' )) DROP TABLE #TempBilledCostInsert
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledCostInsert1' )) DROP TABLE #TempBilledCostInsert1
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledCostInsert2' )) DROP TABLE #TempBilledCostInsert2
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledCostInsert3' )) DROP TABLE #TempBilledCostInsert3
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempBilledCostInsert4' )) DROP TABLE #TempBilledCostInsert4

	-- UnBilledTime_Insert temp tables
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempUnBilledTimeInsert' )) DROP TABLE #TempUnBilledTimeInsert
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempUnBilledTimeInsert1' )) DROP TABLE #TempUnBilledTimeInsert1
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempUnBilledTimeInsert2' )) DROP TABLE #TempUnBilledTimeInsert2
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempUnBilledTimeInsert3' )) DROP TABLE #TempUnBilledTimeInsert3

	-- UnBilledCost_Insert temp tables
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempUnBilledCostInsert' )) DROP TABLE #TempUnBilledCostInsert
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempUnBilledCostInsert1' )) DROP TABLE #TempUnBilledCostInsert1
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempUnBilledCostInsert2' )) DROP TABLE #TempUnBilledCostInsert2
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TempUnBilledCostInsert3' )) DROP TABLE #TempUnBilledCostInsert3
END

--EXEC dbo.usr_JobPerformanceReport 1, '''WC717-01'',''WC720-01'',''WC722-01''', NULL, NULL, NULL, NULL
--EXEC dbo.usr_JobPerformanceReport 2, NULL, NULL, NULL, 'Windtech (Asia)', 'AAL'
--EXEC dbo.usr_JobPerformanceReport 3, NULL, '2014-01-01', '2014-01-10', NULL, 'AAL'
--EXEC dbo.usr_JobPerformanceReport 4, NULL, '2014-01-01', '2014-01-15', 'Windtech (Asia)', NULL

--EXEC dbo.usr_JobPerformanceReport 3, 'DBG001-02', null, null, NULL, 'AAL'