﻿

CREATE PROCEDURE   [dbo].[Record_Remove_Duplicate]

	/*----------------------------------------------------------------

	---------------------------------------------------------------*/
 
AS
 
BEGIN TRY


	-- Make old duplicates IsActive=0 for a db

	DECLARE @sRecordIDs varchar(MAX)='-1';
	DECLARE T_Cursor CURSOR FOR
	SELECT DISTINCT TableID FROM [Record] WHERE IsActive=1 and UniqueKey is not NULL ORDER BY TableID ;
	DECLARE @nTableID INT;
	OPEN T_Cursor;
	FETCH NEXT FROM T_Cursor INTO @nTableID;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
				DECLARE DR_Cursor CURSOR FOR
				SELECT * FROM  ( SELECT RecordID ,R.UniqueKey,DateAdded FROM [Record] R
					WHERE  IsActive=1 and  TableID=@nTableID and R.UniqueKey is not NULL 
					and R.UniqueKey not in
						(SELECT X.UniqueKey FROM 
							(SELECT COUNT(RecordID) C,UniqueKey FROM [Record] 
								WHERE  IsActive=1 and TableID=@nTableID AND UniqueKey is not NULL GROUP BY UniqueKey  ) X 
								WHERE X.C=1 ) )XX
					  ORDER BY XX.UniqueKey ASC,DateAdded DESC;
			
				DECLARE @nRecordID INT; DECLARE @nUniqueKey varchar(205);DECLARE @dtDateAdded datetime;
				DECLARE @nUniqueKeyPre varchar(205)='';
				DECLARE @nCounter2 INT=0;
				OPEN DR_Cursor
				FETCH NEXT FROM DR_Cursor INTO @nRecordID,@nUniqueKey,@dtDateAdded;
				WHILE @@FETCH_STATUS = 0
				   BEGIN	  
					  IF @nUniqueKeyPre=@nUniqueKey
						BEGIN
							SET @sRecordIDs=@sRecordIDs+ ','+ CAST(@nRecordID AS varchar(20))
							--OR
							--UPDATE [Record] SET IsActive=0 WHERE RecordID=@nRecordID
						END			

					  SET @nUniqueKeyPre=@nUniqueKey
					  FETCH NEXT FROM DR_Cursor INTO @nRecordID,@nUniqueKey,@dtDateAdded;
				   END;
				CLOSE DR_Cursor
				DEALLOCATE DR_Cursor

		  FETCH NEXT FROM T_Cursor INTO @nTableID;
	   END;
	CLOSE T_Cursor;
	DEALLOCATE T_Cursor;

	DECLARE @sDRSQL varchar(MAX);
	SET @sDRSQL='UPDATE [Record] SET IsActive=0 WHERE RecordID IN('+@sRecordIDs+');'
	PRINT @sDRSQL
	EXEC (@sDRSQL)
	
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Record_Remove_Duplicate', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
