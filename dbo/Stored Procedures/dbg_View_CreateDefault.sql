﻿


CREATE PROCEDURE   [dbo].[dbg_View_CreateDefault]
(
@ViewPageType varchar(10), 
	@TableID int,
	@ParentTableID INT=NULL,@TableChildID INT=NULL,@UserID int=NULL,
    @ViewID int OUTPUT	,@DisplayInColumnID int=NULL
)
AS

	-- [dbo].[dbg_View_CreateDefault] This create Base Views using Account Holder UserID
BEGIN TRY

 
		DECLARE @AccountID INT
		SELECT @AccountID=AccountID FROM [Table] WHERE TableID=@TableID

		IF (@UserID IS NULL)
		SELECT @UserID  =[dbo].[fnGetAccountHolderUserID] (@AccountID)	--Account Holder
   
     
		INSERT INTO [View]
		(
			TableID, 
			ViewName, 
			UserID, 
			RowsPerPage, 
			SortOrder, 
			Filter, 
			ShowSearchFields, 
			ShowAddIcon, 
			[ShowExportIcon],[ShowEmailIcon], 
			ShowDeleteIcon, 
			ShowViewIcon, 
			ShowBulkUpdateIcon,FilterControlsInfo,ViewPageType,ShowFixedHeader,ParentTableID,TableChildID,DisplayInColumnID
		) SELECT  
			TableID,
			TableName,
			@UserID,
			10,
			NULL,
			NULL,
			1,
			1,
			1,0,
			1,
			1,
			1,NULL,@ViewPageType,0 ,@ParentTableID,@TableChildID,@DisplayInColumnID
			FROM [Table] 
			WHERE TableID=@TableID

		SELECT @ViewID = @@IDENTITY 
   
		EXEC [dbo].dbg_View_CreateDefaultViewItems @ViewID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_View_CreateDefault', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
