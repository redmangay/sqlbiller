﻿
CREATE PROCEDURE [dbo].[usr_rptJobBidSucces]
(
	@From DATE = '01/10/2018',
	@To DATE = '30/10/2018',
	@Group VARCHAR(MAX) =null,
	@Controller VARCHAR(MAX) =null,
	@OrderByColumn INT
)
AS

/*

EXEC [dbo].[usr_rptJobBidSucces] @From= '01/06/2018', @To ='30/06/2018', @OrderByColumn =2 , @Group ='1', @Controller='1'


*/
BEGIN

DECLARE @SQL VARCHAR(MAX)
DECLARE @SQLGroup VARCHAR(MAX) = ''
DECLARE @SQLController VARCHAR(MAX) = ''

DECLARE @OrderBy VARCHAR(MAX) = ''

IF @OrderByColumn = 1
BEGIN
    SET @OrderBy = ' CONVERT(DATE,[Job Information].[V002],103) ASC'
END
ELSE IF @OrderByColumn = 2
BEGIN
     SET @OrderBy = ' ISNULL([Job Information].[V001],'''') ASC'
END
ELSE IF @OrderByColumn = 3
BEGIN
    SET @OrderBy = ' ROUND(ISNULL(CAST(
									   CONVERT(MONEY,[Job Information].V008) / 
									   convert(money,ISNULL([Job Information].V007,1)) * 
									   convert(money,ISNULL([Job Information].V047,1)) 
										  AS MONEY),0),2) ASC'
END

IF @Group <> 1 AND @Group is not null
BEGIN
SET @SQLGroup = ' AND [Job Information].V027 =''' + @Group + ''''
END

IF @Controller <> 1 AND @Controller is not null
BEGIN
SET @SQLController = ' AND [Job Information].V026 =''' + @Controller + ''''
END



   
SET @SQL =	'
DECLARE @OrderByColumn INT = '+ CAST(@OrderByColumn AS VARCHAR)  +'
SELECT [Job Information].[V002] AS [Job Date]
	   , [EmployeeList_Job Controller].V004 AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
	   , [Job Information].V001 AS [Job No]
	   ,ISNULL([Client].[V001],''-- NONE --'') AS [Job Client]
	   , ISNULL([Job Information].V003,''-- NONE --'') AS [Job Title]--Job Title, get from Job Information V003
	   ,Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) AS [Job Value]
	   ,CASE 
		  WHEN [Job Information].[V021] IN (''Complete and Invoiced'',''Fully Paid Up'',''Bad Debt'',''Current'') 
			 THEN ''Won''    
		  WHEN [Job Information].[V021] IN (''Lost Bid'') 
			 THEN ''Lost''
		  WHEN [Job Information].[V021] IN (''Bid'') 
			 THEN NULL 
		  END  AS [Job Status]
	   ,CASE 
		  WHEN [Job Information].[V021] IN (''Complete and Invoiced'',''Fully Paid Up'',''Bad Debt'',''Current'') 
			 THEN Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) --, Job Value from Job Information
		  ELSE 0
		  END  AS [Won Amount]
	   ,CASE 
		  WHEN [Job Information].[V021] IN (''Complete and Invoiced'',''Fully Paid Up'',''Bad Debt'',''Current'') 
		  AND Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) > 0
			 THEN 1
		  ELSE 0
		  END  AS [Won By Number]
	    ,CASE 
		  WHEN [Job Information].[V021] IN (''Lost Bid'')  
			 THEN Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) --, Job Value from Job Information
		  ELSE 0
		  END  AS [Lost Amount]
	   ,CASE 
		  WHEN [Job Information].[V021] IN (''Lost Bid'')  
		    AND Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) > 0
			 THEN 1
		  ELSE 0
		  END  AS [Lost By Number]
	   ,CASE 
		  WHEN [Job Information].[V021] IN (''Bid'') 
			 THEN Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) --, Job Value from Job Information
		  ELSE 0
		  END  AS [Pending Amount]
	   ,CASE 
		  WHEN [Job Information].[V021] IN (''Bid'')  
		    AND Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) > 0
			 THEN 1
		  ELSE 0
		  END  AS [Pending By Number]
	   ,CASE WHEN Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) > 0 THEN 1 ELSE 0 END AS [Job By Number]
	   ,[Reporting Currency].V001 AS [Currency]	
	   ,[Job Group].[V001] AS [Group Name]	
	   	, 	(SELECT V039  FROM RECORD WHERE TABLEID = 2743) AS [Header] 
				  
			FROM [Record] [Job Information] WITH (NOLOCK) 

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		CAST([EmployeeList_Job Controller].RecordID AS VARCHAR(MAX)) = [Job Information].V026 		/*	Job Number	*/

			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		CAST([Currency Rates].RecordID AS VARCHAR(MAX)) = [Job Information].V034 		/*	Job Number	*/

			LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		CAST([Tax Name].RecordID AS VARCHAR(MAX)) = [Job Information].V042 		/*	Job Number	*/
			 
			LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
				AND			[Job Group].IsActive = 1
				AND		CAST( [Job Group].RecordID AS VARCHAR(MAX)) = [Job Information].V027		/*	Currency	*/

			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/
			
			 LEFT JOIN [Record] [Reporting Currency] WITH (NOLOCK) ON [Reporting Currency].TableID = 2665
				AND		[Reporting Currency].IsActive = 1
				AND		CAST([Reporting Currency].RecordID AS VARCHAR(MAX)) = [Job Group].V005 		/*	Job Number	*/
			
			 LEFT JOIN Record [Client] WITH (NOLOCK) ON [Client].TableID = 2474		/*	Currency Rates	*/
				AND			[Client].IsActive = 1
				AND	CAST([Client].RecordID AS VARCHAR(MAX)) = [Job Information].V024		/*	[Group Currency]	*/

					WHERE [Job Information].TableID = 2475 AND
					[Job Information].IsActive = 1 
					' + @SQLGroup + '
					' + @SQLController + '
					AND CONVERT(DATE,[Job Information].V002) >= CAST('''+ CAST(@From AS VARCHAR) +''' AS DATE)  AND
					CONVERT(DATE,[Job Information].V002) <= CAST('''+ CAST(@To AS VARCHAR) + ''' AS DATE)
					
					ORDER BY ' + @OrderBy
				/*	ORDER BY CASE @OrderByColumn
							 WHEN 1 THEN CONVERT(DATE,[Job Information].[V002],103)
							 WHEN 2 THEN ISNULL([Job Information].[V001],'''') 
								END DESC, ROUND(ISNULL(CAST(
									   CONVERT(MONEY,[Job Information].V008) / 
									   convert(money,[Job Information].V007) * 
									   convert(money,[Group Currency Rates].V002) 
										  AS MONEY),0),2) DESC--, Job Value from Job Information' */


EXEC(@SQL);
PRINT @SQL


/*
--DECLARE @OrderByColumn INT = 2
SELECT [Job Information].[V002] AS [Job Date]
	   , [EmployeeList_Job Controller].V004 AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
	   , [Job Information].V001 AS [Job No]
	   ,ISNULL([Client].[V001],'-- NONE --') AS [Job Client]
	   , ISNULL([Job Information].V003,'-- NONE --') AS [Job Title]--Job Title, get from Job Information V003
	   ,Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,[Job Information].V007) * convert(money,[Group Currency Rates].V002) AS MONEY),0),2) AS [Job Value]
	   ,CASE 
		  WHEN [Job Information].[V021] IN ('Complete and Invoiced','Fully Paid Up','Bad Debt','Current') 
			 THEN 'Won'    
		  WHEN [Job Information].[V021] IN ('Lost Bid') 
			 THEN 'Lost'
		  WHEN [Job Information].[V021] IN ('Bid') 
			 THEN NULL 
		  END  AS [Job Status]
	   ,CASE 
		  WHEN [Job Information].[V021] IN ('Complete and Invoiced','Fully Paid Up','Bad Debt','Current') 
			 THEN Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,[Job Information].V007) * convert(money,[Group Currency Rates].V002) AS MONEY),0),2) --, Job Value from Job Information
		  ELSE 0
		  END  AS [Won Amount]
	   ,CASE 
		  WHEN [Job Information].[V021] IN ('Complete and Invoiced','Fully Paid Up','Bad Debt','Current') 
			 THEN 1
		  ELSE 0
		  END  AS [Won By Number]
	    ,CASE 
		  WHEN [Job Information].[V021] IN ('Lost Bid')  
			 THEN Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,[Job Information].V007) * convert(money,[Group Currency Rates].V002) AS MONEY),0),2) --, Job Value from Job Information
		  ELSE 0
		  END  AS [Lost Amount]
	   ,CASE 
		  WHEN [Job Information].[V021] IN ('Lost Bid')  
			 THEN 1
		  ELSE 0
		  END  AS [Lost By Number]
	   ,CASE 
		  WHEN [Job Information].[V021] IN ('Bid') 
			 THEN Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,[Job Information].V007) * convert(money,[Group Currency Rates].V002) AS MONEY),0),2) --, Job Value from Job Information
		  ELSE 0
		  END  AS [Pending Amount]
	   ,CASE 
		  WHEN [Job Information].[V021] IN ('Bid')  
			 THEN 1
		  ELSE 0
		  END  AS [Pending By Number]
	   ,1 AS [Job By Number]
	   ,[Reporting Currency].V001 AS [Currency]	
	   ,[Job Group].[V001] AS [Group Name]	
	   	, 	(SELECT V039  FROM RECORD WHERE TABLEID = 2743) AS [Header] 
				  
			FROM [Record] [Job Information] WITH (NOLOCK) 

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		CAST([EmployeeList_Job Controller].RecordID AS VARCHAR(MAX)) = [Job Information].V026 		/*	Job Number	*/

			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		CAST([Currency Rates].RecordID AS VARCHAR(MAX)) = [Job Information].V034 		/*	Job Number	*/

			LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		CAST([Tax Name].RecordID AS VARCHAR(MAX)) = [Job Information].V042 		/*	Job Number	*/
			 
			LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
				AND			[Job Group].IsActive = 1
				AND		CAST( [Job Group].RecordID AS VARCHAR(MAX)) = [Job Information].V027		/*	Currency	*/

			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/
			
			 LEFT JOIN [Record] [Reporting Currency] WITH (NOLOCK) ON [Reporting Currency].TableID = 2665
				AND		[Reporting Currency].IsActive = 1
				AND		CAST([Reporting Currency].RecordID AS VARCHAR(MAX)) = [Job Group].V005 		/*	Job Number	*/
			
			 LEFT JOIN Record [Client] WITH (NOLOCK) ON [Client].TableID = 2474		/*	Currency Rates	*/
				AND			[Client].IsActive = 1
				AND	CAST([Client].RecordID AS VARCHAR(MAX)) = [Job Information].V024		/*	[Group Currency]	*/

					WHERE [Job Information].TableID = 2475 AND
					[Job Information].IsActive = 1 
					
					-- AND [Job Information].V026 ='1004044'
					AND CONVERT(DATE,[Job Information].V002,103) >= CAST(@From AS DATE)  AND
					CONVERT(DATE,[Job Information].V002,103) <= CAST(@To AS DATE)
					
					ORDER BY CASE @OrderByColumn
							 WHEN 1 THEN [Job Information].[V002] 
							 WHEN 2 THEN [Job Information].[V001] 
								END DESC, ROUND(ISNULL(CAST(
									   CONVERT(MONEY,[Job Information].V008) / 
									   convert(money,[Job Information].V007) * 
									   convert(money,[Group Currency Rates].V002) 
										  AS MONEY),0),2) DESC--, Job Value from Job Information
										  */

END
