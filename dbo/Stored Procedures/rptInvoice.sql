﻿CREATE PROCEDURE [dbo].[rptInvoice]
	@StartDate DateTime= NULL,
	@EndDate DateTime=NULL,
	@IncludeBadDebts Bit =0
AS
BEGIN
SET NOCOUNT ON;
IF OBJECT_ID('tempdb..#tempTableForDateRange') IS NOT NULL
begin
        drop table #tempTableForDateRange
end

IF OBJECT_ID('tempdb..#tempTableIncludeDebtsForDateRange') IS NOT NULL
begin
        drop table #tempTableIncludeDebtsForDateRange
end

IF @IncludeBadDebts=0
	BEGIN
	SELECT b.[Bad Debt Exp Converted],b.[Bad Debts Recov],b.[Bad Debts Recov Converted],b.[Bad Dept Exp],b.Billings,b.[Created Date Time]
	,b.DateAdded,b.Debtors,b.[Debtors Converted],b.Description,b.[Group Currency],b.ID,b.[Invoice Amount],b.[Invoice Amount Converted],b.[Invoice No],b.[Is Exported],b.[Negate Billings],b.[Payment Amount]
	,b.[Payment Amount Converted],b.[Project Number],b.[Record ID],b.[Rpt Debtors Converted],b.TableID
	,b.[Tax Amount],b.[Tax Name],b.[Tax Rate],b.Type,CONVERT(Datetime, b.[Date], 103)  as [Date],Account24918.[vProject].[Status] AS ProjectStatus,
	Account24918.[vProject].[DetailsAttached],
	isnull(Account24918.[vProject].[Company],'') + CHAR(10) + LTRIM(RTRIM(isnull(Account24918.[vProject].[Address1],'')))+ CHAR(10) +
	LTRIM(RTRIM(isnull(Account24918.[vProject].[Address2],''))) + CHAR(10) +Account24918.[vProject].[Suburb] + CHAR(10) +
	isnull(Account24918.[vProject].[State],'') + CHAR(10) +  isnull(Account24918.[vProject].[PostCode],'') + CHAR(10) +
	isnull(Account24918.[vProject].[Country],'') AS CompanyAddress,
	
	('Attention: '+ Account24918.[vProject].[Name]+' '+	Account24918.[vProject].[SumName]) AS Attention,
	
	Account24918.[vProject].[ProjectTitle] AS ProjectTitle,
	
	Account24918.[vProject].[Value] AS Value,Account24918.[vProject].[FixedPrice] AS FixedPrice,
	isnull(Account24918.[vProject].[Currency],'') As CurrencyType,
	
	(CASE WHEN b.[Tax Name] is not null 
	THEN ('Job Value including [' + b.[Tax Name]+']: '+Account24918.[vProject].[Value])
	ELSE
	'Job Value: '+Account24918.[vProject].[Value]
	end ) AS JobValueDescription
	,(Select Symbol from Account24918.[vCurrency Rates] where Currency=IsNull(Account24918.[vProject].[Currency],''))AS CurrencySymbol

	into #tempTableForDateRange
	
    FROM 
    Account24918.[vBilling Information] b LEFT JOIN Account24918.[vProject] ON b.[Project Number] = Account24918.[vProject].ProjectNumber
    WHERE 
    (
    b.[Type] in (1, 4)
    )   order by b.[Date]
    
    Select * from #tempTableForDateRange
	Where (Date BETWEEN @StartDate And @EndDate AND ProjectStatus <>  6) ;
	RETURN;
	
	END
	
	IF @IncludeBadDebts=1
	BEGIN 
	SELECT b.*,Account24918.[vProject].[Status] AS ProjectStatus,
	
	isnull(Account24918.[vProject].[Company],'') + CHAR(10) + LTRIM(RTRIM(isnull(Account24918.[vProject].[Address1],'')))+ CHAR(10) +
	LTRIM(RTRIM(isnull(Account24918.[vProject].[Address2],''))) + CHAR(10) +Account24918.[vProject].[Suburb] + CHAR(10) +
	isnull(Account24918.[vProject].[State],'') + CHAR(10) +  isnull(Account24918.[vProject].[PostCode],'') + CHAR(10) +
	isnull(Account24918.[vProject].[Country],'') AS CompanyAddress,
	
	('Attention: '+ Account24918.[vProject].[Name]+' '+	Account24918.[vProject].[SumName]) AS Attention,
	
	Account24918.[vProject].[ProjectTitle] AS ProjectTitle,
	
	Account24918.[vProject].[Value] AS Value,Account24918.[vProject].[FixedPrice] AS FixedPrice,
	isnull(Account24918.[vProject].[Currency],'') As CurrencyType,
	
	(CASE WHEN b.[Tax Name] is not null 
	THEN ('Job Value including [' + b.[Tax Name]+']: '+Account24918.[vProject].[Value])
	ELSE
	'Job Value: '+Account24918.[vProject].[Value]
	end ) AS JobValueDescription,(Select Symbol from Account24918.[vCurrency Rates] where Currency=Isnull(Account24918.[vProject].[Currency],''))AS CurrencySymbol
	
	into #tempTableIncludeDebtsForDateRange
	
    FROM 
    Account24918.[vBilling Information] b LEFT JOIN Account24918.[vProject] ON b.[Project Number] = Account24918.[vProject].ProjectNumber
    WHERE 
    (
    b.[Type] in (1, 4)
    )  order by b.[Date]
    
    Select * from #tempTableIncludeDebtsForDateRange
	Where (Date BETWEEN @StartDate And @EndDate) ;
	RETURN;
	
	END

END