﻿CREATE PROCEDURE [dbo].[Renzo_PrintOverPaid]
(
	@RecordID  int   		
	
)
AS

--DECLARE @RecordIDx int =  12943690
 ----   exec [dbo].[Renzo_PrintOverPaid]   12986340

 DECLARE @NonTaxRecordID int

 SELECT @NonTaxRecordID = RecordID FROM Record
 WHERE TABLEID = 2696
 AND ISACTIVE = 1
 AND V001 LIKE '%None%'

 SET DATEFORMAT dmy
 SELECT 
		cast([Bill & Payment Activity].[V004] as date) AS [Date]
		,[Job Information].[V001] AS [Job Number]
		,[Bill & Payment Activity].[V017] AS [Refund No]
		,[Client Information].[V017] AS [Client Name]
		,[Client Information].[V002] AS [Client Address1]
		,isnull([Client Information].[V015],'') AS [Client Address2]
		,[Client Information].[V003] AS [Client Suburb]
		,ISNULL([Client Information].[V004],'') + ' ' + ISNULL([Client Information].[V005],'') AS [Client State]
		,[Client Information].[V006] AS [Country]
		,[Job Information].[V003] AS [Job Title]
		,cast(isnull([Bill & Payment Activity].[V007],0) as money) AS [Payment Amount]
		,cast(isnull([Job Information].[V008],0) as money) AS [Job Value]
		,[Job Currency].[V001] AS [Job Currency]
		--,cast(ISNULL([Previous Bill & Payment Activity].[V005],0.00) as money) AS [Previous Invoice Amount]
		--(SELECT ISNULL(SUM(CAST(ISNULL(V005,0) AS MONEY)),0) 
		--	 FROM RECORD [Previous Bill & Payment Activity] 
		--		WHERE [Previous Bill & Payment Activity].TableID = 2711 AND
		--		[Previous Bill & Payment Activity].IsActive = 1 AND  
		--		[Previous Bill & Payment Activity].V002 = CAST([Job Information].[RecordID] AS VARCHAR) AND
		--		[Previous Bill & Payment Activity].V029 IN ('Invoice','Interim Invoice','Credit Entry','Interim Credit') AND
		--		--[Previous Bill & Payment Activity].RecordID < @RecordID) AS [Previous Invoice Amount]
		--		CAST([Previous Bill & Payment Activity].V004 AS DATE) < CAST([Bill & Payment Activity].[V004] AS DATE)) AS [Previous Invoice Amount]
		--,cast(isnull([Bill & Payment Activity].[V063],0) as money) AS [GST]
		--,cast(isnull([Bill & Payment Activity].[V005],0) as money) / ( 1 + CAST(ISNULL( [Job Information].V011,0.00)  AS MONEY)) AS [GST]
		,[Document Layout].V019 AS Notes
		,[Bill & Payment Activity].[V015] AS [Description],

		/* =========== */
		--ISNULL([Client Contact].[V004],'') + ' ' + ISNULL([Client Contact].[V005],'') + ' ' + ISNULL([Client Contact].[V006],'')   AS Attention,
		[Client Information].[V019] AS Attention,
		  --CASE WHEN [Job Information].[V009] = 'FALSE' THEN 'Estimate' ELSE '' END AS [Estimate],
		  --CASE WHEN [Job Information].[V009] = 'TRUE' THEN '% Complete' ELSE '' END AS [% Complete],
		   --CASE WHEN [Job Information].[V009] = 'TRUE' THEN convert(money,ISNULL([Bill & Payment Activity].V028,0.00)) / cast('100.00' as money) ELSE '' END AS [% Complete Value],
		  --ISNULL([Tax Table].[V001],'''') AS [Tax Name],
			 (SELECT OptionValue FROM SYSTEMOPTION WHERE OPTIONKEY = 'FilesLocation') + '/UserFiles/AppFiles/' + [Document Layout].V027  AS HeaderImage
		 --[Bill & Payment Activity].V028 

		 --,[Job Information].RecordID,[Previous Bill & Payment Activity].RECORDID




 FROM [Record] [Bill & Payment Activity] WITH (NOLOCK) 
 JOIN [Record] [Job Information] ON [Job Information].[TableID] = 2475
			   AND CAST([Job Information].[RecordID] AS VARCHAR)= [Bill & Payment Activity].[V002]
			   AND [Job Information].[IsActive] = 1

			   
 --LEFT JOIN [Record] [Office Information] ON [Office Information].[TableID] = 2474
	--		   AND CAST([Office Information].[RecordID] AS VARCHAR) = [Job Information].[V024] 
	--		   AND [Office Information].[IsActive] = 1

 LEFT JOIN [Record] [Client Information] ON [Client Information].[TableID] = 2476
			   AND [Client Information].[V013] = CAST([Job Information].[RecordID] AS VARCHAR)
			   AND [Client Information].[IsActive] = 1

LEFT JOIN [Record] [Client Contact] ON [Client Contact].[TableID] = 2657
			   AND CAST([Client Contact].RecordID AS varchar) = [Client Information].[V008]
			   AND [Client Contact].[IsActive] = 1
			   
 LEFT JOIN [Record] [Job Currency] ON [Job Currency].[TableID] = 2665
			   AND CAST([Job Currency].[RecordID] AS VARCHAR)= [Job Information].[V034] 
			   AND [Job Currency].[IsActive] = 1

 LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
    			  AND			[Job Group].IsActive = 1
			  AND		CAST( [Job Group].RecordID AS VARCHAR(MAX)) = [Job Information].V027		/*	Group	*/

 LEFT JOIN Record [Document Layout] WITH (NOLOCK) ON [Document Layout].TableID = 2744	/*	Document Layout	*/
    AND			[Document Layout].IsActive = 1
    AND		[Document Layout].V005 = CAST([Job Group].RecordID AS VARCHAR)		/*	Group	*/

LEFT JOIN	    [Record] [Tax Table]	ON				1=1
										AND		CAST([Tax Table].[RecordID] AS VARCHAR) = [Job Information].V042	
										AND		[Tax Table].TableID = 2696		/*	Tax Rates	*/
										AND		[Tax Table].IsActive = 1

			   
 --LEFT JOIN [Record] [Previous Bill & Payment Activity] ON [Previous Bill & Payment Activity].[TableID] = 2711 
	--			AND [Previous Bill & Payment Activity].RecordID = 
	--			(SELECT TOP 1 RecordID 
	--				FROM [Record] 
	--					WHERE V002 = CAST([Job Information].[RecordID] AS VARCHAR) AND 
	--						RecordID < @RecordID AND [Previous Bill & Payment Activity].V029 LIKE '%Invoice%' AND
	--						ISACTIVE = 1
	--							ORDER BY [RecordID] DESC )

								
 WHERE [Bill & Payment Activity].[TableID] = 2711	AND
	   [Bill & Payment Activity].[IsActive] = 1		AND
	   [Bill & Payment Activity].[RecordID] = @RecordID

RETURN @@ERROR;