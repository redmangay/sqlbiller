﻿
-- Author:		<Manoj sharma>
-- Create date: <13 March 2016>
-- Description:	<Used as ssrs report and in procedure rptempchargeability>
CREATE procedure [dbo].[usr_rpt_EmployeeHistroy]

as
BEGIN

IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##EmployeeHistroy' )) DROP TABLE ##EmployeeHistroy
create table ##EmployeeHistroy  (
MaxOfDate varchar(max),
EmployeeID varchar(max)

 ) 

 

 insert into ##EmployeeHistroy


   SELECT Max(EmployeeHistroy.Date) AS MaxOfDate, EmployeeHistroy.EmployeeID
FROM  Account24918.vEmployeeHistroy EmployeeHistroy
GROUP BY EmployeeHistroy.EmployeeID;
  
   
END;
