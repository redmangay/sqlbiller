﻿
 
CREATE PROCEDURE [dbo].[ets_DashBorad_Alert]
(
	@nAccountID int ,
	@dLastAlertDate datetime
)
	/*----------------------------------------------------------------
	EXEC [ets_DashBorad_Alert] 13
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SELECT     ST.TableID,ST.TableName, 
		CAST (CONVERT(varchar, COUNT(*)) AS varchar(10)) + ' Warning(s)'  WarningCount
		FROM         Record INNER JOIN
		[Table] ST ON Record.TableID = ST.TableID           
		WHERE     (Record.WarningResults IS NOT NULL AND Record.IsActive=1  
		AND ST.IsActive=1)  
		 AND ST.AccountID = @nAccountID 
		 AND Record.DateAdded>@dLastAlertDate
		GROUP BY ST.TableID,ST.TableName
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_DashBorad_Alert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
