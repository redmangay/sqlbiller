﻿

CREATE PROCEDURE   [dbo].[dbg_View_BestFittingNew]
(
@ViewPageType varchar(10), 
	@UserID INT, 
	@TableID int,
	@ParentTableID INT=NULL,@TableChildID INT=NULL,
	@ViewID int OUTPUT,@DisplayInColumnID int=NULL
)
AS

-- Client SPs: [dbo].[dbg_View_Reset]

BEGIN TRY

			DECLARE @AccountID INT
			SELECT @AccountID=AccountID FROM [Table] WHERE TableID=@TableID



     		-- Try Their Own Personal View
			IF @ViewID IS NULL
				BEGIN
						SELECT TOP 1 @ViewID = ViewID 
						FROM [View] V
					WHERE TableID = @TableID and ViewPageType=@ViewPageType
					AND UserID = @UserID   
						AND ((V.ParentTableID IS NULL AND @ParentTableID IS NULL) OR V.ParentTableID=@ParentTableID)
						AND ((V.TableChildID IS NULL AND @TableChildID IS NULL) OR V.TableChildID=@TableChildID)	
						AND ((V.DisplayInColumnID IS NULL AND @DisplayInColumnID IS NULL) OR V.DisplayInColumnID=@DisplayInColumnID)		
					ORDER BY V.[ViewID] DESC  --latest view
				END
			
		
			-- IF that failed then try for the view saved against their role
			IF @ViewID IS NULL
				BEGIN
					PRINT 'Attempting Role View...'
					DECLARE @RoleID INT
					DECLARE @IsSystemRole bit
					DECLARE @Role_ViewsDefaultFromUserID INT
					--DECLARE @
					SELECT TOP 1 @RoleID=UR.RoleID,@IsSystemRole=R.IsSystemRole,@Role_ViewsDefaultFromUserID=R.ViewsDefaultFromUserID FROM [User] U INNER JOIN [UserRole] UR 
						ON U.UserID=UR.UserID  INNER JOIN [Role] R 
						ON UR.RoleID=R.RoleID WHERE U.UserID=@UserID AND UR.AccountID=@AccountID
					
					IF @IsSystemRole IS NULL OR @IsSystemRole=0
						BEGIN
							SELECT TOP 1 @ViewID = V.ViewID 
							FROM [View] V 
							JOIN [RoleTable] RT ON V.[UserID] = RT.[ViewsDefaultFromUserID] 
							WHERE RT.TableID = @TableID AND RT.RoleID=@RoleID AND V.TableID=@TableID
							AND ((V.ParentTableID IS NULL AND @ParentTableID IS NULL) OR V.ParentTableID=@ParentTableID)
								AND ((V.TableChildID IS NULL AND @TableChildID IS NULL) OR V.TableChildID=@TableChildID)	
								AND ((V.DisplayInColumnID IS NULL AND @DisplayInColumnID IS NULL) OR V.DisplayInColumnID=@DisplayInColumnID)						
							AND V.ViewPageType = @ViewPageType
							ORDER BY V.[ViewID] DESC
						END
					ELSE
						BEGIN
							IF @Role_ViewsDefaultFromUserID IS NOT NULL
								BEGIN
									SELECT TOP 1 @ViewID = V.ViewID 
									FROM [View] V 
									WHERE V.TableID = @TableID AND V.UserID=@Role_ViewsDefaultFromUserID
									AND ((V.ParentTableID IS NULL AND @ParentTableID IS NULL) OR V.ParentTableID=@ParentTableID)
									AND ((V.TableChildID IS NULL AND @TableChildID IS NULL) OR V.TableChildID=@TableChildID)
									AND ((V.DisplayInColumnID IS NULL AND @DisplayInColumnID IS NULL) OR V.DisplayInColumnID=@DisplayInColumnID)
									AND V.ViewPageType = @ViewPageType
									ORDER BY V.[ViewID] DESC
								END								
						END
					
				END


				-- IF that failed then try for the view saved against the Account Holder
			IF @ViewID IS NULL
				BEGIN
					PRINT 'Attempting Base View...'
					DECLARE @AccUserID INT =[dbo].[fnGetAccountHolderUserID] (@AccountID)	--Account Holder

					SELECT TOP 1 @ViewID = [View].ViewID 
						FROM [View]  
						WHERE UserID = @AccUserID
						AND TableID = @TableID
						AND ViewPageType = @ViewPageType
						AND ((ParentTableID IS NULL AND @ParentTableID IS NULL) OR ParentTableID=@ParentTableID)
						AND ((TableChildID IS NULL AND @TableChildID IS NULL) OR TableChildID=@TableChildID)
						AND ((DisplayInColumnID IS NULL AND @DisplayInColumnID IS NULL) OR DisplayInColumnID=@DisplayInColumnID)
						ORDER BY [ViewID] DESC
				END

			IF @ViewID IS NULL
				BEGIN
						-- The Account Holder will have this situation at the very first too
						-- Create base view from Column Table
						DECLARE @DefaultViewID INT
					
						EXEC [dbo].[dbg_View_CreateDefault] @ViewPageType,@TableID,@ParentTableID,@TableChildID,@UserID,@DefaultViewID OUT,@DisplayInColumnID
					
						SELECT @ViewID=@DefaultViewID
				END
							
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_View_BestFittingNew', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
