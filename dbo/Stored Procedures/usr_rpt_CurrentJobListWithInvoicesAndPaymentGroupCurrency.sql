﻿
-- Author:		<Manoj sharma>
-- Create date: <12 March 2016>
-- Description:	<Used as ssrs report and in procedure rptcurrentjoblist>
create procedure [dbo].[usr_rpt_CurrentJobListWithInvoicesAndPaymentGroupCurrency]
as


BEGIN
    
	IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##CurrentJobListWithInvoicesAndPaymentGroupCurrency' )) DROP TABLE ##CurrentJobListWithInvoicesAndPaymentGroupCurrency
	
create table  ##CurrentJobListWithInvoicesAndPaymentGroupCurrency  ( ProjectNumber varchar(max), Invoiced float, Payment float, SumOfInvoiceAmount_Converted float, SumOfPaymentAmount_Converted float ) 

	 
 insert into ##CurrentJobListWithInvoicesAndPaymentGroupCurrency
SELECT BillingInformation.[Project Number] as ProjectNumber ,
 Sum(Round( dbo.ConvertToGroupCurrency(CAST(REPLACE(REPLACE(isnull([Invoice Amount],0),',',''),'$','') AS float),CAST(REPLACE(REPLACE(isnull([CurrencyRate],0),',',''),'$','') AS float),CAST(REPLACE(REPLACE(isnull([RptCurrencyRate],0),',',''),'$','') AS float)),2)) AS Invoiced,
 
 Sum(Round( dbo.ConvertToGroupCurrency(CAST(REPLACE(REPLACE(isnull([Payment Amount],0),',',''),'$','') AS float),CAST(REPLACE(REPLACE(isnull([CurrencyRate],0),',',''),'$','') AS float),CAST(REPLACE(REPLACE(isnull([RptCurrencyRate],0),',',''),'$','') AS float)),2)) AS Payment,
 
  Sum(CAST(REPLACE(REPLACE(isnull(BillingInformation.[Invoice Amount Converted],0),',',''),'$','') AS float)) AS SumOfInvoiceAmount_Converted,
  Sum(CAST(REPLACE(REPLACE(isnull(BillingInformation.[Payment Amount Converted],0),',',''),'$','') AS float)) AS SumOfPaymentAmount_Converted
  
FROM [Account24918].vProject Project INNER JOIN  [Account24918].[vBilling Information] BillingInformation ON Project.ProjectNumber = BillingInformation.[Project Number]
GROUP BY BillingInformation.[Project Number];


   
  
  
END;
