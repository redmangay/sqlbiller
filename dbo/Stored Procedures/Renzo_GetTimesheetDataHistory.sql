﻿

CREATE PROCEDURE [dbo].[Renzo_GetTimesheetDataHistory]
(
	@RecordID int 
)
AS
/*

EXEC [Renzo_GetTimesheetDataHistory]   15059971


*/

BEGIN

DECLARE @TimesheetRecordID int = NULL
DECLARE @DisbursementRecordID int =  null
DECLARE @CurrencyRate VARCHAR(MAX);

SELECT @CurrencyRate = ISNULL(V007,1) 
FROM [Record]
WHERE [RecordID] = ( SELECT [V001] FROM [Record] WHERE [RecordID] = @RecordID );

SELECT @TimesheetRecordID = V011, @DisbursementRecordID = V010 FROM Record WHERE RecordID = @RecordID
	--SELECT @TimesheetRecordID
	--SELECT @CurrencyRate
	IF (@TimesheetRecordID IS NOT NULL)
	BEGIN
		/* Original Value - Should not be converted */ SELECT ISNULL(V007,0.00) AS MultiplierX, ISNULL(V015,0.00) AS Hour, ISNULL(V009,0.00) AS Rate, ISNULL(Round(ISNULL(CONVERT(money,[V019]),0.00) / @CurrencyRate ,2),0.00) AS Total FROM RECORD  WHERE RecordID=@TimesheetRecordID AND ISACTIVE = 1
		UNION ALL
		/* Adjusted Value - Should not be converted */ SELECT ISNULL(V004,0.00) AS MultiplierX, ISNULL(V005,0.00) AS Hour, ISNULL(Round(ISNULL(CONVERT(MONEY,[V006]),0.00) / @CurrencyRate,2),0.00) AS Rate, ISNULL(Round(ISNULL(CONVERT(MONEY,[V007]),0.00) / @CurrencyRate,2),0.00) AS Total FROM RECORD  WHERE RecordID=@RecordID
     END
	ELSE IF (@DisbursementRecordID IS NOT NULL)
	BEGIN
		/* Original Value - Should not be converted */ SELECT 0 AS MultiplierX, ISNULL(V004,0.00) AS Hour, ISNULL(V005,0.00) AS Rate,ISNULL(V007,0.00) AS Total FROM RECORD  WHERE RecordID=@DisbursementRecordID
		UNION ALL
		/* Adjusted Value - Should not be converted */ SELECT ISNULL(V004,0) AS MultiplierX, ISNULL(V005,0.00) AS Hour, ISNULL(Round(ISNULL(CONVERT(MONEY,[V006]),0) / @CurrencyRate,2),0) AS Rate, ISNULL(Round(ISNULL(cast([V007] AS DEC(10,2)),0.00) / cast(@CurrencyRate AS DEC(10,2)),2),0.00) AS Total FROM RECORD  WHERE RecordID=@RecordID
	   
	END

--SELECT 'GST' AS Label, V023 AS Amount FROM RECORD
--WHERE RECORDID = @RecordID
--union all
--SELECT 'Total' AS Label, V023 AS Amount FROM RECORD
--WHERE RECORDID = @RecordID


END

