﻿

CREATE PROCEDURE [dbo].[AccountInvite_Detail]
(
	@nAccountInviteID int 
)
	/*----------------------------------------------------------------
	-- Stored Procedure: AccountInvite_Delete
	-- Generated by: gpGenerateSP (Jon Bosker, DB Gurus Australia) 
	-- Date: Nov 30 2017 11:51AM
	---------------------------------------------------------------*/
AS

BEGIN TRY

	SELECT * 
		FROM AccountInvite
		WHERE AccountInviteID = @nAccountInviteID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('AccountInvite_Detail', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
