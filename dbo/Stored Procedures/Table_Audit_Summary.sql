﻿

CREATE PROCEDURE [dbo].[Table_Audit_Summary]
(
      @nTableID int,
      @nStartRow int = 1, 
	  @nMaxRows int = 20000000 
)
AS
BEGIN TRY
	--EXEC Table_Audit_Summary @nTableID=1133,@nStartRow=1,@nMaxRows=5
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	--SET ROWCOUNT @nMaxRows

     SELECT * FROM (
			SELECT [Audit].TableName, 
	   			   [Audit].DateAdded,
				   [User].FirstName + ' ' + [User].LastName as [User],
				   dbo.fnListChangesST(@nTableID, [Audit].DateAdded) as ColumnList,
				   ROW_NUMBER() OVER(ORDER BY [Audit].DateAdded DESC ) as RowNum 
            FROM [Audit] JOIN
            [USER] ON [User].UserID =[Audit].UserID
            WHERE FieldName != 'DateUpdated' 
				  AND FieldName != 'LastUpdatedUserID'
				  AND (TableName = 'Table' OR TableName = 'Column')
            	  AND TableId = @nTableID
				--AND PrimaryKeyValue = @nTableID 
            GROUP BY [Audit].TableName,
					 [Audit].DateAdded,
					 [User].FirstName + ' ' + [User].LastName,
					 dbo.fnListChangesST(@nTableID, [Audit].DateAdded)
            ) AS AuditInfo
      WHERE RowNum>=@nStartRow AND RowNum<=(@nStartRow+@nMaxRows) 
                  
            
            --now we need to get TotalRows number
            
      SELECT MAX(RowNum) AS TotalRows FROM ( 
			SELECT [Audit].DateAdded,
				[User].FirstName + ' ' + [User].LastName as [User],
				dbo.fnListChangesST(@nTableID, 
				[Audit].DateAdded) as Change,
				ROW_NUMBER() OVER(ORDER BY [Audit].DateAdded DESC ) as RowNum 
			FROM [Audit]
			JOIN [User] ON [User].UserID=[Audit].UserID
            WHERE FieldName != 'DateUpdated' 
				  AND FieldName != 'LastUpdatedUserID'
				  AND (TableName = 'Table' OR TableName = 'Column')
            	  AND TableId = @nTableID
				--AND PrimaryKeyValue = @nTableID 
            GROUP BY [Audit].TableName,
					 [Audit].DateAdded,
					 [User].FirstName + ' ' + [User].LastName,
					 dbo.fnListChangesST(@nTableID, [Audit].DateAdded)
            ) AS AuditInfo
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog]([Module], [ErrorMessage], [ErrorTrack], [ErrorTime], [Path]) 
		VALUES ('Table_Audit_Summary', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
