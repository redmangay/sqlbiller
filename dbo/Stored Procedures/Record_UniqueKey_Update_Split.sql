﻿

 
CREATE PROCEDURE   [dbo].[Record_UniqueKey_Update_Split]
(@nS int,@nE int)
AS
 
 BEGIN
   -- 'datetime','time' columns data has been updated
   -- exec [dbo].[Record_UniqueKey_Update_Split] 1,2
	DECLARE T_Cursor CURSOR FOR
	SELECT TableID FROM zCleanData WHERE  HasDuplicate=1 AND ( SN>=@nS and SN<=@nE)

	DECLARE @nTableID INT;
	OPEN T_Cursor;
	FETCH NEXT FROM T_Cursor INTO @nTableID;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
			 
			  IF EXISTS(SELECT TOP 1 RecordID FROM [Record] WHERE TableID = @nTableID and IsActive=1)
				begin
					EXEC [Record_Set_UniqueKey] @sUpdateRecord='R', @nTableID=@nTableID
					PRINT ' UniqueKey has been updated for TableID=' + CAST(@nTableID AS VARCHAR(10))
				end
					
	   		  
			  FETCH NEXT FROM T_Cursor INTO @nTableID;
	   END;
	CLOSE T_Cursor;
	DEALLOCATE T_Cursor;
 END



