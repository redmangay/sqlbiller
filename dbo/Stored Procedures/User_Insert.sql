﻿
CREATE PROCEDURE [dbo].[User_Insert]
(
	@nNewID int output,
	@sFirstName nvarchar(50)	= NULL,
	@sLastName nvarchar(50)		= NULL,
	@sPhoneNumber varchar(20)	= NULL,
	@sEmail nvarchar(200),
	@bIsActive bit				= 1,
	@sPassword nvarchar(200)	= NULL,
	@sProfilePicture varchar(MAX)=NULL,
	@sAttributes varchar(max) =null
)
/*----------------------------------------------------------------
REVISION HISTORY
----------------
07 Feb 2017 JB Checks to ensure user is not in current account and also links accounts

---------------------------------------------------------------*/
 
AS
-- First check IF user exists in current account
BEGIN TRY
	IF EXISTS(SELECT 1	
				FROM [User] U 
				WHERE U.Email = @sEmail)
	BEGIN
		SELECT @nNewID = [UserID] FROM [User] U 
				WHERE U.Email = @sEmail
	END
	ELSE
	BEGIN 
		INSERT INTO [User]
		(
			FirstName, 
			LastName, 
			PhoneNumber ,
			Email, 
			IsActive,
			ProfilePicture,
			Attributes
		) 
		VALUES 
		(
			@sFirstName,
			@sLastName,
			@sPhoneNumber,
			@sEmail,
			@bIsActive,
			@sProfilePicture,@sAttributes
		)
		SELECT @nNewID = @@IDENTITY

		IF @sPassword IS NOT NULL
			EXEC User_UpdatePassword @nUserID=@nNewID, @sPassword=@sPassword

	END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('User_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
