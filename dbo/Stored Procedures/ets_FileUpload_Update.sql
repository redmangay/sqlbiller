﻿

CREATE PROCEDURE [dbo].[ets_FileUpload_Update]
(
	@nFileUploadID int ,
	@nTableID int ,
	@sUploadName varchar(100) ,
	@sLocation varchar(250) ,
	@sFilename varchar(250) ,
	@bUseMapping bit,
	@nTemplateID int = NULL,
	@bSaveDocument bit,
	@sCriteria varchar(250) = NULL,
	@nDocumentTypeID int = NULL
)

AS

BEGIN TRY 
	UPDATE FileUpload
	SET 
		TableID = @nTableID,
		UploadName = @sUploadName,
		Location = @sLocation,
		Filename = @sFilename,
		UseMapping = @bUseMapping,
		TemplateID = @nTemplateID,
		SaveDocument = @bSaveDocument,
		FileNameCriteria = @sCriteria,
		DocumentTypeID = @nDocumentTypeID
	WHERE FileUploadID = @nFileUploadID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_FileUpload_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
