﻿

CREATE PROCEDURE [dbo].[dbg_Record_One_Update]
(
	@sSystemName varchar(50),
	@sValue varchar(MAX),
	@sRecordID varchar(50)	
)
AS



/*
EXEC [dbo].[Data_Validation] '1111', 'Value > 0 AND Value < 1000'
*/
BEGIN TRY

	DECLARE @sSQL varchar(MAX)
	SELECT @sSQL='UPDATE Record SET '+@sSystemName+'='''+@sValue+''' WHERE RecordID=' + @sRecordID
   
	
	PRINT @sSQL
	
	EXEC (@sSQL)
	
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_Record_One_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

