﻿

CREATE PROCEDURE [dbo].[rrp_LostTimeInjuriesYTD]
(
	@AccountID int,
	@FromDate date, 
	@ToDate date
)
AS  

BEGIN TRY
	-- Workaround: Define the columne in here where it will never be run:
	 IF 1 = 2
		 BEGIN
		   -- These are the actual column names and types returned by the real proc
		   SELECT CAST('' AS nvarchar(20)) AS Title, 
				  CAST('' AS nvarchar(20)) AS [Region],
				  0 as [Count]
		 END;
	-- End Workaround

	DECLARE @sFromDate varchar(10), @sToDate varchar(10)
	SELECT @sFromDate = CONVERT( varchar(10), @FromDate, 103),
		@sToDate = CONVERT( varchar(10), @ToDate, 103)

	DECLARE @sSQL varchar(MAX)
	SET @sSQL = '
		SELECT ''' + @sFromDate + ' to ' + @sToDate + ''' AS   Title,
			ISNULL(R.Region, ''Not Set'') AS [Region], 
			COUNT(*) as [Count]
			FROM Account' + CAST(@AccountID as varchar) + '.vIncidents I 
			JOIN Account' + CAST(@AccountID as varchar) + '.vSites S ON I.[Incident Location] = S.[Record ID] 
			LEFT JOIN Account' + CAST(@AccountID as varchar) +'.vRegion R ON S.[Region] = R.[Record ID] 
			WHERE [Type of Incident] = ''Lost Time Injury''
			GROUP BY R.Region'
	PRINT (@sSQL)	
	EXEC (@sSQL)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('rrp_LostTimeInjuriesYTD', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
