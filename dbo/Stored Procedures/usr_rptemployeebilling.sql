﻿CREATE PROCEDURE [dbo].[usr_rptemployeebilling]   
@StartDate varchar(100)= '01/06/2018', --= '19/10/2012',    
@EndDate varchar(100) ='30/06/2018' , --= '31/10/2014',
@EmployeeID  varchar(50)='1', --= NULL ,   --= '77',
@GroupDivision varchar(100)='1'     --= NULL     --  ='Windtech (Asia)' 
AS  

BEGIN  
/* 

    EXEC dbo.usr_rptEmployeeBilling '01/10/2019','30/10/2019','12911939','2'

    
/
 */

DECLARE @Sql varchar(max) = ''
DECLARE @Employee varchar(max) = ''
DECLARE @Group varchar(max) = ''

IF (@EmployeeID <> 1)
BEGIN
    SET @Employee = ' AND [EmployeeList].RecordID = ' + @EmployeeID
END

IF (@GroupDivision <> 1)
BEGIN
    SET @Group = ' AND [Job Group].RecordID = ''' + @GroupDivision + ''''
END

	  
    CREATE TABLE #ReportTable  (
			  [Counter] int identity(1,1)
			, [Date] date
			--, [Timesheet Date] date
			, [GroupName] varchar(max)
			, [Employee Name] varchar(100)
			, [Timesheet] money
			, [Status] varchar(max)  
			, [Billed] money
			, [Percentage_Billed] float 
			, [Unbilled] money
			, [Percentage_Unbilled] float 
			, [Job Value] money
			, [Currency] varchar(10)
			, [Job Controller] varchar(max)
			, [Job Title] varchar(max)
			, [Invoice No] varchar(max)
			, [Job No] varchar(max)
			, [Invoice Amount] money
			, [TaxAmount] MONEY
			, [TaxRate] float
			, [TaxName] varchar(max)
			, Header varchar(max)
			,BAPAActivity varchar(max)
			
			)

	   			    
				   SET @Sql = '
				    set dateformat dmy;
				    SELECT 
				    convert(date,[Bill & Payment Activity].V004) AS [Date]-- DATE OF INVOICE
				  --, [Timesheet Details].V009 AS [Timesheet Date] --REMOVE TIMESHEET DATE, ADD BELOW NEW INSERT CAME FROM Time Billing Information
				  , [Job Group].V001--[Timesheet Details].V003 AS [Details]   --, Get DETAILS in TS Details - 2729
				  , [EmployeeList].V003 AS [Employee Name] --, Get INITIALS from Time
				  ,
				  
				    CASE 
					   WHEN CAST(ISNULL([Timesheet Details].V007,0) AS MONEY) = 0 THEN ''0.00''
					   WHEN [Timesheet Details].V007 IS NULL THEN 0
					   ELSE 
					   ISNULL(CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1))   /*convert(money,[Group Currency Rates].V002)*/ AS MONEY),0) --, Get the Timesheet (Amount) from TS Details
					   END AS [TimeSheet]
				  , [Timesheet Details].V008 AS [Status]
				  ,  CASE 
					   WHEN CAST(ISNULL([Timesheet Details].V020,0) AS MONEY) = 0 THEN ''0.00''
					   WHEN [Timesheet Details].V020 IS NULL THEN 0
					   ELSE  
				  ISNULL(CAST(CONVERT(MONEY,[Timesheet Details].V020) / convert(money,ISNULL([Job Information].V007,1))  /*convert(money,[Group Currency Rates].V002)*/ AS MONEY),0) --, Get the Billed from TS Details
					   END AS [Billed]
				  , CASE 
					   WHEN CAST(ISNULL([Timesheet Details].V020,0) AS MONEY) = 0 THEN ''0.00''
					   WHEN [Timesheet Details].V020 IS NULL THEN 0
					   ELSE 
					   ISNULL((CAST([Timesheet Details].V020 AS MONEY) / CASE WHEN CAST(ISNULL([Timesheet Details].V007,0) AS MONEY)= 0 THEN 1 END) * 100,0) --, Billed / Timesheet (Amount) * 100 
					   END AS [Percentage Billed]
				  ,  CASE 
					   WHEN CAST(ISNULL([Timesheet Details].V007,0) AS MONEY) = 0 THEN ''0.00''
					   WHEN [Timesheet Details].V007 IS NULL THEN 0
					   ELSE 
					   cast(round(CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1))   /* convert(money,[Group Currency Rates].V002) */ AS MONEY) - CAST(CONVERT(MONEY,[Timesheet Details].V020) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),2,1) as NUMERIC(18,2)  )  --, Timesheet - Billed
					   END AS [Unbilled]
				  , ISNULL((CAST(ISNULL([Timesheet Details].V007,0) AS MONEY) - CAST(ISNULL([Timesheet Details].V020,0) AS MONEY)) / CASE WHEN CAST(ISNULL([Timesheet Details].V007,0) AS MONEY) = 0 THEN 1 ELSE CAST([Timesheet Details].V007 AS MONEY) END * 100,0) AS [Percentage Unbilled]--, (Timesheet (Amount) - Billed) / Timesheet (Amount) * 100
				  , CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Job Value]--, Job Value from Job Information
				  , [Currency Rates].V001 AS [Currency]--, Currency, get from Job Informaiton, from Currency Rate field Currency
				  , [EmployeeList_Job Controller].V004 AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
				  , [Job Information].V003 AS [Job Title]--Job Title, get from Job Information V003
				  , [Bill & Payment Activity].V017  AS [Invoice No] 
				  , [Job Information].V001 AS [Job No]
				  , CAST(Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Invoice Amount]
				  , CAST(Round((Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / (1 + ([Job Information].V011 / 100.00))) * [Job Information].V011 / 100.00,2)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [TaxAmount]--CAST([Bill & Payment Activity].V063 AS NUMERIC(18,2)) AS [GST]
				  , [Job Information].V011 AS [TaxRate]
				  , [Tax Name].V001 AS [Tax Name]
				     , 	(SELECT V039  FROM RECORD WHERE TABLEID = 2743) AS [Header] 
				 -- , ''Invoiced''
				 ,[Bill & Payment Activity].V029
				  
				  
			FROM [Record] [Job Information] WITH (NOLOCK) 

			JOIN [Record] [Bill & Payment Activity] ON [Bill & Payment Activity].TableID = 2711
				AND		[Bill & Payment Activity].IsActive = 1
				AND		[Bill & Payment Activity].V002 = CAST([Job Information].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		[Bill & Payment Activity].V029 = ''Invoice''
				--AND [Bill & Payment Activity].V017  LIKE ''%BD%''
				AND	     CONVERT(DATE,[Bill & Payment Activity].V004) >= CAST('''+ @StartDate +''' AS DATE)
				AND	     CONVERT(DATE,[Bill & Payment Activity].V004) <= CAST('''+ @EndDate +''' AS DATE)
				

			 JOIN [Record] [Timesheet Details] WITH (NOLOCK) ON [Timesheet Details].TableID = 2729
				AND		[Timesheet Details].IsActive = 1
				AND		[Timesheet Details].V016 = CAST([Bill & Payment Activity].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		CAST([Timesheet Details].V007 AS MONEY) IS NOT NULL
				AND		CAST([Timesheet Details].V020 AS MONEY) IS NOT NULL
				--AND	     CONVERT(MONEY,[Timesheet Details].V007) >0
				--AND	     CONVERT(MONEY,[Timesheet Details].V020) >0
				AND		  [Timesheet Details].V008 = ''By Rate''

				--AND	     CONVERT(DATE,[Timesheet Details].V009) >= CAST('''+ @StartDate +''' AS DATE)
				--AND	     CONVERT(DATE,[Timesheet Details].V009) <= CAST('''+ @EndDate +''' AS DATE)
					

		
			 JOIN [Record] [EmployeeList] WITH (NOLOCK) ON [EmployeeList].TableID = 2669
				AND		[EmployeeList].IsActive = 1
				--AND		  [EmployeeList].V027 = ''1''
				--AND		  [EmployeeList].V002 = ''false''
				AND		CAST([EmployeeList].RecordID AS VARCHAR(MAX)) = [Timesheet Details].V002 		/*	Job Number	*/
				'+ @Employee +'

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		CAST([EmployeeList_Job Controller].RecordID AS VARCHAR(MAX)) = [Job Information].V026 		/*	Job Number	*/
				
			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		CAST([Currency Rates].RecordID AS VARCHAR(MAX)) = [Job Information].V034 		/*	Job Number	*/

			LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		CAST([Tax Name].RecordID AS VARCHAR(MAX)) = [Job Information].V042 		/*	Job Number	*/
			 
			 left JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
				AND			[Job Group].IsActive = 1
				AND		CAST( [Job Group].RecordID AS VARCHAR(MAX)) = [EmployeeList].V022		/*	Currency	*/
				'+ @Group + ' 

			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/

					WHERE [Job Information].TableID = 2475 AND
					[Job Information].IsActive = 1 
					AND ISNUMERIC(ISNULL([Job Information].V008,0)) = 1
				     AND ISNUMERIC(ISNULL([Job Information].V007,0)) = 1
					--AND [Job Information].V001 = ''TH511-01''
					' 
					
					 --AND [Job Information].V001 = 'TK138-01'
					--AND CONVERT(MONEY,ISNULL([Job Information].[V008],0)) > 0 
					--AND CONVERT(MONEY,ISNULL([Job Information].[V007],0)) > 0 					
					--AND [Job Information].RecordID IN (SELECT Int_Value FROM DBO.fn_ParseText2Table(@RecordIDs,','))
						--ORDER BY [Bill & Payment Activity].V017 

		INSERT INTO #ReportTable EXEC(@Sql);
		print @Sql

		

/* === Lets Insert the BDR and BD from Time Billing Information === */
/* ============================================================================================================== */
   SET @Sql = '
				    set dateformat dmy;
				    SELECT 
				    convert(date,[Bill & Payment Activity].V004) AS [Date]-- DATE OF INVOICE
				  --, [Timesheet Details].V009 AS [Timesheet Date] --REMOVE TIMESHEET DATE, ADD BELOW NEW INSERT CAME FROM Time Billing Information
				  , [Job Group].V001--[Timesheet Details].V003 AS [Details]   --, Get DETAILS in TS Details - 2729
				   , [EmployeeList].V003 AS [Employee Name] --, Get INITIALS from Time
				  ,''0.00'' --ISNULL(CAST(CONVERT(MONEY,[Time Billing Calculation].V003)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) /*convert(money,[Group Currency Rates].V002)*/ AS MONEY),0) AS [TimeSheet]--, Get the Timesheet (Amount) from TS Details
				  --, [Timesheet Details].V008 AS [Status]
				  ,''By Rate''
				  , ISNULL(CAST(CONVERT(MONEY,[Time Billing Calculation].V003) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) /*convert(money,[Group Currency Rates].V002)*/ AS MONEY),0) AS [Billed]--, Get the Billed from TS Details
				  , ISNULL((CAST([Time Billing Calculation].V003 AS MONEY) / CAST(ISNULL([Time Billing Calculation].V003,0) AS MONEY)) * 100,0) AS [Percentage Billed]--, Billed / Timesheet (Amount) * 100 
				  , cast(round(CAST(CONVERT(MONEY,[Time Billing Calculation].V003)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) /* convert(money,[Group Currency Rates].V002) */ AS MONEY) - CAST(CONVERT(MONEY,[Time Billing Calculation].V003) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),2,1) as NUMERIC(18,2)  ) AS [Unbilled] --, Timesheet - Billed
				  , ISNULL((CAST([Time Billing Calculation].V003 AS MONEY) - CAST([Time Billing Calculation].V003 AS MONEY))/ CAST(ISNULL([Time Billing Calculation].V003,0) AS MONEY) * 100,0) AS [Percentage Unbilled]--, (Timesheet (Amount) - Billed) / Timesheet (Amount) * 100
				  , CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Job Value]--, Job Value from Job Information
				  , [Currency Rates].V001 AS [Currency]--, Currency, get from Job Informaiton, from Currency Rate field Currency
				  , [EmployeeList_Job Controller].V004 AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
				  , [Job Information].V003 AS [Job Title]--Job Title, get from Job Information V003
				  , [Bill & Payment Activity].V017  AS [Invoice No] 
				  , [Job Information].V001 AS [Job No]
				  , CAST(Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Invoice Amount]
				  , CAST(Round((Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / (1 + ([Job Information].V011 / 100.00))) * [Job Information].V011 / 100.00,2)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [TaxAmount]--CAST([Bill & Payment Activity].V063 AS NUMERIC(18,2)) AS [GST]
				  , [Job Information].V011 AS [TaxRate]
				  , [Tax Name].V001 AS [Tax Name]
				     , 	(SELECT V039  FROM RECORD WHERE TABLEID = 2743) AS [Header] 
				 -- , ''Invoiced''
				 ,[Bill & Payment Activity].V029
				  
				  
			FROM [Record] [Job Information] WITH (NOLOCK) 

			JOIN [Record] [Bill & Payment Activity] ON [Bill & Payment Activity].TableID = 2711
				AND		[Bill & Payment Activity].IsActive = 1
				AND		[Bill & Payment Activity].V002 = CAST([Job Information].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		[Bill & Payment Activity].V029 IN (''Bad Debt Recovery Entry'',''Bad Debt Entry'',''Credit Entry'',''Interim Credit'')
				--AND [Bill & Payment Activity].V017  LIKE ''%BD%''
				AND	     CONVERT(DATE,[Bill & Payment Activity].V004) >= CAST('''+ @StartDate +''' AS DATE)
				AND	     CONVERT(DATE,[Bill & Payment Activity].V004) <= CAST('''+ @EndDate +''' AS DATE)
				
		  	JOIN [Record] [Time Billing Calculation] ON [Time Billing Calculation].TableID = 2765
				AND [Time Billing Calculation].IsActive = 1
				AND [Time Billing Calculation].V001 = [Bill & Payment Activity].RecordID
				


			 --JOIN [Record] [Timesheet Details] WITH (NOLOCK) ON [Timesheet Details].TableID = 2729
				--AND		[Timesheet Details].IsActive = 1
				--AND		[Timesheet Details].V016 = CAST([Bill & Payment Activity].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				--AND		CAST([Timesheet Details].V007 AS MONEY) IS NOT NULL
				--AND		CAST([Timesheet Details].V020 AS MONEY) IS NOT NULL
				--AND	     CONVERT(MONEY,[Timesheet Details].V007) >0
				--AND	     CONVERT(MONEY,[Timesheet Details].V020) >0
				--AND		  [Timesheet Details].V008 = ''By Rate''

				--AND	     CONVERT(DATE,[Timesheet Details].V009) >= CAST('''+ @StartDate +''' AS DATE)
				--AND	     CONVERT(DATE,[Timesheet Details].V009) <= CAST('''+ @EndDate +''' AS DATE)
					

		
			 JOIN [Record] [EmployeeList] WITH (NOLOCK) ON [EmployeeList].TableID = 2669
				AND		[EmployeeList].IsActive = 1
				--AND		  [EmployeeList].V027 = ''1''
				--AND		  [EmployeeList].V002 = ''false''
				AND		CAST([EmployeeList].RecordID AS VARCHAR(MAX)) = [Time Billing Calculation].V002 		/*	Job Number	*/
				'+ @Employee +'

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		CAST([EmployeeList_Job Controller].RecordID AS VARCHAR(MAX)) = [Job Information].V026 		/*	Job Number	*/
				
			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		CAST([Currency Rates].RecordID AS VARCHAR(MAX)) = [Job Information].V034 		/*	Job Number	*/

			LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		CAST([Tax Name].RecordID AS VARCHAR(MAX)) = [Job Information].V042 		/*	Job Number	*/
			 
			JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
				AND			[Job Group].IsActive = 1
				AND		CAST( [Job Group].RecordID AS VARCHAR(MAX)) = [EmployeeList].V022		/*	Currency	*/
				'+ @Group + ' 

			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/

					WHERE [Job Information].TableID = 2475 AND
					[Job Information].IsActive = 1 
					AND ISNUMERIC(ISNULL([Job Information].V008,0)) = 1
				     AND ISNUMERIC(ISNULL([Job Information].V007,0)) = 1
					--AND [Job Information].V001 = ''TH511-01''
					' 
					
					 --AND [Job Information].V001 = 'TK138-01'
					--AND CONVERT(MONEY,ISNULL([Job Information].[V008],0)) > 0 
					--AND CONVERT(MONEY,ISNULL([Job Information].[V007],0)) > 0 					
					--AND [Job Information].RecordID IN (SELECT Int_Value FROM DBO.fn_ParseText2Table(@RecordIDs,','))
						--ORDER BY [Bill & Payment Activity].V017 

		INSERT INTO #ReportTable EXEC(@Sql);
		--print @Sql
		 
/* ============================================================================================================== */
  CREATE TABLE  #ReportTableFinal (
			  [Counter] int identity(1,1)
			 ,Counter_Old int
			, [Date] date
			--, [Timesheet Date] date
			, [GroupName] varchar(max)
			, [Employee Name] varchar(100)
			, [Timesheet] money
			, [Status] varchar(max)  
			, [Billed] money
			, [Percentage_Billed] float
			, [Unbilled] money
			, [Percentage_Unbilled] float
			, [Job Value] money
			, [Currency] varchar(10)
			, [Job Controller] varchar(max)
			, [Job Title] varchar(max)
			, [Invoice No] varchar(max)
			, [Job No] varchar(max)
			, [Invoice Amount] money
			, [TaxAmount] MONEY
			, [TaxRate] float
			, [TaxName] varchar(max)
			, Header varchar(max)
			,BAPAActivity varchar(max)
			
			)

INSERT INTO #ReportTableFinal
SELECT * FROM #ReportTable
ORDER BY [Invoice No],[Date]--,[Timesheet Date]

--IF @ReportType = 0
--BEGIN

DECLARE @JobNox   VARCHAR(MAX) = '';
DECLARE @COUNTER   INT = 1;
WHILE EXISTS
(
    SELECT
	  *
    FROM  #ReportTableFinal
    WHERE [Counter] >= @COUNTER 
)
    BEGIN

	   DECLARE @JobNo   VARCHAR(MAX);

	   SELECT
		 @JobNo = [Invoice No]
	   FROM  #ReportTableFinal
	   WHERE [Counter] = @Counter;


	   IF @JobNox = @JobNo
		  BEGIN
			 UPDATE #ReportTableFinal
			   SET
				  [INVOICE AMOUNT] = 0
				  ,[TaxAmount] = 0
			 WHERE
			    [COUNTER] = @COUNTER;
		  END;


	   SET @JobNox = @JobNo;
	   SET @COUNTER = @COUNTER + 1;
    END;

  --  END
	
-- Show the reports

SELECT [Counter],
	  [Date],
	  -- [Timesheet Date],
	  [GroupName],
	  [Employee Name],
	  [Timesheet],
	  [Status],
	  [Billed],
	  [Percentage_Billed],
	  [Unbilled],
	  [Percentage_Unbilled],
	  [Job Value],
	  [Currency],
	  [Job Controller],
	  [Job Title],
	  [Invoice No],
	  [Job No],
	  ISNULL([Invoice Amount],'0.00') AS [Invoice Amount],
	  ISNULL([TaxAmount],'0.00') AS [TaxAmount],
	  [TaxRate],
	  [TaxName],
	  Header	   
	   ,BAPAActivity 
	    FROM #ReportTableFinal
	    --WHERE GROUPNAME ='Major Projects'
	    order by [Employee Name],[Job No] ASC

	   

END 