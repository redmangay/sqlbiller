﻿

CREATE PROCEDURE   [dbo].[ets_Table_Import_Position]
(
	@nTableID int
)
AS
/*
EXEC ets_Table_Import_Position 60
*/
BEGIN TRY
	SET NOCOUNT ON;	
	SET CONCAT_NULL_YIELDS_NULL OFF
	DECLARE @nImportTemplateID int=NULL
	IF @nImportTemplateID IS NULL
		SELECT @nImportTemplateID=DefaultImportTemplateID FROM [Table] WHERE TableID = @nTableID
	
	IF @nImportTemplateID IS NULL
		SELECT top 1 @nImportTemplateID=ImportTemplateID FROM [ImportTemplate] WHERE TableID = @nTableID ORDER BY ImportTemplateID DESC

	IF @nImportTemplateID IS NOT NULL
		BEGIN
  			SELECT C.*,ITI.ImportHeaderName,ITI.PositionOnImport FROM [Column] C INNER JOIN ImportTemplateItem ITI
			ON C.ColumnID=ITI.ColumnID
				WHERE C.TableID = @nTableID and ImportTemplateID=@nImportTemplateID
				AND ITI.PositionOnImport IS NOT NULL
				ORDER BY ITI.ColumnIndex
		
		END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Table_Import_Position', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
