﻿

CREATE PROCEDURE [dbo].[Record_Copy_Field]
(
 @nTableID int,
 @sColumnNameFrom varchar(20),
 @sColumnNameTo varchar(20)
)
AS
BEGIN TRY
	 DECLARE @sSQL varchar(MAX)

	 -- basic version onlyu copies non NULL values to NULL fields:
	 SELECT @sSQL = 'UPDATE [Record] SET ' + @sColumnNameTo + ' = ' + @sColumnNameFrom 
	  + ' WHERE TableID = ' + CAST(@nTableID as varchar(20)) + ' AND ' + @sColumnNameFrom + ' IS NOT NULL AND ' + @sColumnNameTo + ' IS NULL'

	 PRINT @sSQL
	 EXEC (@sSQL)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Record_Copy_Field', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
