﻿create PROCEDURE [dbo].[Renzo_SaveBillindAddress]
(
	@RecordID  int,		/*	Record ID passed from the table on execute, base your solution on this value	*/
	@UserID int,
	@Return varchar(max) output
	, @Context varchar(max) = null
)
AS

/*

EXEC Renzo_SaveGroup @RecordID = '2717569',@UserID = '25197',@Return=''

*/
BEGIN

	   DECLARE @ClientContactRecordID int

	   SELECT @ClientContactRecordID = V008 FROM Record
	   WHERE RecordID = @RecordID

	   IF @ClientContactRecordID IS NULL
	   BEGIN

		  UPDATE Record
		  SET V019 = V018
		  WHERE RecordID = @RecordID

	   END
	   ELSE 
	   BEGIN
		  DECLARE @Name varchar(max)

		  SELECT @Name = 
		  ISNULL(V004,'') + ' ' + ISNULL(V005,'') + ' ' + ISNULL(V006,'')
		  FROM Record
		  WHERE RecordID = @ClientContactRecordID

		  UPDATE Record
		  SET V019 = LTRIM(RTRIM(@Name))
		  WHERE RecordID = @RecordID

	   END



	RETURN @@ERROR;
END;
