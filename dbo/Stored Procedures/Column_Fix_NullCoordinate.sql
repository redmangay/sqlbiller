﻿
 
CREATE PROCEDURE   [dbo].[Column_Fix_NullCoordinate]
(
	@nTableID int 
)
	/*----------------------------------------------------------------
	EXEC Column_Fix_NullCoordinate @nTableID=2971
	---------------------------------------------------------------*/
 
AS

BEGIN TRY
	--UPDATE [Column] SET Xcoordinate=NULL, Ycoordinate=NULL WHERE TableID=@nTableID --AND 	DisplayTextDetail IS NULL		

	DECLARE Page_Cursor CURSOR FOR
	SELECT DISTINCT(TableTabID) FROM [Column] WHERE TableID=@nTableID;
	DECLARE @nTableTabID INT;
	DECLARE @nPageCounter INT=0;
	OPEN Page_Cursor;
	FETCH NEXT FROM Page_Cursor INTO @nTableTabID;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
	 
				--SELECT C.ColumnID, C.DisplayName,C.TableTabID,C.Xcoordinate,C.Ycoordinate,
				--C.DisplayRight,C.DisplayOrder,ROW_NUMBER() OVER(ORDER BY C.DisplayOrder) AS num
				-- FROM [Column] C WHERE TableID=@nTableID AND DisplayTextDetail IS NOT NULL 
				--AND DisplayRight<>1 AND 
				--((@nTableTabID IS NULL AND TableTabID IS NULL) OR (@nTableTabID IS NOT NULL AND TableTabID=@nTableTabID))
			
			DECLARE @nYmax INT
			SELECT @nYmax=MAX(Ycoordinate) FROM [Column] WHERE TableID=@nTableID AND DisplayTextDetail IS NOT NULL 
			AND ((@nTableTabID IS NULL AND TableTabID IS NULL) OR (@nTableTabID IS NOT NULL AND TableTabID=@nTableTabID))
		
			IF @nYmax IS NULL
			 SET @nYmax=0

			 UPDATE [Column] SET Xcoordinate=1, Ycoordinate=Table_A.num + @nYmax
			 FROM [Column] JOIN 
			  ( SELECT C.ColumnID, C.DisplayName,C.TableTabID,C.Xcoordinate,C.Ycoordinate,
				C.DisplayRight,C.DisplayOrder,ROW_NUMBER() OVER(ORDER BY C.DisplayOrder) AS num
				 FROM [Column] C WHERE TableID=@nTableID AND DisplayTextDetail IS NOT NULL AND (Xcoordinate IS NULL OR Ycoordinate IS NULL)
				AND DisplayRight<>1 AND 
				((@nTableTabID IS NULL AND TableTabID IS NULL) OR (@nTableTabID IS NOT NULL AND TableTabID=@nTableTabID))) AS Table_A
				ON [Column].ColumnID=Table_A.ColumnID

			UPDATE [Column] SET Xcoordinate=2, Ycoordinate=Table_A.num + @nYmax
			 FROM [Column] JOIN 
			  ( SELECT C.ColumnID, C.DisplayName,C.TableTabID,C.Xcoordinate,C.Ycoordinate,
				C.DisplayRight,C.DisplayOrder,ROW_NUMBER() OVER(ORDER BY C.DisplayOrder) AS num
				 FROM [Column] C WHERE TableID=@nTableID AND DisplayTextDetail IS NOT NULL  AND (Xcoordinate IS NULL OR Ycoordinate IS NULL)
				AND DisplayRight=1 AND 
				((@nTableTabID IS NULL AND TableTabID IS NULL) OR (@nTableTabID IS NOT NULL AND TableTabID=@nTableTabID))) AS Table_A
				ON [Column].ColumnID=Table_A.ColumnID
	  
		  SET @nPageCounter=@nPageCounter+1
		  FETCH NEXT FROM Page_Cursor INTO @nTableTabID;
	   END;
	CLOSE Page_Cursor;
	DEALLOCATE Page_Cursor;
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Column_Fix_NullCoordinate', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
