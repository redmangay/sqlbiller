﻿

CREATE PROCEDURE [dbo].[spCreateView]
(
	@AccountID		varchar(MAX)
	,@TableName		varchar(MAX)
)
 
AS

BEGIN TRY
	DECLARE			@SQL			varchar(MAX)

	--				Create Schema

	SET				@SQL = 'CREATE SCHEMA Account' + @AccountID

	BEGIN TRY
		EXEC			(@SQL)
	END TRY
	BEGIN CATCH
		PRINT			'Warning: Schema Account' + @AccountID + ' exists
		'
	END CATCH
	
	--				Drop View

	SET				@SQL = 'IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N''Account' + @AccountID + '.[v' + @TableName + ']''))
	DROP VIEW Account' + @AccountID + '.[v' + @TableName + ']'

	EXEC			(@SQL)

	--				Create View
	
	IF  EXISTS (	SELECT			[Table].TableID
					FROM			[Table]
					WHERE			[Table].IsActive = 1
					AND				[Table].AccountID = @AccountID
					AND				[Table].[TableName] = @TableName
				)
	BEGIN
		SET				@SQL = 'CREATE	VIEW	Account' + @AccountID + '.[v' + @TableName + ']
		AS
		SELECT			[Table].[TableID]
		'

		SELECT			@SQL = @SQL + '			,[Record].' + [Column].[SystemName] + '	AS				[' + [Column].[DisplayName] + ']
		'
		FROM			[Table]
		JOIN			[Column] ON						[Column].TableID = [Table].TableID
											AND			([Column].IsStandard = 0
											OR			[Column].SystemName = 'RecordID')
		WHERE			[Table].IsActive = 1
		AND				[Table].AccountID = @AccountID
		AND				[Table].[TableName] = @TableName
		ORDER BY		[Column].[DisplayOrder]

		SET				@SQL = SUBSTRING(@SQL, 1, LEN(@SQL) - 4) + '
		,[Record].DateAdded
		FROM			[Table]
		JOIN			[Record] ON						[Record].IsActive = 1
											AND			[Record].TableID = [Table].TableID
		WHERE			[Table].IsActive = 1
		AND				[Table].AccountID = ' + @AccountID + '
		AND				[Table].[TableName] = ''' + @TableName + ''''
		
		PRINT			@SQL
		EXEC			(@SQL)
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spCreateView', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
