﻿
      
CREATE Procedure [dbo].[usr_rptAccountsReceivable]        


AS        
BEGIN   

/*

EXEC [dbo].[usr_rptAccountsReceivable]     


*/


DECLARE @ReceivableTable TABLE
(
	[Counter]       INT IDENTITY(1, 1)
   , [JobRecordID]   INT
   , [JobNumber]     VARCHAR(MAX)
   , [Client Name]   varchar(max)
   , [JobTitle]      VARCHAR(MAX)
   , [OverDue]       MONEY
   , [Current]       MONEY

    , [<Current (<15 days)]       MONEY
    , [Date (<15 days)]       date
    , [30 days (16-30 days)]       MONEY
    , [Date (16-30 days)]       date
    , [45 days (31-45 days)]       MONEY
    , [Date (31-45 days)]       date	    
    , [60 days (45-60 days)]       MONEY
    , [Date (45-60 days)]       date   
    , [60+ days (>60 days)]       MONEY
    , [Date (>60 days)]       date

   , [Balance]       MONEY
   , [Currency]      VARCHAR(MAX)
   , [Group Name]      VARCHAR(MAX)
   , [IsConverted]   VARCHAR(MAX)
   , [Activity] varchar(100)
);
DECLARE @ReceivablePaymentTable TABLE
(
	[Counter]         INT IDENTITY(1, 1)
   , [JobRecordID]     INT
   , [PaymentAmount]   MONEY
   , [CreditAmount]    MONEY
   , [Activity]        VARCHAR(MAX)
   , [Group Name]      VARCHAR(MAX)
);


DECLARE @ReceivableTableFinal TABLE
(
	[Counter]       INT IDENTITY(1, 1)
   , [JobRecordID]   INT
   , [JobNumber]     VARCHAR(MAX)
     , [Client Name]   varchar(max)
   , [JobTitle]      VARCHAR(MAX)
   --, [OverDue]       MONEY
   --, [Current]       MONEY /* Removed, user <15 days */

    , [<Current (<15 days)]       MONEY
    , [Date (<15 days)]       date
    , [30 days (16-30 days)]       MONEY
    , [Date (16-30 days)]       date
    , [45 days (31-45 days)]       MONEY
    , [Date (31-45 days)]       date	    
    , [60 days (45-60 days)]       MONEY
    , [Date (45-60 days)]       date   
    , [60+ days (>60 days)]       MONEY
    , [Date (>60 days)]       date

   , [Balance]       MONEY
   , [Currency]      VARCHAR(MAX)
   , [Group Name]      VARCHAR(MAX)
   , [IsConverted]   VARCHAR(MAX)
    , [InvoiceDate] date
);



DECLARE @ReceivableTablePreFinal TABLE
(
	[Counter]       INT IDENTITY(1, 1)
   , [JobRecordID]   INT
   , [JobNumber]     VARCHAR(MAX)
     , [Client Name]   varchar(max)
   , [JobTitle]      VARCHAR(MAX)
   --, [OverDue]       MONEY
   --, [Current]       MONEY /* Removed, user <15 days */

    , [<Current (<15 days)]       MONEY
    , [Date (<15 days)]       date
    , [30 days (16-30 days)]       MONEY
    , [Date (16-30 days)]       date
    , [45 days (31-45 days)]       MONEY
    , [Date (31-45 days)]       date	    
    , [60 days (45-60 days)]       MONEY
    , [Date (45-60 days)]       date   
    , [60+ days (>60 days)]       MONEY
    , [Date (>60 days)]       date

   , [Balance]       MONEY
   , [Currency]      VARCHAR(MAX)
   , [Group Name]      VARCHAR(MAX)
   , [IsConverted]   VARCHAR(MAX)
   , [Invoice Date] date
);


SET DATEFORMAT dmy;
DECLARE @BasedDate int --= 30;

SELECT @BasedDate = V003 FROM [RECORD] WHERE TABLEID = 2706

    INSERT INTO @ReceivableTable(
	   JobRecordID
	   ,JobNumber
	   ,[Client Name]
	   ,JoBTitle
	   --,OverDue
	   -- ,[Current] /* Removed, user <15 days */
	   , [<Current (<15 days)]    
	    , [Date (<15 days)]
	   , [30 days (16-30 days)]    
	    , [Date (16-30 days)]   
	   , [45 days (31-45 days)]   
	    , [Date (31-45 days)]    
	   , [60 days (45-60 days)]    
	    , [Date (45-60 days)]   
	   , [60+ days (>60 days)]      
	    , [Date (>60 days)]   
	   ,Currency
	   , [Group Name]      
	   ,IsConverted
	   ,[Activity])
    SELECT [Job Information].RecordID
	   ,[Job Information].[V001]
	   ,[Client Name].V017 
	   ,[Job Information].[V003]
	   --,CASE 
		 --WHEN DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) > @BasedDate
		 --THEN 
		 --(CAST(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V005,0) AS MONEY)),2) / 
			--  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY) ) - 
			--  (CAST(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V007,0) AS MONEY)),2) / 
			--  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY) )
		 -- --ELSE 0 
		 -- --END AS 
		 -- [OVERDUE]

		  /* Removed, user <15 days */
	 --  ,CASE 
		--  WHEN DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) < @BasedDate + 1
		--THEN (CAST(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V005,0) AS MONEY)),2) / 
		--	  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY) ) - 
		--	  (CAST(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V007,0) AS MONEY)),2) / 
		--	  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY) )
		--  ELSE 0 
		--  END AS [CURRENT]

		    ,CASE 
		  WHEN DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) < 16 
		  THEN (CAST(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V005,0) AS MONEY)),2) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY) ) --- 
			  --(CAST(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V007,0) AS MONEY)),2) / 
			 -- CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY) )
		  ELSE 0 
		  END AS [<Current (<15 days)]

		  /* Date */
		   ,CASE 
		  WHEN DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) < 16  AND [Bill & Payment Activity].V029 LIKE '%Invoice%'
		  THEN cast([Bill & Payment Activity].V004 as date)
		  ELSE '01/01/1901'
		  END AS [Date (<15 days)]
		  /* end Date */

		    ,CASE 
		  WHEN DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) > 15 AND
			  DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) < 31
		  THEN (CAST(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V005,0) AS MONEY)),2) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY) ) --- 
			 -- (CAST(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V007,0) AS MONEY)),2) / 
			  --CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY) )
		  ELSE 0 
		  END AS [30 days (16-30 days)]

		  /* Date */
		   ,CASE 
		  WHEN DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) > 15 AND
			  DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) < 31 AND [Bill & Payment Activity].V029 LIKE '%Invoice%'
		   THEN cast([Bill & Payment Activity].V004 as date)
		  ELSE '01/01/1901'
		  END AS [Date (16-30 days)]
		  /* end Date */

		      ,CASE 
		  WHEN DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) > 30 AND
		       DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) < 46
		  THEN(CAST(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V005,0) AS MONEY)),2) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY) )-- - 
			 -- (CAST(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V007,0) AS MONEY)),2) / 
			-- CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY) )
		  ELSE 0 
		  END AS [45 days (31-45 days)]

		   /* Date */
		   ,CASE 
		  WHEN DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) > 30 AND
		       DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) < 46 AND [Bill & Payment Activity].V029 LIKE '%Invoice%'
		  THEN cast([Bill & Payment Activity].V004 as date)
		  ELSE '01/01/1901'
		  END AS [Date (31-45 days)]
		  /* end Date */

		    ,CASE 
		  WHEN DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) > 45 AND
		       DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) < 61
		  THEN (CAST(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V005,0) AS MONEY)),2) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY) ) --- 
			--  (CAST(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V007,0) AS MONEY)),2) / 
			--  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY) )
		  ELSE 0 
		  END AS [60 days (45-60 days)]

		    /* Date */
		   ,CASE 
		  WHEN DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) > 45 AND
		       DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) < 61 AND [Bill & Payment Activity].V029 LIKE '%Invoice%'
		  THEN cast([Bill & Payment Activity].V004 as date)
		  ELSE '01/01/1901'
		  END AS [Date (45-60 days)]
		  /* end Date */

		    ,CASE 
		  WHEN DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) > 60
		  THEN (CAST(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V005,0) AS MONEY)),2) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY) ) --- 
			 -- (CAST(Round(convert(varchar(max),cast(ISNULL([Bill & Payment Activity].V007,0) AS MONEY)),2) / 
			 -- CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY) )
		  ELSE 0 
		  END AS [60+ days (>60 days)]

		   /* Date */
		   ,CASE 
		  WHEN DATEDIFF(day,[Bill & Payment Activity].V004, CONVERT(DATE,GETDATE(),103)) > 60 AND [Bill & Payment Activity].V029 LIKE '%Invoice%'
		   THEN cast([Bill & Payment Activity].V004 as date)
		  ELSE '01/01/1901'
		  END AS [Date (>60 days)]
		  /* end Date */

	   ,[Job Group].V004
	    ,[Job Group].V001
	   ,CASE 
		  WHEN [Job Currency Rates].V001 LIKE '%None%'
		  THEN NULL
		  WHEN [Job Group].V004  LIKE '%None%'
		  THEN NULL
		  WHEN [Job Currency Rates].V001 = [Job Group].V004 
		  THEN NULL 
		  ELSE CONVERT(VARCHAR,'C') 
		  END AS [Converted Symbol]
		  ,[Bill & Payment Activity].V029
    FROM [Record] [Bill & Payment Activity] 
    INNER JOIN [Record] [Job Information] ON CAST([Job Information].[RecordID] AS VARCHAR) = [Bill & Payment Activity].[V002]
		  AND [Job Information].[IsActive] = 1
		  AND [Job Information].[TableID] = 2475
		  AND [Job Information].[V021] NOT IN ('Fully Paid Up', 'Lost Bid', 'Bad Debt')    
		--AND [Job Information].rECORDID =      16670162

     LEFT JOIN Record [Client Name] ON [Client Name].TableID = 2476
	   AND [Client Name].IsActive = 1
	   AND [Client Name].V013 = [Job Information].RecordID
	
	LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
		  AND [Job Group].IsActive = 1
		  AND [Job Group].RecordID = CAST([Job Information].V027 as int)		/*	Currency	*/
	LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
		  AND [Group Currency Rates].IsActive = 1
		  AND CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/
	LEFT JOIN Record [Job Currency Rates] WITH (NOLOCK) on 		[Job Currency Rates].TableID = '2665'		/*	Currency Name	*/
		  AND [Job Currency Rates].IsActive = 1
		  AND [Job Currency Rates].RecordID = CAST([Job Information].V034 as int)		/*	[Group Currency]	*/
    WHERE [Bill & Payment Activity].TableID = 2711
		  AND [Bill & Payment Activity].IsActive = 1

    ORDER BY [Job Information].RecordID, CAST([Bill & Payment Activity].V004 AS DATE) ASC --, [Bill & Payment Activity].IsActive
		  --AND [Bill & Payment Activity].V029 IN ('Invoice','Interim Invoice')

		 --select * from @ReceivableTable
		  --where jobrecordid = 12953741
    /*LETS GET THE PAYMENT AND CREDIT*/
    --INSERT INTO @ReceivablePaymentTable (
		  --JobRecordID
		  --,[PaymentAmount]
		  --,[CreditAmount]
		  --,Activity
		  --,[Group Name])
    --SELECT [Job Information].RecordID
		  --,SUM(CAST(Round(CONVERT(VARCHAR(MAX),CAST(ISNULL([Bill & Payment Activity].V007,0) AS MONEY)),2) / 
			 --CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY))
		  --,SUM(ABS(CAST(ROUND(CONVERT(VARCHAR(max),CAST(ISNULL([Bill & Payment Activity].V005,0) AS MONEY)),2) / 
			 --CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY)))
		  --,[Bill & Payment Activity].[V029]
		  --,[Job Group].V001
    --FROM [Record] [Bill & Payment Activity] 
    --INNER JOIN [Record] [Job Information] ON CAST([Job Information].[RecordID] AS VARCHAR) = [Bill & Payment Activity].[V002]
		  --AND [Job Information].[IsActive] = 1
		  --AND [Job Information].[TableID] = 2475
		  --AND [Job Information].[V021] NOT IN ('Fully Paid Up', 'Lost Bid', 'Bad Debt')    
		  ----AND [Job Information].V001 = 'TK234-01'
    --LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
		  --AND [Job Group].IsActive = 1
		  --AND [Job Group].RecordID = CAST([Job Information].V027 as int)		/*	Currency	*/

    --LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
		  --AND [Group Currency Rates].IsActive = 1
		  --AND CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/
    --WHERE [Bill & Payment Activity].TableID = 2711
		  --AND		[Bill & Payment Activity].IsActive = 1
		  --AND		[Bill & Payment Activity].V029 IN ('Credit Entry','Interim Credit')
    --GROUP BY  [Job Information].RecordID,[Bill & Payment Activity].V029,[Job Group].V001


    --SELECT 'Recievable1',* FROM @ReceivableTable
    --SELECT * FROM @ReceivablePaymentTable


    --DECLARE @Counter INT = 1
    --DECLARE @JobTitle VARCHAR(MAX)
    --DECLARE @JobNumber VARCHAR(MAX) 
    --DECLARE @Activity VARCHAR(MAX)
    --DECLARE @ReportingCurrency VARCHAR(MAX)
    --DECLARE @IsConverted VARCHAR(MAX)
    --DECLARE @GroupName VARCHAR(MAX)
    --DECLARE @PaymentAmount MONEY
    --DECLARE @ProcessedPaymentAmount MONEY = 0.00
    --DECLARE @CreditAmount MONEY
    --DECLARE @JobRecordID INT
    --DECLARE @SumOverDue MONEY
    --DECLARE @SumCurrent MONEY
   -- WHILE EXISTS(SELECT * FROM @ReceivablePaymentTable WHERE [Counter] >= @Counter )
   -- BEGIN
	   
	  -- SELECT	 @Activity = Activity
			-- ,@JobRecordID = JobRecordID
			-- ,@PaymentAmount = PaymentAmount
			-- ,@CreditAmount = CreditAmount
			-- ,@GroupName = [Group Name]
	  -- FROM @ReceivablePaymentTable WHERE [Counter] = @Counter

	  -- SELECT @SumOverDue = SUM(OverDue)
			--,@SumCurrent = SUM([Current])  
	  -- FROM @ReceivableTable 
		 -- WHERE JobRecordID = @JobRecordID 
		 -- AND (([Current] > 0) OR (OVERDUE > 0)) 
	   
	  -- SELECT @IsConverted = IsConverted 
	  -- FROM @ReceivableTable 
	  -- WHERE JobRecordID = @JobRecordID 

	  -- SELECT @JobTitle = [Job Information].V003
		 --   ,@JobNumber  = [Job Information].V001
		 --   ,@ReportingCurrency = [Job Group].V004 
	  -- FROM [Record] [Job Information] 
	  -- LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
		 -- AND [Job Group].IsActive = 1
		 -- AND CAST( [Job Group].RecordID AS VARCHAR(MAX)) = [Job Information].V027		/*	Currency	*/
	  -- LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
		 -- AND [Group Currency Rates].IsActive = 1
		 -- AND CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/	   
	  -- WHERE [Job Information].RecordID = @JobRecordID AND [Job Information].TableID = 2475

	  -- --IF (@Activity = 'Payment Entry' OR @Activity = 'Overpaid Entry')
	  -- --BEGIN
		 -- --SET @ProcessedPaymentAmount = ISNULL(@SumOverDue,0) - @PaymentAmount
		 -- ----SET @ProcessedPaymentAmount = @SumOverDue - @PaymentAmount
		 -- --print 'activity: ' + @Activity
		 -- --print 'overdue: ' + cast(ISNULL(@SumOverDue,0) as varchar)
		 -- --print 'payment amount: ' + cast(@PaymentAmount as varchar)
		 -- --print 'processed payment: ' + cast(isnull(@ProcessedPaymentAmount,0) as varchar)
		 -- --IF (@ProcessedPaymentAmount < 0)
			-- --BEGIn
			-- --INSERT INTO @ReceivableTable(JobRecordID,JobNumber,JobTitle,OverDue,[Current],Currency,IsConverted,[Group Name])
			-- --SELECT @JobRecordID,@JobNumber,@JobTitle,@SumOverDue * -1,@ProcessedPaymentAmount,@ReportingCurrency, @IsConverted,@GroupName

			-- --END
		 -- --ELSE 
			-- --BEGIn
			-- -- INSERT INTO @ReceivableTable(JobRecordID,JobNumber,JobTitle,OverDue,[Current],Currency,IsConverted,[Group Name])
			-- --SELECT @JobRecordID,@JobNumber,@JobTitle,@PaymentAmount * -1,0,@ReportingCurrency, @IsConverted,@GroupName
			-- --END
	  -- --END

	  -- --IF (@Activity LIKE '%Credit%')
	  -- --BEGIN
		 -- --SET @ProcessedPaymentAmount = @SumCurrent - @CreditAmount
		 -- --IF (@ProcessedPaymentAmount < 0)
			-- --BEGIN
			
			-- --INSERT INTO @ReceivableTable(JobRecordID,JobNumber,JobTitle,OverDue,[Current],Currency,IsConverted,[Group Name])
			-- --SELECT @JobRecordID,@JobNumber,@JobTitle,@ProcessedPaymentAmount,@SumCurrent * -1, @ReportingCurrency, @IsConverted,@GroupName

			-- --END
		 -- --ELSE 
			-- --BEGIN
			-- -- INSERT INTO @ReceivableTable(JobRecordID,JobNumber,JobTitle,OverDue,[Current],Currency,IsConverted,[Group Name])
			-- --SELECT @JobRecordID,@JobNumber,@JobTitle,0,@CreditAmount * -1,@ReportingCurrency, @IsConverted,@GroupName
			-- --END
	  -- --END

   -- --SELECT 'Recievable2',* FROM @ReceivableTable
   -- SET @Counter = @Counter + 1
   -- END
    --SELECT * FROM @ReceivableTable

    DECLARE @xCounter int = 1
    DECLARE @RunningBalancex money = 0.00
    DECLARE @JobRecordIDxx int = -1
   

    WHILE EXISTS(SELECT * FROM @ReceivableTable WHERE [Counter] >= @xCounter)
    BEGIN
	    DECLARE @JobRecordIDx int
	     DECLARE @CurrentBalance1 money = 0.00
		  DECLARE @CurrentBalance2 money = 0.00
		    DECLARE @CurrentBalance3 money = 0.00
		      DECLARE @CurrentBalance4 money = 0.00
			   DECLARE @CurrentBalance5 money = 0.00
	    SELECT @JobRecordIDx = JobRecordID, 
			 @CurrentBalance1 = isnull([<Current (<15 days)],0),
			  @CurrentBalance2 = isnull([30 days (16-30 days)],0),
			   @CurrentBalance3 = isnull([45 days (31-45 days)],0),
			    @CurrentBalance4 = isnull([60 days (45-60 days)],0),
			     @CurrentBalance5 = isnull([60+ days (>60 days)],0)	 
			 
			 
			  FROM @ReceivableTable WHERE [Counter] = @xCounter


	    IF(@JobRecordIDx = @JobRecordIDxx)
	    BEGIN
	    /* settle the [60+ days (>60 days)] invoice */
		 IF(@CurrentBalance5 > 0 AND @RunningBalancex > 0)
		 BEGIN
		  print @RunningBalancex
		  SET @RunningBalancex = @RunningBalancex - @CurrentBalance5
		   print @RunningBalancex
		  IF(@RunningBalancex>=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [60+ days (>60 days)] = 0 WHERE [Counter] = @xCounter 
		  END
		  IF(@RunningBalancex<=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [60+ days (>60 days)] = @RunningBalancex * -1 WHERE [Counter] = @xCounter 
		  END
		 END
	   /* settle the [60 days (45-60 days)] invoice  */
	      IF(@CurrentBalance4 > 0  AND @RunningBalancex > 0)
		 BEGIN
		  SET @RunningBalancex = @RunningBalancex - @CurrentBalance4
		  
		  IF(@RunningBalancex>=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [60 days (45-60 days)] = 0 WHERE [Counter] = @xCounter 
		  END
		  IF(@RunningBalancex<=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [60 days (45-60 days)] = @RunningBalancex * -1 WHERE [Counter] = @xCounter 
		  END
		 END
	   /* settle the [45 days (31-45 days)] invoice  */
	      IF(@CurrentBalance3 > 0  AND @RunningBalancex > 0)
		 BEGIN
		  SET @RunningBalancex = @RunningBalancex - @CurrentBalance3
		  
		  IF(@RunningBalancex>=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [45 days (31-45 days)] = 0 WHERE [Counter] = @xCounter 
		  END
		  IF(@RunningBalancex<=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [45 days (31-45 days)] = @RunningBalancex * -1 WHERE [Counter] = @xCounter 
		  END
		 END
	   /* settle the [30 days (16-30 days)] invoice  */
	      IF(@CurrentBalance2 > 0  AND @RunningBalancex > 0)
		 BEGIN
		  SET @RunningBalancex = @RunningBalancex - @CurrentBalance2
		  
		  IF(@RunningBalancex>=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [30 days (16-30 days)] = 0 WHERE [Counter] = @xCounter 
		  END
		  IF(@RunningBalancex<=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [30 days (16-30 days)] = @RunningBalancex * -1 WHERE [Counter] = @xCounter 
		  END
		 END
	   /* settle the [<Current (<15 days)] invoice  */
	      IF(@CurrentBalance1 > 0  AND @RunningBalancex > 0)
		 BEGIN
		  PRINT 'RUNIING BAL: '  + CAST(@RunningBalancex AS VARCHAR)
		 	 PRINT 'Current Bal: ' + cast(@CurrentBalance1 as varchar)
		  SET @RunningBalancex = @RunningBalancex - @CurrentBalance1
		  
		  IF(@RunningBalancex>=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [<Current (<15 days)] = 0 WHERE [Counter] = @xCounter 
		  END
		  IF(@RunningBalancex<=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [<Current (<15 days)] = @RunningBalancex * -1 WHERE [Counter] = @xCounter 
		  END
		 END

	    END
	    ELSE
	    BEGIN


	    SELECT @RunningBalancex = 	      sum(CAST(Round(convert(varchar(max),cast(ISNULL(BAPA.V007,0) AS MONEY)),2) / 
			  CONVERT(MONEY,ISNULL(JOBINFO.V007,1)) * CONVERT(MONEY,ISNULL(JOBINFO.V047,1)) AS MONEY) )
			   FROM Record BAPA
			   JOIN RECORD JOBINFO ON JOBINFO.TableID = 2475
			   AND JOBINFO.IsActive = 1
			   AND JOBINFO.RecordID = @JobRecordIDx
		  WHERE BAPA.TableID = 2711
		  AND BAPA.IsActive = 1
		  AND BAPA.V029 NOT IN ('Bad Debt')
		  AND BAPA.V002 = @JobRecordIDx
		  print @RunningBalancex


		   /* settle the [60+ days (>60 days)] invoice */
		 IF(@CurrentBalance5 > 0 AND @RunningBalancex > 0)
		 BEGIN
		  print @RunningBalancex
		  SET @RunningBalancex = @RunningBalancex - @CurrentBalance5
		   print @RunningBalancex
		  IF(@RunningBalancex>=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [60+ days (>60 days)] = 0 WHERE [Counter] = @xCounter 
		  END
		  IF(@RunningBalancex<=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [60+ days (>60 days)] = @RunningBalancex * -1 WHERE [Counter] = @xCounter 
		  END
		 END
	   /* settle the [60 days (45-60 days)] invoice  */
	      IF(@CurrentBalance4 > 0  AND @RunningBalancex > 0)
		 BEGIN
		  SET @RunningBalancex = @RunningBalancex - @CurrentBalance4
		  
		  IF(@RunningBalancex>=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [60 days (45-60 days)] = 0 WHERE [Counter] = @xCounter 
		  END
		  IF(@RunningBalancex<=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [60 days (45-60 days)] = @RunningBalancex * -1 WHERE [Counter] = @xCounter 
		  END
		 END
	   /* settle the [45 days (31-45 days)] invoice  */
	      IF(@CurrentBalance3 > 0  AND @RunningBalancex > 0)
		 BEGIN
		  SET @RunningBalancex = @RunningBalancex - @CurrentBalance3
		  
		  IF(@RunningBalancex>=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [45 days (31-45 days)] = 0 WHERE [Counter] = @xCounter 
		  END
		  IF(@RunningBalancex<=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [45 days (31-45 days)] = @RunningBalancex * -1 WHERE [Counter] = @xCounter 
		  END
		 END
	   /* settle the [30 days (16-30 days)] invoice  */
	      IF(@CurrentBalance2 > 0  AND @RunningBalancex > 0)
		 BEGIN
		  SET @RunningBalancex = @RunningBalancex - @CurrentBalance2
		  
		  IF(@RunningBalancex>=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [30 days (16-30 days)] = 0 WHERE [Counter] = @xCounter 
		  END
		  IF(@RunningBalancex<=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [30 days (16-30 days)] = @RunningBalancex * -1 WHERE [Counter] = @xCounter 
		  END
		 END
	   /* settle the [<Current (<15 days)] invoice  */
	      IF(@CurrentBalance1 > 0  AND @RunningBalancex > 0)
		 BEGIN
		 PRINT 'RUNIING BAL: '  + CAST(@RunningBalancex AS VARCHAR)
		 	 PRINT 'Current Bal: ' + cast(@CurrentBalance1 as varchar)
		  SET @RunningBalancex = @RunningBalancex - @CurrentBalance1
		  
		  IF(@RunningBalancex>=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [<Current (<15 days)] = 0 WHERE [Counter] = @xCounter 
		  END
		  IF(@RunningBalancex<=0)
		  BEGIN
		  UPDATE @ReceivableTable SET [<Current (<15 days)] = @RunningBalancex * -1 WHERE [Counter] = @xCounter 
		  END
		 END


	    END

    SET @JobRecordIDxx = @JobRecordIDx
    SET @xCounter = @xCounter + 1
    END


    --SELECT * FROM @ReceivableTable

    INSERT INTO @ReceivableTablePreFinal(
		  JobRecordID
		  ,JobNumber
		  ,[Client Name]
		  ,JobTitle
		  --,OverDue
		  --,[Current] /* Removed, user <15 days */
		     , [<Current (<15 days)]    
	    , [Date (<15 days)]
	   , [30 days (16-30 days)]    
	    , [Date (16-30 days)]   
	   , [45 days (31-45 days)]   
	    , [Date (31-45 days)]    
	   , [60 days (45-60 days)]    
	    , [Date (45-60 days)]   
	   , [60+ days (>60 days)]      
	    , [Date (>60 days)]   
		  ,Balance
		  ,Currency
		  , [Group Name]
		  ,IsConverted)
    SELECT  JobRecordID
		  ,JobNumber
		  ,[Client Name]
		  ,JobTitle
		  --,Round(Sum(isnull(OverDue,0)),2) AS [OVERDUE]
		  --,Round(SUM(isnull([Current],0)),2) AS [CURRENT] /* Removed, user <15 days */
		     --, CASE WHEN Round(SUM(isnull([<Current (<15 days)],0)),2) < 0 THEN 0 ELSE Round(SUM(isnull([<Current (<15 days)],0)),2) END AS  [<Current (<15 days)]    
				     , Round(SUM(isnull([<Current (<15 days)],0)),2)  AS  [<Current (<15 days)]    
			 , CASE WHEN Round(SUM(isnull([<Current (<15 days)],0)),2) < 0 THEN Getdate() ELSE max([Date (<15 days)]) end  
			 
			 ,--CASE WHEN 
			 --CASE WHEN Round(SUM(isnull([30 days (16-30 days)],0)),2) < 0 THEN 0 ELSE Round(SUM(isnull([30 days (16-30 days)],0)),2) END 
			  Round(SUM(isnull([30 days (16-30 days)],0)),2)  
			 --+ 
			   -- CASE WHEN Round(SUM(isnull([<Current (<15 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([<Current (<15 days)],0)),2) END < 0 THEN 0 ELSE 
			    --CASE WHEN Round(SUM(isnull([30 days (16-30 days)],0)),2) < 0 THEN 0 ELSE Round(SUM(isnull([30 days (16-30 days)],0)),2) END + 
			    --CASE WHEN Round(SUM(isnull([<Current (<15 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([<Current (<15 days)],0)),2) END END
						  AS [30 days (16-30 days)]    
			  , CASE WHEN Round(SUM(isnull([30 days (16-30 days)],0)),2) < 0 THEN Getdate() ELSE max([Date (16-30 days)]) end    
			 
			 ,--CASE WHEN  
			 --CASE WHEN Round(SUM(isnull([45 days (31-45 days)],0)),2) < 0 THEN 0 ELSE Round(SUM(isnull([45 days (31-45 days)],0)),2) END 
			  Round(SUM(isnull([45 days (31-45 days)],0)),2)  
			 --+
				--	   CASE WHEN Round(SUM(isnull([<Current (<15 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([<Current (<15 days)],0)),2) END + 
				--	   CASE WHEN Round(SUM(isnull([30 days (16-30 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([30 days (16-30 days)],0)),2) END 
				--	   < 0 THEN 0 ELSE 
				--	    CASE WHEN Round(SUM(isnull([45 days (31-45 days)],0)),2) < 0 THEN 0 ELSE Round(SUM(isnull([45 days (31-45 days)],0)),2) END +
				--	   CASE WHEN Round(SUM(isnull([<Current (<15 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([<Current (<15 days)],0)),2) END + 
				--	   CASE WHEN Round(SUM(isnull([30 days (16-30 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([30 days (16-30 days)],0)),2) END END
						  AS [45 days (31-45 days)]     
			  , CASE WHEN Round(SUM(isnull([45 days (31-45 days)],0)),2) < 0 THEN Getdate() ELSE  max([Date (31-45 days)]) end   
			 
			 , --CASE	   WHEN 
			 --CASE WHEN Round(SUM(isnull([60 days (45-60 days)],0)),2) < 0 THEN 0 ELSE Round(SUM(isnull([60 days (45-60 days)],0)),2) END 
			  Round(SUM(isnull([60 days (45-60 days)],0)),2)  
			 --+ 
				--	   CASE WHEN Round(SUM(isnull([<Current (<15 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([<Current (<15 days)],0)),2) END + 
				--	   CASE WHEN Round(SUM(isnull([30 days (16-30 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([30 days (16-30 days)],0)),2) END + 
				--	   CASE WHEN Round(SUM(isnull([45 days (31-45 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([45 days (31-45 days)],0)),2) END 
			 --< 0 THEN 0 
				--ELSE CASE WHEN Round(SUM(isnull([60 days (45-60 days)],0)),2) < 0 THEN 0 ELSE Round(SUM(isnull([60 days (45-60 days)],0)),2) END + 
				--	   CASE WHEN Round(SUM(isnull([<Current (<15 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([<Current (<15 days)],0)),2) END + 
				--	   CASE WHEN Round(SUM(isnull([30 days (16-30 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([30 days (16-30 days)],0)),2) END + 
				--	   CASE WHEN Round(SUM(isnull([45 days (31-45 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([45 days (31-45 days)],0)),2) END END
						 AS [60 days (45-60 days)]			 

			  , CASE WHEN Round(SUM(isnull([60 days (45-60 days)],0)),2) < 0 THEN Getdate() ELSE max([Date (45-60 days)]) end        
			
			 ,--CASE WHEN 
			 --CASE WHEN Round(SUM(isnull([60+ days (>60 days)],0)),2) < 0 THEN 0 ELSE Round(SUM(isnull([60+ days (>60 days)],0)),2) END 
			  Round(SUM(isnull([60+ days (>60 days)],0)),2)  
			 --+ 
				--	   CASE WHEN Round(SUM(isnull([<Current (<15 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([<Current (<15 days)],0)),2) END + 
				--	   CASE WHEN Round(SUM(isnull([30 days (16-30 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([30 days (16-30 days)],0)),2) END + 
				--	   CASE WHEN Round(SUM(isnull([45 days (31-45 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([45 days (31-45 days)],0)),2) END +
				--	     CASE WHEN Round(SUM(isnull([60 days (45-60 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([60 days (45-60 days)],0)),2) END
				--		 < 0 THEN 0 ELSE
				--		CASE WHEN Round(SUM(isnull([60+ days (>60 days)],0)),2) < 0 THEN 0 ELSE Round(SUM(isnull([60+ days (>60 days)],0)),2) END + 
				--	   CASE WHEN Round(SUM(isnull([<Current (<15 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([<Current (<15 days)],0)),2) END + 
				--	   CASE WHEN Round(SUM(isnull([30 days (16-30 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([30 days (16-30 days)],0)),2) END + 
				--	   CASE WHEN Round(SUM(isnull([45 days (31-45 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([45 days (31-45 days)],0)),2) END +
				--	     CASE WHEN Round(SUM(isnull([60 days (45-60 days)],0)),2) > 0 THEN 0 ELSE Round(SUM(isnull([60 days (45-60 days)],0)),2) END  END
					    AS [60+ days (>60 days)]  			  
			  , CASE WHEN Round(SUM(isnull([60+ days (>60 days)],0)),2) < 0 THEN Getdate() ELSE max([Date (>60 days)]) end 
		  ,Round((
			 SUM(isnull([<Current (<15 days)],0)) +
			 SUM(isnull([30 days (16-30 days)],0)) +
			 SUM(isnull([45 days (31-45 days)],0)) +
			 SUM(isnull([60 days (45-60 days)],0)) + 
			 --SUM(isnull([60 days (45-60 days)],0)) +
			 SUM(isnull([60+ days (>60 days)],0))
		  ),2) as Balance,Currency,[Group Name]
		  ,IsConverted
    FROM @ReceivableTable 
    Group By JobRecordID,JobNumber,JobTitle,currency, IsConverted, [Group Name],[Client Name]
    ORDER BY JobRecordID DESC

    /*SHOW FINAL OUTPUT*/

    	
DECLARE @ReportStyle bit = 0

IF ((SELECT V005 FROM [RECORD] WHERE TABLEID = 2743) = 'Yes')
BEGIN
SET @ReportStyle = 1
END




DECLARE @Counter int = 1
--SELECT * FROM @ReceivableTablePreFinal
DECLARE @TableInvoices Table (iCounter int identity(1,1),InvoiceDate date,InvoiceAmount money )
WHILE EXISTS(SELECT * FROM @ReceivableTablePreFinal WHERE [Counter] >= @Counter)
BEGIN

DECLARE @JobRecordID int, @Outstanding money, @RunningBalance money = 0.00

SELECT @JobRecordID = JobRecordID, @Outstanding = Balance FROM @ReceivableTablePreFinal WHERE [Counter] = @Counter

DELETE FROM @TableInvoices
INSERT INTO @TableInvoices (InvoiceDate, InvoiceAmount)
    SELECT cast(BAPA.V004 as date),  Round(CAST(Round(convert(varchar(max),cast(ISNULL(BAPA.V005,0) AS MONEY)),2) / 
			  CONVERT(MONEY,ISNULL(JOBINFO.V007,1)) * CONVERT(MONEY,ISNULL(JOBINFO.V047,1)) AS MONEY),2 ) 
    FROM RECORD BAPA
    JOIN RECORD JOBINFO ON JOBINFO.TABLEID = 2475
    AND JOBINFO.ISACTIVE = 1
    AND JOBINFO.RECORDID = BAPA.V002
    WHERE BAPA.TABLEID = 2711
    AND BAPA.ISACTIVE = 1
    AND BAPA.V002 = @JobRecordID
    AND BAPA.V029 like '%Invoice%'
    ORDER BY CAST(BAPA.V004 as date) desc
   
   
    DECLARE @iCounter int = 1
     DECLARE @InvoiceDate date ,@CurrentBalance money = 0.00
	--if(@JobRecordID= 12953741)
	--begin
	--select * from @TableInvoices
	--select @Outstanding
	--end
  --select * from @TableInvoices
    WHILE EXISTS(SELECT * FROM @TableInvoices WHERE iCounter >= @iCounter)
    BEGIN
	  
	   SELECT @CurrentBalance = InvoiceAmount, @InvoiceDate = InvoiceDate FROM @TableInvoices WHERE iCounter = @iCounter
	    SET @RunningBalance = @RunningBalance + @CurrentBalance

	--    	if(@JobRecordID= 12953741)
	--begin
	--select @InvoiceDate,@CurrentBalance
	--end

	--SELECT @Outstanding,@RunningBalance

	     IF (@Outstanding <= @RunningBalance)
		  BEGIN
			 UPDATE @ReceivableTablePreFinal
			 SET [Invoice Date] = @InvoiceDate
			 WHERE JobRecordID = @JobRecordID
			 BREAK;
		  END
		
		  SET @iCounter = @iCounter + 1
    END
      


SET @Counter = @Counter + 1
END
--SELECT * FROM @ReceivableTablePreFinal
--ORDER BY JOBNUMBER
 -- WHERE Balance <> 0 AND JOBNUMBER = 'TG655-15'





/* lets show the report */

IF @ReportStyle =0
BEGIN
 SELECT *,
 	--(SELECT MIN(CAST(V004 AS DATE))  
	 --  FROM RECORD 
		--  WHERE TABLEID = 2711 
		--  AND V029 LIKE '%Invoice%' 
		--  AND V002 = JobRecordID
		--  AND
		-- -- CASE WHEN [60+ days (>60 days)] >0 
		--	 --THEN 
		--	 CAST(V004 as date) >=  DATEADD(day,-60, CONVERT(DATE,GETDATE(),103)) --ELSE END	  
		  
		--  ) AS [Invoice Date] ,
		
	  
	  
	   @ReportStyle AS [Report Style],
    	(SELECT V039  FROM RECORD WHERE TABLEID = 2743) AS [Header] 
	FROM @ReceivableTablePreFinal
    WHERE Balance > 0 --AND JOBNUMBER = 'WE873-01'
    ORDER BY JobNumber asc  
END
ELSE
BEGIN
    SELECT *,
   --(SELECT MIN(CAST(V004 AS DATE))  
	  -- FROM RECORD 
		 -- WHERE TABLEID = 2711 
		 -- AND V029 LIKE '%Invoice%' 
		 -- AND V002 = JobRecordID
		 -- AND
		 ---- CASE WHEN [60+ days (>60 days)] >0 
			-- --THEN 
			-- CAST(V004 as date) >= 
			-- DATEADD(day,-60, CONVERT(DATE,GETDATE(),103))	  
		  
		 -- ) AS [Invoice Date] ,
    @ReportStyle AS [Report Style],
    	(SELECT V039  FROM RECORD WHERE TABLEID = 2743) AS [Header] 
	FROM @ReceivableTablePreFinal
    WHERE Balance > 0
    ORDER BY [Group Name],JobNumber asc  
END

END