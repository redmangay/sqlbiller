﻿

CREATE PROCEDURE [dbo].[ets_Invoice_Select]
(
	@nAccountID int  = NULL,
	@sAccountName varchar(200)  = NULL,
	@nAccountTypeID int  = NULL,
	@dInvoiceDateFrom datetime  = NULL,
	@dInvoiceDateTo datetime  = NULL,
	@sPaymentMethod char(25)  = NULL,
	@bIsPaid bit  = NULL,
	@sOrganisationName varchar(200)  = NULL,
	@sBillingEmail varchar(200)  = NULL,
	@sBillingAddress varchar(MAX)  = NULL,
	@sCountry varchar(20)  = NULL,
	@sClientRef varchar(20)  = NULL,
	@dDateAdded datetime  = NULL,
	@dDateUpdated datetime  = NULL,
	@sOrder nvarchar(200) = DateAdded, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	 ets_Invoice_Select
	
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(4000)
	DECLARE @sSelectCount nvarchar(4000)

	DECLARE @sWhere nvarchar(4000)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND AccountID = '+CAST(@nAccountID AS NVARCHAR)
	IF @sAccountName IS NOT NULL 
		SET @sWhere = @sWhere + ' AND AccountName LIKE '+'''%' + @sAccountName + '%'''
	IF @nAccountTypeID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND AccountTypeID = '+CAST(@nAccountTypeID AS NVARCHAR)
	IF @dInvoiceDateFrom IS NOT NULL 
			SELECT @sWhere = @sWhere + ' AND InvoiceDate >= ''' + CONVERT(varchar(30), @dInvoiceDateFrom, 120) + ''''

	IF @dInvoiceDateTo IS NOT NULL 
			SELECT @sWhere = @sWhere + ' AND InvoiceDate <=  ''' + CONVERT(varchar(30), @dInvoiceDateTo, 120) + ''''
	IF @sPaymentMethod IS NOT NULL 
		SET @sWhere = @sWhere + ' AND PaymentMethod LIKE '+'''%' + @sPaymentMethod + '%'''
	IF @bIsPaid IS NOT NULL  
	BEGIN
		IF @bIsPaid=1
		SET @sWhere = @sWhere + ' AND PaidDate IS NOT NULL '
	
			IF @bIsPaid=0
		SET @sWhere = @sWhere + ' AND PaidDate IS NULL '		

	END
	IF @sOrganisationName IS NOT NULL 
		SET @sWhere = @sWhere + ' AND OrganisationName LIKE '+'''%' + @sOrganisationName + '%'''
	IF @sBillingEmail IS NOT NULL 
		SET @sWhere = @sWhere + ' AND BillingEmail LIKE '+'''%' + @sBillingEmail + '%'''
	IF @sBillingAddress IS NOT NULL 
		SET @sWhere = @sWhere + ' AND BillingAddress LIKE '+'''%' + @sBillingAddress + '%'''
	IF @sCountry IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Country LIKE '+'''%' + @sCountry + '%'''
	IF @sClientRef IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ClientRef LIKE '+'''%' + @sClientRef + '%'''
	IF @dDateAdded IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DateAdded = '+CAST(@dDateAdded AS NVARCHAR)
	IF @dDateUpdated IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DateUpdated = '+CAST(@dDateUpdated AS NVARCHAR)
	SET @sSelect = 'SELECT * FROM 
	(SELECT Invoice.*, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum FROM Invoice' + @sWhere + ') as UserInfo'
	 + ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)

	SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [Invoice] ' + @sWhere 
 
 
	EXEC (@sSelect)
	PRINT @sSelect

	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Invoice_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
