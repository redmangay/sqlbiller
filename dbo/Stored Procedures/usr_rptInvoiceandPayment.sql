﻿
 
CREATE Procedure [dbo].[usr_rptInvoiceandPayment]        
 @StartDate datetime   ,--='2010-04-05',   
 @EndDate  datetime, --='2015-04-09' 
 @daterange varchar(10), --1
 @month varchar(10)=null , --1
 @year varchar(10) =null   
AS        
BEGIN   

/*

[dbo].[usr_rptInvoiceandPayment] '01/01/2020', '31/01/2020','2','12','2019'


*/

Set dateformat dmy
    
	IF @daterange ='2'
	begin
	--set @StartDate =CAST(cast(YEAR(GETDATE()) as varchar(10)) + '-' + @month + '-01' AS DATETIME)	
	--SELECT	DATEADD(MONTH, 12 * @Year - 22800, 0) AS StartOfMonth,
	--DATEADD(MONTH, 12 * @Year - 22799, -1) AS EndOfMonth,
	--DATEADD(MONTH, 12 * @Year - 22799, 0) AS StartofNextMonth
	

	DECLARE @TODEDUCTMONTH varchar(10)
	SELECT @TODEDUCTMONTH =  month(GETDATE()) - @month
	SET @StartDate =  DATEADD(month,CONVERT(INT,@Month)-1,DATEADD(year,CONVERT(INT,@Year)-1900,0)) 
	SET @EndDate=	    DATEADD(day,-1,DATEADD(month,CONVERT(INT,@Month),DATEADD(year,CONVERT(INT,@Year)-1900,0)))
	print 'red'
	print @StartDate
	print @EndDate
	end


	
DECLARE @ReportStyle bit = 0

IF ((SELECT V005 FROM [Record] WHERE TableID = 2743) = 'Yes')
BEGIN
SET @ReportStyle = 1
END

	    

SELECT			--[Bill & Payment Activity].V005,
--[Bill & Payment Activity].V007,
           --     [Job Number].V007,
			-- [Job Number].V047,
			 [Job Currency Rates].V001 as [Currency Name],
			 [Job Group].V004,
				 CONVERT(date,[Bill & Payment Activity].V004)		AS				[Date]
				,[Job Number].V001		AS			   project_number
				,[Bill & Payment Activity].V039		AS				projecttitle
				--,isnull(CASE  when ISNUMERIC([Bill & Payment Activity].V005) =1 and cast([Bill & Payment Activity].V005 as money) >0 THEN  CAST([Bill & Payment Activity].V005 AS varchar(max))   ELSE cast (0.00 as money) END,'0') as Invoice
				,CASE WHEN [Bill & Payment Activity].V029 IN ('Interim Credit','Credit Entry') THEN 0 ELSE CASE WHEN [Job Currency Rates].V001 = [Job Group].V004 THEN CAST(ISNULL([Bill & Payment Activity].V005,0.00) AS money) ELSE
				ISNULL(CASE WHEN /*ISNUMERIC([Bill & Payment Activity].V005) =1 and */ CAST(ISNULL([Bill & Payment Activity].V005,0.00) AS money) >0 THEN  CAST((convert(money,ISNULL([Bill & Payment Activity].V005,0.00))/CONVERT(money,ISNULL([Job Number].V007,1)) * CONVERT(money,ISNULL([Job Number].V047,1))) AS varchar(max)) ELSE CAST(0.00 AS money) END,'0') END END AS Invoice
				--,isnull(Case when Isnumeric([Bill & Payment Activity].V007)=1 and  cast([Bill & Payment Activity].V007 as money) >0 	Then	CAST([Bill & Payment Activity].V007 AS varchar(max))   ELSE cast (0.00 as money) END , '0')as Payment
								
				,CASE WHEN [Bill & Payment Activity].V029 NOT IN ('Bad Debt Entry','Bad Debt Recovery Entry') THEN	
				 CASE WHEN [Job Currency Rates].V001 = [Job Group].V004 THEN CAST(ISNULL([Bill & Payment Activity].V007,0.00) AS money) ELSE
				ISNULL(CASE WHEN /*ISNUMERIC([Bill & Payment Activity].V007)=1 and  */ CAST(ISNULL([Bill & Payment Activity].V007,0.00) AS money) > 0 	THEN  CAST((CONVERT(money,ISNULL([Bill & Payment Activity].V007,0.00))/ CONVERT(money,ISNULL([Job Number].V007,1)) * CONVERT(money,ISNULL([Job Number].V047,1))) AS varchar(max))  ELSE CAST(0.00 AS money) END , '0')
				
				END ELSE CAST(0 AS money) END AS Payment


				--,isnull(CASE  when ISNUMERIC([Bill & Payment Activity].V045) =1 and cast([Bill & Payment Activity].V045 as money) >0 THEN CAST([Bill & Payment Activity].V045 AS varchar(max))   ELSE   cast([Bill & Payment Activity].V045 as money) END,'0') as Credit
				,CASE WHEN [Bill & Payment Activity].V029 NOT IN ('Interim Credit','Credit Entry') THEN 0 ELSE CASE WHEN [Job Currency Rates].V001 = [Job Group].V004 THEN  CAST(ISNULL([Bill & Payment Activity].V045,0.00) AS money) ELSE
				ISNULL(CASE WHEN ISNUMERIC([Bill & Payment Activity].V045) =1 and CAST(ISNULL([Bill & Payment Activity].V045,0.00) AS money) >0 THEN  CAST((CONVERT(money,ISNULL([Bill & Payment Activity].V045,0.00))/CONVERT(money,ISNULL([Job Number].V007,1)) * CONVERT(money,ISNULL([Job Number].V047,1))) AS varchar(max))   ELSE   CAST(ISNULL([Bill & Payment Activity].V045,0.00) AS money) END,'0') END END AS Credit
				--,isnull(cast([Bill & Payment Activity].V014 as money),'0')		AS				BadDebt
				,CASE WHEN [Bill & Payment Activity].V029 = 'Bad Debt Entry' THEN
				CASE WHEN [Job Currency Rates].V001 = [Job Group].V004 THEN CAST(ISNULL([Bill & Payment Activity].V007,0.00) AS money) ELSE				
				ISNULL(CASE WHEN ISNUMERIC([Bill & Payment Activity].V007) =1 and CAST(ISNULL([Bill & Payment Activity].V007,0.00) AS money) >0 THEN  CAST((CONVERT(money,ISNULL([Bill & Payment Activity].V007,0.00))/CONVERT(money,ISNULL([Job Number].V007,1)) * CONVERT(money,ISNULL([Job Number].V047,1))) AS varchar(max))   ELSE   CAST(ISNULL([Bill & Payment Activity].V007,0.00) AS money) END,'0') 
				END ELSE cast(0 AS money) END AS BadDebt
				--,isnull(CAST((convert(money,[Bill & Payment Activity].V014)/convert(money,[Job Number].V007) * convert(money,[Currency Rates].V002)) AS varchar(max)),'0')		AS				BadDebt
				,ISNULL(CAST([Currency Rates].V002 AS money),'0')		AS				cCurrencyRate
				,[Job Group].V001		AS				GroupDivision
				--,isnull(cast([Bill & Payment Activity].V021 as money)	,'0')	AS				TaxAmount
				,
				
				CASE WHEN [Job Currency Rates].V001 = [Job Group].V004 THEN CONVERT(money,[Bill & Payment Activity].V063) ELSE
				ISNULL(CAST((CONVERT(money,[Bill & Payment Activity].V063)/CONVERT(money,ISNULL([Job Number].V007,1)) * CONVERT(money,ISNULL([Job Number].V047,1))) AS varchar(max))	,'0') END	AS	TaxAmount			
				
				
				--,isnull(CASE  when ISNUMERIC([Bill & Payment Activity].V063) =1 and cast([Bill & Payment Activity].V063 as money) >0 THEN  CAST([Bill & Payment Activity].V063 AS varchar(max))   ELSE cast (0 as money) END,'0') as InvoiceTax 
				--,CASE WHEN [Bill & Payment Activity].V029 LIKE '%Invoice%' THEN ISNULL(CASE  WHEN ISNUMERIC([Bill & Payment Activity].V063) =1 and CAST([Bill & Payment Activity].V063 AS money) >0 THEN  CAST((CONVERT(money,[Bill & Payment Activity].V063)/CONVERT(money,ISNULL([Job Number].V007,1)) * CONVERT(money,ISNULL([Job Number].V047,1))) AS varchar(max))   ELSE cast (0 AS money) END,'0') ELSE 0.00 END AS InvoiceTax  
				,CASE 
				    WHEN [Bill & Payment Activity].V029 LIKE '%Invoice%' THEN
				    CASE WHEN [Job Currency Rates].V001 = [Job Group].V004 THEN 
				    CONVERT(money,[Bill & Payment Activity].V063) ELSE
				 ISNULL(CAST((CONVERT(money,[Bill & Payment Activity].V063)/CONVERT(money,ISNULL([Job Number].V007,1)) * CONVERT(money,ISNULL([Job Number].V047,1))) AS varchar(max))	,'0') END ELSE cast (0 AS money) END  AS InvoiceTax  
				--,isnull(CASE  when ISNUMERIC([Bill & Payment Activity].V047) =1 and cast([Bill & Payment Activity].V047 as money) >=0 THEN  CAST([Bill & Payment Activity].V047 AS varchar(max))   ELSE   cast([Bill & Payment Activity].V047 as money) END,'0') as CreditTax
				,CASE WHEN [Job Currency Rates].V001 = [Job Group].V004 THEN CONVERT(money,ISNULL([Bill & Payment Activity].V047,0)) ELSE
				CASE WHEN [Bill & Payment Activity].V029 LIKE '%Credit%' THEN ISNULL(CASE  WHEN ISNUMERIC([Bill & Payment Activity].V047) =1 and CAST([Bill & Payment Activity].V047 AS money) >=0 THEN  CAST((CONVERT(money,ISNULL([Bill & Payment Activity].V047,0))/CONVERT(money,ISNULL([Job Number].V007,1)) * CONVERT(money,ISNULL([Job Number].V047,1))) AS varchar(max))    ELSE   cast(ISNULL([Bill & Payment Activity].V047,0.00) AS money) END,'0')  ELSE 0.00 END  END AS CreditTax
                ,CASE WHEN [Bill & Payment Activity].V029  IN ('Bad Debt Entry','Bad Debt Recovery Entry') THEN
			 CASE WHEN [Job Currency Rates].V001 = [Job Group].V004 THEN CONVERT(money,[Bill & Payment Activity].V063) ELSE	
			 ISNULL(CASE  WHEN ISNUMERIC([Bill & Payment Activity].V063) =1 and CAST(ISNULL([Bill & Payment Activity].V063,0.00) AS money) >=0 then CAST((CONVERT(money,[Bill & Payment Activity].V063)/CONVERT(money,ISNULL([Job Number].V007,1)) * CONVERT(money,ISNULL([Job Number].V047,1))) AS varchar(max))   ELSE cast (ISNULL([Bill & Payment Activity].V063,0)  AS money) END,'0') 
			 END ELSE CAST(0 AS money) END AS BadDebtTax	
			 
  ,ISNULL([Job Group].V004,'AUD') AS [Reporting Currency]
  ,CONVERT(date,@StartDate,103) AS [Start Date],CONVERT(date,@EndDate,103) AS [End Date]
  ,[Bill & Payment Activity].V014
  ,CAST (ISNULL([Bill & Payment Activity].V014,0)  AS money) AS [Bad Debt Exp Converted]
 -- ,[Job Currency Rates].V001 AS JOBCURRENCY
  --,[Job Group].V004 AS REPORTINGCURRENCY
  ,CASE WHEN [Job Currency Rates].V001 = [Job Group].V004 THEN NULL
  WHEN [Job Currency Rates].V001 = '(None)' AND [Job Group].V004 IS NULL THEN NULL 
   ELSE CONVERT(varchar,'C') END AS [Converted Symbol]
  ,@ReportStyle AS [Report Style]
   , 	(SELECT V039  FROM [Record] WHERE TableID = 2743) AS [Header] 
   ,ISNULL([Currency Rates].V003,'$') as [Symbol]
 -- ,[Job Number].V007 as JobCur
--  ,[Currency Rates].V002  as RepCur
 -- ,isnull(CASE  when ISNUMERIC([Bill & Payment Activity].V005) =1 and cast([Bill & Payment Activity].V005 as money) >0 THEN  CAST([Bill & Payment Activity].V005 AS varchar(max))   ELSE cast (0.00 as money) END,'0') as TempInvoice
				
	FROM		[Record] [Bill & Payment Activity] WITH (NOLOCK)
	JOIN	Record [Job Number] ON					[Job Number].TableID = '2475'		/*	Job Information	*/
											AND		[Job Number].IsActive = 1
											AND		CAST( [Job Number].RecordID AS VARCHAR(MAX)) = [Bill & Payment Activity].V002		/*	Job Number	*/
											--AND [Job Number].v001 ='we453-01'
											--AND	    [Job Number].V039 IS NOT NULL

	
	--LEFT JOIN Record [Group] WITH (NOLOCK) on 		[Group].TableID = '2727'		/*	Group	*/
	--												AND			[Group].IsActive = 1
	--												AND		CAST( [Currency Rates].RecordID AS VARCHAR(MAX)) = [Group].V005		/*	Currency	*/

	 LEFT JOIN Record [Job Group] WITH (NOLOCK) on 		    [Job Group].TableID = '2727'		/*	Group	*/
													AND			[Job Group].IsActive = 1
													AND		CAST( [Job Group].RecordID AS VARCHAR(MAX)) = [Job Number].V027		/*	Currency	*/
													--AND  [Job Group].V001 = 'Windtech (UK)'
													
	LEFT JOIN Record [Currency Rates] WITH (NOLOCK) on 		[Currency Rates].TableID = '2665'		/*	Currency Rates	*/
														AND			[Currency Rates].IsActive = 1
														AND	CAST( [Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/

     LEFT JOIN Record [Job Currency Rates] WITH (NOLOCK) on 		[Job Currency Rates].TableID = '2665'		/*	Currency Name	*/
														AND			[Job Currency Rates].IsActive = 1
														AND	CAST( [Job Currency Rates].RecordID AS VARCHAR(MAX)) = [Job Number].V034		/*	[Group Currency]	*/

	
	

	WHERE		[Bill & Payment Activity].TableID = '2711'		/*	Bill & Payment Activity	*/
	AND			[Bill & Payment Activity].IsActive = 1
	AND			[Bill & Payment Activity].V029 <> 'Overpaid Entry'
	AND	    [Job Number].V027 IS NOT NULL
				
	--AND [Bill & Payment Activity].V018 in (1,2,3,4,6,8)  -- LEGACY SYSTEM
 
    
--AND cast([Bill & Payment Activity].V004 as date) >= cast('01/01/2015' as date) AND  cast([Bill & Payment Activity].V004 as date) <= cast('31/08/2016' as date)     
AND CONVERT(varchar(20),CONVERT(DATE,[Bill & Payment Activity].V004),103) >= @StartDate AND CONVERT(VARCHAR(20),CONVERT(DATE,[Bill & Payment Activity].V004),103)  <= @EndDate    
ORDER BY CONVERT(date,[Bill & Payment Activity].V004) ASC, [Job Number].V001 ASC

END
GO


