﻿
CREATE PROCEDURE	[dbo].[Renzo_DisableGroupReportCurrency]
(
	@RecordID  int = null,
	@FormSetID int = null,
     @UserID int=null,
     @Return varchar(max) =null output
)
AS
BEGIN

/*

EXEC [Renzo_DisableGroupReportCurrency]  @RecordID=2715879

*/
	

SELECT [Job Information].RecordID
    FROM [RECORD] [Job Information]
		  INNER JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
				AND [Job Group].IsActive = 1
				AND CAST( [Job Group].RecordID AS VARCHAR(MAX)) = [Job Information].V027		/*	Currency	*/
				AND [Job Group].RecordID =  @RecordID
    WHERE [Job Information].TableID = 2475
    AND [Job Information].IsActive = 1


END

/**************************************************/
