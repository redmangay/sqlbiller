﻿

CREATE PROCEDURE [dbo].[ets_Chart_GetData_MeanMaxMin]
(
	-- Add the parameters for the stored procedure here
	@nTableID int,
	@sValueFields varchar(MAX),
	@dStartDate datetime,
	@dEndDate datetime,
	@sPeriod varchar(20),
	@nMaxRows int = NULL
	-- Possible values: Year, Month, Day, Hour
	-- When you pass Year it will return the month averages
	-- when you pass Month it will return the day averages
	-- when you pass Week it will return the day averages
	-- when you pass day it will return hour aveages
	-- when you pass hour it will return all data
)
AS
 
/*
 
       PRINT SYSDATETIME()
 
       EXEC [ets_Chart_GetData_MeanMaxMin]
 
               @nTableID=49
              ,@sValueFields = 'V001'
              ,@dStartDate='2010-12-14 13:00'
              ,@dEndDate='2013-12-14 14:00'
              ,@sPeriod='Year'
 
            
 
       PRINT SYSDATETIME()
 
       -- 3603515 1st run
       -- 2919922 2nd run : no IF
       -- 2529297 3rd run : using IN
 
*/
 
 
 
BEGIN TRY
 
	SET NOCOUNT ON;
    DECLARE @Sql varchar(MAX)       
    DECLARE @sDateTime varchar(100)
    DECLARE @sSeriesAvg varchar(MAX), @sSeriesMax varchar(MAX), @sSeriesMin varchar(MAX)
 
	DECLARE @bIgnoreSymbols bit=0
	SELECT @bIgnoreSymbols=IgnoreSymbols FROM [Column] WHERE SystemName=@sValueFields AND TableID=@nTableID 
 
	IF (@bIgnoreSymbols is not NULL and @bIgnoreSymbols=1)
		SET @sValueFields='dbo.RemoveNonNumericChar('+@sValueFields+')' 
		IF(LEN(@sPeriod) = 0) OR @sPeriod = 'Hour'  -- then we do not reduce the data
		BEGIN
			IF @nMaxRows IS NOT NULL
				SET @sql = ' SELECT TOP ' + CAST(@nMaxRows as varchar) + '  DateTimeRecorded,'
			ELSE
				SET @sql = ' SELECT  DateTimeRecorded,'
              
			SET @sql = @sql + @sValueFields + ' AS Mean, ' + @sValueFields + ' AS MAX,' + @sValueFields + ' AS Min' +
				' FROM [Record]' +
				' WHERE IsActive=1 AND DateTimeRecorded >= ''' + CONVERT(varchar(20), @dStartDate, 20) + ''' AND  DateTimeRecorded <= ''' + CONVERT(varchar(20), @dEndDate, 20) + '''' +
				' AND TableID = ' + CAST(@nTableID AS Varchar) +
				' ORDER BY  DateTimeRecorded DESC'
       END   
       ELSE -- we are going to reduce the data
       BEGIN
			-- Convert the fields to averages
			SET  @sSeriesAvg = 'AVG(CAST(' + @sValueFields + ' AS decimal(18,4))) '
			SET  @sSeriesMax = 'MAX(CAST(' + @sValueFields + ' AS decimal(18,4))) '
			SET  @sSeriesMin = 'MIN(CAST(' + @sValueFields + ' AS decimal(18,4))) '
 
            IF @sPeriod = 'Year' SET @sDateTime = 'CAST(CONVERT(varchar(07),DateTimeRecorded,20) + ''-01'' AS datetime)'
            IF @sPeriod = 'Month' SET @sDateTime = 'CAST(CONVERT(varchar(10),DateTimeRecorded,20) AS datetime)'
            IF @sPeriod = 'Week' SET @sDateTime = 'CAST(CONVERT(varchar(10),DateTimeRecorded,20) AS datetime)'
            IF @sPeriod = 'Day' SET @sDateTime = 'CAST(CONVERT(varchar(13),DateTimeRecorded,20) + '':00'' AS datetime)' 
 
            SET @sql =    ' SELECT ' + @sDateTime + 'AS [DateTimeRecorded], ' + @sSeriesAvg + ' AS [Mean],' +
					@sSeriesMax + ' AS [MAX], ' + @sSeriesMin + ' AS [Min] ' +
					' FROM [Record]' +
					' WHERE IsActive=1 AND DateTimeRecorded >= ''' + CONVERT(varchar(20), @dStartDate, 20) + ''' AND  DateTimeRecorded <= ''' + CONVERT(varchar(20), @dEndDate, 20) + '''' +
					' AND TableID = ' + CAST(@nTableID AS Varchar(20)) +
					' AND ISNUMERIC(' + @sValueFields + ') = 1' +
					' GROUP BY  ' + @sDateTime +
					' ORDER BY  ' + @sDateTime
       END
       PRINT @sql
       EXEC(@sql)
 
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Chart_GetData_MeanMaxMin', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
