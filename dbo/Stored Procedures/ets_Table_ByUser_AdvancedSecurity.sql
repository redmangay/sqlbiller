﻿

CREATE PROCEDURE [dbo].[ets_Table_ByUser_AdvancedSecurity]
(
	@nRoleID INT	
)
AS

--@nUserID int, @sRecordRightIDs varchar(100)
--EXEC [dbo].[ets_Table_ByUser_AdvancedSecurity] 66



BEGIN TRY


	SELECT     Distinct TableID 
			FROM         RoleTable
			WHERE     (RoleID = @nRoleID ) AND (RoleType<>6)

	--DECLARE @sSelect nvarchar(MAX)

	--DECLARE @nRoleGroupID INT=NULL

	--SELECT @nRoleGroupID=RoleGroupID FROM [User] WHERE UserID=@nUserID;

	--IF @nRoleGroupID IS NULL	
		--BEGIN
			--SET @sSelect='SELECT     Distinct TableID 
			--FROM         UserTable
			--WHERE     (UserID = '+CAST(@nUserID AS NVARCHAR)+') AND (RecordRightID IN('+@sRecordRightIDs+'))'
		--END

	--IF @nRoleGroupID IS NOT NULL	
	--	BEGIN
	--		SET @sSelect='SELECT     Distinct TableID 
	--		FROM         RoleGroupTable
	--		WHERE     (RoleGroupID = '+CAST(@nRoleGroupID AS NVARCHAR)+') AND (RecordRightID IN('+@sRecordRightIDs+'))'
	--	END
	

		--SET @sSelect='SELECT     Distinct TableID 
		--	FROM         RoleTable
		--	WHERE     (RoleID = '+CAST(@nRoleID AS NVARCHAR)+') AND (RoleType<>6)'


	--EXEC (@sSelect)
	--SET ROWCOUNT 0


END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Table_ByUser_AdvancedSecurity', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
