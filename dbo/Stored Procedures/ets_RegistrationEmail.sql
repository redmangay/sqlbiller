﻿

CREATE PROCEDURE   [dbo].[ets_RegistrationEmail]
(
      @sKey varchar(10) = NULL
)

AS
/*

      UNIT TESTING
      ============
      EXEC dbo.ets_RegistrationEmail
      EXEC dbo.ets_RegistrationEmail @sKey = 6

*/

BEGIN TRY

	DECLARE @nUserID int

	IF @sKey IS NULL
	BEGIN
		PRINT 'NULL'
		SELECT @nUserID = MAX(UserID) FROM [User]
				WHERE IsActive=1
	END
	ELSE
		SELECT @nUserID = CAST(@sKey AS int)

	PRINT '@nUserID = ' + CAST(@nUserID  AS varchar)
	SELECT	[Account].AccountName, AccountType.AccountTypeName, AccountType.MaxTotalRecords, [User].FirstName +' ' + [User].LastName as [Full Name], [User].PhoneNumber, [User].Email,
			--[User].[Password],  -- Red removed this for Ticket 3059
			UserRole.IsAccountHolder, UserRole.IsAdvancedSecurity,'' as [URL]
		FROM [User] INNER JOIN UserRole ON [User].UserID=UserRole.UserID 
		INNER JOIN Account ON UserRole.AccountID=Account.AccountID 
		INNER JOIN AccountType ON Account.AccountTypeID = AccountType.AccountTypeID
		WHERE [User].UserID = @nUserID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_RegistrationEmail', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
