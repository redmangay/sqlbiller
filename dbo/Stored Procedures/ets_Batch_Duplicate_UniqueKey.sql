﻿

CREATE PROCEDURE [dbo].[ets_Batch_Duplicate_UniqueKey]
(
	@nBatchID int,
	@bDebug bit = 0
)
AS

BEGIN TRY
	DECLARE @TimeStamp datetime = GETDATE()
 
	 -- Added by JB on 25 Aug 2016
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	--Need duplicate check

	IF EXISTS(SELECT TOP 1 T.TableID FROM [Table] T JOIN Batch B ON T.TableID=B.TableID WHERE B.BatchID=@nBatchID AND T.UniqueColumnID IS NULL AND T.UniqueColumnID2 IS NULL)
		RETURN;


	-- Ensure that the UniqueKey is set
	IF EXISTS(SELECT TOP 1 RecordID FROM [TempRecord] WHERE BatchID = @nBatchID AND UniqueKey IS NULL)
		EXEC [Record_Set_UniqueKey] @sUpdateRecord='T', @nBatchID=@nBatchID

	UPDATE TempRecord 
		SET RejectReason = ISNULL(TempRecord.RejectReason,'') 
			+ ' DUPLICATE Record in this batch!',UpdateRecord=0
		FROM TempRecord 
		JOIN TempRecord T2 ON T2.[UniqueKey] =
 			TempRecord.[UniqueKey] 
			AND T2.RecordID < TempRecord.RecordID
		WHERE TempRecord.BatchID=@nBatchID AND T2.BatchID=@nBatchID


	DECLARE @sDuplicateRecordAction varchar(50)=NULL,@nTableID INT
	SELECT @sDuplicateRecordAction=T.DuplicateRecordAction,@nTableID=T.TableID FROM Batch B JOIN [Table] T ON B.TableID=T.TableID WHERE B.BatchID=@nBatchID

	IF EXISTS(SELECT TOP 1 RecordID FROM [Record] WHERE TableID = @nTableID AND UniqueKey IS NULL)
		EXEC [Record_Set_UniqueKey] @sUpdateRecord='R', @nTableID=@nTableID



	IF @sDuplicateRecordAction IS NULL OR @sDuplicateRecordAction='Reject Duplicates'
		BEGIN
			UPDATE TempRecord 
				SET RejectReason=ISNULL(TempRecord.RejectReason,'') 
					+ ' DUPLICATE Record!'
				FROM TempRecord 
				JOIN Record ON Record.[UniqueKey] = 
					TempRecord.[UniqueKey] 
				WHERE TempRecord.BatchID=@nBatchID AND Record.IsActive=1 AND TempRecord.UpdateRecord IS NULL
		END
	ELSE
		BEGIN

			UPDATE TempRecord 
				SET UpdateRecord=1
				FROM TempRecord 
				JOIN Record ON Record.[UniqueKey] = 
					TempRecord.[UniqueKey] 
				WHERE TempRecord.BatchID=@nBatchID   AND Record.IsActive=1 AND  TempRecord.UpdateRecord IS NULL
		END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Batch_Duplicate_UniqueKey', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
