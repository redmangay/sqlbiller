﻿CREATE PROCEDURE		[dbo].[renzo_ButtonSetRatesToDefault]
(
	@RecordID  int,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output
)
AS
BEGIN

	/*
					Reset the Job Rates to the Master Rates

					DECLARE		@Return		VARCHAR(MAX)
					EXEC		[renzo_ButtonSetRatesToDefault] @RecordID = 1497124, @Return = @Return

					EXEC		[dbo].[dev_GetAccountSQL] @TableID = 2477, @TableName = 'Designation Rate', @AccountID = '24918'
					EXEC		[dbo].[dev_GetAccountSQL] @TableID = 2478, @TableName = 'Disbursement Rate', @AccountID = '24918'
					EXEC		[dbo].[dev_GetAccountSQL] @TableID = 2475, @TableName = 'Job Information', @AccountID = '24918'

					EXEC		[dbo].[dev_GetAccountSQL] @AccountID = '24918'		/*	buffalobiller1 */
	*/


	/*	Set the current Dispersment rates inactive	*/

	UPDATE			[Record]	/*	Disbursement Rate	*/
		SET			IsActive = 0
		FROM		[Record] [Disbursement Rate]
		WHERE		[Disbursement Rate].TableID = '2478'	/*	Disbursement Rate	*/
		AND			[Disbursement Rate].IsActive = 1
		AND			[Disbursement Rate].V004 = @RecordID	/*	Job No	*/

	/*	Set the current Designation rates inactive	*/

	UPDATE			[Record]	/*	Designation Rate	*/
		SET			IsActive = 0
		FROM		[Record] [Designation Rate]
		WHERE		[Designation Rate].TableID = '2477'		/*	Designation Rate	*/
		AND			[Designation Rate].IsActive = 1
		AND			[Designation Rate].V003 = @RecordID	/*	Job No	*/

	/*	Reset the Dispersment Rates for the Job	*/

	INSERT INTO		[Record] 			/*	[Disbursement Rate]	*/
		(			[TableID]
					,EnteredBy
					,IsActive
					,V004				/*	[Job No]	*/
					,V001				/*	[Item]	*/
					,V002				/*	[$/Unit]	*/
					,V003				/*	[Type]	*/
					,V005				/*	[Is Special]	*/
					,V008				/*	[Master Rate]	*/
		)
		SELECT		2478 AS											TableID		/*	[Disbursement Rate]	*/
					,@UserID AS										[EnteredBy]
					,1 AS											[IsActive]
					,@RecordID	AS									[Job No]
					,[Disbursement Master].V001	AS					[Item]
					,[Disbursement Master].V002	AS					[$/Unit]
					,[Disbursement Master].V003	AS					[Type]
					,'No'						AS					[Is Special]
					,[Disbursement Master].RecordID	AS				[Master Rate]
		FROM		[Record] [Disbursement Master]
		WHERE		[Disbursement Master].TableID = '2713'		/*	Disbursement Master	*/
		AND			[Disbursement Master].IsActive = 1	
		AND			[Disbursement Master].V004 = '1'	
		
	/*	Reset the Designation Rates for the Job	*/

	INSERT INTO		[Record] 			/*	[Designation Rate]	*/
		(			[TableID]
					,EnteredBy
					,IsActive
					,V003				/*	[Job No]	*/
					,V005				/*	[Designation]	*/
					,V002				/*	[Rate ($/hr)]	*/
					,V004				/*	[Is Special]	*/
					,V007				/*	[Master Rate]	*/
		)
		SELECT		2477 AS											TableID		/*	[Designation Rate]	*/			
					,@UserID AS										[EnteredBy]
					,1 AS											[IsActive]
					,@RecordID	AS									[Job No]
					,[Designation Master].V003		AS				[Designation]
					,[Designation Master].V002		AS				[Rate ($hr)]
					,'No'						AS					[Is Special]
					,[Designation Master].RecordID	AS				[Master Rate]
		FROM		[Record] [Designation Master]
		WHERE		[Designation Master].TableID = '2712'		/*	Designation Master	*/
		AND			[Designation Master].IsActive = 1
		AND			[Designation Master].V004 = '1'

	/*	Update the job to be Default rather than Special	*/
	
	UPDATE			[Record]	/*	Disbursement Rate	*/
		SET			V012 = 'FALSE'	/*	[Special Rates of Charge]	*/
		FROM		[Record] [Job Information]
		WHERE		[Job Information].TableID = '2475'		/*	Job Information	*/
		AND			[Job Information].IsActive = 1
		AND			[Job Information].RecordID = @RecordID
END