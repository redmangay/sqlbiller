﻿

CREATE PROCEDURE [dbo].[Menu_ShowForUser]
(
	@MenuID int,
	@UserID int,
	@ShowMenu bit output
)
AS
/*
-- Testing
 EXEC [dbo].[Menu_ShowForUser] 4893,25500,''
 
*/
BEGIN TRY
	-- Are we using "Advanced Security"?
	IF EXISTS(SELECT TOP 1 1 FROM [MenuRole] WHERE [MenuID] = @MenuID)
	BEGIN
		-- Advanced 
		IF EXISTS(SELECT TOP 1 1 
					FROM [MenuRole] M
						JOIN [UserRole] UR ON UR.RoleID = M.RoleID 
						WHERE M.MenuID = @MenuID 
						AND UR.UserID = @UserID)
		BEGIN
		
			SET @ShowMenu = 1 /* Advanced security, has rights */
		END		
		ELSE
			SET @ShowMenu = 0  /* Advanced security, no rights */
	
	END
	ELSE 
	BEGIN
		-- Not advanced security. Is this a table menu?
		IF EXISTS(SELECT TOP 1 1	
				FROM [Menu] M 
				JOIN [Table] T ON T.TableID = M.TableID 
				WHERE M.MenuID = @MenuID)
		BEGIN
			-- Does this user have access to that table?
			IF EXISTS(SELECT *
						FROM [Menu] M
						JOIN [RoleTable] RT ON RT.TableID = M.TableID 
						JOIN [UserRole] UR ON UR.RoleID = RT.RoleID
						WHERE UserID = @UserID AND MenuID = @MenuID)
						--AND RoleType <> '6' ) -- only menu that is/are not None access role
			 BEGIN
				SET @ShowMenu = 1 /* Basic security, table menu, with table rights */
			 END
			ELSE
				SET @ShowMenu = 0 /* Basic security, table menu, no rights */
		END
		ELSE
		BEGIN
			SET @ShowMenu = 1 -- it is not a table so they are allowed to see it
		END
	END

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Menu_ShowForUser', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), '@MenuID: ' + CAST(ISNULL(@MenuID,0) as varchar)) -- JB 10/11/17 added @SQL 
END CATCH

