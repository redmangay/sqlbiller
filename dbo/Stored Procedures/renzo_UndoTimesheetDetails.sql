﻿CREATE PROCEDURE [dbo].[renzo_UndoTimesheetDetails]
(
	@RecordID  int,
	@FormSetID int = null,	
    @UserID int=null,
    @Return varchar(max) output
	
)
AS

/* 


exec [dbo].[renzo_UndoTimesheetDetails]


*/




--DECLARE @TimeSheetDetailsReference int
DECLARE @TaxRate varchar(max),@CurrencyRate	varchar(max), @Job INT

				
SELECT			@Job = [Record].[V001]				
FROM			[Record]
WHERE			[Record].IsActive = 1
AND				[Record].RecordID = @RecordID



-- Red: lets get the currency rate and tax

SELECT  @CurrencyRate = ISNULL(V007,1),
	   @TaxRate = (ISNULL(V011,0) / 100.00) FROM [Record]
WHERE RECORDID = @Job


IF (SELECT V010 FROM [Record] WHERE RecordID = @RecordID) IS NOT NULL
BEGIN

		
---- lets update tax
--SELECT @TaxRate = V011 FROM RECORD WHERE RECORDID =(SELECT V001 FROM RECORD WHERE RECORDID = (SELECT V010 FROM RECORD WHERE RECORDID = @RecordID))

 --    EXEC [dbo].[renzo_UndoTimesheetDetails] @RecordID=  2710256,@Return=''


UPDATE			[Timesheet Details] 			/*	[Timesheet Details]	*/
		SET			DateTimeRecorded = [Disbursement].DateTimeRecorded
					,EnteredBy = [Disbursement].EnteredBy
					,IsActive = [Disbursement].IsActive
					,DateUpdated = [Disbursement].DateUpdated
					,V001 = [Disbursement].V001				/*	[Job No]	*/
					,V009 = [Disbursement].V002				/*	[Date]	*/
					,V002 = [Disbursement].V019				/*	[Initials]	*/
					,V003 = [Disbursement].V006				/*	[Details]	*/
					,V004 = NULL							/*	[(X)]	*/
					,V005 = [Disbursement].V004							/*	[Hr/Unit]	*/
					,V006 = Round(cast(Disbursement.V005 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)),2)--[Disbursement].V005							/*	[Rate]	*/
					--,V012 = [Timesheet].V018				/*	[GST]	*/
					,V007 = Round(cast(Disbursement.V005 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)) * cast(Disbursement.V004 AS MONEY),2)--[Disbursement].V007				/*	[$ Amount]	*/
					,V008 = 'Fixed'							/*	[Status]	*/
					,V010 = [Disbursement].RecordID			/*	[Disbursement]	*/
					--,V011 = [Disbursement].RecordID		/*	[Timesheet]	*/
					,V022 = '2'
		FROM		[Record] [Timesheet Details]
		JOIN		Record [Disbursement] ON					[Disbursement].TableID = '2725'		/*	Disbursement	*/
														AND		[Disbursement].IsActive = 1
														AND		CAST( [Disbursement].RecordID AS VARCHAR(MAX)) = [Timesheet Details].V010		/*	V010	*/
														--AND		[Disbursement].RecordID = @RecordID
		WHERE		[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
		AND			[Timesheet Details].IsActive = 1
		AND			[Timesheet Details].RecordID = @RecordID




UPDATE [Timesheet Details]
SET			V012 = CAST( CAST( REPLACE([Timesheet Details].V007, ',', '')  AS DEC(20,4) ) * @TaxRate AS VARCHAR(MAX) )				/*	[GST]	*/
FROM [RECORD] [Timesheet Details]
WHERE		[Timesheet Details].TableID = '2729'
AND RECORDID = @RecordID

END


IF (SELECT V011 FROM [Record] WHERE RecordID = @RecordID) IS NOT NULL
BEGIN
print 'ts'

---- lets update tax
--SELECT @TaxRate = V011 FROM RECORD WHERE RECORDID =(SELECT V018 FROM RECORD WHERE RECORDID = (SELECT V011 FROM RECORD WHERE RECORDID = @RecordID))


 --    EXEC [dbo].[renzo_ModifyTimeSheetDetails]  2710256
UPDATE			[Timesheet Details] 			/*	[Timesheet Details]	*/
		SET			DateTimeRecorded = [Timesheet].DateTimeRecorded
					,EnteredBy = [Timesheet].EnteredBy
					,IsActive = [Timesheet].IsActive
					,DateUpdated = [Timesheet].DateUpdated
					,V001 = [Timesheet].V018				/*	[Job No]	*/
					,V009 = [Timesheet].V004				/*	[Date]	*/
					,V002 = [Timesheet].V017				/*	[Initials]	*/
					,V003 = [Timesheet].V006				/*	[Details]	*/
					,V004 = [Timesheet].V007				/*	[(X)]	*/
					,V005 = [Timesheet].V015				/*	[Hr/Unit]	*/
					,V006 = Round(cast([Timesheet].V009 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)),2) --[Timesheet].V009				/*	[Rate]	*/
					--,V012 = [Timesheet].V018				/*	[GST]	*/
					,V007 = Round(cast([Timesheet].V009 AS money) * cast(@CurrencyRate AS DEC(10,2)) * cast([Timesheet].V007 AS DEC(10,2))	* cast([Timesheet].V015 AS DEC(10,2)),2) --[Timesheet].V019				/*	[$ Amount]	*/
					,V008 = 'By Rate'						/*	[Status]	*/
					--,V010 = [Timesheet].V018				/*	[Disbursement]	*/
					,V011 = [Timesheet].RecordID			/*	[Timesheet]	*/
					,V022 = '1'
		FROM		[Record] [Timesheet Details]
		JOIN		Record [Timesheet] ON				[Timesheet].TableID = '2488'		/*	Timesheet	*/
												AND		[Timesheet].IsActive = 1
												AND		CAST( [Timesheet].RecordID AS VARCHAR(MAX)) = [Timesheet Details].V011		/*	V011	*/
												--AND		[Timesheet].RecordID = @RecordID
		WHERE		[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
		AND			[Timesheet Details].IsActive = 1
		AND			[Timesheet Details].RecordID = @RecordID


UPDATE [Timesheet Details]
SET			V012 = CAST( CAST( REPLACE([Timesheet Details].V007, ',', '')  AS DEC(20,4) ) * @TaxRate AS VARCHAR(MAX) )				/*	[GST]	*/
FROM [RECORD] [Timesheet Details]
WHERE		[Timesheet Details].TableID = '2729'
AND RECORDID = @RecordID


END









RETURN @@ERROR;