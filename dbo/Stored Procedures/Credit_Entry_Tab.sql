﻿-- =============================================
-- Author:		Barry Matutina	
-- Create date: Nov. 2, 2016
-- Description:	Generates a tab menu for Credit Entry
-- =============================================
CREATE PROCEDURE Credit_Entry_Tab
	-- Add the parameters for the stored procedure here
	
	@RecordID VarChar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	/********************[Interim Credits]*************/
/*	SELECT [Interim Credits] */

SELECT			[Interim Credits].[TableID]
				,[Interim Credits].RecordID	AS				[Record ID]
				,[Interim Credits].V001		AS				[Activity]
				,[Interim Credits].V002		AS				[chkCredit]
				,[Interim Credits].V003		AS				[Date]
				,[Interim Credits].V004		AS				[Credit No]
				,[Interim Credits].V005		AS				[Description]
				,[Interim Credits].V006		AS				[Amount]
				,[Interim Credits].DateAdded
	FROM		[Record] [Interim Credits] WITH (NOLOCK)
	LEFT JOIN	Record [Activity] ON					[Activity].TableID = '2711'		/*	Bill & Payment Activity	*/
											AND		[Activity].IsActive = 1
											AND		CAST( [Activity].RecordID AS VARCHAR(MAX)) = [Interim Credits].V001		/*	Activity	*/
	WHERE		[Interim Credits].TableID = '2731'		/*	Interim Credits	*/
	AND			[Interim Credits].IsActive = 1

	UPDATE			[Record] 			/*	[Interim Credits]	*/
	SET			[TableID] = [TableID]
				,DateTimeRecorded = DateTimeRecorded
				,EnteredBy = EnteredBy
				,IsActive = IsActive
				,DateUpdated = DateUpdated
				,V001 = V001				/*	[Activity]	*/
				,V002 = V002				/*	[chkCredit]	*/
				,V003 = V003				/*	[Date]	*/
				,V004 = V004				/*	[Credit No]	*/
				,V005 = V005				/*	[Description]	*/
				,V006 = V006				/*	[Amount]	*/
	FROM		[Record] [Interim Credits]
	WHERE		[Interim Credits].TableID = '2731'		/*	Interim Credits	*/
	AND			[Interim Credits].IsActive = 1
END