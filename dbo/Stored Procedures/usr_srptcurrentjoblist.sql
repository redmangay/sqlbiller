﻿CREATE procedure [dbo].[usr_srptcurrentjoblist]
(
@sgroup varchar(max),
@pcontroller varchar(max),
@Group_Controller int =1
)

-- Author:		<Manoj sharma>
-- Create date: <12 March 2016>
-- Description:	<Used as ssrs report and in procedure srrptcurrentjoblist>
as

exec usr_rpt_CurrentJobList
SELECT        CurrentJobList.[Group/Division] AS GroupDivision, CurrentJobList.Controller, CurrentJobList.Name, CurrentJobList.ProjectNumber AS JobNumber, CurrentJobList.ProjectTitle AS JobTitle, CurrentJobList.Approved, 
                         CurrentJobList.Invoiced, CurrentJobList.Balanced, CurrentJobList.PaymentBalanced, isnull(CurrencyRate, 1) AS ExchangeRate, status
INTO              #srptcurrentjoblist
FROM            ##CurrentJobList CurrentJobList
                             SELECT        *
                              FROM            #srptcurrentjoblist CurrentJobList
                              WHERE        (status = '2' and
((GroupDivision =@sgroup) or ( @sgroup in('ALL','NULL'))) and ((CurrentJobList.Name= @pcontroller )or (@pcontroller='NULL') ))

                              ORDER BY GroupDivision, CurrentJobList.Name, CurrentJobList.JobNumber;