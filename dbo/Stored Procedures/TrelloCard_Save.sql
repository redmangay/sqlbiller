﻿

-- =============================================
-- Author:		DH
-- Create date: 15 Aug 2017
-- Description:	Save card info
-- =============================================
CREATE PROCEDURE [dbo].[TrelloCard_Save] 
	@RefId varchar(50),
	@BoardRefId varchar(50),
	@Name NVARCHAR(200),
	@Description NVARCHAR(MAX) = NULL,
	@ShortLink varchar(50),
	@Due datetime = NULL,
	@DueComplete bit = 0,
	@Closed bit = 0,
	@DateLastActivity datetime,
	@MembersXML NVARCHAR(MAX) = ''
AS

BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @CardId INT
	DECLARE @BoardId INT
	DECLARE @DateLast datetime
	DECLARE @Changed bit = 0

    SELECT	@CardId = TrelloCardID,
			@DateLast = DateLastActivity
	FROM	[TrelloCard]
	WHERE	RefId = @RefId
	
	IF(@CardId IS NULL)
		BEGIN
			-- Card does not exist in DB, we will insert it
			SELECT @BoardId = TrelloBoardID FROM TrelloBoard WHERE RefId = @BoardRefId
			
			IF(@BoardId > 0)
				BEGIN
					INSERT INTO [TrelloCard] (RefId, TrelloBoardId, [Name], [Description], ShortLink, Due, DueComplete, Closed, DateLastActivity)
					VALUES (@RefId, @BoardId, @Name, @Description, @ShortLink, @Due, @DueComplete, @Closed, @DateLastActivity)

					SET @CardId = SCOPE_IDENTITY()
					SET @Changed = 1
				END
		END
	ELSE
		BEGIN
			-- IF card exists, check IF it need to be Updated or not
			IF(@DateLast < @DateLastActivity)
				BEGIN
					UPDATE	[TrelloCard]
					SET		[Name] = @Name,
							[Description] = @Description,
							ShortLink = @ShortLink,
							Due = @Due,
							DueComplete = @DueComplete,
							Closed = @Closed,
							DateLastActivity = @DateLastActivity
					WHERE	TrelloCardID = @CardId
					SET @Changed = 1
				END
		END
		
	-- IF card was changed, then we continue with saving card members
	IF(@CardId > 0 AND @Changed = 1 AND @MembersXML IS NOT NULL AND @MembersXML <> '')
		BEGIN				
				DELETE FROM TrelloCardMember WHERE TrelloCardId = @CardId
				
				DECLARE @MemberId INT
				DECLARE @MemberRefId varchar(50)
				DECLARE @MemberUsername varchar(50)
				DECLARE @MemberInitials varchar(10)
				DECLARE @MemberAvatarHash varchar(100)
				DECLARE @CardMemberInitials varchar(200) = '';
				
				DECLARE @xmlDoc INT
				EXEC sp_xml_preparedocument @xmlDoc OUTPUT, @MembersXML;  
				
				DECLARE member_cursor CURSOR FOR  
				SELECT  *  
				FROM	OPENXML (@xmlDoc, '/ArrayOfMember/Member',2)  
				WITH (   
					Id varchar(50),  
					Username varchar(50),  
					Initials varchar(10),
					AvatarHash varchar(100)
				)		
	
				OPEN member_cursor   
				FETCH NEXT FROM member_cursor INTO @MemberRefId, @MemberUsername, @MemberInitials, @MemberAvatarHash

				WHILE @@FETCH_STATUS = 0   
					BEGIN   									
						SET @CardMemberInitials = @CardMemberInitials + @MemberInitials + ' '
						SET @MemberId = NULL

						SELECT	@MemberId = TrelloMemberID
						FROM	TrelloMember
						WHERE	RefId = @MemberRefId
						
						IF(@MemberId IS NULL)
							BEGIN
								-- Member does not exist
								INSERT INTO TrelloMember
								(
									RefId,
									Username,
									Initials,
									AvatarHash
								)
								VALUES
								(
									@MemberRefId, 
									@MemberUsername,
									@MemberInitials,
									@MemberAvatarHash
								)

								SET @MemberId = SCOPE_IDENTITY()
							END
						
						INSERT INTO TrelloCardMember (TrelloCardId, TrelloMemberId) VALUES (@CardId, @MemberId)

						FETCH NEXT FROM member_cursor INTO @MemberRefId, @MemberUsername, @MemberInitials, @MemberAvatarHash 
					END   

				CLOSE member_cursor   
				DEALLOCATE member_cursor

				UPDATE	TrelloCard
				SET		MemberInitials = @CardMemberInitials
				WHERE	TrelloCardID = @CardId
		END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('TrelloCard_Save', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
