﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usr_srptEmployeeBilling]
AS
BEGIN
SELECT[TableID]
      ,[Record ID]
      ,[GUID]
      ,[ProjectNumber]
      ,[EmployeeID]
      ,[EmployeeName]
      ,[GroupDivision]
      ,[InvoiceNo]
      ,[InvoiceDate]
      ,[ProjectTitle]
      ,[Controller]
      ,[TimeSheetDollar]
      ,[BilledDollar]
      ,[ExchangeRate]
      ,[Type]
      ,[RecordedDate]
      ,[DateAdded]
  FROM [thedatabase_windtech].[Account24918].[vrptEmpBilling]
END
