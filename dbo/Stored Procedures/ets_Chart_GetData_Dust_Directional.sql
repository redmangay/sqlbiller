﻿

CREATE PROCEDURE [dbo].[ets_Chart_GetData_Dust_Directional]
(
       -- Add the parameters for the stored procedure here
       @nTableID int,
	   @sGraphXAxisColumnID varchar(MAX),
       @sValueFields varchar(MAX),
       @dStartDate datetime,
       @dEndDate datetime,
       @sPeriod varchar(20), -- Possible values: Year, Month, Day, Hour
       -- When you pass Year it will return the month averages
       -- when you pass Month it will return the day averages
       -- when you pass Week it will return the day averages
       -- when you pass day it will return hour aveages
       -- when you pass hour it will return all data
	   @GraphSeriesColumnID varchar(MAX),
	   @GraphSeriesID varchar(MAX),
	   @nDataColumn1ID int = NULL,
	   @nDataColumn2ID int = NULL,
	   @nDataColumn3ID int = NULL,
	   @nDataColumn4ID int = NULL,
	   @ForceAverage bit,
       @bPercent bit= 0,
	   @nMaxRows int = NULL
)
AS
/*
       PRINT SYSDATETIME()
       EXEC ets_Chart_GetData_New
              @nTableID=919
              ,@sValueFields = 'V002'
              ,@dStartDate='2012-12-07 12:28'
              ,@dEndDate='2013-12-09 16:00'
              ,@sPeriod='Hour'
              ,@bPercent=1,@nMaxRows=200
       PRINT SYSDATETIME()
       -- 3603515 1st run
       -- 2919922 2nd run : no IF
       -- 2529297 3rd run : using IN
*/

BEGIN TRY
    SET NOCOUNT ON;
    SET dateformat dmy  
    DECLARE @sql nvarchar(MAX)       
	DECLARE @NorthColumn varchar(MAX)
	DECLARE @EastColumn varchar(MAX)
	DECLARE @SouthColumn varchar(MAX)
	DECLARE @WestColumn varchar(MAX)
	DECLARE @WhereClause varchar(MAX)

	SELECT @NorthColumn = SystemName FROM [Column] WHERE ColumnID = @nDataColumn1ID
	SELECT @EastColumn = SystemName FROM [Column] WHERE ColumnID = @nDataColumn2ID
	SELECT @SouthColumn = SystemName FROM [Column] WHERE ColumnID = @nDataColumn3ID
	SELECT @WestColumn = SystemName FROM [Column] WHERE ColumnID = @nDataColumn4ID

	DECLARE @bIgnoreSymbols bit=1
	--SELECT @bIgnoreSymbols=IgnoreSymbols FROM [Column] WHERE SystemName=@sValueFields AND TableID=@nTableID AND ColumnType='number'

	IF (@bIgnoreSymbols IS NOT NULL AND @bIgnoreSymbols = 1)
	BEGIN
		SET @NorthColumn = 'dbo.RemoveNonNumericChar(' + @NorthColumn + ')'
		SET @EastColumn = 'dbo.RemoveNonNumericChar(' + @EastColumn + ')'
		SET @SouthColumn = 'dbo.RemoveNonNumericChar(' + @SouthColumn + ')'
		SET @WestColumn = 'dbo.RemoveNonNumericChar(' + @WestColumn + ')'
	END

	SET @WhereClause = ' WHERE IsActive = 1' +
			' AND CAST(' + @sGraphXAxisColumnID + ' AS datetime) >= ''' + CONVERT(varchar(20), @dStartDate, 113) +
				''' AND  CAST(' + @sGraphXAxisColumnID + ' AS datetime) <= ''' + CONVERT(varchar(20), @dEndDate, 113) + '''' +
			' AND TableID = ' + CAST(@nTableID AS Varchar) +
			CASE
				WHEN @GraphSeriesColumnID IS NULL THEN ''
				WHEN @GraphSeriesID IS NULL THEN ''
				WHEN LEN(@GraphSeriesColumnID) = 0 THEN ''
				WHEN LEN(@GraphSeriesID) = 0 THEN ''
				ELSE ' AND [' + @GraphSeriesColumnID + '] = ''' + @GraphSeriesID + ''''
			END

    SET @sql = 'SELECT Direction, Value FROM' +
			' (SELECT SUM(CAST(' + @NorthColumn + ' AS decimal (18,4))) AS North,' +
				' SUM(CAST(' + @EastColumn + ' AS decimal (18,4))) AS East,' +
				' SUM(CAST(' + @SouthColumn + ' AS decimal (18,4))) AS South,' +
				' SUM(CAST(' + @WestColumn + ' AS decimal (18,4))) AS West' +
				' FROM [Record]' +
				@WhereClause + ') p' +
			' UNPIVOT' +
				' (Value FOR Direction IN (North, East, South, West)) x'

    PRINT @sql
    EXEC(@sql)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Chart_GetData_Dust_Directional', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
