﻿CREATE PROCEDURE [dbo].[dev_GetAccountInfo]
(
	@AccountName	VARCHAR(MAX) = NULL
	,@AccountID		VARCHAR(MAX) = NULL
)	
AS
BEGIN
	IF @AccountID IS NOT NULL
		SELECT @AccountID = AccountID, @AccountName = AccountName FROM [Account] WHERE AccountID = @AccountID
	ELSE
	BEGIN
		IF @AccountName IS NULL 
			PRINT 'Either @AccountName or @AccountID must be passed to dev_GetAccountInfo'
		ELSE 
			SELECT TOP 1 @AccountID = AccountID, @AccountName = AccountName FROM [Account] WHERE AccountName LIKE ('%'+@AccountName+'%')
	END
	IF @AccountID IS NOT NULL
	BEGIN
		SELECT A.AccountID, A.AccountName, T.[TableName], T.[TableID]
		, 'EXEC [dev_GetTableSQL2] @TableID=' + CAST(T.[TableID] AS varchar) + ' /* ' + [TableName] + ' */' AS [Table SQL] 
		, 'EXEC [dev_ViewRecord]   @TableID=' + CAST(T.[TableID] AS varchar) + ' /* ' + [TableName] + ' */' AS [View Records]
		, 'EXEC [dev_SPFramework]  @TableID=' + CAST(T.[TableID] AS varchar) + ' /* ' + [TableName] + ' */' AS [SP Framework]
			FROM [Account] A
			JOIN [Table] T ON T.AccountID = A.AccountID 
			WHERE A.AccountID = @AccountID
			ORDER BY T.[TableName] 
	END
END

