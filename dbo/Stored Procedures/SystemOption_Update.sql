﻿
 
CREATE PROCEDURE [dbo].[SystemOption_Update]
(
	@nSystemOptionID int ,
	@sOptionKey nvarchar(200) ,
	@sOptionValue nvarchar(1000) ,
	@sOptionNotes ntext,
	@nAccountID int=NULL,
	@nTableID int=NULL 
)
 
AS

BEGIN TRY 
	UPDATE [SystemOption]
	SET 
		OptionKey = @sOptionKey,
		OptionValue = @sOptionValue,
		OptionNotes = @sOptionNotes,
		AccountID=@nAccountID,
		TableID=@nTableID
	WHERE SystemOptionID = @nSystemOptionID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('SystemOption_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
