﻿-- =============================================
-- Author:		<Red Mangay>
-- Create date:	<10/07/2018>
-- Description:	<Get the tableid of the orginal table ie timesheet or disbursement>
-- =============================================
/*

exec [GetTimeSheetTableIDFrom] 2713164

*/


CREATE PROCEDURE [dbo].[GetTimeSheetTableIDFrom]
(
	@RecordID int
	
)
AS
BEGIN
	
		  SELECT  * FROM RECORD
		  WHERE RECORDID =  (
		  SELECT  ISNULL(V010,V011) FROM RECORD
		  WHERE RECORDID =  @RecordID)

END

