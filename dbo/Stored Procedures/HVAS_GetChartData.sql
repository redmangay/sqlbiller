﻿

-- =============================================
-- Author:		Dathq
-- Create date: 11 Jun 2012
-- Description:	Get HVASDust Chart Data
-- =============================================
CREATE PROCEDURE [dbo].[HVAS_GetChartData] 
	-- Add the parameters for the stored procedure here
	@Series varchar(MAX),
	@ValueFields varchar(MAX),
	@StartDate datetime,
	@EndDate datetime
AS

BEGIN TRY
	SET NOCOUNT ON;
	
    DECLARE @Sql varchar(MAX)    
	
	IF(CHARINDEX(',', @Series) > 0)
		BEGIN
			SET @sql =	' SELECT LocationID, DateTimeRecorded,' + @ValueFields +
				' FROM Record' +
				' WHERE DateTimeRecorded BETWEEN ''' + CAST(@StartDate AS Varchar) + ''' AND ''' +  CAST(@EndDate AS Varchar) + '''' +
				' AND LocationID IN (' + @Series + ')' + 
				' AND TableID = 193' +
				' ORDER BY LocationID, DateTimeRecorded' 
		END	
	ELSE
		BEGIN
			SET @sql =	' SELECT DateTimeRecorded,' + @ValueFields +
				' FROM Record' +
				' WHERE DateTimeRecorded BETWEEN ''' + CAST(@StartDate AS Varchar) + ''' AND ''' +  CAST(@EndDate AS Varchar) + '''' +
				' AND LocationID = ' + @Series + 
				' AND TableID = 193' +
				' ORDER BY DateTimeRecorded' 			
		END
	PRINT @sql
	EXEC(@sql)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('HVAS_GetChartData', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
