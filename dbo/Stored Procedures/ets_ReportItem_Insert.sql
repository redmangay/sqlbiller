﻿

 
CREATE PROCEDURE [dbo].[ets_ReportItem_Insert]
(
	@nNewID int output,
	@nReportID int,
	@nObjectID int,
	@nItemType int,
	@sReportItemTitle varchar(500) = NULL,
	@bApplyFilter bit = 0,
	@bApplySort bit = 0,
	@bUseColors bit = 0,
	@bHighlightWarnings bit = 0,
	@nPeriodColumnID int = NULL,
	@nUserID int = NULL,
	@nViewID INT=NULL
)
AS

BEGIN TRY
	DECLARE @nPosiiton int
	SELECT @nPosiiton = MAX(ItemPosition) FROM [ReportItem] WHERE [ReportID] = @nReportID
	INSERT INTO [ReportItem]
	(
		ReportID, 
		ObjectID, 
		ItemType, 
		ItemTitle,
		ApplyFilter,
		ApplySort,
		UseColors,
		HighlightWarnings,
		PeriodColumnID,
		ItemPosition,
		UserID
	) VALUES (
		@nReportID,
		@nObjectID,
		@nItemType,
		@sReportItemTitle,
		@bApplyFilter,
		@bApplySort,
		@bUseColors,
		@bHighlightWarnings,
		@nPeriodColumnID,
		ISNULL(@nPosiiton, -1) + 1,
		@nUserID
	)
	SELECT @nNewID = @@IDENTITY

	IF @nViewID IS NOT NULL AND @nItemType = 1
		INSERT INTO [ReportTableItem]
			(
				ReportItemID, 
				ColumnID, 
				ColumnTitle,
				ColumnPosition
			)
		SELECT  @nNewID, VI.ColumnID, C.DisplayTextSummary, ROW_NUMBER() OVER(ORDER BY VI.ColumnIndex ASC)
			FROM [Column] C JOIN [ViewItem] VI ON VI.ColumnID=C.ColumnID
			WHERE C.[DisplayTextSummary] IS NOT NULL  AND VI.ViewID=@nViewID

	IF @nViewID IS NULL AND  @nItemType = 1
		INSERT INTO [ReportTableItem]
		(
			ReportItemID, 
			ColumnID, 
			ColumnTitle,
			ColumnPosition
		)
		SELECT @nNewID, ColumnID, DisplayTextSummary, ROW_NUMBER() OVER(ORDER BY DisplayOrder ASC)
		FROM [Column]
		WHERE [DisplayTextSummary] IS NOT NULL AND [TableID] = @nObjectID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_ReportItem_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
