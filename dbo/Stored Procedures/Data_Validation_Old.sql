﻿

CREATE PROCEDURE [dbo].[Data_Validation_Old]
(
	@sData varchar(MAX),
	@sValidation varchar(4000)	
)
AS

BEGIN TRY

/*
EXEC [dbo].[Data_Validation] '1111', 'Value > 0 AND Value < 1000'
EXEC [dbo].[Data_Validation] '2', 'Value IN (1,2,3)'
EXEC [dbo].[Data_Validation] 'ABC', 'Value IN (''ABC'',''XYZ'',''PQR'')'
EXEC [dbo].[Data_Validation] '01/01/2010', 'Value IN (''01/05/2010'',''01/01/2010'')'
EXEC [dbo].[Data_Validation] '01/07/2010', 'Value >''01/05/2010'''
EXEC [dbo].[Data_Validation] '3', 'Value = NULL OR ISNUMERIC(0)'
EXEC [dbo].[Data_Validation] '4', 'Value = NULL OR ISNUMERIC(0) = 1'
*/


	DECLARE @sSQL varchar(MAX)
   
	DECLARE @sResult varchar(MAX)
	
    IF ISNUMERIC(@sData)=1
    
		SELECT @sSQL= 'DECLARE @tTemp TABLE
		(result varchar(10), value decimal(16,9)) 
		INSERT INTO @tTemp(value) Values ('+@sData+')
		SELECT ''Valid'', value FROM @tTemp WHERE ' + @sValidation
	ELSE IF ISDATE(@sData)=1
		SELECT @sSQL= 'DECLARE @tTemp TABLE
		(result varchar(10), value datetime) 
		INSERT INTO @tTemp(value) Values ('''+@sData+''')
		SELECT ''Valid'', value FROM @tTemp WHERE ' + @sValidation	
	ELSE
		SELECT @sSQL= 'DECLARE @tTemp TABLE
		(result varchar(10), value varchar(MAX)) 
		INSERT INTO @tTemp(value) Values ('''+@sData+''')
		SELECT ''Valid'', value FROM @tTemp WHERE ' + @sValidation
	
	PRINT @sSQL
	BEGIN TRY
		EXEC (@sSQL)
		PRINT @@ROWCOUNT
	END TRY
	BEGIN CATCH
		SELECT
			'Error',ERROR_MESSAGE() AS ErrorMessage;	
	END CATCH
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Data_Validation_Old', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
