﻿

CREATE PROCEDURE [dbo].[spResetIDs]
    -- Add the parameters for the stored procedure here
    @TableID int,
    @ColumnName varchar(10),
    @TestRun bit = 1
AS

BEGIN TRY
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

    IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'xTempIDTable')
        CREATE TABLE xTempIDTable (RecordID int, ID int identity)
    TRUNCATE TABLE xTempIDTable

    INSERT INTO xTempIDTable (RecordID) SELECT RecordID FROM [Record] WHERE TableID = @TableID AND IsActive = 1 ORDER BY RecordID
    IF @TestRun = 1
        SELECT * FROM xTempIDTable

    DECLARE @SQL varchar(MAX)
    SET @SQL = 'UPDATE [Record] SET [Record].' + @ColumnName + ' = T.ID
        FROM [Record]
        JOIN xTempIDTable T ON T.RecordID = [Record].RecordID  
        WHERE [Record].TableID = ' + cast(@TableID as varchar(10)) + ' AND [Record].IsActive = 1 '
    
    IF @TestRun = 0
    BEGIN
        PRINT 'SQL Run: ' + (@SQL)
        EXEC (@SQL)
    END
    ELSE
        PRINT 'TEST Run: ' + (@SQL)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spResetIDs', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
