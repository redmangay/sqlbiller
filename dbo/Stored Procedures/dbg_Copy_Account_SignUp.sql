﻿

CREATE PROCEDURE [dbo].[dbg_Copy_Account_SignUp] 
(
	@nTemplateAccountID INT,
	@nTargetAccountID INT,
	@nSignUpUserID INT

)
AS 
/* Testing
EXEC [dbg_Copy_Account_SignUp] 24808 ,24809,24989
*/

BEGIN TRY
	BEGIN TRANSACTION --beginning a transaction..
      --GET ALL Table   
                            
    DECLARE Cur_Table CURSOR FOR
    SELECT TableID FROM [Table] WHERE AccountID=@nTemplateAccountID
    DECLARE @nSourceTableID INT;
    OPEN Cur_Table;
    FETCH FROM Cur_Table INTO @nSourceTableID;
    
    WHILE @@FETCH_STATUS = 0
		BEGIN
			-- ets_CopyTable @nSourceTableID, @nTargetAccountID, @nUserID, @bWithData
			EXEC ets_CopyTable @nSourceTableID, @nTargetAccountID,@nSignUpUserID,0
           
			
			FETCH FROM Cur_Table INTO @nSourceTableID;
		END
    CLOSE Cur_Table
	DEALLOCATE Cur_Table	                      
	
	PRINT 'ets_CopyTable DONE'
	
   UPDATE Menu SET ShowOnMenu=0 WHERE Menu='--None--' AND AccountID=@nTargetAccountID
                         
	
	                        
    DECLARE Cur_TableChild CURSOR FOR
    SELECT TableChildID FROM [TableChild] INNER JOIN [Table] 
    ON [TableChild].ParentTableID=[Table].TableID WHERE AccountID=@nTemplateAccountID
    DECLARE @nSourceTableChildID INT;
    OPEN Cur_TableChild;
    FETCH FROM Cur_TableChild INTO @nSourceTableChildID;
    
    WHILE @@FETCH_STATUS = 0
		BEGIN
			DECLARE @nSParentTableID INT
			DECLARE @nSChildTableID INT
            DECLARE @nSConditionColumnID INT
            
            DECLARE @sSParentTableName nvarchar(255)
            DECLARE @sSChildTableName nvarchar(255)
            DECLARE @sSHideColumnSystemName varchar(50)
            
            
            SELECT @nSParentTableID=TableChild.ParentTableID,@nSChildTableID=ChildTableID,
            @nSConditionColumnID=ConditionColumnID FROM TableChild INNER JOIN [Table] ON 
            TableChild.ParentTableID=[Table].TableID
            WHERE TableChildID=@nSourceTableChildID
            
			SELECT @sSParentTableName=TableName FROM [Table] WHERE TableID=@nSParentTableID
			SELECT @sSChildTableName=TableName FROM [Table] WHERE TableID=@nSChildTableID
			
			IF @nSConditionColumnID IS NOT NULL
			SELECT @sSHideColumnSystemName=SystemName FROM [Column] WHERE ColumnID=@nSConditionColumnID
                   
            DECLARE @nTParentTableID INT
			DECLARE @nTChildTableID INT
            DECLARE @nTConditionColumnID INT -- do i need it?
            
            SELECT @nTParentTableID=TableID FROM [Table] 
            WHERE AccountID=@nTargetAccountID AND TableName=@sSParentTableName
            
            SELECT @nTChildTableID=TableID FROM [Table] 
            WHERE AccountID=@nTargetAccountID AND TableName=@sSChildTableName
            
            IF @sSHideColumnSystemName IS NOT NULL
            SELECT @nTConditionColumnID=ColumnID FROM [Column] WHERE TableID=@nTParentTableID AND SystemName=@sSHideColumnSystemName
            
            INSERT INTO TableChild (ParentTableID,ChildTableID,[Description],DetailPageType,
            ShowAddButton,ShowEditButton,DisplayOrder,ConditionColumnID,ConditionValue,ConditionOperator)
            SELECT @nTParentTableID,@nTChildTableID,[Description],DetailPageType,
            ShowAddButton,ShowEditButton,DisplayOrder,@nTConditionColumnID,ConditionValue,ConditionOperator
            FROM TableChild WHERE TableChildID=@nSourceTableChildID
            
            PRINT 'INSERT INTO TableChild each'
            
			FETCH FROM Cur_TableChild INTO @nSourceTableChildID;
		END
    CLOSE Cur_TableChild
	
	DEALLOCATE Cur_TableChild	  
	
	PRINT 'TableChild ALL DONE'
	
	-- now lets work with link
	
	DECLARE Cur_Column CURSOR FOR
    SELECT ColumnID,TableTableID,LinkedParentColumnID,ParentColumnID FROM [Column]INNER JOIN [Table]
    ON [Table].TableID=[Column].TableID WHERE AccountID=@nTargetAccountID
    AND [Table].IsActive=1 AND TableTableID IS NOT NULL AND LinkedParentColumnID IS NOT NULL
    
    
    DECLARE @nTColumnID INT;
    DECLARE @nSTableTableID INT;
    DECLARE @nSLinkedParentColumnID INT;
    DECLARE @nSParentColumnID INT;
    DECLARE @sS_TTTableName nvarchar(255)
    
    OPEN Cur_Column;
    FETCH FROM Cur_Column INTO @nTColumnID,@nSTableTableID,@nSLinkedParentColumnID,@nSParentColumnID;
    
    WHILE @@FETCH_STATUS = 0
		BEGIN
			DECLARE @nTTableTableID INT
			DECLARE @nTLinkedParentColumnID INT
            DECLARE @sTLinkedSystemName varchar(50)
          
           
            SELECT @sS_TTTableName=TableName FROM [Table] WHERE TableID=@nSTableTableID
            SELECT @nTTableTableID=TableID FROM [Table] 
            WHERE AccountID=@nTargetAccountID AND TableName=@sS_TTTableName
            
            IF @nTTableTableID IS NOT NULL
            BEGIN
				
				SELECT @sTLinkedSystemName=SystemName FROM [Column] WHERE ColumnID=@nSLinkedParentColumnID
			 
				SELECT @nTLinkedParentColumnID=columnID FROM [Column] WHERE TableID=@nTTableTableID AND SystemName=@sTLinkedSystemName
				
				IF @nTLinkedParentColumnID IS NOT NULL
				UPDATE [Column] SET TableTableID=@nTTableTableID,
				LinkedParentColumnID=@nTLinkedParentColumnID WHERE ColumnID=@nTColumnID
				
				--Work With Filter
				IF @nSParentColumnID IS NOT NULL
				BEGIN
				 
					DECLARE @nTParentColumnID INT
					DECLARE @sTParentColumnIDSystemName varchar(50)
					SELECT @sTParentColumnIDSystemName=SystemName FROM [Column] WHERE ColumnID=@nSParentColumnID
					SELECT @nTParentColumnID=columnID FROM [Column] 
					WHERE TableID=@nTTableTableID AND SystemName=@sTParentColumnIDSystemName
					
					IF @nTParentColumnID IS NOT NULL
					UPDATE [Column] SET ParentColumnID=@nTParentColumnID 
					WHERE ColumnID=@nTColumnID
					
				END			 
			 			
			END
			
			PRINT 'LinkedParentColumnID Each'
			FETCH FROM Cur_Column INTO @nTColumnID,@nSTableTableID,@nSLinkedParentColumnID,@nSParentColumnID;
		END
    CLOSE Cur_Column
	DEALLOCATE Cur_Column	                      



    PRINT 'LinkedParentColumnID ALL DONE'
	--work on Conditions table
	
	
	DECLARE Cur_HideColumn CURSOR FOR
	SELECT ConditionsID FROM Conditions INNER JOIN [Column]
	ON Conditions.ColumnID=[Column].ColumnID INNER JOIN [Table]
	ON [Table].TableID=[Column].TableID WHERE [Table].AccountID=@nTemplateAccountID

	DECLARE @nSConditionsID INT;
	OPEN Cur_HideColumn;


	FETCH FROM Cur_HideColumn INTO @nSConditionsID;
	

	WHILE @@FETCH_STATUS = 0
	  BEGIN
		DECLARE @nSColumnID INT
		SET @nSConditionColumnID =NULL
		
		SET @nTColumnID =NULL
		SET @nTConditionColumnID =NULL
		
		DECLARE @sSDisplayName varchar(50)
		DECLARE @sSTableName nvarchar(255)
		
		SELECT @nSColumnID=ColumnID,@nSConditionColumnID=ConditionColumnID 
		FROM Conditions WHERE ConditionsID=@nSConditionsID
		
		SELECT @sSDisplayName=DisplayName,@sSTableName=@sSDisplayName 
		FROM [Column]
		INNER JOIN [Table] ON [Column].TableID=[Table].TableID
		WHERE ColumnID=@nSColumnID
		
		SELECT @nTColumnID=ColumnID FROM [Column] INNER JOIN [Table]
		ON [Column].TableID=[Table].TableID
		WHERE DisplayName=@sSDisplayName AND @sSDisplayName=@sSTableName
		AND AccountID=@nTargetAccountID
		
		IF @nTColumnID IS NOT NULL
		BEGIN
			IF @nSConditionColumnID IS NOT NULL
			BEGIN
			SET @sSDisplayName=NULL
			SET @sSTableName=NULL
			SELECT @sSDisplayName=DisplayName,@sSTableName=@sSDisplayName FROM [Column]
			INNER JOIN [Table] ON [Column].TableID=[Table].TableID
			WHERE ColumnID=@nSConditionColumnID
			
			SELECT @nTConditionColumnID=ColumnID FROM [Column] INNER JOIN [Table]
			ON [Column].TableID=[Table].TableID
			WHERE DisplayName=@sSDisplayName AND @sSDisplayName=@sSTableName
			AND AccountID=@nTargetAccountID
			
			END
			
		END
		
		INSERT INTO Conditions(ColumnID,ConditionColumnID,ConditionValue,ConditionOperator,
	DisplayOrder,JoinOperator) SELECT @nTColumnID,@nTConditionColumnID,ConditionValue,ConditionOperator,
	DisplayOrder,JoinOperator FROM Conditions WHERE ConditionsID=@nSConditionsID
		
		PRINT 'INSERT INTO Conditions Each'
		
		FETCH FROM Cur_HideColumn INTO @nSConditionsID;
	  END

	CLOSE Cur_HideColumn
	DEALLOCATE Cur_HideColumn	
			
   PRINT 'Conditions all DONE'
   -- lets work on Dashboard
   
   INSERT INTO Document (AccountID, DocumentText, DocumentTypeID, UniqueName, FileTitle, DocumentDate, DateAdded, DateUpdated, UserID, TableID, ReportHTML, 
                      IsReportPublic, DocumentDescription, DocumentEndDate, ForDashBoard, FolderID, Size)
   SELECT @nTargetAccountID,DocumentText, NULL, UniqueName, FileTitle, DocumentDate, DateAdded, DateUpdated, @nSignUpUserID, NULL, ReportHTML, 
                      IsReportPublic, DocumentDescription, DocumentEndDate, ForDashBoard, NULL, Size 
   FROM Document WHERE ForDashBoard=1 AND   AccountID=  @nTemplateAccountID                                        
   
    PRINT 'Dashboard DONE'
   
   -- need to work with DocumentSection
	-- excluded DocumentSectionStyleID

	
	DECLARE Cur_DocumentSection CURSOR FOR	
	SELECT DocumentSectionID,DocumentSection.DocumentID,ParentSectionID,Document.DateAdded
	FROM DocumentSection INNER JOIN Document 
	ON DocumentSection.DocumentID=Document.DocumentID
	WHERE ForDashBoard=1 AND Document.AccountID=@nTemplateAccountID 
	
	DECLARE @nSDocumentSectionID int;
	DECLARE @nSDocumentID INT;
	DECLARE @sSParentSectionID  INT;
	DECLARE @dDateAdded datetime;
	
	OPEN Cur_DocumentSection;
	FETCH FROM Cur_DocumentSection INTO @nSDocumentSectionID,@nSDocumentID,@sSParentSectionID,@dDateAdded;

	WHILE @@FETCH_STATUS = 0
	  BEGIN
		
		SELECT @nSDocumentID=DocumentID FROM Document WHERE AccountID=@nTargetAccountID
		AND DateAdded=@dDateAdded and ForDashBoard=1
		
		IF @sSParentSectionID IS NOT NULL
		BEGIN
			DECLARE @nPosition int
			SELECT @sSParentSectionID=DocumentID,@nPosition=Position FROM DocumentSection 
			WHERE DocumentSectionID=@sSParentSectionID
					
			SELECT @dDateAdded=DateAdded FROM Document WHERE DocumentID=@sSParentSectionID
			
			SELECT @sSParentSectionID=DocumentID FROM Document WHERE AccountID=@nTargetAccountID
			AND DateAdded=@dDateAdded
			
			IF @sSParentSectionID IS  NOT NULL
			SELECT @sSParentSectionID=DocumentSectionID FROM DocumentSection
			WHERE DocumentID=@sSParentSectionID AND Position=@nPosition
		END
		
		IF @nSDocumentID IS NOT NULL		
		INSERT INTO DocumentSection (DocumentID, DocumentSectionTypeID, Position, SectionName, [Content], Filter, Details, DateAdded, DateUpdated, DocumentSectionStyleID, 
            ValueFields, ParentSectionID, ColumnIndex)
			SELECT @nSDocumentID, DocumentSectionTypeID, Position, SectionName, [Content], Filter, Details, DateAdded, DateUpdated, NULL, 
            ValueFields, @sSParentSectionID, ColumnIndex
			FROM DocumentSection  WHERE DocumentSectionID=@nSDocumentSectionID
	    
		 PRINT 'INSERT INTO DocumentSection EACH'
		
		FETCH FROM Cur_DocumentSection INTO @nSDocumentSectionID,@nSDocumentID,@sSParentSectionID,@dDateAdded;
	  END

	CLOSE Cur_DocumentSection
	DEALLOCATE Cur_DocumentSection

	  PRINT 'DocumentSection all DONE'
	 
	 --NO NEED Table
	 -- TableUser                   
	
	
	------
	
	--Terminology
	INSERT INTO Terminology (AccountID, PageName, InputText, OutputText)
	SELECT @nTargetAccountID,PageName, InputText, OutputText
	FROM Terminology WHERE AccountID=@nTemplateAccountID
	
	 PRINT 'Terminology DONE'
	
	
	PRINT 'Commit'
	COMMIT TRANSACTION --finally, Commit 

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_Copy_Account_SignUp', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
