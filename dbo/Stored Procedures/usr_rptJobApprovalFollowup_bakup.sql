﻿CREATE procedure [dbo].[usr_rptJobApprovalFollowup_bakup]

as

SELECT	         [Job Note].V009	as ProjectNumber          
				,MAX([Job Note].V003)						AS MaxOfDate
			

	into #BidMaxNoteDate
	FROM		[Record] [Job Note] WITH (NOLOCK)
	
	LEFT JOIN	Record [Job No] ON					[Job No].TableID = '2475'		/*	Job Information	*/
											AND		[Job No].IsActive = 1
											AND		CAST( [Job No].RecordID AS VARCHAR(MAX)) = [Job Note].V009		/*	Job No	*/
	
	WHERE		[Job Note].TableID = '2676'		/*	Job Note	*/
	AND			[Job Note].IsActive = 1
	GROUP BY [Job Note].V009
ORDER BY ProjectNumber;




SELECT Project.[Group/Division] AS GroupDivision, 
Project.ProjectNumber AS JobNo, 
Project.ProjectTitle,
 Notes.Date AS NoteDate,
  Notes.Initials,
   Employee_List.Name AS Controller,
    Notes.Subject AS Subject, 
	Project.FixedPrice,
	 Project.Value AS ProjectValue,
	 '' as  Company FROM Account24918.vClient  Client
	  INNER JOIN (((  #BidMaxNoteDate BidMaxNoteDate INNER JOIN Account24918.vNotes Notes ON (BidMaxNoteDate.ProjectNumber = Notes.ProjectNumber) AND (BidMaxNoteDate.MaxOfDate = Notes.Date)) 
	  INNER JOIN Account24918.vProject Project ON Notes.ProjectNumber = Project.ProjectNumber)
	   INNER JOIN Account24918.vEmployee_List Employee_List ON Project.EmployeeID = Employee_List.EmployeeID) ON ''  = Project.ClientID 
	            WHERE (((Notes.Date) <= '01-01-2016')         AND ((Project.ProjectNumber) In (SELECT top 10 ProjectNumber FROM Account24918.vTempJobNumbersCollection TempJobNumbersCollection  )))  
	     UNION ALL
		  SELECT    Project.[Group/Division] AS GroupDivision, Project.ProjectNumber AS JobNo, Project.ProjectTitle,      Notes.Date AS NoteDate,
		   Notes.Initials, Employee_List.Name AS Controller, Notes.Subject AS Subject, Project.FixedPrice,    Project.Value, '' as Company  
		      FROM     Account24918.vClient Client INNER JOIN ((Account24918.vProject  Project LEFT JOIN Account24918.vNotes Notes ON Project.ProjectNumber = Notes.ProjectNumber)    
			    iNNER JOIN Account24918.vEmployee_List Employee_List ON Project.EmployeeID = Employee_List.EmployeeID) ON '' = Project.ClientID   
				      WHERE      Project.ProjectNumber in (                                                   SELECT ProjectNumber FROM Account24918.vTempJobNumbersCollection TempJobNumbersCollection where   ((   (  Notes.Date BETWEEN '01-01-2016'   AND '01-05-2016'                                                                                       AND Project.Status= 1                                                                                   ) OR      (   ((Notes.Date) Is Null) AND     (Project.Status=1))                                                                            ))                                                   )
ORDER BY GroupDivision, Controller, JobNo, NoteDate DESC;

