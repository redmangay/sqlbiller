﻿
CREATE PROCEDURE [dbo].[usr_rptCurrentBidsActivity]
(
	@From DATE, --= '01/10/2018',
	@To DATE, --= --'30/10/2018',
	@Group VARCHAR(MAX) =null,
	@Controller VARCHAR(MAX) =null,
	@OrderByColumn int =1
)
AS

/*

EXEC [dbo].[usr_rptCurrentBidsActivity] @From= '01/01/2020', @To ='12/02/2020', @OrderByColumn =2 , @Group ='16605069', @Controller='1'


*/

BEGIN

SET dateformat dmy;
DECLARE @SQL VARCHAR(MAX)
DECLARE @SQLGroup VARCHAR(MAX) = ''
DECLARE @SQLController VARCHAR(MAX) = ''
DECLARE @SQLOrderBy VARCHAR(MAX) = ''
DECLARE @IsOkayBeyod bit = 0
DECLARE @IsOkayWithin bit = 0

IF @Group <> 1 AND @Group is not null
BEGIN
SET @SQLGroup = ' AND [Job Information].V027 =''' + @Group + ''''
END

IF @Controller <> 1 AND @Controller is not null
BEGIN
SET @SQLController = ' AND [Job Information].V026 =''' + @Controller + ''''
END


IF(@OrderByColumn = 1)
BEGIN
SET @SQLOrderBy = ' ORDER BY [Job Date] '
END
ELSE IF (@OrderByColumn = 2)
BEGIN
SET @SQLOrderBy = ' ORDER BY [Job No] '
END

DECLARE @CurrentBidsSuccess TABLE (

[Counter] int identity(1,1),
[Job Date] date,
[Job Controller] varchar(max),
[Job No] varchar(max),
[Job Client] varchar(max),
[Job Title] varchar(max),
[Job Value] money,
[Current Value] money,
[Number of Bids] varchar(max),
[Currency] varchar(max),
[Group Name] varchar(max),
[Note Job No] varchar(max),
[Note Date] DATE,
[Initial] varchar(max),
[Note] varchar(max),
[Converted Symbol] varchar(10),
[Header] varchar(max),
[Note RecordID] int,
[Job RecordID] int




)


DECLARE @CurrentBidsSuccessBeyondRange TABLE (

[Counter] int ,
[Job Date] date,
[Job Controller] varchar(max),
[Job No] varchar(max),
[Job Client] varchar(max),
[Job Title] varchar(max),
[Job Value] money,
[Current Value] money,
[Number of Bids] varchar(max),
[Currency] varchar(max),
[Group Name] varchar(max),
[Note Job No] varchar(max),
[Note Date] varchar(max),
[Initial] varchar(max),
[Note] varchar(max),
[Converted Symbol] varchar(10),
[Header] varchar(max),
[Note RecordID] int,
[Job RecordID] int,
[Rank] varchar(20)




)



DECLARE @CurrentBidsSuccessFinal TABLE (

[Counter] int ,
[Job Date] date,
[Job Controller] varchar(max),
[Job No] varchar(max),
[Job Client] varchar(max),
[Job Title] varchar(max),
[Job Value] money,
[Current Value] money,
[Number of Bids] varchar(max),
[Currency] varchar(max),
[Group Name] varchar(max),
[Note Job No] varchar(max),
[Note Date] DATE,
[Initial] varchar(max),
[Note] varchar(max),
[Converted Symbol] varchar(10),
[Header] varchar(max),
[Note RecordID] int,
[Job RecordID] int,
[Rank] varchar(20)




)
   



   
DECLARE @CurrentBidsSuccessFinal2 TABLE (
[Counterx] int identity(1,1),
[Counter] int ,
[Job Date] date,
[Job Controller] varchar(max),
[Job No] varchar(max),
[Job Client] varchar(max),
[Job Title] varchar(max),
[Job Value] money,
[Current Value] money,
[Number of Bids] varchar(max),
[Currency] varchar(max),
[Group Name] varchar(max),
[Note Job No] varchar(max),
[Note Date] varchar(max),
[Initial] varchar(max),
[Note] varchar(max),
[Converted Symbol] varchar(10),
[Header] varchar(max),
[Note RecordID] int,
[Job RecordID] int,
[Rank] varchar(20)




)
   
SET @SQL =	'
SET dateformat dmy;
SELECT convert(date,[Job Information].[V002])  AS [Job Date]
	   ,ISNULL([EmployeeList_Job Controller].V003,'''') AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
	   , [Job Information].V001 AS [Job No]
	   ,ISNULL([Client].[V001],''-- NONE --'') AS [Job Client]
	   ,ISNULL([Job Information].V003,''-- NONE --'') AS [Job Title]--Job Title, get from Job Information V003
	   ,0--Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) AS [Job Value]
	   ,0--Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) AS [Current Value]
	  
	   ,1 AS [Number of Bids]
	   ,[Reporting Currency].V001 AS [Currency]	
	   ,ISNULL([Job Group].[V001],'''') AS [Group Name]	
	      ,  [Job Notes].V009 AS [Note Job No]
	   ,  [Job Notes].V003 AS [Note Date]

	    , [EmployeeList_List].V004 AS [Initial]
	   , [Job Notes].V006 AS [Note]		
	   ,CASE 
		  WHEN [Currency Rates].V001 LIKE ''%None%'' THEN NULL 
		  WHEN [Job Group].V004 LIKE ''%None%'' THEN NULL  
		  WHEN [Currency Rates].V001 = [Job Group].V004 THEN NULL 
		  ELSE convert(varchar,''C'') END AS [Converted Symbol]
	    , 	(SELECT V039  FROM RECORD WHERE TABLEID = 2743) AS [Header] 
	    ,  [Job Notes].RecordID
		, /*CASE WHEN [Job Notes].V003 IS NULL AND [EmployeeList_List].V004 IS NULL AND [Job Notes].V006 IS NULL  THEN*/ [Job Information].RecordID /*ELSE null END*/ as [Job RecordID]
	--   ,[Job Information].V008
	--, [Job Information].V007
	--,[Group Currency Rates].V002
	--,[Currency Rates].V002
			FROM [Record]  [Job Notes] WITH (NOLOCK) 

			 JOIN Record [Job Information]  WITH (NOLOCK) ON [Job Information].TableID = 2475
			AND	[Job Information].IsActive = 1 
			AND			 [Job Information].[V021] IN (''Bid'')
			 AND [Job Information].RecordID  = CAST([Job Notes].[V009] as int)

			 LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		[EmployeeList_Job Controller].RecordID = CAST([Job Information].V026 as int) 		/*	Job Number	*/

			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		[Currency Rates].RecordID  = CAST([Job Information].V034 as int) 		/*	Job Number	*/

			 LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		[Tax Name].RecordID = CAST([Job Information].V042 as int)		/*	Job Number	*/
			 
			LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
				AND			[Job Group].IsActive = 1
				AND		[Job Group].RecordID = CAST([Job Information].V027 as int)		/*	Currency	*/
				--AND [Job Group].RecordID =16605069

			 LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/
			 LEFT JOIN Record [Client] WITH (NOLOCK) ON [Client].TableID = 2474		/*	Currency Rates	*/
				AND			[Client].IsActive = 1
				AND	[Client].RecordID = CAST([Job Information].V024 as int)		/*	[Group Currency]	*/
			 --JOIN Record [Job Notes] WITH (NOLOCK) ON
					 
		    LEFT JOIN [Record] [Reporting Currency] WITH (NOLOCK) ON [Reporting Currency].TableID = 2665
				AND		[Reporting Currency].IsActive = 1
				AND		[Reporting Currency].RecordID = CAST([Job Group].V005 as int) 		/*	Job Number	*/

			   JOIN [Record] [EmployeeList_List] WITH (NOLOCK) ON [EmployeeList_List].TableID = 2669 /*	Employee List	*/
				AND		[EmployeeList_List].IsActive = 1
				AND		[EmployeeList_List].RecordID = CAST([Job Notes].V007 as int) 		/*	Job Initial	*/

					WHERE  [Job Notes].TableID = 2676		/*	Job Notes	*/
				AND			[Job Notes].IsActive = 1
				
				--AND CONVERT(DATE,[Job Notes].V003) >=  CONVERT(date,'''+ cast( @From as varchar) + ''' )   AND
					--CONVERT(DATE,[Job Notes].V003) <= CONVERT(date,'''+cast(@To as varchar) + ''') 
					
					--and [Job Information].V001 = ''wd362-02'' --and --''WA597-08''  AND -- ''WD652-02''
					--[Job Information].RecordID =    15223026 and
					
					
					
					
					--ORDER BY [Job Information].v001
					--ORDER BY CASE @OrderByColumn
					--		 WHEN 1 THEN [Job Information].[V002] 
					--		 WHEN 2 THEN [Job Information].[V001] 
					--			END DESC'




INSERT INTO @CurrentBidsSuccess EXEC(@SQL);
--select * from @CurrentBidsSuccess


/* Within the range */
SET @SQL =	' 
--DECLARE @OrderByColumn INT = '+ CAST(@OrderByColumn AS VARCHAR)  +'
SET dateformat dmy;
SELECT null,convert(date,[Job Information].[V002])  AS [Job Date]
	   , ISNULL([EmployeeList_Job Controller].V003,'''') AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
	   , [Job Information].V001 AS [Job No]
	   ,ISNULL([Client].[V001],''-- NONE --'') AS [Job Client]
	   , ISNULL([Job Information].V003,''-- NONE --'') AS [Job Title]--Job Title, get from Job Information V003
	   ,Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) AS [Job Value]
	   ,Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Currency Rates].V002,1)) * convert(money,ISNULL([Group Currency Rates].V002,1)) AS MONEY),0),2) AS [Current Value]
	  
	   ,1 AS [Number of Bids]
	   ,[Reporting Currency].V001 AS [Currency]	
	   ,ISNULL([Job Group].[V001],'''') AS [Group Name]	
	      ,''''-- [Job Notes].V009 AS [Note Job No]
	   , NULL--  [Job Notes].V003 AS [Note Date]

	    ,''''-- [EmployeeList_List].V004 AS [Initial]
	   ,''''-- [Job Notes].V006 AS [Note]		
	   ,CASE 
		  WHEN [Currency Rates].V001 LIKE ''%None%'' THEN NULL 
		  WHEN [Job Group].V004 LIKE ''%None%'' THEN NULL  
		  WHEN [Currency Rates].V001 = [Job Group].V004 THEN NULL 
		  ELSE convert(varchar,''C'') END AS [Converted Symbol]
	    , 	(SELECT V039  FROM RECORD WHERE TABLEID = 2743) AS [Header] 
	    ,''''--   [Job Notes].RecordID
	    , /*CASE WHEN [Job Notes].V003 IS NULL AND [EmployeeList_List].V004 IS NULL AND [Job Notes].V006 IS NULL  THEN*/ [Job Information].RecordID /*ELSE null END*/ as [Job RecordID]
	,''''
	--   ,[Job Information].V008
	--, [Job Information].V007
	--,[Group Currency Rates].V002
	--,[Currency Rates].V002
			FROM [Record] [Job Information] WITH (NOLOCK) 

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		[EmployeeList_Job Controller].RecordID = CAST([Job Information].V026 as int)		/*	Job Number	*/

			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		[Currency Rates].RecordID  = CAST([Job Information].V034 as int) 		/*	Job Number	*/

			LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		[Tax Name].RecordID = CAST([Job Information].V042 as int)		/*	Job Number	*/
			 
			 JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
				AND			[Job Group].IsActive = 1
				AND		[Job Group].RecordID = CAST([Job Information].V027 as int)		/*	Currency	*/
				--AND [Job Group].v001 =''Building Acoustics Team''

			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/
			LEFT JOIN Record [Client] WITH (NOLOCK) ON [Client].TableID = 2474		/*	Currency Rates	*/
				AND			[Client].IsActive = 1
				AND	[Client].RecordID = CAST([Job Information].V024 as int)		/*	[Group Currency]	*/
			
			--LEFT JOIN Record [Job Notes] WITH (NOLOCK) ON [Job Notes].TableID = 2676		/*	Job Notes	*/
			--	AND			[Job Notes].IsActive = 1
			--	AND [Job Notes].[V009]  = CAST([Job Information].RecordID AS VARCHAR)
			--	AND CONVERT(DATE,[Job Notes].V003) >= CONVERT(date,'''+ CAST(@From AS VARCHAR) +''')  AND
			--		CONVERT(DATE,[Job Notes].V003) <= CONVERT(date,'''+ CAST(@To AS VARCHAR) + ''') 
		   LEFT JOIN [Record] [Reporting Currency] WITH (NOLOCK) ON [Reporting Currency].TableID = 2665
				AND		[Reporting Currency].IsActive = 1
				AND		[Reporting Currency].RecordID = CAST([Job Group].V005 as int) 		/*	Job Number	*/

			 --LEFT JOIN [Record] [EmployeeList_List] WITH (NOLOCK) ON [EmployeeList_List].TableID = 2669 /*	Employee List	*/
				--AND		[EmployeeList_List].IsActive = 1
				--AND		CAST([EmployeeList_List].RecordID AS VARCHAR(MAX)) = [Job Notes].V007 		/*	Job Initial	*/

					WHERE [Job Information].TableID = 2475 AND
					[Job Information].IsActive = 1 AND
					--and [Job Information].V001 = ''wd362-02''	--[Job Information].V001 =  ''tj780-01'' and -- ''WC874-01'' and --''WA597-08''  AND -- ''WD652-02''
					[Job Information].[V021] IN (''Bid'')
					' + @SQLGroup + '
					' + @SQLController + '
					
					ORDER BY [Job Information].v001
					--ORDER BY CASE @OrderByColumn
					--		 WHEN 1 THEN [Job Information].[V002] 
					--		 WHEN 2 THEN [Job Information].[V001] 
					--			END DESC'




INSERT INTO @CurrentBidsSuccessFinal EXEC(@SQL);
--select * from @CurrentBidsSuccessFinal

--SELECT * FROM @CurrentBidsSuccess
--SELECT * FROM @CurrentBidsSuccessFinal
/* End */


/* All Notes */
--SELECT * FROM @CurrentBidsSuccess
--WHERE convert(date,[NOTE DATE]) < @From
--ORDER BY convert(date,[Note Date]) desc
IF(
(SELECT COUNT(*)
 FROM @CurrentBidsSuccess CB
WHERE CONVERT(DATE,CB.[Note Date]) >= CAST(@From AS DATE) and CONVERT(DATE,CB.[Note Date]) <= CAST(@To AS DATE)) > 0 )
BEGIN
SET @IsOkayWithin = 1

/* LETS GET THE Notes within the date range */
INSERT INTO @CurrentBidsSuccessFinal
SELECT   CB.[Counter]  ,
CB.[Job Date]  ,
CB.[Job Controller] ,
CB.[Job No]  ,
CB.[Job Client]  ,
CB.[Job Title]  ,
CB.[Job Value]  ,
CB.[Current Value]  ,
CB.[Number of Bids]  ,
CB.[Currency]  ,
CB.[Group Name]  ,
CB.[Note Job No]  ,
CB.[Note Date]  ,
CB.[Initial] ,
CB.[Note]  ,
CB.[Converted Symbol]  ,
CB.[Header] ,
 CB.[Note RecordID] ,
null,''   FROM @CurrentBidsSuccess CB
WHERE CONVERT(DATE,CB.[Note Date]) >= CAST(@From AS DATE) and CONVERT(DATE,CB.[Note Date]) <= CAST(@To AS DATE)

END


/*
    /* LETS GET THE MOST RECENT NOTES */
INSERT INTO @CurrentBidsSuccessFinal
SELECT *
    FROM (SELECT  
*,
			 ROW_NUMBER() OVER (PARTITION BY MostRecentBeyondRange.[Job No] ORDER BY CONVERT(date,MostRecentBeyondRange.[Note Date]) desc) rank
	   FROM @CurrentBidsSuccess MostRecentBeyondRange
	   WHERE MostRecentBeyondRange.[Note Date] > CAST(@To AS DATE)
	
	   ) [MostRecentBeyondRange]
    WHERE [MostRecentBeyondRange].rank = 1 


/* END */


--Insert INTO @CurrentBidsSuccessBeyondRange

UPDATE MostRecentBeyondRangeToUpdate
SET MostRecentBeyondRangeToUpdate.[Job RecordID] = null
FROM @CurrentBidsSuccessFinal MostRecentBeyondRangeToUpdate JOIN(
   SELECT  *
	   FROM @CurrentBidsSuccess MostRecentBelowRange1
	   WHERE MostRecentBelowRange1.[Note Date] > CAST(@To AS DATE)
	
	   ) MostRecentBelowRange2
    ON MostRecentBelowRange2.[Job RecordID] = MostRecentBeyondRangeToUpdate.[Job RecordID]
 
    WHERE MostRecentBeyondRangeToUpdate.[Note Date] is null 
   --AND  MostRecentBelowRange.[Note Date] < CAST(@From AS DATE)
  --  AND MostRecentBelowRange.[Note Date] is null
  --ORDER BY MostRecentBelowRange.[Job No]



  */
  
DECLARE @CurrentBidsSuccessJobsBelowNotes TABLE (
[Counter] int identity(1,1),
[Note Date] date,
[Note RecordID] int
)

/* ========================================================================================================================================= */


/* LETS GET THE MOST RECENT NOTES WHEN JOB HAS NO NOTES WITHIN THE DATES RANGE */
INSERT INTO @CurrentBidsSuccessJobsBelowNotes ([Note Date],[Note RecordID])
SELECT  
MostRecentBelowRange2.[Note Date]  ,
 MostRecentBelowRange2.[Note RecordID] --,
FROM @CurrentBidsSuccessFinal MostRecentBelowRange JOIN(
   SELECT  
[Note Date]  ,
[Note RecordID] ,
[Job RecordID]  ,  
			 ROW_NUMBER() OVER (PARTITION BY MostRecentBelowRange1.[Job RecordID] ORDER BY CONVERT(date,MostRecentBelowRange1.[Note Date]) desc) rank
	   FROM @CurrentBidsSuccess MostRecentBelowRange1
	   WHERE CONVERT(DATE,MostRecentBelowRange1.[Note Date]) > CAST(@To AS DATE)
	   
	
	   ) MostRecentBelowRange2
    ON MostRecentBelowRange2.rank = 1 and 
    MostRecentBelowRange2.[Job RecordID] = MostRecentBelowRange.[Job RecordID]
 
    WHERE MostRecentBelowRange.[Note Date] is null 
    --and  MostRecentBelowRange.[Counter] <> null
   --AND  MostRecentBelowRange.[Note Date] < CAST(@From AS DATE)
  --  AND MostRecentBelowRange.[Note Date] is null
  ORDER BY MostRecentBelowRange.[Job No]


  IF ((SELECT COUNT(*) FROM @CurrentBidsSuccessJobsBelowNotes) > 0)
  BEGIN
  SET   @IsOkayBeyod = 1

  
  INSERT INTO @CurrentBidsSuccessFinal
 SELECT 
  CB.[Counter]  ,
CB.[Job Date]  ,
CB.[Job Controller] ,
CB.[Job No]  ,
CB.[Job Client]  ,
CB.[Job Title]  ,
CB.[Job Value]  ,
CB.[Current Value]  ,
CB.[Number of Bids]  ,
CB.[Currency]  ,
CB.[Group Name]  ,
CB.[Note Job No]  ,
CB.[Note Date]  ,
CB.[Initial] ,
CB.[Note]  ,
CB.[Converted Symbol]  ,
CB.[Header] ,
 CB.[Note RecordID] ,
null,''   
  FROM @CurrentBidsSuccess CB
 JOIN @CurrentBidsSuccessJobsBelowNotes CBSJB ON CBSJB.[Note RecordID] = CB.[Note RecordID]
 AND CBSJB.[Note Date] = CB.[Note Date]

 delete  from @CurrentBidsSuccessJobsBelowNotes

  END



  IF (@IsOkayBeyod =1)
  BEGIN
INSERT INTO @CurrentBidsSuccessJobsBelowNotes ([Note Date],[Note RecordID])
SELECT  
MostRecentBelowRange2.[Note Date]  ,
 MostRecentBelowRange2.[Note RecordID] --,
FROM @CurrentBidsSuccessFinal MostRecentBelowRange JOIN(
   SELECT 
[Note Date]  ,
[Note RecordID] ,
[Job RecordID]  --,  
	   FROM @CurrentBidsSuccess MostRecentBelowRange1
	   WHERE CONVERT(DATE,MostRecentBelowRange1.[Note Date]) > CAST(@To AS DATE)
	   
	
	   ) MostRecentBelowRange2
    ON  MostRecentBelowRange.[Job RecordID] = MostRecentBelowRange2.[Job RecordID]
    AND  MostRecentBelowRange.[Note Date] = MostRecentBelowRange2.[Note Date]
    AND  MostRecentBelowRange.[Note RecordID] <> MostRecentBelowRange2.[Note RecordID]
    --and  MostRecentBelowRange.[Counter] <> null
  
  INSERT INTO @CurrentBidsSuccessFinal
 SELECT 
  CB.[Counter]  ,
CB.[Job Date]  ,
CB.[Job Controller] ,
CB.[Job No]  ,
CB.[Job Client]  ,
CB.[Job Title]  ,
CB.[Job Value]  ,
CB.[Current Value]  ,
CB.[Number of Bids]  ,
CB.[Currency]  ,
CB.[Group Name]  ,
CB.[Note Job No]  ,
CB.[Note Date]  ,
CB.[Initial] ,
CB.[Note]  ,
CB.[Converted Symbol]  ,
CB.[Header] ,
 CB.[Note RecordID] ,
CB.[Job RecordID],''   
  FROM @CurrentBidsSuccess CB
 JOIN @CurrentBidsSuccessJobsBelowNotes CBSJB ON CBSJB.[Note RecordID] = CB.[Note RecordID]
 AND CBSJB.[Note Date] = CB.[Note Date]


  delete  from @CurrentBidsSuccessJobsBelowNotes
  END
/* ========================================================================================================================================= */





















--IF (@IsOkayBeyod = 0 AND @IsOkayWithin = 0)
--BEGIN
/* LETS GET THE MOST RECENT NOTES WHEN JOB HAS NO NOTES WITHIN THE DATES RANGE */
INSERT INTO @CurrentBidsSuccessJobsBelowNotes ([Note Date],[Note RecordID])
SELECT  --MostRecentBelowRange2.[Counter]  ,
--MostRecentBelowRange2.[Job Date]  ,
--MostRecentBelowRange2.[Job Controller] ,
--MostRecentBelowRange2.[Job No]  ,
--MostRecentBelowRange2.[Job Client]  ,
--MostRecentBelowRange2.[Job Title]  ,
--MostRecentBelowRange2.[Job Value]  ,
--MostRecentBelowRange2.[Current Value]  ,
--MostRecentBelowRange2.[Number of Bids]  ,
--MostRecentBelowRange2.[Currency]  ,
--MostRecentBelowRange2.[Group Name]  ,
--MostRecentBelowRange2.[Note Job No]  ,
MostRecentBelowRange2.[Note Date]  ,
--MostRecentBelowRange2.[Initial] ,
--MostRecentBelowRange2.[Note]  ,
--MostRecentBelowRange2.[Converted Symbol]  ,
--MostRecentBelowRange2.[Header] ,
 MostRecentBelowRange2.[Note RecordID] --,
--MostRecentBelowRange2.[Job RecordID],''   
FROM @CurrentBidsSuccessFinal MostRecentBelowRange JOIN(
   SELECT  --[Counter]  ,
--[Job Date]  ,
--[Job Controller] ,
--[Job No]  ,
--[Job Client]  ,
--[Job Title]  ,
--[Job Value]  ,
--[Current Value]  ,
--[Number of Bids]  ,
--[Currency]  ,
--[Group Name]  ,
--[Note Job No]  ,
[Note Date]  ,
--[Initial],
--[Note]  ,
--[Converted Symbol]  ,
--[Header] ,
[Note RecordID] ,
[Job RecordID]  ,  
			 ROW_NUMBER() OVER (PARTITION BY MostRecentBelowRange1.[Job RecordID] ORDER BY CONVERT(date,MostRecentBelowRange1.[Note Date]) desc) rank
	   FROM @CurrentBidsSuccess MostRecentBelowRange1
	   WHERE CONVERT(DATE,MostRecentBelowRange1.[Note Date]) < CAST(@From AS DATE)
	   
	
	   ) MostRecentBelowRange2
    ON MostRecentBelowRange2.rank = 1 and 
    MostRecentBelowRange2.[Job RecordID] = MostRecentBelowRange.[Job RecordID]
 
    WHERE MostRecentBelowRange.[Note Date] is null --and
   -- MostRecentBelowRange.[Counter] <> null
   --AND  MostRecentBelowRange.[Note Date] < CAST(@From AS DATE)
  --  AND MostRecentBelowRange.[Note Date] is null
  ORDER BY MostRecentBelowRange.[Job No]


  INSERT INTO @CurrentBidsSuccessFinal
 SELECT 
  CB.[Counter]  ,
CB.[Job Date]  ,
CB.[Job Controller] ,
CB.[Job No]  ,
CB.[Job Client]  ,
CB.[Job Title]  ,
CB.[Job Value]  ,
CB.[Current Value]  ,
CB.[Number of Bids]  ,
CB.[Currency]  ,
CB.[Group Name]  ,
CB.[Note Job No]  ,
CB.[Note Date]  ,
CB.[Initial] ,
CB.[Note]  ,
CB.[Converted Symbol]  ,
CB.[Header] ,
 CB.[Note RecordID] ,
CB.[Job RecordID],''   
  FROM @CurrentBidsSuccess CB
 JOIN @CurrentBidsSuccessJobsBelowNotes CBSJB ON CBSJB.[Note RecordID] = CB.[Note RecordID]
 AND CBSJB.[Note Date] = CB.[Note Date]

 delete  from @CurrentBidsSuccessJobsBelowNotes


 -- INSERT INTO @CurrentBidsSuccessFinal
INSERT INTO @CurrentBidsSuccessJobsBelowNotes ([Note Date],[Note RecordID])
SELECT  --MostRecentBelowRange2.[Counter]  ,
--MostRecentBelowRange2.[Job Date]  ,
--MostRecentBelowRange2.[Job Controller] ,
--MostRecentBelowRange2.[Job No]  ,
--MostRecentBelowRange2.[Job Client]  ,
--MostRecentBelowRange2.[Job Title]  ,
--MostRecentBelowRange2.[Job Value]  ,
--MostRecentBelowRange2.[Current Value]  ,
--MostRecentBelowRange2.[Number of Bids]  ,
--MostRecentBelowRange2.[Currency]  ,
--MostRecentBelowRange2.[Group Name]  ,
--MostRecentBelowRange2.[Note Job No]  ,
MostRecentBelowRange2.[Note Date]  ,
--MostRecentBelowRange2.[Initial] ,
--MostRecentBelowRange2.[Note]  ,
--MostRecentBelowRange2.[Converted Symbol]  ,
--MostRecentBelowRange2.[Header] ,
 MostRecentBelowRange2.[Note RecordID] --,
--MostRecentBelowRange2.[Job RecordID],''   
FROM @CurrentBidsSuccessFinal MostRecentBelowRange JOIN(
   SELECT  --[Counter]  ,
--[Job Date]  ,
--[Job Controller] ,
--[Job No]  ,
--[Job Client]  ,
--[Job Title]  ,
--[Job Value]  ,
--[Current Value]  ,
--[Number of Bids]  ,
--[Currency]  ,
--[Group Name]  ,
--[Note Job No]  ,
[Note Date]  ,
--[Initial],
--[Note]  ,
--[Converted Symbol]  ,
--[Header] ,
[Note RecordID] ,
[Job RecordID]  --,  
			-- ROW_NUMBER() OVER (PARTITION BY MostRecentBelowRange1.[Job RecordID] ORDER BY CONVERT(date,MostRecentBelowRange1.[Note Date]) desc) rank
	   FROM @CurrentBidsSuccess MostRecentBelowRange1
	   WHERE CONVERT(DATE,MostRecentBelowRange1.[Note Date]) < CAST(@From AS DATE)
	   
	
	   ) MostRecentBelowRange2
    ON  MostRecentBelowRange.[Job RecordID] = MostRecentBelowRange2.[Job RecordID]
    AND  MostRecentBelowRange.[Note Date] = MostRecentBelowRange2.[Note Date]
    AND  MostRecentBelowRange.[Note RecordID] <> MostRecentBelowRange2.[Note RecordID]
   -- AND  MostRecentBelowRange.[Note Date] IS NOT NULL
 


  
  INSERT INTO @CurrentBidsSuccessFinal
 SELECT 
  CB.[Counter]  ,
CB.[Job Date]  ,
CB.[Job Controller] ,
CB.[Job No]  ,
CB.[Job Client]  ,
CB.[Job Title]  ,
CB.[Job Value]  ,
CB.[Current Value]  ,
CB.[Number of Bids]  ,
CB.[Currency]  ,
CB.[Group Name]  ,
CB.[Note Job No]  ,
CB.[Note Date]  ,
CB.[Initial] ,
CB.[Note]  ,
CB.[Converted Symbol]  ,
CB.[Header] ,
 CB.[Note RecordID] ,
CB.[Job RecordID],''   
  FROM @CurrentBidsSuccess CB
 JOIN @CurrentBidsSuccessJobsBelowNotes CBSJB ON CBSJB.[Note RecordID] = CB.[Note RecordID]
 AND CBSJB.[Note Date] = CB.[Note Date]
 --END

IF (@OrderByColumn = 1)
BEGIN
SELECT   
CB.[Job Date]  ,
CB.[Job Controller] ,
CB.[Job No]  ,
CB.[Job Client]  ,
CB.[Job Title]  ,
CB.[Job Value]  ,
CB.[Current Value]  ,
CB.[Number of Bids]  ,
CB.[Currency]  ,
CB.[Group Name]  ,
CB.[Note Job No]  ,
CB.[Note Date]  ,
CB.[Initial] ,
REPLACE(CB.[Note],CHAR(13)+CHAR(10),' ') as [Note],
--CB.[Note]  ,
CB.[Converted Symbol]  ,
CB.[Header] 
 FROM @CurrentBidsSuccessFinal CB
ORDER BY CONVERT(DATE,[Job Date]) asc -- [Job Date]
END
ELSE IF (@OrderByColumn = 2)
BEGIN
SELECT   
CB.[Job Date]  ,
CB.[Job Controller] ,
CB.[Job No]  ,
CB.[Job Client]  ,
CB.[Job Title]  ,
CB.[Job Value]  ,
CB.[Current Value]  ,
CB.[Number of Bids]  ,
CB.[Currency]  ,
CB.[Group Name]  ,
CB.[Note Job No]  ,
CB.[Note Date]  ,
CB.[Initial] ,
REPLACE(CB.[Note],CHAR(13)+CHAR(10),' ') as [Note],
--CB.[Note]  ,
CB.[Converted Symbol]  ,
CB.[Header] 
FROM @CurrentBidsSuccessFinal CB
ORDER BY [Job No]  asc
END



--ORDER BY  CASE 
--    WHEN 1 THEN [Job Date] 
--    WHEN 2 THEN [Job No]
--	   END
/*
--DECLARE @OrderByColumn INT = 2
SELECT [Job Information].[V002] AS [Job Date]
	   , ISNULL([EmployeeList_Job Controller].V003,'') AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
	   , [Job Information].V001 AS [Job No]
	   ,ISNULL([Client].[V001],'-- NONE --') AS [Job Client]
	   , ISNULL([Job Information].V003,'-- NONE --') AS [Job Title]--Job Title, get from Job Information V003
	   ,Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,[Job Information].V007) * convert(money,[Group Currency Rates].V002) AS MONEY),0),2) AS [Job Value]
	   ,Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,[Currency Rates].V002) * convert(money,[Group Currency Rates].V002) AS MONEY),0),2) AS [Current Value]
	  
	   ,1 AS [Number of Bids]
	   ,[Reporting Currency].V001 AS [Currency]	
	   ,ISNULL([Job Group].[V001],'') AS [Group Name]	
	      , [Job Notes].V009 AS [Note Job No]
	   , [Job Notes].V003 AS [Note Date]
	    , [EmployeeList_List].V004 AS [Initial]
	   , [Job Notes].V006 AS [Note]		
	   ,CASE WHEN [Currency Rates].V001 = [Job Group].V004 THEN NULL ELSE convert(varchar,'C') END AS [Converted Symbol]
	 --  ,[Job Information].V008
	--, [Job Information].V007
	--,[Group Currency Rates].V002
	--,[Currency Rates].V002
			FROM [Record] [Job Information] WITH (NOLOCK) 

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		CAST([EmployeeList_Job Controller].RecordID AS VARCHAR(MAX)) = [Job Information].V026 		/*	Job Number	*/

			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		CAST([Currency Rates].RecordID AS VARCHAR(MAX)) = [Job Information].V034 		/*	Job Number	*/

			LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		CAST([Tax Name].RecordID AS VARCHAR(MAX)) = [Job Information].V042 		/*	Job Number	*/
			 
			LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
				AND			[Job Group].IsActive = 1
				AND		CAST( [Job Group].RecordID AS VARCHAR(MAX)) = [Job Information].V027		/*	Currency	*/

			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/
			LEFT JOIN Record [Client] WITH (NOLOCK) ON [Client].TableID = 2474		/*	Currency Rates	*/
				AND			[Client].IsActive = 1
				AND	CAST([Client].RecordID AS VARCHAR(MAX)) = [Job Information].V024		/*	[Group Currency]	*/
			LEFT JOIN Record [Job Notes] WITH (NOLOCK) ON [Job Notes].TableID = 2676		/*	Job Notes	*/
				AND			[Job Notes].IsActive = 1
				AND	CAST([Job Notes].[V009] AS VARCHAR(MAX)) = CAST([Job Information].RecordID AS VARCHAR)
				AND CONVERT(DATE,[Job Notes].V003,103) >= CAST('2018-11-01' AS DATE)  AND
					CONVERT(DATE,[Job Notes].V003,103) <= CAST('2018-11-14' AS DATE) 
		   LEFT JOIN [Record] [Reporting Currency] WITH (NOLOCK) ON [Reporting Currency].TableID = 2665
				AND		[Reporting Currency].IsActive = 1
				AND		CAST([Reporting Currency].RecordID AS VARCHAR(MAX)) = [Job Group].V005 		/*	Job Number	*/

			 LEFT JOIN [Record] [EmployeeList_List] WITH (NOLOCK) ON [EmployeeList_List].TableID = 2669 /*	Employee List	*/
				AND		[EmployeeList_List].IsActive = 1
				AND		CAST([EmployeeList_List].RecordID AS VARCHAR(MAX)) = [Job Notes].V007 		/*	Job Initial	*/

					WHERE [Job Information].TableID = 2475 AND
					[Job Information].IsActive = 1 AND
					--[Job Information].V001 = 'WC426-01' AND
					[Job Information].[V021] IN ('Bid')
					
					
					
					
					ORDER BY CASE @OrderByColumn
							 WHEN 1 THEN [Job Information].[V002] 
							 WHEN 2 THEN [Job Information].[V001] 
								END DESC


				*/				


END
