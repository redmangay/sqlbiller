﻿-- Author:		<Manoj sharma>

-- Description:	<adding dashboard for new user>
CREATE procedure [dbo].[sp_Renzo_AddNewUserDashboard]
(
	@RecordID  int,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output
)

as
--#region employee list desc
	--record id as userid
	--v003 as name
	--2669 employee list table
	declare @param_uniqename varchar(500)
	declare @param_record_userid int
	 declare @documentid int 
	 declare @SystemUser varchar(max)
--#endregion

SELECT	
@param_uniqename=v004,  -- initials
 @param_record_userid=RecordID, --recordid
 @SystemUser =V019 --system user
FROM			[Record] WHERE			TableID = 2669	and RecordID =@RecordID  and isnumeric(@RecordID)=1 --employeelist


--paste/insert  to document 
insert  into [thedatabase_windtech].[dbo].[Document] (AccountId,DocumentText,DocumentTypeID,UniqueName,FileTitle,DocumentDate,dateadded,DateUpdated,UserId,TableId,ReportHTML,IsReportPublic,DocumentDescription,DocumentEndDate,ForDashBoard,FolderId,Size,ReportType)
-- copying the default dashboard with id  891
select AccountId,DocumentText,DocumentTypeID,@param_uniqename,FileTitle,DocumentDate,getdate(),getdate(),@SystemUser,TableId,ReportHTML,IsReportPublic,DocumentDescription,DocumentEndDate,ForDashBoard,FolderId,Size,ReportType
from   [thedatabase_windtech].[dbo].[Document]
where documentid =891


 

set @documentid= @@IDENTITY  -- new documnetid return by document table

--insert into default section record from default windtech dashboard 891
insert into DocumentSection ([DocumentID]  ,[DocumentSectionTypeID] ,[Position] ,[SectionName],[Content],[Filter],[Details],[DateAdded],[DateUpdated],[DocumentSectionStyleID],[ValueFields],[ParentSectionID],[ColumnIndex])
select @documentid ,[DocumentSectionTypeID] ,[Position] ,[SectionName],[Content],[Filter],[Details],getdate(),getdate(),[DocumentSectionStyleID],[ValueFields],[ParentSectionID],[ColumnIndex]
FROM			[DocumentSection]
WHERE			1=1
AND				[DocumentID] = 891




--Handle the parentsection id here

declare @numberow int 
declare @row int =1
select @numberow =count (*)  FROM			[DocumentSection] WHERE			1=1 AND				[DocumentID] = 891 and ParentSectionID is null -- number of parent row
declare @documentsectionid int
declare @old_documentsectionid int

while @row <=@numberow -- loop to get id of parent row
begin

;with cte  -- data of newely added dashboard
as
(
select  ROW_NUMBER() OVER(ORDER BY documentsectionid asc) AS Row, documentsectionid from DocumentSection where parentsectionid is null and documentid =@documentid
)

select @documentsectionid = documentsectionid from cte where  [row]=@Row

;with cte1 -- data of default dashboard
as
(
select ROW_NUMBER() OVER(ORDER BY documentsectionid asc) AS Row, documentsectionid   FROM			[DocumentSection] WHERE			1=1 AND				[DocumentID] = 891 and ParentSectionID is null
)
select @old_documentsectionid = documentsectionid from cte1 where  [row]=@Row

set @row = @row +1

update DocumentSection set parentsectionid =@documentsectionid where parentsectionid =@old_documentsectionid  and documentid =@documentid

end


--handle parentsection id end here







--parsing the xml   between viewid and } closing found from recordtable 10
	select  details    , 	Substring(details,(CHARINDEX('"ViewID":',details)+9),  charindex ('}',details)) as views 
	
	into #documentsection  --stroing  view id in temp for fast result
	
	from 	DocumentSection
	where documentsectiontypeid =10 and documentid =891






--set @param_record_userid= isnull(@param_record_userid,0)

--insert into [view]  (

--       [TableID]
--      ,[ViewName]
--      ,[UserID]
--      ,[RowsPerPage]
--      ,[SortOrder]
--      ,[Filter]
--      ,[ShowSearchFields]
--      ,[ShowAddIcon]
--      ,[ShowEditIcon]
--      ,[ShowDeleteIcon]
--      ,[ShowViewIcon]
--      ,[ShowBulkUpdateIcon]
--      ,[FilterControlsInfo]
--      ,[ViewPageType]
--      ,[FixedFilter]
--      ,[ShowFixedHeader]
--	  )

	  
	 
-- SELECT
 
--      [TableID],
--      [ViewName]
--      ,@SystemUser  --dyanmic system user id
--      ,[RowsPerPage]
--      ,[SortOrder]

--      , case    ViewID 
--	  when '163 ' then ' AND (((( Record.V021 =''Bid'') and ( Record.V025 =''No'')) and ( Record.V026 =''' + convert(varchar(500),@param_record_userid)  +'''))  AND   1=1 )'
--	  when '164 ' then ' AND (((( Record.V021 =''Current'') and ( Record.V025 =''No'')) and ( Record.V026 =''' + convert(varchar(500),@param_record_userid)  +'''))  AND   1=1 )'
--	  when '226'  then  'AND (((( Record.V025 =''Yes'') and ( Record.V026 =''' + convert(varchar(500),@param_record_userid)  +'''))  AND   1=1 )  AND   1=1 )'
--	  when '229'  then  'AND (((( Record.V021 =''Bid'') and ( Record.V027 =''' + convert(varchar(500),@param_record_userid)  +'''))  AND   1=1 )  AND   1=1 )'
--	  when '230'  then   'AND (((( Record.V021 =''Current'') and ( Record.V027 =''' + convert(varchar(500),@param_record_userid)  +'''))  AND   1=1 )  AND   1=1 )'
--	  when '231'  then ' AND (((( Record.V025 =''Yes'') and ( Record.V027 =''' + convert(varchar(500),@param_record_userid)  +'''))  AND   1=1 )  AND   1=1 )'
-- 	    else filter end as [Filter]   --modified fileter 
      
	  
--	  ,[ShowSearchFields]
--      ,[ShowAddIcon]
--      ,[ShowEditIcon]
--      ,[ShowDeleteIcon]
--      ,[ShowViewIcon]
--      ,[ShowBulkUpdateIcon]
--      ,[FilterControlsInfo]
--      ,[ViewPageType]
--      ,[FixedFilter]
--      ,[ShowFixedHeader]
--	   from [view]  --default dashboard view id reteriveing from temporary document section
--	 where 
--	 viewid in (select  LEFT([views],DATALENGTH([views])-1) as viewid  --table documentsection with all view id 163,164...parsing view id here
	 
--	 from  #documentsection)

		 

	






RETURN @@ERROR
