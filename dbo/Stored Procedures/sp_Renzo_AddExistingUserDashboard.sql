﻿
-- =============================================
-- Author:		<Manoj sharma>
-- Create date: <25 may 2016>
-- Description:	<adding dashboard for exisiting user>

CREATE procedure [dbo].[sp_Renzo_AddExistingUserDashboard]
(
	@RecordID  int,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output
)

as

declare @isaccountholder int
declare @SystemUser varchar(max)


SELECT	

 @SystemUser =V019 --system user

FROM			[Record] WHERE			TableID = 2669	and RecordID =@RecordID  and isnumeric(@RecordID)=1 --employeelist


select @isaccountholder = count(*) from UserRole where IsAccountHolder =0 and UserID =@SystemUser and ISNUMERIC(@SystemUser)=1






if isnull(@isaccountholder,0) >0 -- only those are not account holder
begin
	declare @documentid as int
	select @documentid =documentid from Document  where  userid =@SystemUser  and ISNUMERIC(@SystemUser) =1  -- user id used as record id as per discusson same used in sp_newdashboard procedure

	if  isnull(@documentid,0) >0
	begin
	
    delete from [DocumentSection] WHERE [DocumentID] = @documentid
	delete from Document  where  userid =@SystemUser  and ISNUMERIC(@SystemUser) =1 -- user id used as record id as per discusson same used in sp_newdashboard procedure

  --  delete from  [view] where userid =@SystemUser
    
	end
		--exec sp sp_Renzo_AddNewUserDashboard
	exec sp_Renzo_AddNewUserDashboard @RecordID,@FormSetID, @UserID , 0

end


RETURN @@ERROR
