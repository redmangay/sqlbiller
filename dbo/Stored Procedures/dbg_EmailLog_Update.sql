﻿

CREATE PROCEDURE [dbo].[dbg_EmailLog_Update]
(
	@nEmailLogID int ,
	@nAccountID int ,
	@sEmailSubject nvarchar(1000) ,
	@sEmailTo nvarchar(500) ,
	@dEmailDate datetime ,
	@nTableID int  = NULL,
	@nRecordID int  = NULL,
	@sEmailType varchar(25)  = NULL,
	@sRawMessage nvarchar(MAX) 
)
	/*----------------------------------------------------------------
	-- Stored Procedure: theDBEmailLog_Update
	-- Generated by: gpGenerateSP (Jon Bosker, DB Gurus Australia) 
	-- Date: Jul  2 2014 10:30PM
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	UPDATE EmailLog
	SET 
		AccountID = @nAccountID,
		EmailSubject = @sEmailSubject,
		EmailTo = @sEmailTo,
		EmailDate = @dEmailDate,
		TableID = @nTableID,
		RecordID = @nRecordID,
		EmailType = @sEmailType,
		RawMessage = @sRawMessage
	WHERE EmailLogID = @nEmailLogID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_EmailLog_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

