﻿

----------------------------------------------------------------------
-- Paste code in below 
----------------------------------------------------------------------
CREATE PROCEDURE   [dbo].[DetailPhoto_insert]
(
	@Filename varchar(100),
	@TableID int,
	@Size int,
	@ColumnID int
)
AS

BEGIN TRY
	INSERT INTO [DetailPhoto]
	(
		[Filename],
		[TableID],
		[Size],
		[ColumnID]
	)
	VALUES 
	(
		@Filename,
		@TableID,
		@Size,
		@ColumnID
	)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('DetailPhoto_insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

