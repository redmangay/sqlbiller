﻿CREATE PROCEDURE [dbo].[Hii_SaveForm]
(
	@RecordID int,
	@UserID int,
	@Return varchar(max) output
)
AS
BEGIN

	DECLARE @TableID int
	SELECT @TableID = [TableID] FROM [Record] WHERE RecordID = @RecordID
	PRINT @TableID

	IF @TableID = 2063
		UPDATE [Record] 
			SET [Record].V049 = R.V264 -- Progress of form 
			, [Record].V050 = R.V035  -- Primary Surgeon
			FROM [Record] 
			JOIN [Record] R ON [Record].V005 = R.V031 -- Patient ID number
			AND R.RecordID = @RecordID
			WHERE [Record].TableID = 2058 -- safety check
				
	ELSE IF @TableID = 2129
		UPDATE [Record] 
			SET [Record].V049 = R.V343 -- Progress of form 
			, [Record].V050 = R.V061  -- Primary Surgeon
			FROM [Record] JOIN [Record] R ON [Record].V005 = R.V001 -- Patient ID number
			AND R.RecordID = @RecordID
			WHERE [Record].TableID = 2058 -- safety check
			
	ELSE IF @TableID = 2147
		UPDATE [Record] 
			SET [Record].V049 = R.V202 -- Progress of form 
			, [Record].V050 = R.V098  -- Primary Surgeon
			FROM [Record] JOIN [Record] R ON [Record].V005 = R.V001 -- Patient ID number
			AND R.RecordID = @RecordID
			WHERE [Record].TableID = 2058 -- safety check

	ELSE IF @TableID = 2150
		UPDATE [Record] 
			SET [Record].V049 = R.V164 -- Progress of form 
			, [Record].V050 = R.V086  -- Primary Surgeon
			FROM [Record] JOIN [Record] R ON [Record].V005 = R.V001 -- Patient ID number
			AND R.RecordID = @RecordID
			WHERE [Record].TableID = 2058 -- safety check

	ELSE IF @TableID = 2289
		UPDATE [Record] 
			SET [Record].V049 = R.V172 -- Progress of form 
			, [Record].V050 = R.V009  -- Primary Surgeon
			FROM [Record] JOIN [Record] R ON [Record].V005 = R.V028 -- Patient ID number
			AND R.RecordID = @RecordID
			WHERE [Record].TableID = 2058 -- safety check

	ELSE IF @TableID = 2315
		UPDATE [Record] 
			SET [Record].V049 = R.V272 -- Progress of form 
			, [Record].V050 = R.V009  -- Primary Surgeon
			FROM [Record] JOIN [Record] R ON [Record].V005 = R.V003 -- Patient ID number
			AND R.RecordID = @RecordID
			WHERE [Record].TableID = 2058 -- safety check

END


