﻿
CREATE PROCEDURE [dbo].[ets_GetContentTemplateAndRecordData] 
	@ContentId int, 
	@RecordId int
AS
-- =============================================
-- Author:		DAT
-- Create date: 17 Jul 2017
-- Description:	Get template and record data
-- Modifications:
-- 18/09/2018 JB Added error handling
-- 25/09/2018 JB Now using ets_CopyTable
-- =============================================
BEGIN TRY	
	SET NOCOUNT ON
	SET DATEFORMAT DMY
	DECLARE @Template nvarchar(MAX)
	DECLARE @Subject nvarchar(MAX)
	DECLARE @StoredProcedure nvarchar(200)

	SELECT	@Template = C.Content,@Subject=C.Heading,
			@StoredProcedure = C.StoredProcedure
	FROM	Content AS C
	WHERE	C.ContentID = @ContentId

	SELECT @Template,@Subject

	IF(@StoredProcedure IS NOT NULL)
		BEGIN
			IF(EXISTS (SELECT [Name] FROM [sys].[procedures] WHERE [Name] = @StoredProcedure))
				BEGIN
					EXEC @StoredProcedure @RecordId
				END
		END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_GetContentTemplateAndRecordData', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
