﻿-- Author:		<Manoj sharma>
-- Create date: <2016-02-02 16:33:21.433>
-- Description:	<Used as ssrs report>
CREATE procedure [dbo].[usr_rptJobBidSucces_18102018]
(
 @From date,
 @To date,
 @group varchar(max) =null,
 @controller varchar(max) =null,
 @OrderByColumn varchar(500) =0
)
as

--#region calling  temproray table of all view
exec  usr_rptjobbidsuccess_view

--#endregion
select * into #tempjobbid FROM  ##rptJobBidSucces rptJobBidSuccess LEFT JOIN Account24918.[vList_Of_Group_Division] as [List_Of_Group/Division] ON rptJobBidSuccess.GroupDivision = [List_Of_Group/Division].[Group/Division]
 select * into #vProject from Account24918.vProject 
 
 if @OrderByColumn ='1'
 begin

 SELECT Sum(rptJobBidSuccess.WonAmount) AS GroupWonAmount, 
 Sum(rptJobBidSuccess.JobValue) AS GroupJobValue, 
 rptJobBidSuccess.GroupDivision, 
 min(rptJobBidSuccess.Controller) as Controller, 
 IsNull(rptJobBidSuccess.[ReportingCurrency],'') AS GroupCurrency,
 sum(case when rptJobBidSuccess.IntStatus NOT IN ('1', '3') and jobvalue >0  then 1 else 0 end  )*100.00/count(1)   as txtpercentwon ,
 sum(case when rptJobBidSuccess.IntStatus  IN ('3') and jobvalue >0  then 1 else 0 end  )*100.00/count(1)   as txtpercentloss,
 sum(case when rptJobBidSuccess.IntStatus  IN ('1') and jobvalue >0  then 1 else 0 end  )*100.00/count(1)   as txtpercentpending,
  MIN(rptJobBidSuccess.[Date]) as dated,
 Min(Project.ProjectNumber) as JOBNO
 --sum(case when rptJobBidSuccess.IntStatus NOT IN ('1', '3') and jobvalue >0 and Project.RptCurrency =IsNull(rptJobBidSuccess.[ReportingCurrency],'') then 1 else 0 end  )*100.00/count(1)   as txtpercentwoncurrency
  
FROM  #tempjobbid rptJobBidSuccess
left join #vProject Project ON rptJobBidSuccess.JobNo=Project.ProjectNumber
where (convert(date,rptJobBidSuccess.[Date]) between @From and @TO 
and (JobValue>0)

 and  ( (GroupDivision= case when isnull(GroupDivision,'')='' then GroupDivision else  GroupDivision end)
) and ((@group ='ALL')  or (GroupDivision =@group)) and ((@controller ='ALL')  or (rptJobBidSuccess.Controller =@controller)))

GROUP BY rptJobBidSuccess.GroupDivision ,    -- rptJobBidSuccess.Controller, 
IsNull(rptJobBidSuccess.ReportingCurrency,'')
--ORDER BY rptJobBidSuccess.GroupDivision;
ORDER BY 
convert(date,Min(rptJobBidSuccess.[Date]))

END  --order by dated

else if @OrderByColumn ='2'  -- order by job value
 begin

 SELECT Sum(rptJobBidSuccess.WonAmount) AS GroupWonAmount, 
 Sum(rptJobBidSuccess.JobValue) AS GroupJobValue, 
 rptJobBidSuccess.GroupDivision, 
 min(rptJobBidSuccess.Controller) as Controller, 
 IsNull(rptJobBidSuccess.[ReportingCurrency],'') AS GroupCurrency,
 sum(case when rptJobBidSuccess.IntStatus NOT IN ('1', '3') and jobvalue >0  then 1 else 0 end  )*100.00/count(1)   as txtpercentwon ,
 sum(case when rptJobBidSuccess.IntStatus  IN ('3') and jobvalue >0  then 1 else 0 end  )*100.00/count(1)   as txtpercentloss,
 sum(case when rptJobBidSuccess.IntStatus  IN ('1') and jobvalue >0  then 1 else 0 end  )*100.00/count(1)   as txtpercentpending,
  MIN(rptJobBidSuccess.[Date]) as dated,
 Min(Project.ProjectNumber) as JOBNO
 --sum(case when rptJobBidSuccess.IntStatus NOT IN ('1', '3') and jobvalue >0 and Project.RptCurrency =IsNull(rptJobBidSuccess.[ReportingCurrency],'') then 1 else 0 end  )*100.00/count(1)   as txtpercentwoncurrency
  
FROM  #tempjobbid rptJobBidSuccess
left join #vProject Project ON rptJobBidSuccess.JobNo=Project.ProjectNumber
where (convert(date,rptJobBidSuccess.[Date]) between @From and @TO 
and (JobValue>0)

 and  ( (GroupDivision= case when isnull(GroupDivision,'')='' then GroupDivision else  GroupDivision end)
) and ((@group ='ALL')  or (GroupDivision =@group)) and ((@controller ='ALL')  or (rptJobBidSuccess.Controller =@controller)))

GROUP BY rptJobBidSuccess.GroupDivision ,    -- rptJobBidSuccess.Controller, 
IsNull(rptJobBidSuccess.ReportingCurrency,'')
--ORDER BY rptJobBidSuccess.GroupDivision;
ORDER BY 
GroupJobValue

END

else if @OrderByColumn ='3'  -- order by jobnumber
 begin

 SELECT Sum(rptJobBidSuccess.WonAmount) AS GroupWonAmount, 
 Sum(rptJobBidSuccess.JobValue) AS GroupJobValue, 
 rptJobBidSuccess.GroupDivision, 
 min(rptJobBidSuccess.Controller) as Controller, 
 IsNull(rptJobBidSuccess.[ReportingCurrency],'') AS GroupCurrency,
 sum(case when rptJobBidSuccess.IntStatus NOT IN ('1', '3') and jobvalue >0  then 1 else 0 end  )*100.00/count(1)   as txtpercentwon ,
 sum(case when rptJobBidSuccess.IntStatus  IN ('3') and jobvalue >0  then 1 else 0 end  )*100.00/count(1)   as txtpercentloss,
 sum(case when rptJobBidSuccess.IntStatus  IN ('1') and jobvalue >0  then 1 else 0 end  )*100.00/count(1)   as txtpercentpending,
  MIN(rptJobBidSuccess.[Date]) as dated,
 Min(Project.ProjectNumber) as JOBNO
 --sum(case when rptJobBidSuccess.IntStatus NOT IN ('1', '3') and jobvalue >0 and Project.RptCurrency =IsNull(rptJobBidSuccess.[ReportingCurrency],'') then 1 else 0 end  )*100.00/count(1)   as txtpercentwoncurrency
  
FROM  #tempjobbid rptJobBidSuccess
left join #vProject Project ON rptJobBidSuccess.JobNo=Project.ProjectNumber
where (convert(date,rptJobBidSuccess.[Date]) between @From and @TO 
and (JobValue>0)

 and  ( (GroupDivision= case when isnull(GroupDivision,'')='' then GroupDivision else  GroupDivision end)
) and ((@group ='ALL')  or (GroupDivision =@group)) and ((@controller ='ALL')  or (rptJobBidSuccess.Controller =@controller)))

GROUP BY rptJobBidSuccess.GroupDivision ,    -- rptJobBidSuccess.Controller, 
IsNull(rptJobBidSuccess.ReportingCurrency,'')
--ORDER BY rptJobBidSuccess.GroupDivision;
ORDER BY 
JOBNO

END
else
begin

SELECT Sum(rptJobBidSuccess.WonAmount) AS GroupWonAmount, 
 Sum(rptJobBidSuccess.JobValue) AS GroupJobValue, 
 rptJobBidSuccess.GroupDivision, 
 min(rptJobBidSuccess.Controller) as Controller, 
 IsNull(rptJobBidSuccess.[ReportingCurrency],'') AS GroupCurrency,
 sum(case when rptJobBidSuccess.IntStatus NOT IN ('1', '3') and jobvalue >0  then 1 else 0 end  )*100.00/count(1)   as txtpercentwon ,
 sum(case when rptJobBidSuccess.IntStatus  IN ('3') and jobvalue >0  then 1 else 0 end  )*100.00/count(1)   as txtpercentloss,
 sum(case when rptJobBidSuccess.IntStatus  IN ('1') and jobvalue >0  then 1 else 0 end  )*100.00/count(1)   as txtpercentpending,
  MIN(rptJobBidSuccess.[Date]) as dated,
 Min(Project.ProjectNumber) as JOBNO
 --sum(case when rptJobBidSuccess.IntStatus NOT IN ('1', '3') and jobvalue >0 and Project.RptCurrency =IsNull(rptJobBidSuccess.[ReportingCurrency],'') then 1 else 0 end  )*100.00/count(1)   as txtpercentwoncurrency
  
FROM  #tempjobbid rptJobBidSuccess
left join #vProject Project ON rptJobBidSuccess.JobNo=Project.ProjectNumber
where (convert(date,rptJobBidSuccess.[Date]) between @From and @TO 
and (JobValue>0)

 and  ( (GroupDivision= case when isnull(GroupDivision,'')='' then GroupDivision else  GroupDivision end)
) and ((@group ='ALL')  or (GroupDivision =@group)) and ((@controller ='ALL')  or (rptJobBidSuccess.Controller =@controller)))

GROUP BY rptJobBidSuccess.GroupDivision ,    -- rptJobBidSuccess.Controller, 
IsNull(rptJobBidSuccess.ReportingCurrency,'')
--ORDER BY rptJobBidSuccess.GroupDivision;

ORDER BY rptJobBidSuccess.GroupDivision

end
