﻿CREATE procedure [dbo].[jobtest]
as
select * into #tempjobbid FROM  v_rptJobBidSucces rptJobBidSuccess LEFT JOIN Account24918.[vList_Of_Group_Division] as [List_Of_Group/Division] ON rptJobBidSuccess.GroupDivision = [List_Of_Group/Division].[Group/Division]
 
 SELECT Sum(rptJobBidSuccess.WonAmount) AS GroupWonAmount, 
 Sum(rptJobBidSuccess.JobValue) AS GroupJobValue, 
 rptJobBidSuccess.GroupDivision, 
 rptJobBidSuccess.Controller, 
 IsNull(rptJobBidSuccess.[ReportingCurrency],'') AS GroupCurrency
 
 into #temp2
FROM  #tempjobbid rptJobBidSuccess
where rptJobBidSuccess.[Date] between '2015-07-22' and GETDATE() 
GROUP BY rptJobBidSuccess.GroupDivision, rptJobBidSuccess.Controller, 
IsNull(rptJobBidSuccess.ReportingCurrency,'')
ORDER BY rptJobBidSuccess.GroupDivision;

select * into #v_rptJobBidSucces from v_rptJobBidSucces
select * into #vProject from Account24918.vProject 
select (SELECT Sum(case when IntStatus NOT IN ('1', '3')  then 1 else 0 end  )*100.00/COUNT(1) AS TotalNumberOfWonJobs

                        FROM #v_rptJobBidSucces left join #vProject Project ON #v_rptJobBidSucces.JobNo=Project.ProjectNumber
  
                        WHERE  (JobValue>0) and GroupDivision=case when #temp2.GroupDivision='' then GroupDivision else  #temp2.GroupDivision end
                        
                         ) from #temp2
--select dbo.PercentWonByNumberofJobCalculate('','')
 