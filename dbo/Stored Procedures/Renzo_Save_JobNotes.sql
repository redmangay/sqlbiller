﻿CREATE PROCEDURE [dbo].[Renzo_Save_JobNotes] 
(
	@RecordID  int,		/*	Record ID passed from the table on execute, base your solution on this value	*/
	@UserID int,
	@Return varchar(max) output
	, @Context varchar(max) = null
)
AS

/*

EXEC Renzo_Save_JobNotes @RecordID = '2717569',@UserID = '25197',@Return=''

*/
BEGIN
	SET DATEFORMAT dmy;
	DECLARE @FollowUpBy varchar(10)

	SELECT @FollowUpBy = V012 FROM [Record] WHERE RecordID = @RecordID
    
    /* lets update the follow up date by days calculation */
	IF (@FollowUpBy = '1')
	BEGIN
	    UPDATE [Job Notes]
		  SET [Job Notes].V011 = [Job Notes].[V008]--CONVERT(varchar(10),DATEADD(DAY,CAST([Job Notes].V010 AS INT),CONVERT(DATE,[Job Notes].V003,103)),103)
	    FROM [Record] [Job Notes]
	    WHERE [Job Notes].RecordID = @RecordID
		  AND [Job Notes].TableID = 2676
		  AND [Job Notes].IsActive = 1
	END
	

	RETURN @@ERROR;
END;
