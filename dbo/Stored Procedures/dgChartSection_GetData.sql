﻿
-- =============================================
-- Author:		Dathq
-- Create date: 16 Jun 2012
-- Description:	Get chart data
-- =============================================
CREATE PROCEDURE [dbo].[dgChartSection_GetData] 
	-- Add the parameters for the stored procedure here
	@ChartDefinitionID int,
	@Series varchar(MAX),
	@ValueFields varchar(MAX),
	@StartDate datetime,
	@EndDate datetime
AS

BEGIN TRY
	SET NOCOUNT ON;
	
    DECLARE @Sql varchar(MAX)        
    DECLARE @TableID int
    
    SELECT	@TableID = TableID
    FROM	dgChartDefinition
    WHERE	ChartDefinitionID = @ChartDefinitionID
    
	SET @EndDate = DATEADD(Day, 1, @EndDate)
	
	IF(CHARINDEX(',', @Series) > 0)
		BEGIN
			SET @sql =	' SELECT LocationID, DateTimeRecorded,' + @ValueFields +
				' FROM Record' +
				' WHERE DateTimeRecorded BETWEEN ''' + CAST(@StartDate AS Varchar) + ''' AND ''' +  CAST(@EndDate AS Varchar) + '''' +
				' AND LocationID IN (' + @Series + ')' + 
				' AND TableID = ' + CAST(@TableID AS Varchar) + 
 				' ORDER BY LocationID, DateTimeRecorded' 
		END	
	ELSE
		BEGIN
			SET @sql =	' SELECT DateTimeRecorded,' + @ValueFields +
				' FROM Record' +
				' WHERE DateTimeRecorded BETWEEN ''' + CAST(@StartDate AS Varchar) + ''' AND ''' +  CAST(@EndDate AS Varchar) + '''' +
				' AND LocationID = ' + @Series + 
				' AND TableID = ' + CAST(@TableID AS Varchar) + 
				' ORDER BY DateTimeRecorded' 			
		END	
	EXEC(@sql)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dgChartSection_GetData', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
