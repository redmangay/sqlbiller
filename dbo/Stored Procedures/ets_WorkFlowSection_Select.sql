﻿

CREATE PROCEDURE [dbo].[ets_WorkFlowSection_Select]
(
	@nWorkFlowID int  = NULL,
	@nWorkFlowSectionTypeID int  = NULL,
	@sSectionName varchar(100)  = NULL,
	@nPosition int  = NULL,
	@sJSONContent varchar(MAX)  = NULL,
	@nParentSectionID int  = NULL,
	@nColumnIndex int  = NULL,
	@sOrder nvarchar(200) = WorkFlowSectionID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	-- Stored Procedure: ets_WorkFlowSection_Select
	-- Generated by: gpGenerateSP (Jon Bosker, DB Gurus Australia) 
	-- Date: Jan 12 2014  7:25PM
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(4000)
	DECLARE @sWhere nvarchar(4000)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nWorkFlowID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND WorkFlowID = '+CAST(@nWorkFlowID AS NVARCHAR)
	IF @nWorkFlowSectionTypeID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND WorkFlowSectionTypeID = '+CAST(@nWorkFlowSectionTypeID AS NVARCHAR)
	IF @sSectionName IS NOT NULL 
		SET @sWhere = @sWhere + ' AND SectionName LIKE '+'''' + @sSectionName + ''''
	IF @nPosition IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Position = '+CAST(@nPosition AS NVARCHAR)
	IF @sJSONContent IS NOT NULL 
		SET @sWhere = @sWhere + ' AND JSONContent LIKE '+'''' + @sJSONContent + ''''
	IF @nParentSectionID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ParentSectionID = '+CAST(@nParentSectionID AS NVARCHAR)
	IF @nColumnIndex IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ColumnIndex = '+CAST(@nColumnIndex AS NVARCHAR)
	SET @sSelect = 'SELECT * FROM 
	(SELECT WorkFlowSection.*, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum FROM WorkFlowSection' + @sWhere + ') as UserInfo'
 
	-- Extend WHERE to include paging:
	SET @sWhere = @sWhere + ' AND RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	SET @sOrder = ' ORDER BY ' + @sOrder
	SET @sSelect = @sSelect + @sWhere + @sOrder
 
	EXEC (@sSelect)
	SET ROWCOUNT 0
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_WorkFlowSection_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
