﻿CREATE procedure [dbo].[usr_rptJobContactActivity]
(
@Employee INT = 1, /* All */
@From date,
@To date

)
AS

/*

EXEC [dbo].[usr_rptJobContactActivity] '1','01/06/2018','30/06/2018'


*/
BEGIN

set dateformat dmy;
    IF @Employee <> 1
    BEGIN
	    SELECT  ISNULL([Job Information].[V001],'No Job Number')   [Job No]
	   ,ISNULL([Job Information].[V003],'No Title') AS [Job Title] /* Job Title */
	   ,[Job Notes].[V003] AS [Date] /* Note Date */
	   ,CASE 
		  WHEN [Job Notes].[V005] = '0' THEN NULL   
		  WHEN [Job Notes].[V005] = '1' THEN 'Low' 
		  WHEN [Job Notes].[V005] = '2' THEN 'Mid' 
		  ELSE 'High' END AS [Priority] /* Note Priority */
	   ,[Employees].[V004] AS [Initial]/* Note Initial - Employee */
	   ,[Job Notes].[V011] AS [Follow up]/* Note Follow up date */
	   ,[Job Notes].[V006] AS [Note]/* Note Subject */
	   ,@From AS DateFrom
	   ,@To as DateTo
	   FROM [Record] [Job Notes]
	   LEFT JOIN [Record] [Employees] ON CAST([Employees].[RecordID] AS VARCHAR) = [Job Notes].[V007]
		AND [Employees].[IsActive] = 1
		AND [Employees].[TableID] = 2669
	   LEFT JOIN [Record] [Job Information] ON CAST([Job Information].[RecordID] AS VARCHAR) = [Job Notes].[V009]
		AND [Job Information].[IsActive] = 1
		AND [Job Information].[TableID] = 2475
	   WHERE [Job Notes].[TableID] = 2676
		AND [Job Notes].[IsActive] = 1
		AND [Job Notes].[V007] = CAST(@Employee AS VARCHAR)--'1004026'
		AND CONVERT(DATE,[Job Notes].[V003]) >= CONVERT(DATE,@From)--'01/10/2018'
		AND CONVERT(DATE,[Job Notes].[V003]) <= CONVERT(DATE,@To)--'31/10/2018'
		  ORDER BY [Job Information].[V001]
    END
    ELSE 
    BEGIN
	    SELECT  ISNULL([Job Information].[V001],'No Job Number')   [Job No]
	   ,ISNULL([Job Information].[V003],'No Title') AS [Job Title] /* Job Title */
	   ,[Job Notes].[V003] AS [Date] /* Note Date */
	    ,CASE 
		  WHEN [Job Notes].[V005] = '0' THEN NULL   
		  WHEN [Job Notes].[V005] = '1' THEN 'Low' 
		  WHEN [Job Notes].[V005] = '2' THEN 'Mid' 
		  ELSE 'High' END AS [Priority] /* Note Priority */
	   ,[Employees].[V004] AS [Initial]/* Note Initial - Employee */
	   ,[Job Notes].[V011] AS [Follow up]/* Note Follow up date */
	   ,[Job Notes].[V006] AS [Note]/* Note Subject */
	   ,@From AS DateFrom
	   ,@To as DateTo
	    , 	(SELECT V039  FROM RECORD WHERE TABLEID = 2743) AS [Header] 
	   FROM [Record] [Job Notes]
	   LEFT JOIN [Record] [Employees] ON CAST([Employees].[RecordID] AS VARCHAR) = [Job Notes].[V007]
		AND [Employees].[IsActive] = 1
		AND [Employees].[TableID] = 2669
	   LEFT JOIN [Record] [Job Information] ON CAST([Job Information].[RecordID] AS VARCHAR) = [Job Notes].[V009]
		AND [Job Information].[IsActive] = 1
		AND [Job Information].[TableID] = 2475
	   WHERE [Job Notes].[TableID] = 2676
		AND [Job Notes].[IsActive] = 1
		--AND [Job Notes].[V007] = @Employee --'1004026'
		AND CONVERT(DATE,[Job Notes].[V003]) >= CONVERT(DATE,@From)--'01/10/2018'
		AND CONVERT(DATE,[Job Notes].[V003]) <= CONVERT(DATE,@To)--'31/10/2018'
		  ORDER BY [Job Information].[V001]
    END

    



END



/*Commented out old script - Red*/

--SELECT
 
--  [Project].[ProjectNumber] + '-' + [ProjectTitle] AS ProjectInfo, 
--Notes.Follow_Up, Notes.Flag, 
--case when Notes.Flag = '1' then 'Low' 
--when Notes.Flag = '2' then 'Medium' 
--when Notes.Flag = '3' then 'High' end  as getPriority,

--Notes.Date, 
--Notes.Initials, Notes.Subject, Employee_List.Name, Notes.ProjectNumber
--into #tempjobcontact
--FROM 

--[Account24918].vNotes  Notes INNER JOIN [Account24918].[vEmployee_List] Employee_List ON Notes.Initials = Employee_List.Initials INNER JOIN  [Account24918].[vProject] Project ON Notes.ProjectNumber = Project.ProjectNumber
--WHERE (((Employee_List.OldYesNo)='False') and (Employee_List.Name =@Notes or @Notes ='All'))
----and (convert(date,Notes.Date) between  @From and @To)) -- 
----order by [Project].[ProjectNumber]

--select * from #tempjobcontact where (convert(date,#tempjobcontact.Date) between  @From and @To) 
--order by [ProjectNumber] 







