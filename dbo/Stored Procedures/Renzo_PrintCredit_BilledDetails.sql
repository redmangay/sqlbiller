﻿CREATE PROCEDURE [dbo].[Renzo_PrintCredit_BilledDetails]
(
	@RecordID  int   		
	
)
AS

/*

EXEC [Renzo_PrintCredit_BilledDetails]   15235170


*/

BEGIN
 SET DATEFORMAT dmy


    DECLARE @TaxAmount money
    DECLARE @TaxName varchar(max)
    DECLARE @TotalAmount money

    SELECT  @TaxAmount =  V047 FROM RECORD WHERE RECORDID = @RecordID
    SELECT @TotalAmount = V045 FROM RECORD WHERE RECORDID = @RecordID
  
  
 
 SELECT	JI.V001 as JobNo,
		JI.V003 as JobTitle,
		Emp.V003 as [Description],
		CD.V006 as Amount,
		 isnull(@TaxAmount,0.00) as GST,
		isnull(@TotalAmount,0.00) as Total
	FROM Record CD
	JOIN Record BI ON BI.TableID = 2711
	AND BI.IsActive = 1
	AND BI.RecordID = CD.V004
	JOIN Record JI ON JI.TableID = 2475
	AND JI.IsActive = 1
	AND JI.RecordID = BI.V002 
	JOIN Record Emp ON Emp.TableID = 2669
	AND Emp.IsActive = 1
	AND Emp.RecordID = CD.V005
	WHERE CD.TableID = 2732
	AND CD.IsActive = 1
	AND CD.V004 = @RecordID
	ORDER BY Emp.V003
	  



RETURN @@ERROR;

END