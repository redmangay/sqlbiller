﻿

CREATE PROCEDURE [dbo].[CheckIsClient]
(
	@nAccountID int 
)
	/*----------------------------------------------------------------
	EXEC CheckIsClient 2
	---------------------------------------------------------------*/
 
AS

BEGIN TRY
 
	DECLARE @dExpiryDate datetime
	DECLARE @sResult varchar(100)

	SELECT @dExpiryDate=ExpiryDate
		FROM dbo.Account A
		WHERE AccountID=@nAccountID

	SET @sResult ='OK'

	IF @dExpiryDate <GETDATE()

	SELECT @sResult=DATEDIFF (d, @dExpiryDate, GETDATE()) 

	SELECT @sResult	

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('CheckIsClient', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
