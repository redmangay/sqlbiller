﻿

CREATE PROCEDURE [dbo].[ets_TotalRecords]
(
	@nAccountID int	
)
AS
/*
	EXEC ets_TotalRecordsThisMonth 13
*/
BEGIN TRY
	SELECT COUNT(*) FROM [Record] INNER JOIN [Table] 
	ON [Record].TableID=[Table].TableID 
	WHERE [Table].AccountID=@nAccountID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_TotalRecords', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
