﻿

CREATE PROCEDURE [dbo].[spUpdateExistingData]
(
	@nTableID int,
	@sUniqueField varchar(50),
	@nCurrentBatchID int,
	@nUpdatedRowCount int output

)
AS
/*-------------------------------
-- 1. Download the data 
-- 2. User Updates the data
-- 3. User upload the data
-- 4. UPDATE and run this script
DECLARE @nUpdatedRowCount INT
EXEC DBO.spUpdateExistingData 2655,'V003',3412,@nUpdatedRowCount output
SELECT @nUpdatedRowCount

-------------------------------*/
BEGIN TRY
	DECLARE @sSQL varchar(MAX)
	SELECT @sSQL = 'UPDATE Old SET '
	SELECT @sSQL = @sSQL + 'Old.' + SystemName + '=New.' + SystemName + ', '
		FROM [Column] 
		WHERE [TableID] = @nTableID AND SystemName<>@sUniqueField
		AND SystemName LIKE 'V%'
	
	SELECT @sSQL = SUBSTRING(@sSQL, 1, LEN(@sSQL)-1)
	SELECT @sSQL = @sSQL + ' 
		FROM [Record] Old
		JOIN [Record] New ON New.' + @sUniqueField + ' = Old.' + @sUniqueField + '
		WHERE Old.[TableID] = ' + CAST(@nTableID as varchar) + '
			AND (Old.BatchID != ' + CAST(@nCurrentBatchID AS varchar) + ' OR Old.BatchID IS NULL) 
			AND Old.IsActive = 1
		AND New.[TableID] = ' + CAST(@nTableID as varchar) + ' 
			AND New.BatchID  = ' +CAST( @nCurrentBatchID AS varchar) + '
			AND New.IsActive = 1'
	PRINT @sSQL 
	EXEC (@sSQL)

	SELECT @nUpdatedRowCount= @@ROWCOUNT

	 --Make all the new records inactive
	UPDATE [Record] SET IsActive = 0 WHERE [TableID] = @nTableID AND IsActive = 1 AND BatchID = @nCurrentBatchID



	--- Below Code TESTED, working well to add new record too from file

		--SELECT @sSQL = 'UPDATE [Record] SET IsActive=1 
		--WHERE IsActive=0 AND [TableID] = ' + CAST(@nTableID as varchar) + ' 
		--AND BatchID  = ' + CAST(@nCurrentBatchID AS varchar) + '
		--AND ' + @sUniqueField + ' NOT IN  (SELECT ' + @sUniqueField + ' FROM Record 
		--WHERE IsActive=1 AND [TableID] = ' + CAST(@nTableID as varchar) + ')'

		--PRINT @sSQL 
		--EXEC (@sSQL)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spUpdateExistingData', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
