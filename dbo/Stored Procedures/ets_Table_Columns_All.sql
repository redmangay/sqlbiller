﻿

CREATE PROCEDURE [dbo].[ets_Table_Columns_All]
(
	@nTableID int
)
AS
/*

*/
BEGIN TRY
	SET NOCOUNT ON;	
	SET CONCAT_NULL_YIELDS_NULL OFF

	SELECT C.*,C.DisplayName as Heading,
		(SELECT TOP 1 CheckColumnID FROM [Condition] Con WHERE Con.ConditionType='V'AND Con.ColumnID=C.ColumnID ) ConV,
		(SELECT TOP 1 CheckColumnID FROM [Condition] Con WHERE Con.ConditionType='W'AND Con.ColumnID=C.ColumnID ) ConW,
		(SELECT TOP 1 CheckColumnID FROM [Condition] Con WHERE Con.ConditionType='E'AND Con.ColumnID=C.ColumnID ) ConE,
		(SELECT TOP 1 ConditionColumnID FROM [AdvancedCondition] Con WHERE Con.ConditionType='notification'AND Con.ConditionSubType='V'AND Con.ColumnID = C.ColumnID) AdvConV,
		(SELECT TOP 1 ConditionColumnID FROM [AdvancedCondition] Con WHERE Con.ConditionType='notification'AND Con.ConditionSubType='W'AND Con.ColumnID = C.ColumnID) AdvConW,
		(SELECT TOP 1 ConditionColumnID FROM [AdvancedCondition] Con WHERE Con.ConditionType='notification'AND Con.ConditionSubType='E'AND Con.ColumnID = C.ColumnID) AdvConE
		 FROM [Column] C
		WHERE TableID = @nTableID
		ORDER BY DisplayOrder
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Table_Columns_All', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
