﻿

CREATE PROCEDURE [dbo].[ets_DataReminderUser_Select]
(
	@nDataReminderUserID int  = NULL,
	@nDataReminderID int  = NULL,
	@nUserID int  = NULL,
	@sOrder nvarchar(200) = DataReminderUserID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	EXEC [ets_DataReminderUser_Select] @nDataReminderID=52
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(4000)
	DECLARE @sWhere nvarchar(4000)
	DECLARE @sSelectCount nvarchar(MAX)

	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nDataReminderUserID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DataReminderUserID = '+CAST(@nDataReminderUserID AS NVARCHAR)
	IF @nDataReminderID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DataReminderID = '+CAST(@nDataReminderID AS NVARCHAR)
	IF @nUserID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND UserID = '+CAST(@nUserID AS NVARCHAR)
	SET @sSelect = 'SELECT * FROM 
	(SELECT DataReminderUser.*,(FirstName + '' '' + LastName) as UserName,DisplayName, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum 
	FROM DataReminderUser  LEFT JOIN [User] on [DataReminderUser].UserID=[User].UserID 
	  LEFT JOIN [Column] ON [DataReminderUser].ReminderColumnID=[Column].ColumnID ' + @sWhere + ') as UserInfo'
 
	 SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [DataReminderUser] ' + @sWhere 

	-- Extend WHERE to include paging:
	SET @sWhere = @sWhere + ' AND RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	SET @sOrder = ' ORDER BY ' + @sOrder
	SET @sSelect = @sSelect + @sWhere + @sOrder

	 PRINT @sSelect

	EXEC (@sSelect)
	SET ROWCOUNT 0

 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_DataReminderUser_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
