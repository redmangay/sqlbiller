﻿

CREATE PROCEDURE [dbo].[ets_Table_Columns_Summary_Export]
(
	@nTableID int,@nViewID int=NULL
)
AS
/*

*/
BEGIN TRY
	SET NOCOUNT ON;	
	SET CONCAT_NULL_YIELDS_NULL OFF
	IF @nViewID IS NULL
	SELECT c.*,c.DisplayTextSummary as Heading FROM [Column] c
		WHERE TableID = @nTableID
		AND (DisplayTextSummary IS NOT NULL AND LEN(DisplayTextSummary) > 0)
		ORDER BY DisplayOrder
		

	IF @nViewID IS NOT NULL
	SELECT C.*,ISNULL(C.DisplayTextSummary,C.DisplayName) as Heading   FROM [Column] C INNER JOIN ViewItem VT
			ON C.ColumnID =VT.ColumnID
			WHERE ViewID = @nViewID	
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Table_Columns_Summary_Export', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
