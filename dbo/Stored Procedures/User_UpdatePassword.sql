﻿CREATE Procedure [dbo].[User_UpdatePassword]
(
	@nUserID int,
	@sPassword nvarchar(200)
)
AS
BEGIN TRY
	DECLARE @Salt VARBINARY(8) = CRYPT_GEN_RANDOM(8)

	UPDATE dbo.[User]
	SET 	[EncryptedPassword] = HASHBYTES('SHA2_512', @sPassword + cast(@Salt as nvarchar)),
			[SaltPassword] = @Salt
	WHERE [UserID] = @nUserID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('User_UpdatePassword', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
