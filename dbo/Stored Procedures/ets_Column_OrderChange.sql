﻿

CREATE PROCEDURE [dbo].[ets_Column_OrderChange]
(
	@nColumnID int,
	@bMoveUp bit
)
AS
/*
	EXEC [ets_Table_OrderChange]
		@nColumnID = 5,
		@bMoveUp = 0
	SELECT * FROM [Column] WHERE TableID = 8 order by DisplayOrder

*/
BEGIN TRY
	SET NOCOUNT ON;

	DECLARE @nTableID int, @nDisplayOrder int
	SELECT @nTableID = TableID, @nDisplayOrder = DisplayOrder 
		FROM [Column] 
		WHERE ColumnID = @nColumnID
			
	DECLARE @nOtherColumnID int
	DECLARE @nNewOrder int
	
	IF (@bMoveUp = 1)
	BEGIN
			SELECT TOP 1 @nNewOrder = MAX(DisplayOrder)
				FROM [Column]
				WHERE TableID = @nTableID
				AND DisplayOrder < @nDisplayOrder
	END
	ELSE
	BEGIN
			SELECT TOP 1 @nNewOrder = MIN(DisplayOrder)
				FROM [Column]
				WHERE TableID = @nTableID
				AND DisplayOrder > @nDisplayOrder
	END
	
	IF @nNewOrder IS NOT NULL
	BEGIN TRY

	BEGIN TRANSACTION --beginning a transaction..

		-- Switch them round
		UPDATE [Column] SET DisplayOrder = @nDisplayOrder 
			WHERE TableID = @nTableID
			AND DisplayOrder = @nNewOrder
		UPDATE [Column] SET DisplayOrder = @nNewOrder 
			WHERE ColumnID = @nColumnID
	COMMIT TRANSACTION --Commit the transaction as both Success.

	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION --RollBack Transaction as some where error happend.
	END CATCH

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Column_OrderChange', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
