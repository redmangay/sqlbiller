﻿

-- When we UPDATE Table Detail page we UPDATE Base views by [dbo].[dbg_View_TableUpdate]
CREATE PROCEDURE   [dbo].[dbg_View_TableUpdate]
(
	@TableID int
)
 
AS


	-- Remove ViewItem when column has empty DisplayTextSummary
BEGIN TRY


	IF EXISTS (SELECT ViewItemID FROM [ViewItem] WHERE ColumnID IN 
			(SELECT ColumnID FROM [Column] WHERE TableID=@TableID
				 AND (DisplayTextSummary IS NULL OR LEN(DisplayTextSummary)=0 ) )
				 )
				BEGIN

						DELETE ViewItem WHERE 
						ColumnID IN (SELECT ColumnID FROM [Column] WHERE TableID=@TableID AND (DisplayTextSummary IS NULL 
						OR LEN(DisplayTextSummary)=0 ) )


					DECLARE @tView TABLE
					(
						ID int identity(1,1),
						ViewID INT
					)

					INSERT INTO @tView (ViewID)
						SELECT ViewID FROM [View] WHERE TableID=@TableID -- AND UserID=@UserID AND ViewPageType<>'dash'


					DECLARE @counter int
					DECLARE @ViewID INT;
					SET @counter = 1
					WHILE EXISTS(SELECT * FROM @tView WHERE ID >= @counter)
						BEGIN
							SELECT @ViewID=ViewID FROM @tView WHERE ID=@counter

							EXEC [dbo].[dbg_View_ColumnIndexReset] @ViewID


								 SET @counter = @counter + 1
						END

				END

		

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_View_TableUpdate', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
-- old code
	--BEGIN
			--DECLARE @AccountID INT
			--SELECT @AccountID=AccountID FROM [Table] WHERE  TableID=@TableID			
		
			--DECLARE @UserID INT =[dbo].[fnGetAccountHolderUserID] (@AccountID)	--Account Holder
			

			--DECLARE @tView TABLE
			--(
			--	ID int identity(1,1),
			--	ViewID INT
			--)

			--INSERT INTO @tView (ViewID)
			--	SELECT ViewID FROM [View] WHERE TableID=@TableID --AND UserID=@UserID AND ViewPageType<>'dash'
			--	-- Do we need to consider views of RoleTable.ViewsDefaultFromUserID then we can use UseID IN ( , )

			--DECLARE @counter int
			--DECLARE @nViewID INT;
			--SET @counter = 1
			--WHILE EXISTS(SELECT * FROM @tView WHERE ID >= @counter)
			--	BEGIN
			--		SELECT @nViewID=ViewID FROM @tView WHERE ID=@counter

			--		-- insert new ViewItem when column has DisplayTextSummary
			--			--INSERT INTO [ViewItem]
			--			--   ([ViewID]
			--			--   ,[ColumnID]
			--			--   ,[SearchField]
			--			--   ,[FilterField]
			--			--   ,[Alignment]
			--			--   ,[ShowTotal]
			--			--   ,[SummaryCellBackColor]
			--			--   ,[ColumnIndex])
		     
			--			--  SELECT @nViewID
			--			--   ,ColumnID
			--			--   ,SummarySearch 
			--			--   ,1
			--			--   ,Alignment
			--			--   ,ShowTotal
			--			--   ,SummaryCellBackColor
			--			--   ,(SELECT ISNULL(MAX(ColumnIndex),-1) + 1 FROM ViewItem WHERE ViewID=@nViewID) 
			--			--   FROM [Column] WHERE TableID=@TableID 
			--			--	AND ColumnID NOT IN (SELECT ColumnID FROM [ViewItem] WHERE ViewID=@nViewID)
			--			--   AND DisplayTextSummary IS NOT NULL AND LEN(DisplayTextSummary) > 0
			--			--   AND Systemname not in('IsActive','TableID') 
			--			--   AND ColumnType NOT IN ('staticcontent')
			--			--   ORDER BY DisplayOrder		

			--		-- Remove ViewItem when column has empty DisplayTextSummary
			--				DELETE ViewItem WHERE ViewID=@nViewID AND 
			--					ColumnID IN (SELECT ColumnID FROM [Column] WHERE TableID=@TableID AND (DisplayTextSummary IS NULL OR LEN(DisplayTextSummary)=0 ) )

			--		 SET @counter = @counter + 1
			--	END

		
	--END
