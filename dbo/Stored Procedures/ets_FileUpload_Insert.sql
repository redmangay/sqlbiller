﻿
---------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[ets_FileUpload_Insert]
(
	@nNewID int output,
	@nTableID int ,
	@sUploadName varchar(100) ,
	@sLocation varchar(250) ,
	@sFilename varchar(250),
	@bUseMapping bit,
	@nTemplateID int = NULL,
	@bSaveDocument bit,
	@sCriteria varchar(250) = NULL,
	@nDocumentTypeID int =NULL

)
AS
BEGIN TRY
	INSERT INTO FileUpload
	(
		TableID, 
		UploadName, 
		Location, 
		[Filename],
		UseMapping,
		TemplateID,
		SaveDocument,
		FileNameCriteria,
		DocumentTypeID
	) VALUES (
	
		@nTableID,
		@sUploadName,
		@sLocation,
		@sFilename,
		@bUseMapping,
		@nTemplateID,
		@bSaveDocument,
		@sCriteria ,
		@nDocumentTypeID 
	)
	SELECT @nNewID = @@IDENTITY
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_FileUpload_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

