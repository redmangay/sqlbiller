﻿

CREATE PROCEDURE [dbo].[ets_GraphOption_Update]
(
	@nGraphOptionID int ,
	@nAccountID int ,
	@nGraphPanel int ,
	@sHeading varchar(100)  = NULL,
	@bShowTrendline bit = NULL,
	@bShowLimits bit  = NULL,
	@bShowMissing bit  = NULL,
	@sTimePeriod char(1)  = NULL,
	@dFromDate datetime  = NULL,
	@dToDate datetime  = NULL,
	@sLegend varchar(10)  = NULL,
	@nWidth decimal(20,10)  = NULL,
	@nHeight decimal(20,10)  = NULL,
	@bUserReportDate bit  = NULL,
	@sCustomTimePeriod char(1)  = NULL,
	@bDisplay3D bit  = NULL,
	@bReportChart bit=0,
	@sWarningCaption nvarchar(100)  = NULL,
	@nWarningValue decimal(20,10)  = NULL,
	@sWarningColor varchar(50)  = NULL,
	@sExceedanceCaption nvarchar(100)  = NULL,
	@nExceedanceValue decimal(20,10)  = NULL,
	@sExceedanceColor varchar(50)  = NULL,
	@sDateFormat char(2)  = NULL,
	@sSubHeading varchar(100) = NULL,
	@nGraphDefinitionID int = NULL,
	@nYAxisHighestValue decimal(18, 10) = NULL,
	@nYAxisLowestValue decimal(18, 10) = NULL,
	@nYAxisInterval decimal(18, 10) = NULL,
	@nWarningValueMin decimal(20,10)  = NULL,
	@nExceedanceValueMin decimal(20,10)  = NULL,
	@sGraphName varchar(200) = NULL,
 	@nVisibleToUser int = NULL,
	@sMultiChartData varchar(MAX) = NULL,
	@nYAxisOrder int = NULL,
	@nSeriesColumnID int = NULL,
	@sSeriesDataSelected varchar(max) = NULL


 )
	/*----------------------------------------------------------------
	-- Stored Procedure: ets_GraphOption_Update
	-- Generated by: gpGenerateSP (Jon Bosker, DB Gurus Australia) 
	-- Date: Jan  8 2013  5:43PM
	---------------------------------------------------------------*/
 
AS

BEGIN TRY
	UPDATE [GraphOption]
	SET 
			AccountID = @nAccountID,
			GraphPanel = @nGraphPanel,
			Heading = @sHeading,
			ShowTrendline = @bShowTrendline,
			ShowLimits = @bShowLimits,
			ShowMissing = @bShowMissing,
			TimePeriod = @sTimePeriod,
			FromDate = @dFromDate,
			ToDate = @dToDate,
			Legend = @sLegend,
			Width = @nWidth,
			Height = @nHeight,
			UserReportDate = @bUserReportDate,
			CustomTimePeriod = @sCustomTimePeriod,
			Display3D = @bDisplay3D,
			ReportChart=@bReportChart,
			WarningCaption = @sWarningCaption,
			WarningValue = @nWarningValue,
			WarningColor = @sWarningColor,
			ExceedanceCaption = @sExceedanceCaption,
			ExceedanceValue = @nExceedanceValue,
			ExceedanceColor = @sExceedanceColor,
			[DateFormat] = @sDateFormat,
			SubHeading = @sSubHeading,
			GraphDefinitionID = @nGraphDefinitionID,
			YAxisHighestValue = @nYAxisHighestValue,
			YAxisLowestValue = @nYAxisLowestValue,
			YAxisInterval = @nYAxisInterval,
			WarningValueMin = @nWarningValueMin,
			ExceedanceValueMin = @nExceedanceValueMin,
			GraphName = @sGraphName,
 			VisibleToUser = @nVisibleToUser,
			MultiChartData = @sMultiChartData,
			YAxisOrder = @nYAxisOrder,
			SeriesColumnID = @nSeriesColumnID,
			SeriesDataSelected = @sSeriesDataSelected
		WHERE GraphOptionID = @nGraphOptionID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_GraphOption_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
