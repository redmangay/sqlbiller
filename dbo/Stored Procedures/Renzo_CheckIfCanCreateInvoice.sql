﻿-- =============================================
-- Author:		<Red Mangay>
-- Create date:	<10/07/2018>
-- Description:	<Get the tableid of the orginal table ie timesheet or disbursement>
-- =============================================
/*

exec [Renzo_CheckIfCanCreateInvoice]   2713275

*/


CREATE PROCEDURE [dbo].[Renzo_CheckIfCanCreateInvoice]
(
	@RecordID   INT
)
AS
	    BEGIN

		   DECLARE @TotalTimeSheetDetailsAmount   VARCHAR(MAX);
		   DECLARE @CountTimeSheetDetails int =0;
		   DECLARE @CountTimeSheetDetailsHoldStatus  int =0;
		   DECLARE @CanInvoiceOrNot   VARCHAR(MAX)= '';

		   --Lets get the total amount
		   SELECT
			 @TotalTimeSheetDetailsAmount = ISNULL(SUM(CAST(CONVERT(VARCHAR(20), [V007], 1) AS MONEY)), 0)
		   FROM  [Record]
		   WHERE [ISACTIVE] = 1
			    AND [TableID] = '2729'
			    AND [V001] = CAST(@RecordID AS VARCHAR)
			    AND [V016] IS NULL;
		  
		  --Lets counts them all
		   SELECT
			 @CountTimeSheetDetails = ISNULL(COUNT([RecordID]), 0)
		   FROM  [Record]
		   WHERE [ISACTIVE] = 1
			    AND [TableID] = '2729'
			    AND [V001] = CAST(@RecordID AS VARCHAR)
			    AND [V016] IS NULL;
		  
		  --Lets get the count of the Hold
		   SELECT
			 @CountTimeSheetDetailsHoldStatus = ISNULL(COUNT([RecordID]), 0)
		   FROM  [Record]
		   WHERE [ISACTIVE] = 1
			    AND [TableID] = '2729'
			    AND [V001] = CAST(@RecordID AS VARCHAR)
			    AND [V016] IS NULL
			    AND [V022] = '3';

		   IF(@CountTimeSheetDetails > '0')
			  BEGIN
				     -- Check if all records are hold status
					IF (@CountTimeSheetDetailsHoldStatus = @CountTimeSheetDetails )
					BEGIN
					    SET @CanInvoiceOrNot = 'False';
					END
					ELSE
					BEGIN
					    SET @CanInvoiceOrNot = 'True';
					END
			  END
			  ELSE 
			  BEGIN
			  SET @CanInvoiceOrNot = 'False'
			  END


		   SELECT
			 [TableID] = NULL
		    , [V001] = @TotalTimeSheetDetailsAmount
		    , [V002] = @CanInvoiceOrNot
		    , [V003] = NULL
		    , [V004] = NULL
		    , [V005] = NULL
		    , [V006] = NULL
		    , [V007] = NULL
		    , [V008] = NULL
		    , [V009] = NULL
		    , [V010] = NULL
		    , [V011] = NULL
		    , [V012] = NULL
		    , [V013] = NULL
		    , [V014] = NULL
		    , [V015] = NULL
		    , [V016] = NULL
		    , [V017] = NULL
		    , [V018] = NULL
		    , [V019] = NULL
		    , [V020] = NULL
		    , [V021] = NULL
		    , [V022] = NULL
		    , [V023] = NULL
		    , [V024] = NULL
		    , [V025] = NULL
		    , [V026] = NULL
		    , [V027] = NULL
		    , [V028] = NULL
		    , [V029] = NULL
		    , [V030] = NULL
		    , [V031] = NULL
		    , [V032] = NULL
		    , [V033] = NULL
		    , [V034] = NULL
		    , [V035] = NULL
		    , [V036] = NULL
		    , [V037] = NULL
		    , [V038] = NULL
		    , [V039] = NULL
		    , [V040] = NULL
		    , [V041] = NULL
		    , [V042] = NULL
		    , [V043] = NULL
		    , [V044] = NULL
		    , [V045] = NULL
		    , [V046] = NULL
		    , [V047] = NULL
		    , [V048] = NULL
		    , [V049] = NULL
		    , [V050] = NULL
		    , [V051] = NULL
		    , [V052] = NULL
		    , [V053] = NULL
		    , [V054] = NULL
		    , [V055] = NULL
		    , [V056] = NULL
		    , [V057] = NULL
		    , [V058] = NULL
		    , [V059] = NULL
		    , [V060] = NULL
		    , [V061] = NULL
		    , [V062] = NULL
		    , [V063] = NULL
		    , [V064] = NULL
		    , [V065] = NULL
		    , [V066] = NULL
		    , [V067] = NULL
		    , [V068] = NULL
		    , [V069] = NULL
		    , [V070] = NULL
		    , [V071] = NULL
		    , [V072] = NULL
		    , [V073] = NULL
		    , [V074] = NULL
		    , [V075] = NULL
		    , [V076] = NULL
		    , [V077] = NULL
		    , [V078] = NULL
		    , [V079] = NULL
		    , [V080] = NULL
		    , [V081] = NULL
		    , [V082] = NULL
		    , [V083] = NULL
		    , [V084] = NULL
		    , [V085] = NULL
		    , [V086] = NULL
		    , [V087] = NULL
		    , [V088] = NULL
		    , [V089] = NULL
		    , [V090] = NULL
		    , [V091] = NULL
		    , [V092] = NULL
		    , [V093] = NULL
		    , [V094] = NULL
		    , [V095] = NULL
		    , [V096] = NULL
		    , [V097] = NULL
		    , [V098] = NULL
		    , [V099] = NULL
		    , [V100] = NULL;
	    END;

