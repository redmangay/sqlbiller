﻿create procedure  usr_rptMAILTOEmailAddresses 
 
as

SELECT ClientContact.Email
FROM  [Account24918].[vClient Contact]  ClientContact
WHERE (((ClientContact.Email) Like '%@%') AND ((ClientContact.MailOuts)='True'));