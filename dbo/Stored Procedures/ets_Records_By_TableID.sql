﻿

CREATE PROCEDURE [dbo].[ets_Records_By_TableID]
(
	
	@nTableID int
)

/*
EXEC [ets_Records_By_TableID] 1169
*/
AS

BEGIN TRY
	SET NOCOUNT ON;

	DECLARE @tDisplayColumns TABLE
	(
		ID int identity(1,1),
		ColumnID int,
		SystemName varchar(50),
		DisplayText varchar(100)
	)



	INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
		SELECT ColumnID, DisplayName, SystemName
			FROM [Column] 
			WHERE TableID = @nTableID 
			AND( SystemName ='RecordID' OR ColumnType='number')
			ORDER BY DisplayOrder



	DECLARE @sSQL varchar(MAX)
	
	DECLARE @counter int
	SET @counter = 1
		

  
	
	SELECT @sSQL = ' SELECT '

   
    SET @counter = 1
	WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID >= @counter)
	BEGIN
		SELECT @sSQL = @sSQL + 'Record.' + SystemName + ' AS [' + DisplayText + '], ' 
			FROM @tDisplayColumns 
			WHERE ID = @counter
		--PRINT @sSQL
		SET @counter = @counter + 1
		
	END 

	SELECT @sSQL = LEFT(@sSQL, LEN(@sSQL)-1) 
	
	SELECT @sSQL = @sSQL + ' ,Record.DateTimeRecorded,Record.WarningResults,Record.ValidationResults FROM [Record] '


	
	-- ADD IN THE WHERE CRITERIA
	SELECT @sSQL = @sSQL + ' WHERE Record.TableID = ' + CAST(@nTableID as varchar) + ' ORDER BY Record.DateTimeRecorded'
	
        
    
    
	PRINT @sSQL
	EXEC (@sSQL)
	-- PRINT @sTempTable
	
	SET ROWCOUNT 0

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Records_By_TableID', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
