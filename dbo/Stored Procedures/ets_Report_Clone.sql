﻿

CREATE PROCEDURE   [dbo].[ets_Report_Clone]
(
	@nReportFromID int, 
	@nReportToID int 
)
AS

BEGIN TRY
	DECLARE @newReportItemID int
	DECLARE @ReportItemID int
	DECLARE @ObjectID int
	DECLARE @ItemType int
	DECLARE @ItemTitle varchar(500)
	DECLARE @ItemPosition int
	DECLARE @UserID int
	DECLARE @PeriodColumnID int
	DECLARE @UseColors bit
	DECLARE @ApplyFilter bit
	DECLARE @ApplySort bit

	DECLARE ObjectCursor CURSOR FOR
		SELECT ReportItemID, ObjectID, ItemType, ItemTitle, ItemPosition, UserID, PeriodColumnID, UseColors, ApplyFilter, ApplySort
		FROM ReportItem WHERE ReportID = @nReportFromID
	OPEN ObjectCursor  

	FETCH NEXT FROM ObjectCursor   
		INTO @ReportItemID, @ObjectID, @ItemType, @ItemTitle, @ItemPosition, @UserID, @PeriodColumnID, @UseColors, @ApplyFilter, @ApplySort

	WHILE @@FETCH_STATUS = 0  
	BEGIN 
		INSERT INTO ReportItem
			(ReportID, ObjectID, ItemType, ItemTitle, ItemPosition, UserID, PeriodColumnID, UseColors, ApplyFilter, ApplySort)
		VALUES
			(@nReportToID, @ObjectID, @ItemType, @ItemTitle, @ItemPosition, @UserID, @PeriodColumnID, @UseColors, @ApplyFilter, @ApplySort)
		SELECT @newReportItemID = @@IDENTITY

		INSERT INTO ReportItemFilter
			(ReportItemID, FilterColumnID, FilterColumnValue, FilterOperator, DisplayOrder, JoinOperator)
		SELECT @newReportItemID, FilterColumnID, FilterColumnValue, FilterOperator, DisplayOrder, JoinOperator
				FROM ReportItemFilter WHERE ReportItemID = @ReportItemID

		INSERT INTO ReportItemSortOrder
			(ReportItemID, SortColumnID, IsDescending, DisplayOrder)
		SELECT @newReportItemID, SortColumnID, IsDescending, DisplayOrder
				FROM ReportItemSortOrder WHERE ReportItemID = @ReportItemID

		INSERT INTO ReportTableItem
			(ReportItemID, ColumnID, ColumnTitle, ColumnPosition)
		SELECT @newReportItemID, ColumnID, ColumnTitle, ColumnPosition
				FROM ReportTableItem WHERE ReportItemID = @ReportItemID

		FETCH NEXT FROM ObjectCursor   
			INTO @ReportItemID, @ObjectID, @ItemType, @ItemTitle, @ItemPosition, @UserID, @PeriodColumnID, @UseColors, @ApplyFilter, @ApplySort  
	END
	CLOSE ObjectCursor
	DEALLOCATE ObjectCursor

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Report_Clone', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
