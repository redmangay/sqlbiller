﻿
/*
    Author: Red Mangay
    Date:	  19/06/2018

*/

CREATE procedure [dbo].[usr_rptChargeAbility]
(
	@Group varchar(max)='1',
	@Employee varchar(max)='1',
	@DateFrom DATE,
	@DateTo	 DATE,
	@SaveChargeability bit = 0

)
AS

/*

EXEC [dbo].[usr_rptChargeAbility] @DateFrom= '01/01/2020', @DateTo ='29/02/2020', @SaveChargeability = 0 , @Group ='1', @Employee='1'


*/
BEGIN TRY
	SET DATEFORMAT dmy;
	SET LANGUAGE British;

DECLARE @SQLGroup varchar(max) = ''
DECLARE @SQLEmployee varchar(max) = ''

 DECLARE @ChargeabilityFinal Table 
	   (
		  EmployeeRecordID int,
		   GroupDivision varchar(max) ,
		   EmployeeName varchar(max),
		   AvHrPerWk money,
		   HrsSchedLeave money,/* Need From Administration Code */
		   HrsOtherLeave money,/* Need From Administration Code */
		   TimeWorked money,
		   TimeChargedToJobs money,
		   UtilPercent money,
		   Billed money,
		   Adj money,
		   BilledTotal money,
		   EfficiencyPercent money,
		   ChgRate money,
		   TotalHourWorked money, 
		   TotalHourWorkable money,
		   TotalWorkHoursPerWeek money,
		   TotalHourWorkablePerYear money,
		   HourlyRate money,
		   Paid money,
		   HoursPerDay_X_WokringDaysPerAWeek money,
		   Avg_X varchar(50)
	   
	   
	   
	   )
    
IF @Group <> 1 AND @Group is not null
BEGIN
SET @SQLGroup = ' AND [Group].RecordID =' + @Group + ''
END

IF @Employee <> 1 AND @Employee is not null
BEGIN
SET @SQLEmployee = ' AND [Employee].RecordID =' + @Employee + ''
END

DECLARE @EmployeeList Table 
(
    [Counter] int identity(1,1),
    EmployeeRecordID int,
    [Type] varchar(50),
    [Group] varchar(max),
    [EmployeeName] varchar(max),
    [RenumerationPerYear] money
)






DECLARE @SQL varchar(max) = ''

SET @SQL = '
SELECT DISTINCT [Employee].RecordID,[Employee].[V023],[Group].V001,[Employee].V003,CASE WHEN [Employee].V023 = ''$Hourly Rate'' THEN ISNULL([Employee].V025,0) ELSE ISNULL([Employee].V026,0) END
    FROM Record [Employee]
    JOIN Record [Group] ON [Group].TableID = 2727
	   AND [Group].IsActive = 1
	   AND [Group].RecordID = [Employee].V022
	   --AND [Group].RecordID =  16605068
	   '+ @SQLGroup +'
   --LEFT JOIN Record [Timesheet] ON  [Timesheet].TableID = 2729
			--	    AND [Timesheet].IsActive = 1
			--	    AND [Timesheet].V002 = CAST([Employee].RecordID as varchar)
			--	    AND [Timesheet].V011 IS NOT NULL /* Only Timesheet not Disbursement */
				   
			--	    AND [Timesheet].V016 IS NOT NULL
			--	--JOIN Record [Job Information] ON [Job Information].TableID = 2475
			--	--    AND [Job Information].IsActive = 1
			--	--    AND [Job Information].RecordID = [Timesheet].V001
			--	LEFT JOIN Record [Billing Information] ON [Billing Information].TableID = 2711
			--	    AND [Billing Information].IsActive = 1
			--	    AND [Billing Information].RecordID = [Timesheet].V016 
			--	    AND CONVERT(date, [Billing Information].V004) >='''+ CAST(@DateFrom as varchar) +'''
			--	    AND CONVERT(date, [Billing Information].V004) <=''' + CAST(@DateTo as varchar) +'''
				
    WHERE [Employee].TableID = 2669
    AND [Employee].IsActive = 1
      AND [Employee].V002 = ''FALSE''
   '+ @SQLEmployee +'
	   ORDER BY [Employee].V003'


pRINT @sql
INSERT INTO @EmployeeList
EXEC (@SQL);

--SELECT DISTINCT [Employee].RecordID,[Employee].[V023],[Group].V001,[Employee].V003,ISNULL([Employee].V026,0)
--    FROM Record [Employee]
--    JOIN Record [Group] ON [Group].TableID = 2727
--	   AND [Group].IsActive = 1
--	   AND [Group].RecordID = [Employee].V022
--    JOIN Record [Timesheet] ON  [Timesheet].TableID = 2729
--				    AND [Timesheet].IsActive = 1
--				    AND [Timesheet].V002 = CAST([Employee].RecordID as varchar)
--				    AND [Timesheet].V011 IS NOT NULL /* Only Timesheet not Disbursement */
				   
--				    AND [Timesheet].V016 IS NOT NULL
--				JOIN Record [Job Information] ON [Job Information].TableID = 2475
--				    AND [Job Information].IsActive = 1
--				    AND [Job Information].RecordID = [Timesheet].V001
--				JOIN Record [Billing Information] ON [Billing Information].TableID = 2711
--				    AND [Billing Information].IsActive = 1
--				    AND [Billing Information].RecordID = [Timesheet].V016 
--				    AND CONVERT(date, [Billing Information].V004) >= CAST(@DateFrom as varchar)
--				    AND CONVERT(date, [Billing Information].V004) <= CAST(@DateTo as varchar)
				
--    WHERE [Employee].TableID = 2669
--    AND [Employee].IsActive = 1
--    --AND [Employee].RecordID IN ( SELECT [Timesheet].V002 FROM Record [Timesheet] 
--				--JOIN Record [Job Information] ON [Job Information].TableID = 2475
--				--    AND [Job Information].IsActive = 1
--				--    AND [Job Information].RecordID = [Timesheet].V001
--				--JOIN Record [Billing Information] ON [Billing Information].TableID = 2711
--				--    AND [Billing Information].IsActive = 1
--				--    AND [Billing Information].RecordID = [Timesheet].V016 
--				--    AND CONVERT(date, [Billing Information].V004) >= CAST(@DateFrom as varchar)
--				--    AND CONVERT(date, [Billing Information].V004) <= CAST(@DateTo as varchar)
--				--WHERE [Timesheet].TableID = 2729
--				--    AND [Timesheet].IsActive = 1
--				--    --AND [Timesheet].V002 = CAST(@EmployeeRecordID as varchar)
--				--    AND [Timesheet].V011 IS NOT NULL /* Only Timesheet not Disbursement */
				   
--				--    AND [Timesheet].V016 IS NOT NULL)
--	   ORDER BY [Employee].V003


			




    --SELECT * FROM @EmployeeList
    
    DECLARE @EmployeeRecordID int
    DECLARE @Type varchar(50)
    DECLARE @GroupDivision varchar(max)
    DECLARE @EmployeeName varchar(max)
    DECLARE @RenumerationPerYear money

    --SET @DateFrom = '01/06/2018' 
    --SET @DateTo  = '17/06/2018' 

    DECLARE Employee CURSOR FOR
    SELECT 
		  EmployeeRecordID ,
		  [Type],
		  [Group] ,
		  [EmployeeName] ,
		  [RenumerationPerYear] 
	   FROM @EmployeeList
	   --SELECT EmployeeRecordID,[V023],[Group].V001,[Employee].V003,ISNULL([Employee].V026,0)

    --FROM @EmployeeList
    --JOIN Record [Group] ON [Group].TableID = 2727
	   --AND [Group].IsActive = 1
	   --AND [Group].RecordID = [Employee].V022
     
    ----JOIN Record [Timesheet] ON [Timesheet].TableID = 2729
	   ----AND [Timesheet].IsActive = 1
	   ----AND  CAST([Employee].RecordID as varchar) = [Timesheet].V002 
	   ----AND [Timesheet].V010 IS NOT NULL /* Only Timesheet not Disbursement */
	   ----AND CONVERT(date, [Timesheet].V009) >= CAST('16/06/2018' as varchar)
	   ----AND CONVERT(date, [Timesheet].V009) >= CAST('15/07/2018' as varchar)
    --WHERE [Employee].TableID = 2669
    --AND [Employee].IsActive = 1
    ----ORDER BY V003
    ----AND [Employee].V026 NOT LIKE '%[a-z]%'
    --AND [Employee].RecordID IN ( SELECT [Timesheet].V002 FROM Record [Timesheet] 
	   --WHERE [Timesheet].TableID = 2729
	   --AND [Timesheet].IsActive = 1
	   ----AND [Timesheet].V002 = CAST([Employee].RecordID as varchar)
	   --AND [Timesheet].V011 IS NOT NULL /* Only Timesheet not Disbursement */
	   --AND CONVERT(date, [Timesheet].V009) >= CAST('01/06/2018' as varchar)
	   --AND CONVERT(date, [Timesheet].V009) <= CAST('17/06/2018' as varchar))
	   --ORDER BY [Employee].V003
    ----AND [Employee].V002 = 'False'
    ----AND [Employee].V027 = '1'

    OPEN Employee
    FETCH NEXT FROM Employee INTO @EmployeeRecordID, @Type,@GroupDivision,@EmployeeName,@RenumerationPerYear
 
    WHILE @@FETCH_STATUS = 0
    BEGIN

    /* ======================================================================================== */
    /* Summary */
	   --DECLARE @GroupDivision varchar(max)
	   --DECLARE @EmployeeName varchar(max)
	   DECLARE @AvHrPerWk money =0.00
	   DECLARE @HrsSchedLeave money = 0.00 /* Need From Administration Code */
	   DECLARE @HrsOtherLeave money = 0.00 /* Need From Administration Code */
	   DECLARE @TimeWorked money =0.00
	   DECLARE @TimeChargedToJobs money =0.00
	   DECLARE @UtilPercent float = 0.00
	   DECLARE @Billed money = 0.00
	   DECLARE @Adj money = 0.00
	   DECLARE @BilledTotal money = 0.00
	   DECLARE @EfficiencyPercent float =0.00
	   DECLARE @ChgRate float = 0.00
	   DECLARE @TotalAdminCode money = 0.00
	   DECLARE @TotalAdminCodeAmount money = 0.00
    
	   /* Details */
	   DECLARE @TotalHourWorked money = 0.00
	   DECLARE @TotalHourWorkable money = 0.00
	   DECLARE @TotalWorkHoursPerWeek money = 0.00
	   DECLARE @TotalHourWorkablePerYear money = 0.00
	   DECLARE @HourlyRate numeric(18,5)
	   DECLARE @Paid money = 0.00
	   DECLARE @HoursPerDay_X_WokringDaysPerAWeek money = 0.00
	   --DECLARE @RenumerationPerYear money = 0.00
			 --DECLARE @EmployeeRecordID int
			 DECLARE @EmployeePackageType varchar(50)

			 --SELECT	@EmployeeRecordID = EmployeeRecordID, 
				--    @EmployeePackageType = [Type], 
				--    @GroupDivision = [Group], 
				--    @EmployeeName = [EmployeeName],
				--    @RenumerationPerYear = [RenumerationPerYear] 
				--FROM @EmployeeList 
				--WHERE [Counter] = @Counter

			 --JOIN Record [Group] ON [Group].TableID = 2727
				--AND [Group].IsActive = 1
				--AND [Group].RecordID = [Employee].V022


		  /* Get the sched leave */		  
			 SELECT @HrsSchedLeave =SUM(CAST(V015 AS MONEY)) FROM RECORD
			 WHERE TABLEID = 2488
			 AND ISACTIVE = 1
			 AND V041 = '1'
			 AND V017 = CAST(@EmployeeRecordID as varchar) --'12911944'
			 AND CONVERT(date, V004) >= CAST(@DateFrom as varchar)
			 AND CONVERT(date, V004) <= CAST(@DateTo as varchar)
			 AND V043 IN --('S022','S027','S028')
			  (SELECT V001 FROM Record
				WHERE TableID = 2650 /* Administration: Type - Other Leave */
				AND IsActive = 1
				AND V003 = '2' )



		  /* Get the other leave */		  
			 SELECT  @HrsOtherLeave= SUM(CAST(V015 AS MONEY)) FROM RECORD
			 WHERE TABLEID = 2488
			 AND ISACTIVE = 1
			 AND V041 = '1'
			 AND V017 = CAST(@EmployeeRecordID as varchar)  --'12911944'
			 AND CONVERT(date, V004) >= CAST(@DateFrom as varchar)
			 AND CONVERT(date, V004) <= CAST(@DateTo as varchar)
			 AND V043 IN --('S021','S024','S026')
			 (SELECT V001 FROM Record
				WHERE TableID = 2650 /* Administration: Type - Sched Leave */
				AND IsActive = 1
				AND V003 = '3' )


			   /* Get all the admin */		  
			 SELECT @TotalAdminCode =SUM(CAST(V015 AS MONEY)),  @TotalAdminCodeAmount =SUM(CAST(V019 AS MONEY)) FROM RECORD
			 WHERE TABLEID = 2488
			 AND ISACTIVE = 1
			 AND V041 = '1'
			 AND V017 = CAST(@EmployeeRecordID as varchar) --'12911944'
			 AND CONVERT(date, V004) >= CAST(@DateFrom as varchar)
			 AND CONVERT(date, V004) <= CAST(@DateTo as varchar)

			    /* Get all the Timesheet Amount */		  
			 SELECT @TimeWorked = SUM(CAST(CAST(ISNULL(V028,0) as money) * CAST(ISNULL(V029,0) as money) * CAST(ISNULL(V030,0) as money)  as numeric(18,2))) FROM Record [Timesheet] 
				WHERE [Timesheet].TableID = 2729
				AND [Timesheet].IsActive = 1
				AND [Timesheet].V002 = CAST(@EmployeeRecordID as varchar)
				AND [Timesheet].V011 IS NOT NULL /* Only Timesheet not Disbursement */
				AND CONVERT(date, [Timesheet].V009) >= CAST(@DateFrom as varchar)
				AND CONVERT(date, [Timesheet].V009) <= CAST(@DateTo as varchar)


		

			 --SELECT @TotalHourWorked = SUM(CAST(ISNULL(V005,0) as numeric(18,2))) FROM Record [Timesheet] 
				--WHERE [Timesheet].TableID = 2729
				--AND [Timesheet].IsActive = 1
				--AND [Timesheet].V002 = CAST(@EmployeeRecordID as varchar)
				--AND [Timesheet].V011 IS NOT NULL /* Only Timesheet not Disbursement */
				--AND CONVERT(date, [Timesheet].V009) >= CAST(@DateFrom as varchar)
				--AND CONVERT(date, [Timesheet].V009) <= CAST(@DateTo as varchar)


				SELECT @TotalHourWorked = SUM(CAST(ISNULL(V015,0) as numeric(18,2))) FROM Record [Timesheet] 
				WHERE [Timesheet].TableID = 2488
				AND [Timesheet].IsActive = 1
				AND [Timesheet].V017 = CAST(@EmployeeRecordID as varchar)
				--AND [Timesheet].V011 IS NOT NULL /* Only Timesheet not Disbursement */
				AND CONVERT(date, [Timesheet].V004) >= CAST(@DateFrom as varchar)
				AND CONVERT(date, [Timesheet].V004) <= CAST(@DateTo as varchar)
				AND [Timesheet].V041 ='2'
			
			 --SELECT @TotalHourWorked,@TotalAdminCode,@HrsOtherLeave,@HrsSchedLeave
			 --SELECT @TimeWorked
			 
			 SET @TotalHourWorked = ISNULL(@TotalHourWorked,0) + ISNULL(@TotalAdminCode,0) - ISNULL(@HrsOtherLeave,0) - ISNULL(@HrsSchedLeave,0)
			 SET @TimeWorked = ISNULL(@TimeWorked,0) + ISNULL(@TotalAdminCodeAmount,0)
			 --SELECT @TimeWorked,@TotalAdminCodeAmount

			 /* Add Filter to not include Admin codes */
				SELECT  @TimeChargedToJobs = SUM(CAST(CAST(ISNULL(V028,0) as money) * CAST(ISNULL(V029,0) as money) * CAST(ISNULL(V030,0) as money)  as numeric(18,2))) FROM Record [Timesheet]  --SUM(CAST(ISNULL(V031,0) as numeric(18,2))) FROM Record [Timesheet] 
				WHERE [Timesheet].TableID = 2729
				AND [Timesheet].IsActive = 1
				AND [Timesheet].V002 = CAST(@EmployeeRecordID as varchar)
				AND [Timesheet].V011 IS NOT NULL /* Only Timesheet not Disbursement */
				AND CONVERT(date, [Timesheet].V009) >= CAST(@DateFrom as varchar)
				AND CONVERT(date, [Timesheet].V009) <= CAST(@DateTo as varchar)

				    /* Only Invoice, Converted to Job Currency */
				SELECT  @Billed = SUM(CAST(ISNULL(CAST([Timesheet].V020 as money),0) /   CAST(ISNULL([Job Information].V007,1) as money)  as money)) 
				FROM Record [Timesheet] 
				JOIN Record [Job Information] ON [Job Information].TableID = 2475
				    AND [Job Information].IsActive = 1
				    AND [Job Information].RecordID = [Timesheet].V001
				JOIN Record [Billing Information] ON [Billing Information].TableID = 2711
				    AND [Billing Information].IsActive = 1
				    AND [Billing Information].RecordID = [Timesheet].V016 
				    AND CONVERT(date, [Billing Information].V004) >= CAST(@DateFrom as varchar)
				    AND CONVERT(date, [Billing Information].V004) <= CAST(@DateTo as varchar)
				WHERE [Timesheet].TableID = 2729
				    AND [Timesheet].IsActive = 1
				    AND [Timesheet].V002 = CAST(@EmployeeRecordID as varchar)
				    AND [Timesheet].V011 IS NOT NULL /* Only Timesheet not Disbursement */
				   
				    AND [Timesheet].V016 IS NOT NULL

				--    select *
			 --	FROM Record [Timesheet] 
				--JOIN Record [Job Information] ON [Job Information].TableID = 2475
				--    AND [Job Information].IsActive = 1
				--    AND [Job Information].RecordID = [Timesheet].V001
				--JOIN Record [Billing Information] ON [Billing Information].TableID = 2711
				--    AND [Billing Information].IsActive = 1
				--    AND [Billing Information].RecordID = [Timesheet].V016 
				--    AND CONVERT(date, [Billing Information].V004) >= CAST(@DateFrom as varchar)
				--    AND CONVERT(date, [Billing Information].V004) <= CAST(@DateTo as varchar)
				--WHERE [Timesheet].TableID = 2729
				--    AND [Timesheet].IsActive = 1
				--    AND [Timesheet].V002 = CAST(@EmployeeRecordID as varchar)
				--    AND [Timesheet].V011 IS NOT NULL /* Only Timesheet not Disbursement */
				   
				--    AND [Timesheet].V016 IS NOT NULL

			 /* Get BAPA Credit Entry related to an employee  */
			 SELECT @Adj = sum(CAST([Credit Details].V003 as numeric(18,2)))
				FROM Record [Credit Details] 
				JOIN Record [Billing Information] ON [Billing Information].TableID = 2711
				    AND [Billing Information].IsActive = 1
				    AND [Billing Information].RecordID = [Credit Details].V001
				    AND CONVERT(date, [Billing Information].V004) >= CAST(@DateFrom as varchar)
				    AND CONVERT(date, [Billing Information].V004) <= CAST(@DateTo as varchar)
				WHERE [Credit Details].TableID = 2765
				    AND [Credit Details].IsActive = 1
				    AND [Credit Details].V002 = CAST(@EmployeeRecordID as varchar)
	    
			 /* Get the Billed Total - Billed less the Adj (Credit agains the employee) */
			 SET @BilledTotal = @Billed + ISNULL(@Adj,0)
			 SET @EfficiencyPercent = (CAST(@BilledTotal / CASE WHEN @TimeChargedToJobs = 0 THEN 1 ELSE ISNULL(@TimeChargedToJobs,1) END as numeric(18,2))) * 100
			 --SET @ChgRate = CAST(@BilledTotal / CASE WHEN @TimeChargedToJobs = 0 THEN 1 ELSE ISNULL(@TimeChargedToJobs,1) END as numeric(18,2))
	   
		  IF (@EmployeePackageType = '$Annual Package')
		  BEGIN
			 SELECT @TotalHourWorkable =
				(datediff(day, -7, @DateTo)/7-datediff(day, -6, @DateFrom)/7) * CAST(ISNULL(V012,0) AS MONEY) +
				(datediff(day, -6, @DateTo)/7-datediff(day, -5, @DateFrom)/7) * CAST(ISNULL(V013,0) AS MONEY) +
				(datediff(day, -5, @DateTo)/7-datediff(day, -4, @DateFrom)/7) * CAST(ISNULL(V014,0) AS MONEY) +
				(datediff(day, -4, @DateTo)/7-datediff(day, -3, @DateFrom)/7) * CAST(ISNULL(V015,0) AS MONEY) +
				(datediff(day, -3, @DateTo)/7-datediff(day, -2, @DateFrom)/7) * CAST(ISNULL(V016,0) AS MONEY) +
				(datediff(day, -2, @DateTo)/7-datediff(day, -2, @DateFrom)/7) * CAST(ISNULL(V017,0) AS MONEY) +
				(datediff(day, -1, @DateTo)/7-datediff(day, -2, @DateFrom)/7) * CAST(ISNULL(V018,0) AS MONEY)  
				,@TotalWorkHoursPerWeek = V007
				,@HoursPerDay_X_WokringDaysPerAWeek = 
				CASE WHEN V012 IS NOT NULL AND CAST(V012 as money) > 1 THEN CAST(V012 as numeric(18,2)) ELSE 0 END + 
				    CASE WHEN V013 IS NOT NULL  AND CAST(V013 as money) > 1 THEN CAST(V013 as numeric(18,2)) ELSE 0 END + 
				    CASE WHEN V014 IS NOT NULL  AND CAST(V014 as money) > 1 THEN CAST(V014 as numeric(18,2)) ELSE 0 END + 
				    CASE WHEN V015 IS NOT NULL  AND CAST(V015 as money) > 1 THEN CAST(V015 as numeric(18,2)) ELSE 0 END + 
				    CASE WHEN V016 IS NOT NULL  AND CAST(V016 as money) > 1 THEN CAST(V016 as numeric(18,2)) ELSE 0 END + 
					   CASE WHEN V017 IS NOT NULL  AND CAST(V017 as money) > 1 THEN CAST(V017 as numeric(18,2)) ELSE 0 END + 
					   CASE WHEN V018 IS NOT NULL  AND CAST(V018 as money) > 1 THEN CAST(V018 as numeric(18,2)) ELSE 0 END 
 
				FROM Record
				WHERE RecordID = @EmployeeRecordID
		  END
		  ELSE
		  BEGIN
		  /* We are going to get and use the last record with Package Type */
			 SELECT @TotalHourWorkable =
				(datediff(day, -7, @DateTo)/7-datediff(day, -6, @DateFrom)/7) * CAST(ISNULL(V014,0) AS MONEY) +
				(datediff(day, -6, @DateTo)/7-datediff(day, -5, @DateFrom)/7) * CAST(ISNULL(V015,0) AS MONEY) +
				(datediff(day, -5, @DateTo)/7-datediff(day, -4, @DateFrom)/7) * CAST(ISNULL(V016,0) AS MONEY) +
				(datediff(day, -4, @DateTo)/7-datediff(day, -3, @DateFrom)/7) * CAST(ISNULL(V017,0) AS MONEY) +
				(datediff(day, -3, @DateTo)/7-datediff(day, -2, @DateFrom)/7) * CAST(ISNULL(V018,0) AS MONEY) +
				(datediff(day, -2, @DateTo)/7-datediff(day, -2, @DateFrom)/7) * CAST(ISNULL(V019,0) AS MONEY) +
				(datediff(day, -1, @DateTo)/7-datediff(day, -2, @DateFrom)/7) * CAST(ISNULL(V020,0) AS MONEY) 
				,@TotalWorkHoursPerWeek = V008
				,@HoursPerDay_X_WokringDaysPerAWeek = 
				CASE WHEN V014 IS NOT NULL AND CAST(V014 as money) > 1 THEN CAST(V014 as numeric(18,2)) ELSE 0 END + 
				    CASE WHEN V015 IS NOT NULL  AND CAST(V015 as money) > 1 THEN CAST(V015 as numeric(18,2)) ELSE 0 END + 
				    CASE WHEN V016 IS NOT NULL  AND CAST(V016 as money) > 1 THEN CAST(V016 as numeric(18,2)) ELSE 0 END + 
				    CASE WHEN V017 IS NOT NULL  AND CAST(V017 as money) > 1 THEN CAST(V017 as numeric(18,2)) ELSE 0 END + 
				    CASE WHEN V018 IS NOT NULL  AND CAST(V018 as money) > 1 THEN CAST(V018 as numeric(18,2)) ELSE 0 END + 
					   CASE WHEN V019 IS NOT NULL  AND CAST(V019 as money) > 1 THEN CAST(V019 as numeric(18,2)) ELSE 0 END + 
					   CASE WHEN V020 IS NOT NULL  AND CAST(V020 as money) > 1 THEN CAST(V020 as numeric(18,2)) ELSE 0 END 
 
				FROM Record
				WHERE TableID = 2670 AND 
				V021 = @EmployeeRecordID /* Employee History Name */
				AND RecordID = (SELECT top 1 RecordID FROM Record WHERE V021 =@EmployeeRecordID AND TableID = 2670 AND V012 = 'True' AND IsActive = 1 ORDER BY CAST(V003 AS DATE) DESC)

		  END

		  /* lets get the No of working days and the */
		  DECLARE @NoOfWorkingDays FLOAT
		  DECLARE @Divisor FLOAT
		  SELECT @NoOfWorkingDays =
		  (DATEDIFF(dd, @DateFrom, @DateTo) + 1)
		  -(DATEDIFF(wk, @DateFrom, @DateTo) * 2)
		  -(CASE WHEN DATENAME(dw, @DateFrom) = 'Sunday' THEN 1 ELSE 0 END)
		  -(CASE WHEN DATENAME(dw, @DateTo) = 'Saturday' THEN 1 ELSE 0 END)
		  
		  SET @Divisor = @NoOfWorkingDays / 5.00
		  --SELECT @Divisor,@NoOfWorkingDays
		  /* end get */


		  --SELECT *
		  -- FROM Record
		  -- WHERE RecordID = @EmployeeRecordID

			 --SELECT *
			 --FROM Record
			 --WHERE TableID = 2670 AND 
			 --V021 = '12911831' /* Employee History Name */
			 --AND RecordID = (SELECT top 1 RecordID FROM Record WHERE V021 ='12911831' AND TableID = 2670 AND V012 = 'True' AND IsActive = 1 ORDER BY CAST(V003 AS DATE) DESC)

			 --SET @AvHrPerWk = Round(@TotalHourWorked / CASE WHEN @TotalHourWorkable = 0 THEN 1 ELSE ISNULL(@TotalHourWorkable,1) END * @TotalWorkHoursPerWeek,2)
			 --SELECT @TotalHourWorked,@HrsSchedLeave,@TotalHourWorkable,@TotalWorkHoursPerWeek
			 SET @AvHrPerWk = CASE WHEN @Type  = '$Hourly Rate' AND ISNULL(@TotalWorkHoursPerWeek,0)=0 AND ISNULL(@TotalHourWorkable,0) = 0 THEN Round((ISNULL(@TotalHourWorked,0) + ISNULL(@HrsSchedLeave,0))/@Divisor,2)  ELSE  Round(((ISNULL(@TotalHourWorked,1) + ISNULL(@HrsSchedLeave,0))/CASE WHEN @TotalHourWorkable = 0 THEN 1 ELSE ISNULL(@TotalHourWorkable,1) END)* @TotalWorkHoursPerWeek,2) END

			 SET @UtilPercent =    (@TimeChargedToJobs / CASE WHEN @TimeWorked = 0 THEN 1 ELSE ISNULL(@TimeWorked,1) END) * 100
			 SET @TotalHourWorkablePerYear = CAST(@HoursPerDay_X_WokringDaysPerAWeek * 48 as numeric(18,5))
			 SET @HourlyRate = CASE WHEN @Type = '$Hourly Rate' THEN CAST(@RenumerationPerYear as numeric(18,5)) ELSE  CAST(CAST(@RenumerationPerYear as numeric(18,5)) / CASE WHEN @TotalHourWorkablePerYear = 0 THEN 1 ELSE CAST(ISNULL(@TotalHourWorkablePerYear,1) as numeric(18,5)) END as numeric(18,5)) END
			 SET @Paid = CASE WHEN @Type  = '$Hourly Rate' THEN  CAST((ISNULL(@TotalHourWorked,0) - ISNULL(@HrsSchedLeave,0) ) * ISNULL(@HourlyRate,1) as numeric(18,2)) ELSE  CAST((ISNULL(@TotalHourWorkable,0) - ISNULL(@HrsSchedLeave,0) ) * ISNULL(@HourlyRate,1) as numeric(18,2)) END
			 --SELECT CAST(CAST(75000 AS NUMERIC(18,5)) / CAST(1800 AS NUMERIC(18,5)) AS NUMERIC(18,5))
			 SET @ChgRate = CAST(@BilledTotal / CASE WHEN @Paid = 0 THEN 1 ELSE ISNULL(@Paid,1) END as numeric(18,2))
			 
			 DECLARE @SQLChg varchar(max) = ''
			 DECLARE @ChgSetting int

			 SELECT @ChgSetting = CAST(V035 as int) FROM Record
			 WHERE TableID = 2743
			 AND IsActive = 1

			 SET @ChgSetting = @ChgSetting - 1

			 SET @SQLChg = '
			 SELECT SUM(CAST(Chg as money)) /Count(RecordID)
				 FROM  
				(SELECT top '+ CAST(@ChgSetting as varchar) +' V023 as Chg ,  RecordID 
				    FROM RECORD [Chargeability History]  	   
				    WHERE [Chargeability History].TABLEID = 2651
				    AND [Chargeability History].ISACTIVE = 1	  
				    AND [Chargeability History].V024 = '+ CAST(@EmployeeRecordID as varchar) + '
				     ORDER BY RecordID desc
				    union all
				    SELECT '''+ CAST(@ChgRate as varchar) +''' as Chg,1 as RecordID
				   
			
				    ) [Chargeability History]
			 ' 
			 print @SQLChg
			 DECLARE @Chg Table (Chg money)
			 INSERT INTO @Chg Exec(@SQLChg);
			 

			 IF @AvHrPerWk>0 AND @TimeWorked > 0
			 BEGIN
				INSERT INTO @ChargeabilityFinal
				SELECT @EmployeeRecordID,@GroupDivision GroupDivision ,
				@EmployeeName EmployeeName,
				@AvHrPerWk AvHrPerWk,
				@HrsSchedLeave HrsSchedLeave ,/* Need From Administration Code */
				@HrsOtherLeave HrsOtherLeave ,/* Need From Administration Code */
				@TimeWorked TimeWorked ,
				@TimeChargedToJobs TimeChargedToJobs ,
				@UtilPercent UtilPercent ,
				@Billed Billed ,
				@Adj Adj ,
				@BilledTotal BilledTotal ,
				@EfficiencyPercent EfficiencyPercent ,
				@ChgRate ChgRate,
				@TotalHourWorked TotalHourWorked,
				 CASE WHEN @Type = '$Hourly Rate' THEN 0 ELSE @TotalHourWorkable END TotalHourWorkable,
				 CASE WHEN @Type = '$Hourly Rate' THEN 0 ELSE @TotalWorkHoursPerWeek END TotalWorkHoursPerWeek,
				 CASE WHEN @Type = '$Hourly Rate' THEN 0 ELSE @TotalHourWorkablePerYear END TotalHourWorkablePerYear,
				@HourlyRate HourlyRate,
				@Paid Paid,
				@HoursPerDay_X_WokringDaysPerAWeek HoursPerDay_X_WokringDaysPerAWeek,

				/* lets script this to Insert into EXEC(), lets create a dummy table for the container then set it here after inserting */
				/* User below SELECT */
				 (SELECT Chg FROM @Chg)
			  END
			 /*
			  (SELECT (SUM(CAST(Chg as money)) + @ChgRate) /Count(RecordID)
				FROM  
				    (SELECT top 5 V023 as Chg ,  RecordID 
					   FROM RECORD [Chargeability History]  	   
					   WHERE [Chargeability History].TABLEID = 2651
					   AND [Chargeability History].ISACTIVE = 1	  
					   AND [Chargeability History].V024 = @EmployeeRecordID
					   ORDER BY RecordID desc
			
				) [Chargeability History])
	
				*/
		  DELETE FROM @Chg
	   /* ======================================================================================== */

	  FETCH NEXT FROM Employee INTO @EmployeeRecordID, @Type,@GroupDivision,@EmployeeName,@RenumerationPerYear
    END
 
    CLOSE Employee
    DEALLOCATE Employee

    /* ==================================================== Loop the Employee for the values ================================================================== */
    /*DECLARE @Counter int = 1
    WHILE EXISTS(SELECT * FROM @EmployeeList WHERE [Counter] >= @Counter)
    BEGIN

	   /* Summary */
	   DECLARE @GroupDivision varchar(max)
	   DECLARE @EmployeeName varchar(max)
	   DECLARE @AvHrPerWk money =0.00
	   DECLARE @HrsSchedLeave money = 0.00 /* Need From Administration Code */
	   DECLARE @HrsOtherLeave money = 0.00 /* Need From Administration Code */
	   DECLARE @TimeWorked money =0.00
	   DECLARE @TimeChargedToJobs money =0.00
	   DECLARE @UtilPercent float = 0.00
	   DECLARE @Billed money = 0.00
	   DECLARE @Adj money = 0.00
	   DECLARE @BilledTotal money = 0.00
	   DECLARE @EfficiencyPercent float =0.00
	   DECLARE @ChgRate float = 0.00
    
	   /* Details */
	   DECLARE @TotalHourWorked money = 0.00
	   DECLARE @TotalHourWorkable money = 0.00
	   DECLARE @TotalWorkHoursPerWeek money = 0.00
	   DECLARE @TotalHourWorkablePerYear money = 0.00
	   DECLARE @HourlyRate numeric(18,5)
	   DECLARE @Paid money = 0.00
	   DECLARE @HoursPerDay_X_WokringDaysPerAWeek money = 0.00
	   DECLARE @RenumerationPerYear money = 0.00
			 DECLARE @EmployeeRecordID int
			 DECLARE @EmployeePackageType varchar(50)

			 SELECT	@EmployeeRecordID = EmployeeRecordID, 
				    @EmployeePackageType = [Type], 
				    @GroupDivision = [Group], 
				    @EmployeeName = [EmployeeName],
				    @RenumerationPerYear = [RenumerationPerYear] 
				FROM @EmployeeList 
				WHERE [Counter] = @Counter

			 --JOIN Record [Group] ON [Group].TableID = 2727
				--AND [Group].IsActive = 1
				--AND [Group].RecordID = [Employee].V022

		

			 SELECT @TotalHourWorked = SUM(CAST(ISNULL(V005,0) as numeric(18,2))),@TimeWorked = SUM(CAST(ISNULL(V007,0) as numeric(18,2))) FROM Record [Timesheet] 
				WHERE [Timesheet].TableID = 2729
				AND [Timesheet].IsActive = 1
				AND [Timesheet].V002 = CAST(@EmployeeRecordID as varchar)
				AND [Timesheet].V011 IS NOT NULL /* Only Timesheet not Disbursement */
				AND CONVERT(date, [Timesheet].V009) >= CAST(@DateFrom as varchar)
				AND CONVERT(date, [Timesheet].V009) <= CAST(@DateTo as varchar)

			 /* Add Filter to not include Admin codes */
				--SELECT  @TimeChargedToJobs = SUM(CAST(ISNULL(V007,0) as numeric(18,2))) FROM Record [Timesheet] 
				--WHERE [Timesheet].TableID = 2729
				--AND [Timesheet].IsActive = 1
				--AND [Timesheet].V002 = CAST(@EmployeeRecordID as varchar)
				--AND [Timesheet].V011 IS NOT NULL /* Only Timesheet not Disbursement */
				--AND CONVERT(date, [Timesheet].V009) >= CAST(@DateFrom as varchar)
				--AND CONVERT(date, [Timesheet].V009) <= CAST(@DateTo as varchar)

				    /* Only Invoice, Converted to Job Currency */
				--SELECT  @Billed = SUM(CAST(ISNULL(CAST([Timesheet].V020 as money),0) /   CAST(ISNULL([Job Information].V007,1) as money)  as numeric(18,2))) 
				--FROM Record [Timesheet] 
				--JOIN Record [Job Information] ON [Job Information].TableID = 2475
				--    AND [Job Information].IsActive = 1
				--    AND [Job Information].RecordID = [Timesheet].V001

				---- JOIN Record [Job Currency] ON [Job Currency].TableID = 2475
				--    --AND [Job Currency].IsActive = 1
				--    --AND [Job Currency].RecordID = [Job Information].V034
				
				--    WHERE [Timesheet].TableID = 2729
				--    AND [Timesheet].IsActive = 1
				--    AND [Timesheet].V002 = CAST(@EmployeeRecordID as varchar)
				--    AND [Timesheet].V011 IS NOT NULL /* Only Timesheet not Disbursement */
				--    AND CONVERT(date, [Timesheet].V009) >= CAST(@DateFrom as varchar)
				--    AND CONVERT(date, [Timesheet].V009) <= CAST(@DateTo as varchar)
				--    AND [Timesheet].V016 IS NOT NULL

			 /* Get BAPA Credit Entry related to an employee  */
			 --SELECT @Adj = CAST([Credit Details].V003 as numeric(18,2))
				--FROM Record [Credit Details] 
				--JOIN Record [Billing Information] ON [Billing Information].TableID = 2711
				--    AND [Billing Information].IsActive = 1
				--    AND [Billing Information].RecordID = [Credit Details].V001
				--    AND CONVERT(date, [Billing Information].V004) >= CAST(@DateFrom as varchar)
				--    AND CONVERT(date, [Billing Information].V004) <= CAST(@DateTo as varchar)
				--WHERE [Credit Details].TableID = 2765
				--    AND [Credit Details].IsActive = 1
				--    AND [Credit Details].V002 = CAST(@EmployeeRecordID as varchar)
	    
			 /* Get the Billed Total - Billed less the Adj (Credit agains the employee) */
			 SET @BilledTotal = @Billed + @Adj
			 SET @EfficiencyPercent = CAST(@BilledTotal / CASE WHEN @TimeChargedToJobs = 0 THEN 1 ELSE ISNULL(@TimeChargedToJobs,1) END as numeric(18,2))
			 SET @ChgRate = CAST(@BilledTotal / CASE WHEN @TimeChargedToJobs = 0 THEN 1 ELSE ISNULL(@TimeChargedToJobs,1) END as numeric(18,2))
	   
		  IF (@EmployeePackageType = '$Annual Package')
		  BEGIN
			 SELECT @TotalHourWorkable =
				(datediff(day, -7, @DateTo)/7-datediff(day, -6, @DateFrom)/7) * CAST(ISNULL(V012,0) AS MONEY) +
				(datediff(day, -6, @DateTo)/7-datediff(day, -5, @DateFrom)/7) * CAST(ISNULL(V013,0) AS MONEY) +
				(datediff(day, -5, @DateTo)/7-datediff(day, -4, @DateFrom)/7) * CAST(ISNULL(V014,0) AS MONEY) +
				(datediff(day, -4, @DateTo)/7-datediff(day, -3, @DateFrom)/7) * CAST(ISNULL(V015,0) AS MONEY) +
				(datediff(day, -3, @DateTo)/7-datediff(day, -2, @DateFrom)/7) * CAST(ISNULL(V016,0) AS MONEY) +
				(datediff(day, -2, @DateTo)/7-datediff(day, -2, @DateFrom)/7) * CAST(ISNULL(V017,0) AS MONEY) +
				(datediff(day, -1, @DateTo)/7-datediff(day, -2, @DateFrom)/7) * CAST(ISNULL(V018,0) AS MONEY)  
				,@TotalWorkHoursPerWeek = V007
				,@HoursPerDay_X_WokringDaysPerAWeek = 
				CASE WHEN V012 IS NOT NULL AND CAST(V012 as money) > 1 THEN CAST(V012 as numeric(18,2)) ELSE 0 END + 
				    CASE WHEN V013 IS NOT NULL  AND CAST(V013 as money) > 1 THEN CAST(V013 as numeric(18,2)) ELSE 0 END + 
				    CASE WHEN V014 IS NOT NULL  AND CAST(V014 as money) > 1 THEN CAST(V014 as numeric(18,2)) ELSE 0 END + 
				    CASE WHEN V015 IS NOT NULL  AND CAST(V015 as money) > 1 THEN CAST(V015 as numeric(18,2)) ELSE 0 END + 
				    CASE WHEN V016 IS NOT NULL  AND CAST(V016 as money) > 1 THEN CAST(V016 as numeric(18,2)) ELSE 0 END + 
					   CASE WHEN V017 IS NOT NULL  AND CAST(V017 as money) > 1 THEN CAST(V017 as numeric(18,2)) ELSE 0 END + 
					   CASE WHEN V018 IS NOT NULL  AND CAST(V018 as money) > 1 THEN CAST(V018 as numeric(18,2)) ELSE 0 END 
 
				FROM Record
				WHERE RecordID = @EmployeeRecordID
		  END
		  ELSE
		  BEGIN
		  /* We are going to get and use the last record with Package Type */
			 SELECT @TotalHourWorkable =
				(datediff(day, -7, @DateTo)/7-datediff(day, -6, @DateFrom)/7) * CAST(ISNULL(V014,0) AS MONEY) +
				(datediff(day, -6, @DateTo)/7-datediff(day, -5, @DateFrom)/7) * CAST(ISNULL(V015,0) AS MONEY) +
				(datediff(day, -5, @DateTo)/7-datediff(day, -4, @DateFrom)/7) * CAST(ISNULL(V016,0) AS MONEY) +
				(datediff(day, -4, @DateTo)/7-datediff(day, -3, @DateFrom)/7) * CAST(ISNULL(V017,0) AS MONEY) +
				(datediff(day, -3, @DateTo)/7-datediff(day, -2, @DateFrom)/7) * CAST(ISNULL(V018,0) AS MONEY) +
				(datediff(day, -2, @DateTo)/7-datediff(day, -2, @DateFrom)/7) * CAST(ISNULL(V019,0) AS MONEY) +
				(datediff(day, -1, @DateTo)/7-datediff(day, -2, @DateFrom)/7) * CAST(ISNULL(V020,0) AS MONEY) 
				,@TotalWorkHoursPerWeek = V008
				,@HoursPerDay_X_WokringDaysPerAWeek = 
				CASE WHEN V014 IS NOT NULL AND CAST(V014 as money) > 1 THEN CAST(V014 as numeric(18,2)) ELSE 0 END + 
				    CASE WHEN V015 IS NOT NULL  AND CAST(V015 as money) > 1 THEN CAST(V015 as numeric(18,2)) ELSE 0 END + 
				    CASE WHEN V016 IS NOT NULL  AND CAST(V016 as money) > 1 THEN CAST(V016 as numeric(18,2)) ELSE 0 END + 
				    CASE WHEN V017 IS NOT NULL  AND CAST(V017 as money) > 1 THEN CAST(V017 as numeric(18,2)) ELSE 0 END + 
				    CASE WHEN V018 IS NOT NULL  AND CAST(V018 as money) > 1 THEN CAST(V018 as numeric(18,2)) ELSE 0 END + 
					   CASE WHEN V019 IS NOT NULL  AND CAST(V019 as money) > 1 THEN CAST(V019 as numeric(18,2)) ELSE 0 END + 
					   CASE WHEN V020 IS NOT NULL  AND CAST(V020 as money) > 1 THEN CAST(V020 as numeric(18,2)) ELSE 0 END 
 
				FROM Record
				WHERE TableID = 2670 AND 
				V021 = @EmployeeRecordID /* Employee History Name */
				AND RecordID = (SELECT top 1 RecordID FROM Record WHERE V021 =@EmployeeRecordID AND TableID = 2670 AND V012 = 'True' AND IsActive = 1 ORDER BY CAST(V003 AS DATE) DESC)

		  END

		  --SELECT *
		  -- FROM Record
		  -- WHERE RecordID = @EmployeeRecordID

			 --SELECT *
			 --FROM Record
			 --WHERE TableID = 2670 AND 
			 --V021 = '12911831' /* Employee History Name */
			 --AND RecordID = (SELECT top 1 RecordID FROM Record WHERE V021 ='12911831' AND TableID = 2670 AND V012 = 'True' AND IsActive = 1 ORDER BY CAST(V003 AS DATE) DESC)

			 SET @AvHrPerWk = Round(@TotalHourWorked / CASE WHEN @TotalHourWorkable = 0 THEN 1 ELSE ISNULL(@TotalHourWorkable,1) END * @TotalWorkHoursPerWeek,2)
			 SET @UtilPercent =    @TimeChargedToJobs / CASE WHEN @TimeWorked = 0 THEN 1 ELSE ISNULL(@TimeWorked,1) END
			 SET @TotalHourWorkablePerYear = CAST(@HoursPerDay_X_WokringDaysPerAWeek * 48 as numeric(18,5))
			 SET @HourlyRate = CAST(CAST(@RenumerationPerYear as numeric(18,5)) / CASE WHEN @TotalHourWorkablePerYear = 0 THEN 1 ELSE CAST(ISNULL(@TotalHourWorkablePerYear,1) as numeric(18,5)) END as numeric(18,5))
			 SET @Paid = CAST((@TotalHourWorkable - @HrsSchedLeave ) * @HourlyRate as numeric(18,2))
			 --SELECT CAST(CAST(75000 AS NUMERIC(18,5)) / CAST(1800 AS NUMERIC(18,5)) AS NUMERIC(18,5))


			 INSERT INTO @ChargeabilityFinal
			 SELECT @GroupDivision GroupDivision ,
			 @EmployeeName EmployeeName,
			 @AvHrPerWk AvHrPerWk,
			 @HrsSchedLeave HrsSchedLeave ,/* Need From Administration Code */
			 @HrsOtherLeave HrsOtherLeave ,/* Need From Administration Code */
			 @TimeWorked TimeWorked ,
			 @TimeChargedToJobs TimeChargedToJobs ,
			 @UtilPercent UtilPercent ,
			 @Billed Billed ,
			 @Adj Adj ,
			 @BilledTotal BilledTotal ,
			 @EfficiencyPercent EfficiencyPercent ,
			 @ChgRate ChgRate,
			 @TotalHourWorked TotalHourWorked,
			 @TotalHourWorkable TotalHourWorkable,
			 @TotalWorkHoursPerWeek TotalWorkHoursPerWeek,
			 @TotalHourWorkablePerYear TotalHourWorkablePerYear,
			 @HourlyRate HourlyRate,
			 @Paid Paid,
			 @HoursPerDay_X_WokringDaysPerAWeek HoursPerDay_X_WokringDaysPerAWeek
	   SET @Counter= @Counter + 1
    END */
    /* ==================================================== End Loop ================================================================== */




     /* Lets save the data to Chargeability History */
	   DECLARE @IsInserted varchar(20) = 'false'
	   IF @SaveChargeability = 1
	   BEGIN
	   SET @IsInserted = 'true'
		  INSERT INTO Record (
				 TableID
				,[DateTimeRecorded]
				,[EnteredBy]
				,[IsActive]
				,V024
				,V003
				,V005
				,V006
				,V013
				,V014
				,V015
				,V016
				,V017
				,V018
				,V019
				,V020
				,V021
				,V022
				,V023
				,V025

				)
		  SELECT	2651,			 /* Designation Type */
				GETDATE(),
				25162,			 /* Buffalo Biller Acount */
				1,
				EmployeeRecordID, 
				CONVERT(VARCHAR(20),GETDATE(),103), 
				CONVERT(VARCHAR(20),@DateFrom,103),
				CONVERT(VARCHAR(20),@DateTo,103),
				AvHrPerWk,
				HrsSchedLeave ,/* Need From Administration Code */
				HrsOtherLeave ,/* Need From Administration Code */
				TimeWorked ,
				TimeChargedToJobs ,
				UtilPercent ,
				Billed ,
				Adj ,
				BilledTotal ,
				EfficiencyPercent ,
				ChgRate,
				Avg_X 
		  FROM @ChargeabilityFinal
	   END


	SELECT  GroupDivision ,
		   EmployeeName,
		   AvHrPerWk,
		   ISNULL(HrsSchedLeave,0) HrsSchedLeave ,/* Need From Administration Code */
		   ISNULL(HrsOtherLeave,0) HrsOtherLeave ,/* Need From Administration Code */
		   ISNULL(TimeWorked,0) TimeWorked ,
		   ISNULL(TimeChargedToJobs,0) TimeChargedToJobs ,
		   ISNULL(UtilPercent,0) UtilPercent ,
		   ISNULL(Billed,0) Billed ,
		   ISNULL(Adj,0) Adj ,
		   ISNULL(BilledTotal,0) BilledTotal ,
		   ISNULL(EfficiencyPercent,0) EfficiencyPercent ,
		   CASE WHEN ChgRate = 0 THEN NULL ELSE ChgRate END ChgRate,
		   TotalHourWorked,
		   TotalHourWorkable,
		   TotalWorkHoursPerWeek,
		   TotalHourWorkablePerYear,
		   HourlyRate,
		   Paid,
		   HoursPerDay_X_WokringDaysPerAWeek,
		   (SELECT V039  FROM RECORD WHERE TABLEID = 2743) AS [Header] ,
		   Avg_X,
		   @IsInserted IsInserted
	   FROM @ChargeabilityFinal
	  -- WHERE TimeWorked IS NOT NULL
	   ORDER BY GroupDivision, EmployeeName


	  

	  


END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('usr_rptChargeAbility', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH






















/*
== Old Script
SELECT ChargeabilityHistory.StartDate AS [Start Date], 
ChargeabilityHistory.EndDate AS [End Date], 
ChargeabilityHistory.AvgHrsPerWeek, 
ChargeabilityHistory.HrsScheduledLeave, 
ChargeabilityHistory.HrsOtherLeave,
ChargeabilityHistory.TimeSheet AS [$TimeSheet], 
ChargeabilityHistory.ChargedToJobs AS [$Jobs], 
ChargeabilityHistory.ChargedToJobsPercent,
ChargeabilityHistory.Billed AS [$Billed], 
ChargeabilityHistory.Adjustments AS [$Adjustments],
ChargeabilityHistory.TotalBilled AS [$Billed Total], 
ChargeabilityHistory.JobProdPercent, 
ChargeabilityHistory.Chargeability AS Chg,
ChargeabilityHistory.EmployeeID,
ChargeabilityHistory.ExportedTime, 
ChargeabilityHistory.ExportedDate
FROM   Account24918.vChargeabilityHistory ChargeabilityHistory;

	 */