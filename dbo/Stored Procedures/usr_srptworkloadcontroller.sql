﻿CREATE proc [dbo].[usr_srptworkloadcontroller]
(
@Group varchar(max) =null,
@Controller varchar(max) =null,

@From date=null,
@To date =null,
@Group_Controller int =1

)
as
exec usr_rpt_CurrentJobListGroupCurrency
SELECT Sum(rptCurrentJobsList.Approved) AS JobApproved,
 Sum(rptCurrentJobsList.Invoiced) AS JobInvoiced, 
 Sum(rptCurrentJobsList.Balanced) AS JobBalanced,
  Count(*) AS Jobs, rptCurrentJobsList.name,
  rptCurrentJobsList.[group/division]
FROM  ##CurrentJobListGroupCurrency rptCurrentJobsList
where (((rptCurrentJobsList.[group/division]  =@group) or ( @group in ('ALL','NULL'))) and ((rptCurrentJobsList.Name = @controller )or (@controller='NULL') ))


GROUP BY rptCurrentJobsList.name, rptCurrentJobsList.[group/division]
ORDER BY rptCurrentJobsList.name
