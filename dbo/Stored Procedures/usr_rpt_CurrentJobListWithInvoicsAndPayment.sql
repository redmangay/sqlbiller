﻿
-- Author:		<Manoj sharma>
-- Create date: <12 March 2016>
-- Description:	<Used as ssrs report and in procedure rptcurrentjoblist>
create procedure [dbo].[usr_rpt_CurrentJobListWithInvoicsAndPayment]

AS
BEGIN
    
IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##CurrentJobListWithInvoicsAndPayment' )) DROP TABLE ##CurrentJobListWithInvoicsAndPayment
	
create table ##CurrentJobListWithInvoicsAndPayment  (
[Project Number] varchar(max),
Invoiced float,
Payment float

 ) 



 insert into ##CurrentJobListWithInvoicsAndPayment

SELECT BillingInformation.[Project Number],

Sum( CAST(REPLACE(REPLACE(isnull(BillingInformation.[Invoice Amount Converted],0),',',''),'$','') AS float)) AS Invoiced,
Sum( CAST(REPLACE(REPLACE(isnull(BillingInformation.[Payment Amount Converted],0),',',''),'$','') AS float)) AS Payment

  
  
FROM  [Account24918].[vBilling Information] BillingInformation
GROUP BY BillingInformation.[Project Number];


   
  
   RETURN;
END;
