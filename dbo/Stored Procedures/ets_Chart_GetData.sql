﻿

CREATE PROCEDURE [dbo].[ets_Chart_GetData] 
	-- Add the parameters for the stored procedure here
	@nTableID int,
	@sSeries varchar(MAX),
	@sValueFields varchar(MAX),
	@dStartDate datetime,
	@dEndDate datetime
AS

BEGIN TRY
	SET NOCOUNT ON;
	
    DECLARE @Sql varchar(MAX)        
        
	SET @dEndDate = DATEADD(Day, 1, @dEndDate)
	
	IF(CHARINDEX(',', @sSeries) > 0)
		BEGIN
			SET @sql =	' SELECT LocationID, DateTimeRecorded,' + @sValueFields +
				' FROM Record' +
				' WHERE IsActive=1 AND DateTimeRecorded BETWEEN ''' + CAST(@dStartDate AS Varchar) + ''' AND ''' +  CAST(@dEndDate AS Varchar) + '''' +
				' AND LocationID IN (' + @sSeries + ')' + 
				' AND TableID = ' + CAST(@nTableID AS Varchar) + 
 				' ORDER BY LocationID, DateTimeRecorded' 
		END	
	ELSE
		BEGIN
			SET @sql =	' SELECT LocationID,DateTimeRecorded,' + @sValueFields +
				' FROM Record' +
				' WHERE IsActive=1 AND DateTimeRecorded BETWEEN ''' + CAST(@dStartDate AS Varchar) + ''' AND ''' +  CAST(@dEndDate AS Varchar) + '''' +
				' AND LocationID = ' + @sSeries + 
				' AND TableID = ' + CAST(@nTableID AS Varchar) + 
				' ORDER BY DateTimeRecorded' 			
		END	
	EXEC(@sql)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Chart_GetData', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
