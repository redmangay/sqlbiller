﻿

CREATE PROCEDURE [dbo].[Menu_Role_Insert]
(
	@nNewID int output,
	@nMenuID int=NULL,
	@nRoleID int=NULL

)
	/*----------------------------------------------------------------
	-- Stored Procedure: Menu_Role_Insert
	-- Produced by: Red Mangay
	-- Date: Jun  24 2019  6:00PM
	---------------------------------------------------------------*/
 
AS

BEGIN TRY

	INSERT INTO MenuRole
	(
		MenuID,
		RoleID 
	) VALUES (
		@nMenuID,
		@nRoleID
		
	)
	SELECT @nNewID = @@IDENTITY
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Menu_Role_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
