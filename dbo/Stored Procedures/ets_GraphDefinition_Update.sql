﻿

CREATE PROCEDURE [dbo].[ets_GraphDefinition_Update]
(
	@nGraphDefinitionID int ,
	@nAccountID int = NULL,
	@sDefinitionName varchar(200) = NULL ,
	@sDefinition varchar(MAX) = NULL,
	@bIsSystem bit = NULL, 
	@bIsHidden bit = NULL, 
	@nTableID int = NULL,
	@nColumnID int = NULL ,	
	@sDefinitionKey varchar(200) = NULL,
	@nDataColumn1ID int = NULL,
	@nDataColumn2ID int = NULL,
	@nDataColumn3ID int = NULL,
	@nDataColumn4ID int = NULL,
	@bIsSingleSeries bit = 0
)
	/*----------------------------------------------------------------
	-- Stored Procedure: ets_GraphDefinition_Update
	---------------------------------------------------------------*/
 
AS
BEGIN TRY
	DECLARE @sql varchar(MAX) = ''
	IF @nAccountID IS NOT NULL
		SET @sql = @sql + 'AccountID = ' + CAST(@nAccountID AS varchar) + ', '
	IF @sDefinitionName IS NOT NULL
		SET @sql = @sql + 'DefinitionName = ''' + @sDefinitionName + ''', '
	IF @sDefinition IS NOT NULL
		SET @sql = @sql + 'Definition = ''' + REPLACE(@sDefinition, '''', '''''') + ''','
	IF @bIsSystem IS NOT NULL
		SET @sql = @sql + 'IsSystem = ' + CAST(@bIsSystem AS varchar) + ', '
	IF @bIsHidden IS NOT NULL
		SET @sql = @sql + 'IsHidden = ' + CAST(@bIsHidden AS varchar) + ', '
	IF @nTableID IS NOT NULL
		SET @sql = @sql + 'TableID = ' + CAST(@nTableID AS varchar) + ', '
	ELSE
		SET @sql = @sql + 'TableID = NULL, '
	IF @nColumnID IS NOT NULL
		SET @sql = @sql + 'ColumnID = ' + CAST(@nColumnID AS varchar) + ', '
	ELSE
		SET @sql = @sql + 'ColumnID = NULL, '
	IF @sDefinitionKey IS NOT NULL
		SET @sql = @sql + 'DefinitionKey = ''' + @sDefinitionKey + ''', '
	ELSE
		SET @sql = @sql + 'DefinitionKey = NULL, '
	IF @nDataColumn1ID IS NOT NULL
		SET @sql = @sql + 'DataColumn1ID = ' + CAST(@nDataColumn1ID AS varchar) + ', '
	ELSE
		SET @sql = @sql + 'DataColumn1ID = NULL, '
	IF @nDataColumn2ID IS NOT NULL
		SET @sql = @sql + 'DataColumn2ID = ' + CAST(@nDataColumn2ID AS varchar) + ', '
	ELSE
		SET @sql = @sql + 'DataColumn2ID = NULL, '
	IF @nDataColumn3ID IS NOT NULL
		SET @sql = @sql + 'DataColumn3ID = ' + CAST(@nDataColumn3ID AS varchar) + ', '
	ELSE
		SET @sql = @sql + 'DataColumn3ID = NULL, '
	IF @nDataColumn4ID IS NOT NULL
		SET @sql = @sql + 'DataColumn4ID = ' + CAST(@nDataColumn4ID AS varchar) + ', '
	ELSE
		SET @sql = @sql + 'DataColumn4ID = NULL, '

	SET @sql = @sql + 'IsSingleSeries = ' + CAST(@bIsSingleSeries AS varchar) + ', '

	IF LEN(@sql) > 0
	BEGIN
		SET @sql = LEFT(@sql, LEN(@sql) - 1)
		SET @sql = 'UPDATE [GraphDefinition] SET ' + @sql + ' WHERE GraphDefinitionID = ' + CAST(@nGraphDefinitionID AS varchar)
		EXEC (@sql)
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_GraphDefinition_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

