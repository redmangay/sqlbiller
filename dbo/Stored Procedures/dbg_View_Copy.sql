﻿


-- When user edit a view that is not his then we create a view for that user
-- we will be using [dbo].[dbg_View_Copy]

CREATE PROCEDURE   [dbo].[dbg_View_Copy]
(
	@ViewID int,
	@UserID int=NULL,	
	@NewViewID int output

)

 
AS

BEGIN TRY
	INSERT INTO [View]
		   ([TableID]
		   ,[ViewName]
		   ,[UserID]
		   ,[RowsPerPage]
		   ,[SortOrder]
		   ,[Filter]
		   ,[ShowSearchFields]
		   ,[ShowAddIcon]
		   ,[ShowExportIcon],[ShowEmailIcon]
		   ,[ShowDeleteIcon]
		   ,[ShowViewIcon]
		   ,[ShowBulkUpdateIcon]
		   ,[FilterControlsInfo]
		   ,FixedFilter,ShowFixedHeader,ParentTableID,TableChildID,ViewPageType,ShowHeader)
	   SELECT [TableID]
		   ,[ViewName]
		   ,@UserID
		   ,[RowsPerPage]
		   ,[SortOrder]
		   ,[Filter]
		   ,[ShowSearchFields]
		   ,[ShowAddIcon]
		    ,[ShowExportIcon],[ShowEmailIcon]
		   ,[ShowDeleteIcon]
		   ,[ShowViewIcon]
		   ,[ShowBulkUpdateIcon]
		   ,[FilterControlsInfo]
		   ,FixedFilter,ShowFixedHeader,ParentTableID,TableChildID,ViewPageType,ShowHeader
		   FROM [View] 
		   WHERE ViewID=@ViewID
        SELECT @NewViewID = @@IDENTITY
		INSERT INTO [dbo].[ViewItem]
				   ([ViewID]
				   ,[ColumnID]
				   ,[SearchField]
				   ,[FilterField]
				   ,[Alignment]
				   ,[ShowTotal]
				   ,[SummaryCellBackColor]
				   ,[ColumnIndex])
		 SELECT @NewViewID
				  ,[ColumnID]
				   ,[SearchField]
				   ,[FilterField]
				   ,[Alignment]
				   ,[ShowTotal]
				   ,[SummaryCellBackColor]
				   ,[ColumnIndex] 
				   FROM [ViewItem] 
				   WHERE ViewID=@ViewID
					ORDER BY ColumnIndex	
		
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_View_Copy', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH	
