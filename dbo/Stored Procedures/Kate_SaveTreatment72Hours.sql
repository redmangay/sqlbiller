﻿CREATE PROCEDURE [dbo].[Kate_SaveTreatment72Hours]
(
	@RecordID int,
	@UserID int,
	@Return varchar(max) output
)
AS
-- SELECT * FROM [Column] WHERE TableID = 2411: V020	Trauma Record Number
-- exec Kate_Save2ED @RecordID=874425, @UserID=0, @Return=''
BEGIN
	-- Find out the record ID for the current record
	DECLARE @ParentRecordID int, @ED1RecordID int
	SELECT @ParentRecordID = cast(V020 as int) -- Trauma Record Number
		FROM [Record] 
		WHERE [TableID] = 2411 -- Treatment 72 hours
		AND RecordID = @RecordID
		
		print '@ParentRecordID' + CAST(@ParentRecordID AS varchar(50))

	-- Update the current record with the Arrival date and time at hospital from 1st ED Arri: V236
	UPDATE [Record] 
		SET V086 = ED1.V073  -- 2385.V073 ED1 : Time of first review from arrival
		FROM [Record]
		JOIN [Record] ED1 ON ED1.[TableID] = 2385 -- 1st ED Arrival: 2385
			AND CAST(ED1.V298 AS int) = @ParentRecordID  -- Trauma Record Number
		WHERE [Record].[TableID] = 2411 -- Treatment 72 hours
		AND [Record].RecordID = @RecordID


END
