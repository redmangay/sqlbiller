﻿ 
CREATE PROCEDURE [dbo].[SpecialNotificationRecipient_Insert]
(
	@nNewID int output,
	@nSpecialNotificationID int,
	@UserID int  = NULL,
	@nColumnID int  = NULL,
	@sRecipientOption varchar(20) = NULL
)
	/*----------------------------------------------------------------
	-- Stored Procedure: SpecialNotificationRecipient_Insert
	-- Author: Red Mangay 
	-- Date: Jul 16, 2019
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	INSERT INTO SpecialNotificationRecipient
	(
		SpecialNotificationID,
		UserID, 
		ColumnID,
		RecipientOption
	) VALUES (
	     @nSpecialNotificationID,
		@UserID,
		@nColumnID,
		@sRecipientOption
	)
	SELECT @nNewID = @@IDENTITY
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('SpecialNotificationRecipient_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

