﻿

CREATE PROCEDURE [dbo].[User_RoleType]
(
	@UserID int,
	@TableID int
)
AS
/*
	USAGE:
	EXEC User_RoleType @UserID=25664, @TableID = 2123

	HISTORY:
	2017-03-17 JB created SP

*/
BEGIN TRY
	IF EXISTS (SELECT TOP 1 * FROM [User] U
				JOIN [UserRole] UR ON UR.UserID = U.UserID 
				JOIN [Role] R ON R.RoleID = UR.RoleID 
				JOIN [RoleType] ON [RoleType].RoleType = R.RoleType  
				WHERE U.UserID  = @UserID )
	BEGIN
		SELECT U.UserID, T.TableID, T.AccountID, [RoleType].*
			FROM [User] U
			JOIN [UserRole] UR ON UR.UserID = U.UserID 
			JOIN [Role] R ON R.RoleID = UR.RoleID 
			JOIN [Table] T ON T.TableID = @TableID
			JOIN [RoleType] ON [RoleType].RoleType = R.RoleType  
			WHERE U.UserID  = @UserID 
	END
	ELSE
	BEGIN
		SELECT U.UserID, T.TableID, T.AccountID, [RoleType].*
			FROM [User] U
			JOIN [UserRole] UR ON UR.UserID = U.UserID 
			JOIN [Role] R ON R.RoleID = UR.RoleID 
			JOIN [RoleTable] RT ON RT.RoleID = R.RoleID 
			JOIN [Table] T ON T.TableID = RT.TableID
			JOIN [Account] A ON A.AccountID = ur.AccountID 
			JOIN [RoleType] ON [RoleType].RoleType = RT.RoleType 
			WHERE U.[UserID] = @UserID
			AND T.TableID = @TableID
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('User_RoleType', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
