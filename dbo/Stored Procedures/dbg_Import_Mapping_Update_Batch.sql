﻿

CREATE PROCEDURE [dbo].[dbg_Import_Mapping_Update_Batch]
	@TableID int, 
	@BatchID int,
	@Site varchar(MAX) = NULL
AS

BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ValidRecordsCount int = 0
	DECLARE @WarningsCount int = 0
	DECLARE @ErrorsCount int = 0
	DECLARE @sql nvarchar(MAX)


	DECLARE @SourceAccountColumnName AS varchar(50)
	SELECT @SourceAccountColumnName = [SystemName]
		FROM [Column]
		WHERE ([TableID] = @TableID) AND ([DisplayName] = 'Incoming Site')
	PRINT @SourceAccountColumnName

    SET @sql = 'SELECT @ValidRecordsCount = COUNT(*) FROM TempRecord WHERE RejectReason IS NULL AND BatchID=' + CAST(@BatchID AS varchar(20)) +
		CASE
			WHEN @Site IS NULL THEN ''
			ELSE ' AND [' + @SourceAccountColumnName + ']=''' + @Site + ''''
		END

	PRINT @sql
	EXEC sp_executesql
		@sql,
		N'@BatchID int, @Site varchar(MAX), @ValidRecordsCount INT OUTPUT',
		@BatchID = @BatchID,
		@Site = @Site,
		@ValidRecordsCount = @ValidRecordsCount OUTPUT

    SET @sql = 'SELECT @ErrorsCount = COUNT(*) FROM TempRecord WHERE RejectReason IS NOT NULL AND BatchID=' + CAST(@BatchID AS varchar(20)) +
		CASE
			WHEN @Site IS NULL THEN ''
			ELSE ' AND [' + @SourceAccountColumnName + ']=''' + @Site + ''''
		END

	PRINT @sql
	EXEC sp_executesql
		@sql,
		N'@BatchID int, @Site varchar(MAX), @ErrorsCount INT OUTPUT',
		@BatchID = @BatchID,
		@Site = @Site,
		@ErrorsCount = @ErrorsCount OUTPUT

	UPDATE Batch SET
		ValidRecordCount = @ValidRecordsCount,
		WarningRecordCount = @WarningsCount,
		InvalidRecordCount = @ErrorsCount
	WHERE BatchID = @BatchID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_Import_Mapping_Update_Batch', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
