﻿

CREATE PROCEDURE [dbo].[dbg_ColumnColour_Select]
(
	@nID int  = NULL,
	@sContext varchar(25) ='columnid'
	
)
	/*----------------------------------------------------------------

	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SELECT
	   [ColumnColourID]
	 , [Context]
	 , [ID]
	 , [ControllingColumnID]
	 , [Operator]
	 , Value
	 , [ColourText]
	 , [ColourCell]
	FROM  [ColumnColour]
	WHERE [ID] = @nID
		AND [Context] = @sContext
	ORDER BY
	   [ColumnColourID];
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_ColumnColour_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
