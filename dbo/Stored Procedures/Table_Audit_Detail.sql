﻿

CREATE PROCEDURE [dbo].[Table_Audit_Detail]
(
      @nTableID int,
      @UpdateDate datetime
)
AS
BEGIN TRY
--EXEC Table_Audit_Detail 1133,'2012-03-18 21:20:34.483'
 
      -- DECLARE @nTableID int
      -- SET @nTableID = 2152

       SELECT [Audit].DateAdded,
             [User].FirstName + ' ' + [User].LastName as [User],
             FieldName,
             OldValue,
             NewValue
             FROM [Audit]
             JOIN [User] ON [User].UserID = [Audit].UserID
        WHERE (TableName = 'Table' OR TableName = 'Column')
			--AND PrimaryKeyValue = @nTableID
			AND TableID = @nTableID
			AND FieldName != 'DateUpdated'
			AND FieldName != 'LastUpdatedUserID'
			AND [Audit].DateAdded = @UpdateDate
        ORDER BY [Audit].DateAdded

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog]([Module], [ErrorMessage], [ErrorTrack], [ErrorTime], [Path]) 
		VALUES ('Table_Audit_Detail', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
