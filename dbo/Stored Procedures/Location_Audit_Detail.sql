﻿ 

CREATE PROCEDURE [dbo].[Location_Audit_Detail]

(

      @nLocationID int,

      @UpdateDate datetime

)

AS

--EXEC Location_Audit_Detail 1133,'2012-03-18 21:20:34.483'
 

      -- DECLARE @nLocationID int

      -- SET @nLocationID = 2152

BEGIN TRY
      SELECT [Audit].DateAdded,
            [User].FirstName + ' ' + [User].LastName as [User],
             FieldName,
            OldValue,
            NewValue
            FROM [Audit]
            JOIN [User] ON [User].UserID = [Audit].UserID

            WHERE TableName='Location'
            AND FieldName != 'DateUpdated'
			AND FieldName != 'LastUpdatedUserID'
            AND PrimaryKeyValue = @nLocationID
            AND [Audit].DateAdded = @UpdateDate
            ORDER BY [Audit].DateAdded
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Location_Audit_Detail', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
