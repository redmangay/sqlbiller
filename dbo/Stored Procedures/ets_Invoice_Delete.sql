﻿
 
CREATE PROCEDURE [dbo].[ets_Invoice_Delete]
(
	@nInvoiceID int 
)
	/*----------------------------------------------------------------
	-- Stored Procedure: ets_Invoice_Delete
	-- Produced by: DB Gurus (SPGen) 
	-- Date: Dec  3 2010  6:39PM
	---------------------------------------------------------------*/
 
AS

BEGIN TRY
	DELETE [Invoice] 
	WHERE InvoiceID = @nInvoiceID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Invoice_Delete', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH


