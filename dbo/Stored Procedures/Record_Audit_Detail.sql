﻿

CREATE PROCEDURE [dbo].[Record_Audit_Detail]
(
      @RecordID int,
      @UpdateDate datetime
)

AS

--EXEC Record_Audit_Detail 391392,'2012-02-23 10:17:53.930'
-- EXEC Record_Audit_Detail 371767,'2013-06-28 11:31:42.723'
      -- DECLARE @RecordID int
      -- SET @RecordID = 2152 
BEGIN TRY
      SELECT [Audit].DateAdded,[Column].SystemName,
            [User].FirstName + ' ' + [User].LastName as [User],
           -- CASE WHEN [Column].DisplayTextDetail IS NULL THEN FieldName ELSE [Column].DisplayTextDetail END as ColumnName,
            --[Column].DisplayName  as ColumnName,
            CASE WHEN [Column].DisplayName IS NULL THEN FieldName ELSE [Column].DisplayName END as ColumnName,
            OldValue,
            NewValue
            FROM [Audit]
            JOIN [User] ON [User].UserID = [Audit].UserID
            JOIN  [Record] ON [Record].RecordID = @RecordID
            LEFT JOIN [Column] on [Column].TableID = [Record].TableID AND [Column].SystemName = FieldName
            WHERE TableName = 'Record'

            AND FieldName != 'DateUpdated'
			AND FieldName != 'LastUpdatedUserID'
			 AND FieldName != 'WarningResults'
			  AND FieldName != 'ChangeReason'
            AND PrimaryKeyValue = @RecordID
            AND [Audit].DateAdded = @UpdateDate
            ORDER BY [Audit].DateAdded

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Record_Audit_Detail', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

