﻿
 
CREATE PROCEDURE [dbo].[dbg_Condition_Select]
(
	@ColumnID int,	
	@CheckColumnID int= NULL,
	@ConditionType CHAR(1)= NULL,
	@CheckValue varchar(MAX)=NULL
)
	/*----------------------------------------------------------------
	EXEC [dbo].[dbg_Condition_Select] 45694
	---------------------------------------------------------------*/
 
AS
 
BEGIN TRY

	
		SELECT Con.*,C.DisplayName FROM [Condition] Con INNER JOIN [Column] C
			ON Con.CheckColumnID=C.ColumnID WHERE 
			Con.ColumnID = @ColumnID
			AND
			(( @CheckColumnID IS NOT NULL AND @CheckColumnID=@CheckColumnID) OR ( @CheckColumnID IS  NULL AND CheckColumnID IS NOT NULL))
			AND
			(( @ConditionType IS NOT NULL AND ConditionType=@ConditionType) OR ( @ConditionType IS  NULL AND ConditionType IS NOT NULL))
			AND
			(( @CheckValue IS NOT NULL AND CheckValue=@CheckValue) OR ( @CheckValue IS  NULL))
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_Condition_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
