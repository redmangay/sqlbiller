﻿
CREATE PROCEDURE [dbo].[Renzo_GetBadDebtBilling]
(
	@RecordID  int = null,
	@FormSetID int = null,
     @UserID int=null,
     @Return varchar(max) =null output
)
AS
BEGIN

/*

EXEC [Renzo_GetBadDebtBilling]   5365234

*/
	
DECLARE @Data_Output TABLE
(
TableID varchar(max),
V001 VARCHAR(MAX),
V002 VARCHAR(MAX),
V003 VARCHAR(MAX),
V004 VARCHAR(MAX),
V005 VARCHAR(MAX),
V006 VARCHAR(MAX),
V007 VARCHAR(MAX),
V008 VARCHAR(MAX),
V009 VARCHAR(MAX),
V010 VARCHAR(MAX),
V011 VARCHAR(MAX),
V012 VARCHAR(MAX),
V013 VARCHAR(MAX),
V014 VARCHAR(MAX),
V015 VARCHAR(MAX),
V016 VARCHAR(MAX),
V017 VARCHAR(MAX),
V018 VARCHAR(MAX),
V019 VARCHAR(MAX),
V020 VARCHAR(MAX),
V021 VARCHAR(MAX),
V022 VARCHAR(MAX),
V023 VARCHAR(MAX),
V024 VARCHAR(MAX),
V025 VARCHAR(MAX),
V026 VARCHAR(MAX),
V027 VARCHAR(MAX),
V028 VARCHAR(MAX),
V029 VARCHAR(MAX),
V030 VARCHAR(MAX),
V031 VARCHAR(MAX),
V032 VARCHAR(MAX),
V033 VARCHAR(MAX),
V034 VARCHAR(MAX),
V035 VARCHAR(MAX),
V036 VARCHAR(MAX),
V037 VARCHAR(MAX),
V038 VARCHAR(MAX),
V039 VARCHAR(MAX),
V040 VARCHAR(MAX),
V041 VARCHAR(MAX),
V042 VARCHAR(MAX),
V043 VARCHAR(MAX),
V044 VARCHAR(MAX),
V045 VARCHAR(MAX),
V046 VARCHAR(MAX),
V047 VARCHAR(MAX),
V048 VARCHAR(MAX),
V049 VARCHAR(MAX),
V050 VARCHAR(MAX),
V051 VARCHAR(MAX),
V052 VARCHAR(MAX),
V053 VARCHAR(MAX),
V054 VARCHAR(MAX),
V055 VARCHAR(MAX),
V056 VARCHAR(MAX),
V057 VARCHAR(MAX),
V058 VARCHAR(MAX),
V059 VARCHAR(MAX),
V060 VARCHAR(MAX),
V061 VARCHAR(MAX),
V062 VARCHAR(MAX),
V063 VARCHAR(MAX),
V064 VARCHAR(MAX),
V065 VARCHAR(MAX),
V066 VARCHAR(MAX),
V067 VARCHAR(MAX),
V068 VARCHAR(MAX),
V069 VARCHAR(MAX),
V070 VARCHAR(MAX),
V071 VARCHAR(MAX),
V072 VARCHAR(MAX),
V073 VARCHAR(MAX),
V074 VARCHAR(MAX),
V075 VARCHAR(MAX),
V076 VARCHAR(MAX),
V077 VARCHAR(MAX),
V078 VARCHAR(MAX),
V079 VARCHAR(MAX),
V080 VARCHAR(MAX),
V081 VARCHAR(MAX),
V082 VARCHAR(MAX),
V083 VARCHAR(MAX),
V084 VARCHAR(MAX),
V085 VARCHAR(MAX),
V086 VARCHAR(MAX),
V087 VARCHAR(MAX),
V088 VARCHAR(MAX),
V089 VARCHAR(MAX),
V090 VARCHAR(MAX),
V091 VARCHAR(MAX),
V092 VARCHAR(MAX),
V093 VARCHAR(MAX),
V094 VARCHAR(MAX),
V095 VARCHAR(MAX),
V096 VARCHAR(MAX),
V097 VARCHAR(MAX),
V098 VARCHAR(MAX),
V099 VARCHAR(MAX),
V100 VARCHAR(MAX)


);
		
DECLARE @TaxRate money = 0.00
DECLARE @TaxAmount money = 0.00
DECLARE @TotalPayment money = 0.00
DECLARE @TotalInvoice money = 0.00
DECLARE @TotalInvoiceNoTax money = 0.00
DECLARE @JobValueBalance money = 0.00
DECLARE @TotalBadDebt money = 0.00
DECLARE @TotalTotalBilled money = 0.00


-- lets get the tax
SELECT @TaxRate = 1 + ([V011] / 100.00) 
    FROM [Record] 
	   WHERE RECORDID = @RecordID



-- let get the the Total Payment
SELECT @TotalPayment= ISNULL(SUM(CAST(V007 AS MONEY)),0.00) 
    FROM [RECORD] 
	   WHERE ISACTIVE = 1 AND 
		  TABLEID =2711 AND 
		  V029 <> 'Bad Debt Entry' AND 
		  V002 = CAST(@RecordID AS VARCHAR)

-- lets get the Total Invoice
SELECT @TotalInvoice = ISNULL(SUM(CAST(CONVERT(VARCHAR(20),V005,1) AS money)),0) 
    FROM Record 
	   WHERE ISACTIVE = 1 AND 
		  TableID = '2711' AND 
		  V029 LIKE '%Invoice%' AND 
		  V002 = CAST(@RecordID AS VARCHAR)

print @TotalInvoice

-- lets get the Job Value Balance
SET @JobValueBalance = Round(@TotalInvoice - @TotalPayment, 2)
SET @TotalInvoiceNoTax = cast(@JobValueBalance as money) / @TaxRate 

-- lets get the tax amount
SET @TaxAmount = (@JobValueBalance - (@JobValueBalance / @TaxRate)) * -1

    DECLARE @EmployeesBillingDetails TABLE (
	   [Counter] int identity(1,1),
	   EmployeeID int,
	   EmplyeeName varchar(max),
	   Billed varchar(max),
	   [BillingType] varchar(max)

    )

    INSERT INTO @EmployeesBillingDetails (Billed, EmployeeID, [BillingType])
	   SELECT SUM(CAST([RECORD].V020 AS MONEY)),V002, 'Timesheet' FROM [RECORD]
	   WHERE ISACTIVE=1 AND [RECORD].V001 = CAST(@RecordID AS VARCHAR)
		  AND [RECORD].TABLEID = 2729
		  AND [RECORD].ISACTIVE = 1
		  AND [RECORD].V011 IS NOT NULL
			 group by [RECORD].V002
			 order by [RECORD].V002

    INSERT INTO @EmployeesBillingDetails (Billed, EmployeeID, [BillingType])
	   SELECT SUM(CAST([RECORD].V020 AS MONEY)),V002, 'Disbursement' FROM [RECORD]
	   WHERE ISACTIVE=1 AND [RECORD].V001 = CAST(@RecordID AS VARCHAR)
		  AND [RECORD].TABLEID = 2729
		  AND [RECORD].ISACTIVE = 1
		  AND [RECORD].V010 IS NOT NULL
			 group by [RECORD].V002
			 order by [RECORD].V002

    
    SELECT @TotalTotalBilled = SUM(CAST(V020 AS MONEY)) FROM [RECORD] WHERE ISACTIVE=1 AND V011 IS NOT NULL  AND  V001 = CAST(@RecordID AS VARCHAR) AND TABLEID = 2729 AND V016 IS NOT NULL
    --SELECT 'Total Billed',@TotalTotalBilled
    --SELECT * FROM @EmployeesBillingDetails
    DECLARE @Counter INT = 1
    DECLARE @SumDisbursement money =0.00
    SELECT @SumDisbursement =  SUM(cast(Billed as DECIMAL(25,13))) FROM @EmployeesBillingDetails WHERE BillingType = 'Disbursement'
    --SELECT 'Sum Disbursement',@SumDisbursement
    WHILE EXISTS(SELECT * FROM @EmployeesBillingDetails WHERE [Counter] >= @Counter)
    BEGIN
	   DECLARE @EmployeeID int, @Billed money = 0.00, @EmployeeName varchar(max), @BillingType VARCHAR(MAX),@PercentageShare DECIMAL(25,13), @Balance money =0.00, @ShareAmount money =0.00
	   SELECT @Billed = Billed, @EmployeeID = EmployeeID, @BillingType = BillingType FROM @EmployeesBillingDetails WHERE [Counter] = @Counter
	 
	   IF (@BillingType = 'Timesheet')
	   BEGIN
	   SET @PercentageShare = cast(@Billed as DECIMAL(25,13)) / cast(@TotalTotalBilled as DECIMAL(25,13))	 
	   SET @ShareAmount = @PercentageShare * (@TotalInvoiceNoTax - @SumDisbursement)
	   --SELECT 'PERCENTAGE',@PercentageShare, (@TotalInvoiceNoTax - @SumDisbursement)
	   SELECT @EmployeeName = V003 FROM [Record] WHERE TABLEID = 2669 AND [Record].RecordID = @EmployeeID
	   END
	   ELSE
	   BEGIN
	    SET @ShareAmount = cast(@Billed as DECIMAL(25,13))
	   END
      
	   INSERT INTO @Data_Output (V001,V002)
	   SELECT CASE WHEN @BillingType = 'Timesheet' THEN @EmployeeName ELSE 'Disbursement' END, @ShareAmount * -1
	   SET @TotalBadDebt = @TotalBadDebt + (@ShareAmount * -1)
	   SET @Counter = @Counter + 1
    END

    SET @TotalBadDebt = @TotalBadDebt + @TaxAmount
	
    SELECT V001 AS [Description],convert(varchar,cast(V002 as money),1) AS Amount FROM @Data_Output
	   UNION ALL
		  SELECT NULL [Description], NULL Amount
	   UNION ALL
		  SELECT NULL [Description], NULL Amount
	   UNION ALL
		  SELECT 'Tax Amount' [Description], convert(varchar,cast(@TaxAmount as money),1) Amount
	   UNION ALL
		  SELECT 'Total' [Description], convert(varchar,cast(@TotalBadDebt as money),1) Amount
	

END

/**************************************************/
