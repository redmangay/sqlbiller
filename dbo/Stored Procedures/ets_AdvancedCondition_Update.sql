﻿
CREATE PROCEDURE [dbo].[ets_AdvancedCondition_Update]
(
	@nAdvancedConditionID int output,
	@sConditionType varchar(25),
	@sConditionSubType char(1) = NULL,
	@nColumnID int,
	@nConditionColumnID int,
	@sConditionColumnValue varchar(250) = NULL,
	@sConditionOperator varchar(25) = NULL,
	@nDisplayOrder int,
	@sJoinOperator varchar(10) = NULL,
	@Status varchar(1) = NULL
)
AS

BEGIN TRY
	UPDATE [AdvancedCondition]
	SET 
		ColumnID = @nColumnID,
		ConditionType = @sConditionType,
		ConditionSubType = @sConditionSubType,
		ConditionColumnID = @nConditionColumnID,
		ConditionColumnValue = @sConditionColumnValue,
		ConditionOperator = @sConditionOperator,
		DisplayOrder = @nDisplayOrder,
		JoinOperator = @sJoinOperator,
		[Status] = @Status
	WHERE AdvancedConditionID = @nAdvancedConditionID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_AdvancedCondition_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

