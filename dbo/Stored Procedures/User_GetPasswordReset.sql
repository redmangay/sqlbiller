﻿

CREATE PROCEDURE [dbo].[User_GetPasswordReset]
(
	@nPasswordResetID int 
)
AS
BEGIN TRY
	SET NOCOUNT ON;

	SELECT     *
	FROM         [UserPasswordReset]
	WHERE     (PasswordResetID = @nPasswordResetID AND [Expiration] > GETDATE()  AND IsActive=1)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('User_GetPasswordReset', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
