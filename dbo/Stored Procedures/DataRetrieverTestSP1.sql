﻿

CREATE PROCEDURE [dbo].[DataRetrieverTestSP1]
(
	@nTableID INT=NULL,
	@nRecordID int=NULL
	
)

/*
EXEC [DataRetrieverTestSP1] NULL,418528
EXEC [DataRetrieverTestSP1] 1328,-1

*/
AS

BEGIN TRY
		SET NOCOUNT ON;

		DECLARE @tDisplayColumns TABLE
		(
			ID int identity(1,1),
			ColumnID int,
			SystemName varchar(50),
			DisplayText varchar(50)
		)

	--DECLARE @nTableID INT
	IF @nTableID IS NULL and @nRecordID IS NOT NULL
	SELECT @nTableID=TableID FROM Record WHERE RecordID=@nRecordID

	INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
			SELECT ColumnID, DisplayName, SystemName
				FROM [Column] 
				WHERE TableID = @nTableID 
				AND DisplayName IS NOT NULL AND LEN(DisplayName) > 0
				AND SystemName<>'EnteredBy'
			ORDER BY DisplayOrder



		DECLARE @sSQL varchar(MAX)
		DECLARE @counter int
		SET @counter = 1
	

	
	  SELECT @sSQL = 'SELECT * FROM (SELECT RecordInfo.* FROM (SELECT '

	  SET @counter = 1

		WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID >= @counter)
		BEGIN
			SELECT @sSQL = @sSQL + 'Record.' + SystemName + ' AS [' + DisplayText + '], ' 
				FROM @tDisplayColumns 
				WHERE ID = @counter
			SET @counter = @counter + 1
		
		END 

		SELECT @sSQL = LEFT(@sSQL, LEN(@sSQL)-1) 
	
		SELECT @sSQL = @sSQL + ' FROM [Record]'

	

		-- ADD IN THE WHERE CRITERIA
		SELECT @sSQL = @sSQL + ' WHERE  Record.RecordID = ' + CAST(@nRecordID as varchar)
		SELECT @sSQL = @sSQL + '  '	

		SELECT @sSQL= @sSQL + ') as RecordInfo) as RecordFinalInfo '
    
    

		PRINT @sSQL
		EXEC (@sSQL)
		-- PRINT @sSQL
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('DataRetrieverTestSP1', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
