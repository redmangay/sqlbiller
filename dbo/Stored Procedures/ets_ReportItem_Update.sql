﻿
 
CREATE PROCEDURE [dbo].[ets_ReportItem_Update]
(
	@nReportItemID int output,
	@nObjectID int,
	@nItemType int,
	@sItemTitle varchar(500) = NULL,
	@bApplyFilter bit = 0,
	@bApplySort bit = 0,
	@bUseColors bit = 0,
	@bHighlightWarnings bit = 0,
	@nPeriodColumnID int = NULL,
	@nPosition int
)
AS

BEGIN TRY
	UPDATE [ReportItem]
	SET 
		ObjectID = @nObjectID,
		ItemType = @nItemType,
		ItemTitle = @sItemTitle,
		ApplyFilter = @bApplyFilter,
		ApplySort= @bApplySort,
		UseColors = @bUseColors,
		HighlightWarnings = @bHighlightWarnings,
		PeriodColumnID = @nPeriodColumnID,
		ItemPosition = @nPosition
	WHERE ReportItemID = @nReportItemID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_ReportItem_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
