﻿CREATE procedure [dbo].[usr_ChargeabilityHistory]
as
SELECT   dbo.mUtilEncrypt(c.AvgHrsPerWeek,c.EmployeeID) as AvgHrsPerWeek,
c.StartDate AS [Start Date], 
c.EndDate AS [End Date],
 
 --select Format(1222.4, '##,##0.00') 
 
  dbo.mUtilEncrypt(c.HrsScheduledLeave,c.EmployeeID) as HrsScheduledLeave,
  dbo.mUtilEncrypt(c.HrsOtherLeave,c.EmployeeID) as HrsOtherLeave, 
  dbo.mUtilEncrypt(c.TimeSheet,c.EmployeeID) as [$TimeSheet],
  dbo.mUtilEncrypt(c.ChargedToJobs,c.EmployeeID) AS [$Jobs],
  dbo.mUtilEncrypt(c.ChargedToJobsPercent,c.EmployeeID) AS ChargedToJobsPercent,
  dbo.mUtilEncrypt( c.Billed,c.EmployeeID) AS [$Billed],
  dbo.mUtilEncrypt(c.Adjustments,c.EmployeeID) AS [$Adj],
  dbo.mUtilEncrypt(c.TotalBilled,c.EmployeeID) AS [$Billed Total],
  dbo.mUtilEncrypt(c.JobProdPercent,c.EmployeeID) AS JobProdPercent,
  dbo.mUtilEncrypt(c.Chargeability,c.EmployeeID) AS chg,
  
  
     
     c.EmployeeID, e.Name, c.ExportedDate, c.ExportedTime
FROM Account24918.vChargeabilityHistory AS c LEFT JOIN   Account24918.vEmployee_List  AS e ON c.EmployeeID = e.EmployeeID
WHERE (((e.OldYesNo)='False'))

ORDER BY e.Name, convert(Datetime,c.StartDate,103), convert(datetime,c.ExportedDate,103), convert(time, c.ExportedTime);






