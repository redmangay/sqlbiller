﻿create PROCEDURE [dbo].[Kate_SaveDemographics]
(
	@RecordID int,
	@UserID int,
	@Return varchar(max) output
)
AS
-- exec Kate_SaveDemographics @RecordID=769658, @UserID=0, @Return=''
BEGIN
	
	DECLARE @ParentRecordID int, @TraumaRecordNumber varchar(MAX)
	-- Check that the parent id is ok
	IF EXISTS(SELECT 1 FROM [Record] WHERE RecordID = @RecordID AND ISNUMERIC([V002]) = 1)
	BEGIN
		SELECT @ParentRecordID = cast([V002] as int), @TraumaRecordNumber = V070 FROM [Record] WHERE RecordID = @RecordID
		UPDATE [Record] 
			SET V013 = @TraumaRecordNumber
			WHERE TableID = 2347
			AND RecordID = @ParentRecordID
	END	
END
