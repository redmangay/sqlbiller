﻿CREATE PROCEDURE [dbo].[usr_rptInvoicePayments]    
AS    
BEGIN    
SELECT    
b.[Date],    
b.[Project Number],    
p.[ProjectTitle] ,    
CASE WHEN (b.[Invoice Amount Converted]<0)THEN 0     
ELSE b.[Invoice Amount Converted] END AS Invoice,    
 CASE WHEN (b.[Payment Amount Converted]<0) THEN 0      
ELSE b.[Payment Amount Converted] END AS Payment,     
CASE WHEN (b.[Invoice Amount Converted]>0)THEN 0     
ELSE '-' + b.[Invoice Amount Converted]END AS Credit,    
b.[Bad Debt Exp Converted] AS BadDebt,     
CASE WHEN ISNUMERIC(p.[CurrencyRate])>0 THEN CAST(p.[CurrencyRate] AS money)     
ELSE NULL END AS cCurrencyRate,    
b.[Tax Amount], p.[Group/Division] AS GroupDivision,    
CASE WHEN (b.[Invoice Amount Converted]<0) THEN 0     
ELSE b.[Tax Amount] END AS InvoiceTax,     
CASE WHEN (b.[Invoice Amount Converted]>0) THEN 0     
ELSE b.[Tax Amount]END AS CreditTax,     
CASE WHEN (b.[Bad Debt Exp Converted]=0) THEN 0     
ELSE b.[Tax Amount]END AS BadDebtTax     
FROM [Account24918].[vBilling Information] AS b, [Account24918].[vProject] AS p     
    
WHERE (    
--((b.Date)>=getQryStart() And (b.Date)<=getQryEnd()) AND    
((b.Type) In (1,2,3,4,6,8)) And ((p.[ProjectNumber])=b.[Project Number]));    
END 