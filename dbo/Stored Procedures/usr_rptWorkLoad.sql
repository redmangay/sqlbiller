﻿CREATE PROCEDURE [dbo].[usr_rptWorkLoad]
(
	@From DATE = '01/10/2018',
	@To DATE = '30/10/2018',
	@Group VARCHAR(MAX) =null,
	@Controller VARCHAR(MAX) =null,
	@OrderByColumn INT =1
)
AS

/*

EXEC [dbo].[usr_rptWorkLoad] @From= '01/01/2019', @To ='02/14/2019', @OrderByColumn =1 , @Group ='1', @Controller='1'


*/

BEGIN

set dateformat dmy;
   
DECLARE @SQL VARCHAR(max) 
DECLARE @SQLGroup VARCHAR(MAX) = ''
DECLARE @SQLController VARCHAR(MAX) = ''

IF @Group <> 1 AND @Group is not null
BEGIN
SET @SQLGroup = ' AND [Job Information].V027 =''' + @Group + ''''
END

IF @Controller <> 1 AND @Controller is not null
BEGIN
SET @SQLController = ' AND [Job Information].V026 =''' + @Controller + ''''
END



	   DECLARE @WorkLoadAllNotes Table 
	   (
		  JobRecordID int,
		  JobDate date,
		  JobController varchar(max),
		  JobNo varchar(max),
		  JobClient varchar(max),
		  JobTitle varchar(max),
		  JobValue varchar(max),
		  NumberOfBids varchar(20),
		  Currency varchar(100),
		  GroupName varchar(max),
		  NoteDate date,
		  Initial varchar(10),
		  Note varchar(max),
		  ConvertedSymbol varchar(10),
		  ConvertedInvoiced money,
		  PercentBilled varchar(50),
		  PercentPaid varchar(50),
		  Header varchar(max),
		  Balance money,
		  NoteRecordID int
	   )


	   DECLARE @WorkLoad Table 
	   (
		  JobRecordID int,
		  JobDate date,
		  JobController varchar(max),
		  JobNo varchar(max),
		  JobClient varchar(max),
		  JobTitle varchar(max),
		  JobValue varchar(max),
		  NumberOfBids varchar(20),
		  Currency varchar(100),
		  GroupName varchar(max),
		  NoteDate date,
		  Initial varchar(10),
		  Note varchar(max),
		  ConvertedSymbol varchar(10),
		  ConvertedInvoiced money,
		  PercentBilled varchar(50),
		  PercentPaid varchar(50),
		  Header varchar(max),
		  Balance money,
		  NoteRecordID int
	   )

	   DECLARE @WorkLoadFinal Table 
	   (
		  JobRecordID int,
		  JobDate date,
		  JobController varchar(max),
		  JobNo varchar(max),
		  JobClient varchar(max),
		  JobTitle varchar(max),
		  JobValue varchar(max),
		  NumberOfBids varchar(20),
		  Currency varchar(100),
		  GroupName varchar(max),
		  NoteDate date,
		  Initial varchar(10),
		  Note varchar(max),
		  ConvertedSymbol varchar(10),
		  ConvertedInvoiced money,
		  PercentBilled varchar(50),
		  PercentPaid varchar(50),
		  Header varchar(max),
		  Balance money,
		  NoteRecordID int,
		  YesWithin bit,
		  YesBeyond bit

	   )


/* ============================ Get all Jobs Notes with Curent Jobs ====================================== */
SET @SQL =	'
SET dateformat dmy;
SELECT [Job Information].RecordID ,
	   NULL--convert(date,[Job Information].[V002])  AS [Job Date]
	   ,NULL-- ISNULL([EmployeeList_Job Controller].V003,'''') AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
	   , [Job Information].V001 AS [Job No]
	   ,NULL-- ISNULL([Client].[V001],''-- NONE --'') AS [Job Client]
	   ,ISNULL([Job Information].V003,''-- NONE --'') AS [Job Title]--Job Title, get from Job Information V003
	   ,0--Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) AS [Job Value]
	   --,0--Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) AS [Current Value]
	  
	   ,1 AS [Number of Bids]
	   ,NULL  --[Reporting Currency].V001 AS [Currency]	
	   ,NULL --ISNULL([Job Group].[V001],'''') AS [Group Name]	
	      --,  [Job Notes].V009 AS [Note Job No]
	   ,  [Job Notes].V003 AS [Note Date]

	    , [EmployeeList_List].V004 AS [Initial]
	   , [Job Notes].V006 AS [Note]	
	   ,NULL
	   --,CASE 
		  --WHEN [Currency Rates].V001 LIKE ''%None%'' THEN NULL 
		  --WHEN [Job Group].V004 LIKE ''%None%'' THEN NULL  
		  --WHEN [Currency Rates].V001 = [Job Group].V004 THEN NULL 
		  --ELSE convert(varchar,''C'') END AS [Converted Symbol]
		  ,NULL
		  ,NULL
		  ,NULL

	    , 	(SELECT V039  FROM RECORD WHERE TABLEID = 2743) AS [Header] 
	    ,NULL
	    --,  [Job Notes].RecordID
		--, /*CASE WHEN [Job Notes].V003 IS NULL AND [EmployeeList_List].V004 IS NULL AND [Job Notes].V006 IS NULL  THEN*/ [Job Information].RecordID /*ELSE null END*/ as [Job RecordID]
	--   ,[Job Information].V008
	--, [Job Information].V007
	--,[Group Currency Rates].V002
	--,[Currency Rates].V002
	, [Job Notes].RecordID
			FROM [Record]  [Job Notes] WITH (NOLOCK) 

			 JOIN Record [Job Information]  WITH (NOLOCK) ON [Job Information].TableID = 2475
			AND	[Job Information].IsActive = 1 
			AND			 [Job Information].[V021] IN (''Current'')
			 AND [Job Information].RecordID = CAST([Job Notes].[V009]  as int)
			 --and [Job Information].V001 = ''Td029-41''
			 --LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				--AND		[EmployeeList_Job Controller].IsActive = 1
				--AND		CAST([EmployeeList_Job Controller].RecordID AS VARCHAR(MAX)) = [Job Information].V026 		/*	Job Number	*/

			--LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
			--	AND		[Currency Rates].IsActive = 1
			--	AND		CAST([Currency Rates].RecordID AS VARCHAR(MAX)) = [Job Information].V034 		/*	Job Number	*/

			 --LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				--AND		[Tax Name].IsActive = 1
				--AND		CAST([Tax Name].RecordID AS VARCHAR(MAX)) = [Job Information].V042 		/*	Job Number	*/
			 
			--LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
			--	AND			[Job Group].IsActive = 1
			--	AND		CAST( [Job Group].RecordID AS VARCHAR(MAX)) = [Job Information].V027		/*	Currency	*/
			--	--AND [Job Group].v001 =''Building Acoustics Team''

			 --LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				--AND			[Group Currency Rates].IsActive = 1
				--AND	CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/
		
			 --LEFT JOIN Record [Client] WITH (NOLOCK) ON [Client].TableID = 2474		/*	Currency Rates	*/
				--AND			[Client].IsActive = 1
				--AND	CAST([Client].RecordID AS VARCHAR(MAX)) = [Job Information].V024		/*	[Group Currency]	*/
			 --JOIN Record [Job Notes] WITH (NOLOCK) ON
					 
		  --  LEFT JOIN [Record] [Reporting Currency] WITH (NOLOCK) ON [Reporting Currency].TableID = 2665
				--AND		[Reporting Currency].IsActive = 1
				--AND		CAST([Reporting Currency].RecordID AS VARCHAR(MAX)) = [Job Group].V005 		/*	Job Number	*/

			   JOIN [Record] [EmployeeList_List] WITH (NOLOCK) ON [EmployeeList_List].TableID = 2669 /*	Employee List	*/
				AND		[EmployeeList_List].IsActive = 1
				AND		[EmployeeList_List].RecordID = CAST([Job Notes].V007 as int) 		/*	Job Initial	*/

					WHERE  [Job Notes].TableID = 2676		/*	Job Notes	*/
				AND			[Job Notes].IsActive = 1
				
				--AND CONVERT(DATE,[Job Notes].V003) >=  CONVERT(date,'''+ cast( @From as varchar) + ''' )   AND
					--CONVERT(DATE,[Job Notes].V003) <= CONVERT(date,'''+cast(@To as varchar) + ''') 
					--and [Job Information].V001 = ''WD329-01A''
					--and [Job Information].V001 = ''WC088-01'' --and --''WA597-08''  AND -- ''WD652-02''
					--[Job Information].RecordID =    15223026 and
					--AND [Job Information].[V021] IN (''Current'')
					
					
					
					ORDER BY [Job Information].RecordID
					--ORDER BY CASE @OrderByColumn
					--		 WHEN 1 THEN [Job Information].[V002] 
					--		 WHEN 2 THEN [Job Information].[V001] 
					--			END DESC'




INSERT INTO @WorkLoadAllNotes EXEC(@SQL);
--SELECT * FROM @WorkLoadAllNotes
--ORDER BY NoteDate 


/* ============================ Get all Curent Jobs ====================================== */
SET @SQL =	'

SELECT  [Job Information].RecordID,[Job Information].[V002] AS [Job Date]
	   , ISNULL([EmployeeList_Job Controller].V003,'''') AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
	   , [Job Information].V001 AS [Job No]
	   ,ISNULL([Client].[V001],''-- NONE --'') AS [Job Client]
	   , ISNULL([Job Information].V003,''-- NONE --'') AS [Job Title] --Job Title, get from Job Information V003
	   ,Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) AS [Job Value]
	  
	   ,1 AS [Number of Bids]
	   ,[Reporting Currency].V001 AS [Currency]	
	   ,ISNULL([Job Group].[V001],'''') AS [Group Name]	
	   ,NULL --[Job Notes].V003 AS [Note Date]
	    ,NULL-- [EmployeeList_List].V004 AS [Initial]
	   ,NULL-- [Job Notes].V006 AS [Note]		
	   ,CASE WHEN [Currency Rates].V001 = [Job Group].V004 THEN NULL ELSE convert(varchar,''C'') END AS [Converted Symbol]
	
	  ,ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),2),0)  AS [Converted Invoiced]	 
	   ,CASE 
		  WHEN Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2)  > 0
		  THEN CAST(ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			 --AND [Bills And Payment Activity].V029 IN (''Invoice'',''Interim Invoice'')
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),2),0)/
			  Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) * 100 AS INT) 
		  ELSE 0 END AS [%Billed]
	
	    ,CASE 
		  WHEN Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2)  > 0
		  THEN CAST(ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V007,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			 AND [Bills And Payment Activity].V029 IN (''Payment Entry'')
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),2),0)/
			  Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) * 100 AS INT) 
		  ELSE 0 END AS [%Paid]
		    , 	(SELECT V039  FROM RECORD WHERE TABLEID = 2743) AS [Header] 
	   ,CASE WHEN Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) <= 0
		  THEN ''''
		  ELSE Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) -
			 ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0) 
			  END AS [Balance]
			  , null,NULL,NULL
		
			FROM [Record] [Job Information] WITH (NOLOCK) 

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		[EmployeeList_Job Controller].RecordID = CAST([Job Information].V026 as int) 		/*	Job Number	*/

			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		[Currency Rates].RecordID = CAST([Job Information].V034 as int)		/*	Job Number	*/

			LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		[Tax Name].RecordID = CAST([Job Information].V042 as int) 		/*	Job Number	*/
			 
			LEFT JOIN [Record] [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
				AND			[Job Group].IsActive = 1
				AND		 [Job Group].RecordID = CAST([Job Information].V027 as int)		/*	Currency	*/
			
			LEFT JOIN [Record] [Reporting Currency] WITH (NOLOCK) ON [Reporting Currency].TableID = 2665
				AND		[Reporting Currency].IsActive = 1
				AND		[Reporting Currency].RecordID = CAST([Job Group].V005 as int) 		/*	Job Number	*/
			
			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	[Group Currency Rates].V001 = [Job Group].V004		/*	[Group Currency]	*/
		
			LEFT JOIN Record [Client] WITH (NOLOCK) ON [Client].TableID = 2474		/*	Currency Rates	*/
				AND			[Client].IsActive = 1
				AND	[Client].RecordID = CAST([Job Information].V024 as int)	/*	[Group Currency]	*/
			
			--LEFT JOIN Record [Job Notes] WITH (NOLOCK) ON [Job Notes].TableID = 2676		/*	Job Notes	*/
			--	AND			[Job Notes].IsActive = 1
			--	AND	CAST([Job Notes].[V009] AS VARCHAR(MAX)) = CAST([Job Information].RecordID AS VARCHAR)
			--AND CONVERT(DATE,[Job Notes].V003,103) >= CAST('''+ CAST(@From AS VARCHAR) +''' AS DATE)  AND
			--		CONVERT(DATE,[Job Notes].V003,103) <= CAST('''+ CAST(@To AS VARCHAR) + ''' AS DATE) 		  
			 --LEFT JOIN [Record] [EmployeeList_List] WITH (NOLOCK) ON [EmployeeList_List].TableID = 2669 
				--AND		[EmployeeList_List].IsActive = 1
				--AND		CAST([EmployeeList_List].RecordID AS VARCHAR) = [Job Notes].V007 		
				
			WHERE [Job Information].TableID = 2475 AND
					[Job Information].IsActive = 1 AND
					[Job Information].[V021] IN (''Current'')
					-- and [Job Information].V001 = ''Td029-41''
					' + @SQLGroup + '
					' + @SQLController + '
														
					ORDER BY [Job Information].RecordID
							
							 '


INSERT INTO @WorkLoadFinal EXEC(@SQL);
--SELECT * FROM @WorkLoadFinal


/* ========================================= Get all Notes within the date range ==================================================== */

IF(
(   SELECT COUNT(*)
    FROM @WorkLoadAllNotes WorkLoadAllNotes
    WHERE CONVERT(DATE,WorkLoadAllNotes.[NoteDate]) >= CAST(@From AS DATE) and CONVERT(DATE,WorkLoadAllNotes.[NoteDate]) <= CAST(@To AS DATE)) > 0 )
BEGIN
	   /* LETS GET THE Notes within the date range */
	   INSERT INTO @WorkLoadFinal
		  SELECT   
			 WorkLoadAllNotes.[JobRecordID],
			 WorkLoadAllNotes.[JobDate]  ,
			 WorkLoadAllNotes.[JobController] ,
			 WorkLoadAllNotes.[JobNo]  ,
			 WorkLoadAllNotes.[JobClient]  ,
			 WorkLoadAllNotes.[JobTitle]  ,
			 WorkLoadAllNotes.[JobValue]  ,
			 WorkLoadAllNotes.[NumberofBids]  ,
			 WorkLoadAllNotes.[Currency]  ,
			 WorkLoadAllNotes.[GroupName]  ,
			 WorkLoadAllNotes.[NoteDate]  ,
			 WorkLoadAllNotes.[Initial] ,
			 WorkLoadAllNotes.[Note]  ,
			 WorkLoadAllNotes.[ConvertedSymbol]  ,
			 WorkLoadAllNotes.[ConvertedInvoiced]  ,
			0.00, -- WorkLoadAllNotes.[PercentBilled]  ,
			0.00, -- WorkLoadAllNotes.[PercentPaid]  ,
			 WorkLoadAllNotes.[Header] ,
			 WorkLoadAllNotes.[Balance] ,
			 WorkLoadAllNotes.[NoteRecordID],NULL,NULL
	   FROM @WorkLoadAllNotes WorkLoadAllNotes
    WHERE CONVERT(DATE,WorkLoadAllNotes.[NoteDate]) >= CAST(@From AS DATE) and CONVERT(DATE,WorkLoadAllNotes.[NoteDate]) <= CAST(@To AS DATE)

END
	    /* Lets update the YesWithin */			
			 UPDATE wFinal 
				SET wFinal.YesWithin = 1
				FROM @WorkLoadFinal wFinal
				JOIN (
				    SELECT JobRecordID 
				    FROM @WorkLoadFinal wf				 
				    GROUP BY JobRecordID
				    HAVING COUNT(*) >1 
				) WF on
				    wFinal.JobRecordID = WF.JobRecordID


/* ========================================= LETS GET THE MOST RECENT NOTES WHEN JOB HAS NO NOTES WITHIN THE DATES RANGE ==================================================== */


    DECLARE @WorkLoadJobsBelowNotes TABLE (
		  [Counter] int identity(1,1),
		  [NoteDate] date,
		  [NoteRecordID] int
	   )
	   
	   INSERT INTO @WorkLoadJobsBelowNotes ([NoteDate],[NoteRecordID])
	   SELECT  
		   MostRecentBelowRange2.[NoteDate]  ,
		   MostRecentBelowRange2.[NoteRecordID] --,
		  FROM @WorkLoadFinal MostRecentBelowRange 
		  JOIN(
			 SELECT  
				[NoteDate]  ,
				[NoteRecordID] ,
				[JobRecordID]  ,  
				ROW_NUMBER() OVER (PARTITION BY MostRecentBelowRange1.[JobRecordID] ORDER BY CONVERT(date,MostRecentBelowRange1.[NoteDate]) desc) rank
			  FROM @WorkLoadAllNotes MostRecentBelowRange1
			  WHERE CONVERT(DATE,MostRecentBelowRange1.[NoteDate]) > CAST(@To AS DATE)
	
			 ) MostRecentBelowRange2
		  ON MostRecentBelowRange2.rank = 1 and 
		  MostRecentBelowRange2.[JobRecordID] = MostRecentBelowRange.[JobRecordID] 
		  WHERE MostRecentBelowRange.[NoteDate] IS NULL 
		  ORDER BY MostRecentBelowRange.[JobNo]

		  
		IF ((SELECT COUNT(*) FROM @WorkLoadJobsBelowNotes) > 0)
		BEGIN
		    INSERT INTO @WorkLoadFinal
			 SELECT 
				WorkLoadAllNotes.[JobRecordID],
				WorkLoadAllNotes.[JobDate]  ,
				WorkLoadAllNotes.[JobController] ,
				WorkLoadAllNotes.[JobNo]  ,
				WorkLoadAllNotes.[JobClient]  ,
				WorkLoadAllNotes.[JobTitle]  ,
				WorkLoadAllNotes.[JobValue]  ,
				WorkLoadAllNotes.[NumberofBids]  ,
				WorkLoadAllNotes.[Currency]  ,
				WorkLoadAllNotes.[GroupName]  ,
				WorkLoadAllNotes.[NoteDate]  ,
				WorkLoadAllNotes.[Initial] ,
				WorkLoadAllNotes.[Note]  ,
				WorkLoadAllNotes.[ConvertedSymbol]  ,
				WorkLoadAllNotes.[ConvertedInvoiced]  ,
				0.00, -- WorkLoadAllNotes.[PercentBilled]  ,
				0.00, -- WorkLoadAllNotes.[PercentPaid]  ,
				WorkLoadAllNotes.[Header] ,
				WorkLoadAllNotes.[Balance] ,
				WorkLoadAllNotes.[NoteRecordID],NULL,NULL
		  FROM @WorkLoadAllNotes WorkLoadAllNotes
			 JOIN @WorkLoadJobsBelowNotes WorkLoadJobsBelowNotes ON WorkLoadJobsBelowNotes.[NoteRecordID] = WorkLoadAllNotes.[NoteRecordID]
			 AND WorkLoadJobsBelowNotes.[NoteDate] = WorkLoadAllNotes.[NoteDate]

			 DELETE  FROM @WorkLoadJobsBelowNotes

			 /* GET Second dates i.e. if records have the same dates */
			 INSERT INTO @WorkLoadJobsBelowNotes ([NoteDate],[NoteRecordID])
				SELECT  
				    MostRecentBelowRange2.[NoteDate]  ,
				    MostRecentBelowRange2.[NoteRecordID] 
				FROM @WorkLoadFinal MostRecentBelowRange JOIN(
			    SELECT 
				    [NoteDate]  ,
				    [NoteRecordID] ,
				    [JobRecordID]  --,  
				    FROM @WorkLoadAllNotes MostRecentBelowRange1
				    WHERE CONVERT(DATE,MostRecentBelowRange1.[NoteDate]) > CAST(@To AS DATE)
	   
	
				    ) MostRecentBelowRange2
				ON  MostRecentBelowRange.[JobRecordID] = MostRecentBelowRange2.[JobRecordID]
				AND  MostRecentBelowRange.[NoteDate] = MostRecentBelowRange2.[NoteDate]
				AND  MostRecentBelowRange.[NoteRecordID] <> MostRecentBelowRange2.[NoteRecordID]
  
			   INSERT INTO @WorkLoadFinal
			 SELECT 
				WorkLoadAllNotes.[JobRecordID],
				WorkLoadAllNotes.[JobDate]  ,
				WorkLoadAllNotes.[JobController] ,
				WorkLoadAllNotes.[JobNo]  ,
				WorkLoadAllNotes.[JobClient]  ,
				WorkLoadAllNotes.[JobTitle]  ,
				WorkLoadAllNotes.[JobValue]  ,
				WorkLoadAllNotes.[NumberofBids]  ,
				WorkLoadAllNotes.[Currency]  ,
				WorkLoadAllNotes.[GroupName]  ,
				WorkLoadAllNotes.[NoteDate]  ,
				WorkLoadAllNotes.[Initial] ,
				WorkLoadAllNotes.[Note]  ,
				WorkLoadAllNotes.[ConvertedSymbol]  ,
				WorkLoadAllNotes.[ConvertedInvoiced]  ,
				0.00, -- WorkLoadAllNotes.[PercentBilled]  ,
				0.00, -- WorkLoadAllNotes.[PercentPaid]  ,
				WorkLoadAllNotes.[Header] ,
				WorkLoadAllNotes.[Balance] ,
				WorkLoadAllNotes.[NoteRecordID],NULL,NULL
		  FROM @WorkLoadAllNotes WorkLoadAllNotes
			  JOIN @WorkLoadJobsBelowNotes WorkLoadJobsBelowNotes ON WorkLoadJobsBelowNotes.[NoteRecordID] = WorkLoadAllNotes.[NoteRecordID]
			  AND WorkLoadJobsBelowNotes.[NoteDate] = WorkLoadAllNotes.[NoteDate]


			   DELETE  FROM @WorkLoadJobsBelowNotes

			 /* Lets update the YesBeyond */			  
			  	UPDATE wFinal 
				SET wFinal.YesBeyond = 1
				FROM @WorkLoadFinal wFinal
				JOIN (
				    SELECT JobRecordID 
				    FROM @WorkLoadFinal wf
				    --WHERE wf.YesWithin = 1		 
				    GROUP BY JobRecordID
				    HAVING COUNT(*) >1 
				) WF on
				    wFinal.JobRecordID = WF.JobRecordID

		  END


		/* =========================================================================================================================================================================================================== */  

		  --SELECT JobRecordID 
				--    FROM @WorkLoadFinal wf				 
				--    GROUP BY JobRecordID
				--    HAVING COUNT(*) >1 
			
				--UPDATE wFinal 
				--SET wFinal.YesWithin = 1
				--FROM @WorkLoadFinal wFinal
				--JOIN (
				--    SELECT JobRecordID 
				--    FROM @WorkLoadFinal wf				 
				--    GROUP BY JobRecordID
				--    HAVING COUNT(*) >1 
				--) WF on
				--    wFinal.JobRecordID = WF.JobRecordID


		  DELETE  FROM @WorkLoadJobsBelowNotes
		  INSERT INTO @WorkLoadJobsBelowNotes ([NoteDate],[NoteRecordID])
			 SELECT  
				MostRecentBelowRange2.[NoteDate]  ,
				MostRecentBelowRange2.[NoteRecordID] --,
			 FROM @WorkLoadFinal MostRecentBelowRange JOIN(
			 SELECT  
				[NoteDate]  ,
				[NoteRecordID] ,
				[JobRecordID]  ,  
				ROW_NUMBER() OVER (PARTITION BY MostRecentBelowRange1.[JobRecordID] ORDER BY CONVERT(date,MostRecentBelowRange1.[NoteDate]) desc) rank
			   FROM @WorkLoadAllNotes MostRecentBelowRange1
			   WHERE CONVERT(DATE,MostRecentBelowRange1.[NoteDate]) < CAST(@From AS DATE)
	
				) MostRecentBelowRange2
			 ON MostRecentBelowRange2.rank = 1 and 
			 MostRecentBelowRange2.[JobRecordID] = MostRecentBelowRange.[JobRecordID]
 
			 WHERE MostRecentBelowRange.[NoteDate] is null 
			 AND MostRecentBelowRange.YesWithin IS NULL AND MostRecentBelowRange.YesBeyond IS NULL
			 ORDER BY MostRecentBelowRange.[JobNo]



		     INSERT INTO @WorkLoadFinal
			 SELECT 
				WorkLoadAllNotes.[JobRecordID],
				WorkLoadAllNotes.[JobDate]  ,
				WorkLoadAllNotes.[JobController] ,
				WorkLoadAllNotes.[JobNo]  ,
				WorkLoadAllNotes.[JobClient]  ,
				WorkLoadAllNotes.[JobTitle]  ,
				WorkLoadAllNotes.[JobValue]  ,
				WorkLoadAllNotes.[NumberofBids]  ,
				WorkLoadAllNotes.[Currency]  ,
				WorkLoadAllNotes.[GroupName]  ,
				WorkLoadAllNotes.[NoteDate]  ,
				WorkLoadAllNotes.[Initial] ,
				WorkLoadAllNotes.[Note]  ,
				WorkLoadAllNotes.[ConvertedSymbol]  ,
				WorkLoadAllNotes.[ConvertedInvoiced]  ,
				0.00, -- WorkLoadAllNotes.[PercentBilled]  ,
				0.00, -- WorkLoadAllNotes.[PercentPaid]  ,
				WorkLoadAllNotes.[Header] ,
				WorkLoadAllNotes.[Balance] ,
				WorkLoadAllNotes.[NoteRecordID],NULL,NULL
			 FROM @WorkLoadAllNotes WorkLoadAllNotes
			  JOIN @WorkLoadJobsBelowNotes WorkLoadJobsBelowNotes ON WorkLoadJobsBelowNotes.[NoteRecordID] = WorkLoadAllNotes.[NoteRecordID]
			  AND WorkLoadJobsBelowNotes.[NoteDate] = WorkLoadAllNotes.[NoteDate]

			  DELETE  FROM @WorkLoadJobsBelowNotes


			 INSERT INTO @WorkLoadJobsBelowNotes ([NoteDate],[NoteRecordID])
			 SELECT  
				MostRecentBelowRange2.[NoteDate]  ,
				MostRecentBelowRange2.[NoteRecordID] 
			 FROM @WorkLoadFinal MostRecentBelowRange JOIN(
			    SELECT  
				[NoteDate]  ,
				[NoteRecordID] ,
				[JobRecordID]  
				    FROM @WorkLoadAllNotes MostRecentBelowRange1
				    WHERE CONVERT(DATE,MostRecentBelowRange1.[NoteDate]) < CAST(@From AS DATE)
	   
	
				    ) MostRecentBelowRange2
				ON  MostRecentBelowRange.[JobRecordID] = MostRecentBelowRange2.[JobRecordID]
				AND  MostRecentBelowRange.[NoteDate] = MostRecentBelowRange2.[NoteDate]
				AND  MostRecentBelowRange.[NoteRecordID] <> MostRecentBelowRange2.[NoteRecordID]
				 AND MostRecentBelowRange.YesWithin IS NULL AND MostRecentBelowRange.YesBeyond IS NULL
  
			   INSERT INTO @WorkLoadFinal
			 SELECT 
				WorkLoadAllNotes.[JobRecordID],
				WorkLoadAllNotes.[JobDate]  ,
				WorkLoadAllNotes.[JobController] ,
				WorkLoadAllNotes.[JobNo]  ,
				WorkLoadAllNotes.[JobClient]  ,
				WorkLoadAllNotes.[JobTitle]  ,
				WorkLoadAllNotes.[JobValue]  ,
				WorkLoadAllNotes.[NumberofBids]  ,
				WorkLoadAllNotes.[Currency]  ,
				WorkLoadAllNotes.[GroupName]  ,
				WorkLoadAllNotes.[NoteDate]  ,
				WorkLoadAllNotes.[Initial] ,
				WorkLoadAllNotes.[Note]  ,
				WorkLoadAllNotes.[ConvertedSymbol]  ,
				WorkLoadAllNotes.[ConvertedInvoiced]  ,
				0.00, -- WorkLoadAllNotes.[PercentBilled]  ,
				0.00, -- WorkLoadAllNotes.[PercentPaid]  ,
				WorkLoadAllNotes.[Header] ,
				WorkLoadAllNotes.[Balance] ,
				WorkLoadAllNotes.[NoteRecordID],NULL,NULL
			 FROM @WorkLoadAllNotes WorkLoadAllNotes
			  JOIN @WorkLoadJobsBelowNotes WorkLoadJobsBelowNotes ON WorkLoadJobsBelowNotes.[NoteRecordID] = WorkLoadAllNotes.[NoteRecordID]
			  AND WorkLoadJobsBelowNotes.[NoteDate] = WorkLoadAllNotes.[NoteDate]
			  --END



    UPDATE A 
    SET A.GroupName = B.GroupName, A.JobController = B.JobController, A.PercentBilled = B.PercentBilled, A.PercentPaid = B.PercentPaid, A.Currency = B.Currency
    FROM @WorkLoadFinal A 
    JOIN @WorkLoadFinal B ON A.JobRecordID = B.JobRecordID
    AND B.GroupName IS NOT NULL AND B.JobClient IS NOT NULL
    WHERE A.GroupName IS NULL AND A.JobController IS NULL

			  
IF (@OrderByColumn = 1)
BEGIN
SELECT   
	   WorkLoadFinal.[JobRecordID],
	   WorkLoadFinal.[JobDate]  ,
	   ISNULL(WorkLoadFinal.[JobController],'') [JobController] ,
	   WorkLoadFinal.[JobNo]  ,
	   ISNULL(WorkLoadFinal.[JobClient],'') [JobClient] ,
	   ISNULL(WorkLoadFinal.[JobTitle],'') [JobTitle] ,
	   cast(ISNULL(WorkLoadFinal.[JobValue],0) as money) [JobValue] ,
	   WorkLoadFinal.[NumberofBids]  ,
	   WorkLoadFinal.[Currency]  ,
	   ISNULL(WorkLoadFinal.[GroupName],'')  [GroupName],
	   WorkLoadFinal.[NoteDate]  ,
	   WorkLoadFinal.[Initial] ,
	    REPLACE(WorkLoadFinal.[Note],CHAR(13)+CHAR(10),' ') as [Note],
	   --WorkLoadFinal.[Note]  ,
	   WorkLoadFinal.[ConvertedSymbol]  ,
	   WorkLoadFinal.[ConvertedInvoiced]  ,
	   ISNULL(WorkLoadFinal.[PercentBilled],0)  [PercentBilled],
	   ISNULL(WorkLoadFinal.[PercentPaid],0)  [PercentPaid],
	   WorkLoadFinal.[Header] ,
	   ISNULL(WorkLoadFinal.[Balance],0) [Balance],
	   WorkLoadFinal.[NoteRecordID],
	   WorkLoadFinal.YesWithin,
	   WorkLoadFinal.YesBeyond
 FROM @WorkLoadFinal WorkLoadFinal
ORDER BY CONVERT(DATE,[JobDate]) ASC -- [Job Date]
END
ELSE IF (@OrderByColumn = 2)
BEGIN
SELECT  
	   WorkLoadFinal.[JobRecordID],
	   WorkLoadFinal.[JobDate]  ,
	   ISNULL(WorkLoadFinal.[JobController],'') [JobController] ,
	   WorkLoadFinal.[JobNo]  ,
	   ISNULL(WorkLoadFinal.[JobClient],'') [JobClient] ,
	   ISNULL(WorkLoadFinal.[JobTitle],'') [JobTitle] ,
	   cast(ISNULL(WorkLoadFinal.[JobValue],0) as money) [JobValue] ,
	   WorkLoadFinal.[NumberofBids]  ,
	   WorkLoadFinal.[Currency]  ,
	   ISNULL(WorkLoadFinal.[GroupName],'')  [GroupName],
	   WorkLoadFinal.[NoteDate]  ,
	   WorkLoadFinal.[Initial] ,
	    REPLACE(WorkLoadFinal.[Note],CHAR(13)+CHAR(10),' ') as [Note],
	   --WorkLoadFinal.[Note]  ,
	   WorkLoadFinal.[ConvertedSymbol]  ,
	   WorkLoadFinal.[ConvertedInvoiced]  ,
	   ISNULL(WorkLoadFinal.[PercentBilled],0)  [PercentBilled],
	   ISNULL(WorkLoadFinal.[PercentPaid],0)  [PercentPaid],
	   WorkLoadFinal.[Header] ,
	   ISNULL(WorkLoadFinal.[Balance],0) [Balance],
	   WorkLoadFinal.[NoteRecordID],
	   WorkLoadFinal.YesWithin,
	   WorkLoadFinal.YesBeyond
FROM @WorkLoadFinal WorkLoadFinal
ORDER BY [JobNo]  ASC
END


END
