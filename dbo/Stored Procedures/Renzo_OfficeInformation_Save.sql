﻿
CREATE PROCEDURE [Renzo_OfficeInformation_Save]
(
	@RecordID int = null,
	@UserID int = null,
	@Return varchar(max) output
)
AS
/* Stored procedure run after saving a record (Add or Update) */
BEGIN TRY

	IF		@RecordID IS NULL
			UPDATE TOP (100)[Record] 																								/*	[Office Information]	*/
				SET			V025 = [Organisation Name].V001																			/*	[Organisation Name.]	*/
				FROM		[Record]
				JOIN		Record [Organisation Name] ON								[Organisation Name].TableID = '2718'		/*	Organisation Details	*/
																				AND		CAST( [Organisation Name].RecordID AS varchar(MAX)) = [Record].V024		/*	Organisation Name	*/
				WHERE		[Record].TableID = '2474'		/*	Office Information	*/
				AND			[Record].IsActive = 1
				AND			ISNULL([Record].V025, 'syncmenow' ) <> ISNULL([Organisation Name].V001, 'syncmenow' );
	ELSE
			UPDATE			[Record] 																								/*	[Office Information]	*/
				SET			V025 = [Organisation Name].V001																			/*	[Organisation Name.]	*/
				FROM		[Record]
				JOIN		Record [Organisation Name] ON								[Organisation Name].TableID = '2718'		/*	Organisation Details	*/
																				AND		[Organisation Name].IsActive = 1
																				AND		CAST( [Organisation Name].RecordID AS varchar(MAX)) = [Record].V024		/*	Organisation Name	*/
				WHERE		[Record].RecordID = @RecordID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog]([Module], [ErrorMessage], [ErrorTrack], [ErrorTime], [Path]) 
		VALUES ('Renzo_OfficeInformation_Save', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
RETURN @@ERROR
