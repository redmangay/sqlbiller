﻿

CREATE PROCEDURE [dbo].[ets_ReportItemFilter_Insert]
(
	@nNewID int output,
	@nReportItemID int,
	@nFilterColumnID int,
	@sFilterColumnValue varchar(250) = NULL,
	@sFilterOperator varchar(25) = NULL,
	@nDisplayOrder int,
	@sJoinOperator varchar(10) = NULL
)
AS

BEGIN TRY
	INSERT INTO [ReportItemFilter]
	(
		ReportItemID, 
		FilterColumnID, 
		FilterColumnValue,
		FilterOperator,
		DisplayOrder,
		JoinOperator
	) VALUES (
		@nReportItemID,
		@nFilterColumnID,
		@sFilterColumnValue,
		@sFilterOperator,
		@nDisplayOrder,
		@sJoinOperator
	)
	SELECT @nNewID = @@IDENTITY
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_ReportItemFilter_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
