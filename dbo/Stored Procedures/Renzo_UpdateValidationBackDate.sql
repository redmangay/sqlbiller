﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[Renzo_UpdateValidationBackDate]

AS

BEGIN TRY

/*


exec [dbo].[Renzo_UpdateValidationBackDate]

*/

    DECLARE @NoOfBackDate int
    DECLARE @BasedBackDate varchar(20)    

    SELECT @NoOfBackDate = V030 FROM RECORD
    WHERE TABLEID = 2743
    AND ISACTIVE = 1

    SET @NoOfBackDate = @NoOfBackDate * -1    

    SELECT @BasedBackDate = convert(varchar(10),DATEADD(DAY,@NoOfBackDate,GETDATE()),103)
    --SELECT 'value>=' + @BasedBackDate

    UPDATE [Column]
    SET ValidationOnEntry = (SELECT 'value>=' + @BasedBackDate )
    WHERE ColumnID = 49618 /* BAPA: Invoice Date */

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Renzo_UpdateValidationBackDate', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

