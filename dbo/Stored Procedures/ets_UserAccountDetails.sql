﻿

CREATE PROCEDURE [dbo].[ets_UserAccountDetails]
(
      @sKey varchar(10) = NULL
)

AS
/*
      UNIT TESTING
      ============
      EXEC dbo.ets_UserAccountDetails
      EXEC dbo.ets_UserAccountDetails @sKey = 1
*/
BEGIN TRY
    DECLARE @nUserID int

    IF @sKey IS NULL
    BEGIN
		PRINT 'NULL'
        SELECT @nUserID = MAX(UserID) 
			FROM [User]
			WHERE IsActive=1
	END
	ELSE
		SELECT @nUserID = CAST(@sKey AS int)
	PRINT '@nUserID = ' + CAST(@nUserID  AS varchar)
    SELECT     
			Account.AccountName, 
			[User].UserID, 
			[User].FirstName, 
			[User].LastName, 
			[User].PhoneNumber, 
			[User].Email, 
			[User].IsActive, 
			[User].DateAdded, 
			[User].DateUpdated, 
            UserRole.IsAccountHolder, 
            UserRole.IsAdvancedSecurity
            --[User].[EncryptedPassword] -- Red removed - Ticket 3059
		FROM [User] 
		INNER JOIN  UserRole ON [User].UserID = UserRole.UserID
		INNER JOIN Account ON UserRole.AccountID=Account.AccountID
	    WHERE [User].UserID = @nUserID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_UserAccountDetails', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
