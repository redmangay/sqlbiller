﻿

CREATE PROCEDURE [dbo].[spUpdateLinkedTables]
(
    @nParentTableID int,
    @sParentTextField varchar(10),
    @nChildTableID int,
    @sChildTextField varchar(10),
    @sChildLinkField varchar(10),
    @sParentTextField2 varchar(10)=NULL,
    @sChildTextField2 varchar(10)=NULL,
	@bDebug bit = 0
)
AS

BEGIN TRY
    DECLARE @sSQL varchar(MAX)
    SET @sSQL =
    'UPDATE Child
        SET ' + @sChildLinkField + ' = Parent.RecordID
        FROM [Record] Child
        JOIN [Record] Parent ON Parent.' + @sParentTextField + ' = Child.' + @sChildTextField
        

    IF @sParentTextField2 IS NOT NULL AND LEN(@sParentTextField2) > 0 AND @sChildTextField2 IS NOT NULL AND LEN(@sChildTextField2) > 0
    BEGIN
        SET @sSQL = @sSQL + '
            AND Parent.' + @sParentTextField2 + ' = Child.' + @sChildTextField2
    END    

    -- Finish it off:
    SET @sSQL = @sSQL + '
        WHERE Child.TableID = ' + CAST(@nChildTableID as varchar) + '
        AND Parent.TableID = ' + CAST(@nParentTableID as varchar) + '
		AND Child.IsActive = 1
		AND Parent.IsActive = 1'
    PRINT @sSQL
    IF @bDebug = 0
		EXEC (@sSQL)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spUpdateLinkedTables', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

