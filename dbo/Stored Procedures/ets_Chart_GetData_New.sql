﻿/****** Object:  StoredProcedure [dbo].[ets_Chart_GetData_New]    Script Date: 2018-02-01 5:07:02 PM ******/
CREATE PROCEDURE [ets_Chart_GetData_New]
       -- Add the parameters for the stored procedure here
       @nTableID int,
	   @sGraphXAxisColumnID varchar(MAX),
       @sValueFields varchar(MAX),
       @dStartDate datetime,
       @dEndDate datetime,
       @sPeriod varchar(20), -- Possible values: Year, Month, Day, Hour
       -- When you pass Year it will return the month averages
       -- when you pass Month it will return the day averages
       -- when you pass Week it will return the day averages
       -- when you pass day it will return hour aveages
       -- when you pass hour it will return all data
	   @GraphSeriesColumnID varchar(MAX),
	   @GraphSeriesID varchar(MAX),
	   @ForceAverage bit,
       @bPercent bit = 0,
	   @nMaxRows int = NULL
AS
/*
       PRINT SYSDATETIME()
       EXEC ets_Chart_GetData_New
              @nTableID=919
              ,@sValueFields = 'V002'
              ,@dStartDate='2012-12-07 12:28'
              ,@dEndDate='2013-12-09 16:00'
              ,@sPeriod='Hour'
              ,@bPercent=1,@nMaxRows=200
       PRINT SYSDATETIME()
       -- 3603515 1st run
       -- 2919922 2nd run : no IF
       -- 2529297 3rd run : using IN
*/

BEGIN TRY
    SET NOCOUNT ON;
    --SET DATEFORMAT dmy  
    DECLARE @Sql varchar(MAX)       
    DECLARE @sDateTime varchar(500)
    DECLARE @sSeries varchar(MAX)

	DECLARE @bGetAverage bit = 0
	IF (@ForceAverage = 1) OR ((LEN(@sPeriod) > 0) AND (@sPeriod <> 'Hour'))
		SET @bGetAverage = 1

	IF (@ForceAverage = 1 )AND (@sPeriod = 'Hour')
	BEGIN
		SET @sPeriod = CASE
			WHEN DATEDIFF(month, @dStartDate, @dEndDate) > 24 THEN 'Year'
			WHEN DATEDIFF(day, @dStartDate, @dEndDate) > 62 THEN 'Month'
			WHEN DATEDIFF(day, @dStartDate, @dEndDate) > 14 THEN 'Week'
			WHEN DATEDIFF(hour, @dStartDate, @dEndDate) > 48 THEN 'Day'
			ELSE 'Hour'
		END
	END

	PRINT @sPeriod

	DECLARE @bIgnoreSymbols bit = 0
	SELECT @bIgnoreSymbols = IgnoreSymbols FROM [Column] WHERE SystemName = @sValueFields AND TableID = @nTableID AND ColumnType = 'number'

	--IF (@bIgnoreSymbols IS NOT NULL) AND (@bIgnoreSymbols = 1)
		--SET @sValueFields = 'dbo.RemoveNonNumericChar(' + @sValueFields + ') '


	/* Ticket Ticket 4371 - Issues with Archive Function - need to get data from archive */
	DECLARE @RecordSource varchar(200) = '[Record]' -- by default we get data from the [Record] table

	-- Check to see if we need to use the archve database:
	IF EXISTS(SELECT * FROM [Table] 
				WHERE [TableID] = @nTableID 
				AND ArchiveMonths IS NOT NULL /* Table has an archive */ 
				AND @dStartDate < DATEADD(month, -ArchiveMonths, ArchiveLastRun) /* Start date is before our Last live data date */ )
	BEGIN
		/* then we need to get the data from the archive, not from the live data: */
		SELECT @RecordSource = ArchiveDatabase + '.[dbo].[Record]' FROM [Table] WHERE [TableID] = @nTableID 
	END

	PRINT @RecordSource /* check */

	IF @bGetAverage = 0 -- then we do not reduce the data
	BEGIN
		IF @nMaxRows IS NOT NULL
			SET @sql = 'SELECT TOP ' + CAST(@nMaxRows as varchar) + ' ' + @sGraphXAxisColumnID + ', '
		ELSE
			SET @sql = 'SELECT ' + @sGraphXAxisColumnID + ', '

        SET @sql = @sql + @sValueFields + ', [Record].RecordID' +
			' FROM ' + @RecordSource + ' [Record]' + 
			--' WHERE IsActive=1 AND DateTimeRecorded BETWEEN ''' + CAST(@dStartDate AS Varchar) + ''' AND ''' +  CAST(@dEndDate AS Varchar) + '''' +
			/*****RP MODIFIED TICKET 4371*****/
			--' WHERE IsActive=1 AND CAST((SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')) AS datetime) >= ''' + CONVERT(varchar(20), @dStartDate) + '''' + 
			--' AND CAST((SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')) AS datetime) <= ''' + CONVERT(varchar(20), @dEndDate) + '''' +
			' WHERE IsActive=1 AND CAST((SELECT dbo.[fnStringToDate](CONVERT(VARCHAR,'+ @sGraphXAxisColumnID +',120))) AS datetime) >= ''' + CONVERT(varchar(20), @dStartDate) + '''' + 
			' AND CAST((SELECT dbo.[fnStringToDate](CONVERT(VARCHAR,'+ @sGraphXAxisColumnID +',120))) AS datetime) <= ''' + CONVERT(varchar(20), @dEndDate) + '''' +
			/*****END MODIFICATION*****/	
			' AND TableID = ' + CAST(@nTableID AS Varchar) +
			CASE
				WHEN @GraphSeriesColumnID IS NULL THEN ''
				WHEN @GraphSeriesID IS NULL THEN ''
				WHEN LEN(@GraphSeriesColumnID) = 0 THEN ''
				WHEN LEN(@GraphSeriesID) = 0 THEN ''
				ELSE ' AND [' + @GraphSeriesColumnID + '] = ''' + @GraphSeriesID + ''''
			END +
			/*RP MODIFIED TICKET 4371*/
			--' ORDER BY CAST((SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')) AS datetime) DESC'
			' ORDER BY CAST((SELECT dbo.[fnStringToDate](CONVERT(VARCHAR,'+ @sGraphXAxisColumnID +',120))) AS datetime) DESC'
			/*END MODIFICATION*/
	END
	ELSE -- we are going to reduce the data
	BEGIN
		-- Convert the fields to averages
		SET  @sSeries = 'CAST(dbo.RemoveNonNumericChar(' + @sValueFields + ') AS decimal(18,4)) '

		SET @sDateTime = 
			CASE @sPeriod
				WHEN 'Year' THEN 
				/*RP MODIFIED TICKET 4371*/
				--'CAST((SELECT dbo.[fnStringToDate](''01-'' + RIGHT(CONVERT(varchar(10), (SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')), 103), 7))) AS datetime)'
				'CAST((SELECT dbo.[fnStringToDate](''01-'' + RIGHT(CONVERT(varchar(10), (SELECT dbo.[fnStringToDate](CONVERT(VARCHAR,'+ @sGraphXAxisColumnID +',120))), 103), 7))) AS datetime)'
				/*END MODIFICATION*/
				WHEN 'Month' THEN 
				/*RP MODIFIED TICKET 4371*/
				--'CAST(CAST((SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')) AS date) AS datetime)'
				'CAST(CAST((SELECT dbo.[fnStringToDate](CONVERT(VARCHAR,'+ @sGraphXAxisColumnID +',120))) AS date) AS datetime)'
				/*END MODIFICATION*/
				WHEN 'Week' THEN 
				/*RP MODIFIED TICKET 4371*/
				--'CAST(CAST((SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')) AS date) AS datetime)'
				'CAST(CAST((SELECT dbo.[fnStringToDate](CONVERT(VARCHAR,'+ @sGraphXAxisColumnID +',120))) AS date) AS datetime)'
				/*END MODIFICATION*/
				/*RP MODIFIED TICKET 4371*/
				--WHEN 'Day' THEN 'CAST((SELECT dbo.[fnStringToDate](CONVERT(varchar(10), (SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')), 103)'
				--	+ ' + '' '' + ' +
				--	'CONVERT(varchar(2), (SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')), 114) + '':00''))' +
				--	' AS datetime)'
				--WHEN 'Hour' THEN
				--'CAST((SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')) AS datetime)'
				WHEN 'Day' THEN 'CAST((SELECT dbo.[fnStringToDate](CONVERT(varchar(10), (SELECT dbo.[fnStringToDate](CONVERT(VARCHAR,'+ @sGraphXAxisColumnID +',120))), 103)'
					+ ' + '' '' + ' +
					'CONVERT(varchar(2), (SELECT dbo.[fnStringToDate](CONVERT(VARCHAR,'+ @sGraphXAxisColumnID +',120))), 114) + '':00''))' +
					' AS datetime)'
				WHEN 'Hour' THEN
				'CAST((SELECT dbo.[fnStringToDate](CONVERT(VARCHAR,'+ @sGraphXAxisColumnID +',120))) AS datetime)'
				/*END MODIFICATION*/

			END
			
		PRINT @sDateTime

		SET @sql =    ' SELECT ' + @sDateTime + ' AS [' + @sGraphXAxisColumnID + '], ' + @sSeries + 'AS [' + @sValueFields + ']' + ', [Record].RecordID AS RecordID' +
			' FROM ' + @RecordSource + ' [Record]' + 
			/*RP MODIFIED TICKET 4371*/
			--' WHERE IsActive=1 AND CAST((SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')) AS datetime) >= ''' + CONVERT(varchar(20), @dStartDate) + '''' +
			--' AND CAST((SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')) AS datetime) <= ''' + CONVERT(varchar(20), @dEndDate) + '''' +
			' WHERE IsActive=1 AND CAST((SELECT dbo.[fnStringToDate](CONVERT(VARCHAR,'+ @sGraphXAxisColumnID +',120))) AS datetime) >= ''' + CONVERT(varchar(20), @dStartDate) + '''' +
			' AND CAST((SELECT dbo.[fnStringToDate](CONVERT(VARCHAR,'+ @sGraphXAxisColumnID +',120))) AS datetime) <= ''' + CONVERT(varchar(20), @dEndDate) + '''' +
			/*END MODIFICATION*/
			' AND TableID = ' + CAST(@nTableID AS Varchar(20)) +
			CASE
				WHEN @GraphSeriesColumnID IS NULL THEN ''
				WHEN @GraphSeriesID IS NULL THEN ''
				WHEN LEN(@GraphSeriesColumnID) = 0 THEN ''
				WHEN LEN(@GraphSeriesID) = 0 THEN ''
				ELSE ' AND [' + @GraphSeriesColumnID + '] = ''' + @GraphSeriesID + ''''
			END +
            ' AND ISNUMERIC(dbo.RemoveNonNumericChar(' + @sValueFields + ')) = 1'

		SET @sql = 'SELECT [' + @sGraphXAxisColumnID + '], AVG([' + @sValueFields + ']) AS [' + @sValueFields + '], MAX(RecordID) AS RecordID FROM ('+ @sql +') AS Main' + 
					' GROUP BY  Main.[' + @sGraphXAxisColumnID + ']' +
					' ORDER BY  Main.[' + @sGraphXAxisColumnID + ']'
    END

    IF (@bPercent=1)
    BEGIN
		DECLARE @MAX decimal(18,8), @Min decimal(18,8), @Step decimal(18,8),
				@PercentSQL nvarchar(MAX), @id int
		IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tempMeanMinMax')
				SELECT @id = ISNULL(MAX(ID),0) + 1 FROM tempMeanMinMax
		ELSE
		BEGIN
				CREATE TABLE tempMeanMinMax
				(
					ID int,
					DateTimeRecorded datetime,
					[Value] decimal(18,8),
					[Percent] decimal(18,4)
				)
				CREATE INDEX id ON tempMeanMinMax(ID)
				SET @id = 1
		END

		PRINT '@id: ' + cast(@id as varchar)
		SET @PercentSQL = 'INSERT INTO tempMeanMinMax (ID,  DateTimeRecorded, [Value]) ' + replace(@sql, 'SELECT', 'SELECT ' + cast(@id as varchar) + ', ')
		IF @nMaxRows IS NOT NULL AND @sPeriod = 'Hour'
			SET @PercentSQL = 'INSERT INTO tempMeanMinMax (ID,  DateTimeRecorded, [Value]) ' + replace(@sql, 'SELECT TOP ' + CAST(@nMaxRows as varchar) , 'SELECT TOP ' + CAST(@nMaxRows as varchar) + ' '  + cast(@id as varchar) + ', ')

		PRINT @PercentSQL
		EXEC(@PercentSQL)

		SELECT @MAX = MAX([Value]) FROM tempMeanMinMax WHERE [ID] = @id
		SELECT @Min = MIN([Value]) FROM tempMeanMinMax WHERE [ID] = @id
              
		PRINT @MAX
		PRINT @Min
              
		SET @Step=1
		IF @MAX!=0 AND @MAX is not NULL AND (@MAX-@Min)!=0
		SELECT @Step = 100/(@MAX-@Min)
		UPDATE tempMeanMinMax SET [Percent] = ([Value]-@Min) * @Step WHERE [ID] = @id
		SELECT  DateTimeRecorded, [Percent],[Value]  from  tempMeanMinMax WHERE [ID] = @id
		DELETE FROM tempMeanMinMax where ID = @id
    END
    ELSE
    BEGIN
        PRINT @sql
        EXEC(@sql)
    END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Chart_GetData_New', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

