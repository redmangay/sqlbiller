﻿
 
CREATE PROCEDURE [dbo].[ets_GetMaxDateTimeRecorded]
(
	@nTableID int,
	@nRecordID int,
	@DateTimeRecorded DateTime
)
	/*----------------------------------------------------------------
	EXEC ets_GetMaxDateTimeRecorded 109,-1
	---------------------------------------------------------------*/
 
AS

BEGIN TRY
	SELECT MAX(DateTimeRecorded) 
		FROM Record
		WHERE TableID=@nTableID and RecordID<>@nRecordID
		AND IsActive=1
		AND DateTimeRecorded<=@DateTimeRecorded
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_GetMaxDateTimeRecorded', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
