﻿CREATE PROCEDURE [dbo].[Kate_SaveISS]
(
	@RecordID int,
	@UserID int,
	@Return varchar(max) output
)
AS
-- exec Kate_SaveISS @RecordID=769658, @UserID=0, @Return=''
BEGIN
	DECLARE @ParentRecordID int, @TableID int
	SELECT @ParentRecordID = cast([V004] as int) 
		, @TableID = [TableID]
		FROM [Record] WHERE RecordID = @RecordID

	DECLARE @ISS TABLE (Score int)
	DECLARE @NISS TABLE (Score int)
	DECLARE @t2 TABLE (Score int)
	DECLARE @ISS_Score int
	DECLARE @NISS_Score int

	-- ISS table groups by body region
	INSERT INTO @ISS (Score)
			SELECT MAX(SUBSTRING(V003, 8, 1))
				FROM [Record] 
				WHERE TableID = 2377
				AND [V004] = @ParentRecordID -- ParentRecordID
				and SUBSTRING(V003, 8, 1) != '9'
				AND IsActive = 1
				GROUP BY V001 -- Body Region
			
	-- NISS is not grouped by body region	
	INSERT INTO @NISS (Score)
			SELECT SUBSTRING(V003, 8, 1)
				FROM [Record] 
				WHERE TableID = 2377
				AND [V004] = @ParentRecordID -- ParentRecordID
				AND IsActive = 1
				and SUBSTRING(V003, 8, 1) != '9'


	IF EXISTS(SELECT 1 FROM @ISS WHERE Score = 6)
	BEGIN
		SET @ISS_Score = 75
		SET @NISS_Score = 75
	END
	ELSE
	BEGIN
		INSERT INTO @t2 (Score) SELECT TOP 3 Score FROM @ISS ORDER BY Score DESC
		SELECT @ISS_Score = SUM(SQUARE(Score)) FROM @t2 
		DELETE FROM @t2
		INSERT INTO @t2 (Score) SELECT TOP 3 Score FROM @NISS ORDER BY Score DESC
		SELECT @NISS_Score = SUM(SQUARE(Score)) FROM @t2 
	END
	
	-- UPDATE Demographics Table 2347 which has the same parent record
		UPDATE [Record] SET 
			V049 = CAST(@ISS_Score AS varchar), 
			V070 = CAST(@NISS_Score AS varchar) 
			WHERE TableID = 2347
			AND V002 = @ParentRecordID

END


