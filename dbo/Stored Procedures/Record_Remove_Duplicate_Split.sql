﻿

 
CREATE PROCEDURE   [dbo].[Record_Remove_Duplicate_Split]
(@nS int,@nE int)
	/*----------------------------------------------------------------
	exec [dbo].[Record_Remove_Duplicate_Split] 1,2
	---------------------------------------------------------------*/
 
AS
 
BEGIN


	-- Make old duplicates IsActive=0 for a db

	DECLARE @sRecordIDs varchar(max)='-1';
	DECLARE T_Cursor CURSOR FOR
	SELECT TableID FROM zCleanData WHERE  HasDuplicate=1 AND ( SN>=@nS and SN<=@nE);
	DECLARE @nTableID INT;
	OPEN T_Cursor;
	FETCH NEXT FROM T_Cursor INTO @nTableID;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
				DECLARE DR_Cursor CURSOR FOR
				SELECT RecordID ,UniqueKey,DateAdded FROM [Record] 
					WHERE  IsActive=1 and  TableID=@nTableID and UniqueKey is not null
					  ORDER BY UniqueKey ASC,DateAdded DESC; 

				DECLARE @nRecordID INT; DECLARE @nUniqueKey varchar(205);DECLARE @dtDateAdded datetime;
				DECLARE @nUniqueKeyPre varchar(205)='';
				DECLARE @nCounter2 INT=0;
				OPEN DR_Cursor
				FETCH NEXT FROM DR_Cursor INTO @nRecordID,@nUniqueKey,@dtDateAdded;
				WHILE @@FETCH_STATUS = 0
				   BEGIN	  
					  IF @nUniqueKeyPre=@nUniqueKey
						BEGIN
							--SET @sRecordIDs=@sRecordIDs+ ','+ CAST(@nRecordID AS VARCHAR(20))
							--OR
							UPDATE [Record] SET IsActive=0 WHERE RecordID=@nRecordID
						END			

					  SET @nUniqueKeyPre=@nUniqueKey
					  FETCH NEXT FROM DR_Cursor INTO @nRecordID,@nUniqueKey,@dtDateAdded;
				   END;
				CLOSE DR_Cursor
				DEALLOCATE DR_Cursor

		  FETCH NEXT FROM T_Cursor INTO @nTableID;
	   END;
	CLOSE T_Cursor;
	DEALLOCATE T_Cursor;

	--DECLARE @sDRSQL VARCHAR(MAX);
	--SET @sDRSQL='UPDATE [Record] SET IsActive=0 WHERE RecordID IN('+@sRecordIDs+');'
	--PRINT @sDRSQL
	--EXEC (@sDRSQL)
	
END





