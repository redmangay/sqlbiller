﻿

CREATE PROCEDURE [dbo].[dbg_ViewItem_Select]
(
	@nViewID int  = NULL,
	@nColumnID int  = NULL,
	@bSearchField bit  = NULL,
	@bFilterField bit  = NULL,
	@sAlignment varchar(25)  = NULL,
	@nWidth int  = NULL,
	@bShowTotal bit  = NULL,
	@sSummaryCellBackColor varchar(50)  = NULL,
	@sOrder nvarchar(200) = ViewItemID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	-- Stored Procedure: dbg_ViewItem_Select
	EXEC [dbg_ViewItem_Select] 1
	-- Date: Aug 27 2015 11:11AM
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(4000)
	DECLARE @sWhere nvarchar(4000)
	DECLARE @sSelectCount nvarchar(MAX)

	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nViewID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ViewID = '+CAST(@nViewID AS NVARCHAR)
	IF @nColumnID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ColumnID = '+CAST(@nColumnID AS NVARCHAR)

	IF @bSearchField IS NOT NULL 
		SET @sWhere = @sWhere + ' AND SearchField = '+CAST(@bSearchField AS NVARCHAR)
	IF @bFilterField IS NOT NULL 
		SET @sWhere = @sWhere + ' AND FilterField = '+CAST(@bFilterField AS NVARCHAR)
	IF @sAlignment IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Alignment LIKE '+'''' + @sAlignment + ''''
	IF @nWidth IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Width = '+CAST(@nWidth AS NVARCHAR)
	IF @bShowTotal IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ShowTotal = '+CAST(@bShowTotal AS NVARCHAR)
	IF @sSummaryCellBackColor IS NOT NULL 
		SET @sWhere = @sWhere + ' AND SummaryCellBackColor LIKE '+'''' + @sSummaryCellBackColor + ''''
	SET @sSelect = 'SELECT * FROM 
	(SELECT ViewItem.*, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum FROM ViewItem' + @sWhere + ') as UserInfo'
 
	-- Extend WHERE to include paging:

	 SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [ViewItem] ' + @sWhere 


	SET @sWhere = @sWhere + ' AND RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	SET @sOrder = ' ORDER BY ' + @sOrder
	SET @sSelect = @sSelect + @sWhere + @sOrder
 

 
	EXEC (@sSelect)
	SET ROWCOUNT 0

	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_ViewItem_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
