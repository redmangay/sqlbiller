﻿

 
CREATE PROCEDURE   [dbo].[ets_Table_Update]
(
	@TableID int,
	@TableName nvarchar(255),
	@bIsActive bit ,@iAccountID int=NULL,
	@sPinImage varchar(255)=NULL,
	@dMaxTimeBetweenRecords decimal(20,10)=NULL,
	@sMaxTimeBetweenRecordsUnit varchar(50)=NULL,
 	@nLastUpdatedUserID int =NULL,
 	@nLateDataDays int=NULL,
	@sLateDataUnit varchar(50)=NULL,
 	@sDateFormat varchar(30)='DD/MM/YYYY',
 	@bFlashAlerts bit =NULL,
 	@nFilterColumnID int=NULL,
 	@sFilterDefaultValue varchar(MAX)=NULL,
 	@sReasonChangeType varchar(20)=NULL,
 	@sChangeHistoryType varchar(20)=NULL,
 	@bAddWithoutLogin bit=NULL,
 	@nParentTableID int=NULL,
 	@bAddUserRecord bit=NULL,
 	@nAddUserUserColumnID int =NULL,
 	@nAddUserPasswordColumnID int =NULL,
 	@bAddUserNotification bit =NULL,
 	@nSortColumnID int =NULL,
 	@sHeaderName varchar(MAX) =NULL,
 	@bHideAdvancedOption bit=NULL,
 	@nValidateColumnID1 int=NULL,
 	@nValidateColumnID2 int=NULL,
 	@sHeaderColor varchar(6) =NULL,
 	@sJSONAttachmentPOP3 varchar(MAX)= NULL,
 	@sJSONAttachmentInfo varchar(MAX)= NULL,
 	@bShowTabVertically bit=NULL,
 	@bCopyToChildrenAfterImport bit=NULL,
 	@sCustomUploadSheet varchar(MAX)=NULL,
 	@sFilterType varchar(MAX)=NULL,
	@sTabColour varchar(6) =NULL,
	@sTabTextColour varchar(6) =NULL,
	@bBoxAroundField bit =NULL,
	@sFilterTopColour varchar(6) =NULL,
	@sFilterBottomColour varchar(6) =NULL,
	@bShowEditAfterAdd bit=NULL,
	@bHideFilter bit=NULL,
	@bSaveAndAdd bit=NULL,
	@bNavigationArrows bit=1,
	@nGraphXAxisColumnID int = NULL,
	@nGraphSeriesColumnID int = NULL,
	@nGraphDefaultPeriod int = NULL,
	@nDataUpdateUniqueColumnID int=NULL,
	@bAllowCopyRecords bit=NULL, @bShowSentEmails bit=NULL, @bShowReceivedEmails bit=NULL,
	@nUniqueColumnID int=NULL, @nUniqueColumnID2 int=NULL, @bIsDataUpdateAllowed bit=NULL,
	@nPinDisplayOrder int=NULL,
	@sDistanceRings varchar(100) = NULL,
	@bShowOnStart bit = NULL,
	@GraphOnStart varchar(25) =NULL,@GraphDefaultYAxisColumnID INT =NULL,
	@SummaryPageContent varchar(MAX) =NULL,
	@DefaultImportTemplateID int=NULL,
	@sDuplicateRecordAction varchar(50)=NULL,
	@bShowChildTabsOnAdd bit=NULL,
	@WarningMinPeriod decimal(10,2)= NULL,
	@ExceedanceMinPeriod decimal(10,2)= NULL,
	@FlatDataMinPeriod decimal(10,2)= NULL,
	--,@sSPSearchGo varchar(127),@sSPSearchReset varchar(127)
	@DefaultExportTemplateID int=NULL,@LastEmailWarning datetime=NULL, 
	@LastEmailExceedance datetime=NULL, @LastEmailFlatData datetime=NULL,
	@sDeleteAction varchar(100)=NULL,
	@sCustomPreventDeleteNotification varchar(MAX)=NULL,	
	@sPreventDeleteFilterInfo varchar(MAX)=NULL,
	@sPreventDeleteFilter varchar(MAX)=NULL,
	@CopyDataWithTemplate bit=NULL,
	@nArchiveMonts int = NULL,
	@sArchiveDatabase varchar(MAX)=NULL,
	@nArchiveDateColumnID int=NULL,
	@GraphMissingValues varchar(10)=NULL,
	@GraphSpecialCharacters varchar(10)=NULL,@PinImageAdvanced varchar (MAX) =NULL ,@DefaultTableGraphOptionID int=null
)
AS
BEGIN TRY
	--UPDATE Menu SET ParentMenuID=@MenuID WHERE TableID=@TableID
	DECLARE @bUpdateAllUniqueKey bit
	IF @nUniqueColumnID IS NOT NULL
		BEGIN
			DECLARE @nUniqueColumnID_old INT,@nUniqueColumnID2_OLD INT
			SELECT @nUniqueColumnID_old=UniqueColumnID,@nUniqueColumnID2_OLD=UniqueColumnID2 FROM [Table]	
				WHERE TableID=@TableID
			IF @nUniqueColumnID_old IS NULL OR @nUniqueColumnID_old<>@nUniqueColumnID
				SET @bUpdateAllUniqueKey=1
			
			IF (@nUniqueColumnID2_OLD IS NULL AND @nUniqueColumnID2 IS NOT NULL) OR (@nUniqueColumnID2 IS NULL AND @nUniqueColumnID2_OLD IS NOT NULL)
				OR (@nUniqueColumnID2_OLD<>@nUniqueColumnID2)
					SET @bUpdateAllUniqueKey=1

		END

	UPDATE [Table] SET
		[TableName] = @TableName,
		IsActive=@bIsActive,
		AccountID=@iAccountID,
		PinImage=@sPinImage,
		MaxTimeBetweenRecords=@dMaxTimeBetweenRecords,
		MaxTimeBetweenRecordsUnit=@sMaxTimeBetweenRecordsUnit,
		LastUpdatedUserID=@nLastUpdatedUserID,
		LateDataDays=@nLateDataDays,
		LateDataUnit=@sLateDataUnit,
		[DateFormat]=@sDateFormat,
		FlashAlerts=@bFlashAlerts,
		FilterColumnID=@nFilterColumnID,
		FilterDefaultValue=@sFilterDefaultValue,
		ReasonChangeType=@sReasonChangeType,
		ChangeHistoryType=@sChangeHistoryType,
		AddWithoutLogin=@bAddWithoutLogin,
		ParentTableID=@nParentTableID,
		AddUserRecord=@bAddUserRecord,
		AddUserUserColumnID=@nAddUserUserColumnID,
		AddUserPasswordColumnID=@nAddUserPasswordColumnID,
		AddUserNotification=@bAddUserNotification,
		SortColumnID=@nSortColumnID,
		HeaderName=@sHeaderName,
		HideAdvancedOption=@bHideAdvancedOption,
		ValidateColumnID1=@nValidateColumnID1,
		ValidateColumnID2=@nValidateColumnID2,
		HeaderColor=@sHeaderColor,
		JSONAttachmentPOP3=@sJSONAttachmentPOP3,
		JSONAttachmentInfo=@sJSONAttachmentInfo,
		ShowTabVertically=@bShowTabVertically,
		CopyToChildrenAfterImport=@bCopyToChildrenAfterImport,
		CustomUploadSheet=@sCustomUploadSheet,
		FilterType=@sFilterType,
		TabColour=@sTabColour,
		TabTextColour=@sTabTextColour,
		BoxAroundField=@bBoxAroundField,
		FilterTopColour=@sFilterTopColour,
		FilterBottomColour=@sFilterBottomColour,
		ShowEditAfterAdd=@bShowEditAfterAdd,
		HideFilter=@bHideFilter,
		SaveAndAdd=@bSaveAndAdd,
		NavigationArrows=@bNavigationArrows,
		GraphXAxisColumnID = @nGraphXAxisColumnID,
		GraphSeriesColumnID = @nGraphSeriesColumnID,
		GraphDefaultPeriod = @nGraphDefaultPeriod,
		DataUpdateUniqueColumnID=@nDataUpdateUniqueColumnID,
		AllowCopyRecords=@bAllowCopyRecords,
		ShowSentEmails=@bShowSentEmails,
		ShowReceivedEmails=@bShowReceivedEmails,
		UniqueColumnID=@nUniqueColumnID,
		UniqueColumnID2=@nUniqueColumnID2,
		IsDataUpdateAllowed=@bIsDataUpdateAllowed,
		PinDisplayOrder=@nPinDisplayOrder,
		MapCircleDiameters=@sDistanceRings,
		ShowCircleOnStart=@bShowOnStart,
		GraphOnStart=@GraphOnStart,GraphDefaultYAxisColumnID=@GraphDefaultYAxisColumnID,
		SummaryPageContent=@SummaryPageContent,
		DefaultImportTemplateID=@DefaultImportTemplateID,
		DuplicateRecordAction=@sDuplicateRecordAction,
		ShowChildTabsOnAdd=@bShowChildTabsOnAdd,
		WarningMinPeriod=@WarningMinPeriod,
		ExceedanceMinPeriod=@ExceedanceMinPeriod,
		FlatDataMinPeriod=@FlatDataMinPeriod,
		DefaultExportTemplateID=@DefaultExportTemplateID,
		LastEmailWarning=@LastEmailWarning,
		LastEmailExceedance=@LastEmailExceedance,
		LastEmailFlatData=@LastEmailFlatData,
		DeleteAction = @sDeleteAction,
		CustomPreventDeleteNotification = @sCustomPreventDeleteNotification,		
		PreventDeleteFilterInfo = @sPreventDeleteFilterInfo,
		PreventDeleteFilter = @sPreventDeleteFilter,
		CopyDataWithTemplate=@CopyDataWithTemplate,
		ArchiveMonths = @nArchiveMonts,
		ArchiveDatabase = @sArchiveDatabase,
		ArchiveDateColumnID = @nArchiveDateColumnID,
		GraphMissingValues = @GraphMissingValues,
		GraphSpecialCharacters = @GraphSpecialCharacters,
		PinImageAdvanced=@PinImageAdvanced,
		DefaultTableGraphOptionID=@DefaultTableGraphOptionID

		--SPSearchGo=@sSPSearchGo,
		--SPSearchReset=@sSPSearchReset
	WHERE TableID = @TableID

	IF @bUpdateAllUniqueKey=1
		EXEC [Record_Set_UniqueKey] @sUpdateRecord='R', @nTableID=@TableID

	PRINT 'Done'
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog]([Module], [ErrorMessage], [ErrorTrack], [ErrorTime], [Path]) 
		VALUES ('ets_Table_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
