﻿CREATE PROCEDURE [dbo].[rptStatements] 
	 @GUID varchar(max)
	,@IncludeBadDebts bit
AS
BEGIN
	
declare @dCutOffDate datetime

declare @maxcount int
declare @iter int
declare @rowscount int

declare @maxcountForDelete int
declare @iterForDelete int
declare @rowscountForDelete int

declare @maxcountForProjectNumber int
declare @iterForProjectNumber int


declare @ProjectTitle varchar(max)
declare @AttentionTo varchar(max)
declare @Company varchar(max)
declare @Address varchar(max)
declare @JobValueDescription varchar(max)
declare @Special varchar(max)
declare @TaxName varchar(max)
declare @CurrencySymbol varchar(max)
declare @FixedPrice varchar(max)
declare @Amount varchar(max)

IF OBJECT_ID('tempdb..#tempRptPrintBillingAndPaymentActivity') IS NOT NULL
begin
        drop table #tempRptPrintBillingAndPaymentActivity
end
IF OBJECT_ID('tempdb..#tempTableForBillingInformation') IS NOT NULL
begin
        drop table #tempTableForBillingInformation
end

CREATE TABLE #tempRptPrintBillingAndPaymentActivity (
								[GUID] varchar(max),
                                ProjectNumber varchar(max),
                                BilledNote varchar(max),
                                BilledDate DateTime,
                                InvoiceAmount Money,
                                InvoiceAmount_Converted Money,
                                PaymentAmount Money,
                                PaymentAmountConverted Money,
                                [Description] varchar(max),
                                PercentageComplete varchar(max),
                                InvoiceNo varchar(max),
                                TaxName varchar(max),
                                TaxRate varchar(max),
                                TaxAmount Money,
                                CreatedDate varchar(max),
                                ProjectTitle varchar(max), 
        AttentionTo varchar(max),
        Company varchar(max), 
        [Address] varchar(max), 
        JobValueDescription varchar(max), 
        CurrencySymbol varchar(max),Special varchar(max),FixedPrice varchar(max),Amount money
                               )
                                                      
IF @IncludeBadDebts=0                         
INSERT INTO #tempRptPrintBillingAndPaymentActivity([GUID],
ProjectNumber,
BilledNote,
BilledDate,
InvoiceAmount,
InvoiceAmount_Converted,
PaymentAmount,
PaymentAmountConverted,
[Description],
PercentageComplete,
InvoiceNo,
TaxName,
TaxRate,
TaxAmount,
CreatedDate,
Company)

		(SELECT @GUID as [GUID], Account24918.[vBilling Information].[Project Number] as ProjectNumber, 
		(SELECT [Value] FROM Account24918.vDocumentSetting WHERE [Name]='Statement Details') as BilledNote, 
							CONVERT(datetime,	Account24918.[vBilling Information].[Date],103) AS BilledDate,Account24918.[vBilling Information].[Invoice Amount] as InvoiceAmount, 
								Account24918.[vBilling Information].[Invoice Amount Converted]  as InvoiceAmount_Converted, 
							   Account24918.[vBilling Information].[Payment Amount] as PaymentAmount, 
								Account24918.[vBilling Information].[Payment Amount Converted] as PaymentAmountConverted,
								Account24918.[vBilling Information].[Description], Account24918.[vBilling Information].[%Complete] AS PercentageComplete,
								 Account24918.[vBilling Information].[Invoice No] as InvoiceNo, Account24918.[vBilling Information].[Tax Name] TaxName, 
							  Account24918.[vBilling Information].[Tax Rate] as TaxRate,Account24918.[vBilling Information].[Tax Amount] as TaxAmount, 
							   CONVERT(datetime,	GetDate(),103)   as CreatedDate,Account24918.vProject.Company
							  FROM Account24918.[vBilling Information] INNER JOIN 
							  Account24918.vProject ON Account24918.[vBilling Information].[Project Number] = 
							  Account24918.vProject.[ProjectNumber]
							  WHERE Account24918.vProject.[Status] <> 5 and Account24918.vProject.[Status] <> 6)    
else
INSERT INTO #tempRptPrintBillingAndPaymentActivity([GUID],
ProjectNumber,
BilledNote,
BilledDate,
InvoiceAmount,
InvoiceAmount_Converted,
PaymentAmount,
PaymentAmountConverted,
[Description],
PercentageComplete,
InvoiceNo,
TaxName,
TaxRate,
TaxAmount,
CreatedDate)

		(SELECT '2121221212' as [GUID], Account24918.[vBilling Information].[Project Number] as ProjectNumber, 
		(SELECT [Value] FROM Account24918.vDocumentSetting WHERE [Name]='Statement Details') as BilledNote, 
							CONVERT(datetime,Account24918.[vBilling Information].[Date],103) AS BilledDate,Account24918.[vBilling Information].[Invoice Amount] as InvoiceAmount, 
								Account24918.[vBilling Information].[Invoice Amount Converted]  as InvoiceAmount_Converted, 
							   Account24918.[vBilling Information].[Payment Amount] as PaymentAmount, 
								Account24918.[vBilling Information].[Payment Amount Converted] as PaymentAmountConverted,
								Account24918.[vBilling Information].[Description], Account24918.[vBilling Information].[%Complete] AS PercentageComplete,
								 Account24918.[vBilling Information].[Invoice No] as InvoiceNo, Account24918.[vBilling Information].[Tax Name] TaxName, 
							  Account24918.[vBilling Information].[Tax Rate] as TaxRate,Account24918.[vBilling Information].[Tax Amount] as TaxAmount, 
							  CONVERT(datetime, GetDate(),103) as CreatedDate
							  FROM Account24918.[vBilling Information] INNER JOIN 
							  Account24918.vProject ON Account24918.[vBilling Information].[Project Number] = 
							  Account24918.vProject.[ProjectNumber]
							  WHERE Account24918.vProject.[Status] <> 5)
   
	              
	              
	              
	              
	              
	              
SELECT rowscount = row_number() over (order by b.[Project Number]), b.[Project Number] AS ProjectNumber, Sum(Isnull(b.[Payment Amount Converted],0)) AS SumOfPaymentAmount_Converted, p.[Status]
into #tempTableForBillingInformation
    FROM Account24918.[vBilling Information] b LEFT JOIN Account24918.[vProject] p ON b.[Project Number] = p.ProjectNumber 
    GROUP BY b.[Project Number], p.[Status]
    HAVING (((p.[Status])<>5));
    
set @maxcount= (Select Max(Rowscount) From #tempTableForBillingInformation)
set @iter =(Select min(Rowscount) From #tempTableForBillingInformation)  

while @iter<=@maxcount

begin
IF OBJECT_ID('tempdb..#tempTableForDeleteRecord') IS NOT NULL
begin
        drop table #tempTableForDeleteRecord
end

	SELECT rowscountForDelete = row_number() over (order by [ProjectNumber]),ProjectNumber,
    Sum(CAST(ISNULL(InvoiceAmount_Converted,0) AS INT)) AS SumOfInvoiceAmount_Converted 
    into #tempTableForDeleteRecord
    FROM #tempRptPrintBillingAndPaymentActivity
    WHERE 
    [GUID] = @GUID AND ProjectNumber = (select ProjectNumber from #tempTableForBillingInformation where rowscount=@iter)
    AND [BilledDate] < (select DateAdd(Day, -(SELECT CAST(NumberOfDays AS INT) FROM Account24918.vTradingTerm) , GetDate()) AS cDate)
    Group By ProjectNumber

	set @maxcountForDelete= (Select Max(rowscountForDelete) From #tempTableForDeleteRecord)
	set @iterForDelete =(Select min(rowscountForDelete) From #tempTableForDeleteRecord)

		IF isnull(@maxcountForDelete,0)>0 
		Begin			
					IF (select Isnull(SumOfInvoiceAmount_Converted,0) from #tempTableForDeleteRecord where rowscountForDelete=@iterForDelete)-(select Isnull(SumOfPaymentAmount_Converted,0) from #tempTableForBillingInformation where rowscount=@iter)<=0
							DELETE FROM #tempRptPrintBillingAndPaymentActivity WHERE [GUID] = @GUID AND ProjectNumber = (select ProjectNumber from #tempTableForBillingInformation where Rowscount=@iter)		
		END
	ELSE
		DELETE FROM #tempRptPrintBillingAndPaymentActivity WHERE [GUID] = @GUID AND ProjectNumber = (select ProjectNumber from #tempTableForBillingInformation where rowscount=@iter)

set @iter=@iter +1
SET @iterForDelete=@iterForDelete+1

end

IF OBJECT_ID('tempdb..#tempForProjectNumber') IS NOT NULL
begin
        drop table #tempForProjectNumber
end

SELECT DISTINCT ProjectNumber,rowscountForProjectNumber = row_number() over (order by ProjectNumber) into #tempForProjectNumber FROM #tempRptPrintBillingAndPaymentActivity  WHERE [GUID] =@Guid

set @maxcountForProjectNumber= (Select Max(rowscountForProjectNumber) From #tempForProjectNumber)
set @iterForProjectNumber= (Select Min(rowscountForProjectNumber) From #tempForProjectNumber)

while @iterForProjectNumber<=@maxcountForProjectNumber
begin

	SELECT 
	@ProjectTitle=ProjectTitle,
	@AttentionTo= 'Attention: '+ Name +' '+ SumName,
	@Company=Company,
	@Address=  
	isnull(Company,'') + CHAR(10) + LTRIM(RTRIM(isnull(Address1,'')))+ CHAR(10) +
	LTRIM(RTRIM(isnull(Address2,''))) + CHAR(10) +Suburb + CHAR(10) +
	isnull([State],'') + CHAR(10) +  isnull(PostCode,'') + CHAR(10),
	@CurrencySymbol=(SELECT TOP 1 IsNull(Symbol,'$') FROM Account24918.[vCurrency Rates] WHERE Account24918.[vCurrency Rates].Currency =Currency),
  
       @Special=Special,
       @FixedPrice=FixedPrice,
       @TaxName=TaxName,
       @Amount=Value
    
	FROM Account24918.vProject 
	WHERE ProjectNumber =(select ProjectNumber from #tempForProjectNumber where rowscountForProjectNumber=@iterForProjectNumber)

	 Update #tempRptPrintBillingAndPaymentActivity 
        SET ProjectTitle= @ProjectTitle , 
        AttentionTo= @AttentionTo, 
        Company= @Company, 
        Address= @Address, 
        CurrencySymbol= @CurrencySymbol,
        Special=@Special,
        FixedPrice=@FixedPrice,
        TaxName=@TaxName,
        Amount=@Amount
        WHERE GUID =@Guid AND ProjectNumber =(select ProjectNumber from #tempForProjectNumber where rowscountForProjectNumber=@iterForProjectNumber)
set @iterForProjectNumber=@iterForProjectNumber+1
end             

                                     

  select * from #tempRptPrintBillingAndPaymentActivity
  
                   
	
END