﻿

CREATE PROCEDURE [dbo].[UserRole_Update]
(
	@nUserRoleID int ,
	@nUserID int ,
	@nRoleID int,@nAccountID int,
	@bIsAdvancedSecurity bit =0,
	@dateAlertSeen datetime=NULL,
	@bIsDocSecurityAdvanced bit=NULL,
	@sDocSecurityType varchar(20)=NULL,
	@nDashBoardDocumentID int=NULL,
	@nDataScopeColumnID int=NULL,
	@sDataScopeValue nvarchar(500) =NULL,
	@AllowDeleteTable bit =NULL,
	@AllowDeleteColumn bit =NULL,
	@AllowDeleteRecord bit= NULL,
	@isPrimaryAccount bit = NULL
)
 
AS
BEGIN TRY
	--KG 30/11/17 Ticket 3374 - Make sure no other userrole for this user is set to primary
	IF (@isPrimaryAccount = 1)
		UPDATE [UserRole]
			SET IsPrimaryAccount = 0
		WHERE UserID = @nUserID AND IsPrimaryAccount = 1

	UPDATE [UserRole]
	SET 
		UserID = @nUserID,
		RoleID = @nRoleID,
		AccountID=@nAccountID,
		IsAdvancedSecurity=@bIsAdvancedSecurity,
		AlertSeen=@dateAlertSeen,
		IsDocSecurityAdvanced=@bIsDocSecurityAdvanced,
		DocSecurityType=@sDocSecurityType,
		DashBoardDocumentID=@nDashBoardDocumentID,
		DataScopeColumnID=@nDataScopeColumnID,
		DataScopeValue=@sDataScopeValue,
		AllowDeleteTable=@AllowDeleteTable,
		AllowDeleteColumn=@AllowDeleteColumn,
		AllowDeleteRecord=@AllowDeleteRecord,
		IsPrimaryAccount=@isPrimaryAccount
	WHERE UserRoleID = @nUserRoleID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('UserRole_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
