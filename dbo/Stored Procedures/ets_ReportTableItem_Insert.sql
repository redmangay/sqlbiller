﻿

CREATE PROCEDURE [dbo].[ets_ReportTableItem_Insert]
(
	@nNewID int output,
	@nReportItemID int,
	@nColumnID int,
	@sColumnTitle varchar(1000) = NULL
)
AS

BEGIN TRY
	DECLARE @nPosiiton int
	SELECT @nPosiiton = MAX(ColumnPosition) FROM [ReportTableItem] WHERE [ReportItemID] = @nReportItemID
	INSERT INTO [ReportTableItem]
	(
		ReportItemID, 
		ColumnID, 
		ColumnTitle,
		ColumnPosition
	) VALUES (
		@nReportItemID,
		@nColumnID,
		@sColumnTitle,
		ISNULL(@nPosiiton, -1) + 1
	)
	SELECT @nNewID = @@IDENTITY
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_ReportTableItem_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
