﻿CREATE PROCEDURE [dbo].[renzo_SaveModifyTimeSheetDetails]
(
    @RecordID  INT,
    @FormSetID INT          = NULL,
    @UserID    INT          = NULL,
    @Context varchar(max) = null,
    @UpdatedColumn int = NULL,
    @Return    VARCHAR(MAX) OUTPUT
)
AS
         BEGIN

	
 --    EXEC [dbo].[renzo_SaveModifyTimeSheetDetails]  @RecordID=   15958793, @Return=''
             --DECLARE @ColumnID INT;
             --DECLARE @TableID INT;
             DECLARE @ColumnIDStaus BIT;
             --SELECT @TableID = [TableID]
             --FROM [Record]
             --WHERE [RecordID] = @RecordID;
             SELECT @ColumnIDStaus = [IsReadOnly]
             FROM [Column]
             WHERE [ColumnID] = 49888;


/* get the edit column */
DECLARE @SystemName varchar(50)

SELECT @SystemName = SystemName FROM [Column]
    WHERE ColumnID = @UpdatedColumn

-- lets get the Tax Amount and Currency Rate
                     DECLARE @TaxRate VARCHAR(MAX);
				 DECLARE @CurrencyRate VARCHAR(MAX);
                     SELECT @CurrencyRate = isnull(V007,1), @TaxRate = (isnull([V011],0) / 100.00)
                     FROM [RECORD]
                     WHERE [RECORDID] =
(
    SELECT [V001]
    FROM [RECORD]
    WHERE [RECORDID] = @RecordID
);
                  --   PRINT @TaxRate;

--lets disable timesheet
                     DECLARE @RecordIDToUpdate_TimeSheet INT;
                     DECLARE @RecordIDToUpdate_Disbursement INT;

--Lets get RecordIDs for Disbursement and Timesheet
                     SELECT @RecordIDToUpdate_TimeSheet = [V011],
                            @RecordIDToUpdate_Disbursement = [V010]
                     FROM [RECORD]
                     WHERE [RecordID] = @RecordID;	


				 DECLARE @IsXChanged bit = 0
				 DECLARE @IsHourChanged bit = 0
				 DECLARE @IsRateChanged bit = 0

	

-- Timesheet
IF @RecordIDToUpdate_TimeSheet IS NOT NULL
BEGIN
						
/* Lets Restore the figures */
IF (@Context = 'M')
BEGIN

 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Red Test - M', 'Restore', 'Restore', GETDATE(), 'Stored Procedure on ' + DB_NAME())



/* lets check the changes */
--IF NOT EXISTS (
--		  SELECT [TimeSheetDetails].[V011]
--			 FROM [RECORD] [TimeSheetDetails]
--				INNER JOIN [RECORD] [TimeSheet] ON [TimeSheet].[TableID] = 2488
--				    AND [TimeSheet].[IsActive] = 1
--				    AND CAST([TimeSheet].[V007] AS MONEY) = CAST([TimeSheetDetails].[V004] AS MONEY) /* changed X */
--				    AND [TimeSheet].[V018] = [TimeSheetDetails].[V001]
--				    AND CAST([TimeSheet].[RecordID] AS VARCHAR) = [TimeSheetDetails].[V011]
--			 WHERE [TimeSheetDetails].[TableID] = 2729
--				  AND [TimeSheetDetails].[IsActive] = 1
--				  AND [TimeSheetDetails].[RecordID] = @RecordID
--				  AND [TimeSheetDetails].[V022] = '1'
--) -- Status as ByRate 
IF (@SystemName = 'V004')
BEGIN
    SET @IsXChanged = 1
END

--IF NOT EXISTS (
--	   SELECT [TimeSheetDetails].[V011]
--	   FROM [RECORD] [TimeSheetDetails]
--		  INNER JOIN [RECORD] [TimeSheet] ON [TimeSheet].[TableID] = 2488
--                AND [TimeSheet].[IsActive] = 1
--                AND CAST([TimeSheet].[V015] AS MONEY) = CAST([TimeSheetDetails].[V005] AS MONEY) /* changed Hour */
--                AND [TimeSheet].[V018] = [TimeSheetDetails].[V001]
--                AND CAST([TimeSheet].[RecordID] AS VARCHAR) = [TimeSheetDetails].[V011]
--		  WHERE [TimeSheetDetails].[TableID] = 2729
--			   AND [TimeSheetDetails].[IsActive] = 1
--			   AND [TimeSheetDetails].[RecordID] = @RecordID
--			   AND [TimeSheetDetails].[V022] = '1'
--) -- Status as ByRate 

IF (@SystemName = 'V005')
BEGIN
    SET @IsHourChanged = 1
END

--IF NOT EXISTS (
--	   SELECT [TimeSheetDetails].[V011]
--		  FROM [RECORD] [TimeSheetDetails]
--			 INNER JOIN [RECORD] [TimeSheet] ON [TimeSheet].[TableID] = 2488
--			 AND [TimeSheet].[IsActive] = 1
--			 AND CAST([TimeSheet].[V035] AS MONEY) = CAST([TimeSheetDetails].[V006] AS MONEY) /* changed Rate */
--			 AND [TimeSheet].[V018] = [TimeSheetDetails].[V001]
--			 AND CAST([TimeSheet].[RecordID] AS VARCHAR) = [TimeSheetDetails].[V011]
--	   WHERE [TimeSheetDetails].[TableID] = 2729
--	   AND [TimeSheetDetails].[IsActive] = 1
--	   AND [TimeSheetDetails].[RecordID] = @RecordID
--	   AND [TimeSheetDetails].[V022] = '1'
--) -- Status as ByRate 

IF (@SystemName = 'V006')
BEGIN						   
    SET @IsRateChanged = 1
END

/* end checking */

						   
IF (@IsXChanged = 1 OR @IsHourChanged = 1 OR @IsRateChanged = 1 ) -- Status as ByRate 
BEGIN
    UPDATE [Record]
    SET
    [IsReadOnly] = 1
    WHERE [RecordID] = @RecordIDToUpdate_TimeSheet;

    IF (@IsRateChanged = 1) 
    BEGIN
	   UPDATE [Record]
	   SET
	   [V006] = Round(cast([V006] AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)),2) /*	[Rate]	*/ 
	   WHERE [RecordID] = @RecordID;
    END

    UPDATE [Record]
    SET
    [V022] = '7'
    ,V007 = Round(cast([V004] AS DEC(10,2)) * cast([V005] AS DEC(10,2)) * cast(V006 AS DEC(10,2)),2)
    WHERE [RecordID] = @RecordID;

END
ELSE  IF (@IsXChanged = 0 AND @IsHourChanged = 0 AND @IsRateChanged = 0 ) 
BEGIN
    UPDATE [Record]
    SET [IsReadOnly] = 0
    WHERE [RecordID] = @RecordIDToUpdate_TimeSheet;
END

						   --
DECLARE @NewTimeStatus as varchar(max)
DECLARE @OldTimeStatus as varchar(max)

SELECT @NewTimeStatus = CASE WHEN [V022] = '1' THEN 'By Rate' WHEN [V022] = '2' THEN 'Fixed' WHEN [V022] = '3' THEN 'Hold' WHEN [V022] = '4' THEN 'No Charge' WHEN [V022] = '5' THEN 'Write-Off' WHEN [V022] = '6' THEN 'Hold Special' ELSE 'Adjusted'  END
,@OldTimeStatus = V032
FROM Record
WHERE RecordID = @RecordID

-- IF (@NewTimeStatus <> @OldTimeStatus AND ((@NewTimeStatus = 'By Rate') OR (@NewTimeStatus = 'Fixed')) OR )
IF (@NewTimeStatus <> 'Adjusted')
BEGIN
	
    UPDATE [Timesheet Details]
    SET DateUpdated = getdate(),	
	   LastUpdatedUserId = @UserID,
	   [V004] = [Timesheet].V007,				/*	[(X)]	*/
	   [V005] = [Timesheet].V015,				/*	[Hr/Unit]	*/
	   [V006] = Round(cast([Timesheet].V009 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)),2), /*	[Rate]	*/
	   [V007] = Round(cast([Timesheet].V009 AS money) * cast(@CurrencyRate AS DEC(10,2)) * cast([Timesheet].V007 AS DEC(10,2))	* cast([Timesheet].V015 AS DEC(10,2)),2)			/*	[$ Amount]	*/	
    FROM		[Record] [Timesheet Details]
				JOIN		Record [Timesheet] ON				[Timesheet].TableID = '2488'		/*	Timesheet	*/
				AND		[Timesheet].IsActive = 1
				AND		CAST( [Timesheet].RecordID AS VARCHAR(MAX)) = [Timesheet Details].V011		/*	V011	*/
    WHERE		[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
    AND			[Timesheet Details].RecordID = @RecordID
    AND			[Timesheet Details].IsActive = 1

END
							 
END /* End Context "M" */
--ELSE /* Start not Context "M" */
--BEGIN

DECLARE @TimeSheetStatus varchar(10) 
SELECT @TimeSheetStatus= [V022] FROM [RECORD]WHERE [RECORDID] = @RecordID

IF (SELECT [V022] FROM [RECORD]WHERE [RECORDID] = @RecordID ) = '0' OR
  ( SELECT [V022] FROM [RECORD] WHERE [RECORDID] = @RecordID ) = '1' OR
   ( SELECT [V022] FROM [RECORD] WHERE [RECORDID] = @RecordID ) = '2'
BEGIN

DECLARE @Status varchar(max) 
DECLARE @TrueStatus varchar(max) = '1'

SELECT @Status = [V022] FROM [RECORD]WHERE [RECORDID] = @RecordID
SET @TimeSheetStatus = @Status;
PRINT 'RED'
IF (@Status = '2') 
BEGIN
SET @TrueStatus = '2'
END 
 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Red Test', 'Restore', 'Restore', GETDATE(), 'Stored Procedure on ' + DB_NAME())

    UPDATE [Timesheet Details]
    SET
	   [V012] = CAST(CAST(REPLACE([Timesheet Details].[V007], ',', '') AS DEC(20, 4))* @TaxRate AS VARCHAR(MAX))	,	/*	[GST]	*/	
	   DateUpdated = getdate(),	
	   LastUpdatedUserId = @UserID,
	   [V004] = [Timesheet].V007,				/*	[(X)]	*/
	   [V005] = [Timesheet].V015,				/*	[Hr/Unit]	*/
	   [V006] = [Timesheet].V035,				/*	[Rate]	*/--[V006] = Round(cast([Timesheet].V009 AS DEC(10,2)) * cast(ISNULL(@CurrencyRate,1) AS DEC(10,2)),2) ,				/*	[Rate]	*/
	   [V007] = [Timesheet].V019			     /*	[Amount]	*/
	   ,[V022] = @TrueStatus
    FROM		[Record] [Timesheet Details]
				    JOIN		Record [Timesheet] ON				[Timesheet].TableID = '2488'		/*	Timesheet	*/
				    AND		[Timesheet].IsActive = 1
				    AND		CAST( [Timesheet].RecordID AS VARCHAR(MAX)) = [Timesheet Details].V011		/*	V011	*/
												
    WHERE		[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
    AND			[Timesheet Details].RecordID = @RecordID
    AND			[Timesheet Details].IsActive = 1
END
ELSE
BEGIN
	   IF ( SELECT [V022] FROM [RECORD] WHERE [RECORDID] = @RecordID ) = '5' OR
		 ( SELECT [V022] FROM [RECORD] WHERE [RECORDID] = @RecordID ) = '4' OR
		 ( SELECT [V022] FROM [RECORD] WHERE [RECORDID] = @RecordID ) = '3'
	   BEGIN
		  PRINT 'RED2'
		   INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
				  VALUES ('Red Test -  Hold', 'Restore', 'Restore', GETDATE(), 'Stored Procedure on ' + DB_NAME())




			 UPDATE [RECORD]
			 SET
			 LastUpdatedUserId = @UserID			
			 ,DateUpdated = getdate()
			 ,[V007] = 0.00,
			 [V006] = Round(cast([V040] AS DEC(10,2)) * cast(isnull(@CurrencyRate,1) AS DEC(10,2)),2), /*	[Rate]	*/ 
			 [V012] = NULL
			 WHERE [RECORDID] = @RecordID;

	   END
	   ELSE
	   BEGIN
		  PRINT 'RED3'
		  IF (( SELECT [V022] FROM [RECORD] WHERE [RECORDID] = @RecordID ) = '7' AND @Context <> 'M') -- Adjusted
		  BEGIN
			 --UPDATE [RECORD]
			 --SET LastUpdatedUserId = @UserID			
				--,DateUpdated = getdate()
				--,[V007] = 0.00
				--,[V004] = 0.00
				--,[V005] = 0.00
				--,[V006] = 0.00
				--,[V012] = NULL -- Tax
			 --WHERE [RECORDID] = @RecordID;

			   UPDATE [Record]
				SET
				[V006] = Round(cast([V040] AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)),2) /*	[Rate]	*/ 
				WHERE [RecordID] = @RecordID;

			  UPDATE [Record]
			 SET
			 V007 = Round(cast([V004] AS DEC(10,2)) * cast([V005] AS DEC(10,2)) * cast(V006 AS DEC(10,2)),2)
			 WHERE [RecordID] = @RecordID;
		  END;
	   END;
END;
					  
						 
    /* Update Tax */
    UPDATE [Timesheet Details]
    SET [V007] = Round(cast([Timesheet Details].V004 AS money)  * cast([Timesheet Details].V005 AS money)  * CASE WHEN @TimeSheetStatus = '3' THEN 0 ELSE cast(ISNULL([Timesheet Details].V006,0) AS DEC(10,2)) END	,2)			/*	[$ Amount]	*/		
	   ,[V012] = CAST(CAST(REPLACE([Timesheet Details].[V007], ',', '') AS DEC(20, 4))* @TaxRate AS VARCHAR(MAX))--	,	/*	[GST]	*/	
	   FROM		[Record] [Timesheet Details]
		  WHERE	[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
		  AND	[Timesheet Details].RecordID = @RecordID
		  AND	[Timesheet Details].IsActive = 1

--END 

END; /* End of Timesheet */

/* ========================================================================================================================================  */
/* ========================================================================================================================================  */

-- Disbursement
IF @RecordIDToUpdate_Disbursement IS NOT NULL
BEGIN      

  INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
				  VALUES ('Red Test -  Disbursement', 'Restore', 'Restore', GETDATE(), 'Stored Procedure on ' + DB_NAME())

 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Red Test - Disbursement', 'Restore', 'Restore', GETDATE(), 'Stored Procedure on ' + DB_NAME())

                    
IF ( SELECT [V022] FROM [RECORD] WHERE [RECORDID] = @RecordID ) = '0' OR
( SELECT [V022] FROM [RECORD] WHERE [RECORDID] = @RecordID ) = '1' OR
 ( SELECT [V022] FROM [RECORD] WHERE [RECORDID] = @RecordID ) = '2'
BEGIN
    UPDATE [Timesheet Details] 
    SET LastUpdatedUserId = @UserID			
	   ,DateUpdated = getdate()
	   ,[V022] = '2'
	   ,V004 = NULL							/*	[(X)]	*/
	   ,V005 = [Disbursement].V004				/*	[Hr/Unit]	*/
	   ,V006 = Disbursement.V022 /* Rate */
	   ,V007 = Disbursement.V023 /* Amount */
    FROM		[Record] [Timesheet Details]
    JOIN		Record [Disbursement] ON	[Disbursement].TableID = '2725'		/*	Disbursement	*/
		  AND [Disbursement].IsActive = 1
		  AND CAST( [Disbursement].RecordID AS VARCHAR(MAX)) = [Timesheet Details].V010		/*	V010	*/
    WHERE	[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
    AND	[Timesheet Details].RecordID = @RecordID
    AND	[Timesheet Details].IsActive = 1
END;
ELSE
BEGIN
IF ( SELECT [V022] FROM [RECORD] WHERE [RECORDID] = @RecordID ) = '5' OR
   ( SELECT [V022] FROM [RECORD] WHERE [RECORDID] = @RecordID ) = '4' OR
   ( SELECT [V022] FROM [RECORD] WHERE [RECORDID] = @RecordID ) = '3'
BEGIN
    UPDATE [RECORD]
    SET LastUpdatedUserId = @UserID			
	   ,DateUpdated = getdate()
	   ,[V007] = '0.00',
	   [V012] = '0.00'
    WHERE [RECORDID] = @RecordID;
END;
ELSE
BEGIN
IF (( SELECT [V022] FROM [RECORD] WHERE [RECORDID] = @RecordID ) = '7' AND @Context <> 'M') -- Adjusted
BEGIN
    --UPDATE [RECORD]
    --SET LastUpdatedUserId = @UserID			
	   --,DateUpdated = getdate()
	   --,[V007] = 0.00
	   --,[V004] = 0.00
	   --,[V005] = 0.00
	   --,[V006] = 0.00
	   --,[V012] = NULL -- Tax
    --WHERE [RECORDID] = @RecordID;
     UPDATE [Record]
	   SET
	   [V006] = Round(cast([V040] AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)),2) /*	[Rate]	*/ 
	   WHERE [RecordID] = @RecordID;

     UPDATE [Record]
    SET
    V007 = Round(cast([V005] AS DEC(10,2)) * cast(V006 AS DEC(10,2)),2)
    WHERE [RecordID] = @RecordID;
END;
END;
END;

-- lets update the tax
IF ( SELECT [V022] FROM [RECORD] WHERE [RECORDID] = @RecordID ) = '1' OR
   ( SELECT [V022] FROM [RECORD] WHERE [RECORDID] = @RecordID ) = '2' 
BEGIN

IF (@Context = 'M')
BEGIN

DECLARE @NewDisStatus as varchar(max)
DECLARE @OldDisStatus as varchar(max)

SELECT @NewDisStatus = CASE WHEN [V022] = '1' THEN 'By Rate' WHEN [V022] = '2' THEN 'Fixed' WHEN [V022] = '3' THEN 'Hold' WHEN [V022] = '4' THEN 'No Charge' WHEN [V022] = '5' THEN 'Write-Off' WHEN [V022] = '6' THEN 'Hold Special' ELSE 'Adjusted'  END
,@OldDisStatus = V032
FROM Record
WHERE RecordID = @RecordID

IF (@NewDisStatus <> @OldDisStatus AND ((@NewDisStatus = 'By Rate') OR (@NewDisStatus = 'Fixed')))
BEGIN

-- Update Record
--SET V003 = @CurrencyRate
--WHERE RECORDID = @RecordID


UPDATE [Timesheet Details]
SET DateUpdated = getdate(),	
LastUpdatedUserId = @UserID,
[V004] = NULL,				/*	[(X)]	*/
[V005] = [Disbursement].V004	,				/*	[Hr/Unit]	*/
[V006] = Round(cast(Disbursement.V005 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)),2), /*	[Rate]	*/
[V007] = Round(cast(Disbursement.V005 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)) * cast(Disbursement.V004 AS money),2) /*	[Amount]	*/


FROM		[Record] [Timesheet Details]
JOIN		Record [Disbursement] ON				[Disbursement].TableID = '2725'		/*	Timesheet	*/
AND		[Disbursement].IsActive = 1
AND		CAST( [Disbursement].RecordID AS VARCHAR(MAX)) = [Timesheet Details].V010		/*	V011	*/
												
WHERE		[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
AND			[Timesheet Details].RecordID = @RecordID
AND			[Timesheet Details].IsActive = 1

END

							 
END

UPDATE			[Timesheet Details] 			/*	[Timesheet Details]	*/
SET			
[V007] = Round(cast([Timesheet Details].V005 AS money)  * cast(isnull([Timesheet Details].V006,0) AS DEC(10,2))	,2)			/*	[$ Amount]	*/		
,[V012] = CAST(CAST(REPLACE([Timesheet Details].[V007], ',', '') AS DEC(20, 4))* @TaxRate AS VARCHAR(MAX))		
FROM		[Record] [Timesheet Details]
									 
WHERE		[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
AND			[Timesheet Details].RecordID = @RecordID
AND			[Timesheet Details].IsActive = 1
                                   
								
	   END;





													       IF NOT EXISTS
(
    SELECT [TimeSheetDetails].[V010]
    FROM [RECORD] [TimeSheetDetails]
         INNER JOIN [RECORD] [Disbursement] ON [Disbursement].[TableID] = 2725
                                               AND [Disbursement].[IsActive] = 1
                                               AND CAST([Disbursement].[V004] AS MONEY) = CAST([TimeSheetDetails].[V005] AS MONEY) 

/*	[Hr/Unit]	*/

                                               AND CAST([Disbursement].[V022] AS MONEY) = CAST([TimeSheetDetails].[V006] AS MONEY) 

/*	[Rate]	*/

                                               AND CAST([Disbursement].[V023] AS MONEY) = CAST([TimeSheetDetails].[V007] AS MONEY) 

/*	[$ Amount]	*/

                                               AND [Disbursement].[V001] = [TimeSheetDetails].[V001] 

/*	[Job No]	*/

                                               AND CAST([Disbursement].[RecordID] AS VARCHAR) = [TimeSheetDetails].[V010]
    WHERE [TimeSheetDetails].[TableID] = 2729
          AND [TimeSheetDetails].[IsActive] = 1
          AND [TimeSheetDetails].[RecordID] = @RecordID
          AND [TimeSheetDetails].[V022] = '2'
) -- Status as Fixed 
                                 BEGIN

--lets disable Disbursement
                                     UPDATE [Record]
                                       SET
                                           [IsReadOnly] = 1
                                     WHERE [RecordID] = @RecordIDToUpdate_Disbursement;
                                 END

						   ELSE
						   BEGIN

						     UPDATE [Record]
                                       SET
                                           [IsReadOnly] = 0
                                     WHERE [RecordID] = @RecordIDToUpdate_Disbursement;
						   END

                         END;
            

			-- LETS UPDATE THE TAX
			-- REMOVED THIS TEMP....
			-- RETURNED - RED

			UPDATE			[Timesheet Details] 			/*	[Timesheet Details]	*/
									 SET			[V012] = CAST(CAST(REPLACE([Timesheet Details].[V007], ',', '') AS DEC(20, 4))* @TaxRate AS VARCHAR(MAX))		
									 FROM		[Record] [Timesheet Details]
									 
									 WHERE		[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
									 AND			[Timesheet Details].RecordID = @RecordID
									 AND			[Timesheet Details].IsActive = 1
                                   




			  --LETS UPDATE THE DUMMY
     UPDATE [Timesheet Details]
                                       SET
                                          -- [V012] = CAST(CAST(REPLACE([Timesheet Details].[V007], ',', '') AS DEC(20, 4))* @TaxRate AS VARCHAR(MAX))	,	/*	[GST]	*/	
								   --DateUpdated = getdate(),	
								   --LastUpdatedUserId = @UserID,
								   [V028] = [Timesheet Details].V004,				/*	[(X)]	*/
								   [V029] = [Timesheet Details].V005,				/*	[Hr/Unit]	*/
								   [V030] = [Timesheet Details].V006,				/*	[Rate]	*/
								   [V031] = [Timesheet Details].V007			     /*	[Amount]	*/
								  ,[V032] = CASE WHEN [Timesheet Details].[V022] = '1' THEN 'By Rate' WHEN [Timesheet Details].[V022] = '2' THEN 'Fixed' WHEN [Timesheet Details].[V022] = '3' THEN 'Hold' WHEN [Timesheet Details].[V022] = '4' THEN 'No Charge' WHEN [Timesheet Details].[V022] = '5' THEN 'Write-Off' WHEN [Timesheet Details].[V022] = '6' THEN 'Hold Special' ELSE 'Adjusted'  END
								  ,[V033] = [Job Info].[V001]


                                     	FROM		[Record] [Timesheet Details]
												JOIN	   [Record] [Job Info] on [Job Info].RecordID = [Timesheet Details].V001
												 AND   [Job Info].IsActive = 1

								WHERE		[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
								AND			[Timesheet Details].RecordID = @RecordID
								AND			[Timesheet Details].IsActive = 1

             RETURN @@ERROR;
         END;