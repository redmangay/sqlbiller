﻿

CREATE PROCEDURE [dbo].[tdb_remove_duplicates]
(
	@TableID int, 
	@ExecuteUpdate bit = 0 -- pass 1 to run the UPDATE, 0 to check results
)
AS
/*
	History:
		Written by Jon Bosker on 10 April 2018

	Usage: 
		** Make sure that the table has the unique columns specIFied first **
		Then run this command:
		EXEC tdb_remove_duplicates @TableID = 3218 -- to check the output
		EXEC tdb_remove_duplicates @TableID = 3218, @ExecuteUpdate = 1 -- to run it properly
*/
--
BEGIN TRY 
	SET DATEFORMAT DMY
	DECLARE @SQL_GroupBy varchar(100)

	-- This is a convenience:
	DECLARE @sTableID varchar(MAX) = CAST(@TableID as varchar(MAX))

	-- Check that it is set up for unique records
	IF NOT EXISTS (SELECT * FROM [Table] WHERE [TableID] = @TableID AND UniqueColumnID IS NOT NULL)
		PRINT 'Please set the unique column(s) before running this SP'
	ELSE
	BEGIN
		-- UniqueColumnID2
		IF EXISTS (SELECT * FROM [Table] WHERE [TableID] = @TableID AND UniqueColumnID2 IS NOT NULL)
			SELECT @SQL_GroupBy = C1.SystemName + ',' + C2.SystemName 
				FROM [Table] T
				JOIN [Column] C1 ON C1.ColumnID = T.UniqueColumnID 
				JOIN [Column] C2 ON C2.ColumnID = T.UniqueColumnID2
				WHERE T.[TableID] = @TableID
		ELSE
			SELECT @SQL_GroupBy = C1.SystemName 
				FROM [Table] T
				JOIN [Column] C1 ON C1.ColumnID = T.UniqueColumnID 
				WHERE T.[TableID] = @TableID

		PRINT '@SQL_GroupBy:' + @SQL_GroupBy

		DECLARE @SQL_Update varchar(MAX)
		DECLARE @SQL_Select varchar(MAX)

		SET @SQL_Select = 
			'SELECT ''Duplicates'', * FROM [Record] 
				WHERE TableID = ' + @sTableID + ' 
				AND IsActive = 1 
				AND RecordID IN 
			(SELECT RecordID FROM [Record] WHERE TableID = ' + @sTableID + ' AND IsActive = 1  
					AND RecordID NOT IN (SELECT MAX(RecordID) FROM [Record] WHERE TableID = ' + @sTableID + ' AND IsActive = 1 GROUP BY ' + @SQL_GroupBy + '))'

		PRINT @SQL_Select

		SET @SQL_Update = 
			'UPDATE [Record] SET IsActive = 0,DeleteReason = ''Duplicate removed ' + CAST(GETDATE() as varchar(11)) +'''
				WHERE TableID = ' + @sTableID + ' 
				AND IsActive = 1 
				AND RecordID IN 
			(SELECT RecordID FROM [Record] WHERE TableID = ' + @sTableID + ' AND IsActive = 1  
					AND RecordID NOT IN (SELECT MAX(RecordID) FROM [Record] WHERE TableID = ' + @sTableID + ' AND IsActive = 1 GROUP BY ' + @SQL_GroupBy + '))'

		PRINT @SQL_Update
	
		IF @ExecuteUpdate = 1  
			EXEC (@SQL_Update)
		ELSE 
			EXEC (@SQL_Select)
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('tdb_remove_duplicates', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
