﻿

CREATE PROCEDURE [dbo].[dev_ViewRecord]
(
	@TableID		int,
	@NumberOfRows	int = 100
)
 
AS
BEGIN

	DECLARE @SQL varchar(MAX)

	SELECT	@SQL = 'SELECT	A.AccountName, T.TableName, T.[TableID]'

--PRINT 'STEP 1: ' + @SQL

	SELECT	@SQL = @SQL + 
		',R.' + C.[SystemName] + ' AS [' + C.[DisplayName] + ' (' + C.[SystemName] + ')]
		'
		FROM			[Table] T
		JOIN			[Column] C ON						C.TableID = T.TableID
											AND			(C.IsStandard = 0
											OR			C.SystemName = 'RecordID')
		WHERE			T.TableID = @TableID
		ORDER BY		C.[DisplayOrder]

-- PRINT 'STEP 2: ' + @SQL
-- SUBSTRING(@SQL, 1, LEN(@SQL) - 4) + '
	SELECT @SQL = @SQL + ',R.[DateAdded]
		FROM	[Table] T
		JOIN	[Record] R ON T.TableID = R.TableID 
		JOIN	[Account] A ON A.AccountID = T.AccountID
		WHERE	T.[TableID] = ' + CAST(@TableID as varchar) + '
		AND		R.[IsActive] = 1 '
		
	PRINT			@SQL
	EXEC			(@SQL)

	SELECT @SQL = 'SELECT COUNT(*) FROM [Record] WHERE TableID = ' + CAST(@TableID as varchar) 
	EXEC			(@SQL)

END

