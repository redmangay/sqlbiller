﻿
CREATE PROCEDURE   [dbo].[dbg_View_CreateDefaultViewItems]
(
        @ViewID int	
)
AS

BEGIN TRY
			DECLARE @TableID INT
			SELECT @TableID=TableID FROM [View] WHERE ViewID=@ViewID
			DECLARE @AccountID INT
			SELECT @AccountID=AccountID FROM [Table] WHERE TableID=@TableID
			DECLARE @Alignment varchar(25)

			EXEC [dbo].[SystemOption_ValueByKey_Account] @sOptionKey='View Alignment Default',
			@nAccountID=@AccountID,@nTableID=@TableID,@sOptionValue= @Alignment out

			IF @Alignment IS NULL OR LEN(@Alignment)=0
				SET @Alignment='Auto'

			INSERT INTO [dbo].[ViewItem]
					   ([ViewID]
					   ,[ColumnID]
					   ,[SearchField]
					   ,[FilterField]
					   ,[Alignment]
					   ,[ShowTotal]
					   ,[SummaryCellBackColor]
					   ,[ColumnIndex])   

					  SELECT TOP 5 @ViewID
					   ,ColumnID
					   ,1
					   ,1
					   ,@Alignment
					   ,ShowTotal
					   ,SummaryCellBackColor
					   ,DisplayOrder FROM [Column] WHERE TableID=@TableID
					   AND DisplayTextSummary IS NOT NULL AND LEN(DisplayTextSummary) > 0
								ORDER BY DisplayOrder

		
			-- Reset column index
			EXEC [dbo].[dbg_View_ColumnIndexReset] @ViewID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_View_CreateDefaultViewItems', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
