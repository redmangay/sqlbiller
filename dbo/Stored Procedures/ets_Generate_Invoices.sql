﻿

CREATE PROCEDURE [dbo].[ets_Generate_Invoices]

AS

BEGIN TRY
	INSERT INTO [Invoice]
				([AccountID]
				,[AccountName]
				,[AccountTypeID]
				,[NetAmountAUD]
				,[GSTAmountAUD] 
				,[GrossAmountAUD]
				,[InvoiceDate]
				,[StartDate]
				,[EndDate]
				,[PaymentMethod]
				,[PaypalID]
				,[PaidDate]
				,[Notes]
				,[OrganisationName]
				,[BillingEmail]
				,[BillingAddress]
				,[Country])
		SELECT
			A.[AccountID]
			,A.AccountName
			,A.NextBilledAccountTypeID
			,AT.CostPerMonth * BillEveryXMonths AS [NetAmountAUD] 
			,CASE WHEN GSTApplicable = 1 THEN AT.CostPerMonth * BillEveryXMonths * 0.1 ELSE 0 END AS [GSTAmountAUD] 
						,CASE WHEN GSTApplicable = 1 THEN AT.CostPerMonth * BillEveryXMonths * 1.1 ELSE AT.CostPerMonth * BillEveryXMonths END AS [GrossAmountAUD] 
			,GETDATE()AS [InvoiceDate]
			,GETDATE()AS [StartDate]
			,DATEADD(month, BillEveryXMonths,GETDATE())as [EndDate]
			,A.[PaymentMethod]
			,NULL AS [PaypalID]
			,NULL AS [PaidDate]
			,NULL AS [Notes]
			,A.[OrganisationName]
			,A.[BillingEmail]
			,A.[BillingAddress]
			,C.Value AS Country
					FROM Account A
					JOIN AccountType AT ON AT.AccountTypeID = A.NextBilledAccountTypeID
					JOIN LookupData C ON C.LookupDataID = A.CountryID
					LEFT JOIN Invoice I ON I.AccountID = A.AccountID AND I.StartDate <GETDATE()AND I.EndDate >=GETDATE()
					WHERE A.IsActive = 1
					AND A.NextBilledAccountTypeID != 1 
					AND ExpiryDate >=GETDATE()
					AND I.InvoiceID IS NULL-- i.e.there are no invoices for this period

	-- Reset NextBilledAccountTypeID
	UPDATE Account SET
		NextBilledAccountTypeID = Account.AccountTypeID 
		FROM Account 
		JOIN Invoice ON Invoice.AccountID = Account.AccountID
		WHERE Invoice.[InvoiceDate] =CAST(GETDATE()as date)

	-- PayPal it expires on the last day of the invoice period
	UPDATE Account SET
		ExpiryDate = Invoice.[EndDate]
		FROM Account 
		JOIN Invoice ON Invoice.AccountID = Account.AccountID
		WHERE Invoice.[InvoiceDate] =CAST(GETDATE()as date)
		AND Invoice.[PaymentMethod] ='P'
		
	-- EFT we are going to do manually for now.

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Generate_Invoices', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
