﻿

CREATE PROCEDURE [dbo].[ImportDynamasterBlastData]
(
	@AccountID int ,
	@SampleSiteName nvarchar(200) ,		
	@Event nvarchar(200) ,	
	@EventDateTime nvarchar(200) ,	
	@Overpressure nvarchar(200) ,	
	@Vibration nvarchar(200) ,
	@Status int OUTPUT,
	@BatchID INT ,
	@TableID int = 0,
	@Description nvarchar(200) = NULL
)
/*
DECLARE @Status int;
 EXEC [dbo].[ImportDynamasterBlastData] 24928,'Test site','Event','3/17/2016 4:35','108.00','0.008',@Status out
SELECT A.AccountID, A.AccountName , t.* FROM [Table] t join [Account] a on a.accountid = t.AccountID WHERE TableName LIKE '%new blast%' ORDER BY A.AccountName
SELECT A.AccountID, A.AccountName , t.* FROM [Table] t join [Account] a on a.accountid = t.AccountID WHERE TableName LIKE '%Site%blast%' AND T.IsActive = 1 ORDER BY A.AccountName
EXEC dev_GetTableSQL2 2955	
*/ 

AS

BEGIN TRY 
	DECLARE 
		--@TableID int = 0,
		--@SitesTableID int = 0, -- TDB upload will find the table and site recordid
		--@SampleSiteID int = 0,
		@UserID int=61
	

	SET @Status=0



	SELECT TOP 1 @UserID=[User].UserID
		FROM [User] join UserRole ON  [User].UserID=UserRole.UserID
		 WHERE UserRole.IsAccountHolder=1 
		 AND UserRole.AccountID=@AccountID 

	IF @UserID IS NULL
		SET @UserID=61



	--@SampleSiteID>0 AND 
	IF @TableID>0 AND NOT EXISTS (SELECT RecordID FROM Record WHERE(TableID = @TableID) AND (IsActive = 1) AND (V001 = @SampleSiteName) AND (V002 = @Event))
	BEGIN
		INSERT INTO TempRecord
			(TableID, Notes, AccountID,V001
				,V002 /* [Event Code] */
				,V003 /* [Event Trigger Time] */
				,V004 /* [Overpressure (dB)] */
				,V005 /* [Vibration (mm/s)] */
				,V010 /* [Sample Site] */
				,V012 /* [Description] */
				,BatchID
			)
		VALUES         
			(@TableID,'Imported from Dynamasters portal', @AccountID, @SampleSiteName
				,@Event
				,@EventDateTime
				,@Overpressure
				,@Vibration
				,@SampleSiteName  --@SampleSiteID
				,@Description
				,@BatchID
			)
	END

	SET @Status=@@ROWCOUNT
				 
END TRY

BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ImportDynamasterBlastData', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
