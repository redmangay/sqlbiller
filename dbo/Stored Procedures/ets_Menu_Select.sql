﻿
 
CREATE PROCEDURE [dbo].[ets_Menu_Select]
(
	@nMenuID int  = NULL,
	@sMenu nvarchar(255)  = NULL,
	@nAccountID int  = NULL,
	@bShowOnMenu bit  = NULL,
	@sOrder nvarchar(200) = 'M.DisplayOrder ASC', 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647,
	@bIsActive bit  = 1,
	@nParentMenuID INT= NULL,
	@bWithTable bit=0
)
 
 
 -- EXEC ets_Menu_Select @nAccountID=24800,@bWithTable=1,@nParentMenuID=-1
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nMenuID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND MenuID = '+CAST(@nMenuID AS NVARCHAR)

	IF @bWithTable =0
		SET @sWhere = @sWhere + ' AND M.TableID IS NULL '

	IF @nParentMenuID IS NOT NULL 
	BEGIN
	IF @nParentMenuID =-1
		SET @sWhere = @sWhere + ' AND ParentMenuID IS NULL  '
	IF @nParentMenuID <>-1
		SET @sWhere = @sWhere + ' AND ParentMenuID = '+CAST(@nParentMenuID AS NVARCHAR)

	
	END

	
	IF @sMenu IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Menu LIKE '+'''%' + @sMenu + '%'''
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND M.AccountID = '+CAST(@nAccountID AS NVARCHAR)
	IF @bShowOnMenu IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ShowOnMenu = '+CAST(@bShowOnMenu AS NVARCHAR)

	IF @bIsActive IS NOT NULL 
		SET @sWhere = @sWhere + ' AND M.IsActive = '+CAST(@bIsActive AS NVARCHAR)



	SET @sSelect = 'SELECT *
		FROM
			(SELECT DocumentID, ReportID, M.TableID,ParentMenuID,M.DisplayOrder,MenuID,  Menu, ShowOnMenu , M.AccountID, AccountName,M.IsActive, 
					ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ' ) as RowNum, ExternalPageLink,OpenInNewWindow,T.TableName,M.DocumentTypeID,M.MenuType
			 FROM [Menu] M INNER JOIN Account A ON
			 M.AccountID=A.AccountID LEFT JOIN [Table] T  ON M.TableID=T.TableID ' + @sWhere + '	 
			) as RecordInfo
		WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)


	SET @sSelectCount = 'SELECT  COUNT(MenuID) AS TotalRows  FROM [Menu] M INNER JOIN 
	Account A ON M.AccountID=A.AccountID ' + @sWhere
 
 
	PRINT @sSelect

	EXEC (@sSelect)
	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Menu_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
