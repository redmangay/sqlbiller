﻿

CREATE PROCEDURE [dbo].[dbg_Conditions_CheckRecordID]
(
	@nTableID int ,
	@nSpecialNotificationID int,
	@nRecordID int
)
	/*----------------------------------------------------------------
	-- By: (Red Mangay, DB Gurus Australia) 
	-- Date: Aug 22 2019

	
	
	History:
	Red 22 Aug 2019: This SP returns the Record Detail of the Record if it's in the condition
				: Could use to return as True or False
	
	
	
	== -- Testing -- ==
	exec [dbo].[dbg_Conditions_CheckRecordID] 4242,2205,1816156



	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	DECLARE @Sql varchar(max) = ' '
DECLARE @Where varchar(max) = 'WHERE RecordID = ' + CAST(@nRecordID as varchar) +' AND IsActive = 1 AND TableID =' + CAST(@nTableID as varchar) + ' AND'

DECLARE @Conditions Table (

    iCounter int IDENTITY(1,1),
    ConditionColumnID int,
    ConditionSystemName varchar(100),
    ColumnType varchar(max),
    TableReference int,
    DropDownTypeReference varchar(max),
    ColumnReference varchar(max), /* Display Column */
    ConditionOperator varchar(100),
    ConditionOperatorSymbol varchar(100),
    ConditionJoinOperator varchar(max),
    ConditionValue varchar(max)

)

    INSERT INTO @Conditions (
			 ConditionColumnID,
			 ConditionSystemName,
			 ColumnType,
			 TableReference,
			 DropDownTypeReference,
			 ColumnReference,
			 ConditionOperator,
			 ConditionOperatorSymbol,
			 ConditionJoinOperator,
			 ConditionValue)

	   SELECT  Con.ConditionColumnID,
			 Col.SystemName,
			 Col.ColumnType,
			 TableTableID,
			 DropDownType,
			 DisplayColumn,
			 Con.ConditionOperator,
			 CASE WHEN Con.ConditionOperator = 'notequal' THEN '<>'
				 WHEN Con.ConditionOperator = 'greaterthan' THEN '>'
				 WHEN Con.ConditionOperator = 'greaterthanequal' THEN '>='
				 WHEN Con.ConditionOperator = 'lessthan' THEN '<'
				 WHEN Con.ConditionOperator = 'lessthanequal' THEN '<='
				 WHEN Con.ConditionOperator = 'contains' THEN 'LIKE'
				 WHEN Con.ConditionOperator = 'notcontains' THEN 'NOT LIKE'
				 WHEN Con.ConditionOperator = 'empty' THEN 'IS NULL'
				 WHEN Con.ConditionOperator = 'notempty' THEN 'IS NOT NULL'
				 WHEN Con.ConditionOperator = 'greaterthandays' THEN '='
				 WHEN Con.ConditionOperator = 'lessthandays' THEN '='
				 ELSE '=' END  ,
			 Con.JoinOperator,
			 Con.ConditionValue
	   FROM Conditions Con
		  JOIN [Column] Col ON 
			 Col.ColumnID = Con.ConditionColumnID
	   WHERE Con.SpecialNotificationID = @nSpecialNotificationID

	   


	   /* Lets loop to set the Conditions to Where Clause */
	   DECLARE @iiCounter int = 1
	   WHILE EXISTS(SELECT * FROM @Conditions WHERE iCounter >= @iiCounter) 
	   BEGIN
		  DECLARE @SystemName varchar(100)
		  DECLARE @TableReference varchar(100)
		  DECLARE @ConditionOperator varchar(100)

		  SELECT 
			 @ConditionOperator = ConditionOperator, 
			 @TableReference = TableReference 
			FROM @Conditions 
			WHERE iCounter = @iiCounter


		  IF (@ConditionOperator = 'contains') 
		  BEGIN
			 SELECT @Where =  @Where +  
				    ISNULL(ConditionJoinOperator,'') + ' '+  
				    ConditionSystemName + ' ' + 
				    ConditionOperatorSymbol +  ' ''%' + 
				    ConditionValue + '%'' ' 
				  FROM @Conditions 
				  WHERE iCounter = @iiCounter
		  END
		  ELSE if (@ConditionOperator = 'notcontains') /* DOES NOT CONTAIN */
		  BEGIN
			 SELECT @Where =  @Where +  
				    ISNULL(ConditionJoinOperator,'') + ' '+  
				    ConditionSystemName + ' ' + 
				    ConditionOperatorSymbol +  ' ''%' + 
				    ConditionValue + '%'' ' 
				  FROM @Conditions 
				  WHERE iCounter = @iiCounter
		  END
		  ELSE if (@ConditionOperator = 'empty') /* IS NULL */
		  BEGIN
			 SELECT @Where =  @Where +  
				    ISNULL(ConditionJoinOperator,'') + ' '+  
				    ConditionSystemName + ' ' + 
				    ConditionOperatorSymbol + ' ' 
				  FROM @Conditions 
				  WHERE iCounter = @iiCounter
		  END
		  ELSE if (@ConditionOperator = 'notempty') /* IS NOT NULL */
		  BEGIN
			 SELECT @Where =  @Where +  
				    ISNULL(ConditionJoinOperator,'') + ' '+  
				    ConditionSystemName + ' ' + 
				    ConditionOperatorSymbol + ' ' 
				  FROM @Conditions 
				  WHERE iCounter = @iiCounter
		  END
		  ELSE if (@ConditionOperator = 'lessthandays') /* IS NOT NULL */
		  BEGIN
			 SELECT @Where =  @Where +  
				    ISNULL(ConditionJoinOperator,'') + ' '+ 
				    'DATEADD(DAY,-' + ConditionValue + ', CONVERT(varchar(20),' + ConditionSystemName + ',103))' + ' ' + 
				    ConditionOperatorSymbol + ' ' + 'CONVERT(VARCHAR(20),''' + CONVERT(varchar(20), GETDATE(), 103) + ''',103) '
				  FROM @Conditions 
				  WHERE iCounter = @iiCounter
		  END
		  ELSE if (@ConditionOperator = 'greaterthandays') /* IS NOT NULL */
		  BEGIN
			 SELECT @Where =  @Where +  
				    ISNULL(ConditionJoinOperator,'') + ' '+ 
				    'DATEADD(DAY,' + ConditionValue + ', CONVERT(varchar(20),' + ConditionSystemName + ',103))' + ' ' + 
				    ConditionOperatorSymbol + ' ' + 'CONVERT(VARCHAR(20),''' + CONVERT(varchar(20), GETDATE(), 103) + ''',103) '
				  FROM @Conditions 
				  WHERE iCounter = @iiCounter
		  END
		  ELSE 
		  BEGIN
			 SELECT @Where =  @Where +  
				    ISNULL(ConditionJoinOperator,'') + ' '+  
				    ConditionSystemName + ' ' + 
				    ConditionOperatorSymbol +  '''' +
				    ConditionValue   + ''' '
				  FROM @Conditions 
				  WHERE iCounter = @iiCounter
		  END

	   /*END*/

   
		  SET @iiCounter = @iiCounter + 1
	   END
    
    /* Lets get the valid Columns only... */
    PRINT @Where
    DECLARE @Counter int= 1
    DECLARE @Queue TABLE
	(
		[Counter] int identity(1,1),
		ColumnName varchar(30),
		RealDate datetime
	)

		  INSERT INTO @Queue (ColumnName)
			SELECT SystemName 
			FROM [Column]    
			WHERE [TableID] = @nTableID 
			AND SystemName <> 'RecordID'

		  DECLARE @sSQL varchar(MAX) = 'SET DATEFORMAT DMY;
		    SELECT 
		  '
		   WHILE EXISTS(SELECT * FROM @Queue WHERE [Counter] >= @Counter)
		   BEGIN
			   SELECT @sSQL = @sSQL +  ' Record.' + ColumnName  +',
			   '
				   FROM @Queue WHERE [Counter] = @Counter
				
			   SET @Counter = @Counter + 1
		   END 

	   SELECT @sSQL = @sSQL + ' Record.RecordID
		  FROM Record' +'
		  '

	   IF ((SELECT COUNT(*) FROM @Conditions) <= 0)
	   BEGIN
		  SET @Where = @Where + ' 1<>1 '
	   END

	   SELECT  @Sql = @sSQL + @Where
	   PRINT @SQL
	   EXEC(@SQL);

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_Conditions_CheckRecordID', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH




