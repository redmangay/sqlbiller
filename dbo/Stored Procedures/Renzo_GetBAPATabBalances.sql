﻿-- =============================================
-- Author:		<Red Mangay>
-- Create date:	<28/11/2018>
-- Description:	<>
-- =============================================
/*

exec [Renzo_GetBAPATabBalances]  11538529

*/


CREATE PROCEDURE [dbo].[Renzo_GetBAPATabBalances]
(
	@RecordID int
	
)
AS
BEGIN
	
	

 -- DECLARE @RecordID int =  2711483
  DECLARE @TotalInvoiceAmount money
  DECLARE @TotalPaymentAmount money
  DECLARE @JobValue money

 /* BAPA tab: Get the total invoice */
 SELECT  @TotalInvoiceAmount = ISNULL(SUM(CAST(V005 AS MONEY)),0) FROM [RECORD] WHERE TABLEID =2711 AND ISACTIVE =1  AND V002 =  CAST(@RecordID AS VARCHAR)
 --SELECT @TotalInvoiceAmount AS [Total Invoice]

 /* BAPA tab: Get the Job Balance Value */
 SELECT @JobValue = V008 FROM RECORD WHERE TABLEID = 2475 AND RECORDID = @RecordID
 --SELECT @JobValue - @TotalInvoiceAmount AS [Job Value Balance]

 /* BAPA tab: Get the total Payment */
 SELECT @TotalPaymentAmount = ISNULL(SUM(CAST(V007 AS MONEY)),0.00) FROM [RECORD] WHERE IsActive = 1 and  TABLEID =2711 AND V029 <> 'Bad Debt Entry'  AND V002 = CAST(@RecordID AS VARCHAR)
 --SELECT @TotalPaymentAmount AS [Total Payment]

 /* BAPA tab: Get the Invoice Balance net of Payment */
 --SELECT @TotalInvoiceAmount - @TotalPaymentAmount AS [Invoice Balance]

 SELECT 'Total' AS [Label], 
 ISNULL(@TotalInvoiceAmount,0.00) AS [Invoice],
 ISNULL(@TotalPaymentAmount,0.00) AS [Payment]
 
 UNION ALL
 SELECT 'Balance' AS [Label], 
 ISNULL(@JobValue - @TotalInvoiceAmount,0.00) AS [Invoice],
 ISNULL(@TotalInvoiceAmount - @TotalPaymentAmount,0.00) AS [Payment]
 


END

