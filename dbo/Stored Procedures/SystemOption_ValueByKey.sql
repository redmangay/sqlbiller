﻿

CREATE PROCEDURE [dbo].[SystemOption_ValueByKey]
(
	@sOptionKey nvarchar(200),
	@sOptionValue nvarchar(1000) output
)
AS

BEGIN TRY

	SELECT @sOptionValue =  OptionValue 
		FROM SystemOption
		WHERE OptionKey=@sOptionKey
	
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('SystemOption_ValueByKey', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
