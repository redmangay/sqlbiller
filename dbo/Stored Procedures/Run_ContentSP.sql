﻿

CREATE PROCEDURE [dbo].[Run_ContentSP]

(
	  @sSPName varchar(100) ,
      @sKey varchar(10) = NULL,
      @sRoot varchar(200)=NULL

)

AS

/*

      UNIT TESTING

      ============

      Run_ContentSP 'ep_Article_Content','2'

      EXEC dbo.ep_Article_Content @sKey = 1

*/
BEGIN TRY
	BEGIN

		  DECLARE @sSQL varchar(MAX)

		  IF @sKey IS NULL

		  BEGIN

				SET @sSQL='EXEC dbo.' + @sSPName            

		  END

		  ELSE
			BEGIN
				IF @sRoot is NULL
				SET @sSQL='EXEC dbo.' + @sSPName + ' @sKey=''' + @sKey + '''' 
				IF @sRoot is not NULL
				SET @sSQL='EXEC dbo.' + @sSPName + ' @sKey=''' + @sKey + ''',@sRoot=''' + @sRoot + '''' 
            
			END	
     
	END

	 Exec (@sSQL)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Run_ContentSP', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
