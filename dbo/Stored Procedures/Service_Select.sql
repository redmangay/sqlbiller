﻿

CREATE PROCEDURE [dbo].[Service_Select]
(
	@sServiceType varchar(227)  = NULL,
	@nTableID int  = NULL,
	@nTableChildID int  = NULL,
	@nAccountID int  = NULL,
	@bIsServerCode bit  = NULL,
	@sServiceJSON varchar(MAX)  = NULL,
	@sOrder varchar(200) = ServiceID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	EXEC [dbo].[Service_Select] @nTableID=2971,@sOrder='ServiceID DESC',@nStartRow=1,@nMaxRows=50
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(4000)
	DECLARE @sWhere nvarchar(4000)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @sServiceType IS NOT NULL and len(@sServiceType)>0
		SET @sWhere = @sWhere + ' AND ServiceType LIKE '+'''%' + @sServiceType + '%'''
	IF @nTableID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND TableID = '+CAST(@nTableID AS NVARCHAR)
	IF @nTableChildID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND TableChildID = '+CAST(@nTableChildID AS NVARCHAR)
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND AccountID = '+CAST(@nAccountID AS NVARCHAR)
	IF @bIsServerCode IS NOT NULL 
		SET @sWhere = @sWhere + ' AND IsServerCode = '+CAST(@bIsServerCode AS NVARCHAR)
	IF @sServiceJSON IS NOT NULL and len(@sServiceJSON)>0
		SET @sWhere = @sWhere + ' AND ServiceJSON LIKE '+'''%' + @sServiceJSON + '%'''
	SET @sSelect = 'SELECT * FROM 
	(SELECT Service.*, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum FROM Service' + @sWhere + ') as UserInfo'
 
	-- Extend WHERE to include paging:
	SET @sWhere = @sWhere + ' AND RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	SET @sOrder = ' ORDER BY ' + @sOrder
	SET @sSelect = @sSelect + @sWhere + @sOrder
 
	EXEC (@sSelect)
	SET ROWCOUNT 0
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Service_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
