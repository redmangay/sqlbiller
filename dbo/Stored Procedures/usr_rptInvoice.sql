﻿CREATE Procedure [dbo].[usr_rptInvoice]
AS
BEGIN

	SELECT CASE When billing.[%Complete] is null Then 'N/A' ELSE billing.[%Complete]+ ' %' END as PercentComplete, 
	billing.*, Project.Status
	FROM Account24918.[vBilling Information] as billing 
	LEFT JOIN Account24918.[vProject] as Project 
	ON billing.[Project Number] = Project.ProjectNumber;
END


  
  
