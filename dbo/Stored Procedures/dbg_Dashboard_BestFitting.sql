﻿

CREATE PROCEDURE [dbo].[dbg_Dashboard_BestFitting]
(
	 @DocumentText varchar(250) = NULL,
	 @UserID INT, 
	 @RoleID INT,
	 @DocumentID int OUTPUT
)
AS


BEGIN TRY
		
	DECLARE @AccountID INT
	DECLARE @DashboardDefaultFromUserID INT
	SELECT @AccountID=AccountID,@DashboardDefaultFromUserID=DashboardDefaultFromUserID
	 FROM [Role] WHERE RoleID=@RoleID
	
	
	IF @DocumentText IS NOT NULL
		BEGIN
			-- security leak here, anyone can put a dashboard name and get access to it.
			SELECT TOP 1	@DocumentID=DocumentID
			FROM [dbo].[Document]
			WHERE ForDashBoard=1 AND AccountID = @AccountID 
			AND DocumentText=@DocumentText
			ORDER BY  DocumentID DESC
		END

	IF @DocumentID IS NULL
		BEGIN
			SELECT TOP 1 @DocumentID=DocumentID
					FROM [dbo].[Document]
					WHERE ForDashBoard=1 AND AccountID = @AccountID 
					AND UserID = @UserID
					ORDER BY  DocumentID DESC

		END

	IF @DocumentID IS NULL
		BEGIN
			SELECT TOP 1 @DocumentID=DocumentID
					FROM [dbo].[Document]
					WHERE ForDashBoard=1 AND AccountID = @AccountID 
					AND UserID = @UserID
					ORDER BY  DocumentID DESC

		END

	IF @DocumentID IS NULL AND @DashboardDefaultFromUserID IS NOT NULL
		BEGIN
			SELECT TOP 1 @DocumentID=DocumentID
					FROM [dbo].[Document]
					WHERE ForDashBoard=1 AND AccountID = @AccountID 
					AND UserID = @DashboardDefaultFromUserID
					ORDER BY  DocumentID DESC

		END
		
	IF @DocumentID IS NULL
			BEGIN
				DECLARE @AccUserID INT =[dbo].[fnGetAccountHolderUserID] (@AccountID)	--Account Holder
				SELECT TOP 1 @DocumentID=DocumentID
					FROM [dbo].[Document]
					WHERE ForDashBoard=1 AND AccountID = @AccountID 
					AND UserID = @AccUserID
					ORDER BY  DocumentID DESC

			END

	IF @DocumentID IS NULL
			BEGIN
				-- we will DELETE this part later, we are doing this to get old implementetion default dashboard(when UserID is NULL)
				SELECT TOP 1 @DocumentID=DocumentID
					FROM [dbo].[Document]
					WHERE ForDashBoard=1 AND AccountID = @AccountID 
					AND UserID IS NULL
					ORDER BY  DocumentID DESC
				DECLARE @AccUserID2 INT =[dbo].[fnGetAccountHolderUserID] (@AccountID)	--Account Holder
				IF @DocumentID IS NOT NULL
					UPDATE [Document] SET UserID=@AccUserID2 WHERE DocumentID=@DocumentID -- it's fix of old data
			END
	
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_Dashboard_BestFitting', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
