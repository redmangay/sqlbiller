﻿CREATE PROCEDURE [dbo].[Renzo_RefreshCurrencyRateJobInformation]
(
	@RecordID  int,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output
)
AS



UPDATE		[Job Information]	
SET			[Job Information].V007 =	 ISNULL([Currency Rates].V002,'0')	   
FROM		[Record] [Job Information] 
JOIN	    [Record] [Currency Rates]	ON				1=1
										AND		[Currency Rates].RecordID	 = [Job Information].V034	--	Currency	
										AND		[Currency Rates].TableID = '2665'		/*	Currency Rates	*/
										AND		[Currency Rates].IsActive = 1
										
	
WHERE		 1=1
AND		[Job Information].RecordID =@RecordID
AND		[Job Information].TableID = '2475'		/*	Job Information	*/
AND		[Job Information].IsActive = 1




RETURN @@ERROR;