﻿

CREATE PROCEDURE [dbo].[ets_Record_Avg_ForARecordID]
(
	@nRecordID INT
	
)
AS
/*
EXEC ets_Record_Avg 109,781,10,'05/22/2012'
*/
BEGIN TRY
	DECLARE @nTableID int 
	DECLARE	@dDateTimeRecorded datetime	

	SELECT @nTableID=TableID,@dDateTimeRecorded=DateTimeRecorded FROM Record 
	WHERE RecordID=@nRecordID


	DECLARE Cur_SS CURSOR FOR
	SELECT SystemName, AvgColumnID,AvgNumberOfRecords FROM [Column] 
		WHERE TableID=@nTableID AND ColumnType='number'	AND NumberType=4

	DECLARE	@nAvgColumnID INT
	DECLARE @sSystemName1 AS varchar(10)
	DECLARE @nAvgNumberOfRecords INT


	OPEN Cur_SS;
	FETCH FROM Cur_SS INTO @sSystemName1,@nAvgColumnID,@nAvgNumberOfRecords;
	WHILE @@FETCH_STATUS = 0
	  BEGIN
		DECLARE @sSystemName2 AS varchar(10)

		SELECT @sSystemName2=SystemName FROM [Column] WHERE ColumnID=@nAvgColumnID
	
	
		DECLARE @sSelect nvarchar(MAX)

		SET @sSelect ='
	
		DECLARE @dAvgValue DECIMAL(18,10)
	
		SELECT @dAvgValue=AVG(CONVERT(decimal(18,10),'+CAST(@sSystemName2 AS varchar)+')) FROM 
	(SELECT  '+CAST(@sSystemName2 AS varchar)+',DateTimeRecorded,ROW_NUMBER() OVER(ORDER BY DateTimeRecorded DESC) as RowNum  
	FROM Record WHERE '+CAST(@sSystemName2 AS varchar)+' IS NOT NULL AND IsActive=1 
	AND DateTimeRecorded <=''' + CONVERT(varchar(30), @dDateTimeRecorded, 120) + '''
	AND TableID='+CAST(@nTableID AS varchar)+') AS
	RecordInfo
	WHERE RowNum<=' + CAST(@nAvgNumberOfRecords AS varchar) + ' '
	+ ' UPDATE Record SET '+@sSystemName1+'=CAST(@dAvgValue AS varchar) WHERE RecordID=' + + CAST(@nRecordID AS varchar)


		PRINT @sSelect
		EXEC (@sSelect)
  
		FETCH FROM Cur_SS INTO @sSystemName1,@nAvgColumnID,@nAvgNumberOfRecords;
    
	  END

	CLOSE Cur_SS
	DEALLOCATE Cur_SS	

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Record_Avg_ForARecordID', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
