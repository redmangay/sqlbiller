﻿ CREATE procedure [dbo].[usr_rptjobperformancewithCompleteJOB]
as

set dateformat dmy
SELECT DISTINCT BillingInformation.[Project Number]
		, Max(cast(BillingInformation.Date as date)) AS MaxOfDate

		into #JPCompletedByMaxInvoiceDate1
	FROM Account24918.[vBilling Information] BillingInformation
		LEFT JOIN Account24918.vProject Project ON BillingInformation.[Project Number] = Project.ProjectNumber
	GROUP BY BillingInformation.[Project Number]
		, Project.Status
		, BillingInformation.[Invoice Amount]
	HAVING  (Project.Status='4' OR Project.Status='5' OR Project.Status='6')
		AND (BillingInformation.[Invoice Amount] IS NOT NULL)







sEleCT DISTINCT 
    Project.[Group/Division] as GroupDivision, TimeSheet.Date as TimeDate, Employee_List.Initials AS EInitials, TimeSheet.Details, 
    EmployeeHistroy.Name as EName, 
    case TimeSheet.STATUS when '5' then isnull(Timesheet.[Total],0) else isnull(TimeSheet.[$_Amount],0) end as TimeSheet,
    isnull(TimeSheet.[$_Billed],0) as BilledDoller,
   case  TimeSheet.STATUS 
   when '0' then  'By Rate'
   when '1' then  'Hold'
   when '3' then 'Fixed'
   when '4' then 'No Charge'
   when '5' then 'Write-off' 
   when '6' then 'Adjusted'
   else 'NONE' end as BilledStatus,
   PNEmployeeIDTimeSheetID.TimeSheetID,
   BillingInformation.ID AS BillingID,
   BillingInformation.[Invoice No], 
   BillingInformation.[Invoice Amount Converted] as InvoiceAmount, 
   BillingInformation.Date AS InvoiceDate, BillingInformation.[Tax Name], 
   BillingInformation.[Tax Amount], BillingInformation.[Tax Rate],   Project.ProjectNumber as JobNo, Project.ProjectTitle,
   case when isnull(cast(Project.CurrencyRate as money),0)=0 then Project.VALUE
       when isnumeric(cast(Project.VALUE as money))=1 then cast(Project.VALUE as money)/cast(Project.CurrencyRate as money)
      else Project.VALUE end as ProjectValue,
        case when isnull(cast(Project.CurrencyRate as money) ,0) =0 or Project.CurrencyRate ='' then     '1' else Project.CurrencyRate end as ExchangeRate,
        case when isnull(cast(TimeSheet.[$_Amount] as  money),0) =0 then 0 else (cast (TimeSheet.[$_Billed] as money)/cast(TimeSheet.[$_Amount] as money)) * 100 end as PercentBilled, 
        case when isnull(cast(TimeSheet.[$_Amount] as money),0) =0 then 0 else   ((cast(TimeSheet.[$_Amount] as money) - cast(TimeSheet.[$_Billed] as money))/ cast(TimeSheet.[$_Amount] as money)) * 100 end as PercentUnBilled     
        
    
    
    
  --FROM part of query when options SINGLE JOB or JOBS WITHIN JOB NO RANGE chosen
    FROM (Account24918.vEmployee_List Employee_List INNER JOIN (Account24918.vProject Project RIGHT JOIN ((Account24918.vPNEmployeeIDTimeSheetID PNEmployeeIDTimeSheetID INNER JOIN 
    Account24918.vTimeSheet TimeSheet ON PNEmployeeIDTimeSheetID.TimeSheetID = TimeSheet.TimeSheetID) INNER JOIN 
   Account24918.[vBilling Information] BillingInformation ON (PNEmployeeIDTimeSheetID.ProjectNumber = BillingInformation.[Project Number]) AND
   (TimeSheet.InvoiceNo = BillingInformation.[Invoice No])) ON Project.ProjectNumber = BillingInformation.[Project Number]) ON 
   Employee_List.EmployeeID = PNEmployeeIDTimeSheetID.EmployeeID) LEFT JOIN Account24918.vEmployeeHistroy EmployeeHistroy ON 
   Project.EmployeeID = EmployeeHistroy.EmployeeID
   LEFT JOIN #JPCompletedByMaxInvoiceDate1 qryJPCompletedByMaxInvoiceDate ON Project.ProjectNumber = qryJPCompletedByMaxInvoiceDate.[Project Number] 
    
WHERE ( BillingInformation.Type = '1'and BillingInformation.[Project Number] ='WC722-01')
    


   
    
    
  
