﻿

CREATE PROCEDURE [dbo].[Column_ReplaceDisplayColumn]
(
	@TableID int,
	@OldColumnName varchar(MAX),
	@NewColumnName varchar(MAX)
	
)
AS

BEGIN TRY 
	-- EXEC [Column_ReplaceDisplayColumn_MR] 2720,'Name','DeptName'
	DECLARE @AccountID int
	DECLARE @sTableName nvarchar(255)

	SELECT @AccountID=AccountID FROM [Table] WHERE TableID=@TableID
	SELECT @sTableName=TableName FROM [Table] WHERE TableID=@TableID


	-- [Column].[DisplayColumn]

	UPDATE [Column] 
		SET [DisplayColumn] = REPLACE([DisplayColumn],'[' + @OldColumnName + ']','[' + @NewColumnName + ']') 
		WHERE [TableTableID] = @TableID AND CHARINDEX('[' + @OldColumnName + ']',[DisplayColumn])>0




	--[Column].[MapPopup]

	UPDATE [Column] 
		SET [MapPopup] = REPLACE([MapPopup], '[' + @OldColumnName + ']','[' + @NewColumnName + ']') 
		WHERE [TableID] = @TableID AND CHARINDEX('[' + @OldColumnName + ']',[MapPopup])>0

	UPDATE C
		SET C.[MapPopup] = REPLACE(C.[MapPopup], '[' +@sTableName +':' + @OldColumnName + ']',
		'[' +@sTableName +':' +  + @NewColumnName + ']') 
		FROM [Column] AS C INNER JOIN [Table] AS T ON C.TableID=T.TableID 
		WHERE T.AccountID = @AccountID AND CHARINDEX('[' +@sTableName +':' + @OldColumnName + ']',C.[MapPopup])>0


	--[DataReminder].[ReminderContent]

	--- NO NEED	, [ReminderHeader] = REPLACE([ReminderHeader], '[' + @OldColumnName + ']','[' + @NewColumnName + ']') 

	UPDATE [DataReminder] 
		SET [ReminderContent] = REPLACE([ReminderContent], '[' + @OldColumnName + ']','[' + @NewColumnName + ']') 
		WHERE [ColumnID] IN (SELECT [ColumnID] FROM [Column] WHERE TableID = @TableID)
		AND CHARINDEX('[' + @OldColumnName + ']',[ReminderContent])>0

	UPDATE DR
		SET DR.[ReminderContent] = REPLACE(DR.[ReminderContent], '[' +@sTableName +':' + @OldColumnName + ']',
		'[' +@sTableName +':' +  + @NewColumnName + ']') 
		FROM [DataReminder] AS DR INNER JOIN [Column] AS C ON DR.ColumnID=C.ColumnID  
		INNER JOIN [Table] T ON C.TableID=T.TableID
		WHERE T.AccountID = @AccountID AND CHARINDEX('[' +@sTableName +':' + @OldColumnName + ']',DR.[ReminderContent])>0


	-- [Table].[HeaderName]
	UPDATE [Table]  
		SET [HeaderName] = REPLACE([HeaderName] , '[' + @OldColumnName + ']','[' + @NewColumnName + ']') 
		WHERE TableID = @TableID AND CHARINDEX('[' + @OldColumnName + ']',[HeaderName])>0

	UPDATE [Table]  
		SET [HeaderName] = REPLACE([HeaderName] , '[' +@sTableName +':' + @OldColumnName + ']',
		'[' +@sTableName +':' +  @NewColumnName + ']') 
		WHERE AccountID = @AccountID  AND CHARINDEX('[' +@sTableName +':' + @OldColumnName + ']',[HeaderName])>0

	

	--for calendar display text DocumentSection.Details

	UPDATE DocumentSection
		SET Details=REPLACE(Details , '[' + @OldColumnName + ']','[' + @NewColumnName + ']') 
		WHERE DocumentSectionTypeID=11 AND Details like '%"TableID":'+CAST(@TableID AS varchar)+'%'
	AND CHARINDEX('"TableID":'+CAST(@TableID AS varchar),Details)>0

	UPDATE DS
		SET DS.Details=REPLACE(DS.Details ,'[' + @sTableName + ':' + @OldColumnName + ']',
		'[' + @sTableName + ':' + @NewColumnName + ']') 
		FROM DocumentSection AS DS INNER JOIN Document D ON DS.DocumentID=D.DocumentID   
		WHERE DocumentSectionTypeID=11 AND D.AccountID=@AccountID
	AND CHARINDEX('[' + @sTableName + ':' + @OldColumnName + ']',DS.Details)>0


	UPDATE Record
	SET ValidationResults =REPLACE(ValidationResults , 'INVALID:' + @OldColumnName,
	'INVALID:' + @NewColumnName)  WHERE TableID=@TableID AND ValidationResults IS NOT NULL
	AND CHARINDEX('INVALID:' + @OldColumnName,ValidationResults)>0


	UPDATE Record
	SET WarningResults =REPLACE(WarningResults , 'WARNING: ' + @OldColumnName,
	'WARNING: ' + @NewColumnName)  WHERE TableID=@TableID AND WarningResults IS NOT NULL
	AND CHARINDEX('WARNING: ' + @OldColumnName,WarningResults)>0

	UPDATE Record
	SET WarningResults =REPLACE(WarningResults , 'EXCEEDANCE: ' + @OldColumnName,
	'EXCEEDANCE: ' + @NewColumnName)  WHERE TableID=@TableID AND WarningResults IS NOT NULL
	AND CHARINDEX('EXCEEDANCE: ' + @OldColumnName,WarningResults)>0

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Column_ReplaceDisplayColumn', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
