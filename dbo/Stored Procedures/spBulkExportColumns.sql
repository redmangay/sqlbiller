﻿
-- =============================================
-- Author:		<Manoj sharma>
-- ModIFiy date: <31 August 2016>
-- Description:	<use to return sql based in tableid>
-- =============================================
CREATE PROCEDURE [dbo].[spBulkExportColumns]
	(
		 @TableID		varchar(MAX)
	
	)	 

AS

BEGIN TRY
	--EXEC		[dbo].[spBulkExportColumns] 3127

	 /*
	REVISION HISTORY
	----------------
		/*	Creation/Remove of Temp Table that Contain Column List	*/
		Red 20/01/2017 : UPDATE this sp, just load this in SVN db from the Brain db. No changes made.
  */
	



	IF OBJECT_ID('tempdb..#ColumnList') IS NOT NULL   /*Then it exists*/
		DROP TABLE #ColumnList

	CREATE    TABLE        #ColumnList

		(
				ColumnID        INT  
				,TableName            varchar(MAX)
				,TableID            INT
				,ColumnDisplayName             varchar(MAX)
			    ,ColumnSystemName    varchar(MAX)
            
		)

	

	

	DECLARE			@SQL						varchar(MAX) 
	SET NOCOUNT ON;

	DECLARE @IsTable INT
	DECLARE @TableName		varchar(MAX)

	SELECT TOP	1   @IsTable= COUNT(*),@TableName = MIN(TableName) 
	FROM		[Table] 
	WHERE		TableID =@TableID 
	AND 		[Table].IsActive = 1

	IF	 ISNULL(@IsTable,0) =0
	BEGIN
	PRINT		'Include Active TableID';
	
	END
	

		
	
			SET				@SQL = ''

			SELECT			@SQL = @SQL + '	INSERT INTO 	#ColumnList	(ColumnID,TableName,TableID,ColumnSystemName,ColumnDisplayName) VALUES	('''+ cast( [Column].[ColumnID] as varchar(MAX))+ ''',''['+@TableName+']'','''+@TableID+''', ''['+@TableName+'].' + [Column].[SystemName] + ''',''[' + [Column].[DisplayName] + ']'')
			'
			FROM			[Table]
			JOIN			[Column] ON						[Column].TableID = [Table].TableID
												AND			([Column].IsStandard = 0
												OR			[Column].SystemName = 'RecordID')
			WHERE			[Table].IsActive = 1
			AND				[Table].TableID = @TableID
			AND				[Table].[TableName] = @TableName
			ORDER BY		[Column].[DisplayOrder]

			SET				@SQL = SUBSTRING(@SQL, 1, LEN(@SQL) - 4) + '
			
			
			
			 '
		
			EXEC  (@sql)
			

			
	
	DECLARE			@ChildColumnID				INT = 1
		DECLARE         @RelatedTableName varchar(MAX)
	DECLARE			@RelatedTableID    varchar(MAX)
	
	SET			@ChildColumnID = 1

		--SELECT			ROW_NUMBER() OVER(ORDER BY [Column].DisplayOrder DESC) AS		ChildColumnID,
		--				[Column].ColumnID
		--	INTO		#ChildColumn
		--	FROM		[Column]
		--	JOIN		[Table] ON					[Table].TableID = [Column].TableID
			
		--	JOIN		[Table] ChildTable ON		ChildTable.TableID = [Column].TableTableID
		--	WHERE		[Column].TableTableID IS NOT NULL
		--	and [Column].TableID =@tableid
		--	ORDER BY	[Column].TableID;
		;with RelatedTable
				AS
				(
				SELECT DISTINCT C.TableID
								
				FROM [Column] C JOIN [Table] T ON C.TableID=T.TableID
						LEFT OUTER JOIN [Column] PC ON PC.[ColumnID] = C.[LinkedParentColumnID]					
						WHERE C.TableID IN (SELECT ChildTableID FROM TableChild WHERE ParentTableID= @tableid)
						AND C.DisplayTextSummary IS NOT NULL AND LEN(C.DisplayTextSummary) > 0 
						)
					
						SELECT ROW_NUMBER() OVER(ORDER BY TableID) AS		ChildColumnID , TableID INTO		#ChildColumn FROM RelatedTable

		WHILE EXISTS	(	SELECT			#ChildColumn.ChildColumnID
								FROM		#ChildColumn
								WHERE		#ChildColumn.ChildColumnID = @ChildColumnID
						)
			BEGIN
				SELECT			@RelatedTableID = '' + CAST( #ChildColumn.TableID AS varchar(MAX) ) + ''	
					FROM		#ChildColumn WHERE #ChildColumn.ChildColumnID = @ChildColumnID
					
	
				
				SELECT TOP	1   @RelatedTableName = MIN(TableName) 
	FROM		[Table] 
	WHERE		TableID =@RelatedTableID 
	AND 		[Table].IsActive = 1


	 	SET				@SQL = ''

			SELECT			@SQL = @SQL + '	INSERT INTO 	#ColumnList	(ColumnID,TableName,TableID,ColumnSystemName,ColumnDisplayName) VALUES	('''+ cast( [Column].[ColumnID] as varchar(MAX))+ ''',''['+@RelatedTableName+']'','''+@RelatedTableID+''', ''['+@RelatedTableName+'].' + [Column].[SystemName] + ''',''[' + [Column].[DisplayName] + ']'')
			'
			FROM			[Table]
			JOIN			[Column] ON						[Column].TableID = [Table].TableID
												AND			([Column].IsStandard = 0
												OR			[Column].SystemName = 'RecordID')
			WHERE			[Table].IsActive = 1
			AND				[Table].TableID = @RelatedTableID
			AND				[Table].[TableName] = @RelatedTableName
			ORDER BY		[Column].[DisplayOrder]

			SET				@SQL = SUBSTRING(@SQL, 1, LEN(@SQL) - 4) + ''
			
			EXEC  (@sql)

				
				SET			@ChildColumnID = @ChildColumnID + 1
			END;

			SELECT ColumnID, TableName, ColumnSystemName, ColumnDisplayName FROM #ColumnList
		DROP TABLE		#ChildColumn;

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spBulkExportColumns', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH			 
