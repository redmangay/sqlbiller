﻿

CREATE PROCEDURE [dbo].[ets_Record_Sync_Run]
	
AS

BEGIN TRY
	DECLARE @TableTableIDToSync TABLE (
	[Counter] int identity(1,1)
	,TableIDIDToSync int
	,ArchiveMonths int
	,ArchiveLastRun datetime )

	INSERT INTO @TableTableIDToSync (TableIDIDToSync,ArchiveMonths,ArchiveLastRun)
	SELECT [Live_Table].[TableID], [Live_Table].[ArchiveMonths],[Live_Table].[ArchiveLastRun] 
	FROM [Table] [Live_Table] JOIN [Table] [Archive_Table] ON 1=1
	AND [Live_Table].[ArchiveDatabase] IS NOT NULL
	AND [Live_Table].[ArchiveMonths] IS NOT NULL
	AND [Archive_Table].[TableID] = [Live_Table].[TableID]

		--SELECT * FROM @TableTableIDToSync

		DECLARE @Counter int= 1
		WHILE EXISTS (SELECT * FROM @TableTableIDToSync WHERE [Counter] >= @Counter)
		BEGIN
			DECLARE @TableIDToSync int --, @ArchiveMonths int, @ArchiveLastRun datetime, @NumberOfMonthsNow int
			SELECT @TableIDToSync = TableIDIDToSync --, @ArchiveMonths = ArchiveMonths, @ArchiveLastRun = ArchiveLastRun 
			FROM @TableTableIDToSync  WHERE [Counter] = @Counter

			-- lets sync the heck: daily
			EXEC [dbo].[ets_Record_Sync] @TableIDToSync

			SET @Counter = @Counter + 1
		END

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog]([Module], [ErrorMessage], [ErrorTrack], [ErrorTime], [Path]) 
		VALUES ('ets_Record_Sync_Run', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())

		--LETS EMAIL THE ERROR?
END CATCH
