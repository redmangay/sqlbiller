﻿

CREATE PROCEDURE [dbo].[ets_Document_Select]
(
	@nDocumentID int  = NULL,
	@nAccountID int  = NULL,
	@sDocumentText varchar(250)  = NULL,
	@nDocumentTypeID int  = NULL,
	--@sFileTitle nvarchar(255)  = NULL,
	@dFromDocumentDate datetime  = NULL,
	@dToDocumentDate datetime  = NULL,
	@dDateAdded datetime  = NULL,
	@dDateUpdated datetime  = NULL,
	@nUserID int  = NULL,
	@nTableID int=NULL,
	@sOrder nvarchar(200) = DocumentID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647,
	@sTableIn varchar(MAX)  =NULL ,
	@bForPuiblic bit=NULL,
	@nParentMenuID int=NULL
)
	/*----------------------------------------------------------------
	EXEC ets_Document_Select @nAccountID=374
	---------------------------------------------------------------*/
 
AS
 
BEGIN TRY
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1 AND ([Document].ForDashBoard IS NULL OR [Document].ForDashBoard<>1) '
	IF @nDocumentID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Document].DocumentID = '+CAST(@nDocumentID AS NVARCHAR)
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Document].AccountID = '+CAST(@nAccountID AS NVARCHAR)
	IF @sDocumentText IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ([Document].DocumentText LIKE '+'''%' + @sDocumentText + '%''' + ' OR [Document].FileTitle LIKE '+'''%' + @sDocumentText + '%''' + ' OR [Document].DocumentDescription LIKE '+'''%' + @sDocumentText + '%'')'
	IF @nDocumentTypeID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Document].DocumentTypeID = '+CAST(@nDocumentTypeID AS NVARCHAR)
	
	IF @nDocumentTypeID IS NULL 
		SET @sWhere = @sWhere + ' AND ([DocumentType].DocumentTypeName <>''Custom Reports'' OR [Document].DocumentTypeID is NULL) '

	--IF @sFileTitle IS NOT NULL 
		--SET @sWhere = @sWhere + ' AND [Document].FileTitle LIKE '+'''' + @sFileTitle + ''''
	IF @dFromDocumentDate IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Document].DocumentDate >= ''' + CONVERT(varchar(30), @dFromDocumentDate, 120) + ''''
	IF @dToDocumentDate IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Document].DocumentDate <= ''' + CONVERT(varchar(30), @dToDocumentDate, 120) + ''''

	
	IF @dDateAdded IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Document].DateAdded = '+CAST(@dDateAdded AS NVARCHAR)
	IF @dDateUpdated IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Document].DateUpdated = '+CAST(@dDateUpdated AS NVARCHAR)
	IF @nUserID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Document].UserID = '+CAST(@nUserID AS NVARCHAR)

	IF @nTableID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Document].TableID = '+CAST(@nTableID AS NVARCHAR)

	IF @sTableIn IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Document].TableID  IN ( '+ @sTableIn + ')'

	IF @bForPuiblic IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [Document].ReportHTML is not NULL AND [Document].IsReportPublic = '+CAST(@bForPuiblic AS NVARCHAR)

	IF @nParentMenuID IS NOT NULL
	  SET @sWhere = @sWhere + ' AND [Document].DocumentID IN (SELECT DocumentID FROM Menu WHERE ParentMenuID='+CAST(@nParentMenuID AS NVARCHAR)+')'

	SET @sSelect = 'SELECT * FROM 
	(SELECT [Document].DocumentID, [Document].AccountID, [Document].DocumentText, [Document].DocumentTypeID, [Document].UniqueName, [Document].FileTitle, 
						  [Document].DocumentDate, [Document].DateAdded, [Document].DateUpdated, [Document].UserID, ([User].FirstName +'' '' + [User].LastName) AS FullName,[Document].TableID, 
						  DocumentType.DocumentTypeName,[Document].ReportHTML,[Document].IsReportPublic,[Document].DocumentDescription,[Document].DocumentEndDate, 
						  ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum 
						  FROM [Document] LEFT JOIN
						  DocumentType ON [Document].DocumentTypeID = DocumentType.DocumentTypeID LEFT JOIN
						  [User] ON [Document].UserID = [User].UserID ' + @sWhere + ') as DocumentInfo'

	SET @sSelectCount=REPLACE(@sSelect, 'SELECT * FROM', 'SELECT COUNT(*) AS TotalRows FROM')
	SELECT @sSelect= @sSelect + ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)

 
	 SET ROWCOUNT @nMaxRows
	--PRINT @sSelect
	EXEC (@sSelect)
	SET ROWCOUNT 0

	--PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Document_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
