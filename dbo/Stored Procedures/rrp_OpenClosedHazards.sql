﻿

CREATE PROCEDURE [dbo].[rrp_OpenClosedHazards]
(
	@AccountID int,
	@FromDate date,
	@ToDate date
)
AS 

BEGIN TRY
	-- Test: EXEC rrp_OpenClosedHazards @AccountID = 24822, @FromDate='2013-01-01', @ToDate='2015-01-01'

	-- Workaround: Define the columne in here where it will never be run:
	 IF 1 = 2
		 BEGIN
		   -- These are the actual column names and types returned by the real proc
		   SELECT CAST('' AS nvarchar(20)) AS Title, 
				  CAST('' AS nvarchar(20)) AS [Item],
				  0 as [Count]
		 END;
	-- End Workaround

	DECLARE @sFromDate varchar(10), @sToDate varchar(10)
	SELECT @sFromDate = CONVERT( varchar(10), @FromDate, 103),
		@sToDate = CONVERT( varchar(10), @ToDate, 103)

	DECLARE @sSQL varchar(MAX)
	SET @sSQL = '
	DECLARE @Results table ([Item] varchar(50), [Count] int);
	SET DATEFORMAT DMY;
	INSERT INTO @Results ([Item], [Count]) SELECT ''Added'', COUNT(*) FROM Account' + CAST(@AccountID as varchar) + '.vHazard 
		WHERE ISDATE([Date IdentIFied])=1 AND CAST([Date IdentIFied] as date) between 
		CAST('''	+ @sFromDate + ''' as date) AND CAST(''' + @sToDate + ''' as date); 
	INSERT INTO @Results ([Item], [Count]) SELECT ''Closed'', COUNT(*) FROM Account' + CAST(@AccountID as varchar) + '.vHazard 
		WHERE ISDATE([Date Closed])=1 and CAST([Date Closed] as date) between 
		CAST('''	+ @sFromDate + ''' as date) AND CAST(''' + @sToDate + ''' as date); 
	INSERT INTO @Results ([Item], [Count]) SELECT ''Total'', COUNT(*) FROM Account' + CAST(@AccountID as varchar) + '.vHazard ;

	SELECT ''' + @sFromDate + ' to ' + @sToDate + ''' AS Title,* FROM	@Results ;
	'
	PRINT @sSQL
	EXEC (@sSQL)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('rrp_OpenClosedHazards', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
