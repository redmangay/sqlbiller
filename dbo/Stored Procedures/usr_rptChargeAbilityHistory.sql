﻿
/*
    Author: Red Mangay
    Date:	  19/06/2018

*/

CREATE PROCEDURE [dbo].[usr_rptChargeAbilityHistory]
AS

/*

EXEC [dbo].[usr_rptChargeAbilityHistory]


*/
BEGIN TRY
	SET DATEFORMAT dmy;
	
	SELECT		 [Employee List].V003 EmployeeName
				--,[Group].V001 GroupDivision
				--,[Chargeability History].V024 
				,[Chargeability History].V003 ExportedDate
				,[Chargeability History].V005 StartDate
				,[Chargeability History].V006 EndDate
				,CASE WHEN [Chargeability History].V013 = '' THEN '0' ELSE ISNULL([Chargeability History].V013,'0') END AvgHrsPerWeek
				,CASE WHEN [Chargeability History].V014 = '' THEN '0' ELSE ISNULL([Chargeability History].V014,'0') END HrsScheduledLeave
				,CASE WHEN [Chargeability History].V015 = '' THEN '0' ELSE ISNULL([Chargeability History].V015,'0') END HrsOtherLeave
				,CAST(CASE WHEN [Chargeability History].V016 = '' THEN '0' ELSE ISNULL([Chargeability History].V016,'0') END as money) TimeSheet
				,CAST(CASE WHEN [Chargeability History].V017 = '' THEN '0' ELSE ISNULL([Chargeability History].V017,'0') END as money) ChargedToJobs
				,CASE WHEN [Chargeability History].V018 = '' THEN '0' ELSE ISNULL([Chargeability History].V018,'0') END ChargedToJobsPercent
				,CAST(CASE WHEN [Chargeability History].V019 = '' THEN '0' ELSE ISNULL([Chargeability History].V019,'0') END as money) Billed
				,CAST(CASE WHEN [Chargeability History].V020 = '' THEN '0' ELSE ISNULL([Chargeability History].V020,'0') END as money) Adjustments
				,CAST(CASE WHEN [Chargeability History].V021 = '' THEN '0' ELSE ISNULL([Chargeability History].V021,'0') END as money) TotalBilled
				,CASE WHEN [Chargeability History].V022 = '' THEN '0' ELSE ISNULL([Chargeability History].V022,'0') END JobProdPercent
				,CAST(CASE WHEN [Chargeability History].V023 = '' THEN '0' ELSE ISNULL([Chargeability History].V023,'0')  END as money) Chargeability--[Chargeability History].V023 Chargeability--
				,CASE WHEN [Chargeability History].V025 ='' THEN '0' ELSE ISNULL([Chargeability History].V025,'0') END Average
				, 'Avg: ' + (SELECT V035 FROM RECORD
				    WHERE TABLEID = 2743
				    AND IsActive = 1) AverageHeader
				    ,  (SELECT V039  FROM RECORD WHERE TABLEID = 2743) AS [Header] 
	FROM Record [Chargeability History]
	JOIN Record [Employee List] ON [Employee List].TableID = 2669
	   AND [Employee List].IsActive = 1
	   AND [Employee List].V027 = 1
	   AND [Employee List].V002 = 'False'
	   AND [Employee List].RecordID = [Chargeability History].V024
    -- JOIN Record [Group] ON [Group].TableID = 2727
	   --AND [Group].IsActive = 1
	   --AND [Group].RecordID = [Employee List].V022
	WHERE [Chargeability History].TableID = 2651
	  AND [Chargeability History].IsActive = 1

	  ORDER BY [Employee List].V003,cast([Chargeability History].V005 as date) asc


	  


END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('usr_rptChargeAbilityHistory', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH






















/*
== Old Script
SELECT ChargeabilityHistory.StartDate AS [Start Date], 
ChargeabilityHistory.EndDate AS [End Date], 
ChargeabilityHistory.AvgHrsPerWeek, 
ChargeabilityHistory.HrsScheduledLeave, 
ChargeabilityHistory.HrsOtherLeave,
ChargeabilityHistory.TimeSheet AS [$TimeSheet], 
ChargeabilityHistory.ChargedToJobs AS [$Jobs], 
ChargeabilityHistory.ChargedToJobsPercent,
ChargeabilityHistory.Billed AS [$Billed], 
ChargeabilityHistory.Adjustments AS [$Adjustments],
ChargeabilityHistory.TotalBilled AS [$Billed Total], 
ChargeabilityHistory.JobProdPercent, 
ChargeabilityHistory.Chargeability AS Chg,
ChargeabilityHistory.EmployeeID,
ChargeabilityHistory.ExportedTime, 
ChargeabilityHistory.ExportedDate
FROM   Account24918.vChargeabilityHistory ChargeabilityHistory;

	 */