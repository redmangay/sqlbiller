﻿
CREATE PROCEDURE		[dbo].[Renzo_TimeshetDetailsJobPerformanceReport]
(
     @RecordID		INT = NULL
     ,@JobRecordID	INT = NULL
)	
AS
BEGIN

	/*	Initial report results, stored in a temp table so an agregate can be applied and reutrned for the totals in the report	*/
	/*
	EXEC			Renzo_TimeshetDetailsJobPerformanceReport
					@RecordID = NULL
					,@JobRecordID = 1495331

	*/

	--SET				@JobRecordID = 1495331

	SELECT			[Controller].V004				AS				[Controller]
					,[Job No].RecordID				AS				JobRecordID
					,[Job No].V003					AS				[Title]
					,[Job No].V008					AS				[Job value]
					,[Job No].V001					AS				[Job No]
					,SUBSTRING([Timesheet Details].V009, 1, 10)
													AS				[Date]
					,[Initials].V004				AS				[Initials]
					,[Timesheet Details].V003		AS				[Details]
					,[Timesheet Details].V004		AS				[(X)]
					,[Timesheet Details].V005		AS				[Hr/Unit]
					,[Timesheet Details].V006		AS				[Rate]
					,[Timesheet Details].V012		AS				[GST]
					,CASE WHEN	ISNUMERIC( [Timesheet Details].V007 ) = 1 AND [Timesheet Details].V007 IS NOT NULL
						THEN	CAST( CAST([Timesheet Details].V007 AS Money) AS VARCHAR(MAX) )
						ELSE	'0.00'
					END								AS				[$ Amount]
					,[Timesheet Details].V015		AS				[Total]
					,[Timesheet Details].V008		AS				[Status]
					,CASE WHEN	ISNUMERIC( [Timesheet Details].V020 ) = 1 AND [Timesheet Details].V020 IS NOT NULL
						THEN	CAST( CAST([Timesheet Details].V020 AS Money) AS VARCHAR(MAX) )
						ELSE	'0.00'
					END								AS				[$ Billed]
					,[Timesheet Details].DateAdded
					,[Invoice Number].V005			AS				[Invoice Amount]
					,[Invoice Number].V058			AS				[Write Up/Down]
					,[Timesheet Details].V016		AS				[Invoice Number]
		INTO		#JobPerformanceReport
		FROM		[Record] [Timesheet Details] WITH (NOLOCK)
		JOIN		Record [Initials] WITH (NOLOCK) ON				[Initials].TableID = '2669'				/*	Employee_List	*/
															AND		[Initials].IsActive = 1
															AND		CAST( [Initials].RecordID AS VARCHAR(MAX)) = [Timesheet Details].V002		/*	Initials	*/
		JOIN		Record [Job No] WITH (NOLOCK) ON				[Job No].TableID = '2475'				/*	Job Information	*/
															AND		[Job No].IsActive = 1
															AND		CAST( [Job No].RecordID AS VARCHAR(MAX)) = [Timesheet Details].V001		/*	Job No	*/
		JOIN		Record [Controller] WITH (NOLOCK) ON			[Controller].TableID = '2669'		/*	Employee_List	*/
															AND		[Controller].IsActive = 1
															AND		CAST( [Controller].RecordID AS VARCHAR(MAX)) = [Job No].V026		/*	Controller	*/
		LEFT JOIN	Record [Invoice Number] WITH (NOLOCK) ON		[Invoice Number].TableID = '2711'		/*	Bill & Payment Activity	*/
															AND		[Invoice Number].IsActive = 1
															AND		CAST( [Invoice Number].RecordID AS VARCHAR(MAX)) = [Timesheet Details].V016		/*	Invoice Number	*/
		WHERE		[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
		AND			[Timesheet Details].IsActive = 1
		AND			(@RecordID IS NULL OR [Timesheet Details].RecordID = @RecordID) 
		AND			(@JobRecordID IS NULL OR [Job No].RecordID = @JobRecordID)
		AND			ISNUMERIC( [Timesheet Details].V007 ) = 1;

	SELECT			JobRecordID
					,CAST(SUM(CASE	WHEN	ISNUMERIC( #JobPerformanceReport.[$ Billed] ) = 1 AND #JobPerformanceReport.[$ Billed] IS NOT NULL
								THEN	CAST(#JobPerformanceReport.[$ Billed] AS Money)
								ELSE	0
					END) AS VARCHAR(MAX) ) AS							[$ BilledTotal]	
					,CAST( CAST( ROUND( SUM(CASE	WHEN	ISNUMERIC( #JobPerformanceReport.[$ Billed] ) = 1 AND #JobPerformanceReport.[$ Billed] IS NOT NULL
								THEN	CAST(#JobPerformanceReport.[$ Billed] AS Money)
								ELSE	0
					END) * 0.1, 2) AS MONEY ) AS VARCHAR(MAX) ) AS		[BilledGSTTotal]
					,CAST( CAST( ROUND( SUM(CASE	WHEN	ISNUMERIC( #JobPerformanceReport.[$ Billed] ) = 1 AND #JobPerformanceReport.[$ Billed] IS NOT NULL
								THEN	CAST(#JobPerformanceReport.[$ Billed] AS Money)
								ELSE	0
					END) * 1.1, 2) AS MONEY ) AS VARCHAR(MAX) ) AS		[BilledGrossTotal]	
					,CAST(SUM(CASE	WHEN	ISNUMERIC( #JobPerformanceReport.[$ Amount] ) = 1 AND #JobPerformanceReport.[$ Amount] IS NOT NULL
								THEN	CAST(#JobPerformanceReport.[$ Amount] AS Money) 
								ELSE	0
					END) AS VARCHAR(MAX) ) AS							[$ AmountTotal]		
					,CAST(SUM(CASE	WHEN	ISNUMERIC( #JobPerformanceReport.[GST] ) = 1 AND #JobPerformanceReport.[GST] IS NOT NULL
								THEN	CAST(#JobPerformanceReport.[GST] AS Money)
								ELSE	0
					END) AS VARCHAR(MAX) ) AS							[GSTTotal]	
					,CAST( CAST( ROUND(
					( SUM(CASE	WHEN	ISNUMERIC( #JobPerformanceReport.[$ Billed] ) = 1 AND #JobPerformanceReport.[$ Billed] IS NOT NULL
								THEN	CAST(#JobPerformanceReport.[$ Billed] AS Money)
								ELSE	0
					END) )
					/ ( SUM(CASE	WHEN	ISNUMERIC( #JobPerformanceReport.[$ Amount] ) = 1 AND #JobPerformanceReport.[$ Amount] IS NOT NULL
								THEN	CAST(#JobPerformanceReport.[$ Amount] AS Money) 
								ELSE	0
					END) ) * 100, 0 )  AS INT )  AS VARCHAR(MAX) ) AS	[Percentage]		
		INTO		#JobPerformanceReportTotal		
		FROM		#JobPerformanceReport
		GROUP BY	JobRecordID;

	SELECT			#JobPerformanceReport.Controller
					,#JobPerformanceReport.JobRecordID
					,#JobPerformanceReport.Title
					,'$' + #JobPerformanceReport.[Job value]	AS			[Job value]
					,#JobPerformanceReport.[Job No]
					,#JobPerformanceReport.[Date]
					,#JobPerformanceReport.Initials
					,#JobPerformanceReport.Details
					,#JobPerformanceReport.[(X)]
					,#JobPerformanceReport.[Hr/Unit]
					,#JobPerformanceReport.Rate
					,#JobPerformanceReport.GST
					,'$' + #JobPerformanceReport.[$ Amount]	AS				[$ Amount]
					,#JobPerformanceReport.Total
					,#JobPerformanceReport.[Status]
					,'$' + #JobPerformanceReport.[$ Billed] AS				[$ Billed]
					,#JobPerformanceReport.DateAdded
					,'$' + #JobPerformanceReportTotal.[$ AmountTotal]	AS	[$ AmountTotal]
					,'$' + #JobPerformanceReportTotal.[$ BilledTotal]	AS	[$ BilledTotal]
					,'$' + #JobPerformanceReportTotal.GSTTotal	AS			GSTTotal
					,#JobPerformanceReport.[Invoice Amount]	AS				[Invoice Amount]
					,#JobPerformanceReport.[Write Up/Down] AS				[Write Up/Down]
					,#JobPerformanceReportTotal.[Percentage] AS				[Percentage]
					,'$' + #JobPerformanceReportTotal.BilledGSTTotal AS		[BilledGSTTotal]
					,'$' + #JobPerformanceReportTotal.BilledGrossTotal AS	BilledGrossTotal
		FROM		#JobPerformanceReport
		JOIN		#JobPerformanceReportTotal ON			#JobPerformanceReportTotal.JobRecordID = #JobPerformanceReport.JobRecordID
		ORDER BY	#JobPerformanceReport.[Date];

	DROP TABLE		#JobPerformanceReport;
	DROP TABLE		#JobPerformanceReportTotal;

END
