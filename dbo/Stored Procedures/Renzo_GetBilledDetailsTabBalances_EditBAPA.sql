﻿-- =============================================
-- Author:		<Red Mangay>
-- Create date:	<28/11/2018>
-- Description:	<>
-- =============================================
/*

exec [Renzo_GetBilledDetailsTabBalances_EditBAPA]    15244439

*/


CREATE PROCEDURE [dbo].[Renzo_GetBilledDetailsTabBalances_EditBAPA]
(
	@RecordID int
	
)
AS
BEGIN
	

    DECLARE @TaxAmount money
    DECLARE @interimTaxAmount money
    DECLARE @TaxName varchar(max)
    DECLARE @TotalAmount money
    DECLARE @JobRecordID int
    DECLARE @TableID int
    SELECT  @TableID = TableID FROM RECORD WHERE RECORDID = @RecordID

    IF (@TableID = 2711)
    BEGIN
    SELECT @JobRecordID = V002 FROM RECORD WHERE RECORDID = @RecordID

        /*Billed Details tab: Get the Interim Tax Amount */
	  SELECT @interimTaxAmount =  CAST(CONVERT(VARCHAR(20),ISNULL(V063,0),1) AS money)  FROM Record WHERE ISACTIVE=1 AND TableID='2711' AND V061= CAST(@RecordID as varchar)
	   /*Billed Details tab: Get the Tax Amount */
	  SELECT @TaxAmount =  ISNULL(CAST(CONVERT(VARCHAR(20),V063,1) AS money),0)  FROM Record WHERE ISACTIVE=1 AND TableID='2711' AND RECORDID= @RecordID 
	  SET @TaxAmount = @TaxAmount +  ISNULL(@interimTaxAmount,0)

	   /*Timehseet Details tab: Get the Tax Name */
	  SELECT @TaxName = V001 FROM RECORD WHERE RECORDID = (SELECT V042 FROM RECORD WHERE RECORDID =@JobRecordID)

		/*Timehseet Details tab: Get the Total */
	  SELECT  @TotalAmount = ISNULL(SUM(CAST(CONVERT(VARCHAR(20),V020,1) AS money)),0) FROM Record WHERE ISACTIVE=1 AND TableID='2729' AND  V016 = CAST(@RecordID AS VARCHAR)  AND V001 = CAST(@JobRecordID AS VARCHAR)
	  SET @TotalAmount = @TotalAmount + @TaxAmount
    
	  SELECT @TaxName AS [Label], ISNULL(@TaxAmount,0.00) AS [Amount]
	  UNION ALL
	  SELECT 'Total' AS [Label], ISNULL(@TotalAmount,0.00) AS [Amount]


    END
    


END

