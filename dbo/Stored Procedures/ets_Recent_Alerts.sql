﻿

CREATE PROCEDURE [dbo].[ets_Recent_Alerts]
(
	@nAccountID int
)
AS
-- EXEC ets_Recent_Alerts @nAccountID=13

BEGIN TRY
	
	SELECT * 
		FROM (SELECT MAX(Record.DateTimeRecorded) LastWarningTime,ST.TableName,
		CONVERT(varchar, COUNT(*)) + ' Warning(s)' WarningCount,ST.TableID
		FROM Record 
		INNER JOIN [Table] ST ON Record.TableID = ST.TableID          
		WHERE (Record.WarningResults IS NOT NULL AND Record.IsActive=1 AND ST.IsActive=1)   
		AND ST.AccountID = @nAccountID
		GROUP BY ST.TableID,ST.TableName ) 
		AS Warning
		ORDER BY LastWarningTime  DESC
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Recent_Alerts', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
