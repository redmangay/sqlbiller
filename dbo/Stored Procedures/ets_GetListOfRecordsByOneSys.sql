﻿

CREATE PROCEDURE [dbo].[ets_GetListOfRecordsByOneSys]
(
	@sSystemName varchar(50),
	@nTableID int 
)
AS
/*
EXEC User_Details 2
*/
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @mySQL varchar(4000)
	
	set @mySQL='SELECT ' +  @sSystemName  + ',RecordID,WarningResults,ValidationResults FROM Record
	 WHERE IsActive=1 AND  TableID=' + convert(varchar,@nTableID ) + ' AND ' +  @sSystemName  + ' IS NOT NULL';
    
    PRINT @mySQL
    
    EXEC (@mySQL)


END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_GetListOfRecordsByOneSys', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
