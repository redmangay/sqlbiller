﻿

CREATE PROCEDURE [dbo].[Column_Audit_Summary]
(
     @nColumnID int,
     @nStartRow int = 1, 
	 @nMaxRows int = 20000000 
)
AS

BEGIN TRY
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF

     SELECT * FROM ( SELECT [Audit].DateAdded,
            [User].FirstName + ' ' + [User].LastName as [User],
            dbo.fnListChangesSC(@nColumnID, [Audit].DateAdded) as ColumnList,ROW_NUMBER() 
            OVER(ORDER BY [Audit].DateAdded DESC) as RowNum 
            FROM [Audit] JOIN
            [USER] ON [User].UserID =[Audit].UserID
            WHERE Tablename='Column'
            AND FieldName != 'DateUpdated' AND FieldName != 'LastUpdatedUserID'
            AND PrimaryKeyValue = CONVERT(varchar(1000), @nColumnID) 
            GROUP BY [Audit].DateAdded,
                  [User].FirstName + ' ' + [User].LastName,
                  dbo.fnListChangesSC(@nColumnID, [Audit].DateAdded)
                  ) AS AuditInfo
                  WHERE RowNum>=@nStartRow AND RowNum<=(@nStartRow+@nMaxRows) 
                           
            --now we need to get TotalRows number
            
            SELECT MAX(RowNum) AS TotalRows FROM ( SELECT [Audit].DateAdded,

            [User].FirstName + ' ' + [User].LastName as [User],

            dbo.fnListChangesSC(@nColumnID, [Audit].DateAdded) as Change,ROW_NUMBER() 
            OVER(ORDER BY [Audit].DateAdded DESC) as RowNum 
            FROM [Audit]
            JOIN [User] ON [User].UserID=[Audit].UserID
            WHERE Tablename='Column'
            AND FieldName != 'DateUpdated' AND FieldName != 'LastUpdatedUserID'
            AND PrimaryKeyValue = CONVERT(varchar(1000), @nColumnID) 
            GROUP BY [Audit].DateAdded,
                  [User].FirstName + ' ' + [User].LastName,
                  dbo.fnListChangesSC(@nColumnID, [Audit].DateAdded)
                  ) AS AuditInfo
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog]([Module], [ErrorMessage], [ErrorTrack], [ErrorTime], [Path]) 
		VALUES ('Column_Audit_Summary', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
