﻿

CREATE PROCEDURE [dbo].[ets_ReportItemSortOrder_Insert]
(
	@nNewID int output,
	@nReportItemID int,
	@nSortColumnID int,
	@bIsDescending bit = 0,
	@nDisplayOrder int
)
AS

BEGIN TRY
	INSERT INTO [ReportItemSortOrder]
	(
		ReportItemID, 
		SortColumnID, 
		IsDescending,
		DisplayOrder
	) VALUES (
		@nReportItemID,
		@nSortColumnID,
		@bIsDescending,
		@nDisplayOrder
	)
	SELECT @nNewID = @@IDENTITY
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_ReportItemSortOrder_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
