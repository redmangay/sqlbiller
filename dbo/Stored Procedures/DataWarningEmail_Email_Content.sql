﻿

CREATE PROCEDURE [dbo].[DataWarningEmail_Email_Content]

(
      @sKey varchar(10) = NULL
)

AS

BEGIN TRY
/*
      UNIT TESTING
      ============
      EXEC dbo.[DataWarningEmail_Email_Content]
*/
      SELECT '' DateTime,'' [Table], '' Record,'' Field, '' Value,'' WarningIF

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('DataWarningEmail_Email_Content', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
