﻿

CREATE PROCEDURE   [dbo].[ets_Table_Columns_Export]
(
	@nTableID int
)
AS
/*
EXEC ets_Table_Columns_Export 60
*/
BEGIN TRY
	SET NOCOUNT ON;	
	SET CONCAT_NULL_YIELDS_NULL OFF
	DECLARE @nExportTemplateID INT
	SELECT TOP 1 @nExportTemplateID=ExportTemplateID FROM ExportTemplate WHERE TableID=@nTableID ORDER BY ExportTemplateID DESC
	IF @nExportTemplateID IS NOT NULL
	SELECT C.*,ETI.ExportHeaderName FROM [Column] C JOIN  ExportTemplateItem ETI ON ETI.ColumnID=C.ColumnID
		WHERE C.TableID = @nTableID  AND ETI.ExportTemplateID=@nExportTemplateID
		ORDER BY ETI.ColumnIndex
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Table_Columns_Export', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
