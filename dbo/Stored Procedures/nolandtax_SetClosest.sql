﻿CREATE PROCEDURE nolandtax_SetClosest 
(
	@RecordID int
)
AS
DECLARE @Results TABLE (ID int identity, RecordID int)

-- DECLARE  @Results TABLE (ID int identity, RecordID int)
INSERT INTO @Results (RecordID)
SELECT TOP 3 P.RecordID -- , V.V007, V.V008, P.V007, P.V008, P.V009, P.V010, P.V011,
	 -- SQRT(SQUARE(ABS(CAST(V.V013 as real)- CAST(P.V014 as real))) + SQUARE(ABS(CAST(V.V014 as real) - CAST(P.V015 as real))))
FROM [Record] V
JOIN [Record] P 
	ON P.TableID = 2100 
	AND P.IsActive = 1 
	AND ISNUMERIC(P.V014)=1 
	AND ISNUMERIC(P.V015)=1
WHERE V.TableID = 2101 AND v.IsActive = 1 and ISNUMERIC(V.V013)=1 and ISNUMERIC(V.V014)=1
AND V.RecordID = @RecordID
ORDER BY SQRT(SQUARE(ABS(CAST(V.V013 as real)- CAST(P.V014 as real))) + SQUARE(ABS(CAST(V.V014 as real) - CAST(P.V015 as real)))) asc

UPDATE [Record] SET V016 = R.RecordID FROM [Record] JOIN @Results R ON R.ID = 1 WHERE [Record].RecordID = @RecordID
UPDATE [Record] SET V017 = R.RecordID FROM [Record] JOIN @Results R ON R.ID = 2 WHERE [Record].RecordID = @RecordID
UPDATE [Record] SET V018 = R.RecordID FROM [Record] JOIN @Results R ON R.ID = 3 WHERE [Record].RecordID = @RecordID

