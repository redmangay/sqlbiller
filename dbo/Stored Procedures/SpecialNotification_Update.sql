﻿ 
CREATE PROCEDURE [dbo].[SpecialNotification_Update]
(
	@nSpecialNotificationID int,
	@sNotificationHeader varchar(500)  = NULL,
	@sNotificationContent varchar(MAX)  = NULL,
	@sDelivery varchar(MAX)  = NULL
)
	/*----------------------------------------------------------------
	-- Stored Procedure: SpecialNotification_Insert
	-- Author: Red Mangay 
	-- Date: Jul 16, 2019
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	Update SpecialNotification
	SET NotificationHeader = @sNotificationHeader , 
	    NotificationContent = @sNotificationContent,
	    Delivery = @sDelivery
    WHERE SpecialNotificationID = @nSpecialNotificationID

    
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('SpecialNotification_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
