﻿CREATE PROCEDURE [dbo].[Renzo_CopyDocumentLayouts]
(
	@RecordID  int,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output
)
AS



UPDATE		[Documment Layouts]	
SET			[Documment Layouts].V002 = [Documment Layouts 2].V002,
			[Documment Layouts].V004 = [Documment Layouts 2].V004,
			[Documment Layouts].V007 = [Documment Layouts 2].V007,
			[Documment Layouts].V009 = [Documment Layouts 2].V009,
			[Documment Layouts].V011 = [Documment Layouts 2].V011,
			[Documment Layouts].V013 = [Documment Layouts 2].V013,
			[Documment Layouts].V015 = [Documment Layouts 2].V015,
			[Documment Layouts].V017 = [Documment Layouts 2].V017,
			[Documment Layouts].V019 = [Documment Layouts 2].V019,
			[Documment Layouts].V021 = [Documment Layouts 2].V021,
			[Documment Layouts].[LastUpdatedUserID] = @UserID,
			[Documment Layouts].[DateUpdated] = getdate()
FROM		[Record] [Documment Layouts] 
JOIN	    [Record] [Documment Layouts 2]	ON				1=1
								AND		[Documment Layouts 2].RecordID  = @RecordID	--	Basis Record	
								AND		[Documment Layouts 2].TableID	  = '2744'		 
								AND		[Documment Layouts 2].IsActive  = 1										
	
WHERE		 1=1
AND		[Documment Layouts].RecordID <> @RecordID
AND		[Documment Layouts].TableID = '2744'		/*	Document Layouts	*/
AND		[Documment Layouts].IsActive = 1




RETURN @@ERROR;