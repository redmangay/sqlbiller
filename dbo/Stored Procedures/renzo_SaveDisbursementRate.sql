﻿CREATE PROCEDURE		[dbo].[renzo_SaveDisbursementRate]
(
	@RecordID  int,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output
    , @Context varchar(max) = null
)
AS
BEGIN

	/*
					Reset the Job Rates to the Master Rates

					DECLARE		@Return		VARCHAR(MAX)
					EXEC		[renzo_SaveDisbursementRate] @RecordID = 2072174, @Return = @Return

					EXEC		[dbo].[dev_GetAccountSQL] @TableID = 2475, @TableName = 'Job Information', @AccountID = '24918'
					EXEC		[dbo].[dev_GetAccountSQL] @TableID = 2725, @TableName = 'Disbursement', @AccountID = '24918'
					EXEC		[dbo].[dev_GetAccountSQL] @TableID = 2713, @TableName = 'Disbursement Master', @AccountID = '24918'
					EXEC		[dbo].[dev_GetAccountSQL] @TableID = 2478, @TableName = 'Disbursement Rate', @AccountID = '24918'

					EXEC		[dbo].[dev_GetAccountSQL] @AccountID = '24918'		/*	buffalobiller1 */

					UPDATE		[Table] 
					SET			[AddRecordSP] = 'renzo_SaveDisbursementRate'
								,[SPSaveRecord]  = 'renzo_SaveDisbursementRate'
					--SELECT		*
					FROM		[Table]
					WHERE		[TableID] = 2478
	*/

	DECLARE			@JobRecordID		INT

	SELECT			[Disbursement Master].[TableID]
					,[Disbursement Master].RecordID	AS				[Record ID]
					,[Disbursement Master].V001		AS				[Item]
					,[Disbursement Master].V003		AS				[Type]
					,[Disbursement Master].V002		AS				[$/Unit]
					,[Disbursement Master].DateAdded
		INTO		#DisbursementMaster
		FROM		[Record] [Disbursement Master]
		WHERE		[Disbursement Master].TableID = '2713'		/*	Disbursement Master	*/
		AND			[Disbursement Master].IsActive = 1

	/*	Update the job to be Special; if the master rate doesnt not match the new rate. 	*/
	
	UPDATE			[Record]	/*	Disbursement Rate	*/
		SET			V005	= 'Yes'	/*	[Is Special]	*/
	--SELECT			[Disbursement Rate].V005
	--				,[Disbursement Master].[$/Unit]
	--				,[Disbursement Rate].V002
		FROM		[Record] [Disbursement Rate]
		LEFT JOIN	#DisbursementMaster [Disbursement Master] ON	[Disbursement Master].TableID = '2713'		/*	Disbursement Master	*/
															AND		[Disbursement Master].[Record ID] = [Disbursement Rate].V008		/*	V008	*/
		WHERE		[Disbursement Rate].TableID = '2478'		/*	Disbursement Rate	*/
		AND			[Disbursement Rate].IsActive = 1
		AND			ISNUMERIC( ISNULL( [Disbursement Rate].V008, 0 ) ) = 1
		AND			[Disbursement Rate].RecordID = @RecordID
		AND			(	[Disbursement Master].[$/Unit] <> [Disbursement Rate].V002
					OR	[Disbursement Rate].V008 IS NULL
					)
		
	DROP TABLE		#DisbursementMaster;

	/*	Update the job to be Special rather than Default	*/
	
	SELECT			@JobRecordID = [Disbursement Rate].V004	/*	[Job No]	*/
		FROM		[Record] [Disbursement Rate]
		WHERE		[Disbursement Rate].TableID = '2478'		/*	Disbursement Rate	*/
		AND			[Disbursement Rate].RecordID = @RecordID
		AND			[Disbursement Rate].IsActive = 1
		AND			[Disbursement Rate].V005	= 'Yes'	/*	[Is Special]	*/

	UPDATE			[Record]	/*	Disbursement Rate	*/
		SET			V012 = 'TRUE'	/*	[Special Rates of Charge]	*/
		FROM		[Record] [Job Information]
		WHERE		[Job Information].TableID = '2475'		/*	Job Information	*/
		AND			[Job Information].IsActive = 1
		AND			[Job Information].RecordID = @JobRecordID

END
