﻿
CREATE PROCEDURE [dbo].[usr_rptAllInvoices]
(
	@ReportType  int,
	@DateFrom DATE,
	@DateTo	 DATE,
	@IncludeBadDebtJob bit = 1
	
)
AS

--DECLARE @RecordIDx int = 2709836
 /*
 
    exec [dbo].[usr_rptAllInvoices] 2,  @DateFrom = '01/01/2020', @DateTo = '19/02/2020',@IncludeBadDebtJob =0
 
 */



BEGIN TRY



  DECLARE @NonTaxRecordID int

 SELECT @NonTaxRecordID = ISNULL(RecordID,'-1') FROM Record
 WHERE TABLEID = 2696
 AND ISACTIVE = 1
 AND V001 LIKE '%None%'



 DECLARE @StatementWhere varchar(max) = ''
 DECLARE @PreviouslyInvoiceAmount varchar(max) = ' ,'''' AS [Previous Invoice Amount] '
 DECLARE @PreviouslyInvoiceAmountJOIN VARCHAR(MAX) = ''
 DECLARE @IncludeBadDebtJobWhere VARCHAR(MAX) = ''
 IF @ReportType = 2
 BEGIN
    --DECLARE @DateToday date = CONVERT(varchar(20),DATEADD(DAY,-30, CONVERT(VARCHAR(20), GETDATE(),103)),103)
    --SET @StatementWhere =  ' AND CONVERT(DATE,[Bill & Payment Activity].V004) <= ''' + CAST(@DateToday AS VARCHAR) + ''''
    
 DECLARE @OverdueData Table(JobRecordID int, InvoiceAmount money, PaymentAmount money,Outstanding money)
 insert into @OverdueData
 SELECT distinct [BAPA].V002,sum(isnull(CAST([BAPA].[V005] as money),0)) AS [Invoice Amount]
		,sum(isnull(CASE WHEN [BAPA].[V029] IN ('Bad Debt Entry') THEN '' ELSE cast([BAPA].[V007] as money) END,0)) AS [Payment Amount]
		 ,sum(CAST([BAPA].[V005] as money)) - sum(isnull(CASE WHEN [BAPA].[V029] IN ('Bad Debt Entry') THEN '' ELSE cast([BAPA].[V007] as money) END,0))
 FROM RECORD [BAPA]
 --JOIN RECORD JI ON JI.TABLEID = 2475
 --AND JI.ISACTIVE =1
 --AND JI.RECORDID = CAST([BAPA].V002 AS INT)
 WHERE [BAPA].TABLEID = 2711
 AND [BAPA].ISACTIVE = 1
 and isnumeric([BAPA].V002) =1
 group by [BAPA].V002

 --select * from @OverdueData
 --WHERE JOBRECORDID =  16675251
 --where Outstanding > 0





 DECLARE @OverdueJobs varchar(max)
 SELECT  @OverdueJobs = LEFT(JobRecordIDs, LEN(JobRecordIDs) - 1) 
								FROM ( SELECT CONVERT(varchar(50), JobRecordID) + ',' FROM @OverdueData  where Outstanding > 0 FOR XML PATH ('') ) C (JobRecordIDs)
--select @OverdueJobs

    SET @StatementWhere = ' AND [Bill & Payment Activity].V002 IN ('+ @OverdueJobs +' ) '
					   
					  
 END

 IF @IncludeBadDebtJob = 0
 BEGIN
 SET @IncludeBadDebtJobWhere = '  AND [Job Information].[V021] NOT IN (''Bad Debt'') '
 END

 DECLARE @Header varchar(max) = ' (SELECT OptionValue FROM SYSTEMOPTION WHERE OPTIONKEY = ''FilesLocation'') + ''/UserFiles/AppFiles/'' + 
			 CASE WHEN [Job Information].V042 = '''+cast( @NonTaxRecordID as varchar) +'''
			 THEN CASE WHEN [Bill & Payment Activity].[V029] = ''Invoice'' THEN [Document Layout].V048 ELSE [Document Layout].V050 END
			 ELSE CASE WHEN [Bill & Payment Activity].[V029] = ''Invoice'' THEN [Document Layout].V039 ELSE [Document Layout].V035 END END AS HeaderImage'


  IF @ReportType = 1
 BEGIN
    SET @StatementWhere =  ' AND [Bill & Payment Activity].V029 LIKE ''%Invoice%'' 
					    AND CONVERT(DATE,[Bill & Payment Activity].V004) >= ''' + CAST(@DateFrom AS VARCHAR) + '''
					    AND CONVERT(DATE,[Bill & Payment Activity].V004) <= ''' + CAST(@DateTo AS VARCHAR) + ''' '

    --SET @PreviouslyInvoiceAmountJOIN = ' INNER JOIN [Record] [Previous Bill & Payment Activity] ON [Previous Bill & Payment Activity].[TableID] = 2711 
				--AND [Previous Bill & Payment Activity].RecordID = 
				--(SELECT TOP 1 RecordID 
				--	FROM [Record] 
				--		WHERE V002 = CAST([Job Information].[RecordID] AS VARCHAR) AND 
				--			RecordID < [Bill & Payment Activity].[RecordID] 
				--				ORDER BY [RecordID] DESC ) '

    --SET @PreviouslyInvoiceAmount = ' ,ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 --WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 AND CONVERT(DATE,[Bills And Payment Activity].V004) < '''+ CAST(@DateFrom AS VARCHAR) + '''
				--AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0) AS [Previous Invoice Amount]  ' 

   SET  @PreviouslyInvoiceAmount = ' ,(SELECT ISNULL(SUM(CAST(ISNULL(V005,0) AS MONEY)),0) 
			 FROM RECORD [Previous Bill & Payment Activity] 
				WHERE [Previous Bill & Payment Activity].TableID = 2711 AND
				[Previous Bill & Payment Activity].IsActive = 1 AND  
				[Previous Bill & Payment Activity].V002 = CAST([Job Information].[RecordID] AS VARCHAR) AND
				[Previous Bill & Payment Activity].V029 IN (''Invoice'',''Interim Invoice'',''Credit Entry'',''Interim Credit'') AND
				CAST([Previous Bill & Payment Activity].V004 AS DATE) < CAST([Bill & Payment Activity].[V004] AS DATE)) AS [Previous Invoice Amount]'
 END

  IF @ReportType = 2
  BEGIN
  SET @Header = '(SELECT OptionValue FROM SYSTEMOPTION WHERE OPTIONKEY = ''FilesLocation'') + ''/UserFiles/AppFiles/'' + [Document Layout].V041  AS HeaderImage'
  END



 DECLARE @SQL varchar(max) = ''

 SET @SQL = 'SET DATEFORMAT dmy
 SELECT 
		cast([Bill & Payment Activity].[V004] as date) AS [Date]
		,[Job Information].[V001] AS [Job Number]
		,[Bill & Payment Activity].[V017] AS [Invoice No]
		,[Client Information].[V017] as [Client Name]  --[Office Information].[V001] AS [Client Name]
		,[Client Information].[V002] AS [Client Address1]
		,isnull([Client Information].[V015],'''') AS [Client Address2]
		,[Client Information].[V003] AS [Client Suburb]
		,CASE WHEN [Client Information].[V004] IS NULL THEN '''' ELSE [Client Information].[V004] + '' '' +  [Client Information].[V005] END AS [Client State]
		,[Client Information].[V006] AS [Country]
		,[Client Information].V019 as  [Attention Name]  --[Client Contact].V004 + '' '' + [Client Contact].V005 + '' '' + [Client Contact].V006 AS [Attention Name]
		,[Job Information].[V003] AS [Job Title]
		,ISNULL([Tax Table].[V001],'''') AS [Tax Name]
			,CAST(ISNULL([Bill & Payment Activity].[V005],0) as money)  AS [Invoice Amount]
		,ISNULL(CASE WHEN [Bill & Payment Activity].[V029] IN (''Bad Debt Entry'') THEN '''' ELSE cast(ISNULL([Bill & Payment Activity].[V007],0) as money) END,0) AS [Payment Amount]
		,ISNULL(cast(ISNULL([Job Information].[V008],0.00) as money),''0.00'') AS [Job Value]
		,ISNULL([Job Currency].[V001],'''') AS [Job Currency]
		,ISNULL([Job Currency].[V003],ISNULL([Job Currency].[V001],''''))  AS [Currency Symbol]
		,CASE WHEN [Job Information].[V009] = ''FALSE'' THEN ''Estimate'' ELSE '''' END AS [Estimate]
		--,CAST(ISNULL([Previous Bill & Payment Activity].[V005],0.00) as money) AS [Previous Invoice Amount]
		' + @PreviouslyInvoiceAmount +'
		,CAST(ISNULL([Bill & Payment Activity].[V063],0) as money) AS [GST]
		,[Bill & Payment Activity].[V015] AS [Description]

		,/*CASE WHEN LEN(LTRIM(RTRIM([Bill & Payment Activity].[V028]))) = 0 THEN 0 ELSE */ 
		CASE  
			 WHEN CAST([Bill & Payment Activity].[V028] AS MONEY) = '''' THEN ''0'' ELSE
			 ISNULL([Bill & Payment Activity].[V028],''0'') 
		END
		
		AS [Percentage Complete]
		
		
		,CASE WHEN '+ CAST(@ReportType AS VARCHAR) +' = 2 THEN [Document Layout].V017 ELSE 		
		 CASE WHEN [Bill & Payment Activity].[V029] IN (''Invoice'') THEN [Document Layout].V015 ELSE [Document Layout].V011 END END AS Notes
		, [Job Group].V001 AS [Group]
		--,[Client Information].[V001]
		--,CAST([Job Information].[RecordID] AS VARCHAR)
		--,[Client Information].[V013]

		  ,CASE WHEN [Job Information].[V009] = ''TRUE'' THEN ''% Complete'' ELSE '''' END AS [% Complete],
		   CASE WHEN [Job Information].[V009] = ''TRUE'' THEN convert(money,ISNULL([Bill & Payment Activity].V028,0)) / cast(''100.00'' as money) ELSE '''' END AS [% Complete Value]
 , '+ @Header +'
 FROM [Record] [Bill & Payment Activity] WITH (NOLOCK) 
 JOIN [Record] [Job Information] ON [Job Information].[TableID] = 2475
			   AND [Job Information].[RecordID] = CAST([Bill & Payment Activity].[V002] as int)
			   AND [Job Information].[IsActive] = 1
			   --AND [Job Information].v009 =''TRUE''
			   ' + @IncludeBadDebtJobWhere +'
			   
 --LEFT JOIN [Record] [Office Information] ON [Office Information].[TableID] = 2474
	--		   AND [Office Information].[RecordID] = CAST([Job Information].[V024] as int) 
	--		   AND [Office Information].[IsActive] = 1

 LEFT JOIN [Record] [Client Information] ON [Client Information].[TableID] = 2476
			   AND CAST([Client Information].[V013] as int) = [Job Information].[RecordID]
			   AND [Client Information].[IsActive] = 1

			   
 LEFT JOIN [Record] [Job Currency] ON [Job Currency].[TableID] = 2665
			   AND [Job Currency].[RecordID] = CAST([Job Information].[V034] as int) 
			   AND [Job Currency].[IsActive] = 1

 LEFT JOIN [Record] [Tax Table]	ON	1=1
				AND		[Tax Table].[RecordID] = CAST([Job Information].V042 as int)	
				AND		[Tax Table].TableID = 2696		/*	Tax Rates	*/
				AND		[Tax Table].IsActive = 1

 LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
				AND			[Job Group].IsActive = 1
				AND		[Job Group].RecordID = CAST([Job Information].V027 as int)		/*	Group	*/

LEFT JOIN Record [Document Layout] WITH (NOLOCK) ON [Document Layout].TableID = 2744	/*	Document Layout	*/
				AND			[Document Layout].IsActive = 1
				AND		CAST([Document Layout].V005 as int) = [Job Group].RecordID		/*	Group	*/

LEFT JOIN Record [Client Contact] WITH (NOLOCK) ON [Client Contact].TableID = 2657	/*	Document Layout	*/
				AND			[Client Contact].IsActive = 1
				AND		[Client Contact].RecordID = CAST([Client Information].V008 as int)		/*	Group	*/
			 								
 WHERE [Bill & Payment Activity].[TableID] = 2711	AND
	   [Bill & Payment Activity].[IsActive] = 1 
	  --and [Job Information].[V001] =  ''TD029-60''
	  -- AND [Bill & Payment Activity].[V002] = ''2705119''
	  '+ @StatementWhere + '
	  	  
	  ORDER BY cast([Bill & Payment Activity].[V004] as date)' -- end quote

	  


	  EXEC(@SQL);
	  PRINT @SQL


	  ----------------

	  ----------------

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('usr_rptAllInvoices', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH