﻿
--------------------------------------------------------------------------------------
-- PLEASE DO NOT CHANGE THE SCRIPT ABOVE - Simply set @Sequence & @Description
-- and paste your script in BELOW:
--------------------------------------------------------------------------------------

/* ============================================================================================================== === == */

	CREATE PROCEDURE [dbo].[dbg_Import_Mapping] 
		-- Add the parameters for the stored procedure here
		@TableID int, 
		@BatchID int
	AS
	-- JB 04 Apr 2017 : Updated to use a table variable to reduce the number of times the [Column] table 
	-- KG 29 May 2017 : Replaced Account24929.[vSystem Column Lookup] View with [EMD_System_Column_Lookup] table
	-- RP 23 Jul 2019 : Added ms/cm and us/cm convertion
	BEGIN

		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
		SET NOCOUNT ON;

		-- Performance monitoring
		-- DROP TABLE _PerfLog
		-- CREATE TABLE _PerfLog (Process int, Point varchar(10), TimeTaken int, ExtraInfo varchar(MAX))
		DECLARE @bDebug bit = 1 --<<<<<<< turn this off when not needed
		DECLARE @Process int = RAND() * 100000
		DECLARE @StartTime datetime = GETDATE()
		-- IF @bDebug = 1 INSERT INTO _PerfLog (Process, Point, TimeTaken) VALUES (@Process, 'Start', DATEDIFF(MILLISECOND, @StartTime, GETDATE()))


		-- Now using WITH(NOLOCK) hint.. SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		DECLARE @sql varchar(MAX)

		-- Table variable to hold values:
		DECLARE @t TABLE ([SystemName] varchar(50), [DisplayName] varchar(50))

		INSERT INTO @t ([SystemName], [DisplayName])
			SELECT [SystemName], [DisplayName]
				FROM [Column] WITH(NOLOCK)
				WHERE ([TableID] = @TableID)

		--IF @bDebug = 1 INSERT INTO _PerfLog (Process, Point, TimeTaken) VALUES (@Process, 'Insert', DATEDIFF(MILLISECOND, @StartTime, GETDATE()))


		DECLARE @SourceAccountColumnName AS varchar(50)
		SELECT @SourceAccountColumnName = [SystemName] FROM @t WHERE [DisplayName] = 'Incoming Site'

		DECLARE @SourceTableColumnName AS varchar(50)
		SELECT @SourceTableColumnName = [SystemName] FROM @t WHERE [DisplayName] = 'Incoming Sample Type'

		DECLARE @SourceAnalyteColumnName AS varchar(50)
		SELECT @SourceAnalyteColumnName = [SystemName] FROM @t WHERE [DisplayName] = 'Incoming Analyte Name'

		DECLARE @SourceTotalOrDissolvedColumnName AS varchar(50)
		SELECT @SourceTotalOrDissolvedColumnName = [SystemName] FROM @t WHERE [DisplayName] = 'Total Or Dissolved'

		DECLARE @TargetAccountColumnName AS varchar(50)
		SELECT @TargetAccountColumnName = [SystemName] FROM @t WHERE [DisplayName] = 'Target Account'

		DECLARE @TargetTableColumnName AS varchar(50)
		SELECT @TargetTableColumnName = [SystemName] FROM @t WHERE [DisplayName] = 'Target Table'

		DECLARE @TargetColumnColumnName AS varchar(50)
		SELECT @TargetColumnColumnName = [SystemName] FROM @t WHERE [DisplayName] = 'Target Column'

		DECLARE @ExpectedUnitColumnName AS varchar(50)
		SELECT @ExpectedUnitColumnName = [SystemName] FROM @t WHERE [DisplayName] = 'Expected Unit'

		DECLARE @SampleValueColumnName AS varchar(50) 
		SELECT @SampleValueColumnName = [SystemName] FROM @t WHERE [DisplayName] = 'Sample Value' 

		DECLARE @ValueUnitColumnName AS varchar(50)
		SELECT @ValueUnitColumnName = [SystemName] FROM @t WHERE [DisplayName] = 'Value Unit'

		--IF @bDebug = 1 INSERT INTO _PerfLog (Process, Point, TimeTaken) VALUES (@Process, 'Strings', DATEDIFF(MILLISECOND, @StartTime, GETDATE()))


		SET @sql = 'UPDATE [TempRecord] SET [RejectReason]=''Site is Required''' +
			' WHERE (' + @SourceAccountColumnName + ' IS NULL OR LEN(' + @SourceAccountColumnName + ') = 0) AND [BatchID] = ' + CAST(@BatchID AS varchar(20))
		EXEC (@sql)

		SET @sql = 'UPDATE [TempRecord] SET [RejectReason]=''Sample Type is Required''' +
			' WHERE (' + @SourceTableColumnName + ' IS NULL OR LEN(' + @SourceTableColumnName + ') = 0) AND [BatchID] = ' + CAST(@BatchID AS varchar(20))
		EXEC (@sql)

		SET @sql = 'UPDATE [TempRecord] SET [RejectReason]=''Analyte Name is Required''' +
			' WHERE (' + @SourceAnalyteColumnName + ' IS NULL OR LEN(' + @SourceAnalyteColumnName + ') = 0) AND [BatchID] = ' + CAST(@BatchID AS varchar(20))
		EXEC (@sql)

			--Commented by KG 29/5/2017 Ticket 2494
			--SET @sql = 'UPDATE [TempRecord] SET [' + @TargetColumnColumnName + '] = Account24929.[vSystem Column Lookup].[NameOnImport (auto populated)],' +
			--' [' + @TargetTableColumnName +  + '] = Account24929.[vSystem Column Lookup].[TableID (auto populated)],' +
			--' [' + @ExpectedUnitColumnName +  + '] = [ALS_Mapping].[Expected Units]' +
			--' FROM [TempRecord] LEFT JOIN [ALS_Mapping] ON [TempRecord].[' + @SourceAccountColumnName + '] = [ALS_Mapping].[Incoming Site]' +
			--' AND [TempRecord].[' +  @SourceTableColumnName + '] = [ALS_Mapping].[Incoming Sample Type]' +
			--' AND [TempRecord].[' + @SourceAnalyteColumnName + '] = [ALS_Mapping].[Incoming Analyte Name]' +
			--' AND (([TempRecord].[' + @SourceTotalOrDissolvedColumnName + '] = [ALS_Mapping].[Total Or Dissolved])' +
			--' OR (([TempRecord].[' + @SourceTotalOrDissolvedColumnName + '] IS NULL) AND ([ALS_Mapping].[Total Or Dissolved] IS NULL)))' +
			--' LEFT JOIN Account24929.[vSystem Column Lookup] ON [ALS_Mapping].[Target Column ID] = Account24929.[vSystem Column Lookup].[Record ID]' +
			--' WHERE [RejectReason] IS NULL AND [BatchID]=' + CAST(@BatchID AS varchar(20))

		SET @sql = 'UPDATE [TempRecord] SET [' + @TargetColumnColumnName + '] = [EMD_System_Column_Lookup].[NameOnImport (auto populated)],' +
			' [' + @TargetTableColumnName +  + '] = [EMD_System_Column_Lookup].[TableID (auto populated)],' +
			' [' + @ExpectedUnitColumnName +  + '] = [ALS_Mapping].[Expected Units]' +
			' FROM [TempRecord] LEFT JOIN [ALS_Mapping] ON [TempRecord].[' + @SourceAccountColumnName + '] = [ALS_Mapping].[Incoming Site]' +
			' AND [TempRecord].[' +  @SourceTableColumnName + '] = [ALS_Mapping].[Incoming Sample Type]' +
			' AND [TempRecord].[' + @SourceAnalyteColumnName + '] = [ALS_Mapping].[Incoming Analyte Name]' +
			' AND (([TempRecord].[' + @SourceTotalOrDissolvedColumnName + '] = [ALS_Mapping].[Total Or Dissolved])' +
			' OR (([TempRecord].[' + @SourceTotalOrDissolvedColumnName + '] IS NULL) AND ([ALS_Mapping].[Total Or Dissolved] IS NULL)))' +
			' LEFT JOIN [EMD_System_Column_Lookup] ON [ALS_Mapping].[Target Column ID] = [EMD_System_Column_Lookup].[Record ID]' +
			' WHERE [RejectReason] IS NULL AND [BatchID]=' + CAST(@BatchID AS varchar(20))

		-- PRINT @sql
		EXEC (@sql)

		--IF @bDebug = 1 INSERT INTO _PerfLog (Process, Point, TimeTaken, ExtraInfo) VALUES (@Process, 'Exec 1', DATEDIFF(MILLISECOND, @StartTime, GETDATE()), @sql)

		SET @sql = 'UPDATE [TempRecord] SET [RejectReason]=''Mapping Not Found''' +
			' WHERE [RejectReason] IS NULL AND (' + @TargetColumnColumnName + ' IS NULL OR ' + @TargetTableColumnName + ' IS NULL)  AND [BatchID] = ' + CAST(@BatchID AS varchar(20))
		EXEC (@sql)
		--IF @bDebug = 1 INSERT INTO _PerfLog (Process, Point, TimeTaken) VALUES (@Process, 'Exec 2', DATEDIFF(MILLISECOND, @StartTime, GETDATE()))

		--SET @sql = 'UPDATE [TempRecord] SET' +
		--	' ' + @SampleValueColumnName + ' =' +
		--		' CASE' +
		--			' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''ug/l'' AND LOWER(' + @ValueUnitColumnName + ') = ''mg/l'' THEN' +
		--				' CAST(CAST(' + @SampleValueColumnName + ' AS decimal(18, 4)) * 1000 AS varchar(MAX))' +
		--			' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''Ág/l'' AND LOWER(' + @ValueUnitColumnName + ') = ''mg/l'' THEN' +
		--				' CAST(CAST(' + @SampleValueColumnName + ' AS decimal(18, 4)) * 1000 AS varchar(MAX))' +
		--			' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''mg/l'' AND LOWER(' + @ValueUnitColumnName + ') = ''ug/l'' THEN' +
		--				' CAST(CAST(' + @SampleValueColumnName + ' AS decimal(18, 4)) / 1000 AS varchar(MAX))' +
		--			' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''mg/l'' AND LOWER(' + @ValueUnitColumnName + ') = ''Ág/l'' THEN' +
		--				' CAST(CAST(' + @SampleValueColumnName + ' AS decimal(18, 4)) / 1000 AS varchar(MAX))' +
		--			' ELSE ' + @SampleValueColumnName +
		--		' END,' +
		--	' RejectReason =' +
		--		' CASE' +
		--			' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''ug/l'' AND ((' + @ValueUnitColumnName + ' IS NULL ) OR (LOWER(' + @ValueUnitColumnName + ') NOT IN (''mg/l'', ''ug/l'', ''Ág/l''))) THEN' +
		--				' ''Unexpected value units''' +
		--			' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''Ág/l'' AND ((' + @ValueUnitColumnName + ' IS NULL ) OR (LOWER(' + @ValueUnitColumnName + ') NOT IN (''mg/l'', ''ug/l'', ''Ág/l''))) THEN' +
		--				' ''Unexpected value units''' +
		--			' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''mg/l'' AND ((' + @ValueUnitColumnName + ' IS NULL ) OR (LOWER(' + @ValueUnitColumnName + ') NOT IN (''mg/l'', ''ug/l'', ''Ág/l''))) THEN' +
		--				' ''Unexpected value units''' +
		--			' ELSE NULL' +
		--		' END' + 
		--	' WHERE [RejectReason] IS NULL AND ISNUMERIC(' + @SampleValueColumnName + ') = 1 AND [BatchID] = ' + CAST(@BatchID AS varchar(20))

		SET @sql = 'UPDATE [TempRecord] SET' +
			' ' + @SampleValueColumnName + ' =' +
				' CASE' +
					' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''ug/l'' AND LOWER(' + @ValueUnitColumnName + ') = ''mg/l'' THEN' +
						' CASE' +
							' WHEN LEFT([' + @SampleValueColumnName + '], 1) = ''<'' OR LEFT([' + @SampleValueColumnName + '], 1) = ''>'' THEN LEFT([' +
									@SampleValueColumnName + '], 1) + CAST(dbo.dbgNum(SUBSTRING([' + @SampleValueColumnName + '], 2, 999)) * 1000 as varchar(MAX))' +
							' WHEN ISNUMERIC([' + @SampleValueColumnName + ']) = 1 THEN CAST(dbo.dbgNum(' + @SampleValueColumnName + ') * 1000 AS varchar(MAX))' +
							' ELSE ''''' +
						' END' +
					' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''Ág/l'' AND LOWER(' + @ValueUnitColumnName + ') = ''mg/l'' THEN' +
						' CASE' +
							' WHEN LEFT([' + @SampleValueColumnName + '], 1) = ''<'' OR LEFT([' + @SampleValueColumnName + '], 1) = ''>'' THEN LEFT([' +
									@SampleValueColumnName + '], 1) + CAST(dbo.dbgNum(SUBSTRING([' + @SampleValueColumnName + '], 2, 999)) * 1000 as varchar(MAX))' +
							' WHEN ISNUMERIC([' + @SampleValueColumnName + ']) = 1 THEN CAST(dbo.dbgNum(' + @SampleValueColumnName + ') * 1000 AS varchar(MAX))' +
							' ELSE ''''' +
						' END' +
					' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''mg/l'' AND LOWER(' + @ValueUnitColumnName + ') = ''ug/l'' THEN' +
						' CASE' +
							' WHEN LEFT([' + @SampleValueColumnName + '], 1) = ''<'' OR LEFT([' + @SampleValueColumnName + '], 1) = ''>'' THEN LEFT([' +
									@SampleValueColumnName + '], 1) + CAST(dbo.dbgNum(SUBSTRING([' + @SampleValueColumnName + '], 2, 999)) / 1000 as varchar(MAX))' +
							' WHEN ISNUMERIC([' + @SampleValueColumnName + ']) = 1 THEN CAST(dbo.dbgNum(' + @SampleValueColumnName + ') / 1000 AS varchar(MAX))' +
							' ELSE ''''' +
						' END' +
					' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''mg/l'' AND LOWER(' + @ValueUnitColumnName + ') = ''Ág/l'' THEN' +
						' CASE' +
							' WHEN LEFT([' + @SampleValueColumnName + '], 1) = ''<'' OR LEFT([' + @SampleValueColumnName + '], 1) = ''>'' THEN LEFT([' +
									@SampleValueColumnName + '], 1) + CAST(dbo.dbgNum(SUBSTRING([' + @SampleValueColumnName + '], 2, 999)) / 1000 as varchar(MAX))' +
							' WHEN ISNUMERIC([' + @SampleValueColumnName + ']) = 1 THEN CAST(dbo.dbgNum(' + @SampleValueColumnName + ') / 1000 AS varchar(MAX))' +
							' ELSE ''''' +
						' END' +
					' ELSE ' + @SampleValueColumnName +
				' END,' +
			' RejectReason =' +
				' CASE' +
					' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''ug/l'' AND ((' + @ValueUnitColumnName + ' IS NULL ) OR (LOWER(' + @ValueUnitColumnName + ') NOT IN (''mg/l'', ''ug/l'', ''Ág/l''))) THEN' +
						' ''Unexpected value units''' +
					' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''Ág/l'' AND ((' + @ValueUnitColumnName + ' IS NULL ) OR (LOWER(' + @ValueUnitColumnName + ') NOT IN (''mg/l'', ''ug/l'', ''Ág/l''))) THEN' +
						' ''Unexpected value units''' +
					' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''mg/l'' AND ((' + @ValueUnitColumnName + ' IS NULL ) OR (LOWER(' + @ValueUnitColumnName + ') NOT IN (''mg/l'', ''ug/l'', ''Ág/l''))) THEN' +
						' ''Unexpected value units''' +
					' ELSE NULL' +
				' END' + 
			' WHERE [RejectReason] IS NULL AND [BatchID] = ' + CAST(@BatchID AS varchar(20))

		--PRINT @sql
		EXEC (@sql)
		--IF @bDebug = 1 INSERT INTO _PerfLog (Process, Point, TimeTaken) VALUES (@Process, 'Exec 3', DATEDIFF(MILLISECOND, @StartTime, GETDATE()))

		-- RP Added Ticket 4727
		SET @sql = 'UPDATE [TempRecord] SET' +
			' ' + @SampleValueColumnName + ' =' +
				' CASE' +
					' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''us/cm'' AND LOWER(' + @ValueUnitColumnName + ') = ''ms/cm'' THEN' +
						' CASE' +
							' WHEN LEFT([' + @SampleValueColumnName + '], 1) = ''<'' OR LEFT([' + @SampleValueColumnName + '], 1) = ''>'' THEN LEFT([' +
									@SampleValueColumnName + '], 1) + CAST(dbo.dbgNum(SUBSTRING([' + @SampleValueColumnName + '], 2, 999)) * 1000 as varchar(MAX))' +
							' WHEN ISNUMERIC([' + @SampleValueColumnName + ']) = 1 THEN CAST(dbo.dbgNum(' + @SampleValueColumnName + ') * 1000 AS varchar(MAX))' +
							' ELSE ''''' +
						' END' +
					' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''Ás/cm'' AND LOWER(' + @ValueUnitColumnName + ') = ''ms/cm'' THEN' +
						' CASE' +
							' WHEN LEFT([' + @SampleValueColumnName + '], 1) = ''<'' OR LEFT([' + @SampleValueColumnName + '], 1) = ''>'' THEN LEFT([' +
									@SampleValueColumnName + '], 1) + CAST(dbo.dbgNum(SUBSTRING([' + @SampleValueColumnName + '], 2, 999)) * 1000 as varchar(MAX))' +
							' WHEN ISNUMERIC([' + @SampleValueColumnName + ']) = 1 THEN CAST(dbo.dbgNum(' + @SampleValueColumnName + ') * 1000 AS varchar(MAX))' +
							' ELSE ''''' +
						' END' +
					' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''ms/cm'' AND LOWER(' + @ValueUnitColumnName + ') = ''us/cm'' THEN' +
						' CASE' +
							' WHEN LEFT([' + @SampleValueColumnName + '], 1) = ''<'' OR LEFT([' + @SampleValueColumnName + '], 1) = ''>'' THEN LEFT([' +
									@SampleValueColumnName + '], 1) + CAST(dbo.dbgNum(SUBSTRING([' + @SampleValueColumnName + '], 2, 999)) / 1000 as varchar(MAX))' +
							' WHEN ISNUMERIC([' + @SampleValueColumnName + ']) = 1 THEN CAST(dbo.dbgNum(' + @SampleValueColumnName + ') / 1000 AS varchar(MAX))' +
							' ELSE ''''' +
						' END' +
					' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''ms/cm'' AND LOWER(' + @ValueUnitColumnName + ') = ''Ás/cm'' THEN' +
						' CASE' +
							' WHEN LEFT([' + @SampleValueColumnName + '], 1) = ''<'' OR LEFT([' + @SampleValueColumnName + '], 1) = ''>'' THEN LEFT([' +
									@SampleValueColumnName + '], 1) + CAST(dbo.dbgNum(SUBSTRING([' + @SampleValueColumnName + '], 2, 999)) / 1000 as varchar(MAX))' +
							' WHEN ISNUMERIC([' + @SampleValueColumnName + ']) = 1 THEN CAST(dbo.dbgNum(' + @SampleValueColumnName + ') / 1000 AS varchar(MAX))' +
							' ELSE ''''' +
						' END' +
					' ELSE ' + @SampleValueColumnName +
				' END,' +
			' RejectReason =' +
				' CASE' +
					' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''us/cm'' AND ((' + @ValueUnitColumnName + ' IS NULL ) OR (LOWER(' + @ValueUnitColumnName + ') NOT IN (''ms/cm'', ''us/cm'', ''Ás/cm''))) THEN' +
						' ''Unexpected value units''' +
					' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''Ás/cm'' AND ((' + @ValueUnitColumnName + ' IS NULL ) OR (LOWER(' + @ValueUnitColumnName + ') NOT IN (''ms/cm'', ''us/cm'', ''Ás/cm''))) THEN' +
						' ''Unexpected value units''' +
					' WHEN LOWER(' + @ExpectedUnitColumnName + ') = ''ms/cm'' AND ((' + @ValueUnitColumnName + ' IS NULL ) OR (LOWER(' + @ValueUnitColumnName + ') NOT IN (''ms/cm'', ''us/cm'', ''Ás/cm''))) THEN' +
						' ''Unexpected value units''' +
					' ELSE NULL' +
				' END' + 
			' WHERE [RejectReason] IS NULL AND [BatchID] = ' + CAST(@BatchID AS varchar(20))

		--PRINT @sql
		EXEC (@sql)
		-- END MODIFICATION


		--select @TargetTableColumnName
		SET @sql = 'SELECT DISTINCT [' + @TargetTableColumnName + '] FROM [TempRecord]' +
			' WHERE RejectReason IS NULL AND [BatchID] = ' + CAST(@BatchID AS varchar(20))
		EXEC (@sql)

		--kg 23/6/2017 Ticket 2740
		DECLARE @sql2 varchar(max) 
		SET @sql2 = 'SELECT DISTINCT [' + @SourceAccountColumnName + '] FROM [TempRecord]' +
			' WHERE RejectReason IS NOT NULL AND [BatchID] = ' + CAST(@BatchID AS varchar(20))
		EXEC (@sql2)

		--IF @bDebug = 1 INSERT INTO _PerfLog (Process, Point, TimeTaken) VALUES (@Process, 'Exec 4', DATEDIFF(MILLISECOND, @StartTime, GETDATE()))
	END
