﻿

CREATE PROCEDURE [dbo].[MR_SavePatient]
(
	@RecordID int,
	@UserID int,
	@Return varchar(MAX) output
)
AS
BEGIN TRY
	DECLARE @ErrorNumber int, @RecordCount int
	UPDATE [Record] SET V002 = 'OG:' + V001 WHERE RecordID = @RecordID
	SELECT @ErrorNumber=@@ERROR
	IF @ErrorNumber=0 
		SELECT @Return = CAST(@RecordCount AS varchar) + ' records Updated.'
	ELSE 
		SELECT @Return = 'Error Code: ' + CAST(@ErrorNumber AS varchar) 
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('MR_SavePatient', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
