﻿

CREATE PROCEDURE   [dbo].[ets_Batch_Select]
  (
        @nBatchID int  = NULL,
        @nTableID int  = NULL,
        @sBatchDescription varchar(MAX)  = NULL,
        @sUploadedFileName varchar(MAX)  = NULL,
        @sSearch varchar(MAX)  = NULL,
        @dDateFrom datetime = NULL,
        @dDateTo datetime = NULL,
        @nUniqueName uniqueidentifier  = NULL,
        @nUserIDUploaded int  = NULL,
        @nAccountID int  = NULL,
        @sOrder nvarchar(200) = BatchID,
        @nStartRow int = 1,
        @nMaxRows int = 2147483647,
        @sTableName nvarchar(255)  = NULL,
        @sTableIn varchar(MAX)  =NULL,
  	  @bShowIntermediate bit = NULL
  )
AS
  /*
	REVISION HISTORY
	----------------
	9 Jun 2017 KG: Changed Join to [Table] to LEFT JOIN 
				   and changed @sWhere = ' WHERE ([Table].IsActive=1 OR [Table].IsActive IS NULL)' 
				   and fixed the formatting of queries
	27 Jan 2017 JA: Show Counts(Valid,Warning,Invalid) on Batch "Page Summary View" From TempRecord 
	5 Dec 2016 JA: Join the Column Table particularly the [Column].DisplayName for us to use it in the Page Summary View
	EXEC [dbo].[ets_Batch_Select] 12283
  */
BEGIN TRY
	  SET NOCOUNT ON
	  SET CONCAT_NULL_YIELDS_NULL OFF
	  SET ROWCOUNT @nMaxRows
	  -- DECLARE local variables
	  DECLARE @sSelect nvarchar(MAX)
	  DECLARE @sSelectCount nvarchar(MAX)
	  DECLARE @sWhere nvarchar(MAX)
	  -- Build WHERE clause
	  SET @sWhere = ' WHERE ([Table].IsActive=1 OR [Table].IsActive IS NULL)'
	  IF @nBatchID IS NOT NULL
			SET @sWhere = @sWhere + ' AND BatchID = '+CAST(@nBatchID AS NVARCHAR)
	  IF @nTableID IS NOT NULL
			SET @sWhere = @sWhere + ' AND Batch.TableID = '+CAST(@nTableID AS NVARCHAR)
	  IF @sBatchDescription IS NOT NULL
			SET @sWhere = @sWhere + ' AND BatchDescription LIKE '+'''' + @sBatchDescription + ''''
	  IF @sUploadedFileName IS NOT NULL
			SET @sWhere = @sWhere + ' AND UploadedFileName LIKE '+'''' + @sUploadedFileName + ''''
   
	  IF @sSearch IS NOT NULL
			SET @sWhere = @sWhere + ' AND (BatchDescription LIKE '+'''%' + @sSearch + '%''' + ' OR UploadedFileName LIKE '+'''%' + @sSearch + '%'')'
   
	  IF @dDateFrom IS NOT NULL
				  SELECT @sWhere = @sWhere + ' AND Batch.DateAdded >= ''' + CONVERT(varchar(30), @dDateFrom, 120) + ''''
   
	  IF @dDateTo IS NOT NULL
				  SELECT @sWhere = @sWhere + ' AND Batch.DateAdded <=  ''' + CONVERT(varchar(30), @dDateTo, 120) + ''''
       
	  IF @nUniqueName IS NOT NULL
			SET @sWhere = @sWhere + ' AND UniqueName = '+CAST(@nUniqueName AS NVARCHAR)
	  IF @nUserIDUploaded IS NOT NULL
			SET @sWhere = @sWhere + ' AND UserIDUploaded = '+CAST(@nUserIDUploaded AS NVARCHAR)
	  IF @nAccountID IS NOT NULL
			SET @sWhere = @sWhere + ' AND Batch.AccountID = '+CAST(@nAccountID AS NVARCHAR)
	  IF (@bShowIntermediate IS NULL) OR (@bShowIntermediate = 0)
			SET @sWhere = @sWhere + ' AND (Batch.IsIntermediate = 0 OR Batch.IsIntermediate IS NULL)'
	  IF @sTableName IS NOT NULL
			SET @sWhere = @sWhere + ' AND TableName LIKE '+'''%' + @sTableName + '%'''
       
	  IF @sTableIn IS NOT NULL
			SET @sWhere = @sWhere + ' AND Batch.TableID  IN ( '+ @sTableIn + ')'
   
       
	  SET @sSelect = 'SELECT * FROM
				(SELECT BatchID,
					Batch.TableID,
					BatchDescription,
					UploadedFileName,
					Batch.DateAdded,
					UniqueName,
					UserIDUploaded,
					Batch.AccountID,
					IsImported,
					ImportTemplate.IsImportPositional,
					IsIntermediate,
					([User].FirstName + '' '' + [User].LastName) as FullName,				
					ValidRecordCount AS ValidCount,		
					WarningRecordCount AS WarningCount,			
					TableName,
					InvalidRecordCount AS NotValidCount,			
					ImportTemplate.ImportTemplateName, 
					SourceBatchID,CustomInfo,
					ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum
				FROM [Batch] 
					INNER JOIN [User] ON Batch.UserIDUploaded = [User].UserID 
					LEFT JOIN [Table] ON Batch.TableID=[Table].TableID 
					LEFT JOIN ImportTemplate ON Batch.ImportTemplateID=ImportTemplate.ImportTemplateID ' 
				+ @sWhere + ') as UserInfo'
				 
				 
				SET @sSelectCount=REPLACE(@sSelect, 'SELECT * FROM', 'SELECT COUNT(*) AS TotalRows FROM')
				 
				SELECT @sSelect= @sSelect + ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
				--SELECT @sSelectCount= @sSelect
				 
				--SET @sSelectCount='SELECT COUNT(*) FROM
				--(SELECT [Batch].* FROM [Batch] INNER JOIN [User] ON Batch.UserIDUploaded = [User].UserID' + @sWhere + ') as UserInfo'
				 SET @sOrder=REPLACE(@sOrder,'Batch.','')
				 
				 SELECT @sSelect= @sSelect + ' ORDER BY ' + @sOrder
				 SET ROWCOUNT @nMaxRows
				PRINT @sSelect
				EXEC (@sSelect)
				SET ROWCOUNT 0
				 
				--PRINT @sSelectCount
				EXEC (@sSelectCount)
  
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Batch_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
