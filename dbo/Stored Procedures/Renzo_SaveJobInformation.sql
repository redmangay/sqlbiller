﻿


CREATE PROCEDURE [dbo].[Renzo_SaveJobInformation]
(
	@RecordID  int,		/*	Record ID passed from the table on execute, base your solution on this value	*/
	@UserID int = null,
	@Return varchar(max) output
	,@Context varchar(max) = null
)
AS


/*
 exec [dbo].[Renzo_SaveJobInformation] @RecordID= 15969367


*/

BEGIN TRY 

DECLARE @IsCurrencyChanged bit = 0
DECLARE @NewCurrencyRate varchar(50)
DECLARE @OldCurrencyRate varchar(50)
DECLARE @UpdateCurrencyRate bit = 1

/* Check if has Bills and Payment Activity */
IF NOT EXISTS(SELECT * FROM Record WHERE TableID = 2711 AND IsActive = 1 AND CAST(V002 AS int) = @RecordID)
BEGIN
	SET @UpdateCurrencyRate = 0

	
	SELECT @OldCurrencyRate = [Job Information].V044, @NewCurrencyRate = [Currency Rates Table].V001			  

	FROM		[Record] [Job Information] 
										
	LEFT JOIN	    [Record] [Currency Rates Table]	ON				1=1
		   AND		CAST([Currency Rates Table].[RecordID] AS VARCHAR) = [Job Information].V034	
		   AND		[Currency Rates Table].TableID = '2665'		/*	Currency Rates	*/
		   AND		[Currency Rates Table].IsActive = 1	
	WHERE		 1=1
	AND		[Job Information].RecordID =@RecordID
	AND		[Job Information].TableID = '2475'		/*	Job Information	*/
	AND		[Job Information].IsActive = 1





	UPDATE		[Job Information]
			
	SET			[Job Information].V029	 =	CAST(ISNULL([Group].V004,'') AS VARCHAR(MAX)) +' ' +	 CAST(ISNULL([Currency Rates].V002,'0') as VARCHAR(MAX)),
				[Job Information].V007 = [Currency Rates Table].V002, --ISNULL([Job Information].V007, [Currency Rates Table].V002),
				[Job Information].V011 = [Tax Table].V002, --ISNULL([Job Information].V011, [Tax Table].V002),
				[Job Information].V044 = [Currency Rates Table].V001,  -- ISNULL([Job Information].V044, [Currency Rates Table].V001),
				[Job Information].V045 = CAST([Currency Rates Table].V001 AS VARCHAR) + ' ' + CONVERT(varchar, CAST([Job Information].V008 AS money), 1)
				  

	FROM		[Record] [Job Information] 

	JOIN		[Record] [Group]			ON				1=1
								
											AND		CAST( [Group].RecordID AS VARCHAR(MAX)) = [Job Information].V027 
											AND		[Group].TableID = '2727'		/*	Group	*/
											AND		[Group].IsActive = 1

	LEFT JOIN	    [Record] [Currency Rates]	ON				1=1
											AND		[Currency Rates].V001 = [Group].V004	--Reporting Currency	
											AND		[Currency Rates].TableID = '2665'		/*	Currency Rates	*/
											AND		[Currency Rates].IsActive = 1
										
	LEFT JOIN	    [Record] [Currency Rates Table]	ON				1=1
											AND		CAST([Currency Rates Table].[RecordID] AS VARCHAR) = [Job Information].V034	
											AND		[Currency Rates Table].TableID = '2665'		/*	Currency Rates	*/
											AND		[Currency Rates Table].IsActive = 1

	LEFT JOIN	    [Record] [Tax Table]	ON				1=1
											AND		CAST([Tax Table].[RecordID] AS VARCHAR) = [Job Information].V042	
											AND		[Tax Table].TableID = '2696'		/*	Currency Rates	*/
											AND		[Tax Table].IsActive = 1
										
	
	WHERE		 1=1
	AND		[Job Information].RecordID =@RecordID
	AND		[Job Information].TableID = '2475'		/*	Job Information	*/
	AND		[Job Information].IsActive = 1
	--AND		  [Job Information].V002 >= '01/01/2018'


	/* Get the Group Currency Rate */

	UPDATE [Job Information]
	SET [Job Information].V047 = [Currency].V002
	FROM [Record] [Job Information]

	LEFT JOIN [Record] [Group] ON [Group].TableID = 2727
	AND [Group].IsActive = 1
	AND CAST( [Group].RecordID AS VARCHAR(MAX)) = [Job Information].V027 

	LEFT JOIN [Record] [Currency] ON [Currency].TableID = 2665
	AND [Currency].IsActive = 1
	AND CAST( [Currency].RecordID AS VARCHAR(MAX)) = [Group].V005 

	WHERE [Job Information].TableID = 2475
	AND [Job Information].IsActive = 1
	AND		[Job Information].RecordID =@RecordID
	/* End */

END


/* ========================================== Update Billing Address == */

IF @Context = 'A'
BEGIN
	INSERT INTO [DBO].[Record] (
	TableID
    ,[DateTimeRecorded]
    ,[EnteredBy]
    ,[IsActive]  
    ,V013		 /* Job No */
    ,V014		 /* Job Title */
    ,V017		 /* Office Name */
    ,V002		 /* Address 1 */
    ,V015		 /* Address 2 */
    ,V003		 /* Suburb */
    ,V004		 /* State */
    ,V005		 /* PostCode */
    ,V006		 /* Country */
    ,V009		 /* Phone */
    ,V010		 /* Fax */)

      SELECT	 2476,	   /* Billing Address */
			 GETDATE(),
			 25162,	   /* Buffalo Biller Acount */
			 1,		   /* IsActive */
			 [Job Information].RecordID,
			 [Job Information].RecordID,
			 [Client Information].V001,
			 [Client Information].V002,
			 [Client Information].V020,
			 [Client Information].V003,
			 [Client Information].V004,
			 [Client Information].V005,
			 [Client Information].V006,
			 [Client Information].V007,
			 [Client Information].V008
    FROM [Record] [Job Information]
    JOIN [Record] [Client Information] ON [Client Information].TableID = 2474
    AND [Client Information].RecordID = [Job Information].V024
    WHERE [Job Information].TableID = 2475
    AND [Job Information].RecordID = @RecordID
    AND [Job Information].IsActive = 1

END



/* ========================================== End Update Billing Address == */


/* == Update the Timesheets Currencies == */




    IF(@UpdateCurrencyRate = 1)
    BEGIN

	   DECLARE @Timesheet Table ([Counter] int identity(1,1), TimesheetRecordID int, Timesheet varchar(100))

	   INSERT INTO @Timesheet (TimesheetRecordID, Timesheet)
	   SELECT RecordID, 'Timesheet' FROM Record WHERE IsActive = 1 AND TableID = 2488 AND CAST(V018 AS INT) =   @RecordID

	   INSERT INTO @Timesheet (TimesheetRecordID, Timesheet)
	   SELECT RecordID, 'Disbursement' FROM Record WHERE IsActive = 1 AND TableID = 2725 AND CAST(V001 AS INT) =   @RecordID
	   --SELECT * FROM @Timesheet
	   DECLARE @Counter int = 1
	   WHILE EXISTS(SELECT * FROM @Timesheet WHERE [Counter] >= @Counter )
	   BEGIN

	   DECLARE @RecordIDTimesheet int
	   DECLARE @TimesheetName varchar(100)
	   SELECT @RecordIDTimesheet = TimesheetRecordID, @TimesheetName = Timesheet  FROM @Timesheet WHERE [Counter] = @Counter

	   IF(@TimesheetName = 'Timesheet')
	   BEGIN
	   EXEC [dbo].[renzo_SaveTimeSheet] @RecordID= @RecordIDTimesheet, @Return=''
	   END
	   ELSE
	   BEGIN

	   EXEC [dbo].[renzo_SaveDisbursement] @RecordID= @RecordIDTimesheet, @Return=''
	   END

	   SET @Counter = @Counter + 1
    END

END
/* == End Red == */


RETURN @@ERROR;

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Renzo_SaveJobInformation', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

GO

