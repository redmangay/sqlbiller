﻿

CREATE PROCEDURE [dbo].[DetailPhoto_list]
(
	@TableID int,
	@ColumnID int = NULL
)
AS

BEGIN TRY
	IF(@ColumnID IS NOT NULL)
	 BEGIN
		SELECT * FROM [DetailPhoto] WHERE TableID = @TableID AND ColumnID = @ColumnID
	END
	ELSE IF (@ColumnID IS NULL)
	BEGIN
		SELECT * FROM [DetailPhoto] WHERE TableID = @TableID
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('DetailPhoto_list', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

