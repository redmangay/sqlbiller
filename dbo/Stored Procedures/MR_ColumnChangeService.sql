﻿

CREATE PROCEDURE  		[dbo].[MR_ColumnChangeService]
(
	@AddMode varchar(8)='edit',
	@RecordID  int=NULL,
	@UserID int=NULL,
	@TableID int=NULL,
	@RecordInSeachCriteria int=NULL,
	@ReturnRecordInSeachCriteria int output
)
AS

BEGIN TRY
	DECLARE @xmlUpdatedRecord varchar(MAX),@sRecord varchar(MAX)

	SELECT @sRecord=SearchText FROM SearchCriteria WHERE SearchCriteriaID=@RecordInSeachCriteria
	DECLARE @xmlTheRecord XML
	SET @xmlTheRecord=@sRecord
	DECLARE @tTheRecord TABLE
	(
		xmlColumn XML
	)
				 
	INSERT INTO @tTheRecord SELECT @xmlTheRecord
					
	DECLARE @t3TheRecord TABLE
	(
		ID int identity(1,1),
		ColumnID int,
		SystemName varchar(50),
		CurrentValue varchar(MAX) NULL
	)
	INSERT INTO @t3TheRecord(ColumnID,SystemName,CurrentValue)
	SELECT
			N.C.value('ColumnID[1]', 'int') ColumnID,
			N.C.value('SystemName[1]', 'varchar(50)') SystemName,
			N.C.value('CurrentValue[1]', 'varchar(MAX)') CurrentValue					  
	FROM @tTheRecord
	CROSS APPLY xmlColumn.nodes('//EachColumn') N(C)


	DECLARE @V001_old varchar(MAX),@V002_old varchar(MAX)

	SELECT @V001_old=CurrentValue FROM @t3TheRecord WHERE SystemName='V001'
	SELECT @V002_old=CurrentValue FROM @t3TheRecord WHERE SystemName='V002'

	UPDATE @t3TheRecord SET CurrentValue=@V001_old WHERE SystemName='V002'
	UPDATE @t3TheRecord SET CurrentValue=@V002_old WHERE SystemName='V001'


	DECLARE @counter int
	SET @counter = 1		
	set @xmlUpdatedRecord='<TheRecord>' 
	DECLARE @TEMP varchar(MAX)
	   WHILE EXISTS(SELECT * FROM @t3TheRecord WHERE ID >= @counter)
		BEGIN
			SELECT 
					@TEMP =  '<EachColumn>'
						 +'<ColumnID>' + cast(ColumnID as varchar) + '</ColumnID>'
						 +'<SystemName>' + SystemName + '</SystemName>'
						  + '<CurrentValue>' + isnull(CurrentValue,'') + '</CurrentValue>'
					+'</EachColumn>' 
					FROM @t3TheRecord where id=@counter
	
				SET @xmlUpdatedRecord = @xmlUpdatedRecord+@TEMP
				SET @counter =@counter+ 1		 
		END 
	SELECT @xmlUpdatedRecord= @xmlUpdatedRecord +'</TheRecord>' 
	DECLARE @sSearchText varchar(MAX)
	
	DECLARE @message  varchar(MAX)
		SELECT @message= 'Message from SP'

	Set @sSearchText='<root><xmlUpdatedRecord>'+@xmlUpdatedRecord+'</xmlUpdatedRecord><message>'
	+@message+'</message></root>'

	INSERT INTO [SearchCriteria]
	(
		SearchText
	) VALUES (
		@sSearchText
	
	)
	DECLARE @nNewID int
	SELECT @nNewID = @@IDENTITY
	SELECT @ReturnRecordInSeachCriteria=CAST(@nNewID AS varchar(25))
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('MR_ColumnChangeService', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
