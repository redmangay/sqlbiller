﻿

CREATE PROCEDURE [dbo].[ets_Record_Export_All]
(
	@nAccountID int
)
AS
-- --------------------------------------------------------
-- Unit testing
-- EXEC dbo.[ets_Record_Export_All] @nAccountID = 307
-- SELECT * FROM Account where AccountName like 'demo%' -- 307
-- --------------------------------------------------------
BEGIN TRY

	------------------ TEMP CODE ----------------------
	-- DECLARE @nAccountID int
	-- SET @nAccountID = 307
	------------------ TEMP CODE ----------------------

	-- Get list of Record types used by Account
	DECLARE @t TABLE
	(
		id int identity(1,1),
		TableID int
	)

	INSERT INTO @t (TableID)
		SELECT TableID FROM [Table] WHERE AccountID = @nAccountID AND IsActive = 1


	SELECT * FROM @t

	DECLARE @sSQL varchar(8000)
	DECLARE @counter int
	SET @counter = 1


	--Loop through Record Types
	WHILE EXISTS(SELECT * FROM @t WHERE id >= @counter)
	BEGIN
		SELECT @sSQL = 'EXEC [ets_Record_List] @nTableID=' + CAST(TableID as varchar)
			+ ', @bIsActive = 1'
			FROM @t WHERE id = @counter
		PRINT @sSQL
		EXEC (@sSQL)
		SET @counter = @counter + 1
	END 	
	
	-- Export first result set to CSV
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Record_Export_All', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
