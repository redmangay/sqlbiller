﻿
 
CREATE PROCEDURE [dbo].[dbg_OfflineTaskLog_Detail]
(
	@nOfflineTaskLogID int 
)
	/*----------------------------------------------------------------
	-- Stored Procedure: dbg_OfflineTaskLog_Delete
	-- Generated by: gpGenerateSP (Jon Bosker, DB Gurus Australia) 
	-- Date: Jun  7 2016 10:38PM
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SELECT * FROM OfflineTaskLog
	WHERE OfflineTaskLogID = @nOfflineTaskLogID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_OfflineTaskLog_Detail', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
