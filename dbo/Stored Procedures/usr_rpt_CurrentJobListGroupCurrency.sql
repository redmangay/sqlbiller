﻿
-- Author:		<Manoj sharma>
-- Create date: <12 March 2016>
-- Description:	<Used as ssrs report and in procedure rptcurrentjoblist>
CREATE procedure [dbo].[usr_rpt_CurrentJobListGroupCurrency]
as



BEGIN

exec usr_rpt_ProjectValueConverted
exec usr_rpt_CurrentJobListWithInvoicesAndPaymentGroupCurrency

     
IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##CurrentJobListGroupCurrency' )) DROP TABLE ##CurrentJobListGroupCurrency
create table ##CurrentJobListGroupCurrency  (
ProjectNumber varchar(max),
ProjectTitle varchar(max),
Date varchar(max),
Controller varchar(max),
[Group/Division] varchar(max),
CurrencyRate varchar(max),
Name varchar(max),
Balanced float,
Approved float,
Invoiced float,
PaymentBalanced float,
JobCurrency varchar(max),
GroupCurrency varchar(max)


) 
 insert into ##CurrentJobListGroupCurrency

 SELECT     ProjectValueConverted.ProjectNumber, ProjectValueConverted.ProjectTitle, ProjectValueConverted.Date, Employee_List.Initials AS Controller, ProjectValueConverted.[Group/Division], 
                      ProjectValueConverted.CurrencyRate, Employee_List.Name, CASE WHEN ISNULL(ProjectValueGroupCurrency, 0) = 0 THEN 0 ELSE [ProjectValueGroupCurrency] - [Invoiced] END AS Balanced, 
                      ISNULL(ProjectValueConverted.ProjectValueGroupCurrency, 0) AS Approved, CurrentJobListWithInvoicesAndPaymentGroupCurrency.Invoiced, 
                      ROUND(CurrentJobListWithInvoicesAndPaymentGroupCurrency.Invoiced, 2) - ROUND(CurrentJobListWithInvoicesAndPaymentGroupCurrency.Payment, 2) AS PaymentBalanced, 
                      ProjectValueConverted.Currency AS JobCurrency, ProjectValueConverted.RptCurrency AS GroupCurrency
FROM       

  ##ProjectValueConverted  AS ProjectValueConverted INNER JOIN        Account24918.vEmployee_List AS Employee_List ON ProjectValueConverted.EmployeeID = Employee_List.EmployeeID LEFT OUTER JOIN
   ##CurrentJobListWithInvoicesAndPaymentGroupCurrency      AS CurrentJobListWithInvoicesAndPaymentGroupCurrency ON         ProjectValueConverted.ProjectNumber = CurrentJobListWithInvoicesAndPaymentGroupCurrency.ProjectNumber
WHERE     (ProjectValueConverted.Status = '2') AND (ProjectValueConverted.BilledTo = 'False')
   
   
  
  
END;
