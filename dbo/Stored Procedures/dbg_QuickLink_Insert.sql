﻿

CREATE PROCEDURE [dbo].[dbg_QuickLink_Insert]
(
	@sURL varchar(MAX)=NULL,
	@nNewID int output
)
AS
BEGIN TRY

	IF  EXISTS (SELECT * FROM [dbo].[QuickLink] WHERE [URL] = @sURL)
	BEGIN
		SELECT @nNewID = [QuickLinkID] FROM [dbo].[QuickLink] WHERE [URL] = @sURL
	END
	ELSE
	BEGIN
		INSERT INTO [dbo].[QuickLink]
			   ([URL])
		VALUES
			   (@sURL)

		SELECT @nNewID = @@IDENTITY
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_QuickLink_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
