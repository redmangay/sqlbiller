﻿

CREATE PROCEDURE [dbo].[ets_Record_Count]
(
	@nTableID  int,
	 @dtStart datetime,
	 @dEnd datetime
)
AS

BEGIN TRY
	SELECT COUNT(*) 
		FROM Record 
		WHERE IsActive=1 AND TableID=@nTableID
		AND DateTimeRecorded>=@dtStart AND DateTimeRecorded<=@dEnd
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Record_Count', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
