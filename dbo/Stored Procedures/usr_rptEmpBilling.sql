﻿create Procedure usr_rptEmpBilling
AS
BEGIN


SELECT Sum(Account24918.[vrptEmpBilling].TimeSheetDollar) AS SumTimeSheetDollar,
 Sum(Account24918.[vrptEmpBilling].BilledDollar) AS SumBilledDollar, 
 Account24918.[vrptEmpBilling].GroupDivision
FROM Account24918.[vrptEmpBilling]
GROUP BY Account24918.[vrptEmpBilling].GroupDivision
ORDER BY Account24918.[vrptEmpBilling].GroupDivision;
END
