﻿

CREATE PROCEDURE [dbo].[Batch_Validation_Count_Update]
(
	@BatchID int
)
AS	 

BEGIN TRY      


	--	DECLARE @BatchID int = 11097

	DECLARE @ValidCount int=0
    SELECT @ValidCount= COUNT(*)
		FROM TempRecord 
		WHERE RejectReason IS NULL 
		AND WarningReason IS NULL 
		AND BatchID=@BatchID

	DECLARE @WarningCount int=0
	SELECT  @WarningCount=COUNT(*) 
		FROM TempRecord 
		WHERE RejectReason IS NULL 
		AND WarningReason is not NULL 
		AND BatchID=@BatchID

	-- Invalid
	DECLARE @InvalidCount int=0
	SELECT @InvalidCount=COUNT(*) 
		FROM TempRecord 
		WHERE RejectReason IS NOT NULL 
		AND BatchID=@BatchID

		PRINT 'ValidCount: ' + CAST(@ValidCount as varchar)
		PRINT 'WarningCount: ' + CAST(@WarningCount as varchar)
		PRINT 'InvalidCount: ' + CAST(@InvalidCount as varchar)

	/* UPDATE the table */
	UPDATE [Batch] SET 
		ValidRecordCount =@ValidCount,
		WarningRecordCount = @WarningCount,
		InvalidRecordCount =@InvalidCount 
		WHERE BatchID=@BatchID	
		
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Batch_Validation_Count_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
