﻿
 
CREATE PROCEDURE [dbo].[ets_Notification_Select]
(
	@nTableID int  = NULL,	
	@nAccountID int ,
	@dDateFrom datetime = NULL,
	@dDateTo datetime = NULL,
	@sOrder nvarchar(200) = [st.TableID], 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647,
	@sTableIn varchar(MAX)  =NULL   
	
)
	/*----------------------------------------------------------------
	EXEC [ets_Notification_Select] @nAccountID=13
	EXEC [ets_Notification_Select] @nAccountID=13,@nTableID=109

	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' '
	IF @nTableID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND st.TableID = '+CAST(@nTableID AS NVARCHAR)

	SET @sWhere = @sWhere + ' AND stg.AccountID = '+CAST(@nAccountID AS NVARCHAR)

	IF @dDateFrom IS NOT NULL 
			SELECT @sWhere = @sWhere + ' AND Record.DateTimeRecorded >= ''' + CONVERT(varchar(30), @dDateFrom, 120) + ''''

	IF @dDateTo IS NOT NULL 
			SELECT @sWhere = @sWhere + ' AND Record.DateTimeRecorded <= ''' + CONVERT(varchar(30), @dDateTo, 120) + ''''

	IF @sTableIn IS NOT NULL 
		SET @sWhere = @sWhere + ' AND st.TableID  IN ( '+ @sTableIn + ')'



	SET @sWhere = @sWhere + ' AND stg.IsActive = 1'

	SET @sSelect = 'SELECT * FROM (SELECT     ST.TableID,ST.TableName, MAX(Record.DateTimeRecorded) LastWarningTime,
	CONVERT(varchar, COUNT(*)) + '' Warning(s)'' WarningCount,
	ROW_NUMBER() OVER(ORDER BY ST.TableID DESC) as RowNum
	FROM         Record INNER JOIN
						  [Table] ST ON Record.TableID = ST.TableID INNER JOIN
						  Menu STG ON ST.TableID = STG.TableID                  
	WHERE     (Record.WarningResults IS NOT NULL AND Record.IsActive=1  AND  STG.IsActive=1 AND ST.IsActive=1) '+ @sWhere +'
	GROUP BY ST.TableID,ST.TableName
	) AS Warning
	WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow) + ' ORDER BY LastWarningTime  DESC'


	SET @sSelectCount = 'SELECT  COUNT(*) AS TotalRows  FROM  (SELECT     ST.TableID,ST.TableName, MAX(Record.DateTimeRecorded) LastWarningTime,
	CONVERT(varchar, COUNT(*)) + '' Warning'' WarningCount,
	ROW_NUMBER() OVER(ORDER BY ST.TableID DESC) as RowNum
	FROM         Record INNER JOIN
						  [Table] ST ON Record.TableID = ST.TableID INNER JOIN
						  Menu STG ON ST.TableID = STG.TableID                  
	WHERE     (Record.WarningResults IS NOT NULL AND Record.IsActive=1 AND STG.IsActive=1 AND ST.IsActive=1) '+ @sWhere +'
	GROUP BY ST.TableID,ST.TableName
	) AS Warning' 
 

	PRINT @sSelect

	EXEC (@sSelect)
	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Notification_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
