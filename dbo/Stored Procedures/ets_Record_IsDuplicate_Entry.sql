﻿


CREATE PROCEDURE [dbo].[ets_Record_IsDuplicate_Entry]
(
	@iTableID int,
	@iRecordID int,
	@sUniqueColumnIDSys varchar(20) =NULL,
	@sUniqueColumnIDValue varchar(MAX)=NULL,
	@sUniqueColumnID2Sys varchar(20) =NULL,
	@sUniqueColumnID2Value varchar(MAX)=NULL	
	
)
AS

DECLARE @sSQL varchar(MAX)


SELECT @sSQL= 'SELECT * from [Record] WHERE IsActive=1 '  + 
            'AND [TableID] =' +  CAST(@iTableID as varchar)  +
			
	  + CASE WHEN (@sUniqueColumnIDSys IS NOT NULL AND @sUniqueColumnIDSys<>'DateTimeRecorded' AND @sUniqueColumnIDValue IS NOT NULL) THEN ' AND ['+@sUniqueColumnIDSys+'] ='''+@sUniqueColumnIDValue+''''  ELSE '' END +
	  	  + CASE WHEN (@sUniqueColumnIDSys IS NOT NULL AND @sUniqueColumnIDSys='DateTimeRecorded' AND @sUniqueColumnIDValue IS NOT NULL) THEN ' AND ['+@sUniqueColumnIDSys+'] =CONVERT(Datetime,'''+@sUniqueColumnIDValue+''',103)'  ELSE '' END +
	  + CASE WHEN (@sUniqueColumnID2Sys IS NOT NULL AND @sUniqueColumnID2Sys<>'DateTimeRecorded' AND @sUniqueColumnID2Value IS NOT NULL) THEN ' AND ['+@sUniqueColumnID2Sys+'] ='''+@sUniqueColumnID2Value+''''  ELSE '' END +
	  	  	  + CASE WHEN (@sUniqueColumnID2Sys IS NOT NULL AND @sUniqueColumnID2Sys='DateTimeRecorded' AND @sUniqueColumnID2Value IS NOT NULL) THEN ' AND ['+@sUniqueColumnID2Sys+'] =CONVERT(Datetime,'''+@sUniqueColumnID2Value+''',103)'  ELSE '' END +
            ' AND RecordID <> ' + CAST(@iRecordID as varchar)
          
           
PRINT @sSQL
EXEC (@sSQL)
