﻿

CREATE PROCEDURE   [dbo].[ets_Table_Select]
(
	@nTableID int  = NULL,
	@sTableName nvarchar(255)  = NULL,
	@nMenuID int  = NULL,
	@nAccountID int  = NULL,
	@dDateAdded datetime  = NULL,
	@dDateUpdated datetime  = NULL,
	@sOrder nvarchar(200) = [stg.ParentMenuID], 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647,
	@bIsActive bit  = NULL,
	@sTableIn varchar(MAX)  =NULL                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
)
	/*----------------------------------------------------------------
	EXEC ets_Table_Select
	EXEC ets_Table_Select @nAccountID=24800
	EXEC ets_Table_Select @nAccountID=2,@nMenuID=1
	---------------------------------------------------------------*/
AS
BEGIN TRY

	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nTableID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND st.TableID = '+CAST(@nTableID AS NVARCHAR)
	IF @sTableName IS NOT NULL 
		SET @sWhere = @sWhere + ' AND st.TableName LIKE '+'''%' + @sTableName + '%'''
	IF @nMenuID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND stg.ParentMenuID = '+CAST(@nMenuID AS NVARCHAR)
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND st.AccountID = '+CAST(@nAccountID AS NVARCHAR)
	IF @bIsActive IS NOT NULL 
		SET @sWhere = @sWhere + ' AND st.IsActive = '+CAST(@bIsActive AS NVARCHAR)

	IF @dDateAdded IS NOT NULL 
		SET @sWhere = @sWhere + ' AND st.DateAdded = '+CAST(@dDateAdded AS NVARCHAR)
	IF @dDateUpdated IS NOT NULL 
		SET @sWhere = @sWhere + ' AND st.DateUpdated = '+CAST(@dDateUpdated AS NVARCHAR)

	IF @sTableIn IS NOT NULL 
		SET @sWhere = @sWhere + ' AND st.TableID  IN ( '+ @sTableIn + ')'
	
	
	--SET @sWhere = @sWhere + ' AND stg.IsActive = 1'

	SET @sSelect = 'SELECT * FROM 
	(SELECT    st.DisplayOrder, st.TableID, st.TableName, stg.ParentMenuID AS MenuID , 
	st.DateAdded, st.DateUpdated, st.IsActive,st.PinImage,st.MaxTimeBetweenRecords,
	st.MaxTimeBetweenRecordsUnit,st.LastUpdatedUserID,st.LateDataDays, stg.Menu,ac.AccountID,ac.AccountName,st.LateDataUnit,
	 ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ' ) as RowNum
	FROM    Account ac INNER JOIN [Table] st 
	ON st.AccountID=ac.AccountID LEFT JOIN
	Menu stg   
	 ON st.TableID = stg.TableID 
	 ' + @sWhere + ') as TableInfo'
	+ ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)


	SET @sSelectCount = 'SELECT  COUNT(*) AS TotalRows  FROM  [Table] st 
						LEFT JOIN   Menu stg 
						ON st.TableID = stg.TableID ' + @sWhere 
 
	-- Extend WHERE to include paging:
	--SET @sWhere = @sWhere + ' AND RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	--SET @sOrder = ' ORDER BY ' + @sOrder
	--SET @sSelect = @sSelect + @sWhere + @sOrder
 
	 PRINT @sSelect

	EXEC (@sSelect)
	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)


	PRINT 'Done'
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog]([Module], [ErrorMessage], [ErrorTrack], [ErrorTime], [Path]) 
		VALUES ('ets_Table_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
