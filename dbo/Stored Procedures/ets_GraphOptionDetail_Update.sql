﻿
CREATE PROCEDURE [dbo].[ets_GraphOptionDetail_Update]
(
	@nGraphOptionDetailID int ,
	@nGraphOptionID int ,
	@nTableID int ,
	@nColumnID int ,
	@sLabel varchar(200)  = NULL,
	@sAxis varchar(20)  = NULL,
	@nGraphOrder int  = NULL,
	@nScale decimal(18,10)  = NULL,
	@sGraphType varchar(20)  = NULL,
	@sColour varchar(20)  = NULL,
	@dHigh decimal(18,10)  = NULL,
	@dLow decimal(18,10)  = NULL,
	@sGraphSeriesColumnID varchar(20) = NULL,
	@sGraphSeriesID varchar(200) = NULL,
	@nTrueTableID int
)
	/*----------------------------------------------------------------
	EXEC ets_GraphOptionDetail_Update @nGraphOptionDetailID=1,
	@nGraphOptionID=54,@nTableID=432,@nColumnID=6142
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	UPDATE [GraphOptionDetail]
	SET 
		GraphOptionID = @nGraphOptionID,
		TableID = @nTableID,
		ColumnID = @nColumnID,
		Label = @sLabel,
		Axis = @sAxis,	
		GraphOrder = @nGraphOrder,	
		Scale = @nScale,
		GraphType = @sGraphType,
		Colour = @sColour,
		High = @dHigh,
		Low = @dLow,
		GraphSeriesColumnID = @sGraphSeriesColumnID,
		GraphSeriesID = @sGraphSeriesID,
		TrueTableID = @nTrueTableID

	WHERE GraphOptionDetailID = @nGraphOptionDetailID
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_GraphOptionDetail_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
