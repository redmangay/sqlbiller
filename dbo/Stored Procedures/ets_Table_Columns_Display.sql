﻿

CREATE PROCEDURE [dbo].[ets_Table_Columns_Display]
(
	@nTableID int,
	@bIsStandard bit=NULL,
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
AS
/*
	SELECT * FROM dbo.[Table]
	
	EXEC ets_Table_Columns_Display 1335,0
*/
BEGIN TRY
	SET NOCOUNT ON;	
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows

	--1st dataset has the columns of the Selected [Table]
	IF @bIsStandard IS NULL
	BEGIN
	SELECT * FROM ( SELECT [Column].*, ROW_NUMBER() OVER(ORDER BY DisplayOrder) as RowNum
		FROM [Column] 
		WHERE TableID = @nTableID AND SystemName<>'TableID'
		 AND  SystemName<>'IsActive') AS Info
		WHERE RowNum >= @nStartRow
	--2nd TotalRows
	SELECT COUNT(*) AS TotalRows FROM [Column] WHERE  TableID = @nTableID AND SystemName<>'TableID'
		 AND  SystemName<>'IsActive'
	END
	
	IF @bIsStandard IS NOT NULL
	BEGIN
	SELECT * FROM ( SELECT [Column].*, ROW_NUMBER() OVER(ORDER BY DisplayOrder) as RowNum
		FROM [Column] 
		WHERE TableID = @nTableID AND SystemName<>'TableID' AND IsStandard=@bIsStandard
		 AND  SystemName<>'IsActive') AS Info
		WHERE RowNum >= @nStartRow
	--2nd TotalRows
	SELECT COUNT(*) AS TotalRows FROM [Column] WHERE  TableID = @nTableID AND SystemName<>'TableID'
		 AND  SystemName<>'IsActive' AND  IsStandard=@bIsStandard
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Table_Columns_Display', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
