﻿Create PROCEDURE [dbo].[rptInvoiceGetNotes] 
	@Type varchar(max),
	@ProjectNumber varchar(max)
AS
BEGIN
	
	select case  when @Type =4 then
	(Select DS.Value From Account24918.vDocumentSetting DS
where DS.Name='Invoice Details' and (DS.GroupID=
(Select [Group/DivisionID] from Account24918.[vList_Of_Group_Division] where [Group/Division]=
(Select vp.[Group/Division] from Account24918.vProject vp where vp.[ProjectNumber]=@ProjectNumber)) or
DS.GroupID IS Null or DS.GroupID=0
)) 
else
(Select DS.Value From Account24918.vDocumentSetting DS
where DS.Name='Interim Invoice Details' and (DS.GroupID=
(Select [Group/DivisionID] from Account24918.[vList_Of_Group_Division] where [Group/Division]=
(Select vp.[Group/Division] from Account24918.vProject vp where vp.[ProjectNumber]=@ProjectNumber)) or
DS.GroupID IS Null or DS.GroupID=0
))
end AS InvoiceNotes
END