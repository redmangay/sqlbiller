﻿


CREATE PROCEDURE [dbo].[Account_Insert]
(
	@nNewID int output,
	@sAccountName nvarchar(100) ,	
	@Logo image = NULL,
	@IsActive bit = 1,	
	@iAccountTypeID int=NULL,
	@dateExpiryDate datetime=NULL,
	@bUseDefaultLogo bit=1,
	@dMapCentreLat decimal(9,6)=NULL,
	@dMapCentreLong decimal(9,6)=NULL,
	@nMapZoomLevel int=NULL,	
	@nMapDefaultTableID int=NULL,
	@nOtherMapZoomLevel int=NULL,
	@nCountryID int=NULL,
	@sPhoneNumber nvarchar(20) =NULL,
	@bCreatedByWizard bit=0,
	@sOrganisationName nvarchar(200)=NULL
	
)
 
AS
 
BEGIN TRY
	BEGIN TRANSACTION --beginning a transaction..

		INSERT INTO [Account]
		(
			AccountName, 
			Logo,IsActive,
			AccountTypeID,ExpiryDate,UseDefaultLogo,
			MapCentreLat,MapCentreLong,MapZoomLevel,
			MapDefaultTableID,OtherMapZoomLevel,CountryID,PhoneNumber,
			CreatedByWizard,OrganisationName,HomeMenuCaption
	
		) VALUES (
			@sAccountName,
			@Logo,@IsActive,
			@iAccountTypeID,@dateExpiryDate,@bUseDefaultLogo,
			@dMapCentreLat,@dMapCentreLong,@nMapZoomLevel,	
			@nMapDefaultTableID,@nOtherMapZoomLevel,@nCountryID,@sPhoneNumber,
			@bCreatedByWizard,@sOrganisationName,'Dashboard'
		)
		SELECT @nNewID = @@IDENTITY


		--Insert Table & None menu
		INSERT INTO Menu(Menu, 
								AccountID,ShowOnMenu,IsActive ,DisplayOrder) 
								VALUES ('Tables',@nNewID,1,1,1) 
						
		INSERT INTO Menu(Menu, 
								AccountID,ShowOnMenu,IsActive ,DisplayOrder) 
								VALUES ('--None--',@nNewID,0,1,2) 

		 INSERT INTO [Content]
			(ContentKey,Heading,Content,StoredProcedure,AccountID,MainContentID,ForAllAccount) 
			SELECT  ContentKey, 
			Heading,Content, 
			StoredProcedure,@nNewID,NULL,0
			FROM [Content] WHERE ContentKey='HomeWelcome' AND AccountID IS NULL;

		UPDATE [Content]
		SET Content=Replace(Content,'[AccountName]',@sAccountName)
		WHERE Contentkey='HomeWelcome' AND AccountID=@nNewID 


		INSERT DocumentType(DocumentTypeName,AccountID) VALUES('Audit Report',@nNewID)
		INSERT DocumentType(DocumentTypeName,AccountID) VALUES('Custom Reports',@nNewID)


		--INSERT Role this account

		INSERT INTO [dbo].[Role]([Role] ,[RoleType],[RoleNotes],[AccountID],[IsSystemRole],[IsActive])	 
		(SELECT [Role] ,[RoleType],[RoleNotes],@nNewID,[IsSystemRole],[IsActive] 
		FROM [Role] WHERE IsSystemRole=1 AND AccountID IS NULL)

		UPDATE [Role] SET AllowEditDashboard=1 WHERE RoleType='2'  AND [AccountID]=@nNewID

		--insert DocumentSectionStyle

		DECLARE @nTemplateAccontID int

		SELECT @nTemplateAccontID=OptionValue  FROM SystemOption WHERE OptionKey='TemplateAccountID' 

		  INSERT INTO [DocumentSectionStyle]
			(AccountID,StyleName,StyleDefinition,IsSystem) 
			SELECT  @nNewID, 
			StyleName,StyleDefinition,IsSystem
			FROM [DocumentSectionStyle] WHERE AccountID=@nTemplateAccontID AND IsSystem=1;

	COMMIT TRANSACTION --finally, Commit the transaction as all Success.

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Account_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
