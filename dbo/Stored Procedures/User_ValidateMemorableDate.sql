﻿

CREATE PROCEDURE [dbo].[User_ValidateMemorableDate]
(
	@sEmail nvarchar(200),
	@dMemDate nvarchar(200)
)
 
AS
BEGIN TRY
 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL on
	SET ROWCOUNT 1

	 DECLARE @userID INT
	
		IF EXISTS (SELECT TOP 1 UserID FROM [dbo].[User] WHERE Email=@sEmail)
		BEGIN
			SET @userID	= (SELECT UserID 
							FROM [dbo].[User] 
								WHERE Email=@sEmail AND 
									[MemorableDate]	= HASHBYTES('SHA2_512', cast(@dMemDate as nvarchar) + CAST([SaltMemDate] AS NVARCHAR) ))

				IF @userID IS NOT NULL
					BEGIN
						SELECT * FROM [User] WHERE USERID = @userID
					END
				
		END 
END TRY
	BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('User_ValidateMemorableDate', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
