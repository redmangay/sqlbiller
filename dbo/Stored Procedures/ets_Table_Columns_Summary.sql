﻿

CREATE PROCEDURE [dbo].[ets_Table_Columns_Summary]
(
	@nTableID int,
	@nViewID int=NULL
)
AS
/*
EXEC [ets_Table_Columns_Summary] 1883,1
*/
BEGIN TRY
	SET NOCOUNT ON;	
	SET CONCAT_NULL_YIELDS_NULL OFF

	IF @nViewID IS NULL
		SELECT c.*,c.DisplayTextSummary as Heading,
		NULL AS ViewItemID,NULL AS ViewID,
		NULL AS SearchField,NULL AS FilterField,NULL AS Width FROM [Column] c
			WHERE TableID = @nTableID
			AND (DisplayTextSummary IS NOT NULL AND LEN(DisplayTextSummary) > 0
			OR SystemName='RecordID')
			ORDER BY DisplayOrder

	IF @nViewID IS NOT NULL
	SELECT     C.ColumnID, C.TableID, C.SystemName, C.DisplayName, C.DisplayTextSummary, C.DisplayTextDetail, 
	  VT.ColumnIndex AS DisplayOrder, C.DisplayRight, VT.Alignment, C.TextWidth, C.TextHeight, C.IsStandard, C.GraphTypeID, C.GraphLabel, C.ValidationOnWarning, C.ValidationOnEntry, 
	  C.CheckUnlikelyValue, C.Constant, C.Calculation, VT.ShowTotal, C.IgnoreSymbols, C.IsRound, C.RoundNumber, C.Notes, C.DropdownValues, 
	  C.Importance, C.NumberType, C.DefaultValue, C.AvgColumnID, C.AvgNumberOfRecords, C.ShowGraphExceedance, C.ShowGraphWarning, 
	  C.FlatLineNumber, C.MaxValueAt, C.ColumnType, C.DropDownType, C.TableTableID, C.DisplayColumn, VT.SummaryCellBackColor, C.FormVerticalPosition, 
	  C.FormHorizontalPosition, C.ParentColumnID, C.TextType, C.RegEx,  C.DateCalculationType, 
	  C.IsSystemColumn, C.LinkedParentColumnID, C.DataRetrieverID, C.VerticalList,  C.QuickAddLink,C.CalculationIsActive, 
	  C.DateAdded, C.DateUpdated, C.LastUpdatedUserID, C.TableTabID, C.ViewName, C.SPDefaultValue, C.DefaultType, C.DefaultColumnID, C.ShowViewLink, 
	  C.FilterParentColumnID, C.FilterOtherColumnID, C.FilterValue, C.MapPinHoverColumnID, C.CompareColumnID, C.CompareOperator,
	  C.MapPopup,C.TrafficLightColumnID,C.TrafficLightValues,C.DefaultRelatedTableID,C.DefaultUpdateValues,C.ValidationCanIgnore,
	  C.DefaultGraphDefinitionID,C.ImageOnSummary,C.AllowCopy,C.ValidationOnExceedance,C.ColourCells ,C.ButtonInfo,C.IsReadOnly,
	  C.HideTimeSecond,
	  VT.ViewItemID, VT.ViewID, 
	  ISNULL(C.DisplayTextSummary,C.DisplayName) AS Heading, VT.SearchField, VT.FilterField, VT.Width,
	  (SELECT TOP 1 CheckColumnID FROM [Condition] Con WHERE Con.ConditionType='V'AND Con.ColumnID=C.ColumnID ) ConV,
	(SELECT TOP 1 CheckColumnID FROM [Condition] Con WHERE Con.ConditionType='W'AND Con.ColumnID=C.ColumnID ) ConW,
	(SELECT TOP 1 CheckColumnID FROM [Condition] Con WHERE Con.ConditionType='E'AND Con.ColumnID=C.ColumnID ) ConE,
	(SELECT TOP 1 ConditionColumnID FROM [AdvancedCondition] Con WHERE Con.ConditionType='notification'AND Con.ConditionSubType='V'AND Con.ColumnID = C.ColumnID) AdvConV,
	(SELECT TOP 1 ConditionColumnID FROM [AdvancedCondition] Con WHERE Con.ConditionType='notification'AND Con.ConditionSubType='W'AND Con.ColumnID = C.ColumnID) AdvConW,
	(SELECT TOP 1 ConditionColumnID FROM [AdvancedCondition] Con WHERE Con.ConditionType='notification'AND Con.ConditionSubType='E'AND Con.ColumnID = C.ColumnID) AdvConE, C.EditableOnSummary
	FROM         [Column] AS C INNER JOIN
	ViewItem AS VT ON C.ColumnID = VT.ColumnID WHERE ViewID=@nViewID 
	UNION
	SELECT C.ColumnID, C.TableID, C.SystemName, C.DisplayName, C.DisplayTextSummary, C.DisplayTextDetail, 
	  C.DisplayOrder, C.DisplayRight, C.Alignment, C.TextWidth, C.TextHeight, C.IsStandard, C.GraphTypeID, C.GraphLabel, C.ValidationOnWarning, C.ValidationOnEntry, 
	  C.CheckUnlikelyValue, C.Constant, C.Calculation, C.ShowTotal, C.IgnoreSymbols, C.IsRound, C.RoundNumber, C.Notes, C.DropdownValues, 
	  C.Importance, C.NumberType, C.DefaultValue, C.AvgColumnID, C.AvgNumberOfRecords, C.ShowGraphExceedance, C.ShowGraphWarning, 
	  C.FlatLineNumber, C.MaxValueAt, C.ColumnType, C.DropDownType, C.TableTableID, C.DisplayColumn, C.SummaryCellBackColor, C.FormVerticalPosition, 
	  C.FormHorizontalPosition, C.ParentColumnID, C.TextType, C.RegEx,  C.DateCalculationType, 
	  C.IsSystemColumn, C.LinkedParentColumnID, C.DataRetrieverID, C.VerticalList, C.QuickAddLink,  C.CalculationIsActive, 
	  C.DateAdded, C.DateUpdated, C.LastUpdatedUserID, C.TableTabID, C.ViewName, C.SPDefaultValue, C.DefaultType, C.DefaultColumnID, C.ShowViewLink, 
	  C.FilterParentColumnID, C.FilterOtherColumnID, C.FilterValue, C.MapPinHoverColumnID, C.CompareColumnID, C.CompareOperator,
		C.MapPopup,C.TrafficLightColumnID,C.TrafficLightValues,C.DefaultRelatedTableID,C.DefaultUpdateValues,C.ValidationCanIgnore,
	  C.DefaultGraphDefinitionID,C.ImageOnSummary ,C.AllowCopy,C.ValidationOnExceedance,C.ColourCells,C.ButtonInfo,C.IsReadOnly, 
	  C.HideTimeSecond,
	NULL AS ViewItemID,NULL AS ViewID,NULL AS Heading,
	NULL AS SearchField,NULL AS FilterField,NULL AS Width ,
	(SELECT TOP 1 CheckColumnID FROM [Condition] Con WHERE Con.ConditionType='V'AND Con.ColumnID=C.ColumnID ) ConV,
	(SELECT TOP 1 CheckColumnID FROM [Condition] Con WHERE Con.ConditionType='W'AND Con.ColumnID=C.ColumnID ) ConW,
	(SELECT TOP 1 CheckColumnID FROM [Condition] Con WHERE Con.ConditionType='E'AND Con.ColumnID=C.ColumnID ) ConE,
	(SELECT TOP 1 ConditionColumnID FROM [AdvancedCondition] Con WHERE Con.ConditionType='notification'AND Con.ConditionSubType='V'AND Con.ColumnID = C.ColumnID) AdvConV,
	(SELECT TOP 1 ConditionColumnID FROM [AdvancedCondition] Con WHERE Con.ConditionType='notification'AND Con.ConditionSubType='W'AND Con.ColumnID = C.ColumnID) AdvConW,
	(SELECT TOP 1 ConditionColumnID FROM [AdvancedCondition] Con WHERE Con.ConditionType='notification'AND Con.ConditionSubType='E'AND Con.ColumnID = C.ColumnID) AdvConE,C.EditableOnSummary

	FROM [Column] C 
	WHERE TableID = @nTableID AND SystemName='RecordID'
	AND ColumnID NOT IN (SELECT ColumnID FROM [ViewItem] WHERE ViewID=@nViewID)
	ORDER BY DisplayOrder


		

	--IF @nViewID IS NOT NULL
	--SELECT C.*,VT.Heading FROM [Column] C INNER JOIN ViewItem VT
	--		ON C.ColumnID =VT.ColumnID
	--		WHERE ViewID = @nViewID		
	--UNION
	--SELECT C.*,NULL AS Heading  FROM [Column] C 
	--WHERE TableID = @nTableID AND SystemName='RecordID'
	--AND ColumnID NOT IN (SELECT ColumnID FROM [ViewItem] WHERE ViewID=@nViewID)
	--ORDER BY DisplayOrder

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Table_Columns_Summary', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
