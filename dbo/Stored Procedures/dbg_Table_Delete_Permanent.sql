﻿

CREATE PROCEDURE   [dbo].[dbg_Table_Delete_Permanent]

(

      @nTableID int

)

      /*----------------------------------------------------------------

      EXEC [dbg_Table_Delete_Permanent] 1427

      ---------------------------------------------------------------*/

AS

BEGIN TRY
	

	DELETE FROM [ScheduledTaskColumn] 
		WHERE ScheduledTaskID IN (SELECT ScheduledTaskID FROM [ScheduledTask] WHERE TableID = @nTableID)
	PRINT '[ScheduledTaskColumn]'

	DELETE FROM [ScheduledTask] WHERE TableID = @nTableID
	PRINT '[ScheduledTask]'

	--

	DELETE [Message] WHERE TableID = @nTableID AND MessageID NOT IN (SELECT ISNULL(MessageID,-1) FROM [ScheduledTask])
	PRINT '[Message]'

	DELETE FROM [EmailLog] WHERE TableID = @nTableID
	PRINT '[EmailLog]'

	--Now RecordID has no ref

	DELETE FROM [Record] WHERE TableID = @nTableID
					OR BatchID IN (SELECT BatchID FROM Batch	WHERE TableID = @nTableID)
	PRINT '[Record]'
	DELETE FROM TempRecord WHERE TableID = @nTableID
			OR BatchID IN (SELECT BatchID FROM Batch	WHERE TableID = @nTableID)
	PRINT 'TempRecord'




	UPDATE [Menu] SET [Menu]=[Menu]+CAST(MenuID AS varchar)
		WHERE TableID = @nTableID 

	UPDATE [Menu] SET 
		[TableID] = NULL,ParentMenuID=NULL
		WHERE TableID = @nTableID 


	DELETE [Menu] WHERE TableID = @nTableID 
	PRINT '[Menu]'

	--


	UPDATE [Document] SET TableID=NULL WHERE  TableID = @nTableID
	PRINT '[Document]'



	---
	DELETE FROM DocTemplate WHERE DataRetrieverID in (SELECT DataRetrieverID FROM [DataRetriever] WHERE TableID = @nTableID)
	PRINT '[DocTemplate]'


	UPDATE [Column] SET DataRetrieverID=NULL
	WHERE DataRetrieverID in (SELECT DataRetrieverID FROM [DataRetriever] WHERE TableID = @nTableID)

	DELETE FROM [DataRetriever] WHERE TableID = @nTableID
	PRINT '[DataRetriever]'
	--


	--need to drop this 2 tables
	DELETE FROM [MonitorScheduleUser] WHERE MonitorScheduleID IN (SELECT MonitorScheduleID FROM [MonitorSchedule]  WHERE TableID = @nTableID)
	PRINT '[MonitorScheduleUser] 1'

	DELETE FROM [MonitorSchedule] WHERE TableID = @nTableID

	PRINT '[MonitorSchedule]'


	--

	DELETE FROM [DataReminderUser] 
		WHERE DataReminderID IN (SELECT DataReminderID FROM [DataReminder] WHERE ColumnID IN (SELECT ColumnID FROM [Column] WHERE TableID = @nTableID))
	PRINT '[DataReminderUser]'

	DELETE FROM [DataReminder] 
		WHERE ColumnID IN (SELECT ColumnID FROM [Column] WHERE TableID = @nTableID)
	PRINT '[DataReminder]'


	DELETE FROM [Conditions] WHERE 
	[ConditionColumnID] IN (SELECT ColumnID FROM [Column] WHERE TableID = @nTableID)
	OR [ColumnID]  IN (SELECT ColumnID FROM [Column] WHERE TableID = @nTableID)
	PRINT '[Conditions]'

	--

	DELETE FROM [GraphOptionDetail] WHERE TableID = @nTableID
	PRINT '[GraphOptionDetail] 1'



	UPDATE [Account] SET MapDefaultTableID=NULL
		WHERE MapDefaultTableID = @nTableID

	UPDATE [Role] SET DashboardTableID=NULL
		WHERE DashboardTableID = @nTableID
	
	UPDATE [Column] SET AvgColumnID =NULL,LinkedParentColumnID =NULL,TableTableID=NULL,
		ParentColumnID =NULL,DefaultColumnID=NULL,FilterOtherColumnID=NULL,FilterParentColumnID=NULL,
		MapPinHoverColumnID=NULL,CompareColumnID=NULL,TrafficLightColumnID=NULL,TableTabID=NULL,
		GraphTypeID=NULL,DataRetrieverID=NULL,DefaultRelatedTableID=NULL,DefaultGraphDefinitionID=NULL
		 WHERE TableID = @nTableID


	DELETE FROM [GraphDefinition] WHERE TableID = @nTableID
			OR TableID = @nTableID
			OR ColumnID IN (SELECT C.ColumnID FROM [Column] C WHERE C.TableID = @nTableID)
	PRINT '[GraphDefinition]'
	--
	DELETE FROM [Upload] WHERE TableID = @nTableID
	PRINT '[Upload]'

	DELETE FROM TableTab WHERE TableID = @nTableID
	PRINT '[TableTab]'

	DELETE FROM TableUser WHERE UserID IN (SELECT [User].UserID FROM [User] INNER JOIN UserRole ON UserRole.UserID=[User].UserID WHERE  TableID = @nTableID AND IsPrimaryAccount=1)
	PRINT '[TableUser]'


	DELETE FROM ColumnColour WHERE (Context='columnid' AND ID IN (SELECT C.ColumnID FROM [Column] C WHERE C.TableID=@nTableID) )
				OR ControllingColumnID IN (SELECT C.ColumnID FROM [Column] C WHERE C.TableID=@nTableID)
	PRINT 'ColumnColour'


	DELETE FROM [Condition] WHERE ColumnID IN (SELECT C.ColumnID FROM [Column] C WHERE C.TableID=@nTableID)  
				OR CheckColumnID IN (SELECT C.ColumnID FROM [Column] C WHERE C.TableID=@nTableID) 

	PRINT '[Condition]'

	--
	DELETE FROM ViewItem WHERE ViewID IN (SELECT V.ViewID FROM [View] V WHERE V.TableID=@nTableID)
	PRINT '[ViewItem]'

	DELETE FROM [View] WHERE TableID =@nTableID
	PRINT '[View]'
	--
	--DELETE FROM [ImportTemplateItem] WHERE ImportTemplateID IN (SELECT I.ImportTemplateID FROM [ImportTemplate] I WHERE I.TableID=@nTableID)
	--PRINT '[ImportTemplateItem]'
	--UPDATE [Table] SET DefaultImportTemplateID=NULL WHERE TableID =@nTableID
	--UPDATE [Table] SET DefaultImportTemplateID=NULL WHERE TableID = @nTableID
	--UPDATE Upload SET TemplateID=NULL WHERE TemplateID IN (SELECT I.ImportTemplateID FROM [ImportTemplate] I WHERE I.TableID=@nTableID)

	--DELETE FROM Batch WHERE ImportTemplateID IN (SELECT I.ImportTemplateID FROM [ImportTemplate] I WHERE I.TableID=@nTableID)






	DELETE FROM [ImportTemplate] WHERE TableID =@nTableID
	PRINT '[ImportTemplate]'
	--
	DELETE FROM [ExportTemplateItem] WHERE ExportTemplateID IN (SELECT E.ExportTemplateID FROM [ExportTemplate] E WHERE E.TableID=@nTableID)
	PRINT '[ExportTemplateItem]'
	DELETE FROM [ExportTemplate] WHERE TableID =@nTableID
	PRINT '[ExportTemplate]'
	--
	DELETE FROM [SearchGroupColumn] WHERE SearchGroupID IN (SELECT SG.SearchGroupID FROM [SearchGroup] SG INNER JOIN [Table] T ON SG.TableID=T.TableID WHERE T.TableID= @nTableID)
	PRINT '[SearchGroupColumn]'
	DELETE FROM [SearchGroup] WHERE TableID IN (SELECT SG.TableID FROM [SearchGroup] SG INNER JOIN [Table] T ON SG.TableID=T.TableID WHERE T.TableID= @nTableID)
	PRINT '[SearchGroup]'
	--
	DELETE FROM [RoleTable] WHERE TableID IN (SELECT T.TableID FROM  [Table] T  WHERE T.TableID= @nTableID)
	PRINT '[RoleTable]'
	UPDATE [UserRole] SET DataScopeTableID=NULL WHERE DataScopeTableID=@nTableID
	UPDATE [UserRole] SET DataScopeColumnID=NULL WHERE DataScopeColumnID  IN (SELECT ColumnID FROM [Column]  WHERE TableID = @nTableID)
	PRINT '[UserRole]'


	DELETE FROM [SystemOption] WHERE  TableID =@nTableID
	PRINT '[SystemOption]'

	DELETE FROM [TableChild] WHERE ParentTableID = @nTableID
									OR ChildTableID  = @nTableID
	PRINT '[TableChild]'

	DELETE FROM Batch 
		WHERE TableID = @nTableID
	
	PRINT 'Batch'
	UPDATE [Table] SET [FilterColumnID] = NULL,ParentTableID=NULL,
		[AddUserPasswordColumnID] = NULL,DataUpdateUniqueColumnID=NULL,
		[AddUserUserColumnID] = NULL,
		[SortColumnID] = NULL,ValidateColumnID1=NULL,UniqueColumnID=NULL,UniqueColumnID2=NULL,
		ValidateColumnID2 = NULL,GraphXAxisColumnID=NULL,GraphSeriesColumnID=NULL,GraphDefaultYAxisColumnID=NULL
		WHERE TableID = @nTableID 

	--Clean old bug related false entry, that has inter account columnid
	--UPDATE FormSetForm SET UpdateColumnID=NULL,UpdateColumnValue=NULL
	 --WHERE UpdateColumnID IN (SELECT C.ColumnID FROM [Column] C WHERE C.TableID = @nTableID)

	 UPDATE [Column] SET AvgColumnID =NULL WHERE AvgColumnID IN (SELECT C.ColumnID FROM [Column] C WHERE C.TableID = @nTableID)
	 UPDATE [Column] SET LinkedParentColumnID =NULL WHERE LinkedParentColumnID IN (SELECT C.ColumnID FROM [Column] C WHERE C.TableID = @nTableID)
	 UPDATE [Column] SET ParentColumnID =NULL WHERE ParentColumnID IN (SELECT C.ColumnID FROM [Column] C WHERE C.TableID = @nTableID)
	 UPDATE [Column] SET FilterOtherColumnID =NULL WHERE FilterOtherColumnID IN (SELECT C.ColumnID FROM [Column] C WHERE C.TableID = @nTableID)
	 UPDATE [Column] SET FilterParentColumnID =NULL WHERE FilterParentColumnID IN (SELECT C.ColumnID FROM [Column] C WHERE C.TableID = @nTableID)
	 UPDATE [Column] SET MapPinHoverColumnID =NULL WHERE MapPinHoverColumnID IN (SELECT C.ColumnID FROM [Column] C WHERE C.TableID = @nTableID)
	  UPDATE [Column] SET CompareColumnID =NULL WHERE CompareColumnID IN (SELECT C.ColumnID FROM [Column] C WHERE C.TableID = @nTableID)
	 UPDATE [Column] SET TrafficLightColumnID =NULL WHERE TrafficLightColumnID IN (SELECT C.ColumnID FROM [Column] C WHERE C.TableID = @nTableID)
 
	 UPDATE [Column] SET TableTableID=NULL  WHERE TableTableID = @nTableID
	 UPDATE [Column] SET DefaultRelatedTableID=NULL  WHERE DefaultRelatedTableID = @nTableID
	 UPDATE [Table] SET FilterColumnID = NULL WHERE FilterColumnID IN (SELECT C.ColumnID FROM [Column] C WHERE C.TableID = @nTableID)
	  UPDATE [Table] SET SortColumnID = NULL WHERE SortColumnID IN (SELECT C.ColumnID FROM [Column] C WHERE C.TableID = @nTableID)

		UPDATE [Table] SET ParentTableID = NULL WHERE ParentTableID = @nTableID
	
	DELETE FROM [Column] WHERE TableID = @nTableID
	PRINT '[Column]'

									
	--
	DELETE FROM AuditRaw WHERE TableID = @nTableID
	PRINT 'AuditRaw'
	DELETE FROM Audit WHERE TableID = @nTableID
	PRINT 'Audit'
	--

	DELETE FROM [Table] WHERE TableID =@nTableID
	PRINT '[Table]'
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_Table_Delete_Permanent', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
