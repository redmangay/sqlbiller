﻿-- =============================================
-- Author:		<Red Mangay>
-- Create date:	<28/11/2018>
-- Description:	<>
-- =============================================
/*

exec [Renzo_GetCreaditDetailsTabBalances_EditBAPA]  2744065

*/


CREATE PROCEDURE [dbo].[Renzo_GetCreaditDetailsTabBalances_EditBAPA]
(
	@RecordID int
	
)
AS
BEGIN
	
   --SELECT @TaxAmount =  ISNULL(CAST(CONVERT(VARCHAR(20),V063,1) AS money),0)  FROM Record WHERE ISACTIVE=1 AND TableID='2711' AND RECORDID= @RecordID 

    DECLARE @TaxAmount money
    DECLARE @TaxName varchar(max)
    DECLARE @TotalAmount money
   -- DECLARE @JobRecordID int

   -- SELECT @JobRecordID = V002 FROM RECORD WHERE RECORDID = @RecordID

    /*Timehseet Details tab: Get the Tax Amount */
   --SELECT @TaxAmount =  ISNULL(CAST(CONVERT(VARCHAR(20),V063,1) AS money),0)  FROM Record WHERE ISACTIVE=1 AND TableID='2711' AND RECORDID= @RecordID 
   SELECT  @TaxAmount =  V047 FROM RECORD WHERE RECORDID = @RecordID

      /*Timehseet Details tab: Get the Tax Name */
   -- SELECT @TaxName = V001 FROM RECORD WHERE RECORDID = (SELECT V042 FROM RECORD WHERE RECORDID =@RecordID)

      /*Timehseet Details tab: Get the Total */
	 SELECT @TotalAmount = V045 FROM RECORD WHERE RECORDID = @RecordID

 --  SELECT  @TotalAmount = ISNULL(SUM(CAST(CONVERT(VARCHAR(20),V020,1) AS money)),0) FROM Record WHERE ISACTIVE=1 AND TableID='2729' AND  V016 = CAST(@RecordID AS VARCHAR)  AND V001 = CAST(@JobRecordID AS VARCHAR)
    --SET @TotalAmount = @TotalAmount + @TaxAmount
    
    SELECT 'Tax Amount' AS [Label], isnull(@TaxAmount,0.00) AS [Amount]
    UNION ALL
    SELECT 'Total' AS [Label], isnull(@TotalAmount,0.00) AS [Amount]



END

