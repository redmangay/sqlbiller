﻿
---------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[Account_Select]
(
	@nAccountID int  = NULL,
	@sAccountName nvarchar(100)  = NULL,	
	@dDateAdded datetime  = NULL,
	@dDateUpdated datetime  = NULL,
	@sOrder nvarchar(200) = AccountID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	EXEC [dbo].[Account_Select]
	---------------------------------------------------------------*/
 
AS

BEGIN TRY

	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND AccountID = '+CAST(@nAccountID AS NVARCHAR)
	IF @sAccountName IS NOT NULL 
		SET @sWhere = @sWhere + ' AND AccountName LIKE '+'''%' + @sAccountName + '%'''

	IF @dDateAdded IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DateAdded = '+CAST(@dDateAdded AS NVARCHAR)
	IF @dDateUpdated IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DateUpdated = '+CAST(@dDateUpdated AS NVARCHAR)
	SET @sSelect = 'SELECT * FROM 
	(SELECT AccountID, AccountName,  DateAdded, DateUpdated, IsActive,AccountTypeID,ExpiryDate, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum FROM [Account]' + @sWhere + ') as AccountInfo'
	+ ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)

	SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [Account] ' + @sWhere 
 
 
	EXEC (@sSelect)
	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Account_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
