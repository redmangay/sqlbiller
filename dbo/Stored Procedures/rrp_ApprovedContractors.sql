﻿

CREATE PROCEDURE [dbo].[rrp_ApprovedContractors]
(
	@AccountID int,
	@AsAtDate date
)
AS 
-- Test: EXEC rrp_ApprovedContractors @AccountID = 24822, @AsAtDate='2013-01-01', @ToDate='2015-01-01'

-- Workaround: Define the columne in here where it will never be run:
BEGIN TRY
	IF 1 = 2
		 BEGIN
		   -- These are the actual column names and types returned by the real proc
		   SELECT CAST('' AS nvarchar(200)) AS Title, 
				  CAST('' AS nvarchar(200)) AS [Item],
				  0 as [Count]
		 END;
	-- End Workaround

	DECLARE @sAsAtDate varchar(10), @sToDate varchar(10)
	SELECT @sAsAtDate = CONVERT( varchar(10), @AsAtDate, 103)

	DECLARE @sSQL varchar(MAX)
	SET @sSQL = '
	DECLARE @Results table ([Item] varchar(50), [Count] int);
	SET DATEFORMAT DMY;
	INSERT INTO @Results ([Item], [Count]) SELECT ''Approved'',     COUNT(*) FROM Account' + CAST(@AccountID as varchar) + '.vCompany 
		WHERE [DateAdded] < CAST('''	+ @sAsAtDate + ''' as date)
		AND [Approval Status] = ''Approved''; 
	INSERT INTO @Results ([Item], [Count]) SELECT ''Not Approved'', COUNT(*) FROM Account' + CAST(@AccountID as varchar) + '.vCompany 
		WHERE [DateAdded] < CAST('''	+ @sAsAtDate + ''' as date)
		AND [Approval Status] IS NULL; 
	INSERT INTO @Results ([Item], [Count]) SELECT ''Total'', COUNT(*) FROM Account' + CAST(@AccountID as varchar) + '.vCompany 
		WHERE [DateAdded] < CAST('''	+ @sAsAtDate + ''' as date); 


	SELECT ''As At: ' + @sAsAtDate + ''' AS Title,* FROM	@Results ;
	'
	PRINT @sSQL
	EXEC (@sSQL)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('rrp_ApprovedContractors', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
