﻿
 
CREATE PROCEDURE [dbo].[ets_Report_CustomFileName]
(
	@nRecordID int ,
	@nDocumentID int
)
	/*----------------------------------------------------------------	
	-- HISTORY --	

	-- Red Mar 13 2020: Initial Script


	--== Usage == --
	exec ets_Report_CustomFileName  1 ,904

	---------------------------------------------------------------*/
 
AS

BEGIN TRY 

    /* Define Custom File Name below */
    DECLARE @ReportName varchar(max)
    SELECT @ReportName = DocumentText FROM Document
    WHERE DocumentID = @nDocumentID

	
	IF @ReportName LIKE '%Invoice and Payment%'
    BEGIN
		SELECT 'Invoice and Payment Report'
	END
    ELSE IF @ReportName LIKE '%Invoice%'
    BEGIN
		SELECT V017 + ' ' + 'Invoice' FROM Record
		WHERE RecordID = @nRecordID
	END
	 ELSE IF @ReportName LIKE '%Timesheet and Disbursement%'
    BEGIN
		SELECT 'Timesheet and Disbursement Report'
	END
	
	ELSE IF @ReportName LIKE '%Statement%'
    BEGIN
		SELECT JobInfo.V001 + ' ' + 'Statement' 
		FROM Record BAPA 
		JOIN Record JobInfo ON JobInfo.TableID = 2475
		AND JobInfo.IsActive = 1
		AND JobInfo.RecordID =CAST(BAPA.V002 as int)	
		WHERE BAPA.RecordID = @nRecordID
	END
	ELSE IF @ReportName LIKE '%Authority%'
    BEGIN
		SELECT V001 + ' ' + 'Client Authority' FROM Record
		WHERE RecordID = @nRecordID
	END
	ELSE
	BEGIN
		SELECT @ReportName
	END
	

	DECLARE @ErrorTrackX varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES (@ReportName, '', '', GETDATE(), 'Stored Procedure on ' + DB_NAME())


END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Report_CustomFileName', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
