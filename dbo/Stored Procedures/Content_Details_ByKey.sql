﻿

CREATE PROCEDURE [dbo].[Content_Details_ByKey]
(
	@sContentKey  nvarchar(50),
	@nAccountID int=NULL
)
AS

--EXEC [Content_Details_ByKey] @sContentKey='DataUploadEmail', @nAccountID=13
--EXEC [Content_Details_ByKey] @sContentKey='DataUploadWarningSMS', @nAccountID=13

--EXEC [Content_Details_ByKey] @sContentKey='RegistrationEmailWizard' 

BEGIN TRY
	
	IF @nAccountID IS NULL
	BEGIN
		IF EXISTS (SELECT   * FROM    Content 
				WHERE ContentKey=@sContentKey AND AccountID IS NULL)
				BEGIN
					SELECT   * FROM    Content 
				WHERE ContentKey=@sContentKey AND AccountID IS NULL
				END
		ELSE
			BEGIN
				SELECT   * FROM    Content 
				WHERE ContentKey=@sContentKey 
			END
    END
    IF @nAccountID IS NOT NULL
    BEGIN
		IF EXISTS (SELECT ContentID FROM Content WHERE ContentKey=@sContentKey AND AccountID= @nAccountID)
			BEGIN
				SELECT * FROM Content WHERE ContentKey=@sContentKey AND AccountID= @nAccountID
			END
		ELSE
			BEGIN
				SELECT * FROM Content WHERE ContentKey=@sContentKey AND AccountID IS NULL
			END
		
    END
    
	
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Content_Details_ByKey', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
