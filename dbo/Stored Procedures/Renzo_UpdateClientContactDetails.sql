﻿-- =============================================
-- Author:		Barry Jone Matutina
-- Create date: 10/14/2016
-- Description:	Stored procedure to get client contacts needed for Ticket 1755
-- =============================================
CREATE PROCEDURE Renzo_UpdateClientContactDetails 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT		[Client Contact].[TableID]
				,[Client Contact].RecordID	AS				[Record ID]
				,[Client Contact].V001		AS				[ID]
				,[Client Contact].V002		AS				[ClientID]
				,[Client Contact].V004		AS				[Title]
				,[Client Contact].V005		AS				[Name]
				,[Client Contact].V006		AS				[LastName]
				,[Client Contact].V007		AS				[Designation]
				,[Client Contact].V008		AS				[Phone]
				,[Client Contact].V009		AS				[MobilePhone]
				,[Client Contact].V010		AS				[Fax]
				,[Client Contact].V011		AS				[Email]
				,[Client Contact].V003		AS				[MailOuts]
				,[Client Contact].DateAdded
	FROM		[Record] [Client Contact] WITH (NOLOCK)
	LEFT JOIN	Record [V002] ON					[V002].TableID = '2474'		/*	Office Information	*/
											AND		[V002].IsActive = 1
											AND		CAST([V002].RecordID AS VARCHAR(MAX)) = [Client Contact].V002		/*	V002	*/
	WHERE		[Client Contact].TableID = '2657'		/*	Client Contact	*/
	AND			[Client Contact].IsActive = 1
	

	SELECT		[Client Contact].[TableID]
				,GETDATE()
				,[Client Contact].EnteredBy
				,[Client Contact].IsActive
				,GETDATE()
				,V001				/*	[ID]	*/
				,V002				/*	[ClientID]	*/
				,V004				/*	[Title]	*/
				,V005				/*	[Name]	*/
				,V006				/*	[LastName]	*/
				,V007				/*	[Designation]	*/
				,V008				/*	[Phone]	*/
				,V009				/*	[MobilePhone]	*/
				,V010				/*	[Fax]	*/
				,V011				/*	[Email]	*/
				,V003				/*	[MailOuts]	*/
				,[Client Contact].DateAdded
	FROM		[Record] [Client Contact]
	WHERE		[Client Contact].TableID = '2657'		/*	Client Contact	*/
	AND			[Client Contact].IsActive = 1


END
