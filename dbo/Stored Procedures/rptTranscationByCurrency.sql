﻿  
CREATE PROCEDURE [dbo].[rptTranscationByCurrency]   
 @StartDateSundryDebtors DateTime = null  
AS  
BEGIN  
IF (SELECT object_id('TempDB..#tempTable1')) IS NOT NULL  
BEGIN  
    DROP TABLE #tempTable1  
END  
  
IF (SELECT object_id('TempDB..#tempTableGroup1')) IS NOT NULL  
BEGIN  
    DROP TABLE #tempTableGroup1  
END  
  
IF (SELECT object_id('TempDB..#tempTableSub')) IS NOT NULL  
BEGIN  
    DROP TABLE #tempTableSub  
END  
  
SELECT  CASE WHEN ISNULL(p.[Group/Division],'')='' or ISNULL(b.[Group Currency], '')=''  
            THEN CAST(ISNULL(b.[Debtors Converted], 0) AS MONEY)  
            ELSE CAST(ISNULL(b.[Rpt Debtors Converted], 0) AS MONEY)   
       END AS Debtors,   
ISNULL(b.[Group Currency], '') AS [Group Currency], CONVERT(Datetime, b.[Date], 103)  as [Date] , b.[Project Number]  
into #tempTable1  
FROM          
    Account24918.[vBilling Information] AS b INNER JOIN  
                         Account24918.vProject AS p ON b.[Project Number] = p.ProjectNumber  
WHERE        (b.[Project Number] IN  
                             (SELECT        ProjectNumber  
                               FROM            Account24918.vProject))  
ORDER BY b.Date, b.[Project Number]  
  
select SUM(Debtors) AS DBSum,[Group Currency] into #tempTableSub  
from #tempTable1   
group by [Group Currency]  
  
select SUM(Debtors) AS DBSum,[Group Currency] AS GC into #tempTableGroup1   
from #tempTable1   
where Date >= @StartDateSundryDebtors 
group by [Group Currency]  
  
select (t1.DBSum-ISNULL(t2.DbSum,0)) As Begining_Balance,t1.[Group Currency]   
FROM #tempTableSub t1 LEFT JOIN #tempTableGroup1 t2  
on t1.[Group Currency]=t2.GC  
       
  
END  