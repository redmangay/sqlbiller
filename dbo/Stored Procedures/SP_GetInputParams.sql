﻿

-- =============================================
-- Author:		Dathq
-- Create date: 24 Oct 2011
-- Description:	Get stored procedure's params
-- =============================================
CREATE PROCEDURE [dbo].[SP_GetInputParams] 
	-- Add the parameters for the stored procedure here
	@SPName AS varchar(100)
AS
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT	PARAMETER_NAME,
			DATA_TYPE,
			ISNULL(CHARACTER_MAXIMUM_LENGTH, 0) AS CHARACTER_MAXIMUM_LENGTH
	FROM	INFORMATION_SCHEMA.PARAMETERS
	WHERE	SPECIFIC_NAME = @SPName
			AND PARAMETER_MODE = 'IN'			
	ORDER BY ORDINAL_POSITION
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('SP_GetInputParams', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
