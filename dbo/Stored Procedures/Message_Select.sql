﻿

CREATE PROCEDURE   [dbo].[Message_Select]
(
	@nRecordID int  = NULL,
	@nTableID INT= NULL,
	@nAccountID INT= NULL,
	@dDateTimeFrom datetime  = NULL,
	@dDateTimeTo datetime  = NULL,
	@sMessageType char(1)  = NULL,
	@sDeliveryMethod char(1)  = NULL,
	@bIsIncoming bit  = NULL,
	@sOtherParty varchar(MAX)  = NULL,
	@sSubject varchar(MAX)  = NULL,
	@sBody varchar(MAX)  = NULL,
	@sLink varchar(MAX)  = NULL,
	@sExternalMessageKey varchar(1000)  = NULL,
	@sOrder nvarchar(200) = MessageID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	-- Stored Procedure: Message_Select
	-- Generated by: gpGenerateSP (Jon Bosker, DB Gurus Australia) 
	-- Date: Mar  9 2016  9:01AM
	---------------------------------------------------------------*/
 
AS
 
BEGIN TRY
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(4000)
	DECLARE @sWhere nvarchar(4000)
	DECLARE @sSelectCount nvarchar(MAX)

	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'

	IF @nRecordID IS NOT NULL 
		BEGIN
			SET @sWhere = @sWhere + ' AND RecordID = '+CAST(@nRecordID AS NVARCHAR)
			SET @nTableID=NULL;
			SET @nAccountID=NULL;
		END
	
	IF @nTableID IS NOT NULL 
		BEGIN
			SET @sWhere = @sWhere + ' AND M.TableID = '+CAST(@nTableID AS NVARCHAR)
			SET @nAccountID=NULL;
		END

	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND M.AccountID = '+CAST(@nAccountID AS NVARCHAR)





	IF @dDateTimeFrom IS NOT NULL and @dDateTimeTo IS NOT NULL
				SELECT @sWhere = @sWhere + ' AND [DateTime]>= ''' + CAST(@dDateTimeFrom AS varchar) + ''''

	IF @dDateTimeFrom IS NOT NULL and @dDateTimeTo IS  NULL
				SELECT @sWhere = @sWhere + ' AND [DateTime] = ''' + CAST(@dDateTimeFrom AS varchar) + ''''
 
	IF @dDateTimeTo IS NOT NULL
				SELECT @sWhere = @sWhere + ' AND [DateTime] <=  ''' + CAST(@dDateTimeTo AS varchar) + ''''





	IF @sMessageType IS NOT NULL 
		SET @sWhere = @sWhere + ' AND MessageType = '+'''' + @sMessageType + ''''
	IF @sDeliveryMethod IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DeliveryMethod = '+'''' + @sDeliveryMethod + ''''
	IF @bIsIncoming IS NOT NULL and @bIsIncoming=1
		SET @sWhere = @sWhere + ' AND IsIncoming = '+CAST(@bIsIncoming AS NVARCHAR)
	IF @bIsIncoming IS NOT NULL and @bIsIncoming=0
		SET @sWhere = @sWhere + ' AND IsIncoming IS NULL '



	IF @sOtherParty IS NOT NULL 
		SET @sWhere = @sWhere + ' AND OtherParty LIKE '+'''%' + @sOtherParty + '%'''
	IF @sSubject IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Subject LIKE '+'''%' + @sSubject + '%'''
	IF @sBody IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Body LIKE '+'''%' + @sBody + '%'''
	IF @sLink IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Link LIKE '+'''%' + @sLink + '%'''
	IF @sExternalMessageKey IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ExternalMessageKey = '+'''' + @sExternalMessageKey + ''''
	SET @sSelect = 'SELECT * FROM 
	(SELECT M.*,TableName, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum FROM Message M LEFT JOIN [Table] T
	 ON M.TableID=T.TableID ' + @sWhere + ') as UserInfo'
 
	  SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [Message] M  ' + @sWhere 

	-- Extend WHERE to include paging:
	SET @sWhere = ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	SET @sOrder = ' ORDER BY ' + @sOrder
	SET @sSelect = @sSelect + @sWhere + @sOrder
 
	EXEC (@sSelect)
	SET ROWCOUNT 0

	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Message_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
