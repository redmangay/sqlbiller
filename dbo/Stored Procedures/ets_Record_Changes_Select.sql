﻿

CREATE PROCEDURE [dbo].[ets_Record_Changes_Select]
(
	@nRecordID int,
	@nTableID int,
	@sOrder nvarchar(200)='ChangeID DESC', 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
AS
/*

*/
BEGIN TRY
	SET NOCOUNT ON;

	DECLARE @tDisplayColumns TABLE
	(
		ID int identity(1,1),
		ColumnID int,
		SystemName varchar(50),
		DisplayText varchar(50)
	)

	INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
		SELECT ColumnID, DisplayName, SystemName
			FROM [Column] 
			WHERE TableID = @nTableID AND SystemName not in('EnteredBy','RecordID','TableID')
			ORDER BY DisplayOrder

	DECLARE @sSQL varchar(MAX)
	DECLARE @sSelectCount varchar(MAX)
	DECLARE @counter int
	SET @counter = 1
		

  -- Get the sOrder IF it is NULL
   WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID >= @counter)
	BEGIN
		
		IF @sOrder is NULL
		 SELECT @sOrder= ' [' + DisplayText  + ']' FROM @tDisplayColumns WHERE ID = @counter
		 SET @counter = @counter + 1
	END 
	
  SELECT @sSQL = 'SELECT * FROM (SELECT RecordInfo.*,ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ' )
   as RowNum FROM (SELECT Record_Changes.ChangeID,Record_Changes.DateUpdated as [Date Changed], '
  
  
    SET @counter = 1
	WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID >= @counter)
	BEGIN
	   
		SELECT @sSQL = @sSQL + 'Record_Changes.' + SystemName + ' AS [' + DisplayText + '], ' 
			FROM @tDisplayColumns 
			WHERE ID = @counter
		--PRINT @sSQL
		SET @counter = @counter + 1
		IF @sOrder is NULL
		 SELECT @sOrder= ' [' + DisplayText  + ']' FROM @tDisplayColumns WHERE ID = @counter
	END 

	SELECT @sSQL = LEFT(@sSQL, LEN(@sSQL)-1) 
	
	
	SELECT @sSQL = @sSQL + ',(SELECT Email from [User]  where userid=Record_Changes.[LastUpdatedUserID]) as [User Updated] FROM [Record_Changes]'

	IF PATINDEX('%Record_Changes.LocationID%', @sSQL) > 0
	BEGIN
		SET @sSQL = REPLACE(@sSQL, 'Record_Changes.LocationID', 'Location.LocationName')
		SET @sSQL = REPLACE(@sSQL, 'FROM [Record_Changes]', 'FROM [Record_Changes] LEFT JOIN Location ON Record_Changes.LocationID = Location.LocationID')
	END	

	IF PATINDEX('%Record_Changes.TableID%', @sSQL) > 0
	BEGIN
		SET @sSQL = REPLACE(@sSQL, 'Record_Changes.TableID', '[Table].TableName')
		SET @sSQL = REPLACE(@sSQL, 'FROM [Record_Changes]', 'FROM [Record_Changes] JOIN [Table] ON Record_Changes.TableID = [Table].TableID')
	END	

	--IF PATINDEX('%Record_Changes.EnteredBy%', @sSQL) > 0
	--BEGIN
	--	SET @sSQL = REPLACE(@sSQL, 'Record_Changes.EnteredBy', '[User].Email')
	--	SET @sSQL = REPLACE(@sSQL, 'FROM [Record_Changes]', 'FROM [Record_Changes] JOIN [User] ON Record_Changes.EnteredBy = [User].UserID')
	--END	
	
	--IF PATINDEX('%Record_Changes.LastUpdatedUserID%', @sSQL) > 0
	--BEGIN
	--	SET @sSQL = REPLACE(@sSQL, 'Record_Changes.LastUpdatedUserID', '[User].Email')
	--	SET @sSQL = REPLACE(@sSQL, 'FROM [Record_Changes]', 'FROM [Record_Changes] JOIN [User] ON Record_Changes.LastUpdatedUserID = [User].UserID')
	--END	
	

	-- ADD IN THE WHERE CRITERIA
	SELECT @sSQL = @sSQL + ' WHERE Record_Changes.RecordID = ' + CAST(@nRecordID as varchar)
	

    
    SELECT @sSelectCount= @sSQL + ') as RecordInfo) as RecordFinalInfo'
	SELECT @sSQL= @sSQL + ') as RecordInfo) as RecordFinalInfo WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
    --SET NOCOUNT OFF;
    
    SET @sSelectCount= 'SELECT COUNT(*) AS TotalRows FROM Record_Changes WHERE Record_Changes.RecordID = ' + CAST(@nRecordID as varchar)
    
    
    SET ROWCOUNT @nMaxRows
	PRINT @sSQL
	EXEC (@sSQL)
	-- PRINT @sTempTable
	
	SET ROWCOUNT 0
 
   PRINT @sSelectCount
   EXEC (@sSelectCount)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Record_Changes_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
