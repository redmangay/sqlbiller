﻿CREATE PROCEDURE Kate_DefaultCentre
(
	@ColumnID int,
	@RecordID int,
	@UserID int,
	@Return varchar(max) output
)
AS
BEGIN
IF @UserID = 25121	-- Sarah	Adams
		or @UserID = 25122 -- Sandy Thompson
	SET @Return = 769114 -- Sydney Children’s Hospital
	
ELSE IF @UserID = 25123
	SET @Return = 769113 -- The Children’s Hospital Westmead
	
ELSE IF @UserID = 25124
	SET @Return =769115 -- John Hunter Children’s Hospital
	
ELSE
	SET @Return = 0 -- not sure what to return for default

END
