﻿

CREATE PROCEDURE [dbo].[Table_CheckUserPermission] 
	@nTableId INT,
	@sEmail NVARCHAR(200)
AS
-- =============================================
-- Description:	Check user permission on table
-- =============================================
/*
REVISION HISTORY
----------------
17 Jan 2017: DH Created SP
25 Jan 2016: JB Rewrote it to take into account SystemRoles and RoleTypes
*/
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @HasPermission bit = 0

	IF EXISTS
		(
			SELECT TOP 1 * 
				FROM [User] U
				JOIN [UserRole] UR ON UR.UserID = U.UserID 
				JOIN [Role] R ON R.RoleID = UR.RoleID 
				WHERE U.Email = @sEmail
				AND R.IsSystemRole = 1
				AND R.RoleType != 7 /* None */
		)
		SET @HasPermission = 1
	ELSE
	BEGIN
		IF EXISTS
			(
				SELECT TOP 1 * 
					FROM [User] U
					JOIN [UserRole] UR ON UR.UserID = U.UserID 
					JOIN [Role] R ON R.RoleID = UR.RoleID 
					JOIN [RoleTable] RT ON RT.RoleID = R.RoleID 
					WHERE U.Email = @sEmail
					AND R.IsSystemRole = 0
					AND RT.TableID = @nTableId
					AND RT.RoleType !=  6 /* None */
					AND RT.CanExport = 1
			)
			SET @HasPermission = 1
	END
	SELECT @HasPermission
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Table_CheckUserPermission', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
