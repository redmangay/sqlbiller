﻿


CREATE PROCEDURE [dbo].[UserSearch_InsertUpdate]
(
 @nUserID int,
 @nViewID int,
 @sSearchXML varchar(MAX)
)
AS

BEGIN TRY
	DECLARE @nUserSearchID INT=NULL

	SELECT @nUserSearchID=UserSearchID FROM UserSearch WHERE UserID=@nUserID AND ViewID=@nViewID

	IF @nUserSearchID IS NOT NULL
		UPDATE UserSearch SET [SearchXML]=@sSearchXML, DateUpdated=GETDATE() WHERE UserSearchID=@nUserSearchID
	ELSE
		INSERT INTO [UserSearch]
		 ([UserID], [ViewID], [SearchXML])
		VALUES
		 (@nUserID, @nViewID, @sSearchXML)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('UserSearch_InsertUpdate', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
