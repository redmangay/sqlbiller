﻿
 
CREATE PROCEDURE [dbo].[LookUpData_Select]
(
	@nLookupDataID int  = NULL,
	@nLookupTypeID int  = NULL,
	@sDisplayText varchar(200)  = NULL,
	@sDisplayTextNotLike varchar(200)  = NULL,
	@sValue varchar(200)  = NULL,
	@dDateAdded datetime  = NULL,
	@dDateUpdated datetime  = NULL,
	@sOrder nvarchar(200) = LookupDataID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	EXEC LookUpData_Select @nLookupTypeID=-5,@sDisplayTextNotLike=']>'
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nLookupDataID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND LookupDataID = '+CAST(@nLookupDataID AS NVARCHAR)
	IF @nLookupTypeID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND LookupTypeID = '+CAST(@nLookupTypeID AS NVARCHAR)
	IF @sDisplayText IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DisplayText LIKE '+'''%' + @sDisplayText + '%'''

	IF @sDisplayTextNotLike IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DisplayText NOT LIKE '+'''%' + @sDisplayTextNotLike + '%'''

	
	IF @sValue IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Value LIKE '+'''%' + @sValue + '%'''
	
	IF @dDateAdded IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DateAdded = '+CAST(@dDateAdded AS NVARCHAR)
	IF @dDateUpdated IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DateUpdated = '+CAST(@dDateUpdated AS NVARCHAR)
	SET @sSelect = 'SELECT * FROM 
	(SELECT [LookUpData].*, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum FROM [LookUpData]' + @sWhere + ') as UserInfo'
	SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [LookUpData] ' + @sWhere 
 
	-- Extend WHERE to include paging:
	SET @sWhere = @sWhere + ' AND RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	SET @sOrder = ' ORDER BY ' + @sOrder
	SET @sSelect = @sSelect + @sWhere + @sOrder
 
	EXEC (@sSelect)
	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('LookUpData_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
