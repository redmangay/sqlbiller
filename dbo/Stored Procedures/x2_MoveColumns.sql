﻿
----------------------------------------------------------------------------
-- 18 June 2015
----------------------------------------------------------------------------
CREATE PROCEDURE x2_MoveColumns
(
	 @TargetTableID int, 
	 @SourceTableID int,
	 @PageTitle varchar(MAX)
)
AS
BEGIN
	DECLARE @Position int
	SELECT @Position FROM TableTab WHERE TableID = @TargetTableID
	SELECT @Position = ISNULL(@Position,0) + 1
	
	INSERT INTO TableTab (TableID, TabName, DisplayOrder) VALUES (@TargetTableID, @PageTitle, @Position)
	DECLARE @n int
	SELECT @n = TableTabID FROM TableTab WHERE TableID = @TargetTableID AND DisplayOrder = @Position

	DECLARE @v int
	SELECT @v = CAST(SUBSTRING(MAX(SystemName),2,3) AS int) FROM [Column] WHERE TableID = @TargetTableID AND SystemName LIKE 'V%'
	PRINT @v

	UPDATE 	[Column] 
		SET TableID = @TargetTableID,
	--SELECT 
		[TableTabID] = @n,
		SystemName = 'V' + RIGHT('000' + CAST((CAST(SUBSTRING(SystemName,2,3) AS int) + @v) AS varchar(3)),3),
		[ViewName] = CAST(@TargetTableID AS varchar) + ',' + CAST(@SourceTableID AS varchar)  + ',' +  @PageTitle  -- So we can find it later if we need to
	--from [Column] 
		WHERE TableID = @SourceTableID AND SystemName LIKE 'V%'
		AND [DisplayName] not in (SELECT [DisplayName] FROM [Column] WHERE TableID = @TargetTableID)
END
