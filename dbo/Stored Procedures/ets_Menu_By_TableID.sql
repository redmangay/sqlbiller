﻿

CREATE PROCEDURE   [dbo].[ets_Menu_By_TableID]
(
	@nTableID int 
)
AS
/*
EXEC [ets_Menu_Parent_Table] 1
*/
BEGIN TRY
	SET NOCOUNT ON;

	SELECT   TOP 1 M.*
FROM         Menu M    INNER JOIN [Table] T
ON M.TableID=T.TableID WHERE  M.TableID=@nTableID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Menu_By_TableID', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
