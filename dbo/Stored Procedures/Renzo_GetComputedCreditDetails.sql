﻿-- =============================================
-- Author:		<Red Mangay>
-- Create date:	<10/07/2018>
-- Description:	<>
-- =============================================
/*

exec [Renzo_GetComputedCreditDetails]  @RecordID=   15221835 , @param001 =275,@BAPARecordID=  15235170

*/


CREATE PROCEDURE [dbo].[Renzo_GetComputedCreditDetails]
(
	@RecordID int -- Job Info RecorID
	,@param001 money -- Credit Amount
	,@BAPARecordID int = null
	,@EnteredBy int = null
	,@IsUpdate bit = 0
	
)
AS
BEGIN
	
	
DECLARE @Data_Output TABLE
(
TableID varchar(max),
V001 VARCHAR(MAX),
V002 VARCHAR(MAX),
V003 VARCHAR(MAX),
V004 VARCHAR(MAX),
V005 VARCHAR(MAX),
V006 VARCHAR(MAX),
V007 VARCHAR(MAX),
V008 VARCHAR(MAX),
V009 VARCHAR(MAX),
V010 VARCHAR(MAX),
V011 VARCHAR(MAX),
V012 VARCHAR(MAX),
V013 VARCHAR(MAX),
V014 VARCHAR(MAX),
V015 VARCHAR(MAX),
V016 VARCHAR(MAX),
V017 VARCHAR(MAX),
V018 VARCHAR(MAX),
V019 VARCHAR(MAX),
V020 VARCHAR(MAX),
V021 VARCHAR(MAX),
V022 VARCHAR(MAX),
V023 VARCHAR(MAX),
V024 VARCHAR(MAX),
V025 VARCHAR(MAX),
V026 VARCHAR(MAX),
V027 VARCHAR(MAX),
V028 VARCHAR(MAX),
V029 VARCHAR(MAX),
V030 VARCHAR(MAX),
V031 VARCHAR(MAX),
V032 VARCHAR(MAX),
V033 VARCHAR(MAX),
V034 VARCHAR(MAX),
V035 VARCHAR(MAX),
V036 VARCHAR(MAX),
V037 VARCHAR(MAX),
V038 VARCHAR(MAX),
V039 VARCHAR(MAX),
V040 VARCHAR(MAX),
V041 VARCHAR(MAX),
V042 VARCHAR(MAX),
V043 VARCHAR(MAX),
V044 VARCHAR(MAX),
V045 VARCHAR(MAX),
V046 VARCHAR(MAX),
V047 VARCHAR(MAX),
V048 VARCHAR(MAX),
V049 VARCHAR(MAX),
V050 VARCHAR(MAX),
V051 VARCHAR(MAX),
V052 VARCHAR(MAX),
V053 VARCHAR(MAX),
V054 VARCHAR(MAX),
V055 VARCHAR(MAX),
V056 VARCHAR(MAX),
V057 VARCHAR(MAX),
V058 VARCHAR(MAX),
V059 VARCHAR(MAX),
V060 VARCHAR(MAX),
V061 VARCHAR(MAX),
V062 VARCHAR(MAX),
V063 VARCHAR(MAX),
V064 VARCHAR(MAX),
V065 VARCHAR(MAX),
V066 VARCHAR(MAX),
V067 VARCHAR(MAX),
V068 VARCHAR(MAX),
V069 VARCHAR(MAX),
V070 VARCHAR(MAX),
V071 VARCHAR(MAX),
V072 VARCHAR(MAX),
V073 VARCHAR(MAX),
V074 VARCHAR(MAX),
V075 VARCHAR(MAX),
V076 VARCHAR(MAX),
V077 VARCHAR(MAX),
V078 VARCHAR(MAX),
V079 VARCHAR(MAX),
V080 VARCHAR(MAX),
V081 VARCHAR(MAX),
V082 VARCHAR(MAX),
V083 VARCHAR(MAX),
V084 VARCHAR(MAX),
V085 VARCHAR(MAX),
V086 VARCHAR(MAX),
V087 VARCHAR(MAX),
V088 VARCHAR(MAX),
V089 VARCHAR(MAX),
V090 VARCHAR(MAX),
V091 VARCHAR(MAX),
V092 VARCHAR(MAX),
V093 VARCHAR(MAX),
V094 VARCHAR(MAX),
V095 VARCHAR(MAX),
V096 VARCHAR(MAX),
V097 VARCHAR(MAX),
V098 VARCHAR(MAX),
V099 VARCHAR(MAX),
V100 VARCHAR(MAX)


);
		

DECLARE @TotalTotalBilled money = 0.00
DECLARE @CreditAmountNoTax money = 0.00
--DECLARE @CreditAmountNoTax money = 0.00
DECLARE @TaxRate money = 0.00
DECLARE @TaxAmount money = 0.00


-- lets get the tax
SELECT @TaxRate = 1 + ([V011] / 100.00) FROM [Record] WHERE RECORDID = @RecordID

SET @CreditAmountNoTax = cast(@param001 as money) / @TaxRate 
SET @TaxAmount = @param001 - @CreditAmountNoTax

DECLARE @EmployeesBillingDetails table (
    [Counter] int identity(1,1),
    EmployeeID int,
    EmplyeeName varchar(max),
    Billed varchar(max),
    ShareAmount varchar(max),
    [BillingType] varchar(max)

)

--INSERT INTO @EmployeesBillingDetails (Billed, EmployeeID)
--SELECT SUM(CAST([RECORD].V020 AS MONEY)),V002 FROM [RECORD]
--WHERE ISACTIVE=1 AND [RECORD].V001 = CAST('2715710' AS VARCHAR)
--AND [RECORD].TABLEID = 2729
--group by [RECORD].V002
--order by [RECORD].V002

INSERT INTO @EmployeesBillingDetails (Billed, EmployeeID, [BillingType])
	   SELECT SUM(CAST([RECORD].V031 AS MONEY)),V002, 'Timesheet' FROM [RECORD]
	   WHERE ISACTIVE=1 AND [RECORD].V001 = CAST(@RecordID AS VARCHAR)
		  AND [RECORD].TABLEID = 2729
		  AND [RECORD].ISACTIVE = 1
		  --AND [RECORD].V011 IS NOT NULL
			 group by [RECORD].V002
			 order by SUM(CAST([RECORD].V031 AS MONEY)) ASC

    --INSERT INTO @EmployeesBillingDetails (Billed, EmployeeID, [BillingType])
	   --SELECT SUM(CAST([RECORD].V020 AS MONEY)),V002, 'Disbursement' FROM [RECORD]
	   --WHERE ISACTIVE=1 AND [RECORD].V001 = CAST(@RecordID AS VARCHAR)
		  --AND [RECORD].TABLEID = 2729
		  --AND [RECORD].ISACTIVE = 1
		  --AND [RECORD].V010 IS NOT NULL
			 --group by [RECORD].V002
			 --order by [RECORD].V002




SELECT @TotalTotalBilled = SUM(CAST(V020 AS MONEY)) FROM [RECORD] WHERE ISACTIVE=1 AND V001 = CAST(@RecordID AS VARCHAR) AND TABLEID = 2729 AND V016 IS NOT NULL
DECLARE @Counter INT = 1
DECLARE @MaxCounter int 
SELECT @MaxCounter = MAX(COUNTER) FROM @EmployeesBillingDetails


WHILE EXISTS(SELECT * FROM @EmployeesBillingDetails WHERE [Counter] >= @Counter)
BEGIN
    DECLARE @EmployeeID int, @Billed money = 0.00, @ShareAmount money =0.00, @EmployeeName varchar(max), @PercentageShare DECIMAL(25,13), @Balance money =0.00, @BillingType VARCHAR(MAX)
    SELECT @Billed=Billed,@EmployeeID =EmployeeID,@BillingType = BillingType  FROM @EmployeesBillingDetails WHERE [Counter] = @Counter
    SET @PercentageShare = cast(@Billed as MONEY) / cast(@TotalTotalBilled as MONEY)
    SET @ShareAmount = @PercentageShare * @CreditAmountNoTax
    SET @Balance = @CreditAmountNoTax -@ShareAmount
    SELECT @EmployeeName = V003 FROM [Record] WHERE TABLEID = 2669 AND [Record].RecordID = @EmployeeID
      
--INSERT INTO @Data_Output (V001,V002,V003)
--SELECT CASE WHEN @BillingType = 'Timesheet' THEN @EmployeeName ELSE 'Disbursement' END,@ShareAmount * -1 ,
--CASE WHEN @BillingType = 'Timesheet' THEN @EmployeeID ELSE NULL END

IF (@MaxCounter = @COUNTER)
BEGIN
DECLARE @TotalSharedAmount money 
SELECT @TotalSharedAmount = SUM(CAST(V002 AS MONEY)) FROM @Data_Output

   SET @ShareAmount = @CreditAmountNoTax + @TotalSharedAmount  
END

INSERT INTO @Data_Output (V001,V002,V003,V004)
SELECT @EmployeeName,@ShareAmount * -1 ,
 @EmployeeID, @PercentageShare


SET @Counter = @Counter + 1

END


INSERT INTO @Data_Output (V001,V002)
SELECT 'Tax Amount',@TaxAmount * -1

INSERT INTO @Data_Output (V001,V002)
SELECT 'Total',@param001 * -1

-- For Viewing
SELECT * FROM @Data_Output

-- WHERE ISNUMERIC(V003) = 1 OR V001 = 'Disbursement'

-- lets create a record for Credit Details
IF (@IsUpdate = 1)
BEGIN

    IF EXISTS(SELECT * FROM RECORD WHERE V004 = CAST(@BAPARecordID AS VARCHAR(MAX)) AND TABLEID = 2732 AND ISACTIVE=1)
    BEGIN
	   DELETE FROM RECORD  
	   WHERE V004 = CAST(@BAPARecordID AS VARCHAR(MAX)) AND 
	   TABLEID = 2732 AND 
	   ISACTIVE=1
    END

    INSERT INTO Record ([TableID],[DateTimeRecorded],[EnteredBy],V004,V005,V006,V007 )
    SELECT 2732,GETDATE(),@EnteredBy,@BAPARecordID,V003,V002, V004  FROM @Data_Output
    WHERE ISNUMERIC(V003) = 1 --OR V001 = 'Disbursement'
END


END

