﻿

-- Author:		<Manoj sharma>
-- Create date: <13 March 2016>
-- Description:	<Used as ssrs report and in procedure rptjobperformance>
CREATE procedure [dbo].[usr_rpt_JPInvoicedUnBilledTime]


AS
BEGIN

IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##JPInvoicedUnBilledTime' )) DROP TABLE ##JPInvoicedUnBilledTime
create table ##JPInvoicedUnBilledTime  (
GroupDivision varchar(max),
TimeDate varchar(max),
EInitials varchar(max),
Details varchar(max),
EName varchar(max),
TimeSheet money,
BilledDoller money,
BilledStatus varchar(max),
TimeSheetID varchar(max),
ProjectTitle varchar(max),
JobNo varchar(max),
ProjectValue money

 ) 


     
 insert into ##JPInvoicedUnBilledTime
 SELECT DISTINCT Project.[Group/Division] AS GroupDivision
		, TimeSheet.Date AS TimeDate
		, Employee_List.Initials AS EInitials
		, TimeSheet.Details, EmployeeHistroy.Name AS EName
		, CASE WHEN TimeSheet.[Status]='5' THEN ISNULL((TimeSheet.Total ),0) ELSE ISNULL((TimeSheet.[$_Amount] ),0) END AS TimeSheet
		, ISNULL((TimeSheet.[$_Amount] ),0) AS BilledDoller
		, CASE WHEN TimeSheet.STATUS='0' THEN 'By Rate' ELSE
			CASE WHEN TimeSheet.STATUS='1' THEN 'Hold' ELSE
				CASE WHEN TimeSheet.STATUS='3' THEN 'Fixed' ELSE
					CASE WHEN TimeSheet.STATUS='4' THEN 'No Charge' ELSE
						CASE WHEN TimeSheet.STATUS='5' THEN 'Write-off' ELSE
							CASE WHEN TimeSheet.STATUS='6' THEN 'Adjusted' ELSE
								'NONE'
							END
						END
					END
				END
			END
		  END AS BilledStatus
		, PNEmployeeIDTimeSheetID.TimeSheetID
		, Project.ProjectTitle
		, Project.ProjectNumber AS JobNo
		, (ISNULL(Project.Value,'0') ) AS ProjectValue
	FROM ( Account24918.vEmployee_List Employee_List INNER JOIN Account24918.vEmployeeHistroy EmployeeHistroy ON Employee_List.EmployeeID = EmployeeHistroy.EmployeeID)
		INNER JOIN (Account24918.vProject Project INNER JOIN (Account24918.vPNEmployeeIDTimeSheetID PNEmployeeIDTimeSheetID
		INNER JOIN Account24918.vTimeSheet TimeSheet ON PNEmployeeIDTimeSheetID.TimeSheetID = TimeSheet.TimeSheetID) ON Project.ProjectNumber = PNEmployeeIDTimeSheetID.ProjectNumber) ON Employee_List.EmployeeID = PNEmployeeIDTimeSheetID.EmployeeID
	WHERE (TimeSheet.Billed='False')
 
  
   RETURN;
END;

