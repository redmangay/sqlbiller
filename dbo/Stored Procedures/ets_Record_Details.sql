﻿

CREATE PROCEDURE   [dbo].[ets_Record_Details]
(
	--@nAccountID int,
	@nRecordID int,
	@nTableID int = NULL,
	@bUseArchiveDatabase bit = NULL
)
AS
/*
=========================
CHANGE HISTORY
=========================
Red 04 Jan 18: UPDATE the SP for Archive Database
Red 08 Jan 18: Added new param, nTableID, for archive database




===============
Testing
===============

EXEC [dbo].[ets_Record_DetailS] 1360188,0


*/
BEGIN TRY
	SET NOCOUNT ON;

	DECLARE @tDisplayColumns TABLE
	(
		ID int identity(1,1),
		ColumnID int,
		SystemName varchar(50),
		DisplayText varchar(255)
	)

	INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
		SELECT ColumnID, DisplayTextDetail, SystemName
			FROM [Column] 
			WHERE TableID = @nTableID 
			AND DisplayTextDetail IS NOT NULL 
			-- AND LEN(DisplayTextDetail) > 0
			ORDER BY DisplayOrder

	DECLARE @sSQL varchar(MAX)
	DECLARE @counter int
	SET @counter = 1
	EXEC (@sSQL)
	
	-- Get the labels
	SELECT @sSQL = 'SELECT '
	WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID >= @counter)
	BEGIN
		SELECT @sSQL = @sSQL + '''' + REPLACE(DisplayText,'''','''''') + ''' AS [' + SystemName + '], ' 
			FROM @tDisplayColumns 
			WHERE ID = @counter
		SET @counter = @counter + 1
	END 

	IF @counter=1
		SET @sSQL=@sSQL+ ' RecordID AS RID, '

	SELECT @sSQL = LEFT(@sSQL, LEN(@sSQL)-1) 
	SELECT @sSQL = @sSQL + ' FROM [Record] WHERE RecordID = ' + CAST(@nRecordID as varchar)
	--PRINT @sSQL
	EXEC (@sSQL)


	SET @counter = 1
	-- Get the values
	SELECT @sSQL = 'SELECT '
	WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID >= @counter)
	BEGIN
		SELECT @sSQL = @sSQL + 'Record.' + SystemName + ' AS [' + SystemName + '], ' 
			FROM @tDisplayColumns 
			WHERE ID = @counter
		 --PRINT @sSQL
		SET @counter = @counter + 1
	END 

	--SELECT @sSQL = LEFT(@sSQL, LEN(@sSQL)-1) 
	SELECT @sSQL = @sSQL + ' WarningResults,validationresults FROM [Record] WHERE RecordID = ' + CAST(@nRecordID as varchar)

	-- Red Ticket 3358 - Archive Old Data
	DECLARE @ArchiveDatabase varchar(MAX)
	
	IF @bUseArchiveDatabase = 1 
		BEGIN
			SELECT @ArchiveDatabase = ArchiveDatabase FROM [Table]
			WHERE TableID = @nTableID

			IF @ArchiveDatabase <> ''
			BEGIN
				SET @sSQL = REPLACE(@sSQL, 'FROM [Record]', ' FROM ['+ @ArchiveDatabase +'].[dbo].[Record] ')	
			END
		END
	-- end Red

	--PRINT @sSQL
	EXEC (@sSQL)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Record_Details', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
