﻿

CREATE PROCEDURE [dbo].[CompressFile] 
                @ZipFile   varchar(255), 
                @FileToZip varchar(255) 
AS 

BEGIN TRY
  --author: dee-u of CodeGuru, vbforums
  DECLARE  @hr           INT, 
           @folderObject INT, 
           @shellObject  INT, 
           @src          varchar(255), 
           @desc         varchar(255), 
           @command      varchar(255), 
           @password     varchar(255), 
           @username     varchar(255) 
   
  SET @username = 'username'    
  SET @password = 'password' 
   
  --Create table to save dummy text to create zip file 
  CREATE TABLE ##DummyTable ( [DummyColumn] [varchar](255)) 
   
  --header of a zip file 
  DECLARE  @zipHeader varchar(22)    
  SET @zipHeader = CHAR(80) + CHAR(75) + CHAR(5) + CHAR(6) + REPLICATE(CHAR(0),18) 
   
  --insert zip header 
  INSERT INTO ##DummyTable (DummyColumn) VALUES (@zipHeader) 
   
  --save/create target zip 
  SET @command = 'bcp "..##DummyTable" out "' + @ZipFile + '" -c -U "' + @username + '" -P "' + @password + '"'    
  EXEC MASTER..xp_cmdshell @command 
   
  --Drop used temporary table 
  DROP TABLE ##DummyTable 
   
  --get shell object 
  EXEC @hr = sp_OACreate 
    'Shell.Application' , 
    @shellObject OUT 
   
  IF @hr <> 0 
    BEGIN 
      EXEC sp_OAGetErrorInfo @shellObject , @src OUT , @desc OUT 
       
      SELECT hr = convert(VARBINARY(4),@hr), 
             Source = @src, 
             DESCRIPTION = @desc 
       
      RETURN 
    END 
   
  --get folder 
  SET @command = 'NameSpace("' + @ZipFile + '")'    
  EXEC @hr = sp_OAMethod @shellObject , @command , @folderObject OUT    
  IF @hr <> 0 
    BEGIN 
      EXEC sp_OAGetErrorInfo @shellObject , @src OUT , @desc OUT 
       
      SELECT hr = convert(VARBINARY(4),@hr), 
             Source = @src, 
             DESCRIPTION = @desc 
       
      RETURN 
    END 
   
  --copy file to zip file 
  SET @command = 'CopyHere("' + @FileToZip + '")'    
  EXEC @hr = sp_OAMethod @folderObject , @command 
  IF @hr <> 0 
    BEGIN 
      EXEC sp_OAGetErrorInfo @folderObject , @src OUT , @desc OUT 
       
      SELECT hr = convert(VARBINARY(4),@hr), 
             Source = @src, 
             DESCRIPTION = @desc 
       
      RETURN 
    END 
   
  --Destroy the objects used. 
  EXEC sp_OADestroy @shellObject    
  EXEC sp_OADestroy @folderObject 

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('CompressFile', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
