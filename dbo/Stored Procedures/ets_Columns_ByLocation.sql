﻿

CREATE PROCEDURE [dbo].[ets_Columns_ByLocation]
(
	@nLocationID int 
)
AS
/*
EXEC ets_Columns_ByLocation 94
*/
BEGIN TRY
	SET NOCOUNT ON;

	SELECT (ST.TableName + ': ' +SC.DisplayName) TypeColumn,ST.TableID ,SC.ColumnID  FROM [Column] SC

      JOIN [Table] ST ON ST.TableID = SC.TableID

      JOIN dbo.LocationTable SSST ON SSST.TableID = ST.TableID

      JOIN Location SS ON SSST.LocationID = SS.LocationID

      WHERE SS.LocationID = @nLocationID AND ST.IsActive=1

      AND SC.IsStandard = 0

      ORDER BY ST.TableName + ': ' +SC.DisplayName

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Columns_ByLocation', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
