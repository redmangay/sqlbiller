﻿CREATE PROCEDURE [dbo].[ets_Column_Delete]
(
	@nColumnID int 
)
	/*----------------------------------------------------------------
	EXEC [dbo].[ets_Column_Delete] 50342
	SELECT * FROM [Column] WHERE ColumnID=52221

	MODIFICATION HISTORY
	====================
	26 Sep 2018 JB conflicted with the REFERENCE constraint "FK_Table_Column_GraphDefaultYAxisColumnID"
	31 Jan 2019	JB commented out EXEC (@sSQL) for Ticket 4415 - Deleting a Field Slows TDB down

	---------------------------------------------------------------*/
 
AS

BEGIN TRANSACTION;

BEGIN TRY

	DECLARE @sDisplayName varchar(1000)=NULL

	DECLARE @nTableID int
	DECLARE @sSystemName varchar(10)
	DECLARE @sSQL varchar(100)
	DECLARE @nUserID int
	DECLARE @nAccountID int
	DECLARE @sTableName nvarchar(255)
	DECLARE @dDateUpdated datetime = getdate()

	SELECT  @nColumnID=C.ColumnID,
		@sSystemName=C.SystemName,
		@sDisplayName=C.DisplayName, 
		@nTableID=C.TableID,
		@nUserID = IIF(C.[LastUpdatedUserID] IS NULL,T.[LastUpdatedUserID],C.[LastUpdatedUserID]),
		@nAccountID = T.[AccountID],
		@sTableName = T.TableName
	FROM [Column] C
	JOIN [Table] T ON T.TableID = C.TableID
	WHERE C.ColumnID = @nColumnID

	IF  EXISTS (SELECT ColumnID FROM [Column] WHERE AvgColumnID = @nColumnID )
		UPDATE [Column] SET AvgColumnID =NULL WHERE  AvgColumnID = @nColumnID
	

	
	IF  EXISTS (SELECT ColumnID FROM [Column] WHERE LinkedParentColumnID = @nColumnID )
		UPDATE [Column] SET LinkedParentColumnID =NULL,TableTableID=NULL, ColumnType='text'
			WHERE  LinkedParentColumnID = @nColumnID

	 IF  EXISTS (SELECT ColumnID FROM [Column] WHERE ParentColumnID = @nColumnID )
		UPDATE [Column] SET ParentColumnID =NULL WHERE  ParentColumnID = @nColumnID

	IF  EXISTS (SELECT ColumnID FROM [Column] WHERE DefaultColumnID = @nColumnID )
		UPDATE [Column] SET DefaultColumnID=NULL WHERE DefaultColumnID = @nColumnID

	IF  EXISTS (SELECT ColumnID FROM [Column] WHERE FilterParentColumnID = @nColumnID )
		UPDATE [Column] SET FilterParentColumnID=NULL WHERE FilterParentColumnID = @nColumnID

	IF  EXISTS (SELECT ColumnID FROM [Column] WHERE FilterOtherColumnID = @nColumnID )
		UPDATE [Column] SET FilterOtherColumnID=NULL WHERE FilterOtherColumnID = @nColumnID

	IF  EXISTS (SELECT ColumnID FROM [Column] WHERE MapPinHoverColumnID = @nColumnID )
		UPDATE [Column] SET MapPinHoverColumnID=NULL WHERE MapPinHoverColumnID = @nColumnID

	IF  EXISTS (SELECT ColumnID FROM [Column] WHERE CompareColumnID = @nColumnID )
		UPDATE [Column] SET CompareColumnID=NULL WHERE CompareColumnID = @nColumnID

	IF  EXISTS (SELECT ColumnID FROM [Column] WHERE TrafficLightColumnID = @nColumnID )
		UPDATE [Column] SET TrafficLightColumnID=NULL WHERE TrafficLightColumnID = @nColumnID

	DELETE ScheduledTaskColumn WHERE ColumnID = @nColumnID

	IF  EXISTS (SELECT TableID FROM  [Table] WHERE FilterColumnID = @nColumnID )
		UPDATE [Table] SET FilterColumnID=NULL WHERE FilterColumnID=@nColumnID
	
	IF  EXISTS (SELECT TableID FROM  [Table] WHERE SortColumnID = @nColumnID )
		UPDATE [Table] SET SortColumnID=NULL WHERE SortColumnID=@nColumnID

	IF  EXISTS (SELECT TableID FROM  [Table] WHERE AddUserUserColumnID = @nColumnID )
		UPDATE [Table] SET AddUserUserColumnID=NULL WHERE AddUserUserColumnID=@nColumnID

	IF  EXISTS (SELECT TableID FROM  [Table] WHERE AddUserPasswordColumnID = @nColumnID )
		UPDATE [Table] SET AddUserPasswordColumnID=NULL WHERE AddUserPasswordColumnID=@nColumnID

	IF  EXISTS (SELECT TableID FROM  [Table] WHERE ValidateColumnID1 = @nColumnID )
		UPDATE [Table] SET ValidateColumnID1=NULL WHERE ValidateColumnID1=@nColumnID

	IF  EXISTS (SELECT TableID FROM  [Table] WHERE ValidateColumnID2 = @nColumnID )
		UPDATE [Table] SET ValidateColumnID2=NULL WHERE ValidateColumnID2=@nColumnID

	IF  EXISTS (SELECT TableID FROM  [Table] WHERE GraphXAxisColumnID = @nColumnID )
		UPDATE [Table] SET GraphXAxisColumnID=NULL WHERE GraphXAxisColumnID=@nColumnID

	IF  EXISTS (SELECT TableID FROM  [Table] WHERE GraphSeriesColumnID = @nColumnID )
		UPDATE [Table] SET GraphSeriesColumnID=NULL WHERE GraphSeriesColumnID=@nColumnID

	IF  EXISTS (SELECT TableID FROM  [Table] WHERE DataUpdateUniqueColumnID = @nColumnID )
		UPDATE [Table] SET DataUpdateUniqueColumnID=NULL WHERE DataUpdateUniqueColumnID=@nColumnID

	IF  EXISTS (SELECT TableID FROM  [Table] WHERE UniqueColumnID = @nColumnID )
		UPDATE [Table] SET UniqueColumnID=NULL WHERE UniqueColumnID=@nColumnID

	IF  EXISTS (SELECT TableID FROM  [Table] WHERE UniqueColumnID2 = @nColumnID )
		UPDATE [Table] SET UniqueColumnID2=NULL WHERE UniqueColumnID2=@nColumnID

	IF  EXISTS (SELECT TableID FROM  [Table] WHERE GraphDefaultYAxisColumnID = @nColumnID )
		UPDATE [Table] SET GraphDefaultYAxisColumnID=NULL WHERE GraphDefaultYAxisColumnID=@nColumnID

	UPDATE GraphDefinition SET ColumnID=NULL WHERE ColumnID=@nColumnID
	UPDATE GraphDefinition SET DataColumn1ID=NULL WHERE DataColumn1ID=@nColumnID
	UPDATE GraphDefinition SET DataColumn2ID=NULL WHERE DataColumn2ID=@nColumnID
	UPDATE GraphDefinition SET DataColumn3ID=NULL WHERE DataColumn3ID=@nColumnID
	UPDATE GraphDefinition SET DataColumn4ID=NULL WHERE DataColumn4ID=@nColumnID
	UPDATE [UserRole] SET DataScopeTableID=NULL,DataScopeColumnID=NULL,DataScopeValue=NULL
		WHERE DataScopeColumnID=@nColumnID

	UPDATE TableChild SET ConditionColumnID=NULL,ConditionValue=NULL,ConditionOperator=NULL WHERE ConditionColumnID=@nColumnID

	UPDATE [DetailPhoto] SET ColumnID=NULL WHERE ColumnID=@nColumnID

	DELETE DataReminderUser WHERE ReminderColumnID=@nColumnID
	DELETE DataReminderUser WHERE DataReminderID IN (SELECT DataReminderID FROM DataReminder  WHERE ColumnID=@nColumnID )
	DELETE DataReminder  WHERE ColumnID=@nColumnID 
	DELETE GraphOptionDetail WHERE ColumnID=@nColumnID

	DELETE ViewItem WHERE ColumnID= @nColumnID
	DELETE ExportTemplateItem WHERE ColumnID= @nColumnID

	DELETE ImportTemplateItem WHERE  ColumnID= @nColumnID
	UPDATE ImportTemplateItem SET ParentImportColumnID=NULL WHERE ParentImportColumnID=@nColumnID

	DELETE Conditions WHERE ColumnID=@nColumnID
	DELETE Conditions WHERE ConditionColumnID=@nColumnID

	DELETE [Condition]  WHERE ColumnID=@nColumnID or  CheckColumnID=@nColumnID
	SELECT @sSQL='UPDATE Record SET '+@sSystemName+'=NULL WHERE TableID=' + CAST(@nTableID AS varchar)

	-- EXEC (@sSQL)

	DELETE FROM [Column] WHERE ColumnID = @nColumnID

	SELECT @sDisplayName = 'Deleted Field ' + @sDisplayName + ' from Table ' + @sTableName

	------ Audit Insert
	IF (@nUserID IS NOT NULL)
	BEGIN
		EXECUTE [dbo].[Audit_Insert]
			@sTableName='Column',
			@nTableID=@nTableID,
			@sPrimaryKeyValue=@nColumnID,
			@sFieldName=@sDisplayName,
			@sOldValue=NULL,
			@sNewValue=NULL,
			@dDateAdded=@dDateUpdated,
			@sUserID=@nUserID,
			@nAccountID=@nAccountID,
			@sChangeReason=NULL
	END 

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Column_Delete', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

IF @@TRANCOUNT > 0  
    COMMIT TRANSACTION;  
