﻿

CREATE PROCEDURE [dbo].[ets_GraphDefinition_Select]
(
	@nAccountID int = NULL,
	@sDefinitionName nvarchar(200) = NULL,
	@sDefinition nvarchar(MAX) = NULL,
	@bIsSystem bit = NULL,
	@bIsHidden bit = NULL,
	@nTableID int = NULL,
	@nColumnID int = NULL,
	@sDefinitionKey nvarchar(200) = NULL,
	@bIsActive bit = NULL,
	@nDataColumn1ID int = NULL,
	@nDataColumn2ID int = NULL,
	@nDataColumn3ID int = NULL,
	@nDataColumn4ID int = NULL,

	@sOrder nvarchar(200) = 'GraphDefinitionID', 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647

)
	/*----------------------------------------------------------------
	ets_GraphDefinition_Select
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)

	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND AccountID = ' + CAST(@nAccountID AS nvarchar)
	IF @sDefinitionName IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DefinitionName LIKE ''%' + @sDefinitionName + '%'''
	IF @sDefinition IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Definition = ''' + @sDefinition + ''''
	IF @bIsSystem IS NOT NULL 
		SET @sWhere = @sWhere + ' AND IsSystem = ' + CAST(@bIsSystem AS nvarchar)
	IF @bIsHidden IS NOT NULL
	BEGIN
		IF @bIsHidden = 0
			SET @sWhere = @sWhere + ' AND ((IsHidden IS NULL) OR (IsHidden = 0))'
	END
	ELSE
		SET @sWhere = @sWhere + ' AND ((IsHidden IS NULL) OR (IsHidden = 0))'
	IF @nTableID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND (TableID = ' + CAST(@nTableID AS nvarchar) + ')'
	IF @nColumnID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND (ColumnID = ' + CAST(@nColumnID AS nvarchar) + ')'
	IF @sDefinitionKey IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DefinitionKey = ''' + @sDefinitionKey + ''''
	IF @bIsActive IS NOT NULL 
		SET @sWhere = @sWhere + ' AND [IsActive] = ' + CAST(@bIsActive AS nvarchar)
	IF @nDataColumn1ID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND (DataColumn1ID = ' + CAST(@nDataColumn1ID AS nvarchar) + ')'
	IF @nDataColumn2ID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND (DataColumn2ID = ' + CAST(@nDataColumn2ID AS nvarchar) + ')'
	IF @nDataColumn3ID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND (DataColumn3ID = ' + CAST(@nDataColumn3ID AS nvarchar) + ')'
	IF @nDataColumn4ID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND (DataColumn4ID = ' + CAST(@nDataColumn4ID AS nvarchar) + ')'

	--DECLARE @IsDefaultCase varchar(300) = 
	--	'CAST(' + 
	--		'CASE [Column].[DefaultGraphDefinitionID] ' +
	--			'WHEN NULL THEN 0 ' +
	--			'WHEN [GraphDefinition].[GraphDefinitionID] THEN 1 ' +
	--			'ELSE 0 ' +
	--		'END ' +
	--	'AS bit)'

	SET @sSelect = 'SELECT * FROM 
	(SELECT [GraphDefinition].*, [Table].[TableName] AS TableName, [Column].[GraphLabel] AS GraphLabel,' +
		--' ' + @IsDefaultCase + ' AS IsDefault,' +
		' ROW_NUMBER() OVER(ORDER BY ' + 
		--REPLACE(@sOrder, 'IsDefault', @IsDefaultCase) 
		+ REPLACE(@sOrder, 'IsActive', '[GraphDefinition].IsActive') + ') as RowNum FROM [GraphDefinition]' + 
		' LEFT JOIN [Table] ON [GraphDefinition].TableID = [Table].TableID' +
		' LEFT JOIN [Column] ON [GraphDefinition].ColumnID = [Column].ColumnID' +
		REPLACE(
			REPLACE(
				REPLACE(@sWhere, '[IsActive]', '[GraphDefinition].[IsActive]'),
				'TableID', '[GraphDefinition].[TableID] IS NULL OR [GraphDefinition].[TableID]'),
			'ColumnID', '[GraphDefinition].[ColumnID] IS NULL OR [GraphDefinition].[ColumnID]') + 
		') as UserInfo'

	SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [GraphDefinition] ' +
		REPLACE(REPLACE(@sWhere, 'TableID', '[GraphDefinition].[TableID] IS NULL OR [GraphDefinition].[TableID]'),
			'ColumnID', '[GraphDefinition].[ColumnID] IS NULL OR [GraphDefinition].[ColumnID]')
 
	-- Extend WHERE to include paging:
	SET @sWhere = 
		REPLACE(REPLACE(@sWhere, 'TableID', '[TableID] IS NULL OR [TableID]'),
			'ColumnID', '[ColumnID] IS NULL OR [ColumnID]') + 
		' AND RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	SET @sOrder = ' ORDER BY ' + @sOrder
	SET @sSelect = @sSelect + @sWhere + @sOrder
	PRINT @sSelect
	EXEC (@sSelect)
	SET ROWCOUNT 0

	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_GraphDefinition_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH


