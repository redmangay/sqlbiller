﻿create procedure usr_rptjobperformance
as
----SELECT DISTINCT 
----    Project.[Group/Division] as GroupDivision, TimeSheet.Date as TimeDate, Employee_List.Initials AS EInitials, TimeSheet.Details, 
----    EmployeeHistroy.Name as EName, 
----    case TimeSheet.STATUS when '5' then isnull(Timesheet.[Total],0) else isnull(TimeSheet.[$_Amount],0) end as TimeSheet,
----    isnull(TimeSheet.[$_Billed],0) as BilledDoller,
----   case  TimeSheet.STATUS 
----   when '0' then  'By Rate'
----   when '1' then  'Hold'
----   when '3' then 'Fixed'
----   when '4' then 'No Charge'
----   when '5' then 'Write-off' 
----   when '6' then 'Adjusted'
----   else 'NONE' end as BilledStatus,
----   PNEmployeeIDTimeSheetID.TimeSheetID,
----   BillingInformation.ID AS BillingID,
----   BillingInformation.InvoiceNo, 
----   BillingInformation.InvoiceAmount_Converted as InvoiceAmount, 
----   BillingInformation.Date AS InvoiceDate, BillingInformation.TaxName, 
----   BillingInformation.TaxAmount, BillingInformation.TaxRate,   Project.ProjectNumber as JobNo, Project.ProjectTitle, 
----   case when isnull(Project.CurrencyRate,0)=0 then Project.VALUE
----        when isnumeric(Project.VALUE)=1 then Project.VALUE/Project.CurrencyRate
----        else Project.VALUE end as ProjectValue,
----        case when isnull(Project.CurrencyRate ,0) =0 or Project.CurrencyRate ='' then     '1' else Project.CurrencyRate end as ExchangeRate,
        
        
----    sQuery = sQuery & "iif(TimeSheet.[$_Amount] = 0, 0, (TimeSheet.[$_Billed]/TimeSheet.[$_Amount]) * 100) as PercentBilled, "
----    sQuery = sQuery & "iif(TimeSheet.[$_Amount] = 0, 0, ((TimeSheet.[$_Amount] - TimeSheet.[$_Billed])/TimeSheet.[$_Amount]) * 100) as PercentUnBilled "
    
----    'FROM part of query when options SINGLE JOB or JOBS WITHIN JOB NO RANGE chosen
----    sQryFromPart = "FROM (Employee_List INNER JOIN (Project RIGHT JOIN ((PNEmployeeIDTimeSheetID INNER JOIN "
----    sQryFromPart = sQryFromPart & "TimeSheet ON PNEmployeeIDTimeSheetID.TimeSheetID = TimeSheet.TimeSheetID) INNER JOIN "
----    sQryFromPart = sQryFromPart & "BillingInformation ON (PNEmployeeIDTimeSheetID.ProjectNumber = BillingInformation.ProjectNumber) AND "
----    sQryFromPart = sQryFromPart & "(TimeSheet.InvoiceNo = BillingInformation.InvoiceNo)) ON Project.ProjectNumber = BillingInformation.ProjectNumber) ON "
----    sQryFromPart = sQryFromPart & "Employee_List.EmployeeID = PNEmployeeIDTimeSheetID.EmployeeID) LEFT JOIN EmployeeHistroy ON "
----    sQryFromPart = sQryFromPart & "Project.EmployeeID = EmployeeHistroy.EmployeeID "
    
----    'FROM part of query when option COMPLETED JOBS WITHIN DATE RANGE chosen
----    sQryFromPartCompletedJobs = "FROM (Employee_List INNER JOIN ((Project RIGHT JOIN ((PNEmployeeIDTimeSheetID INNER JOIN "
----    sQryFromPartCompletedJobs = sQryFromPartCompletedJobs & "TimeSheet ON PNEmployeeIDTimeSheetID.TimeSheetID = TimeSheet.TimeSheetID) INNER JOIN "
----    sQryFromPartCompletedJobs = sQryFromPartCompletedJobs & "BillingInformation ON (TimeSheet.InvoiceNo = BillingInformation.InvoiceNo) AND "
----    sQryFromPartCompletedJobs = sQryFromPartCompletedJobs & "(PNEmployeeIDTimeSheetID.ProjectNumber = BillingInformation.ProjectNumber)) ON Project.ProjectNumber = BillingInformation.ProjectNumber) "
----    sQryFromPartCompletedJobs = sQryFromPartCompletedJobs & "LEFT JOIN EmployeeHistroy ON Project.EmployeeID = EmployeeHistroy.EmployeeID) ON Employee_List.EmployeeID = PNEmployeeIDTimeSheetID.EmployeeID) "
----    sQryFromPartCompletedJobs = sQryFromPartCompletedJobs & "LEFT JOIN qryJPCompletedByMaxInvoiceDate ON Project.ProjectNumber = qryJPCompletedByMaxInvoiceDate.ProjectNumber "
    
----    Select Case QueryType
----        Case mdlJobPerformance.OPTION_SELECTED          'just one job number
----            sQuery = sQuery & sQryFromPart & "WHERE "
----            sQuery = sQuery & "TimeSheet.Billed = True AND "
----            sQuery = sQuery & "BillingInformation.ProjectNumber = '" & Forms!frmJobOptions.cboJobNumber & "'"
----        Case mdlJobPerformance.OPTION_RANGE             'range of job numbers
----            sQuery = sQuery & sQryFromPart & strWhereClause
----            sQuery = sQuery & " AND TimeSheet.Billed = True"
----        Case mdlJobPerformance.OPTION_CURRENT           'current within date range
----            sQuery = sQuery & sQryFromPart & strWhereClause
----            sQuery = sQuery & " AND TimeSheet.Billed = True"
----        Case mdlJobPerformance.OPTION_COMPLETE          'completed within date range
----            sQuery = sQuery & sQryFromPartCompletedJobs & strWhereClause
----            sQuery = sQuery & " AND TimeSheet.Billed = True"
----    End Select
    
----    sQuery = sQuery & " AND BillingInformation.Type = 1"
    
----    BilledTime_SQL_Get = sQuery
----End Function
