﻿

CREATE PROCEDURE [dbo].[dbg_ExportTemplateItem_MakeUniqueExportTemplateItemHeading]

	/*----------------------------------------------------------------
	
 SELECT ExportTemplateID,ExportHeaderName , count(*) as qty 
 FROM [ExportTemplateItem] 
 GROUP BY ExportTemplateID,ExportHeaderName HAVING count(*)> 1


--EXEC [dbo].[dbg_ExportTemplateItem_MakeUniqueExportTemplateItemHeading]
	---------------------------------------------------------------*/
 
AS
 
BEGIN TRY  
	DECLARE Cur_UniqueExportTemplateItemHeading CURSOR FOR
	SELECT ExportTemplateID,ExportHeaderName, count(*) as qty 
	FROM [ExportTemplateItem]
	GROUP BY ExportTemplateID,ExportHeaderName HAVING count(*)> 1
 
	DECLARE @nExportTemplateID INT;
	DECLARE @sExportHeaderName varchar(250);

	DECLARE @nQty INT;
	DECLARE @sNewExportHeaderName varchar(250);

	OPEN Cur_UniqueExportTemplateItemHeading;
	FETCH NEXT FROM Cur_UniqueExportTemplateItemHeading INTO @nExportTemplateID,@sExportHeaderName,@nQty;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
		
		  
			DECLARE Cur_ExportTemplateID CURSOR FOR
			SELECT ExportTemplateItemID FROM ExportTemplateItem WHERE ExportTemplateID=@nExportTemplateID AND ExportHeaderName=@sExportHeaderName;
			DECLARE @nExportTemplateItemID INT;
		
			OPEN Cur_ExportTemplateID;
			FETCH NEXT FROM Cur_ExportTemplateID INTO @nExportTemplateItemID;
			WHILE @@FETCH_STATUS = 0
			   BEGIN
			  
				  --SELECT dbo.[fnNextAvailableExportTemplateItemHeading] ('Checkbox1',853,NULL)
				  SELECT @sNewExportHeaderName=dbo.fnNextAvailableExportTemplateItemHeading(@sExportHeaderName,@nExportTemplateID,@nExportTemplateItemID);
			  
				  IF @sExportHeaderName<>@sNewExportHeaderName
					  UPDATE ExportTemplateItem
					  SET ExportHeaderName=@sNewExportHeaderName
					  WHERE ExportTemplateItemID=@nExportTemplateItemID
			 			  
				  FETCH NEXT FROM Cur_ExportTemplateID INTO @nExportTemplateItemID;
			   END;
			CLOSE Cur_ExportTemplateID;
			DEALLOCATE Cur_ExportTemplateID;  
	 
		  FETCH NEXT FROM Cur_UniqueExportTemplateItemHeading INTO @nExportTemplateID,@sExportHeaderName,@nQty;
	   END;
	CLOSE Cur_UniqueExportTemplateItemHeading;
	DEALLOCATE Cur_UniqueExportTemplateItemHeading;
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_ExportTemplateItem_MakeUniqueExportTemplateItemHeading', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
