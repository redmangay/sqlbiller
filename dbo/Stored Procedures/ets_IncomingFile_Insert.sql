﻿

CREATE PROCEDURE [dbo].[ets_IncomingFile_Insert]
(
	@nNewID int output,
	@sLocation nvarchar(250)  = NULL,
	@sFileName nvarchar(250) ,
	@sFiles nvarchar(200) ,
	@dFileDate datetime  = NULL,
	@dDateCreated datetime  = NULL,
	@sBatchIDs nvarchar(200)  = NULL
)

AS
BEGIN TRY
	INSERT INTO [IncomingFile]
	(
		[Location], 
		[FileName], 
		[Files], 
		[FileDate], 
		[DateCreated], 
		[BatchIDs]
	) VALUES (
		@sLocation,
		@sFileName,
		@sFiles,
		@dFileDate,
		@dDateCreated,
		@sBatchIDs
	)
	SELECT @nNewID = @@IDENTITY
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_IncomingFile_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

