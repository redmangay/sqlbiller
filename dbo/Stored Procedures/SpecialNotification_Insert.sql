﻿ 
CREATE PROCEDURE [dbo].[SpecialNotification_Insert]
(
	@nNewID int output,
	@nTableID int,
	@sNotificationHeader varchar(500)  = NULL,
	@sNotificationContent varchar(MAX)  = NULL,
	@sDelivery varchar(MAX)  = NULL
)
	/*----------------------------------------------------------------
	-- Stored Procedure: SpecialNotification_Insert
	-- Author: Red Mangay 
	-- Date: Jul 16, 2019
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	INSERT INTO SpecialNotification
	(
		TableID,
		NotificationHeader, 
		NotificationContent,
		Delivery
	) VALUES (
	     @nTableID,
		@sNotificationHeader,
		@sNotificationContent,
		@sDelivery
	)
	SELECT @nNewID = @@IDENTITY
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('SpecialNotification_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH


