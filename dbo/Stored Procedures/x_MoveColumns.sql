﻿CREATE PROCEDURE x_MoveColumns
(
	 @MainTableID int, 
	 @SubTableID int,
	 @PageTitle varchar(MAX),
	 @Position int
)
AS
BEGIN
	INSERT INTO TableTab (TableID, TabName, DisplayOrder) VALUES (@MainTableID, @PageTitle, @Position)
	DECLARE @n int
	SELECT @n = TableTabID FROM TableTab WHERE TableID = @MainTableID AND DisplayOrder = @Position

	DECLARE @v int
	SELECT @v = CAST(SUBSTRING(MAX(SystemName),2,3) AS int) FROM [Column] WHERE TableID = @MainTableID AND SystemName LIKE 'V%'
	PRINT @v

	UPDATE 	[Column] 
		SET TableID = @MainTableID,
	--SELECT 
		[TableTabID] = @n,
		SystemName = 'V' + RIGHT('000' + CAST((CAST(SUBSTRING(SystemName,2,3) AS int) + @v) AS varchar(3)),3),
		[ViewName] = CAST(@MainTableID AS varchar) + ',' + CAST(@SubTableID AS varchar)  + ',' +  @PageTitle  -- So we can find it later if we need to
	--from [Column] 
		WHERE TableID = @SubTableID AND SystemName LIKE 'V%'
		AND [DisplayName] not in (SELECT [DisplayName] FROM [Column] WHERE TableID = @MainTableID)
END
