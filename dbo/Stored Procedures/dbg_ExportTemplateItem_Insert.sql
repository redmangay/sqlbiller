﻿

CREATE PROCEDURE [dbo].[dbg_ExportTemplateItem_Insert]
(
	@nNewID int output,
	@nExportTemplateID int ,
	@nColumnID int ,
	@sExportHeaderName varchar(250) ,
	@nColumnIndex int,
	@sOption varchar(MAX) = NULL
)
	/*----------------------------------------------------------------
	-- Stored Procedure: dbg_ExportTemplateItem_Insert
	-- Generated by: gpGenerateSP (Jon Bosker, DB Gurus Australia) 
	-- Date: Oct 14 2015  5:14PM
	---------------------------------------------------------------*/
	-- ModIFied by KG 20/7/17 to Replace Square brackets with pharethesis
	-- Red 20112017: Added Option Param
AS

BEGIN TRY 
	SELECT @sExportHeaderName=dbo.fnNextAvailableExportTemplateItemHeading(@sExportHeaderName,@nExportTemplateID,NULL);

	INSERT INTO ExportTemplateItem
	(
		ExportTemplateID, 
		ColumnID, 
		ExportHeaderName, 
		ColumnIndex,
		[Option]
	) VALUES (
		@nExportTemplateID,
		@nColumnID,
		REPLACE(REPLACE(@sExportHeaderName, '[','('), ']',')'),
		@nColumnIndex,
		@sOption 
	)
	SELECT @nNewID = @@IDENTITY
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_ExportTemplateItem_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
