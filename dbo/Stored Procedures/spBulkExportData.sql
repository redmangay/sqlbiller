﻿
----------------------------------------------------------------------
-- Paste your script in below: 
----------------------------------------------------------------------
CREATE	PROCEDURE [dbo].[spBulkExportData]
(
     @TableID		varchar(MAX)
	,@RecordList	varchar(MAX) = NULL
	,@ColumnList	varchar(MAX) = NULL
	,@Debug			INT = 0
	,@ResultType	INT = 0 -- unused can be removed
)

/* REVISION HISTORY
	
	03 Feb 2017: Jarrod made UPDATE on to call ets_Record_List instead of ets_Record_ListJB and made few UPDATE also
	24 Mar 2017: Added in an option for the bulk export to export in dIFferent formats. Its a system option called 'BulkExportMode' the default is 1 for everyone except the Brain who uses a flat table output, option 2.


*/
AS

BEGIN TRY
	DECLARE				@ResultID			INT = 2;		--	Ignore the header row
	DECLARE				@RecordID			INT;
	DECLARE				@ChildTableID		INT; 
	DECLARE				@Error				INT = 0;		--	Error flag
	DECLARE				@ChildColumnName	varchar(MAX); 
	DECLARE				@ChildTableName		varchar(MAX); 
	DECLARE				@sReturnSQL			varchar(MAX); 
	DECLARE				@sReturnHeaderSQL	varchar(MAX);
	DECLARE				@SQL				varchar(MAX);
	DECLARE				@Data				varchar(MAX) = ''; 
	DECLARE				@sTextSearch		varchar(MAX); 
	DECLARE				@FinalData			varchar(MAX) = '';
	DECLARE				@MaxRow				INT = 0;
	DECLARE				@RC					INT
	DECLARE				@sOptionKey			NVARCHAR(200)
	DECLARE				@sOptionValue		NVARCHAR(1000)
	--DECLARE				@ResultType			INT
	
	SET					@sOptionKey = 'BulkExportMode'

	EXECUTE @Error =		[SystemOption_ValueByKey] 
							@sOptionKey
							,@sOptionValue OUTPUT
							
	SET					@ResultType = CAST( @sOptionValue AS INT ) 

	CREATE TABLE		#Child
		(				QueueID							INT IDENTITY(1,1)
						,ChildTableID					INT
						,ChildTableName					varchar(MAX)
						,ChildColumnID					INT
						,ChildColumnName				varchar(MAX)
		);

	CREATE TABLE		#ExportResultParent
		(				ResultID						INT IDENTITY(1,1)
						,IsData							bit
						,DBGSystemRecordID				INT
						,TableID						INT
						,ColumnCount					INT
						,DataDump						varchar(MAX)
		);

	CREATE TABLE		#ExportResultAll
		(				ResultID						INT
						,IsData							bit
						,DBGSystemRecordID				INT
						,TableID						INT
						,ColumnCount					INT
						,RecordID						INT
						,DataDump						varchar(MAX)
						,OrderBy						INT DEFAULT 0
		);
		
	IF					@Debug = 0
		SET				NOCOUNT ON

	/*		Insert the Parent table	*/
	
	IF					@RecordList IS NOT NULL
		SET				@sTextSearch = 'Record.RecordID IN (SELECT Int_Value FROM DBO.fn_ParseText2Table(''' + ISNULL(@RecordList, '' ) + ''','',''))'

	
	IF					@Debug = 1
		PRINT			@sTextSearch

	INSERT				#ExportResultParent
	(					IsData
						,DBGSystemRecordID
						,ColumnCount
						,TableID
						,DataDump
	)
	EXEC				ets_Record_List 
						@nTableID = @TableID
						,@bIsActive = 1
						,@sType = 'exportBULK'
						,@sTextSearch = @sTextSearch
						,@sColumnIDs= @ColumnList;

	/*		List of child tables		*/

	INSERT INTO			#Child
	(					ChildTableID
	)
	VALUEs
	(					@TableID
	);	

	INSERT INTO			#Child
	(					ChildTableID
						,ChildTableName
						,ChildColumnID
						,ChildColumnName
	)
	SELECT				[Table].[TableID] AS											ChildTableID
						,[Table].[TableName] AS											ChildTableName
						,[Column].[ColumnID] AS											ChildColumnID
						,[Column].[SystemName] AS										ChildColumnName
		FROM			[Table]
		JOIN			[Column] ON								[Column].[TableID] = [Table].[TableID]
													--AND			(@ColumnList IS NULL OR [Column].ColumnID IN (SELECT Int_Value FROM DBO.fn_ParseText2Table(@ColumnList,',')) )
		JOIN			[Table] [ParentTable] ON				[ParentTable].[TableID] = [Column].[TableTableID]
		JOIN			[Column] [TableColumn] ON				[TableColumn].[TableID] = [Table].[TableID]
													AND			(@ColumnList IS NULL OR [TableColumn].ColumnID IN (SELECT Int_Value FROM DBO.fn_ParseText2Table(@ColumnList,',')) )
		WHERE			[ParentTable].[TableID] = @TableID
		AND				[Table].[IsActive] = 1
		AND				[ParentTable].[IsActive] = 1
		GROUP BY		[Table].[TableID]
						,[Table].[TableName]
						,[Column].[ColumnID]
						,[Column].[SystemName]
		ORDER BY		[Table].[TableID]  DESC;

	IF					@Debug = 1
		SELECT			*
			FROM		#Child

	/*		Return results for each parent row	*/
	
	IF					@Debug = 1
		SELECT			*
			FROM		#ExportResultParent;
		
	WHILE  EXISTS	(	SELECT			#ExportResultParent.ResultID
							FROM		#ExportResultParent
							WHERE		ResultID = @ResultID
					)	AND	@Error = 0
					--	AND	@ResultID < 3
	BEGIN

		SELECT			@RecordID = #ExportResultParent.DBGSystemRecordID
			FROM		#ExportResultParent
			WHERE		ResultID = @ResultID

		/*	Coallate the results into one table	*/
		
		INSERT INTO		#ExportResultAll
			(			ResultID
						,IsData
						,DBGSystemRecordID
						,ColumnCount
						,TableID
						,RecordID
						,DataDump
						,OrderBy
			)
		SELECT			1
						,IsData
						,DBGSystemRecordID
						,ColumnCount
						,TableID
						,@RecordID
						,DataDump
						,-1
			FROM		#ExportResultParent
			WHERE		DBGSystemRecordID = 0
		
		INSERT INTO		#ExportResultAll
			(			ResultID
						,IsData
						,DBGSystemRecordID
						,ColumnCount
						,TableID
						,RecordID
						,DataDump
						,OrderBy
			)
		SELECT			2
						,IsData
						,DBGSystemRecordID
						,ColumnCount
						,TableID
						,@RecordID
						,DataDump
						,-1
			FROM		#ExportResultParent
			WHERE		DBGSystemRecordID = @RecordID

		/*		Return results for each child table	for the parent row	*/

		DECLARE			@QueueID			INT = 2;		--	Ignore the parent table
		
		WHILE  EXISTS	(	SELECT			#Child.ChildTableID
								FROM		#Child
								WHERE		QueueID = @QueueID
						) --AND	@QueueID <3
		BEGIN

			/*	Get the child table	*/

			SELECT			@ChildTableID = #Child.ChildTableID
							,@ChildColumnName = ChildColumnName
							,@ChildTableName = ChildTableName
				FROM		#Child
				WHERE		QueueID = @QueueID

			CREATE TABLE	#ExportResult
				(			ResultID						INT IDENTITY(1,1)
							,IsData							bit
							,DBGSystemRecordID				INT
							,TableID						INT
							,ColumnCount					INT
							,RecordID						INT
							,DataDump						varchar(MAX)
				);

			SET				@SQL = '
	INSERT			#ExportResult
	(				IsData
					,DBGSystemRecordID
					,ColumnCount
					,TableID
					,DataDump
	)
	EXEC			ets_Record_List 
					@nTableID = ' + CAST( @ChildTableID AS varchar(MAX)) + '
					,@bIsActive = 1
					,@sType=''exportBULK''
					' + CASE WHEN	@ColumnList IS NULL THEN '' ELSE ',@sColumnIDs= ''' + @ColumnList + '''' END +'
					,@sTextSearch=''Record.' + @ChildColumnName + ' like ' + CAST( @RecordID AS varchar(MAX) ) + ''''
					--,@sTextSearch=''Record.' + @ChildColumnName + ' like ''''%' + @ChildColumnName + '%'''''''; @ResultID
		
			BEGIN TRY

				/*	Run the export to get the data for the child table	*/

				EXEC			(@SQL);
			
				IF				@Debug = 1
				BEGIN
					PRINT		@SQL;
					PRINT		'QueueID: ' + CAST(@QueueID AS varchar(MAX))
				END

			END TRY
			BEGIN CATCH

				/*	IF the export fails clean up and break the loop	*/

				PRINT			@SQL;
				DROP TABLE		#ExportResult;

				SET			@Error = @@ERROR

				BREAK;
			END CATCH
	
			SET				@QueueID = @QueueID + 1;
			
			IF				@Debug = 1
				PRINT			'@QueueID : ' + CAST( @QueueID AS varchar(MAX) )

			/*	Coallate the results into one table	*/

			INSERT INTO		#ExportResultAll
				(			ResultID
							,IsData
							,DBGSystemRecordID
							,ColumnCount
							,TableID
							,RecordID
							,DataDump
				)
			SELECT			ResultID
							,IsData
							,DBGSystemRecordID
							,ColumnCount
							,TableID
							,@RecordID
							,DataDump
				FROM		#ExportResult

				
			IF				@Debug = 1
				SELECT			*
					FROM		#ExportResult;

			DROP TABLE		#ExportResult;

		END
		
		IF				@Debug = 1	
			SELECT					*
					FROM			#ExportResultAll

		IF	@ResultType = 0
		BEGIN
			/*	Create a result list	*/
			
			SELECT				@FinalData = @FinalData + CHAR(13) + CHAR(10) + #ExportResultAll.DataDump
				FROM			#ExportResultAll
				WHERE			RecordID = @RecordID
				ORDER BY		#ExportResultAll.OrderBy
								,#ExportResultAll.TableID
								,#ExportResultAll.ResultID;
		END
		
		DECLARE				@Index		INT	= 1;

		CREATE TABLE		#Matrix
		(					ResultID		INT
							,TableID		INT
							,DataDump		varchar(MAX)
							,OrderBy		INT
		);

		IF	@ResultType = 1
		BEGIN
			/*	Group the results by table horizontally	*/
			
			/*	Get the maximum rows	*/

			SELECT				@MaxRow = MAX(#ExportResultAll.ResultID)
				FROM			#ExportResultAll
				WHERE			#ExportResultAll.RecordID = @RecordID;
			
			/*	Create result matrix	*/

			SET					@Index = 1

			WHILE				@Index <= @MaxRow
			BEGIN
			
				INSERT INTO			#Matrix
				(					ResultID
									,TableID
									,DataDump
									,OrderBy
				)
				SELECT				@Index AS			ResultID
									,#ExportResultAll.TableID
									,SUBSTRING( REPLICATE('"",', #ExportResultAll.ColumnCount), 1, #ExportResultAll.ColumnCount * 3 - 1)  AS		DataDump
									,OrderBy
					FROM			#ExportResultAll
					WHERE			#ExportResultAll.RecordID = @RecordID
					AND				#ExportResultAll.ResultID = 1
					ORDER BY		#ExportResultAll.OrderBy
									,#ExportResultAll.TableID
									,#ExportResultAll.ResultID;

				SET				@Index = @Index + 1;
			END
			
			IF				@Debug = 1
				SELECT				*
					FROM			#Matrix
					ORDER BY		OrderBy
									,TableID
									,ResultID;
			
			IF				@Debug = 1
				PRINT		@MaxRow

			/*	Show the header row for the first row only	*/

			IF					LEN( @FinalData ) > 0
				SET				@Index = 2;
			ELSE
				SET				@Index = 1;

			/*	Build the result data	*/
			
			WHILE				@Index <= @MaxRow
			BEGIN
			
				SET					@Data = '';

				SELECT				TOP 10000 @Data = @Data + ',' + ISNULL( #ExportResultAll.DataDump, #Matrix.DataDump )
					FROM			#Matrix
					LEFT JOIN		#ExportResultAll ON					#ExportResultAll.ResultID = #Matrix.ResultID
															AND			#ExportResultAll.TableID = #Matrix.TableID
															AND			#ExportResultAll.RecordID = @RecordID
					WHERE			#Matrix.ResultID = @Index
					ORDER BY		#Matrix.OrderBy
									,#Matrix.TableID
									,#Matrix.ResultID;
									
				IF					@Debug = 1
					PRINT				'@Index: ' + CAST( @Index AS varchar(MAX) )
				IF					@Debug = 1
					PRINT				'@Data: ' + @Data

				SET					@FinalData =  @FinalData + CASE WHEN @FinalData <> '' THEN CHAR(13) + CHAR(10)  ELSE '' END + SUBSTRING( @Data, 2, LEN( @Data ) );
			
				IF					@Debug = 1
					PRINT			'@FinalData: ' + @FinalData

				SET					@Index = @Index + 1;
			END

			--DROP TABLE			#Matrix;

		END

		
		DROP TABLE			#Matrix;

		SET				@ResultID = @ResultID + 1;
	END

	--SELECT				*
	--	FROM			#ExportResultAll
	--	ORDER BY		#ExportResultAll.RecordID
	--					,#ExportResultAll.OrderBy
	--					,#ExportResultAll.TableID
	--					,#ExportResultAll.ResultID;
	IF	@ResultType = 2
		BEGIN
			/*	Flatten the table out into one row per parent row	*/

			CREATE	TABLE		#Output
				(				OutputID				INT	IDENTITY(1,1)
								,ParentRecordID			INT
								,TableID				INT
								,ColumnCount			INT
								,RowsPerParent			INT
								,MaxRowsAcrossParents	INT
								,DataDump				varchar(MAX)
				)

			/*	Setup the output data -- Set the number of rows per patient	*/
			
			INSERT INTO			#Output
				(				ParentRecordID
								,TableID
								,RowsPerParent
				)
				SELECT			#ExportResultAll.RecordID
								,#ExportResultAll.TableID
								,COUNT(#ExportResultAll.ResultID) AS		RowsPerParent
				FROM			#ExportResultAll
				WHERE			#ExportResultAll.IsData = 1
				GROUP BY		#ExportResultAll.RecordID
								,#ExportResultAll.TableID
				ORDER BY		#ExportResultAll.RecordID
								,#ExportResultAll.TableID;
			
			INSERT INTO			#Output
				(				ParentRecordID
								,TableID
								,RowsPerParent
				)
				SELECT			ParentRecordID.ParentRecordID
								,TableID.TableID
								,0 AS		RowsPerParent
				FROM			(	SELECT				ParentRecordID
										FROM			#Output
										GROUP BY		ParentRecordID
								) ParentRecordID
				CROSS JOIN		(	SELECT				TableID
										FROM			#Output
										GROUP BY		TableID
								) TableID
				WHERE NOT EXISTS(	SELECT		#Output.OutputID
									FROM		#Output
									WHERE		#Output.ParentRecordID = ParentRecordID.ParentRecordID
									AND			#Output.TableID = TableID.TableID
								);

			/*	Maximum number of rows in a table	*/

			UPDATE				#Output
				SET				MaxRowsAcrossParents = RowsPerParent.RowsPerParent
				FROM			#Output
				JOIN			(	SELECT			TableID
													,MAX(RowsPerParent) AS			RowsPerParent
									FROM			#Output
									GROUP BY		#Output.TableID
								)	RowsPerParent ON		RowsPerParent.TableID = #Output.TableID;

			/*	Set column count	*/

			UPDATE				#Output
				SET				ColumnCount = #ExportResultAll.ColumnCount
				FROM			#Output
				JOIN			#ExportResultAll ON					#ExportResultAll.RecordID = #Output.ParentRecordID
														AND			#ExportResultAll.TableID = #Output.TableID
				WHERE			#ExportResultAll.IsData = 0;

			/*	Set the header	*/

			SET					@FinalData = ''

			SELECT				@FinalData = @FinalData + REPLICATE(#ExportResultAll.DataDump + ',', #Output.MaxRowsAcrossParents)
				FROM			#ExportResultAll
				JOIN			#Output ON							#Output.TableID = #ExportResultAll.TableID
				WHERE			#ExportResultAll.IsData = 0
				GROUP BY		#ExportResultAll.TableID
								,#Output.MaxRowsAcrossParents
								,#ExportResultAll.DataDump
				ORDER BY		#ExportResultAll.TableID;

			/*	Set the data	*/
			
			SET					@SQL = ''

			SELECT				@SQL = ISNULL( @SQL, '' ) + '
			UPDATE				#Output
				SET				DataDump = ISNULL( #Output.DataDump, '''' ) + '','' + #ExportResultAll.DataDump
				FROM			#Output
				JOIN			#ExportResultAll ON			#ExportResultAll.RecordID = #Output.ParentRecordID
													AND		#ExportResultAll.TableID = #Output.TableID									
				WHERE			#ExportResultAll.IsData = 1
				AND				#ExportResultAll.DBGSystemRecordID = ' + CAST( #ExportResultAll.DBGSystemRecordID AS varchar(MAX) ) + ';
			'
				FROM			#ExportResultAll
				WHERE			#ExportResultAll.IsData = 1
				ORDER BY		#ExportResultAll.TableID;
			
			--PRINT				@SQL;
			EXEC				(@SQL);
							
			UPDATE				#Output
				SET				DataDump = ISNULL( #Output.DataDump, '' ) + REPLICATE(REPLICATE(',', #Output.ColumnCount), #Output.MaxRowsAcrossParents - #Output.RowsPerParent)
				FROM			#Output;
			
			SET		@ResultID = 2

			WHILE  EXISTS	(	SELECT			#ExportResultParent.ResultID
									FROM		#ExportResultParent
									WHERE		ResultID = @ResultID
							)	AND	@Error = 0
				--	AND	@ResultID < 3
			BEGIN

				SELECT			@RecordID = #ExportResultParent.DBGSystemRecordID
					FROM		#ExportResultParent
					WHERE		ResultID = @ResultID

				SET					@FinalData = @FinalData + '
'

				SELECT				@FinalData = @FinalData + SUBSTRING(#Output.DataDump + ',', 2, LEN(#Output.DataDump) )
					FROM			#Output
					WHERE			#Output.ParentRecordID = @RecordID
					ORDER BY		#Output.TableID;

				SET				@ResultID = @ResultID + 1
			END
			
			DROP TABLE			#Output;

		END
	
	SELECT				@FinalData AS FlatData;
	
	PRINT				'Final: 
' + @FinalData;
	
	/*		Clean up	*/
	DROP TABLE			#Child;
	DROP TABLE			#ExportResultAll;

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spBulkExportData', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
