﻿
CREATE PROCEDURE [dbo].[ets_GraphOption_Select]
(
	@nAccountID int  = NULL,
	@nTableID int  = NULL,
	@nGraphPanel int  = NULL,
	@bReportChart bit=0,
	@bIsActive bit  = NULL,
	@sOrder nvarchar(200) = GraphOptionID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647,
	@nUserID int = NULL
)
	/*----------------------------------------------------------------
	ets_GraphOption_Select
	---------------------------------------------------------------*/
 
AS
BEGIN TRY
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows

	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	DECLARE @sXWhere nvarchar(MAX)

	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nAccountID IS NOT NULL
	BEGIN
		SET @sWhere = @sWhere + ' AND AccountID = '+CAST(@nAccountID AS NVARCHAR)
		SET @sXWhere = @sXWhere + ' AND AccountID = '+CAST(@nAccountID AS NVARCHAR)
	END
	IF @nTableID IS NOT NULL
	BEGIN 
		SET @sWhere = @sWhere + ' AND EXISTS (SELECT GraphOptionDetailID FROM [GraphOptionDetail] 
		WHERE [GraphOptionDetail].[GraphOptionID] = [GraphOption].[GraphOptionID] 
		AND ([TrueTableID] = '+CAST(@nTableID AS nvarchar) + ' OR ([TrueTableID] IS NULL AND TableID = '+CAST(@nTableID AS nvarchar) + ')))'

		SET @sXWhere = @sXWhere + ' AND EXISTS (SELECT GraphOptionDetailID FROM [GraphOptionDetail] 
		WHERE [GraphOptionDetail].[GraphOptionID] = [GraphOption].[GraphOptionID] 
		AND ([TrueTableID] = '+CAST(@nTableID AS nvarchar) + ' OR ([TrueTableID] IS NULL AND TableID = '+CAST(@nTableID AS nvarchar) + ')))'
	END
	IF @bReportChart IS NOT NULL
	BEGIN
		SET @sWhere = @sWhere + ' AND ReportChart = '+CAST(@bReportChart AS NVARCHAR)
		SET @sXWhere = @sXWhere + ' AND ReportChart = '+CAST(@bReportChart AS NVARCHAR)
	END
	IF @bIsActive IS NOT NULL
	BEGIN 
		SET @sWhere = @sWhere + ' AND IsActive = '+CAST(@bIsActive AS NVARCHAR)
		SET @sXWhere = @sXWhere + ' AND IsActive = '+CAST(@bIsActive AS NVARCHAR)
	END
	IF @nGraphPanel IS NOT NULL
	BEGIN 
		SET @sWhere = @sWhere + ' AND GraphPanel = '+CAST(@nGraphPanel AS NVARCHAR)
		SET @sXWhere = @sXWhere + ' AND GraphPanel = '+CAST(@nGraphPanel AS NVARCHAR)
	END
	IF @nUserID IS NOT NULL
	BEGIN
		SET @sWhere = @sWhere + ' AND VisibleToUser = '+CAST(@nUserID AS NVARCHAR)
	END

	SET @sWhere = @sWhere + ' OR (VisibleToUser IS NULL ' +  @sXWhere + ')'

	SET @sSelect = 'SELECT * FROM 
	(SELECT [GraphOption].*, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum FROM [GraphOption]' + @sWhere + ') as UserInfo'
	SET @sSelectCount = 'SELECT COUNT(GraphOptionID) AS TotalRows FROM [GraphOption]' + @sWhere 
 
	-- Extend WHERE to include paging: -- AA 27/07/2017 we don't need to exted WHERE - data is filtered already in inner query
	SET @sWhere = ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	SET @sOrder = ' ORDER BY ' + @sOrder
	SET @sSelect = @sSelect + @sWhere + @sOrder
 
	PRINT @sSelect
	EXEC (@sSelect)

	SET ROWCOUNT 0
	--PRINT @sSelectCount
	EXEC (@sSelectCount)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_GraphOption_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

-----------------------------------------------------------------------

