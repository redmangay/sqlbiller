﻿CREATE PROCEDURE ets_Records_By_BatchForEmail
(
	
	@nBatchID int,
	@nTableID int
)
/*
Modification History
--------------------
18 Jun 2019	JB	Added a fix for [square brackets] in the DisplayText see https://stackoverflow.com/questions/37756842/how-to-query-a-table-with-square-brackets-in-it
*/
AS

BEGIN TRY
		SET NOCOUNT ON;

		DECLARE @tDisplayColumns TABLE
		(
			ID int identity(1,1),
			ColumnID int,
			SystemName varchar(50),
			DisplayText varchar(100)
		)


		DECLARE @nColumnIDRecordID INT

		SELECT @nColumnIDRecordID=ColumnID FROM [Column] 
		WHERE TableID = @nTableID AND SystemName='RecordID'

			INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
				SELECT ColumnID, DisplayName, SystemName
					FROM [Column] 
					WHERE TableID = @nTableID 
					ORDER BY DisplayOrder


		INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
		VALUES (@nColumnIDRecordID,'DBGSystemRecordID','RecordID')


		DECLARE @sSQL varchar(MAX)
	
		DECLARE @counter int
		SET @counter = 1
		

  
	
	  SELECT @sSQL = ' SELECT '

   
		SET @counter = 1
		WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID >= @counter)
		BEGIN
			--SELECT @sSQL = @sSQL + 'Record.' + SystemName + ' AS [' + DisplayText + '], ' 
			-- JB added this fix to a problem caused by having square brackets in the DisplayText:
			SELECT @sSQL = @sSQL + 'Record.' + SystemName + ' AS [' + REPLACE(DisplayText,']',']]') + '], ' 
				FROM @tDisplayColumns 
				WHERE ID = @counter
			--PRINT @sSQL
			SET @counter = @counter + 1
		
		END 

		SELECT @sSQL = LEFT(@sSQL, LEN(@sSQL)-1) 
	
		SELECT @sSQL = @sSQL + ' ,Record.DateTimeRecorded,Record.WarningResults,Record.ValidationResults FROM [Record] '


	
		-- ADD IN THE WHERE CRITERIA
		SELECT @sSQL = @sSQL + ' WHERE BatchID  = ' + CAST(@nBatchID as varchar) + ' ORDER BY Record.DateTimeRecorded'
	       
    
    
		PRINT @sSQL
		EXEC (@sSQL)
		-- PRINT @sTempTable
	
		SET ROWCOUNT 0
 
   

END TRY
BEGIN CATCH
	DECLARE @ErrorPath varchar(1000) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Records_By_BatchForEmail', ERROR_MESSAGE(), ISNULL(@sSQL,'NULL'), GETDATE(), @ErrorPath)
END CATCH

