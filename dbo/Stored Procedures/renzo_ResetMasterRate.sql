﻿
-- =============================================
-- Author:		<manoj>
-- Create date: <17 May 2016>
-- Description:	<used to refersh child table from master table>
-- =============================================
CREATE procedure  [dbo].[renzo_ResetMasterRate]
(
	@RecordID  int,
	@FormSetID int = null,
    @UserID int=null,
	@jobnumber varchar(max),
    @Return varchar(max) output
)
as

declare @designationid int,
        @rate varchar(500),
		@designation_org varchar(max),
		@denteredby varchar(max),
        @disactive varchar(max),
		@item varchar(max),
		@unit varchar(max),
		@type varchar(max),
	   @disenteredby varchar(max),
	   @disisactive varchar(max)

--#region copying value from MAster designation table  2712

 select  @designationid= V003          --designation id
       ,@rate=V002                     -- rate ($hr)
	   ,@designation_org=V001          -- designation_org
	   ,@denteredby =EnteredBy
	   ,@disactive =isactive
	    from record where tableid ='2712' and isnumeric(v003) =1  and RecordID =@RecordID
--#endregion copying value from MAster designation table  2712


--#region copying value form = 2713 [Master Disbursement Rate)

select @item= V001              -- item
      ,@unit=V002              --$/unit
	  ,@type=V003   --Type
	  , @disenteredby  =EnteredBy
	   ,@disisactive =isactive           
from record where tableid ='2713' and RecordID =@RecordID
--#endregion  copying value form = 2713 [Master Disbursement Rate)



--#region deleting from  Designation Rate and disbursment rate against job number
delete from  record where tableid ='2477' and v003 =@jobnumber and @jobnumber is not null

delete from  record where tableid ='2478' and V004 =@jobnumber and @jobnumber is not null   --disbursement



--inserting default value to child tables  designation
  insert into  record  
      ( 	  
	  tableid,
	  datetimerecorded,
	  dateadded,
	  dateupdated,
	  EnteredBy, 
	  isactive ,
	  V003 -- job number
      ,v005 --designation
	  ,V001 --designation_orig
	  ,V002 --rate
	   ,V004  --issepecial
	  ) 
	   values
	   (
	   '2477',
	   getdate(),
	   getdate(),
	   getdate(),
	   @denteredby,
	   @disactive,
	    null      --jobnumber
	    ,@designationid
	   ,@designation_org
       ,@rate              
	   , null  -- issepecial
	   )




--insert into disbursement
insert into record
(

       tableid,
	  datetimerecorded,
	  dateadded,
	  dateupdated,
	  EnteredBy, 
	  isactive ,
        V004 --jobnumber     
       , V001 --item    
       ,v002 --$/Unit
       ,v003 --Type 
       ,V005 --isspecial 
)

values
(
       '2478',
	   getdate(),
	   getdate(),
	   getdate(),
	   @disenteredby,
	   @disisactive
      ,null --jobnumber
     , @item    -- item
      ,@unit   --$/unit
	  ,@type  --type
	  ,null   --isspecial              


)


 
      

	  









