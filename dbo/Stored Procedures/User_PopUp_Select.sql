﻿

CREATE PROCEDURE [dbo].[User_PopUp_Select]
(
	@sSeacrh nvarchar(50)  = NULL,	
	@nAccountID int  = NULL,
	@sOrder nvarchar(200) = FirstName, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
 
AS

BEGIN TRY
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause EXEC User_PopUp_Select @nAccountID=13,@sSeacrh='mohsin'
	SET @sWhere = ' WHERE 1=1'

	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ur.AccountID = '+CAST(@nAccountID AS NVARCHAR)
	
	IF @sSeacrh IS NOT NULL 
		SET @sWhere = @sWhere + ' AND (FirstName LIKE '+'''%' + @sSeacrh + '%'' OR 
		LastName LIKE '+'''%' + @sSeacrh + '%'' OR Email LIKE '+'''%' + @sSeacrh + '%'')'


	SET @sSelect = 'SELECT * FROM 
		(SELECT u.UserID,  FirstName, LastName,PhoneNumber,Email,
		IsActive, u.DateAdded,u.DateUpdated, ur.AccountID,IsAccountHolder, IsAdvancedSecurity,
		ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum 
		FROM [User] u INNER JOIN UserRole ur ON
		 u.UserID=ur.UserID   ' + @sWhere + ') as UserInfo'
		 + ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
 
 
	SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [User] u INNER JOIN UserRole ur ON
		u.UserID=ur.UserID    ' + @sWhere 
 
	PRINT  @sSelect
	EXEC (@sSelect)
	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0')  
	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('User_PopUp_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
