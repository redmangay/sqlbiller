﻿




CREATE PROCEDURE [dbo].[ets_Record_List_BCP]
(
	@sSQL varchar(MAX),
	@sHeaderSQL varchar(MAX),
	@sFileName nvarchar(1000) 
)

/*
EXEC [ets_Record_List_BCP] '[Account24768].[vSites]',@sFileName='E:\_delete\BCP\a1212BSFLog'
*/
AS

BEGIN TRY


    DECLARE @cmd varchar(8000) 
	 DECLARE @DBName varchar(50)
	 SELECT @DBName='[' + DB_NAME()  + ']'
    SET ROWCOUNT 2147483647

	--PRINT @sSQL
	--SET @sSQL=@sColumns + @sSQL

	SET @sSQL = REPLACE(@sSQL, 'SET DATEFORMAT dmy;', '')
	SET @sSQL='SET DATEFORMAT dmy; ' + @sHeaderSQL + ' UNION ' + @sSQL

	SET @cmd='bcp "'+ @sSQL +'" queryout ' + @sFileName + '.csv -c -t"," -S' + @@ServerName + ' -T' + ' -d'+DB_NAME()

	--PRINT @cmd
	EXEC master.dbo.xp_cmdshell  @cmd
	   
	
	SET ROWCOUNT 0
 

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Record_List_BCP', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
