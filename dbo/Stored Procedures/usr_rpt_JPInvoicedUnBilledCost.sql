﻿

-- Author:		<Manoj sharma>
-- Create date: <13 March 2016>
-- Description:	<Used as ssrs report and in procedure rptjobperformance>
create procedure [dbo].[usr_rpt_JPInvoicedUnBilledCost]
as



BEGIN
     
	 IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##JPInvoicedUnBilledCost' )) DROP TABLE ##JPInvoicedUnBilledCost
	 create table ##JPInvoicedUnBilledCost  (
GroupDivision varchar(max),
TimeDate varchar(max),
EInitials varchar(max),
Details varchar(max),
EName varchar(max),
TimeSheet money not null,
BilledDoller money not null,
BilledStatus varchar(9) not null,
CostID varchar(max),
ProjectTitle varchar(max),
JobNo varchar(max),
ProjectValue money


 ) 





 insert into ##JPInvoicedUnBilledCost

 	SELECT DISTINCT Project.[Group/Division] AS GroupDivision
		, TimeSheet_Cost.Date AS TimeDate
		, Employee_List.Initials AS EInitials
		, TimeSheet_Cost.Details, EmployeeHistroy.Name AS EName
		, CASE WHEN TimeSheet_Cost.[Status]='5' THEN ISNULL((TimeSheet_Cost.Dollar_Amount ),0) ELSE ISNULL((TimeSheet_Cost.[$_Amount] ),0) END AS TimeSheet
		, ISNULL((TimeSheet_Cost.[$_Amount] ),0) AS BilledDoller
		, CASE WHEN TimeSheet_Cost.STATUS='0' THEN 'By Rate' ELSE
			CASE WHEN TimeSheet_Cost.STATUS='1' THEN 'Hold' ELSE
				CASE WHEN TimeSheet_Cost.STATUS='3' THEN 'Fixed' ELSE
					CASE WHEN TimeSheet_Cost.STATUS='4' THEN 'No Charge' ELSE
						CASE WHEN TimeSheet_Cost.STATUS='5' THEN 'Write-off' ELSE
							CASE WHEN TimeSheet_Cost.STATUS='6' THEN 'Adjusted' ELSE
								'NONE'
							END
						END
					END
				END
			END
			END AS BilledStatus
		, PNEmployeeIDCostID.CostID
		, Project.ProjectTitle
		, Project.ProjectNumber AS JobNo
		, (ISNULL(Project.Value,'0')) AS ProjectValue
	FROM (Account24918.vEmployee_List Employee_List INNER JOIN  Account24918.vEmployeeHistroy EmployeeHistroy ON Employee_List.EmployeeID = EmployeeHistroy.EmployeeID)
		INNER JOIN (Account24918.vTimeSheet_Cost TimeSheet_Cost INNER JOIN (Account24918.vPNEmployeeIDCostID PNEmployeeIDCostID INNER JOIN Account24918.vProject Project ON PNEmployeeIDCostID.ProjectNumber = Project.ProjectNumber) ON TimeSheet_Cost.CostID = PNEmployeeIDCostID.CostID) ON Employee_List.EmployeeID = PNEmployeeIDCostID.EmployeeID
	WHERE (TimeSheet_Cost.Billed='0')
  
   RETURN;
END;

