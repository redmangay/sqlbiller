﻿

CREATE PROCEDURE [dbo].[ets_ExportRelatedData]
(
	@ParentTableTableID INT
)

/*

	EXEC [ets_ExportRelatedData] @ParentTableTableID = 2454	
	EXEC [ets_ExportRelatedData] @ParentTableTableID = 2428
	--SET				@ParentTableTableID = 2428

	EXEC			[ets_Record_List] 
					@nTableID = 2454
					,@sType='exportBULK'

*/
AS

BEGIN TRY
	SET NOCOUNT ON;
	SET CONCAT_NULL_YIELDS_NULL OFF;

	DECLARE			@SQL							varchar(MAX)
					,@Data							varchar(MAX)
					,@Index							INT
					,@ChildTableID					INT
					,@ChildIndex					INT
					,@SQLParent						varchar(MAX)
					,@ParentRecordID				INT

	--		Table to store Parent Child data from the spGetParentChildSQL stored procedure
				
	CREATE TABLE	#ParentChildSQL
	(				ParentTableID					INT
					,ChildTableID					INT
					,ParentColumnID					INT
					,ChildColumnID					INT
					,ParentCol						varchar(MAX)
					,ChildCol						varchar(MAX)
					,FromClause						varchar(MAX)
					,WhereClause					varchar(MAX)
	)

	--		Table to coallate all the results from all the child tables

	CREATE TABLE	#Result
	(				ResultID						INT IDENTITY(1,1)
					,ParentRecordID					INT
					,ChildRecordID					INT
					,ChildTableID					INT
					,ResultData						varchar(MAX)
					,ResultSequenceID				INT
	)

	--		Table to hold the final output

	CREATE TABLE	#ResultTranspose
	(				ResultTransposeID				INT IDENTITY(1,1)
					,ColumnData						varchar(MAX)
	)

	--		Table to hold the parent table data

	CREATE TABLE	#ParentExportResult
	(				ResultID						INT IDENTITY(1,1)
					,IsData							bit
					,DBGSystemRecordID				INT
					,TableID						INT
					,ColumnCount					INT
					,DataDump						varchar(MAX)
	)


	--		Get child table relationship list

	SELECT			ROW_NUMBER ( ) OVER ( ORDER BY [Table].[TableID] DESC ) AS		ChildRelationshipID
					,[Column].[ColumnID] AS											ChildColumnID
					,[Column].[SystemName] AS										ChildColumnName
					,[Table].[TableID] AS											ChildTableID
					,[Column].[TableTableID] AS										ParentTableID
					,[Column].[LinkedParentColumnID] AS								ParentColumnID
					,0 AS															MaxPatient
					,0 AS															ColumnCount
	INTO			#ChildRelationship
	FROM			[Table]
	JOIN			[Column] ON								[Column].[TableID] = [Table].[TableID]
	JOIN			[Table] [ParentTable] ON				[ParentTable].[TableID] = [Column].[TableTableID]
	WHERE			[ParentTable].[TableID] = @ParentTableTableID
	AND				[Table].[IsActive] = 1
	AND				[ParentTable].[IsActive] = 1
	GROUP BY		[Column].[ColumnID]
					,[Column].[SystemName]
					,[Table].[TableID]
					,[Column].[TableTableID]
					,[Column].[LinkedParentColumnID]
	ORDER BY		[Table].[TableID]  DESC

	--		Get the Parent data 

	INSERT			#ParentExportResult
	(				IsData
					,DBGSystemRecordID
					,ColumnCount
					,TableID
					,DataDump
	)

	EXEC			[ets_Record_List] 
					@nTableID = @ParentTableTableID
					,@sType='exportBULK'

	--		Add a header row

	INSERT			#ResultTranspose
	(				ColumnData
	)
	SELECT			#ParentExportResult.DataDump
	FROM			#ParentExportResult
	WHERE			#ParentExportResult.IsData = 0

	--		For each table create the header, fill the data into the list table and build the blank dummy row

	SET				@Index = 1

	WHILE  EXISTS	(	SELECT			#ChildRelationship.ChildTableID
						FROM			#ChildRelationship
						WHERE			ChildRelationshipID = @Index
					)
	BEGIN
	
		--		Get the next child table

		SELECT			@ChildTableID = #ChildRelationship.ChildTableID
		FROM			#ChildRelationship
		WHERE			ChildRelationshipID = @Index

		--		Get the SQL used to join the parent table with the child table

		INSERT			#ParentChildSQL			
		(				ParentTableID
						,ChildTableID
						,ParentColumnID
						,ChildColumnID
						,ParentCol
						,ChildCol
						,FromClause
						,WhereClause
		)				
		EXEC			spGetParentChildSQL @nParentTableID = @ParentTableTableID, @nChildTableID = @ChildTableID

		--		Get the data from the child table

		CREATE TABLE	#ExportResult
		(				ResultID						INT IDENTITY(1,1)
						,IsData							bit
						,DBGSystemRecordID				INT
						,TableID						INT
						,ColumnCount					INT
						,DataDump						varchar(MAX)
		)

		SET				@SQL = '
	INSERT			#ExportResult
	(				IsData
					,DBGSystemRecordID
					,ColumnCount
					,TableID
					,DataDump
	)

	EXEC			[ets_Record_List] 
					@nTableID = ' + CAST( @ChildTableID AS varchar(MAX)) + '
					,@sType=''exportBULK''
	'
		BEGIN TRY
			EXEC		(@SQL)
		END TRY
		BEGIN CATCH
			DROP TABLE		#ExportResult
			BREAK
		END CATCH

		--		Move the data into the Result table to be processed later

		SELECT			@SQL = '
	INSERT INTO	#Result
	(			ParentRecordID
				,ChildRecordID
				,ChildTableID
				,ResultData
				,ResultSequenceID
	)

	SELECT	#ParentExportResult.DBGSystemRecordID
	,#ExportResult.DBGSystemRecordID
	,#ExportResult.TableID
	,#ExportResult.DataDump
	,ROW_NUMBER ( ) OVER ( PARTITION BY #ParentExportResult.DBGSystemRecordID ORDER BY		#ParentExportResult.DBGSystemRecordID ) AS		ResultSequenceID
	' + FromClause + '
	JOIN		#ExportResult ON			#ExportResult.DBGSystemRecordID = Child.RecordID
	JOIN		#ParentExportResult ON		#ParentExportResult.DBGSystemRecordID = Parent.RecordID
	' + WhereClause + '
	AND			#ExportResult.IsData = 1
	ORDER BY		#ParentExportResult.DBGSystemRecordID
	'
		FROM			#ParentChildSQL
		WHERE			ChildTableID = @ChildTableID

		--		Set the blank rows used to pad the output when there is no real data

		UPDATE			#ChildRelationship
		SET				ColumnCount = #ExportResult.ColumnCount
		FROM			#ChildRelationship
		JOIN			#ExportResult ON				#ExportResult.TableID = #ChildRelationship.ChildTableID

		--		Set the header
		--Test

		DECLARE		@TableName		varchar(MAX)

		SELECT		@TableName = [TableName]
		FROM		[Table]
		WHERE		TableID = @ChildTableID
		--
		UPDATE			#ResultTranspose
		SET				ColumnData = ColumnData + ', !!!' + @TableName + '!! ' + #ExportResult.DataDump
		FROM			#ExportResult
		WHERE			#ExportResult.IsData = 0

		BEGIN TRY
			EXEC		(@SQL)
		END TRY
		BEGIN CATCH
			DROP TABLE		#ExportResult
			BREAK
		END CATCH

		DROP TABLE		#ExportResult

		SET				@Index = @Index + 1
	END

	--		Transpose the data
	--		For each Parent row get the parent row data
	--		For each result sequence add the child rows
	--		Start at 2nd row becuase the header is on the first row and its already been processed

	SET				@Index = 2

	WHILE  EXISTS	(	SELECT			#ParentExportResult.ResultID
						FROM			#ParentExportResult
						WHERE			#ParentExportResult.ResultID = @Index
					)
	BEGIN

		SELECT			@SQLParent =  #ParentExportResult.DataDump + ','
						,@ParentRecordID = #ParentExportResult.DBGSystemRecordID
		FROM			#ParentExportResult
		WHERE			#ParentExportResult.ResultID = @Index

		--		Transpose the child table data
		--		IF no data exists then add a blank row
	
		SET				@ChildIndex = 1

		IF	NOT EXISTS	(	SELECT			#Result.ResultSequenceID
							FROM			#Result
							WHERE			#Result.ResultSequenceID = @ChildIndex
							AND				#Result.ParentRecordID = @ParentRecordID
						)
		BEGIN

			SET				@Data = @SQLParent 

			SELECT			@Data = @Data + ISNULL( #Result.ResultData, REPLICATE ( ',', #ChildRelationship.ColumnCount) ) + ','
			FROM			#ChildRelationship
			LEFT JOIN		#Result ON				#Result.ChildTableID = #ChildRelationship.ChildTableID
										AND			#Result.ParentRecordID = @ParentRecordID
										AND			#Result.ResultSequenceID = @ChildIndex
			ORDER BY		#ChildRelationship.ChildRelationshipID

			INSERT INTO		#ResultTranspose
			(				ColumnData
			)
			SELECT			SUBSTRING( @Data, 1, LEN(@Data) - 1 )

		END

		--		Add the data from the Results table for each child table, group each row by parentid and sequence number 
			
		WHILE  EXISTS	(	SELECT			#Result.ResultSequenceID
							FROM			#Result
							WHERE			#Result.ResultSequenceID = @ChildIndex
							AND				#Result.ParentRecordID  = @ParentRecordID
						)
		BEGIN
	
			SET				@Data = @SQLParent 
		
			SELECT			@Data = @Data + ISNULL( #Result.ResultData, REPLICATE ( ',', #ChildRelationship.ColumnCount - 1) ) + ','
			FROM			#ChildRelationship
			LEFT JOIN		#Result ON				#Result.ChildTableID = #ChildRelationship.ChildTableID
										AND			#Result.ParentRecordID = @ParentRecordID
										AND			#Result.ResultSequenceID = @ChildIndex
			ORDER BY		#ChildRelationship.ChildRelationshipID
						
			INSERT INTO		#ResultTranspose
			(				ColumnData
			)
			SELECT			SUBSTRING( @Data, 1, LEN(@Data) - 1 )

			SET				@ChildIndex = @ChildIndex + 1
		
		END

		SET			@Index = @Index + 1
	END

	SELECT			Columndata
	FROM			#ResultTranspose
	ORDER BY		ResultTransposeID

	--		Clean up

	DROP TABLE		#ChildRelationship 
	DROP TABLE		#ParentExportResult
	DROP TABLE		#ParentChildSQL 
	DROP TABLE		#ResultTranspose 
	DROP TABLE		#Result 

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_ExportRelatedData', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
