﻿
 
CREATE PROCEDURE [dbo].[UserRole_Select]
(
	@nUserRoleID int  = NULL,
	@nAccountID int  = NULL,
	@nUserID int  = NULL,
	@nRoleID int  = NULL,
	@dDateAdded datetime  = NULL,
	@dDateUpdated datetime  = NULL,
	@sOrder nvarchar(200) = UserRoleID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'

	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND AccountID = '+CAST(@nAccountID AS NVARCHAR)

	IF @nAccountID IS NULL 
		SET @sWhere = @sWhere + ' AND AccountID IS NULL '


	IF @nUserRoleID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND UserRoleID = '+CAST(@nUserRoleID AS NVARCHAR)
	IF @nUserID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ur.UserID = '+CAST(@nUserID AS NVARCHAR)
	IF @nRoleID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ur.RoleID = '+CAST(@nRoleID AS NVARCHAR)
	IF @dDateAdded IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ur.DateAdded = '+CAST(@dDateAdded AS NVARCHAR)
	IF @dDateUpdated IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ur.DateUpdated = '+CAST(@dDateUpdated AS NVARCHAR)
	SET @sSelect = 'SELECT * FROM 
	(SELECT (u.FirstName + '' '' + u.LastName )as FullName, ur.*, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum FROM 
	 Role r INNER JOIN
						  UserRole ur ON r.RoleID = ur.RoleID INNER JOIN
						  [User] u ON ur.UserID = u.UserID ' + @sWhere + ') as UserRoleInfo'
						  + ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
                      
	SET @sSelectCount = 'SELECT     COUNT(*) AS TotalRows 
	FROM         Role r INNER JOIN
						  UserRole ur ON r.RoleID = ur.RoleID INNER JOIN
						  [User] u ON ur.UserID = u.UserID  ' + @sWhere
 
	EXEC (@sSelect)
	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('UserRole_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
