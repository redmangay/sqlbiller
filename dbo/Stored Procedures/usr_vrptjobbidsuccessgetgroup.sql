﻿

CREATE procedure usr_vrptjobbidsuccessgetgroup
as

exec  usr_rptjobbidsuccess_view

select distinct GroupDivision, 1 as id from ##rptJobBidSucces
where GroupDivision is not null

union all 
select 'All' as GroupDivision, 0 as id

 order by id,GroupDivision