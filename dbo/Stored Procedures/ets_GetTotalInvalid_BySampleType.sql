﻿
 
CREATE PROCEDURE [dbo].[ets_GetTotalInvalid_BySampleType]
(
	@nSampleTypeID int ,
	@sSystemName varchar(50) ,
	@sValidationOnEntry nvarchar(4000)  = NULL	
)
	/*----------------------------------------------------------------
	EXEC ets_GetTotalInvalid_BySampleType 96,'V001','Value>0 AND Value<100'

	---------------------------------------------------------------*/
 
AS

BEGIN TRY
	DECLARE @data varchar(MAX);
	DECLARE @nSampleID int;
	DECLARE @nCounter int=0;
  
	
		DECLARE @myCurSQL2 varchar(4000)
		SET @myCurSQL2='DECLARE Cur_Sample2 CURSOR FOR SELECT ' +  @sSystemName  + ',SampleID FROM Sample
		 WHERE ' +  @sSystemName  + ' is not NULL AND SampleTypeID=' + convert(varchar,@nSampleTypeID );
		EXEC (@myCurSQL2);
	
		OPEN Cur_Sample2;
		FETCH FROM Cur_Sample2 INTO @data,@nSampleID;
      
		WHILE @@FETCH_STATUS = 0
		  BEGIN
			DECLARE @bIsValid2 bit=1;      
       
		   DECLARE @sSQL2 varchar(MAX);
   
		--DECLARE @sResult varchar(MAX)
	
				IF ISNUMERIC(@data)=1
		    
					SELECT @sSQL2= 'DECLARE @tTemp TABLE
					(result varchar(10), value decimal(16,9)) 
					INSERT INTO @tTemp(value) Values ('+@data+')
					SELECT ''Valid'', value FROM @tTemp WHERE ' + @sValidationOnEntry
				ELSE IF ISDATE(@data)=1
					SELECT @sSQL2= 'DECLARE @tTemp TABLE
					(result varchar(10), value datetime) 
					INSERT INTO @tTemp(value) Values ('''+@data+''')
					SELECT ''Valid'', value FROM @tTemp WHERE ' + @sValidationOnEntry	
				ELSE
					SELECT @sSQL2= 'DECLARE @tTemp TABLE
					(result varchar(10), value varchar(MAX)) 
					INSERT INTO @tTemp(value) Values ('''+@data+''')
					SELECT ''Valid'', value FROM @tTemp WHERE ' + @sValidationOnEntry
			
				BEGIN TRY
					EXEC (@sSQL2)		
					IF @@ROWCOUNT=0
						SET @bIsValid2=0
					
				END TRY
				BEGIN CATCH
					SELECT
						'Error',ERROR_MESSAGE() AS ErrorMessage;
						SET @bIsValid2=0	
				END CATCH
		       
				IF @bIsValid2=0
					SET @nCounter=@nCounter+1;				
				 
	      
				FETCH FROM Cur_Sample2 INTO @data,@nSampleID;
      
		  END	
	CLOSE Cur_Sample2;       
	DEALLOCATE Cur_Sample2;
  
	SELECT @nCounter;
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_GetTotalInvalid_BySampleType', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
