﻿
 
CREATE PROCEDURE [dbo].[ets_MonitorSchedule_Select]
(
	@nMonitorScheduleID int  = NULL,
	@nAccountID int  = NULL,
	@nTableID int  = NULL,
	@dScheduleDateTimeStart datetime  = NULL,	
	@dScheduleDateTimeEnd datetime  = NULL,	
	@bHasAlarm bit  = NULL,
	@dAlarmDateTime datetime  = NULL,
	@nInitialScheduleID int=NULL,
	@dateOneDay datetime=NULL,
	@sSearchText varchar(250)=NULL	
	
)
	/*----------------------------------------------------------------
	ets_MonitorSchedule_Select @nInitialScheduleID=184, @dScheduleDateTimeStart='10/10/2011' @nAccountID=13,@nTableID=109
	ets_MonitorSchedule_Select @sSearchText='Record'
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	--SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nMonitorScheduleID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND MonitorScheduleID = '+CAST(@nMonitorScheduleID AS NVARCHAR)
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND MonitorSchedule.AccountID = '+CAST(@nAccountID AS NVARCHAR)
	IF @nTableID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND MonitorSchedule.TableID = '+CAST(@nTableID AS NVARCHAR)
	IF @dScheduleDateTimeStart IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ScheduleDateTime >= ''' + CONVERT(varchar(30), @dScheduleDateTimeStart, 120) + ''''
	
	IF @dScheduleDateTimeEnd IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ScheduleDateTime <= ''' + CONVERT(varchar(30), @dScheduleDateTimeEnd, 120) + '''' 

	IF @bHasAlarm IS NOT NULL 
		SET @sWhere = @sWhere + ' AND HasAlarm = '+CAST(@bHasAlarm AS NVARCHAR)
	IF @dAlarmDateTime IS NOT NULL 
		SET @sWhere = @sWhere + ' AND AlarmDateTime = '+CAST(@dAlarmDateTime AS NVARCHAR)
	
	IF @nInitialScheduleID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND InitialScheduleID = ' +CAST(@nInitialScheduleID AS NVARCHAR)


	IF @dateOneDay IS NOT NULL 
		SET @sWhere = @sWhere + ' AND CONVERT(date,ScheduleDateTime) =CONVERT(date, ''' + CONVERT(varchar(30), @dateOneDay, 120) + ''')'


	IF @sSearchText IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ( MonitorSchedule.Description LIKE ''%'+ @sSearchText +'%''
		OR [Table].TableName LIKE ''%'+ @sSearchText +'%'') '



	SET @sSelect = 'SELECT MonitorSchedule.*,TableName FROM [MonitorSchedule] 
	   LEFT JOIN [Table] ON MonitorSchedule.TableID=[Table].TableID ' + @sWhere 
	   + ' ORDER BY ScheduleDateTime,AlarmDateTime'

	PRINT (@sSelect)
	EXEC (@sSelect)
	SET ROWCOUNT 0
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_MonitorSchedule_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH 
