﻿

CREATE PROCEDURE [dbo].[ErrorLog_Select]
(
	@nErrorLogID int  = NULL,
	@sModule nvarchar(50)  = NULL,
	@sErrorMessage nvarchar(MAX)  = NULL,
	@sErrorTrack nvarchar(MAX)  = NULL,
	@dErrorTime datetime  = NULL,
	@sPath nvarchar(1000)  = NULL,
	@sOrder nvarchar(200) = ErrorLogID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	-- Stored Procedure: ErrorLog_Select
	-- Generated by: gpGenerateSP (Jon Bosker, DB Gurus Australia) 
	-- Date: Jan 24 2011  1:34AM
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nErrorLogID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ErrorLogID = '+CAST(@nErrorLogID AS NVARCHAR)
	IF @sModule IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Module LIKE '+'''%' + @sModule + '%'''
	IF @sErrorMessage IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ErrorMessage LIKE '+'''%' + @sErrorMessage + '%'''
	IF @sErrorTrack IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ErrorTrack LIKE '+'''' + @sErrorTrack + ''''
	IF @dErrorTime IS NOT NULL 
		SET @sWhere = @sWhere + ' AND ErrorTime = '+CAST(@dErrorTime AS NVARCHAR)
	IF @sPath IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Path LIKE '+'''' + @sPath + ''''
	SET @sSelect = 'SELECT * FROM 
	(SELECT [ErrorLog].*, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum FROM [ErrorLog]' + @sWhere + ') as UserInfo'
	+ ' WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)

	SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [ErrorLog] ' + @sWhere 
 
	-- Extend WHERE to include paging:
	--SET @sWhere = @sWhere + ' AND RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	--SET @sOrder = ' ORDER BY ' + @sOrder
	--SET @sSelect = @sSelect + @sWhere + @sOrder
 
	EXEC (@sSelect)
	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ErrorLog_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
