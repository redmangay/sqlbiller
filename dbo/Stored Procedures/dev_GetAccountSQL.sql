﻿

CREATE		PROCEDURE [dbo].[dev_GetAccountSQL]
(
     @TableID		varchar(MAX) = NULL
	,@TableName		varchar(MAX) = NULL
	,@AccountID		varchar(MAX) = NULL
)	
AS

BEGIN TRY
/*	List formatted SQL for an Account or Table
	
SELECT			'EXEC		[dbo].[dev_GetAccountSQL] @AccountID = ''' + CAST( AccountID AS varchar(MAX) ) + '''		/*	' + AccountName + ' */'
	FROM		[Account]
	WHERE		[IsActive] = 1
	AND			AccountName LIKE '%MT%'
	ORDER BY	AccountName

	EXEC		[dbo].[dev_GetAccountSQL] @AccountID = '24918'		/*	buffalobiller1 */
	EXEC		[dbo].[dev_GetAccountSQL] @AccountID = '24910'		/*	DBGurus JC */
	EXEC		[dbo].[dev_GetAccountSQL] @TableID = 2428, @TableName = 'Client', @AccountID = '24910'
*/

	DECLARE			@FilteredAccountTableID		INT = 1
	DECLARE			@SQL						varchar(MAX) 
	DECLARE			@ChildColumnID				INT = 1

	SET				CONCAT_NULL_YIELDS_NULL OFF;
	SET				NOCOUNT ON;

	/*	Get account from tableID	*/

	IF			@AccountID IS NULL
		AND		@TableID IS NOT NULL
		SELECT		@AccountID = [Table].AccountID
		FROM		[Table]
		WHERE		1 = 1
		AND			TableID = @TableID;

	IF			@AccountID IS NULL
		BEGIN
			PRINT		'Include an AccountID or TableID';
			RETURN		0;
		END;

	/*	Get a list of table commands from the account	*/

	SELECT			'EXEC		[dbo].[dev_GetAccountSQL] @TableID = ' + CAST(TableID AS varchar(MAX)) + ', @TableName = ''' + TableName + '''' + ', @AccountID = ''' + @AccountID + '''' AS	GetTable
					,TableID
					,TableName
					,AccountName
		INTO		#AccountTable
		FROM		[Table]
		JOIN		[Account] ON			[Account].AccountID = [Table].AccountID
		WHERE		1 = 1
		AND			[Account].AccountID = @AccountID
		AND 		[Table].IsActive = 1
		ORDER BY	TableName;

	SELECT			GetTable
					,TableID
					,TableName
					,AccountName
		FROM		#AccountTable
		ORDER BY	TableName;

	/*	Create a list of tables to build the set of usueful SQL	*/

	SELECT			ROW_NUMBER() OVER(ORDER BY TableName DESC) AS FilteredAccountTableID
					,GetTable
					,TableID
					,TableName 
					,AccountName
		INTO		#FilteredAccountTable 
		FROM		#AccountTable
		WHERE		1 = 1
		AND			(	(	@TableID IS NULL AND @TableName IS NULL  )	/*	Full account information	*/
					OR	TableID = @TableID								/*	TableID only				*/
					OR	TableName LIKE @TableName						/*	All for matching name		*/
					);

	/*	Build SQL statements for each table	*/

	WHILE EXISTS	(	SELECT		#FilteredAccountTable.FilteredAccountTableID
						FROM		#FilteredAccountTable 
						WHERE		1 = 1
						AND			#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID
					)		
	BEGIN
		
	/*	Build SELECT statement	*/

		SELECT			@SQL = '
/' + SUBSTRING( REPLICATE( '*', 20 ) + '[' + TableName + ']' + REPLICATE( '*', 50 ), 1, 50 ) + '/
/*	SELECT [' + TableName + '] */

SELECT			[' + TableName + '].[TableID]
'
			FROM		#FilteredAccountTable 
			WHERE		1 = 1
			AND			#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID;

		SELECT			@SQL = @SQL + '				,['+[Table].TableName+'].' + [Column].[SystemName] + CASE WHEN LEN([Column].[SystemName]) < 7 THEN '	' ELSE '' END + '	AS				[' + [Column].[DisplayName] + ']
'
			FROM			[Table]
			JOIN			#FilteredAccountTable ON		#FilteredAccountTable.TableID = [Table].TableID
													AND		#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID
			JOIN			[Column] ON						[Column].TableID = [Table].TableID
													AND		([Column].IsStandard = 0
													OR		[Column].SystemName = 'RecordID')
			ORDER BY		[Column].[DisplayOrder];		

		SELECT			@SQL = @SQL + '				,['+TableName+'].DateAdded
	FROM		[Record] ['+TableName+'] WITH (NOLOCK)'
			FROM		#FilteredAccountTable 
			WHERE		1 = 1
			AND			#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID;

		PRINT			@SQL;
		
	/*	Build child table joins	*/

		SET			@ChildColumnID = 1

		SELECT			ROW_NUMBER() OVER(ORDER BY [Column].DisplayOrder DESC) AS		ChildColumnID
						,[Column].ColumnID
			INTO		#ChildColumn
			FROM		[Column]
			JOIN		[Table] ON					[Table].TableID = [Column].TableID
			JOIN		#FilteredAccountTable ON	#FilteredAccountTable.TableID = [Table].TableID
											AND		#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID
			JOIN		[Table] ChildTable ON		ChildTable.TableID = [Column].TableTableID
			WHERE		[Column].TableTableID IS NOT NULL
			ORDER BY	[Column].DisplayOrder;

		WHILE EXISTS	(	SELECT			#ChildColumn.ChildColumnID
								FROM		#ChildColumn
								WHERE		#ChildColumn.ChildColumnID = @ChildColumnID
						)
			BEGIN
				SELECT			@SQL = '	LEFT JOIN	Record [' + ISNULL( [Column].[DisplayTextDetail], [Column].[SystemName] ) + '] WITH (nolock) ON					[' + ISNULL( [Column].[DisplayTextDetail], [Column].[SystemName] ) + '].TableID = ''' + CAST( [Column].TableTableID AS varchar(MAX) ) + '''		/*	' + ChildTable.TableName + '	*/
											AND		[' + ISNULL( [Column].[DisplayTextDetail], [Column].[SystemName] ) + '].IsActive = 1
											AND		CAST( [' + ISNULL( [Column].[DisplayTextDetail], [Column].[SystemName] ) + '].RecordID AS varchar(MAX)) = [' + [Table].TableName + '].' + [Column].SystemName + '		/*	' + ISNULL( [Column].[DisplayTextDetail], [Column].[SystemName] ) + '	*/'
					FROM		[Column]
					JOIN		#ChildColumn ON					#ChildColumn.ColumnID = [Column].ColumnID
													AND			#ChildColumn.ChildColumnID = @ChildColumnID
					JOIN		[Table] ON						[Table].TableID = [Column].TableID
					JOIN		[Table] ChildTable ON			ChildTable.TableID = [Column].TableTableID;
	
				PRINT		@SQL

				SET			@ChildColumnID = @ChildColumnID + 1
			END;

		DROP TABLE		#ChildColumn;
	
		SELECT			@SQL = '	WHERE		['+TableName+'].TableID = ''' + CAST( TableID AS varchar(MAX) ) + '''		/*	' + TableName + '	*/
	AND			['+TableName+'].IsActive = 1'
			FROM		#FilteredAccountTable 
			WHERE		1 = 1
			AND			#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID;

		PRINT			@SQL;

	/*	Build DELETE statement	*/
	
		SELECT			@SQL = '
/*	DELETE [' + TableName + '] */

DELETE
	FROM		[Record] 
	WHERE		TableID = ''' + CAST( TableID AS varchar(MAX) ) + '''	/*	[' + TableName + ']	*/
'
			FROM			#FilteredAccountTable 
			WHERE			1 = 1
			AND				#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID;

		PRINT			@SQL;
		
	/*	Build Insert statement	*/

		SELECT			@SQL = '
/*	INSERT [' + TableName + '] */

INSERT INTO		[Record] 			/*	[' + TableName + ']	*/
	(			[TableID]
				,DateTimeRecorded
				,EnteredBy
				,IsActive
				,DateUpdated'
			FROM		#FilteredAccountTable 
			WHERE		1 = 1
			AND			#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID;
			
		SELECT			@SQL = @SQL + '
				,' + [Column].[SystemName] + '				/*	[' + [Column].[DisplayName] + ']	*/'
			FROM		[Table]
			JOIN		#FilteredAccountTable ON			#FilteredAccountTable.TableID = [Table].TableID
													AND		#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID
			JOIN		[Column] ON							[Column].TableID = [Table].TableID
													AND		([Column].IsStandard = 0
													OR		[Column].SystemName = 'RecordID')
			WHERE		[Column].SystemName not in ('RecordID')
			ORDER BY	[Column].[DisplayOrder];

		
		SET				@SQL = SUBSTRING(@SQL, 1, LEN(@SQL)) + '
				,DateAdded
	)'

		SELECT			@SQL = @SQL + '
	SELECT		['+TableName+'].[TableID]
				,GETDATE()
				,['+TableName+'].EnteredBy
				,['+TableName+'].IsActive
				,GETDATE()'
			FROM		#FilteredAccountTable 
			WHERE		1 = 1
			AND			#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID;

		SELECT			@SQL = @SQL + '
				,' + [Column].[SystemName] + '				/*	[' + [Column].[DisplayName] + ']	*/'
			FROM		[Table]
			JOIN		#FilteredAccountTable ON			#FilteredAccountTable.TableID = [Table].TableID
													AND		#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID
			JOIN		[Column] ON							[Column].TableID = [Table].TableID
													AND		([Column].IsStandard = 0
													OR		[Column].SystemName = 'RecordID')
			WHERE		[Column].SystemName not in ('RecordID')
			ORDER BY	[Column].[DisplayOrder];

		SELECT			@SQL = @SQL + '
				,['+TableName+'].DateAdded
	FROM		[Record] ['+TableName+']
	WHERE		['+TableName+'].TableID = ''' + CAST( TableID AS varchar(MAX) ) + '''		/*	' + TableName + '	*/
	AND			['+TableName+'].IsActive = 1
	
/' + SUBSTRING( REPLICATE( '*', 20 ) + REPLICATE( '*', LEN(TableName) ) + REPLICATE( '*', 50 ), 1, 50 ) + '/
'
			FROM		#FilteredAccountTable 
			WHERE		1 = 1
			AND			#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID;

		PRINT			@SQL;	
		
	/*	Build UPDATE statement	*/

		SELECT			@SQL = '
/*	UPDATE [' + TableName + '] */

UPDATE			[Record] 			/*	[' + TableName + ']	*/
	SET			[TableID] = [TableID]
				,DateTimeRecorded = DateTimeRecorded
				,EnteredBy = EnteredBy
				,IsActive = IsActive
				,DateUpdated = DateUpdated'
			FROM		#FilteredAccountTable 
			WHERE		1 = 1
			AND			#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID;
			
		SELECT			@SQL = @SQL + '
				,' + [Column].[SystemName] + ' = ' + [Column].[SystemName] + '				/*	[' + [Column].[DisplayName] + ']	*/'
			FROM		[Table]
			JOIN		#FilteredAccountTable ON			#FilteredAccountTable.TableID = [Table].TableID
													AND		#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID
			JOIN		[Column] ON							[Column].TableID = [Table].TableID
													AND		([Column].IsStandard = 0
													OR		[Column].SystemName = 'RecordID')
			WHERE		[Column].SystemName not in ('RecordID')
			ORDER BY	[Column].[DisplayOrder];
		
		SELECT			@SQL = @SQL + '
	FROM		[Record] ['+TableName+']
	WHERE		['+TableName+'].TableID = ''' + CAST( TableID AS varchar(MAX) ) + '''		/*	' + TableName + '	*/
	AND			['+TableName+'].IsActive = 1
	
/' + SUBSTRING( REPLICATE( '*', 20 ) + REPLICATE( '*', LEN(TableName) ) + REPLICATE( '*', 50 ), 1, 50 ) + '/
'
			FROM		#FilteredAccountTable 
			WHERE		1 = 1
			AND			#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID;

		PRINT			@SQL;	
		
	/*	Build Stored procedure for Add and Save	*/

		EXEC	dev_SPFramework @FilteredAccountTableID;

--		SELECT			@SQL = '
--/*	sp for Save [' + TableName + '] */

--CREATE PROCEDURE		[dbo].[' + REPLACE(AccountName, ' ', '') + '_Save' + REPLACE(TableName, ' ', '') + ']
--(
--	@RecordID  int = NULL,
--	@FormSetID int = NULL,
--    @UserID int=NULL,
--    @Return varchar(MAX) output
--)
--AS
--BEGIN
--	/*
--					DECLARE		@Return		varchar(MAX)
--					EXEC		[' + REPLACE(AccountName, ' ', '') + '_Save' + REPLACE(TableName, ' ', '') + '] @RecordID = n, @Return = @Return

--					EXEC		[dbo].[dev_GetAccountSQL] @TableID = ' + CAST( TableID AS varchar(MAX) ) + ', @AccountID = ' + CAST( @AccountID AS varchar(MAX) ) + '
					
--					--UPDATE			[Table] 
--					--	SET			[AddRecordSP] = ' + REPLACE(AccountName, ' ', '') + '_Save' + REPLACE(TableName, ' ', '') + '
--					--				,[SPSaveRecord]  = ' + REPLACE(AccountName, ' ', '') + '_Save' + REPLACE(TableName, ' ', '') + '
--					SELECT			AddRecordSP
--									,SPSaveRecord
--									,*
--						FROM		[Table]
--						WHERE		[TableID] = ' + CAST( TableID AS varchar(MAX) ) + '

--					SELECT			[UserID]
--									,[FirstName]
--									,[LastName]
--									,[PhoneNumber]
--									,[Email]
--									,[Password]
--									,[IsActive]
--									,[DateAdded]
--									,[DateUpdated]
--									,[LoginCount]
--									,[LastLoginTime]
--						FROM		[User]

						
--					AND			' + REPLACE(TableName, ' ', '') + '.RecordID = @RecordID
--	*/
--END'
--			FROM		#FilteredAccountTable 
--			WHERE		1 = 1
--			AND			#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID;
					
		SELECT			@SQL = @SQL + '

/' + SUBSTRING( REPLICATE( '*', 20 ) + REPLICATE( '*', LEN(TableName) ) + REPLICATE( '*', 50 ), 1, 50 ) + '/
'
			FROM		#FilteredAccountTable 
			WHERE		1 = 1
			AND			#FilteredAccountTable.FilteredAccountTableID = 	@FilteredAccountTableID;

		PRINT			@SQL;	

	/*	Insert Above	*/

		SET				@FilteredAccountTableID = @FilteredAccountTableID + 1;
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dev_GetAccountSQL', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
