﻿-- =============================================
-- Author:		<Red Mangay>
-- Create date:	<10/07/2018>
-- Description:	<>
-- =============================================
/*

exec [Renzo_GetCurrencyRate]   11549291, 2

*/


CREATE PROCEDURE [dbo].[Renzo_GetCurrencyRate]
(
	@RecordID int,
	@param001 VARCHAR(MAX)
	
)
AS
BEGIN			

DECLARE @CurrencyRate varchar(max)
   
    SELECT @CurrencyRate = ISNULL(V007,1) FROM [Record] 
    WHERE ISACTIVE=1 AND RecordID = @RecordID

    

   SELECT Round(cast(@param001 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)),2)  AS Rate


END

