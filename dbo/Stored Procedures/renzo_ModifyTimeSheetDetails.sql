﻿CREATE PROCEDURE [dbo].[renzo_ModifyTimeSheetDetails]
(@RecordID  INT,
 @FormSetID INT          = NULL,
 @UserID    INT          = NULL,
 @Return    VARCHAR(MAX) OUTPUT
)
AS
         BEGIN

 --    EXEC [dbo].[renzo_ModifyTimeSheetDetails]  @RecordID= 2713336, @Return=''
             DECLARE @ColumnID INT;
             DECLARE @TableID INT;
             DECLARE @ColumnIDStaus BIT;
             SELECT @TableID = [TableID]
             FROM [Record]
             WHERE [RecordID] = @RecordID;
             SELECT @ColumnIDStaus = [IsReadOnly]
             FROM [Column]
             WHERE [ColumnID] = 49888;

--PRINT @ColumnIDStaus
             IF @ColumnIDStaus = 1
                 BEGIN
--PRINT ''
                     UPDATE [COLUMN]
                       SET
                           [ISREADONLY] = 0
                     WHERE [COLUMNID] IN(49888, 49889, 49887 , 50073 --Status
				 , 49884, 49890)
                     AND [TableID] = 2729;
                 END;
             IF @ColumnIDStaus = 0
                 BEGIN
                     UPDATE [COLUMN]
                       SET
                           [ISREADONLY] = 1
                     WHERE [COLUMNID] IN(49888, 49889, 49887   , 50073 --Status
				 , 49884, 49890)
                     AND [TableID] = 2729;

		  
-- lets update Tax Amount
                     DECLARE @TaxRate VARCHAR(MAX);
                     SELECT @TaxRate = ([V011] / 100.00)
                     FROM [RECORD]
                     WHERE [RECORDID] =
(
    SELECT [V001]
    FROM [RECORD]
    WHERE [RECORDID] = @RecordID
);
                     PRINT @TaxRate;

--lets disable timesheet
                     DECLARE @RecordIDToUpdate_TimeSheet INT;
                     DECLARE @RecordIDToUpdate_Disbursement INT;

--Lets get RecordIDs for Disbursement and Timesheet
                     SELECT @RecordIDToUpdate_TimeSheet = [V011],
                            @RecordIDToUpdate_Disbursement = [V010]
                     FROM [RECORD]
                     WHERE [RecordID] = @RecordID;	
	
	--print @RecordIDToUpdate

-- Timesheet
                     IF @RecordIDToUpdate_TimeSheet IS NOT NULL
                         BEGIN
                             IF NOT EXISTS
(
    SELECT [TimeSheetDetails].[V011]
    FROM [RECORD] [TimeSheetDetails]
         INNER JOIN [RECORD] [TimeSheet] ON [TimeSheet].[TableID] = 2488
                                            AND [TimeSheet].[IsActive] = 1
                                            AND CAST([TimeSheet].[V007] AS MONEY) = CAST([TimeSheetDetails].[V004] AS MONEY)
                                            AND CAST([TimeSheet].[V015] AS MONEY) = CAST([TimeSheetDetails].[V005] AS MONEY)
                                            AND CAST([TimeSheet].[V009] AS MONEY) = CAST([TimeSheetDetails].[V006] AS MONEY)
                                            AND [TimeSheet].[V018] = [TimeSheetDetails].[V001]
                                            AND CAST([TimeSheet].[RecordID] AS VARCHAR) = [TimeSheetDetails].[V011]
    WHERE [TimeSheetDetails].[TableID] = 2729
          AND [TimeSheetDetails].[IsActive] = 1
          AND [TimeSheetDetails].[RecordID] = @RecordID
          AND [TimeSheetDetails].[V022] = '1'
) -- Status as ByRate 
                                 BEGIN

--lets Disable Timesheet
                                     UPDATE [Record]
                                       SET
                                           [IsReadOnly] = 1
                                     WHERE [RecordID] = @RecordIDToUpdate_TimeSheet;
                                 END;
                             IF
(
    SELECT [V022]
    FROM [RECORD]
    WHERE [RECORDID] = @RecordID
) = '0'
                                 BEGIN
                                     UPDATE [RECORD]
                                       SET
                                           [V022] = '1'
                                     WHERE [RECORDID] = @RecordID;
                                 END;
                                 ELSE
                                 BEGIN
                                     IF
(
    SELECT [V022]
    FROM [RECORD]
    WHERE [RECORDID] = @RecordID
) = '5'
OR
(
    SELECT [V022]
    FROM [RECORD]
    WHERE [RECORDID] = @RecordID
) = '4'
OR
(
    SELECT [V022]
    FROM [RECORD]
    WHERE [RECORDID] = @RecordID
) = '3'
                                         BEGIN
                                             UPDATE [RECORD]
                                               SET
                                                   [V007] = 0.00,
                                                   [V012] = NULL
                                             WHERE [RECORDID] = @RecordID;
                                         END;
                                         ELSE
                                         BEGIN
                                             IF
(
    SELECT [V022]
    FROM [RECORD]
    WHERE [RECORDID] = @RecordID
) = '7' -- Adjusted
                                                 BEGIN
                                                     UPDATE [RECORD]
                                                       SET
                                                           [V007] = 0.00,
                                                           [V004] = 0.00
                                                           , -- Amount 
                                                           [V005] = 0.00
                                                           , -- Hr 
                                                           [V006] = 0.00
                                                           , -- Rate 
                                                           [V012] = NULL -- Tax
                                                     WHERE [RECORDID] = @RecordID;
                                                 END;
                                         END;
                                 END;

-- lets update the tax
                             IF
(
    SELECT [V022]
    FROM [RECORD]
    WHERE [RECORDID] = @RecordID
) = '1'
OR
(
    SELECT [V022]
    FROM [RECORD]
    WHERE [RECORDID] = @RecordID
) = '2'
                                 BEGIN

-- Update Tax
                                     UPDATE [Timesheet Details]
                                       SET
								   [V012] = CAST(CAST(REPLACE([Timesheet Details].[V007], ',', '') AS DEC(20, 4))*@TaxRate AS VARCHAR(MAX))				

/*	[GST]	*/

                                     FROM [RECORD] [Timesheet Details]
                                     WHERE [Timesheet Details].[TableID] = '2729'
                                           AND [Timesheet Details].[RECORDID] = @RecordID;
                                 END;
                         END;
-- Disbursement
                     IF @RecordIDToUpdate_Disbursement IS NOT NULL
                         BEGIN
                             IF NOT EXISTS
(
    SELECT [TimeSheetDetails].[V010]
    FROM [RECORD] [TimeSheetDetails]
         INNER JOIN [RECORD] [Disbursement] ON [Disbursement].[TableID] = 2725
                                               AND [Disbursement].[IsActive] = 1
                                               AND CAST([Disbursement].[V004] AS MONEY) = CAST([TimeSheetDetails].[V005] AS MONEY) 

/*	[Hr/Unit]	*/

                                               AND CAST([Disbursement].[V005] AS MONEY) = CAST([TimeSheetDetails].[V006] AS MONEY) 

/*	[Rate]	*/

                                               AND CAST([Disbursement].[V007] AS MONEY) = CAST([TimeSheetDetails].[V007] AS MONEY) 

/*	[$ Amount]	*/

                                               AND [Disbursement].[V001] = [TimeSheetDetails].[V001] 

/*	[Job No]	*/

                                               AND CAST([Disbursement].[RecordID] AS VARCHAR) = [TimeSheetDetails].[V010]
    WHERE [TimeSheetDetails].[TableID] = 2729
          AND [TimeSheetDetails].[IsActive] = 1
          AND [TimeSheetDetails].[RecordID] = @RecordID
          AND [TimeSheetDetails].[V022] = '2'
) -- Status as Fixed 
                                 BEGIN

--lets disable Disbursement
                                     UPDATE [Record]
                                       SET
                                           [IsReadOnly] = 1
                                     WHERE [RecordID] = @RecordIDToUpdate_Disbursement;
                                 END;
                             IF
(
    SELECT [V022]
    FROM [RECORD]
    WHERE [RECORDID] = @RecordID
) = '0'
                                 BEGIN
                                     UPDATE [RECORD]
                                       SET
                                           [V022] = '2'
                                     WHERE [RECORDID] = @RecordID;
                                 END;
                                 ELSE
                                 BEGIN
                                     IF
(
    SELECT [V022]
    FROM [RECORD]
    WHERE [RECORDID] = @RecordID
) = '5'
OR
(
    SELECT [V022]
    FROM [RECORD]
    WHERE [RECORDID] = @RecordID
) = '4'
OR
(
    SELECT [V022]
    FROM [RECORD]
    WHERE [RECORDID] = @RecordID
) = '3'
                                         BEGIN
                                             UPDATE [RECORD]
                                               SET
                                                   [V007] = NULL,
                                                   [V012] = NULL
                                             WHERE [RECORDID] = @RecordID;
                                         END;
                                         ELSE
                                         BEGIN
                                             IF
(
    SELECT [V022]
    FROM [RECORD]
    WHERE [RECORDID] = @RecordID
) = '7' -- Adjusted
                                                 BEGIN
                                                     UPDATE [RECORD]
                                                       SET
                                                           [V007] = 0.00,
                                                           [V004] = 0.00
                                                           , -- Amount 
                                                           [V005] = 0.00
                                                           , -- Hr 
                                                           [V006] = 0.00
                                                           , -- Rate 
                                                           [V012] = NULL -- Tax
                                                     WHERE [RECORDID] = @RecordID;
                                                 END;
                                         END;
                                 END;


-- lets update the tax
                             IF
(
    SELECT [V022]
    FROM [RECORD]
    WHERE [RECORDID] = @RecordID
) = '1'
OR
(
    SELECT [V022]
    FROM [RECORD]
    WHERE [RECORDID] = @RecordID
) = '2'
                                 BEGIN

-- Update Tax
                                     UPDATE [Timesheet Details]
                                       SET
                                           [V012] = CAST(CAST(REPLACE([Timesheet Details].[V007], ',', '') AS DEC(20, 4))*@TaxRate AS VARCHAR(MAX))				

/*	[GST]	*/

                                     FROM [RECORD] [Timesheet Details]
                                     WHERE [Timesheet Details].[TableID] = '2729'
                                           AND [RECORDID] = @RecordID;
                                 END;
                         END;
                 END;
             RETURN @@ERROR;
         END;