﻿-- =============================================
-- Author:		<Red Mangay>
-- Create date:	<10/07/2018>
-- Description:	<Get the tableid of the orginal table ie timesheet or disbursement>
-- =============================================
/*

exec [GetTimeSheetTableIDFrom] 2713164

*/


CREATE PROCEDURE [dbo].[Renzo_DisableTimeSheetDetailsStatus]
(@RecordID  INT,
 @FormSetID INT          = NULL,
 @UserID    INT          = NULL,
 @Return    VARCHAR(MAX) OUTPUT
)
AS
BEGIN
	
		
		  --UPDATE [COLUMN]
    --                   SET
    --                       [ISREADONLY] = 1
    --                 WHERE [COLUMNID] IN(49888, 49889, 49887, 50073
				--			 ,49884, 49890)
    --                 AND [TableID] = 2729;

				UPDATE [COLUMN]
                    SET
                    [DisplayTextDetail] = NULL
                    WHERE [COLUMNID] IN(49888, 49889, 49887, 50073
				,49884, 49890, 50075)
                    AND [TableID] = 2729;

				SELECT * FROM [COLUMN]
				WHERE [TableID] = 2729;


				 UPDATE [COLUMN]
				 SET [DisplayTextDetail] = '(X)'
				 WHERE [COLUMNID] = 50090
				 AND [TableID] = 2729;

				  UPDATE [COLUMN]
				 SET [DisplayTextDetail] = 'Hr/Unit'
				 WHERE [COLUMNID] = 50091
				 AND [TableID] = 2729;

				  UPDATE [COLUMN]
				 SET [DisplayTextDetail] = 'Rate'
				 WHERE [COLUMNID] = 50092
				 AND [TableID] = 2729;

				   UPDATE [COLUMN]
				 SET [DisplayTextDetail] = 'Amount'
				 WHERE [COLUMNID] = 50093
				 AND [TableID] = 2729;

				 UPDATE [COLUMN]
				 SET [DisplayTextDetail] = 'Status'
				 WHERE [COLUMNID] = 50094
				 AND [TableID] = 2729;

				  UPDATE [COLUMN]
				 SET [DisplayTextDetail] = 'Modify'
				 WHERE [COLUMNID] = 50074
				 AND [TableID] = 2729;

				  UPDATE [COLUMN]
				 SET [DisplayTextDetail] = 'Job No'
				 WHERE [COLUMNID] = 50095
				 AND [TableID] = 2729;

				 
				 


END

