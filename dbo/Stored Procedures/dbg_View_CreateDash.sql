﻿
-- Dash view create SP, it's like [dbg_View_CreateDefault]


CREATE PROCEDURE   [dbo].[dbg_View_CreateDash]
(
	@UserID INT=NULL,
	@TableID int,
    @ViewID int OUTPUT	
)
AS

BEGIN TRY

     
		INSERT INTO [View]
		(
			TableID, 
			ViewName, 
			UserID, 
			RowsPerPage, 
			SortOrder, 
			Filter, 
			ShowSearchFields, 
			ShowAddIcon, 
			[ShowExportIcon],[ShowEmailIcon], 
			ShowDeleteIcon, 
			ShowViewIcon, 
			ShowBulkUpdateIcon,FilterControlsInfo,ViewPageType,ShowFixedHeader
		) SELECT  
			TableID,
			TableName,
			@UserID,
			10,
			NULL,
			NULL,
			1,
			1,
			1,0,
			1,
			1,
			1,NULL,'dash',0 
			FROM [Table] 
			WHERE TableID=@TableID

		SELECT @ViewID = @@IDENTITY 
   
		EXEC [dbo].dbg_View_CreateDefaultViewItems @ViewID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_View_CreateDash', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
