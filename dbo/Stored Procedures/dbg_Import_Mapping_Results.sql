﻿

CREATE PROCEDURE [dbo].[dbg_Import_Mapping_Results] 
	-- Add the parameters for the stored procedure here
	@TableID int, 
	@BatchID int,
	@Site varchar(MAX) = NULL,
	@TotalRecordsCount int OUTPUT,
	@ErrorsCount int OUTPUT
AS
BEGIN TRY
	SET NOCOUNT ON;

	DECLARE @sql nvarchar(MAX)

	DECLARE @SourceAccountColumnName AS varchar(50)
	SELECT @SourceAccountColumnName = [SystemName]
		FROM [Column]
		WHERE ([TableID] = @TableID) AND ([DisplayName] = 'Incoming Site')
	PRINT @SourceAccountColumnName

	DECLARE @SourceTableColumnName AS varchar(50)
	SELECT @SourceTableColumnName = [SystemName]
		FROM [Column]
		WHERE ([TableID] = @TableID) AND ([DisplayName] = 'Incoming Sample Type')
	PRINT @SourceTableColumnName

	DECLARE @SourceAnalyteColumnName AS varchar(50)
	SELECT @SourceAnalyteColumnName = [SystemName]
		FROM [Column]
		WHERE ([TableID] = @TableID) AND ([DisplayName] = 'Incoming Analyte Name')
	PRINT @SourceAnalyteColumnName

	DECLARE @SourceTotalOrDissolvedColumnName AS varchar(50)
		SELECT @SourceTotalOrDissolvedColumnName = [SystemName]
		FROM [Column]
		WHERE ([TableID] = @TableID) AND ([DisplayName] = 'Total Or Dissolved')
	PRINT @SourceTotalOrDissolvedColumnName

	SET @sql = 'SELECT DISTINCT [' + @SourceAccountColumnName + '] AS [Site],' +
		' [' +  @SourceTableColumnName + '] AS [Sample Type],' +
		' [' + @SourceAnalyteColumnName + '] AS [Analyte Name],' +
		' [' + @SourceTotalOrDissolvedColumnName + '] AS [Total Or Dissolved],' +
		' COUNT(RecordID) AS [Number Of Records]' +
		' FROM TempRecord' +
		' WHERE [BatchID]=' + CAST(@BatchID AS varchar(20)) + ' AND RejectReason IS NULL' +
		CASE
			WHEN @Site IS NULL THEN ''
			ELSE ' AND [' + @SourceAccountColumnName + ']=''' + @Site + ''''
		END +
		' GROUP BY [' + @SourceAccountColumnName + '],' +
		' [' +  @SourceTableColumnName + '],' +
		' [' + @SourceAnalyteColumnName + '],' +
		' [' + @SourceTotalOrDissolvedColumnName + ']'

	PRINT @sql
	EXEC (@sql)

	SET @sql = 'SELECT DISTINCT [' + @SourceAccountColumnName + '] AS [Site],' +
		' [' +  @SourceTableColumnName + '] AS [Sample Type],' +
		' [' + @SourceAnalyteColumnName + '] AS [Analyte Name],' +
		' [' + @SourceTotalOrDissolvedColumnName + '] AS [Total Or Dissolved],' +
		' RejectReason AS [Reject Reason],' +
		' COUNT(RecordID) AS [Number Of Records]' +
		' FROM TempRecord' +
		' WHERE [BatchID]=' + CAST(@BatchID AS varchar(20)) + ' AND RejectReason IS NOT NULL' +
		CASE
			WHEN @Site IS NULL THEN ''
			ELSE ' AND [' + @SourceAccountColumnName + ']=''' + @Site + ''''
		END +
		' GROUP BY [' + @SourceAccountColumnName + '],' +
		' [' +  @SourceTableColumnName + '],' +
		' [' + @SourceAnalyteColumnName + '],' +
		' [' + @SourceTotalOrDissolvedColumnName + '],' +
		' RejectReason'

	PRINT @sql
	EXEC (@sql)

    SET @sql = 'SELECT @TotalRecordsCount = COUNT(*) FROM TempRecord WHERE BatchID=' + CAST(@BatchID AS varchar(20)) +
		CASE
			WHEN @Site IS NULL THEN ''
			ELSE ' AND [' + @SourceAccountColumnName + ']=''' + @Site + ''''
		END

	PRINT @sql
	EXEC sp_executesql
		@sql,
		N'@BatchID int, @Site varchar(MAX), @TotalRecordsCount INT OUTPUT',
		@BatchID = @BatchID,
		@Site = @Site,
		@TotalRecordsCount = @TotalRecordsCount OUTPUT

    SET @sql = 'SELECT @ErrorsCount = COUNT(*) FROM TempRecord WHERE RejectReason IS NOT NULL AND BatchID=' + CAST(@BatchID AS varchar(20)) +
		CASE
			WHEN @Site IS NULL THEN ''
			ELSE ' AND [' + @SourceAccountColumnName + ']=''' + @Site + ''''
		END

	PRINT @sql
	EXEC sp_executesql
		@sql,
		N'@BatchID int, @Site varchar(MAX), @ErrorsCount INT OUTPUT',
		@BatchID = @BatchID,
		@Site = @Site,
		@ErrorsCount = @ErrorsCount OUTPUT


END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_Import_Mapping_Results', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
