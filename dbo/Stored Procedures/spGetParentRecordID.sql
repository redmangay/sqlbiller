﻿

CREATE PROCEDURE [dbo].[spGetParentRecordID]
(
	@nChildRecordID int = NULL
)
AS

BEGIN TRY
	-- Get ParentID from current record id
	-- test: EXEC spGetParentRecordID @nChildRecordID=874421
	DECLARE @SQL varchar(MAX)
	SELECT @SQL = 'SELECT Parent.RecordID' +  
			+ ' FROM [Record] Parent ' 
			+ ' JOIN [Record] Child ON Child.'  + C.SystemName + ' = Parent.' + ParentCol.SystemName
			+ ' WHERE Parent.TableID = ' + cast(ParentCol.TableID as varchar)
			+ ' AND Child.RecordID = ' + CAST(@nChildRecordID AS varchar)
		FROM [Record] R
		JOIN [Table] T ON T.TableID = R.TableID 
		JOIN [Column] C ON C.TableID = R.TableID AND C.TableTableID = T.ParentTableID 
		JOIN [Column] ParentCol ON ParentCol.ColumnID = C.LinkedParentColumnID
		WHERE R.RecordID = @nChildRecordID

	PRINT @SQL	 
	EXEC (@SQL)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spGetParentRecordID', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
