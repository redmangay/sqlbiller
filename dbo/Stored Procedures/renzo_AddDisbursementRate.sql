﻿CREATE PROCEDURE		[dbo].[renzo_AddDisbursementRate]
(
	@RecordID  int,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output
)
AS

/*
		Set/update the rate based on the designation of the employee and the rate against that designation on the job

		EXEC			[renzo_AddDisbursementRate] 1574658

		
		EXEC [dbo].[spGetTableSQL] 2478, 'Disbursement Rate'
*/

DECLARE			@Job			INT
				,@Disbursement	INT
				,@Rate			VARCHAR(MAX)
				,@Details		VARCHAR(MAX)

--		Get the Disbursement and job from the Disbursement record

SELECT			@Disbursement = [Record].[V003]
				,@Job = [Record].[V001]
FROM			[Record]
WHERE			[Record].TableID = 2725	-- Disbursement table
AND				ISNUMERIC( [Record].[V001] ) = 1
AND				ISNUMERIC( [Record].[V003] ) = 1
AND				[Record].IsActive = 1
AND				[Record].RecordID = @RecordID

--		Use the Job and Designation info to get the Rate store for that designation against the Job

SELECT			@Rate = REPLACE( [Record].[V002], '$', '' )
				,@Details = [Record].[V001]
FROM			[Record]
WHERE			[Record].TableID = 2478 -- Disbursement Rate table
AND				[Record].RecordID = @Disbursement
AND				[IsActive] = 1
ORDER BY		[Record].[V003]

PRINT			'@Job ' + CAST( @Job AS VARCHAR(MAX) ) 
PRINT			'@Disbursement ' + CAST( @Disbursement AS VARCHAR(MAX) ) 
PRINT			'@Rate ' + CAST( @Rate AS VARCHAR(MAX) ) 
PRINT			'@Details ' + CAST( @Details AS VARCHAR(MAX) ) 

--		Update the Rate for the Timesheet entry to the rate stored against the job.

UPDATE			[Record]
SET				[V005] = ISNULL( @Rate, 0 )
				,[V006] = @Details
WHERE			[Record].[RecordID] = @RecordID

RETURN @@ERROR
