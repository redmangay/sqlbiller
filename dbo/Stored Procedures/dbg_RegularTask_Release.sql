﻿

CREATE PROCEDURE [dbo].[dbg_RegularTask_Release] 
	@TaskID int,
	@StartDateTime datetime,
	@Message varchar(MAX)
AS
BEGIN TRY
	SET NOCOUNT ON;

	DECLARE @interval int
	SELECT @interval = [Interval] FROM [dbo].[RegularTask] WHERE [RegularTaskID] = @TaskID

	UPDATE [dbo].[RegularTask]
	SET [NextRun] = DATEADD(minute, @interval, @StartDateTime),
		[LastRun] = @StartDateTime,
		[LastRunResult] = @Message
	WHERE [RegularTaskID] = @TaskID

	INSERT INTO [dbo].[RegularTaskLog]
		([RegularTaskID], [StartTime], [EndTime], [ResultMessage])
	VALUES
		(@TaskID, @StartDateTime, GETDATE(), @Message)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_RegularTask_Release', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
