﻿

CREATE PROCEDURE [dbo].[spDataRetriever] 
	@sDataRetrieverName varchar(MAX),
	@nTableID int,
	@sSQL varchar(MAX)
AS

BEGIN TRY
	-- set these parameters
	-- SET @sSQL = 'SET DATEFORMAT DMY; SELECT CONVERT(varchar(16), MAX(CAST(V003 as date)), 103) FROM [Record] WHERE TableID = 1531 and V002 = #ID#'
	-- SET @sDataRetrieverName = 'Latest Medical CertIFicate'
	-- SELECT @nTableID = TableID FROM [Table] WHERE TableName = 'Incidents'

	IF EXISTS(SELECT * FROM DataRetriever WHERE DataRetrieverName = @sDataRetrieverName)
		UPDATE DataRetriever SET CodeSnippet = @sSQL WHERE DataRetrieverName = @sDataRetrieverName
	ELSE
		INSERT INTO DataRetriever (TableID, DataRetrieverName, CodeSnippet) 
			VALUES (@nTableID, @sDataRetrieverName, @sSQL)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spDataRetriever', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
