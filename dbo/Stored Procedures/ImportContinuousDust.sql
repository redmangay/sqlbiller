﻿

CREATE PROCEDURE [dbo].[ImportContinuousDust]

AS

BEGIN TRY
	--Temp. Config Table
	DECLARE @tvSitesInfo table(
		AccountID int NOT NULL,
		AccountName varchar(MAX),
		DustTableID int,
		SiteInfoTableID int,
		SiteID int,
		SiteName varchar(MAX),
		SiteNameOnImportCol varchar(MAX),
		SiteNameOnImport varchar(MAX));

	--Get All Necessary IDs
	INSERT  INTO @tvSitesInfo( AccountID,	AccountName,    DustTableID,	SiteInfoTableID,   SiteID, SiteName,    SiteNameOnImportCol)
		SELECT        t.AccountID, a.AccountName, t.DustTableID, s.SitesTableID, s1.SiteID, s1.SiteName, i.SystemName AS SiteNameOnImportCol  
	FROM            (SELECT        RecordID AS SiteID, TableID, V001 AS SiteName
							  FROM            Record
							  WHERE        (IsActive = 1)) AS s1 INNER JOIN
								 (SELECT        AccountID, AccountName
								   FROM            Account
								   WHERE        (IsActive = 1)) AS a INNER JOIN
								 (SELECT        TableID AS DustTableID, AccountID
								   FROM            [Table]
								   WHERE        (TableName = N'Real-time Dust') AND (IsActive = 1)) AS t ON a.AccountID = t.AccountID INNER JOIN
								 (SELECT        TableID AS SitesTableID, AccountID
								   FROM            [Table] AS Table_1
								   WHERE        (TableName LIKE N'%Site%Dust%') AND (IsActive = 1)) AS s ON t.AccountID = s.AccountID ON s1.TableID = s.SitesTableID LEFT OUTER JOIN
								 (SELECT        TableID, SystemName
								   FROM            [Column]
								   WHERE        (DisplayName = 'Site Name On Import File')) AS i ON s.SitesTableID = i.TableID

	--Get SiteNameOnImport value IF filled
	UPDATE @tvSitesInfo 
	SET SiteNameOnImport=(SELECT CASE 
	 WHEN t.SiteNameOnImportCol='V020' THEN V020 
	 WHEN t.SiteNameOnImportCol='V021' THEN V021 
	 WHEN t.SiteNameOnImportCol='V022' THEN V022 
	 WHEN t.SiteNameOnImportCol='V023' THEN V023 
	 WHEN t.SiteNameOnImportCol='V024' THEN V024 
	 WHEN t.SiteNameOnImportCol='V025' THEN V025 
	 WHEN t.SiteNameOnImportCol='V026' THEN V026 
	 WHEN t.SiteNameOnImportCol='V027' THEN V027 
	 WHEN t.SiteNameOnImportCol='V028' THEN V028 
	 WHEN t.SiteNameOnImportCol='V029' THEN V029 
	 ELSE NULL END 
	FROM Record WHERE (RecordID = t.SiteID))  from @tvSitesInfo t;

	--debug
	--SELECT * from @tvSitesInfo;

	WITH ContinuousDustData(DustTableID, AccountID, SiteID, SiteName, AccountName, StatDate,  PM10MC, PM25MC) AS 
	(SELECT    distinct    DustTableID, AccountID, SiteID, SiteName, AccountName, StatDate,  PM10MC, PM25MC
	FROM            (SELECT        t.DustTableID, t.AccountID, t.SiteID, COALESCE(t.SiteNameOnImport, t.SiteName) as SiteName, t.AccountName, xcnemd_upload.dbo.ContinuousDust.StatDate, 
							 xcnemd_upload.dbo.ContinuousDust.PM10MC, xcnemd_upload.dbo.ContinuousDust.PM25MC
	FROM            @tvSitesInfo t LEFT OUTER JOIN
							 xcnemd_upload.dbo.ContinuousDust ON REPLACE(COALESCE(t.SiteNameOnImport, t.SiteName),CHAR(9),'') = xcnemd_upload.dbo.ContinuousDust.UnitId + '-' + xcnemd_upload.dbo.ContinuousDust.ModId) 
							 AS tmp
	WHERE        (StatDate IS NOT NULL))

	INSERT INTO Record
							 (TableID, Notes, EnteredBy, V001, V002, V003, V004)
		SELECT        c.DustTableID, 'Imported from xcnemd_upload', 61, c.SiteID, CONVERT(nvarchar(MAX), CONVERT(smalldatetime, c.StatDate, 102), 103), 
			CASE WHEN c.PM10MC='-99' THEN NULL ELSE c.PM10MC END,
			CASE WHEN c.PM25MC='-99' THEN NULL ELSE c.PM25MC END
		 FROM            ContinuousDustData AS c LEFT OUTER JOIN
								  Record AS r ON c.DustTableID = r.TableID AND CAST(c.SiteID AS nvarchar(MAX)) = r.V001 AND CONVERT(nvarchar(MAX), CONVERT(smalldatetime, c.StatDate, 102), 
								  103) = r.V002
		 WHERE        (r.V001 IS NULL)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ImportContinuousDust', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
