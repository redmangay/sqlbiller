﻿

CREATE PROCEDURE   [dbo].[ets_Table_Insert]
(
	@nNewID int output,
	@sTableName nvarchar(255) ,
	@nParentMenuID int  = NULL,
	@bIsActive bit  = 1,
	@iAccountID int=NULL,
	@sPinImage varchar(255)=NULL,
	@dMaxTimeBetweenRecords decimal(20,10)=NULL,
	@sMaxTimeBetweenRecordsUnit varchar(50)=NULL,
	@nLateDataDays int=NULL
		

)
	/*----------------------------------------------------------------
	EXEC ets_Column_Insert  NULL, 14,'Sys Name',0,'Summary','Detaill','import name','export name',NULL,'validation import','validation Export'

	EXEC  ets_Table_Insert @nNewID=NULL,@sTableName='Testing ST3',@iAccountID=24793
	
	---------------------------------------------------------------*/
 
AS

BEGIN TRY

	BEGIN TRANSACTION --beginning a transaction..
     
		--DECLARE @nDisplayOrder INT
		--SET @nDisplayOrder =0

		--SELECT @nDisplayOrder=MAX(DisplayOrder) FROM [Menu] WHERE ParentMenuID=@nParentMenuID

		--IF @nDisplayOrder IS NULL
		--	SET @nDisplayOrder=0

		--IF @nDisplayOrder IS NOT NULL
		--	SET @nDisplayOrder=@nDisplayOrder+1
		
		
     
		INSERT INTO [Table]
		(
			TableName, 
			IsActive,AccountID,PinImage,
			MaxTimeBetweenRecords,MaxTimeBetweenRecordsUnit,LateDataDays
			
		) VALUES (
			@sTableName,
			@bIsActive,@iAccountID,@sPinImage,
			@dMaxTimeBetweenRecords,@sMaxTimeBetweenRecordsUnit,@nLateDataDays
			
		)
		SELECT @nNewID = @@IDENTITY

		--@nNewID int output,	@sMenu nvarchar(255) ,	@nAccountID int ,	@bShowOnMenu bit ,
		--@bIsActive bit  = 1,	@nParentMenuID int =NULL,	@nTableID int=NULL
		EXEC ets_Menu_Insert NULL,@sMenu=@sTableName,@nAccountID=@iAccountID,@bShowOnMenu=1,@bIsActive=1,@nParentMenuID=@nParentMenuID,@nTableID=@nNewID
		--Insert Standard columns for this Record Type
						--@nNewID,@nTableID,@nDisplayOrder,@sSystemName,@sDisplayTextSummary,@sDisplayTextDetail,
						--,@nGraphTypeID,@sValidationOnWarning,@sValidationOnEntry,
						--@bIsStandard, @sDisplayName, @nPositionOnImport, @sGraphLabel, @sConstant, @sCalculation, @bShowTotal
		DECLARE @nCID INT
		EXEC ets_Column_Insert NULL, @nTableID= @nNewID,@sSystemName='RecordID',@nDisplayOrder=0,@sDisplayName='Record ID',@bIsStandard=1
		EXEC ets_Column_Insert  NULL,@nTableID=  @nNewID,@sSystemName='DateTimeRecorded',@nDisplayOrder=1,@sDisplayName='Date Time Recorded',@bIsStandard=1,@sColumnType='datetime',@sDefaultValue='yes'
		EXEC ets_Column_Insert  NULL,@nTableID=  @nNewID,@sSystemName='TableID',@nDisplayOrder=2,@sDisplayName='Table',@bIsStandard=1
		EXEC ets_Column_Insert  NULL, @nTableID= @nNewID,@sSystemName='EnteredBy',@nDisplayOrder=3,@sDisplayName='Entered By',@bIsStandard=1
		EXEC ets_Column_Insert  NULL, @nTableID= @nNewID,@sSystemName='IsActive',@nDisplayOrder=4,@sDisplayName='Is Active',@bIsStandard=1
		--EXEC ets_Column_Insert  NULL, @nNewID,'Notes',6,'Notes','Notes',NULL,NULL,NULL,NULL,NULL,1,'Notes',NULL
		
		EXEC  dbg_TableTab_Insert NULL,@nTableID=@nNewID,@sTabName='Page 1',@nDisplayOrder=0
		
		--UPDATE [Column] SET ColumnType='datetime',DefaultValue='yes'
		--WHERE TableID=@nNewID AND SystemName='DateTimeRecorded'
		--MobileName='Date Time Recorded',
		
	COMMIT TRANSACTION --finally, Commit the transaction as all Success.

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Table_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
