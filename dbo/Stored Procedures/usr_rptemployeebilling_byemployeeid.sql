﻿--stored procedure for employeeid  
CREATE PROCEDURE [dbo].[usr_rptemployeebilling_byemployeeid]     
@empid varchar(50) -- ='77'  
AS    
  
BEGIN    
SELECT   
BillingInformation.[Project Number] as ProjectNumber,  
 EmployeeHistroy.EmployeeID AS EmployeeId,   
    EmployeeHistroy.[Group/Division] AS GroupDivision,   
    BillingInformation.[Invoice No] as InvoiceNo,   
    BillingInformation.Date  AS InvoiceDate,   
    Project.ProjectTitle, Project.Controller,   
    EmployeeHistroy.Name as Employee,  
    Sum(Cast(isnull(TimeSheet.[$_Amount],0) as Money)) AS TimeSheetDollar,   
    sum(CAST(CASE WHEN ISNUMERIC(TimeSheet.[$_Billed]) = 1  
                 THEN TimeSheet.[$_Billed]  
                 ELSE NULL END AS MONEY)) AS BilledDollar  
                    
     into #tempbilling from  
   
    (Account24918.qryEmployeeHistory as qryEmployeeHistory   
  INNER JOIN [Account24918].vEmployeeHistroy as EmployeeHistroy ON   
    (qryEmployeeHistory.MaxOfDate  = EmployeeHistroy.Date ) AND   
    (qryEmployeeHistory.EmployeeID = EmployeeHistroy.EmployeeID))   
    INNER JOIN (( [Account24918].vProject as Project INNER JOIN   
    ([Account24918].vTimeSheet as  TimeSheet   
  INNER JOIN [Account24918].[vBilling Information] as BillingInformation   
  ON TimeSheet.InvoiceNo = BillingInformation.[Invoice No])   
    ON Project.ProjectNumber = BillingInformation.[Project Number])   
    INNER JOIN [Account24918].vPNEmployeeIDTimeSheetID as   
    PNEmployeeIDTimeSheetID ON (TimeSheet.TimeSheetID = PNEmployeeIDTimeSheetID.TimeSheetID)   
    AND (BillingInformation.[Project Number] = PNEmployeeIDTimeSheetID.[ProjectNumber]))   
    ON qryEmployeeHistory.EmployeeID = PNEmployeeIDTimeSheetID.EmployeeID   
   -- WHERE  
  --      BillingInformation.Type = 1 AND cast(EmployeeHistroy.EmployeeID as varchar(50))= @empid   
          
    GROUP BY   
    BillingInformation.[Project Number], EmployeeHistroy.EmployeeID,   
   EmployeeHistroy.[Group/Division], Project.Controller, BillingInformation.[Invoice No],   
   BillingInformation.Date, Project.ProjectTitle, Project.Controller, EmployeeHistroy.Name  
  --------------select temp teble and appy date cond.  
  select * from #tempbilling  
  where EmployeeId = @empid 
    
  --drop temporary table  
  Drop table #tempbilling  
    
END 