﻿

CREATE PROCEDURE [dbo].[ets_ReportSchedule_Update] 
	@nReportID int,
	@nScheduledFrequency int,
	@dtNextRunDateTime datetime = NULL,
	@sScheduleOptions varchar(MAX) = NULL

AS

BEGIN TRY
	SET NOCOUNT ON;

	UPDATE [dbo].[Report]
		SET
			ScheduledFrequency = @nScheduledFrequency,
			NextRunDateTime = @dtNextRunDateTime,
			ScheduleOptions = @sScheduleOptions
		WHERE ReportID = @nReportID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_ReportSchedule_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
