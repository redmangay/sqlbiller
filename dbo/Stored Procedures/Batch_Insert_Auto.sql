﻿CREATE PROCEDURE dbo.Batch_Insert_Auto
(
	@TableID int,
	@BatchID int OUTPUT,
	@AccountID int OUTPUT,
	@MissingData bit = 0
)
AS
	/* Modification History
	JB	04 Jul 2017 :	Added SET @MissingData = ISNULL(@MissingData,0)
	JB	31 Oct 2018	:	Updated the SVN version
	*/

BEGIN TRY

	DECLARE @TableName VARCHAR(100)
	SELECT @AccountID=AccountID,@TableName=TableName FROM [Table] WHERE TableID=@TableID
	PRINT CAST(@AccountID AS VARCHAR)
	DECLARE @AccUserID int =[dbo].[fnGetAccountHolderUserID] (@AccountID)	--Account Holder
--	DECLARE @BatchID int

	SET @MissingData = ISNULL(@MissingData,0)

	DECLARE @myid uniqueidentifier = NEWID();  

	INSERT INTO [Batch]
		(
			TableID, 
			BatchDescription, 
			UploadedFileName, 
			DateAdded, 
			UniqueName, 
			UserIDUploaded, 
			AccountID,
			  AutoProcess
		) VALUES (
			@TableID,
			@TableName + ' Auto - ' +  CAST(Getdate() AS VARCHAR(20)) + CASE WHEN @MissingData = 1 THEN ' (No Data Found)' ELSE '' END,
			'NA',
			GETDATE(),
			@myid,
			@AccUserID,
			@AccountID,
			1
		) 

	SELECT @BatchID = @@IDENTITY

	IF @MissingData = 0
		EXEC [Batch_Validation_Count_Update] @BatchID=@BatchID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Batch_Insert_Auto', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
