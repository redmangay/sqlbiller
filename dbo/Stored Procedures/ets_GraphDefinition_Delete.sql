﻿

CREATE PROCEDURE [dbo].[ets_GraphDefinition_Delete]
(
	@nGraphDefinitionID int 
)
 
AS


--BEGIN TRY
--	BEGIN TRANSACTION --beginning a transaction..

--	COMMIT TRANSACTION --finally, Commit the transaction as all Success.

--END TRY

--BEGIN CATCH
--	ROLLBACK TRANSACTION --RollBack Transaction as some where error happend.
--	return 2
--END CATCH
BEGIN TRY
	DECLARE @x int
	SELECT @x = COUNT(GraphDefinitionID) FROM GraphDefinition
		WHERE GraphDefinitionID = @nGraphDefinitionID AND IsSystem = 1
	IF @x > 0
		RETURN -2
	ELSE
	BEGIN
		UPDATE GraphDefinition SET IsActive = 0 WHERE GraphDefinitionID = @nGraphDefinitionID
		RETURN 0
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_GraphDefinition_Delete', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
