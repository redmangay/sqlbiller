﻿CREATE PROCEDURE [dbo].[Renzo_PrintTimesheetOrDisbursement]
(
	@EmployeeRecordID  int ,
	@DateFrom date,
	@DateTo date,
	@Report varchar(10)  		
	
)
AS
BEGIN
--DECLARE @RecordIDx int =  12943690
 /*   
 
 exec [dbo].[Renzo_PrintTimesheetOrDisbursement]   12911831,'01/01/2018','31/01/2018',1


 DECLARE @t time = '12:10:30.123';   
SELECT DATENAME(year, @t);  


SELECT Left(DATENAME(dw,'29/10/2018'),3) 
--becomes
SELECT DATENAME(dw,'09-23-2013') 

 */


 SET DATEFORMAT dmy

 DECLARE @Data Table (
	EmployeeName varchar(max),
	Designation varchar(max),
	GroupDivistion varchar(max),
	DateFrom date,
	DateTo date,
	[Date] date,
	[Day] varchar(100),
	[DateTime] datetime,
	JobNo varchar(max),
	Item varchar(max),
	Details varchar(max),
	[X] varchar(10),
	Rate money,
	StartTime varchar(max),
	EndTime varchar(max),
	Unit varchar(10),
	Cost money,
	[Hours] money,
	[Hours Job] money,
	IsAdmin varchar(10),
	IsAtWork varchar(10),
	IsAtWorkHours money

 )

 

 
 IF @Report = 1
 BEGIN
 INSERT INTO @Data (
 	EmployeeName ,
	Designation ,
	GroupDivistion ,
	DateFrom ,
	DateTo ,
	[Date] ,
	[Day],
	[DateTime],
	JobNo,
	Item ,
	Details ,
	[X] ,
	Rate ,
	StartTime ,
	EndTime ,
	Unit ,
	Cost,
	[Hours],
	[Hours Job],
	IsAdmin,
	IsAtWork,
	IsAtWorkHours
 )
	SELECT	 LTRIM(RTRIM([Employee].V003)) as Name,
			 [Designation].V001 as Designation, 
			 [Group].V001 as [Group Division], 
			 @DateFrom as [Start Date],
			 @DateTo as [End Date],
			 [Timesheet].V004 as [Date],
			 LEFT(DATENAME(dw,CAST([Timesheet].V004 as date)),3) as [Day],
			 CAST([Timesheet].V004 + ' ' +  ISNULL([Timesheet].V013,'00:00') as datetime),
			 [Timesheet].V043 as [Job No],
			 NULL,
			 [Timesheet].V006 as Details,
			 [Timesheet].V007 as [X],
			 CAST(ISNULL([Timesheet].V009,0) as money) as [Rate],
			 [Timesheet].V013 as [Start Time],
			 [Timesheet].V014 as [End Time],
			 NULL,
			 NULL,
			 CAST([Timesheet].V015 as money) as [Hours],
			 CASE WHEN [Timesheet].V041 = '2' THEN  CAST([Timesheet].V015 as money)  ELSE 0 END as [Hours Job],
			 [Timesheet].V041,
			 CASE WHEN [Timesheet].V041 = '1' THEN 
				CASE WHEN [Administration].V004 = '2717597' THEN 'Yes' ELSE 'No' END ELSE 'Yes' END as IsAtWork,
			 CASE WHEN [Timesheet].V041 = '1' THEN 
				CASE WHEN [Administration].V004 = '2717597' THEN  CAST([Timesheet].V015 as money)  ELSE 0 END ELSE CAST([Timesheet].V015 as money) END as IsAtWorkHours
	   FROM Record [Employee] 
	   JOIN Record [Designation] ON [Designation].TableID = 2726
		  AND [Designation].IsActive = 1
		  AND [Designation].RecordID = CAST([Employee].V021 as int)
	   JOIN Record [Timesheet] ON [Timesheet].TableID = 2488
		  AND [Timesheet].IsActive = 1
		  AND [Timesheet].V017 = [Employee].RecordID
		  AND CAST([Timesheet].V004 as date) >= @DateFrom
		  AND CAST([Timesheet].V004 as date) <= @DateTo
	
	    LEFT JOIN Record [Timesheet Details] ON [Timesheet Details].TableID = 2729
		  AND [Timesheet Details].IsActive = 1
		  AND CAST([Timesheet Details].V011 as int) = [Timesheet].RecordID
		  AND [Timesheet Details].V022 <> '3'
	
	  LEFT JOIN Record [Job Information] ON [Job Information].TableID = 2475
		  AND [Job Information].IsActive = 1
		  AND [Job Information].RecordID = CAST([Timesheet].V018 as int)

	  LEFT JOIN Record [Administration] ON [Administration].TableID = 2650
		  AND [Administration].IsActive = 1
		  AND [Administration].RecordID = CAST([Timesheet].V042 as int)

	   JOIN Record [Group] ON [Group].TableID = 2727
		  AND [Group].RecordID = CAST([Employee].V022 as int)
	   WHERE [Employee].TableID = 2669
		  AND [Employee].IsActive = 1
		  AND [Employee].RecordID = @EmployeeRecordID

END


 IF @Report = 2
 BEGIN
 INSERT INTO @Data (
 	EmployeeName ,
	Designation ,
	GroupDivistion ,
	DateFrom ,
	DateTo ,
	[Date] ,
	[Day],
	[DateTime],
	JobNo,
	Item ,
	Details ,
	[X] ,
	Rate ,
	StartTime ,
	EndTime ,
	Unit ,
	Cost,
	[Hours],
	[Hours Job],
	IsAdmin,
	IsAtWork,
	IsAtWorkHours 
 )
	SELECT	 LTRIM(RTRIM([Employee].V003)) as Name,
			 [Designation].V001 as Designation, 
			 [Group].V001 as [Group Division], 
			 @DateFrom as [Start Date],
			 @DateTo as [End Date],
			 [Disbursement].V002 as [Date],
			 LEFT(DATENAME(dw,CAST([Disbursement].V002 as date)),3) as [Day],
			 NULL,
			 [Disbursement].V032 as [Job No],
			 [Disbursement].V003 as Item,
			 [Disbursement].V006 as Details,
			 NULL,
			 CAST(ISNULL([Disbursement].V005,0) as money) as [Rate],
			 NULL,
			 NULL,
			 CAST([Disbursement].V004 as money) as [Unit],
			 CAST(ISNULL([Disbursement].V007,0) as money) as [Cost],
			 NULL,
			 NULL,
			 [Disbursement].V030,
			 CASE WHEN [Administration].V004 = '2717597' THEN 'Yes' ELSE 'No' END as IsAtWork,
			 NULL
	   FROM Record [Employee] 
	   JOIN Record [Designation] ON [Designation].TableID = 2726
		  AND [Designation].IsActive = 1
		  AND [Designation].RecordID = CAST([Employee].V021 as int)
	   JOIN Record [Disbursement] ON [Disbursement].TableID = 2725
		  AND [Disbursement].IsActive = 1
		  AND [Disbursement].V019 = [Employee].RecordID
		  AND CAST([Disbursement].V002 as date) >= @DateFrom
		  AND CAST([Disbursement].V002 as date) <= @DateTo
	  LEFT JOIN Record [Job Information] ON [Job Information].TableID = 2475
		  AND [Job Information].IsActive = 1
		  AND [Job Information].RecordID = CAST([Disbursement].V001 as int)

	  LEFT JOIN Record [Administration] ON [Administration].TableID = 2650
		  AND [Administration].IsActive = 1
		  AND [Administration].RecordID = CAST([Disbursement].V031 as int)

	   JOIN Record [Group] ON [Group].TableID = 2727
		  AND [Group].RecordID = CAST([Employee].V022 as int)
	   WHERE [Employee].TableID = 2669
		  AND [Employee].IsActive = 1
		  AND [Employee].RecordID = @EmployeeRecordID		    
		  
END



IF((SELECT COUNT(*)  FROM @Data)>0)
BEGIN
    IF(@Report = 1)
    BEGIN
      SELECT *,
		   (SELECT OptionValue FROM SYSTEMOPTION WHERE OPTIONKEY = 'FilesLocation') + '/UserFiles/AppFiles/' + 
		   (SELECT V027 FROM Record WHERE TableID = 2744 AND IsActive = 1 AND V005 = (SELECT V022 FROM Record WHERE RecordID = @EmployeeRecordID) )  AS HeaderImage,
		   @Report ReportType
		   FROM @Data
		  ORDER BY [DateTime],JobNo
    END
    ELSE
    BEGIN
      SELECT *,
		   (SELECT OptionValue FROM SYSTEMOPTION WHERE OPTIONKEY = 'FilesLocation') + '/UserFiles/AppFiles/' + 
		   (SELECT V027 FROM Record WHERE TableID = 2744 AND IsActive = 1 AND V005 = (SELECT V022 FROM Record WHERE RecordID = @EmployeeRecordID) )  AS HeaderImage,
		   @Report ReportType
		   FROM @Data
		  ORDER BY CAST([Date] as date)
    END
		

END
ELSE
BEGIN
SELECT ''EmployeeName ,
	''Designation ,
	''GroupDivistion ,
	''DateFrom ,
	''DateTo ,
	''[Date] ,
	''[Day],
	''[DateTime],
	''JobNo,
	''Item ,
	''Details ,
	''[X] ,
	''Rate ,
	''StartTime ,
	''EndTime ,
	''Unit ,
	''Cost,
	''[Hours],
	''[Hours Job],
	''IsAdmin ,
	''IsAtWork,
 (SELECT OptionValue FROM SYSTEMOPTION WHERE OPTIONKEY = 'FilesLocation') + '/UserFiles/AppFiles/' + 
(SELECT V027 FROM Record WHERE TableID = 2744 AND IsActive = 1 AND V005 = (SELECT V022 FROM Record WHERE RecordID = @EmployeeRecordID) )  AS HeaderImage,
 @Report ReportType
 FROM @Data
END



RETURN @@ERROR;

END