﻿
create procedure usr_rptjobbidsuccess_view
as

IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##rptJobBidSucces' )) DROP TABLE ##rptJobBidSucces


create table ##rptJobBidSucces ( GroupDivision varchar(max),[Date] varchar(max),  Guid       uniqueidentifier,   Controller varchar(max),   JobNo  varchar(max) ,   Title   varchar(max) ,
   JobValue  float ,   IntStatus varchar(max) ,   [Status] varchar(4),   WonAmount float,   [Convert] varchar(1),   RecordedDate datetime,   EmployeeID varchar(max))

   insert into ##rptJobBidSucces
    SELECT Project.[Group/Division] AS GroupDivision

      , Project.[Date]
      
       ,null 

      , Employee_List.Initials As Controller

      , Project.ProjectNumber AS JobNo, Project.ProjectTitle AS Title

     
      ,ISNULL(dbo.ConvertToGroupCurrency(CAST(REPLACE(REPLACE(isnull(Project.Value, 0), ',', ''), '$', '') AS float) ,  CAST(REPLACE(REPLACE(isnull(Project.CurrencyRate, 0), ',', ''), '$', '') AS float), CAST(REPLACE(REPLACE(isnull(Project.RptCurrencyRate, 0), ',', ''), '$', '') AS float)) ,0) AS JobValue

      , Project.Status AS IntStatus

      , CASE WHEN Project.Status NOT IN(1,3) THEN 'Won' ELSE CASE WHEN Project.Status=3 THEN 'Lost' ELSE '' END END AS Status

      , CASE WHEN Project.Status NOT IN(1,3) THEN ISNULL(dbo.ConvertToGroupCurrency(CAST(REPLACE(REPLACE(isnull(Project.Value, 0), ',', ''), '$', '') AS float) ,  CAST(REPLACE(REPLACE(isnull(Project.CurrencyRate, 0), ',', ''), '$', '') AS float), CAST(REPLACE(REPLACE(isnull(Project.RptCurrencyRate, 0), ',', ''), '$', '') AS float)) ,0) ELSE 0 END AS WonAmount

      , CASE WHEN Project.Currency IS NULL THEN 'C' ELSE '' END AS [Convert]

      , GETDATE() AS RecordedDate

      , Project.EmployeeID

FROM (Account24918.[vProject] as Project INNER JOIN Account24918.vSubsidiary as Client ON Client.ClientID = Project.ClientID)

      left JOIN Account24918.[vEmployee_List]Employee_List ON Project.EmployeeID = Employee_List.EmployeeID
  
  
