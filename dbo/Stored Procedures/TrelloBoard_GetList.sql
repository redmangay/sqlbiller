﻿

-- =============================================
-- Author:		DH
-- Create date: 15 Aug 2017
-- Description:	Get all boards info
-- =============================================
CREATE PROCEDURE [dbo].[TrelloBoard_GetList] 	
AS
BEGIN TRY
	SET NOCOUNT ON;    
	
	SELECT	RefId As Id,
			[Name],
			ShortLink,
			DateLastActivity
	FROM	TrelloBoard	
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('TrelloBoard_GetList', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
