﻿

CREATE PROCEDURE [dbo].[Report_DataView_Demo] 
(
	@AccountID int,
	@UserID int,
	@ReportID int = NULL,
	@sType varchar(50) = NULL
)
AS

BEGIN TRY
	SET NOCOUNT ON;

	DECLARE @ViewColumn TABLE
	(
		ViewColumnID int NOT NULL IDENTITY(1, 1),
		DisplayName varchar(200) NOT NULL,
		ColumnType varchar(15) NOT NULL,
		IsRound bit NOT NULL DEFAULT 0,
		RoundNumber int NOT NULL DEFAULT 2
	)

	-- Set up columns
	INSERT INTO @ViewColumn (DisplayName, ColumnType) VALUES ('Date / Time Sampled', 'datetime')
	INSERT INTO @ViewColumn (DisplayName, ColumnType) VALUES ('Sample Site', 'text')
	INSERT INTO @ViewColumn (DisplayName, ColumnType) VALUES ('Numeric Value w/o round', 'number')
	INSERT INTO @ViewColumn (DisplayName, ColumnType, IsRound, RoundNumber) VALUES ('Numeric Value rounded (0 digits)', 'number', 1, 0)
	INSERT INTO @ViewColumn (DisplayName, ColumnType, IsRound, RoundNumber) VALUES ('Numeric Value rounded (1 digits)', 'number', 1, 1)
	INSERT INTO @ViewColumn (DisplayName, ColumnType, IsRound, RoundNumber) VALUES ('Numeric Value rounded (2 digits)', 'number', 1, 2)
	INSERT INTO @ViewColumn (DisplayName, ColumnType, IsRound, RoundNumber) VALUES ('Numeric Value rounded (3 digits)', 'number', 1, 3)
	INSERT INTO @ViewColumn (DisplayName, ColumnType, IsRound, RoundNumber) VALUES ('Numeric Value rounded (4 digits)', 'number', 1, 4)
	INSERT INTO @ViewColumn (DisplayName, ColumnType) VALUES ('Report Period from (no time part)', 'date')
	INSERT INTO @ViewColumn (DisplayName, ColumnType) VALUES ('Report Period to (no time part)', 'date')

	-- Report period
	DECLARE @ReportPeriodFrom datetime
	DECLARE @ReportPeriodTo datetime

	IF @ReportID IS NOT NULL
		SELECT @ReportPeriodFrom = DateFrom, @ReportPeriodTo = DateTo FROM [dbo].[GetReportDates](@ReportID)

	-- Demo data source
	DECLARE @DemoData TABLE
	(
		DateTimeColumn datetime,
		TextColumn varchar(100),
		NumericColumnn1 decimal (18, 5),
		NumericColumnn2 decimal (18, 5),
		NumericColumnn3 decimal (18, 5),
		NumericColumnn4 decimal (18, 5),
		NumericColumnn5 decimal (18, 5),
		NumericColumnn6 decimal (18, 5),
		DateColumn1 datetime,
		DateColumn2 datetime
	)

	DECLARE @Counter int = 0
	DECLARE @NRows int = 100
	WHILE @Counter < @NRows
	BEGIN
		INSERT INTO @DemoData
			(DateTimeColumn, TextColumn, NumericColumnn1, NumericColumnn2, NumericColumnn3, NumericColumnn4, NumericColumnn5, NumericColumnn6, DateColumn1, DateColumn2)
		VALUES
			(DATEADD(DAY, -1 * @Counter, GETDATE()), 'Sample Site', RAND() * 1, RAND() * 3, RAND() * 10, RAND() * 5, RAND() * 20, RAND() * 33, @ReportPeriodFrom, @ReportPeriodTo)
	   SET @Counter = @Counter + 1;
	END

	IF @sType = 'Header' OR @sType = 'Full'
		SELECT * FROM @ViewColumn ORDER BY ViewColumnID

	IF @sType = 'Data' OR @sType = 'Full'
		SELECT -- View data here
			DateTimeColumn, TextColumn, NumericColumnn1, NumericColumnn2, NumericColumnn3, NumericColumnn4, NumericColumnn5, NumericColumnn6, DateColumn1, DateColumn2
		FROM @DemoData
		WHERE DateTimeColumn BETWEEN @ReportPeriodFrom AND @ReportPeriodTo

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Report_DataView_Demo', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
