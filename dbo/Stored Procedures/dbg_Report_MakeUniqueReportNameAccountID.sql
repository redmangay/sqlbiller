﻿

 CREATE PROCEDURE [dbo].[dbg_Report_MakeUniqueReportNameAccountID]

	/*----------------------------------------------------------------
	
SELECT AccountID, ReportName, count(*) as qty 
 FROM [Report] 
 GROUP BY AccountID, ReportName HAVING count(*)> 1
 

SELECT * FROM [Report] WHERE AccountID=2385 AND ReportName='1st ED Arrival Summary'
 
EXEC [dbo].[dbg_Report_MakeUniqueReportNameAccountID]
	---------------------------------------------------------------*/
 
AS
 
BEGIN TRY  
	DECLARE Cur_UniqueReportNameAccountID CURSOR FOR
	SELECT AccountID, ReportName, count(*) as Qty 
	FROM [Report] 
	 GROUP BY AccountID, ReportName HAVING count(*)> 1;
 
	DECLARE @nAccountID INT;
	DECLARE @sReportName varchar(500);
	DECLARE @nQty INT;
	DECLARE @sNewReportName varchar(500);

	OPEN Cur_UniqueReportNameAccountID;
	FETCH NEXT FROM Cur_UniqueReportNameAccountID INTO @nAccountID,@sReportName,@nQty;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
	  
	  
			DECLARE Cur_ReportID CURSOR FOR
			SELECT ReportID FROM [Report] WHERE AccountID=@nAccountID AND ReportName=@sReportName;
			DECLARE @nReportID INT;
		
			OPEN Cur_ReportID;
			FETCH NEXT FROM Cur_ReportID INTO @nReportID;
			WHILE @@FETCH_STATUS = 0
			   BEGIN
			  
				  SELECT @sNewReportName=dbo.fnNextAvailableReportName(@sReportName,@nAccountID,@nReportID);
			  
				  IF @sReportName<>@sNewReportName
				  UPDATE [Report]
				  SET ReportName=@sNewReportName
				  WHERE ReportID=@nReportID
			 			  
				  FETCH NEXT FROM Cur_ReportID INTO @nReportID;
			   END;
			CLOSE Cur_ReportID;
			DEALLOCATE Cur_ReportID;  
	 
		  FETCH NEXT FROM Cur_UniqueReportNameAccountID INTO @nAccountID,@sReportName,@nQty;
	   END;
	CLOSE Cur_UniqueReportNameAccountID;
	DEALLOCATE Cur_UniqueReportNameAccountID;
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_Report_MakeUniqueReportNameAccountID', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
