﻿-- =============================================
-- Author:		<Red Mangay>
-- Create date:	<10/07/2018>
-- Description:	<Get the tableid of the orginal table ie timesheet or disbursement>
-- =============================================
/*

exec [GetTimeSheetTableIDFrom] 2713164

*/


CREATE PROCEDURE [dbo].[Renzo_CheckInvoiceNumber]
(
	@RecordID int,
	@param001 VARCHAR(MAX),
	@param002 VARCHAR(MAX)
	
)
AS
/*

EXEC [dbo].[Renzo_CheckInvoiceNumber]  2716410,'J110INV1'

*/

BEGIN

DECLARE @BAPA VARCHAR(MAX) = @param002
DECLARE @JobNo varchar(max) = @RecordID

--SELECT @BAPA = V029,@JobNo = V002 FROM [Record]
--    WHERE RecordID = @RecordID

IF (@BAPA IN ('Invoice','Interim Invoice'))
BEGIN
    SELECT * FROM [RECORD]
    WHERE [V017] =  @param001
    AND V002 = @JobNo
    AND ISACTIVE = 1
    AND V029 IN ('Invoice','Interim Invoice')
END
ELSE IF (@BAPA IN ('Overpaid Entry'))
BEGIN
    SELECT * FROM [RECORD]
    WHERE [V017] =  @param001
    AND V002 = @JobNo
    AND ISACTIVE = 1
    AND V029 IN ('Overpaid Entry')
END
ELSE IF (@BAPA IN ('Bad Debt Entry'))
BEGIN
    SELECT * FROM [RECORD]
    WHERE [V017] =  @param001
    AND V002 = @JobNo
    AND ISACTIVE = 1
    AND V029 IN ('Bad Debt Entry')
END
ELSE IF (@BAPA IN ('Bad Debt Recovery Entry'))
BEGIN
    SELECT * FROM [RECORD]
    WHERE [V017] =  @param001
    AND V002 = @JobNo
    AND ISACTIVE = 1
    AND V029 IN ('Bad Debt Recovery Entry')
END
ELSE IF (@BAPA IN ('Credit Entry','Interim Credit'))
    BEGIN
    SELECT * FROM [RECORD]
    WHERE [V046] =  @param001
    AND V002 = @JobNo
    AND ISACTIVE = 1
    AND V029 IN ('Credit Entry','Interim Credit')

END

END

