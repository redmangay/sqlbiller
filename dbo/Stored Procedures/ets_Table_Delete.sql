﻿

CREATE PROCEDURE [dbo].[ets_Table_Delete]
(
	@TableID int,
	@UserID int 
)
AS
BEGIN TRY
	UPDATE [Table] SET IsActive=0, LastUpdatedUserID = @UserID
		WHERE TableID = @TableID

	UPDATE MENU SET IsActive=0
		WHERE TableID=@TableID

	/****** Object:  StoredProcedure [dbo].[ets_Table_Select]    Script Date: 10/03/2014 15:34:07 ******/
	SET ANSI_NULLS ON
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Table_Delete', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
