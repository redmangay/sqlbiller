﻿CREATE PROCEDURE [dbo].[Kate_SaveTransfer]
(
	@RecordID int,
	@UserID int,
	@Return varchar(max) output
)
AS
-- 
-- Copy from the Notification table
BEGIN

	--SELECT *
	--	FROM [Record]
	--	JOIN [Record] N ON N.TableID = 2454  -- Join to Notification Form 
	--		AND N.RecordID = [Record].V026		 -- on Trauma Record Number
	--	WHERE [Record].RecordID = @RecordID


	UPDATE [Record] 
		SET V029= N.V015 
		FROM [Record]
		JOIN [Record] N ON N.TableID = 2454  -- Join to Notification Form 
			AND N.RecordID = [Record].V026		 -- on Trauma Record Number
		WHERE [Record].RecordID = @RecordID
END
