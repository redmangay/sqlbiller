﻿-- =============================================
-- Author:		<Barry Matutina>
-- Create date: <09/02/2016>
-- Description:	<Job number display in Job Information>
-- =============================================5
CREATE PROCEDURE [dbo].[buffalobiller1_UpdateViewInformation]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT		 [Job Information].[TableID]
				,[Job Information].RecordID	AS				[Record ID]
				,[Job Information].V002		AS				[Date]
				,[Job Information].V020		AS				[ClientID orig]
				,[Job Information].V024		AS				[ClientID]
				,[Job Information].V022		AS				[TLStatus]
				,[Job Information].V001		AS				[Job No]
				,[Job Information].V021		AS				[Status]
				,[Job Information].V025		AS				[On Hold]
				,[Job Information].V003		AS				[Title]
				,[Job Information].V004		AS				[Controller_old]
				,[Job Information].V026		AS				[Controller]
				,[Job Information].V019		AS				[Content Space]
				,[Job Information].V005		AS				[Group_old]
				,[Job Information].V027		AS				[Group]
				,[Job Information].V006		AS				[Currency]
				,[Job Information].V007		AS				[Currency Rate]
				,[Job Information].V008		AS				[Job value]
				,[Job Information].V009		AS				[Fixed value]
				,[Job Information].V010		AS				[Tax Name]
				,[Job Information].V011		AS				[Tax Rate%]
				,[Job Information].V012		AS				[Special Rates of Charge]
				,[Job Information].V018		AS				[Set Rates to Default]
				,[Job Information].V014		AS				[Our Reference]
				,[Job Information].V015		AS				[Your Reference]
				,[Job Information].V016		AS				[Details Attached]
				,[Job Information].V017		AS				[Attachment]
				,[Job Information].V023		AS				[RateBlankID]
				,[Job Information].V029		AS				[Group reporting currency / rate]
				,[Job Information].V028		AS				[Last job number]
				,[Job Information].V030		AS				[Refresh]
				,[Job Information].V032		AS				[Content Space 2]
				,[Job Information].V031		AS				[Standard rate of charge]
				,[Job Information].DateAdded
	FROM		[Record] [Job Information] WITH (NOLOCK)
	LEFT JOIN	Record [Group] ON					[Group].TableID = '2727'		/*	Group	*/
											AND		[Group].IsActive = 1
											AND		CAST( [Group].RecordID AS VARCHAR(MAX)) = [Job Information].V027		/*	Group	*/
	LEFT JOIN	Record [Controller] ON					[Controller].TableID = '2669'		/*	Employee_List	*/
											AND		[Controller].IsActive = 1
											AND		CAST( [Controller].RecordID AS VARCHAR(MAX)) = [Job Information].V026		/*	Controller	*/
	LEFT JOIN	Record [Office Name] ON					[Office Name].TableID = '2474'		/*	Office Information	*/
											AND		[Office Name].IsActive = 1
											AND		CAST( [Office Name].RecordID AS VARCHAR(MAX)) = [Job Information].V024		/*	Office Name	*/
	WHERE		[Job Information].TableID = '2475'		/*	Job Information	*/
	AND			[Job Information].IsActive = 1

END
