﻿CREATE PROC dbg_QuickLink_DetailWithRecordID
(
	@nQuickLinkID int,
	@CheckDigit int,
	@sURL varchar(MAX) OUTPUT
)
AS
BEGIN TRY

DECLARE @IsValid BIT

SELECT @IsValid = dbo.fnIsLuhnValid(@CheckDigit)

IF (@IsValid = 1)
	BEGIN
		SELECT @sURL = URL + '&RecordID=' + ISNULL(CAST(RecordID AS VARCHAR(50)), '0')
		FROM [dbo].[QuickLink]
		WHERE QuickLinkID = @nQuickLinkID			
	END	
ELSE
	BEGIN
		SET @sURL = ''
	END

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_QuickLink_DetailWithCheckDigit', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

