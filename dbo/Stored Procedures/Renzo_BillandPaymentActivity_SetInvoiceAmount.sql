﻿CREATE PROCEDURE		[dbo].[Renzo_BillandPaymentActivity_SetInvoiceAmount]
(
	@RecordID  int,
	@UserID int,
	@Return varchar(max) output
)
AS
BEGIN

--		% Complete (V028) * Job Value (V008) to get the % of the total job value to be billed in this invoice.

UPDATE			[Bill & Payment Activity] 			/*	[Bill & Payment Activity]	*/
	SET			V005 = CASE WHEN ISNUMERIC([Bill & Payment Activity].V028) = 1 THEN  [Bill & Payment Activity].V028/100	ELSE CAST(1 AS DEC(18,4)) END *
				CASE WHEN ISNUMERIC([Job Number].V008) = 1 THEN  [Job Number].V008	ELSE CAST(0 AS DEC(18,4)) END				/*	[Invoice Amount]	*/
	FROM		[Record] [Bill & Payment Activity]
	LEFT JOIN	Record [Job Number] ON					[Job Number].TableID = '2475'		/*	Job Information	*/
												AND		[Job Number].IsActive = 1
												AND		CAST( [Job Number].RecordID AS VARCHAR(MAX)) = [Bill & Payment Activity].V002		/*	Job Number	*/
	WHERE		[Bill & Payment Activity].TableID = '2711'		/*	Bill & Payment Activity	*/
	AND			[Bill & Payment Activity].IsActive = 1
	AND			[Bill & Payment Activity].RecordID = @RecordID;
END
