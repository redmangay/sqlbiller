﻿

 
CREATE PROCEDURE   [dbo].[Record_CleanData_ByColumnType_Split]
(
	@nS int,@nE int

)
	/*----------------------------------------------------------------
	EXEC [dbo].[Record_CleanData_ByColumnType_Split] 1,2
	---------------------------------------------------------------*/
 
AS
 
BEGIN


	DECLARE @sSQL VARCHAR(MAX)=NULL
		SELECT @sSQL=COALESCE(@sSQL+'','','''')+'UPDATE [Record] SET '+ SystemName + '=FORMAT(CAST('+ SystemName + ' AS DATETIME),''dd/MM/yyyy HH:mm:ss'') WHERE TableID=' + CAST(TableID AS VARCHAR)
	+ '  AND '+ SystemName + ' IS NOT NULL AND ISDATE('+ SystemName + ')=1; '
	FROM [Column] WHERE ColumnType='datetime' AND IsStandard=0 AND UPPER( SUBSTRING(SystemName,1,1))='V'
	AND TableID IN (SELECT TableID FROM zCleanData WHERE HasDateTime=1 AND ( SN>=@nS and SN<=@nE))
	
	DECLARE @sSQL2 VARCHAR(MAX)=NULL
		SELECT @sSQL2=COALESCE(@sSQL2+'','','''')+ 'UPDATE [Record] SET '+ SystemName + '=FORMAT(CAST(''01/01/2000 ''+ '+ SystemName + ' AS datetime),''HH:mm:ss'') WHERE TableID=' + CAST(TableID AS VARCHAR)
	+ '  AND '+ SystemName + ' IS NOT NULL AND ISDATE(''01/01/2000 ''+ '+ SystemName + ' )=1 ; '
	FROM [Column] WHERE ColumnType='time' AND IsStandard=0 AND UPPER( SUBSTRING(SystemName,1,1))='V'
	AND TableID IN (SELECT TableID FROM zCleanData WHERE  HasTime=1 AND ( SN>=@nS and SN<=@nE))

	IF @sSQL2 IS NULL
	 SET @sSQL2=''
	IF @sSQL IS NULL
	 SET @sSQL=''

	SET @sSQL=@sSQL+@sSQL2;

	PRINT @sSQL
	EXEC (@sSQL)
	

END


