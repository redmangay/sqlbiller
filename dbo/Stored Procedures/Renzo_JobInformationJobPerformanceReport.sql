﻿

CREATE PROCEDURE	[Renzo_JobInformationJobPerformanceReport]
(
     @RecordID		INT = NULL
)	
AS
BEGIN

SELECT			[Job Information].RecordID	AS				[JobRecordID]
				,[Job Information].V001		AS				[Job No]
				,[Job Information].V026		AS				[ControllerRecordID]
				,[Controller].V004			AS				[Initials]
				,[Job Information].V003		AS				[Title]
				,[Job Information].V008		AS				[Job value]
	FROM		Record [Job Information] WITH (NOLOCK)
	JOIN		Record [Group] WITH (NOLOCK) ON				[Group].TableID = '2727'			/*	Group	*/
											AND				[Group].IsActive = 1
											AND				CAST( [Group].RecordID AS VARCHAR(MAX)) = [Job Information].V027		/*	Group	*/
	JOIN		Record [Controller] WITH (NOLOCK) ON		[Controller].TableID = '2669'		/*	Employee_List	*/
											AND				[Controller].IsActive = 1
											AND				CAST( [Controller].RecordID AS VARCHAR(MAX)) = [Job Information].V026		/*	Controller	*/
	WHERE		[Job Information].TableID = '2475'		/*	Job Information	*/
	AND			[Job Information].IsActive = 1
	AND			(@RecordID IS NULL OR [Job Information].RecordID = @RecordID) 
	ORDER BY	[Job Information].V001;

END
