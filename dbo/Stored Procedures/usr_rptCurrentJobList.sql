﻿CREATE PROCEDURE [dbo].[usr_rptCurrentJobList]
(
	--@From DATE = '01/10/2018',
	--@To DATE = '30/10/2018',
	@Group VARCHAR(MAX) =null,
	@Controller VARCHAR(MAX) =null
	--@OrderByColumn INT =1
)
AS

/*

EXEC [dbo].[usr_rptCurrentJobList_23032020] @Group ='1', @Controller='1'


*/

BEGIN


DECLARE @CurrentJobsListBalances TABLE (
		  [Counter] int identity(1,1), 		
		  JobRecordID int,
		  BalanceOne money,
		  BalanceTwo money,
		  PaidUp varchar(10),
		  JobValue money
		  ) 


	   DECLARE @CurrentJobsList TABLE (
		  [Counter] int identity(1,1), 
		  JobRecordID int,
		  [Job Date] date, 
		  [Job Controller] varchar(max), 
		  [Job No] varchar(max), 
		  [Job Client] varchar(max), 
		  [Job Title] varchar(max), 
		  [Job Value] money, 
		  [Number of Bids] varchar(max), 
		  [Currency] varchar(max),
		  [Group Name] varchar(max),
		  [Converted Symbol] varchar(10),
		  [Converted Invoiced] money,
		  [Paid Up] int,
		    [Balance] money,
		  [Header] varchar(max),
		  CurrencyRate varchar(20),
		  GroupRate varchar(20),
		   JobCurrencyRate varchar(20),
		  JobGroupRate varchar(20)
		  )


DECLARE @SQL VARCHAR(MAX) = ''
DECLARE @SQLGroup VARCHAR(MAX) = ''
DECLARE @SQLController VARCHAR(MAX) = ''

IF @Group <> 1 AND @Group is not null
BEGIN
SET @SQLGroup = ' AND [Job Information].V027 =''' + @Group + ''''
END

IF @Controller <> 1 AND @Controller is not null
BEGIN
SET @SQLController = ' AND [Job Information].V026 =''' + @Controller + ''''
END



   
SET @SQL =	'
SELECT [Job Information].RecordID  as JobRecordID, [Job Information].[V002] AS [Job Date]
	   , ISNULL([EmployeeList_Job Controller].V003,'''') AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
	   , [Job Information].V001 AS [Job No]
	   ,ISNULL([Client].[V001],''-- NONE --'') AS [Job Client]
	   , ISNULL([Job Information].V003,''-- NONE --'') AS [Job Title] --Job Title, get from Job Information V003
	   , CASE WHEN [Currency Rates].V001 = [Job Group].V004 THEN CONVERT(MONEY,[Job Information].V008) ELSE
	   Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) END AS [Job Value]
	  
	   ,1 AS [Number of Bids]
	  --,[Currency Rates].V001 AS [Currency]	
	   ,[Reporting Currency].V001 AS [Currency]
	   ,ISNULL([Job Group].[V001],'''') AS [Group Name]
	   
	   ---------------------------------------------------------------------------------------	
	 /*  , [Job Notes].V003 AS [Note Date] 
	    , [EmployeeList_List].V004 AS [Initial]
	   , [Job Notes].V006 AS [Note]		*/
	   ---------------------------------------------------------------------------------------

	   ,CASE WHEN [Currency Rates].V001 = [Job Group].V004 THEN NULL WHEN [Currency Rates].V001 LIKE ''%None%'' THEN NULL  ELSE convert(varchar,''C'') END AS [Converted Symbol]
	
	
	---------------------------------------------------------------------------------------
	 /*  ,ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0) AS [Invoiced] */
     ---------------------------------------------------------------------------------------


	  , 0
	  --CASE WHEN [Currency Rates].V001 = [Job Group].V004 
	  --THEN Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			-- WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			--	AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2)
	  -- ELSE  ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			-- WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			--	AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			--  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0) END  AS [Converted Invoiced]
	  
	  --------------------------------------------------------------------------
	  /* ,ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			 AND [Bills And Payment Activity].V029 IN (''Invoice'',''Interim Invoice'')
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0) AS [Total Invoiced Amount] */

	/*    ,ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			 AND [Bills And Payment Activity].V029 IN (''Invoice'',''Interim Invoice'')
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  CONVERT(MONEY,[Job Information].V007) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0)  AS [Converted Total Invoiced Amount]  */

	 /*  ,CASE 
		  WHEN Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,[Job Information].V007) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2)  > 0
		  THEN CAST(ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			 AND [Bills And Payment Activity].V029 IN (''Invoice'',''Interim Invoice'')
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  CONVERT(MONEY,[Job Information].V007) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0)/
			  Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,[Job Information].V007) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) * 100 AS INT) 
		  ELSE 0 END AS [%Billed] */

	  /* ,ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V007,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			 AND [Bills And Payment Activity].V029 IN (''Payment Entry'')
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0) AS [Total Payment Amount] */

	/*   ,ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V007,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			 AND [Bills And Payment Activity].V029 IN (''Payment Entry'')
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  CONVERT(MONEY,[Job Information].V007) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0)  AS [Converted Total Payment Amount] */

	   /* ,CASE 
		  WHEN Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,[Job Information].V007) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2)  > 0
		  THEN CAST(ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V007,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			 AND [Bills And Payment Activity].V029 IN (''Payment Entry'')
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0)/
			  Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,[Job Information].V007) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) * 100 AS INT) 
		  ELSE 0 END AS [%Paid] */
		  ------------------------------------------------------------------------

	 --   ,CASE
		--  WHEN  ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
		--		WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
		--		AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
		--		CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0) 
		--		> 
		--		0
		--	 AND ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
		--		WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
		--		AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
		--		CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0) 
		--		<=				
		--		ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V007,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
		--		    WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
		--		    AND [Bills And Payment Activity].V029 IN (''Payment Entry'',''Overpaid Entry'')
		--		    AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
		--		    CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0)
		--  THEN ''1''
		--  ELSE ''2''
		--END AS [Paid Up]
		,0,0
	   , 	(SELECT V039  FROM RECORD WHERE TABLEID = 2743) AS [Header] 
	 --  ,CASE 
		--  WHEN Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) 
		--  <= 
		--  0
		--  THEN ''''
		--  ELSE	CASE WHEN [Currency Rates].V001 = [Job Group].V004 THEN CONVERT(MONEY,[Job Information].V008)
		--		ELSE	Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) END
		--		-
		--		CASE WHEN [Currency Rates].V001 = [Job Group].V004 
		--			   THEN  Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
		--			   WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
		--			   AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2)
		--		ELSE
		--			   ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
		--			   WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
		--			   AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
		--			   CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0)  
		--		END
		--END AS [Balance]
		,[Currency Rates].V001 , [Job Group].V004,[Job Information].V007,[Job Information].V047

			FROM [Record] [Job Information] WITH (NOLOCK) 

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		[EmployeeList_Job Controller].RecordID = CAST([Job Information].V026 as int) 		/*	Job Number	*/

			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		[Currency Rates].RecordID = CAST([Job Information].V034 as int) 		/*	Job Number	*/

			LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		[Tax Name].RecordID = CAST([Job Information].V042 as int)		/*	Job Number	*/
			 
			LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
				AND			[Job Group].IsActive = 1
				AND		[Job Group].RecordID  = CAST([Job Information].V027 as int)		/*	Currency	*/

			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/
		
		  LEFT JOIN [Record] [Reporting Currency] WITH (NOLOCK) ON [Reporting Currency].TableID = 2665
				AND		[Reporting Currency].IsActive = 1
				AND		[Reporting Currency].RecordID = CAST([Job Group].V005 as int) 		/*	Job Number	*/

		
			LEFT JOIN Record [Client] WITH (NOLOCK) ON [Client].TableID = 2474		/*	Currency Rates	*/
				AND			[Client].IsActive = 1
				AND	[Client].RecordID = CAST([Job Information].V024 as int)		/*	[Group Currency]	*/
		
				
			WHERE [Job Information].TableID = 2475 AND
					[Job Information].IsActive = 1 AND
					[Job Information].[V021] IN (''Current'')
					' + @SQLGroup + '
					' + @SQLController + '									
					'


--INSERT INTO @CurrentJobsList (JobRecordID ,
--		  [Job Date] , 
--		  [Job Controller] , 
--		  [Job No] , 
--		  [Job Client]  , 
--		  [Job Title]  , 
--		  [Job Value]  , 
--		  [Number of Bids]  , 
--		  [Currency] ,
--		  [Group Name]  ,
--		  [Converted Symbol]  ,
--		  [Converted Invoiced]  ,
--		  [Paid Up]  ,
--		    [Balance]  ,
--		  [Header]  ,
--		  CurrencyRate   ,
--		  GroupRate  ,
--		   JobCurrencyRate ,
--		  JobGroupRate  ) EXEC(@SQL)

		  
INSERT INTO @CurrentJobsList  EXEC(@SQL)
--SELECT * FROM @CurrentJobsList
--EXEC(@SQL);
PRINT CAST(@SQL AS NTEXT) 













	INSERT INTO @CurrentJobsListBalances(JobRecordID, BalanceOne, BalanceTwo,PaidUp, JobValue)
			SELECT [Job Information].RecordID,
			Round(ISNULL( SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)),0),2) as [THESAME]
				,
			CAST(ISNULL(Round(CAST(Round(ISNULL( SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) ,0),2) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0) AS MONEY) AS [NOTTHESAME]
			
			  ,case when ISNULL(Round(CAST(Round(ISNULL( SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) 
				,0),2) / 
				CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0) > 0
				AND ISNULL(Round(CAST(Round(ISNULL( SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) ,0),2) / 
			 CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0) 
			 <=				
			 ISNULL(Round(CAST(Round(ISNULL( SUM(CAST(ISNULL([Bills And Payment Activity].V007,0) AS MONEY))  ,0),2) / 
			 CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0)
			 THEN '1'
			 ELSE '2'
			 END AS [Paid Up]
			 ,Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) as JobValue
		




			  FROM [Record] [Bills And Payment Activity]
			  JOIN [Record] [Job Information] ON [Job Information].TableID = 2475
			  AND [Job Information].IsActive = 1
			  AND [Job Information].RecordID = CAST([Bills And Payment Activity].V002 as int)
			  --AND [Job Information].V027 = '16605069'
			  AND [Job Information].[V021] IN ('Current')
			  --AND [Job Information].[RECORDID] = 16669176
			  LEFT JOIN [Record] [Bills And Payment Activity2]
			 ON [Bills And Payment Activity2].TableID = 2711
			  AND [Bills And Payment Activity2].IsActive = 1
			  AND [Bills And Payment Activity2].RecordID = [Bills And Payment Activity].RecordID
			  AND [Bills And Payment Activity2].V029 IN ('Payment Entry','Overpaid Entry')
			  WHERE [Bills And Payment Activity].TableID = 2711
			  
			  AND [Bills And Payment Activity].IsActive = 1
			  GROUP BY [Job Information].RecordID, CONVERT(MONEY,ISNULL([Job Information].V007,1)),CONVERT(MONEY,ISNULL([Job Information].V047,1)),[Job Information].V008


  DECLARE @Counter int = 1
	WHILE EXISTS(SELECT * FROM @CurrentJobsList WHERE [Counter] >= @Counter)
	BEGIN

	

	   DECLARE @CurrencyRate varchar(max), @GroupCurrency varchar(max), @ConvertedInvoiced money, @JobRecordID int, @JobCurrencyRate varchar(max), @JobGroupRate varchar(max), @PaidUp varchar(10), @JobValue money, @Balance money
	   SELECT @CurrencyRate = CurrencyRate, @GroupCurrency = GroupRate, @JobRecordID = JobRecordID, @JobCurrencyRate = JobCurrencyRate, @JobGroupRate = JobGroupRate, @JobValue = [Job Value] FROM @CurrentJobsList WHERE [Counter] = @Counter
	 
	    
	   --IF NOT EXISTS(SELECT * FROM @CurrentJobsListBalances WHERE JobRecordID =  @JobRecordID)	
	   --BEGIN
		  --SELECT 'NOT EXIST : ' +  CAST(@JobRecordID AS VARCHAR)
	   --END


	    IF( @CurrencyRate = @GroupCurrency)
	    BEGIN
		  SELECT @ConvertedInvoiced = BalanceOne FROM  @CurrentJobsListBalances WHERE JobRecordID = @JobRecordID
	    END
	    ELSE
	    BEGIN
	     SELECT @ConvertedInvoiced = BalanceTwo FROM  @CurrentJobsListBalances WHERE JobRecordID = @JobRecordID
	    END
	   
	    SELECT @PaidUp = PaidUp FROM  @CurrentJobsListBalances WHERE JobRecordID = @JobRecordID

	  IF(@JobValue < 0)
	  BEGIN
	  SET @Balance = ''
	  END
	   ELSE
	   BEGIN
		   IF( @CurrencyRate = @GroupCurrency)
		   BEGIN
			 SELECT @Balance = @JobValue - @ConvertedInvoiced
		   END
		   ELSE
		   BEGIN
			 SELECT @Balance = JobValue - @ConvertedInvoiced FROM  @CurrentJobsListBalances WHERE JobRecordID = @JobRecordID 
		   END
	   END

	   
	   
	    UPDATE @CurrentJobsList
	    SET [Converted Invoiced] = @ConvertedInvoiced, [Paid Up] = @PaidUp, Balance = @Balance
	    WHERE [Counter] = @Counter 

	
	
	
	
	SET @Counter = @Counter +1
	END

	SELECT 
	 
		  [Job Date] , 
		  [Job Controller] , 
		  [Job No] , 
		  [Job Client]  , 
		  [Job Title]  , 
		  [Job Value]  , 
		  [Number of Bids]  , 
		  [Currency] ,
		  [Group Name]  ,
		  [Converted Symbol]  ,
		  [Converted Invoiced]  ,
		  [Paid Up]  ,
		    [Balance]  ,
		  [Header]  
	 FROM @CurrentJobsList




	
























/*
SELECT [Job Information].[V002] AS [Job Date]
	   , ISNULL([EmployeeList_Job Controller].V003,'') AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
	   , [Job Information].V001 AS [Job No]
	   ,ISNULL([Client].[V001],'-- NONE --') AS [Job Client]
	   , ISNULL([Job Information].V003,'-- NONE --') AS [Job Title] --Job Title, get from Job Information V003
	   ,Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) AS [Job Value]
	  
	   ,1 AS [Number of Bids]
	  -- ,[Currency Rates].V001 AS [Currency]	
	   ,[Reporting Currency].V001 AS [Currency]
	   ,ISNULL([Job Group].[V001],'') AS [Group Name]	
	 /*  , [Job Notes].V003 AS [Note Date] 
	    , [EmployeeList_List].V004 AS [Initial]
	   , [Job Notes].V006 AS [Note]		*/
	   ,CASE WHEN [Currency Rates].V001 = [Job Group].V004 THEN NULL ELSE convert(varchar,'C') END AS [Converted Symbol]
	 /*  ,ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0) AS [Invoiced] */
	  ,ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0)  AS [Converted Invoiced]
	  /* ,ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			 AND [Bills And Payment Activity].V029 IN ('Invoice','Interim Invoice')
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0) AS [Total Invoiced Amount] */
	/*    ,ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			 AND [Bills And Payment Activity].V029 IN ('Invoice','Interim Invoice')
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  CONVERT(MONEY,[Job Information].V007) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0)  AS [Converted Total Invoiced Amount]  */
	 /*  ,CASE 
		  WHEN Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,[Job Information].V007) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2)  > 0
		  THEN CAST(ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			 AND [Bills And Payment Activity].V029 IN ('Invoice','Interim Invoice')
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  CONVERT(MONEY,[Job Information].V007) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0)/
			  Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,[Job Information].V007) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) * 100 AS INT) 
		  ELSE 0 END AS [%Billed] */

	  /* ,ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V007,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			 AND [Bills And Payment Activity].V029 IN ('Payment Entry')
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0) AS [Total Payment Amount] */
	/*   ,ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V007,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			 AND [Bills And Payment Activity].V029 IN ('Payment Entry')
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  CONVERT(MONEY,[Job Information].V007) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0)  AS [Converted Total Payment Amount] */
	   /* ,CASE 
		  WHEN Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,[Job Information].V007) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2)  > 0
		  THEN CAST(ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V007,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			 AND [Bills And Payment Activity].V029 IN ('Payment Entry')
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0)/
			  Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,[Job Information].V007) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) * 100 AS INT) 
		  ELSE 0 END AS [%Paid] */
	    ,CASE
	   WHEN ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0) > 0
		AND ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  CONVERT(MONEY,[Job Information].V007) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0) =
		  ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V007,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
			 AND [Bills And Payment Activity].V029 IN ('Payment Entry','Overpaid Entry')
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0)
	   THEN '1'
	   ELSE '2'
	   END AS [Paid Up]
	   , 	(SELECT V039  FROM RECORD WHERE TABLEID = 2743) AS [Header] 
	   ,CASE WHEN Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) <= 0
		  THEN '0.00'
		  ELSE Round(isnull(CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) AS MONEY),0),2) -
			 ISNULL(Round(CAST(Round(ISNULL((SELECT SUM(CAST(ISNULL([Bills And Payment Activity].V005,0) AS MONEY)) FROM [Record] [Bills And Payment Activity] 
			 WHERE [Bills And Payment Activity].IsActive = 1 AND [Bills And Payment Activity].TableID = 2711 
				AND [Bills And Payment Activity].V002 = CAST([Job Information].RecordID AS VARCHAR)),0),2) / 
			  CONVERT(MONEY,ISNULL([Job Information].V007,1)) * CONVERT(MONEY,ISNULL([Job Information].V047,1)) AS MONEY),2),0) 
			  END AS [Balance]


			FROM [Record] [Job Information] WITH (NOLOCK) 

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		CAST([EmployeeList_Job Controller].RecordID AS VARCHAR(MAX)) = [Job Information].V026 		/*	Job Number	*/

			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		CAST([Currency Rates].RecordID AS VARCHAR(MAX)) = [Job Information].V034 		/*	Job Number	*/

			LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		CAST([Tax Name].RecordID AS VARCHAR(MAX)) = [Job Information].V042 		/*	Job Number	*/
			 
			LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
				AND			[Job Group].IsActive = 1
				AND		CAST( [Job Group].RecordID AS VARCHAR(MAX)) = [Job Information].V027		/*	Currency	*/

			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/
		
		  LEFT JOIN [Record] [Reporting Currency] WITH (NOLOCK) ON [Reporting Currency].TableID = 2665
				AND		[Reporting Currency].IsActive = 1
				AND		CAST([Reporting Currency].RecordID AS VARCHAR(MAX)) = [Job Group].V005 		/*	Job Number	*/

		
			LEFT JOIN Record [Client] WITH (NOLOCK) ON [Client].TableID = 2474		/*	Currency Rates	*/
				AND			[Client].IsActive = 1
				AND	CAST([Client].RecordID AS VARCHAR(MAX)) = [Job Information].V024		/*	[Group Currency]	*/
		
				
			WHERE [Job Information].TableID = 2475 AND
					[Job Information].IsActive = 1 AND
					[Job Information].[V021] IN ('Current')
					
														
*/

								
END
