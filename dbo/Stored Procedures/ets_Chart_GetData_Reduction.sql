﻿

CREATE PROCEDURE [dbo].[ets_Chart_GetData_Reduction] 
	-- Add the parameters for the stored procedure here
	@nTableID int,
	@sValueFields varchar(MAX),
	@dStartDate datetime,
	@dEndDate datetime,
	@sReducedData varchar(1000)
AS

BEGIN TRY
	SET NOCOUNT ON;
	
    DECLARE @Sql varchar(MAX)        
        
	SET @dEndDate = DATEADD(Day, 1, @dEndDate)
	
	
		BEGIN
			SET @sql =	' SELECT DateTimeRecorded,' + @sValueFields +
				' FROM Record' +
				' WHERE IsActive=1 AND DateTimeRecorded BETWEEN ''' + CAST(@dStartDate AS Varchar) + ''' AND ''' +  CAST(@dEndDate AS Varchar) + '''' +
				' AND TableID = ' + CAST(@nTableID AS Varchar) + ' AND ' + @sReducedData +
				' ORDER BY DateTimeRecorded' 			
		END	
	EXEC(@sql)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Chart_GetData_Reduction', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
