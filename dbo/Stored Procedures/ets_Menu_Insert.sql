﻿

CREATE PROCEDURE [dbo].[ets_Menu_Insert]
(
	@nNewID int output,
	@sMenu nvarchar(255) ,
	@nAccountID int ,
	@bShowOnMenu bit ,
	@bIsActive bit  = 1,
	@nParentMenuID int =NULL,
	@nTableID int=NULL,
	@nDocumentID int=NULL,	
	@sExternalPageLink nvarchar(1000)=NULL,
	@bOpenInNewWindow bit=NULL,
	@nDocumentTypeID INT=NULL,
	@sMenuType varchar(10)= NULL,
	@nReportID int = NULL,
	@bIsAdvancedSecurity bit = NULL
			--@nDisplayOrder int

)
	/*----------------------------------------------------------------
	-- Stored Procedure: ets_Menu_Insert
	-- Produced by: DB Gurus (SPGen) 
	-- Date: Dec  3 2010  6:35PM
	---------------------------------------------------------------*/
 
AS

BEGIN TRY
	DECLARE @nDisplayOrder INT
	SET @nDisplayOrder =0

	IF @nParentMenuID IS NULL
	SELECT @nDisplayOrder=MAX(DisplayOrder) FROM Menu WHERE ParentMenuID IS NULL AND AccountID=@nAccountID

	IF @nParentMenuID IS NOT NULL
	SELECT @nDisplayOrder=MAX(DisplayOrder) FROM Menu WHERE ParentMenuID =@nParentMenuID

	SELECT @sMenu=dbo.fnNextAvailableMenu(@sMenu,@nAccountID,@nParentMenuID,NULL)

	IF @nDisplayOrder IS NULL
		SET @nDisplayOrder=0

	IF @nDisplayOrder IS NOT NULL
		SET @nDisplayOrder=@nDisplayOrder+1

	INSERT INTO Menu
	(
		Menu, 
		AccountID, 
		ShowOnMenu,IsActive,DisplayOrder,ParentMenuID,TableID,DocumentID, 
		ExternalPageLink,OpenInNewWindow,DocumentTypeID,MenuType,ReportID, IsAdvancedSecurity
	) VALUES (
		@sMenu,
		@nAccountID,
		@bShowOnMenu,@bIsActive,@nDisplayOrder,@nParentMenuID,@nTableID,@nDocumentID, 
		@sExternalPageLink,@bOpenInNewWindow,@nDocumentTypeID,@sMenuType,@nReportID,@bIsAdvancedSecurity
	)
	SELECT @nNewID = @@IDENTITY
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Menu_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
