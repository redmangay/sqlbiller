﻿CREATE PROCEDURE [dbo].[Kate_SaveED1]
(
	@RecordID int,
	@UserID int,
	@Return varchar(max) output
)
AS
-- SELECT * FROM [Column] WHERE DisplayName = 'ED1 Arrival Time'
-- 
-- exec Kate_Save2ED @RecordID=874425, @UserID=0, @Return=''
BEGIN
	UPDATE [Record] SET V015 = ED1.V064 -- 'Arrival date and time at hospital'
		FROM [Record] 
		JOIN [Record] ED1 ON ED1.TableID = 2385 and ED1.RecordID = @RecordID
		WHERE [Record].RecordID = CAST(ED1.V298 as int)  -- Trauma Record Number

END
