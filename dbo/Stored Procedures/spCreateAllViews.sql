﻿

CREATE PROCEDURE [dbo].[spCreateAllViews]
(
	@AccountID	int,			-- Account ID to create the views for
	@DatabaseUser varchar(200) = NULL	-- Database user to grant access to
)
AS
/*
	spCreateAllViews 24822, rrponline
*/
BEGIN TRY
	DECLARE @SQL	varchar(MAX)

	SET				@SQL = ''

	SELECT			@SQL = @SQL + 'EXEC			[dbo].spCreateView	@AccountID = ''' + CAST(@AccountID AS varchar(MAX)) + '''	,@TableName = ''' + [Table].[TableName] + ''';
	'
	FROM			[Table]
	WHERE			[Table].IsActive = 1
	AND				[Table].AccountID = @AccountID

	PRINT			@SQL
	EXEC			(@SQL)

	IF @DatabaseUser IS NOT NULL AND LEN(@DatabaseUser) > 0
	BEGIN
		SET	@SQL = 'GRANT SELECT ON SCHEMA :: Account' + CAST(@AccountID AS varchar(MAX)) + ' TO ' + @DatabaseUser 
		PRINT		@SQL
		EXEC		(@SQL)
	
	
	
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spCreateAllViews', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
