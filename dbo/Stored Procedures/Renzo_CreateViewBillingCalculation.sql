﻿-- =============================================
-- Author:  Barry Matutina
-- Create date: 09/15/2016
-- Description:	Create a Job Billing Calculation View
-- =============================================
CREATE PROCEDURE Renzo_CreateViewBillingCalculation
	-- Add the parameters for the stored procedure here
	@RecordID AS INT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT V008,(V008 * V009) AS CALC  FROM [thedatabase_windtech].[Record] WHERE 
	[Record].RecordID = @RecordID
	
END
