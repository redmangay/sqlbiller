﻿CREATE procedure [dbo].[usr_srptCurrentBidsNotes]
(
@Group varchar(max) =null,
@Controller varchar(max) =null,
@From date =null,
@To date  =null,
@filter varchar(50) ='0'
)
as
SELECT Client.Company,
 Project.ProjectNumber,
  Project.ProjectTitle, Project.Date AS ProjectDate, Project.[Group/Division],
  Project.[Status],
isnull(dbo.ConvertToGroupCurrency(cast(ISNULL(Value, 0) as money),cast(ISNULL(CurrencyRate, 0) as money), cast(ISNULL(RptCurrencyRate, 0) as money)),0) as ProjectValue
 , Project.EmployeeID AS Controller,
 case when cast(ISNULL(CurrencyRate, 0) as money)=0 then '' 
      when [FixedPrice] ='False' then '' 
      else 'C' end as IsConverted
      into #rptCurrentBidsActivity
 FROM Account24918.vSubsidiary Client   INNER JOIN Account24918.vProject Project ON Client.ClientID = Project.ClientID

SELECT Notes.ProjectNumber, Max(cast(Notes.Date as Date)) AS MaxOfDate
into #BidMaxNoteDate
FROM Account24918.vNotes  Notes INNER JOIN  Account24918.vProject Project ON Notes.ProjectNumber = Project.ProjectNumber
WHERE (((Project.Status)='1'))
GROUP BY Notes.ProjectNumber
ORDER BY Notes.ProjectNumber;





SELECT cast(CurrentBidsList.ProjectDate as date) ProjectDate,
 CurrentBidsList.Controller,
 CurrentBidsList.ProjectNumber as JobNumber,
  CurrentBidsList.ProjectTitle,
   CurrentBidsList.[Group/Division],
    CurrentBidsList.ProjectValue,
     CurrentBidsList.IsConverted,
     Employee_List.Name, 
    -- Left(Nz([Employee_List.Name],""),20) as ChildName, 
     Notes.Initials, Notes.Subject, 
     Notes.Date AS NotesDate,
      CurrentBidsList.Company 
      into #withoutdate
      FROM (( #rptCurrentBidsActivity CurrentBidsList INNER JOIN Account24918.vNotes Notes ON CurrentBidsList.ProjectNumber = Notes.ProjectNumber) 
      INNER JOIN Account24918.vEmployee_List Employee_List ON CurrentBidsList.Controller = Employee_List.EmployeeID)
       INNER JOIN #BidMaxNoteDate BidMaxNoteDate ON (cast(BidMaxNoteDate.MaxOfDate as date) = cast(Notes.Date as date)) 
       AND (BidMaxNoteDate.ProjectNumber = Notes.ProjectNumber) 
       AND (CurrentBidsList.ProjectNumber = BidMaxNoteDate.ProjectNumber) 
     
   


        
        SELECT cast(CurrentBidsList.ProjectDate as date) ProjectDate, CurrentBidsList.Controller,CurrentBidsList.ProjectNumber as JobNumber, 
        CurrentBidsList.ProjectTitle,CurrentBidsList.[Group/Division],CurrentBidsList.ProjectValue, 
        CurrentBidsList.IsConverted,Employee_List.Name,
        -- Left(Nz([Employee_List.Name],""),20) as ChildName,
         Notes.Initials, Notes.Subject,
         Notes.Date AS NotesDate, CurrentBidsList.Company
         into #withdate
          FROM 
         ( #rptCurrentBidsActivity CurrentBidsList LEFT JOIN  Account24918.vNotes Notes ON CurrentBidsList.ProjectNumber = Notes.ProjectNumber)
          LEFT JOIN  Account24918.vEmployee_List  Employee_List ON CurrentBidsList.Controller = Employee_List.EmployeeID
       

            select * from #withoutdate 
            WHERE   
            cast(#withoutdate.NotesDate as date) not  between @From and @To
            and    
          
     (
--( (@group ='All') or (#withoutdate.[Group/Division] =@group))
--and
--( (@controller ='All') or (name =@controller))
( ((@filter ='0') and ((@group ='All') or (#withoutdate.[Group/Division] =@group)))  OR ( ( (@filter ='1') AND (name =@controller))) )
)
            union all
            select * from #withdate
            WHERE   cast(#withdate.NotesDate as date)   between @From and @To
             and    
          
     (
--( (@group ='All') or (#withdate.[Group/Division] =@group))
--and
--( (@controller ='All') or (name =@controller))
( ((@filter ='0') and ((@group ='All') or (#withdate.[Group/Division] =@group)))  OR ( ( (@filter ='1') AND (name =@controller))) )
)
            




--SELECT Sum(rptCurrentBidsActivity.ProjectValue) AS JobValue,
-- Count(*) AS Bids, 
-- Name,
-- IsNull(ReportingCurrency,'') as ReportingCurrency,
--  rptCurrentBidsActivity.[Group/Division]
--FROM  #rptCurrentBidsActivity rptCurrentBidsActivity
-- INNER JOIN  Account24918.vEmployee_List e ON (rptCurrentBidsActivity.Controller = e.EmployeeID) 
-- LEFT JOIN Account24918.vList_Of_Group_Division [List_Of_Group/Division] ON rptCurrentBidsActivity.[Group/Division] = [List_Of_Group/Division].[Group/Division]
--where rptCurrentBidsActivity.Status ='1'


--GROUP BY Name, rptCurrentBidsActivity.[Group/Division],IsNull(ReportingCurrency,'')
--ORDER BY Name, rptCurrentBidsActivity.[Group/Division];
