﻿

CREATE PROCEDURE		[dbo].[renzo_CheckBadDebtEntry]
(
	@RecordID  int = null,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output
)
AS
BEGIN
	/*
					DECLARE		@Return		VARCHAR(MAX)
					EXEC		renzo_SaveBillPaymentActivity @RecordID = n, @Return = @Return

					EXEC		[dbo].[dev_GetAccountSQL] @TableID = 2711, @AccountID = 24918
					
					UPDATE			[Table] 
						SET			[AddRecordSP] = 'renzo_SaveBillPaymentActivity'
									,[SPSaveRecord]  = 'renzo_SaveBillPaymentActivity'
					--SELECT			AddRecordSP
					--				,SPSaveRecord
					--				,*
						FROM		[Table]
						WHERE		[TableID] = 2711

	*/
	
DECLARE			@JobNumber		VARCHAR(MAX)
DECLARE			@JobValue		VARCHAR(MAX)

SELECT			@JobNumber = [Bill & Payment Activity].V002				/*	[Job Number]	*/
	FROM		[Record] [Bill & Payment Activity] WITH (NOLOCK)
	WHERE		[Bill & Payment Activity].TableID = '2711'				/*	Bill & Payment Activity	old 2711*/
	AND			[Bill & Payment Activity].IsActive = 1
	AND			[Bill & Payment Activity].RecordID = @RecordID

SELECT			@JobValue = [Job Information].V008						/*	[Job value]	*/
	FROM		[Record] [Job Information] WITH (NOLOCK)
	WHERE		[Job Information].TableID = '2475'						/*	Job Information	*/
	AND			[Job Information].IsActive = 1
	AND			[Job Information].RecordID = @JobNumber

/*	SELECT [Bill & Payment Activity] */

SELECT			(CAST( ISNULL(REPLACE([Bill & Payment Activity].V015, ',', ''), 0 ) AS DEC(10,2)))
	FROM		[Record] [Bill & Payment Activity] WITH (NOLOCK)
	WHERE		[Bill & Payment Activity].TableID = '2711'		/*	Bill & Payment Activity	if Job Information and Invoice*/
	AND			[Bill & Payment Activity].IsActive = 1
	AND			[Bill & Payment Activity].V002 = @JobNumber

END

/**************************************************/