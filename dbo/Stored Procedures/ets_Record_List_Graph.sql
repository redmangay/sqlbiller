﻿

CREATE PROCEDURE [dbo].[ets_Record_List_Graph]
(
	--@nAccountID int,
	@nTableID int,
--	@sSearch varchar(MAX) = NULL,
	@nEnteredBy int = NULL,
	@bIsActive bit = NULL,
	@dDateFrom datetime = NULL,
	@dDateTo datetime = NULL,
	@sOrder nvarchar(200)=NULL , 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
-- EXEC [ets_Record_List_Graph] @nTableID=1169

AS

BEGIN TRY
	SET NOCOUNT ON;

	DECLARE @tDisplayColumns TABLE
	(
		ID int identity(1,1),
		ColumnID int,
		SystemName varchar(50),
		DisplayText varchar(50)
	)

	INSERT INTO @tDisplayColumns (ColumnID, DisplayText, SystemName)
		SELECT ColumnID, DisplayTextSummary, SystemName
			FROM [Column] 
			WHERE TableID = @nTableID 
			AND DisplayTextSummary IS NOT NULL AND LEN(DisplayTextSummary) > 0
			ORDER BY DisplayOrder

	DECLARE @sSQL varchar(MAX)
	DECLARE @sSelectCount varchar(MAX)
	DECLARE @counter int
	SET @counter = 1
		

  -- Get the sOrder IF it is NULL
   WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID >= @counter)
	BEGIN
		
		IF @sOrder is NULL
		 SELECT @sOrder= ' [' + DisplayText  + ']' FROM @tDisplayColumns WHERE ID = @counter
		 SET @counter = @counter + 1
	END 
	
  SELECT @sSQL = 'SELECT * FROM (SELECT RecordInfo.*,ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ' ) as RowNum FROM (SELECT '


    SET @counter = 1
	WHILE EXISTS(SELECT * FROM @tDisplayColumns WHERE ID >= @counter)
	BEGIN
		SELECT @sSQL = @sSQL + 'Record.' + SystemName + ' AS [' + DisplayText + '], ' 
			FROM @tDisplayColumns 
			WHERE ID = @counter
		--PRINT @sSQL
		SET @counter = @counter + 1
		IF @sOrder is NULL
		 SELECT @sOrder= ' [' + DisplayText  + ']' FROM @tDisplayColumns WHERE ID = @counter
	END 

	SELECT @sSQL = LEFT(@sSQL, LEN(@sSQL)-1) 
	
	SELECT @sSQL = @sSQL + ' FROM [Record]'


	IF PATINDEX('%Record.TableID%', @sSQL) > 0
	BEGIN
		SET @sSQL = REPLACE(@sSQL, 'Record.TableID', '[Table].TableName')
		SET @sSQL = REPLACE(@sSQL, 'FROM [Record]', 'FROM [Record] JOIN [Table] ON Record.TableID = [Table].TableID')
	END	

	IF PATINDEX('%Record.EnteredBy%', @sSQL) > 0
	BEGIN
		SET @sSQL = REPLACE(@sSQL, 'Record.EnteredBy', '[User].FirstName')
		SET @sSQL = REPLACE(@sSQL, 'FROM [Record]', 'FROM [Record] JOIN [User] ON Record.EnteredBy = [User].UserID')
	END	

	-- ADD IN THE WHERE CRITERIA
	SELECT @sSQL = @sSQL + ' WHERE Record.TableID = ' + CAST(@nTableID as varchar)


	IF @dDateFrom IS NOT NULL 
		SELECT @sSQL = @sSQL + ' AND Record.DateTimeRecorded >= ''' + CONVERT(varchar(30), @dDateFrom, 120) + ''''

	IF @dDateTo IS NOT NULL 
		SELECT @sSQL = @sSQL + ' AND Record.DateTimeRecorded <= ''' + CONVERT(varchar(30), @dDateTo, 120) + ''''

	IF @nEnteredBy IS NOT NULL 
		SELECT @sSQL = @sSQL + ' AND Record.EnteredBy = ' + CAST(@nEnteredBy AS varchar)

	IF @bIsActive IS NOT NULL 
		SELECT @sSQL = @sSQL + ' AND Record.IsActive = ' + CAST(@bIsActive AS varchar)
    
    SELECT @sSelectCount= @sSQL + ') as RecordInfo) as RecordFinalInfo'
	SELECT @sSQL= @sSQL + ') as RecordInfo) as RecordFinalInfo WHERE RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
    --SET NOCOUNT OFF;
    
    SET @sSelectCount=REPLACE(@sSelectCount, 'SELECT * FROM', 'SELECT COUNT(*) AS TotalRows FROM')
    
    
    SET ROWCOUNT @nMaxRows
	PRINT @sSQL
	EXEC (@sSQL)
	-- PRINT @sTempTable
	
	SET ROWCOUNT 0
 
   PRINT @sSelectCount
   EXEC (@sSelectCount)

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Record_List_Graph', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
