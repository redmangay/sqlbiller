﻿

CREATE PROCEDURE [dbo].[EMD_ALS_Mapping_Refresh]

AS

BEGIN TRY
	TRUNCATE TABLE ALS_Mapping;

	INSERT INTO ALS_Mapping
	(
		TableID 
		,[Record ID]
		,[Incoming Site]
		,[Incoming Sample Type]
		,[Incoming Analyte Name]
		,[Total Or Dissolved]
		,[Target EMD]
		,[Target Table]
		,[Target Column]
		,[Expected Units]
		,[Target Column ID]
		,[DateAdded]
	)
	SELECT TableID, 
		RecordID AS [Record ID], 
		V001 AS [Incoming Site], 
		V002 AS [Incoming Sample Type], 
		V003 AS [Incoming Analyte Name], 
		V004 AS [Total Or Dissolved], 
		V005 AS [Target EMD], 
		V006 AS [Target Table], 
		V007 AS [Target Column], 
		V008 AS [Expected Units], 
		CASE WHEN ISNUMERIC(V009) = 1 THEN V009 ELSE NULL END AS [Target Column ID], 
		DateAdded
		FROM Record 
		WHERE TableID = 2558
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('EMD_ALS_Mapping_Refresh', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
