﻿
-- =============================================
-- Author:		DH
-- Create date: 15 Aug 2017
-- Description:	Save board info
-- =============================================
CREATE PROCEDURE [dbo].[TrelloBoard_Save] 
	@RefId varchar(50),
	@Name NVARCHAR(200),
	@ShortLink varchar(100),
	@CompanyID INT
AS

BEGIN TRY
	SET NOCOUNT ON;    
	DECLARE @Id INT

	SELECT	@Id = TrelloBoardID
	FROM	TrelloBoard
	WHERE	RefId = @RefId

	IF(@Id IS NULL)
		BEGIN
			INSERT INTO TrelloBoard 
			(
				RefId,
				[Name],
				ShortLink,
				CompanyID
			)
			VALUES
			(
				@RefId,
				@Name,
				@ShortLink,
				@CompanyID
			)
			SELECT SCOPE_IDENTITY()
		END	
	ELSE
		BEGIN
			UPDATE	TrelloBoard
			SET		[Name] = @Name,
					CompanyID = @CompanyID			
			WHERE	TrelloBoardID = @Id

			SELECT @Id
		END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('TrelloBoard_Save', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
