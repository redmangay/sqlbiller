﻿CREATE PROCEDURE [dbo].[Renzo_SaveBill & Payment Activity] 
(
	@RecordID  int,		/*	Record ID passed from the table on execute, base your solution on this value	*/
	@UserID int,
	@Return varchar(max) output
	, @Context varchar(max) = null
)
AS
BEGIN
	DECLARE			@Activity				VARCHAR(MAX);
	DECLARE			@JobNo					VARCHAR(MAX);
	DECLARE			@InvoiceAmount			money;
	DECLARE			@InvoiceAmount_Current money;
	DECLARE			@IsUnAllocated					VARCHAR(MAX);

	
	-- EXEC [dbo].[Renzo_SaveBill & Payment Activity]     2714584, 25162,''

	--EXEC  [dbo].[dev_GetAccountSQL] @TableID = 2711, @TableName = 'Bill & Payment Activity', @AccountID = '24918'
	--EXEC  [dbo].[dev_GetAccountSQL] @TableID = 2729, @TableName = 'Timesheet Details', @AccountID = '24918'
	--EXEC  [dbo].[dev_GetAccountSQL] @TableID = 2488, @TableName = 'Timesheet', @AccountID = '24918'
	--EXEC  [dbo].[dev_GetAccountSQL] @TableID = 2475, @TableName = 'Job Information', @AccountID = '24918'

	/*
	--Test

	DECLARE			@RecordID INT

	SET				@RecordID = 2707851

	UPDATE			[Timesheet Details]
		SET			[Timesheet Details].V020 = NULL		/*	[$ Billed]	*/
					,[Timesheet Details].V016 = NULL
		FROM		[Record] [Timesheet Details]
		WHERE		[Timesheet Details].TableID = '2729'
		AND			CAST( @RecordID AS VARCHAR(MAX)) = [Timesheet Details].V016
		AND			ISNULL( [Timesheet Details].V008, '' ) = 'By Rate';	

		SELECT			[Job Information].[TableID]
						,[Job Information].RecordID	AS				[Record ID]
						,[Job Information].V002		AS				[Date]
						,[Job Information].V020		AS				[ClientID orig]
						,[Job Information].V024		AS				[ClientID]
						,[Job Information].V022		AS				[TLStatus]
						,[Job Information].V001		AS				[Job No]
						,[Job Information].DateAdded
			FROM		[Record] [Job Information] WITH (NOLOCK)
			WHERE		[Job Information].TableID = '2475'		/*	Job Information	*/
			AND			[Job Information].IsActive = 1
			AND			[Job Information].V001 = 'WB458-04'

	*/		

	SELECT			@Activity = [Bill & Payment Activity].V029				/*	[Activity]		*/
					,@JobNo = [Bill & Payment Activity].V002				/*	[Job No]	*/	
					,@InvoiceAmount_Current = [Bill & Payment Activity].V005		    /*	[Invoice Amount]	*/
					,@InvoiceAmount = [Bill & Payment Activity].V059		    /*	[Invoice Amount] - Including Interim	*/
					,@IsUnAllocated = 	[Bill & Payment Activity].V036
		FROM		[Record] [Bill & Payment Activity]  WITH (NOLOCK)
		WHERE		[Bill & Payment Activity].RecordID = @RecordID;


	UPDATE			[Bill & Payment Activity]	
		SET			[Bill & Payment Activity].V039	 =	CAST(ISNULL([Job Number].V003,'') AS VARCHAR(MAX)) 
		FROM		[Record] [Bill & Payment Activity] 
		LEFT JOIN	Record [Job Number]  ON				[Job Number].TableID = '2475'		/*	Job Information	*/
												AND		[Job Number].IsActive = 1
												AND		CAST( [Job Number].RecordID AS VARCHAR(MAX)) = [Bill & Payment Activity].V002		/*	Job Number	*/
		WHERE		[Bill & Payment Activity].RecordID =@RecordID
		AND			[Bill & Payment Activity].TableID = '2711'		/*	Bill & Payment Activity	*/
		AND			[Bill & Payment Activity].IsActive = 1;

	IF	@Activity IN ( 'Invoice' )
		AND		ISNULL(@JobNo, '' ) <> ''
		BEGIN
			UPDATE [Record]
				SET V036 = NULL
				WHERE [RecordID] = @RecordID;	
		END


	-- Lets update the Timesheet Write Up/Down and Billed

	IF	@Activity IN ( 'Invoice' )
	BEGIN
			DECLARE @TimeSheetDetails TABLE ([Counter] int identity(1,1), BillingDate date, RecordID int , Amount money , [Status] varchar(100))
			
			INSERT INTO @TimeSheetDetails (BillingDate, RecordID, Amount,[Status])
				SELECT convert(date,[V009],103),RecordID, CAST([V007] AS MONEY), [V022] --[V008] 
				FROM [Record]
				WHERE TABLEID = 2729 AND [V016] = CAST(@RecordID AS VARCHAR)
				ORDER BY convert(date,[V009],103),RecordID

			DECLARE @TotalFixed_TimeSheet money =0.00, @TotalByRate_TimeSheet money = 0.00		
			select * from @TimeSheetDetails
			SELECT @TotalByRate_TimeSheet = ISNULL(SUM(Amount),0.00)
				FROM @TimeSheetDetails
					WHERE [Status] = '1' --'By Rate'

			SELECT @TotalFixed_TimeSheet = ISNULL(SUM(Amount),0.00)
				FROM @TimeSheetDetails
					WHERE [Status] <> '1' --'By Rate'
				
			--SELECT * FROM @TimeSheetDetails WHERE [status] ='By Rate'
			DECLARE @GSTRate FLOAT -- = 11 --?
			SELECT @GSTRate = ISNULL(V011,0.00) FROM [Record] WHERE RECORDID = @JobNo
			SELECT @GSTRate = (@GSTRate / 100.00)

			DECLARE @GSTXRate Float = .10 --?
			DECLARE @GSTAmount money = 0.00
			DECLARE @GSTAmountNetOfFixed money = 0.00
			DECLARE @NetOfTaxInvoiceAmount money = 0.00 -- net of tax
			DECLARE @NetInvoiceAmount DECIMAL(25,13)-- net of tax and fixed amount

			SELECT @GSTAmount = ROUND((@InvoiceAmount / (1 + @GSTRate)),2)
			SELECT @GSTAmount = @GSTAmount * @GSTRate
			print 'GST AMOUNT: ' + CAST( @GSTAmount AS VARCHAR(MAX))
			print 'Invoice AMOUNT: ' + CAST( @InvoiceAmount AS VARCHAR(MAX))
			print 'Rate AMOUNT: ' + CAST( @GSTRate AS VARCHAR(MAX))

			SET @NetInvoiceAmount = @InvoiceAmount - @GSTAmount - @TotalFixed_TimeSheet
			SET @NetOfTaxInvoiceAmount = @InvoiceAmount - @GSTAmount
			SET @GSTAmountNetOfFixed = @TotalFixed_TimeSheet / @NetOfTaxInvoiceAmount * @GSTAmount
			SET @GSTAmountNetOfFixed = @GSTAmount - @GSTAmountNetOfFixed
			--select @NetOfTaxInvoiceAmount
			--select 'GST',@GSTAmount
			--SELECT 'TFIXED',@TotalFixed_TimeSheet
			--SELECT 'NET OF TAX',@NetOfTaxInvoiceAmount
			--SELECT 'GST NET OF FIXED',@GSTAmountNetOfFixed



			DECLARE @MarkUpDownFor_ByRate DECIMAL(25,13)

			--select * from @TimeSheetDetails
			--print @NetInvoiceAmount
			--PRINT @TotalByRate_TimeSheet
			--print @MarkUpDownFor_ByRate
			--SELECT  CONVERT(DECIMAL(25,13), @NetInvoiceAmount / @TotalByRate_TimeSheet)  --.508072--
			SET @MarkUpDownFor_ByRate =   @NetInvoiceAmount / @TotalByRate_TimeSheet  --.508072--
			PRINT @NetInvoiceAmount
			PRINT @TotalByRate_TimeSheet
			print @MarkUpDownFor_ByRate
			DECLARE @Counter int = 1
			DECLARE @MaxCounter int
			DECLARE @CountByRateRecord int = 0
			DECLARE @RunningInvoiceAmount money = 0.00
			DECLARE @RunningTota_Fixed money = 0.00
			DECLARE @RunningTotal_ByRate money = 0.00
			DECLARE @RunningTotal_TimeSheet money = 0.00
			DECLARE @Running_GSTAmountNetOfFixed MONEY = 0.00

			SET  @Running_GSTAmountNetOfFixed = @GSTAmountNetOfFixed

			SELECT @MaxCounter = max([Counter]) FROM @TimeSheetDetails WHERE [Status] = '1' --'By Rate'
			SELECT @CountByRateRecord = Count(RecordID) FROM @TimeSheetDetails WHERE [Status] = '1' -- 'By Rate'
			--SELECT 'MarkUpDownRate', @MarkUpDownFor_ByRate
			--SELECT 'Net Invoice', @NetInvoiceAmount
			--SELECT 'Invoice Amount', @InvoiceAmount
			--SELECT 'GST', @GSTAmount
			--SELECT 'Total Fixed', @TotalFixed_TimeSheet
			--SELECT 'Total By Rate', @TotalByRate_TimeSheet
			

			WHILE EXISTS(SELECT * FROM @TimeSheetDetails WHERE [Counter] >= @Counter)
			BEGIN
				
				DECLARE @Status varchar(100), @Amount money, @TimeSheet_RecordID int

				SELECT @Status = [Status], @Amount = Amount, @TimeSheet_RecordID = RecordID 
					FROM @TimeSheetDetails 
						WHERE [Counter] = @Counter 
				
				IF @Status = '2' --'Fixed'
					BEGIN
						UPDATE [Record]
							SET [V020] = @Amount, -- Billed
								[V021] = '0.00', -- WriteUp/Down
								[V012] = (@Amount / @NetOfTaxInvoiceAmount) * @GSTAmount -- GST

							WHERE RecordID = @TimeSheet_RecordID

							select @Counter,'fixed',@Amount
						--SET @RunningTotal_TimeSheet = @RunningTotal_TimeSheet + @Amount
					END
				
				ELSE  -- By Rate
					BEGIN
						DECLARE @ByRateAmount money = 0.00
						IF @CountByRateRecord = 1
						BEGIN
							SET @ByRateAmount = @InvoiceAmount - @GSTAmount - @TotalFixed_TimeSheet 
							UPDATE [Record]
								SET [V020] = @ByRateAmount, -- Billed
									[V021] = @ByRateAmount - @Amount, -- WriteUp/Down
									[V012] =  @Running_GSTAmountNetOfFixed --  (@ByRateAmount / @NetOfTaxInvoiceAmount) * @GSTAmount -- GST
								WHERE RecordID = @TimeSheet_RecordID


								--PRINT CAST(@Counter as varchar) + ' -  ONLY ONE BY RATE'
							
						END
						ELSE IF @MaxCounter = @Counter AND @CountByRateRecord > 1
						BEGIN
							SET @ByRateAmount =  @InvoiceAmount - @GSTAmount - @TotalFixed_TimeSheet - @RunningTotal_ByRate 
							UPDATE [Record]
								SET [V020] = @ByRateAmount, -- Billed
									[V021] = @ByRateAmount - @Amount, -- WriteUp/Down
									[V012] =  @Running_GSTAmountNetOfFixed  --(Round(@ByRateAmount / @NetOfTaxInvoiceAmount,4) ) * @GSTAmount -- GST
								WHERE RecordID = @TimeSheet_RecordID

								
								select @Counter,'max',@ByRateAmount 
							--PRINT CAST(@Counter as varchar) + ' - MAX COUNTER'
							--print @InvoiceAmount
							--print @GSTAmount
							--print @TotalFixed_TimeSheet
							--print @RunningTotal_ByRate
							--print @ByRateAmount

						END
						
						ELSE
						BEGIN
							
							SET @ByRateAmount = Round(@Amount * @MarkUpDownFor_ByRate,2)
							UPDATE [Record]
								SET [V020] = @ByRateAmount, -- Billed
									[V021] = @ByRateAmount - @Amount, -- WriteUp/Down
									[V012] =  (@ByRateAmount / @NetOfTaxInvoiceAmount) * @GSTAmount -- GST
								WHERE RecordID = @TimeSheet_RecordID
								
								select @Counter,'NOT max',@ByRateAmount
								--PRINT CAST(@Counter as varchar) + ' -  NOT MAX COUNTER'
								--print @Amount
								--print @MarkUpDownFor_ByRate
								--print @ByRateAmount
						END
						--SELECT 'LAST',@Running_GSTAmountNetOfFixed
						SET @Running_GSTAmountNetOfFixed = @Running_GSTAmountNetOfFixed  - ((@ByRateAmount / @NetOfTaxInvoiceAmount) * @GSTAmount)
						SET @RunningTotal_ByRate = @RunningTotal_ByRate + @ByRateAmount
						select'last',@Running_GSTAmountNetOfFixed
					END
				
				SET @Counter = @Counter + 1
			END

			-- Update Bill and Payment Activity Invoice Amount Tax
			/*================================================================================================================================ */
			
		DECLARE @invoiceGSTAmount money = 0.00
		SELECT @invoiceGSTAmount = ROUND((@InvoiceAmount_Current / (1 + @GSTRate)),2)
		SELECT @invoiceGSTAmount = @invoiceGSTAmount * @GSTRate
			UPDATE [Record]
				SET V063 = @invoiceGSTAmount
				WHERE [RecordID] = @RecordID;	 

	END
	ELSE IF	@Activity IN ( 'Interim Invoice' )
	BEGIN

		DECLARE @interimGSTRate FLOAT -- = 11 --?
		DECLARE @interimGSTAmount money = 0.00
		SELECT @interimGSTRate = ISNULL(V011,0.00) FROM [Record] WHERE RECORDID = @JobNo
		SELECT @interimGSTRate = (@interimGSTRate / 100.00)

		SELECT @interimGSTAmount = ROUND((@InvoiceAmount_Current / (1 + @interimGSTRate)),2)
		SELECT @interimGSTAmount = @interimGSTAmount * @interimGSTRate

		-- Update Bill and Payment Activity Invoice Amount Tax
			UPDATE [Record]
				SET V063 = @interimGSTAmount
				WHERE [RecordID] = @RecordID;	 

	END
	ELSE IF	@Activity IN ( 'Payment Entry' )
	BEGIN
	   IF (@IsUnAllocated = 'Yes')
	   BEGIN
		  UPDATE [Record]
				SET V002 = NULL, -- Job No
				    V017 = NULL  -- Invoice Numbe
				WHERE [RecordID] = @RecordID;	 

	   END

	   IF (@IsUnAllocated = 'No')
	   BEGIN
		  UPDATE [Record]
				SET V017 = NULL -- Invoice Number
				,   V036 = NULL -- Unallocated Yes|No
				WHERE [RecordID] = @RecordID;	 

	   END

	   
	END


	ELSE IF	@Activity IN ( 'Overpaid Entry' )
	BEGIN
	  UPDATE [Record]
				SET V007 = CAST(CONVERT(MONEY,V007) * -1 AS VARCHAR)
				,   V036 = NULL -- Unallocated Yes|No
				WHERE [RecordID] = @RecordID;	 

	END

	ELSE IF	@Activity IN ( 'Credit Entry' )
	BEGIN
	  UPDATE [Record]
				SET V005 = CAST(CONVERT(MONEY,V045) * -1 AS VARCHAR),
				    V036 = NULL, -- Unallocated Yes|No
				    IsReadOnly = 1,
				    V017 = V046
				WHERE [RecordID] = @RecordID;	 



	   --lets create credit details
	   DECLARE @CreditAmount money = 0.00
	   DECLARE @EnteredBy int
	   DECLARE @BAPAJobRecordID int
	   SELECT @CreditAmount = V045,@EnteredBy = EnteredBy, @BAPAJobRecordID = V002 FROM RECORD WHERE RECORDID = @RecordID

	   EXEC [Renzo_GetComputedCreditDetails] @BAPAJobRecordID, @CreditAmount,@RecordID,@EnteredBy, 1

	END
	
	ELSE IF	@Activity IN ( 'Interim Credit' )
	BEGIN
	  UPDATE [Record]
				SET V005 = CAST(CONVERT(MONEY,V045) * -1 AS VARCHAR),
				  --  V036 = NULL -- Unallocated Yes|No
				   V017 = V046
				WHERE [RecordID] = @RecordID;	 

	END

	ELSE IF	@Activity IN ( 'Bad Debt Entry' )
	BEGIN
	  UPDATE [Record]
				SET V036 = NULL -- Unallocated Yes|No
				WHERE [RecordID] = @RecordID;	 

	END
	

	PRINT @Activity

	/* old scripts
	IF	@Activity IN ( 'Invoice' )
		BEGIN
			DECLARE			@EstimateAmount			VARCHAR(MAX);
			DECLARE			@AmountByRate			MONEY;
			DECLARE			@AmountOther			MONEY;
			DECLARE			@VariableAmount			MONEY;
			DECLARE			@BilledPercentage		MONEY;
			DECLARE			@LastEntry				MONEY;
			DECLARE			@Correction				MONEY;
			DECLARE			@CorrectionRecordID		INT;
			
			SELECT			@AmountByRate = ISNULL( CASE WHEN	ISNULL( [Timesheet Details].V008, '' ) = 'By Rate' THEN SUM( ISNULL( CAST( [Timesheet Details].V007 AS MONEY ), 0 ) ) END, 0 )
							,@AmountOther = ISNULL( CASE WHEN	ISNULL( [Timesheet Details].V008, '' ) <> 'By Rate' THEN SUM( ISNULL( CAST( [Timesheet Details].V007 AS MONEY ), 0 ) ) END, 0 )
				FROM		[Record] [Timesheet Details]
				WHERE		[Timesheet Details].TableID = '2729'
				AND			CAST( @RecordID AS VARCHAR(MAX)) = [Timesheet Details].V016
				GROUP BY	[Timesheet Details].V008;

			SET				@VariableAmount = @InvoiceAmount - @AmountOther

			IF				@AmountByRate = 0
				SET			@BilledPercentage = 1
			ELSE
				SET			@BilledPercentage = @VariableAmount / @AmountByRate
		
			PRINT			'@AmountByRate: ' + CAST( @AmountByRate AS VARCHAR(MAX) );
			PRINT			'@AmountOther: ' + CAST( @AmountOther AS VARCHAR(MAX) );
			PRINT			'@VariableAmount: ' +	CAST( @VariableAmount AS VARCHAR(MAX) );
			PRINT			'@BilledPercentage: ' + CAST( @BilledPercentage AS VARCHAR(MAX) );
	
			SELECT			@Correction = @InvoiceAmount - SUM( ROUND( CAST( ISNULL( [Timesheet Details].V007, 0 ) AS	MONEY )	* @BilledPercentage, 2 ) )		/*	[$ Billed]	*/
				FROM		[Record] [Timesheet Details]
				WHERE		[Timesheet Details].TableID = '2729'
				AND			CAST( @RecordID AS VARCHAR(MAX)) = [Timesheet Details].V016
				AND			ISNULL( [Timesheet Details].V008, '' ) = 'By Rate';
	
			PRINT			'@Correction: ' + CAST( @Correction AS VARCHAR(MAX) );

			SELECT TOP 1	@CorrectionRecordID = [Timesheet Details].RecordID
				FROM		[Record] [Timesheet Details]
				WHERE		[Timesheet Details].TableID = '2729'
				AND			CAST( @RecordID AS VARCHAR(MAX)) = [Timesheet Details].V016
				ORDER BY	[Timesheet Details].V009 DESC;
	
			SELECT			ROUND( CAST( ISNULL( [Timesheet Details].V007, 0 ) AS	MONEY )	* @BilledPercentage, 2 ) + CASE WHEN [Timesheet Details].RecordID = @CorrectionRecordID THEN @Correction ELSE 0 END			/*	[$ Billed]	*/
				FROM		[Record] [Timesheet Details]
				WHERE		[Timesheet Details].TableID = '2729'
				AND			CAST( @RecordID AS VARCHAR(MAX)) = [Timesheet Details].V016
				AND			ISNULL( [Timesheet Details].V008, '' ) = 'By Rate'
				ORDER BY	[Timesheet Details].V009;
	
			UPDATE			[Timesheet Details]
				SET			[Timesheet Details].V020 = ROUND( CAST( ISNULL( [Timesheet Details].V007, 0 ) AS	MONEY )	* @BilledPercentage, 2 ) + CASE WHEN [Timesheet Details].RecordID = @CorrectionRecordID THEN @Correction ELSE 0 END		/*	[$ Billed]	*/
				FROM		[Record] [Timesheet Details]
				WHERE		[Timesheet Details].TableID = '2729'
				AND			CAST( @RecordID AS VARCHAR(MAX)) = [Timesheet Details].V016
				AND			ISNULL( [Timesheet Details].V008, '' ) = 'By Rate';
		END





		*/


	RETURN @@ERROR;
END;
