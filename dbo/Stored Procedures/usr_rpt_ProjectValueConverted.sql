﻿
-- Author:		<Manoj sharma>
-- Create date: <12 March 2016>
-- Description:	<Used as ssrs report and in procedure rptcurrentjoblist>
create procedure [dbo].[usr_rpt_ProjectValueConverted]


AS
BEGIN

IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##ProjectValueConverted' )) DROP TABLE ##ProjectValueConverted
create table ##ProjectValueConverted  (
ProjectNumber varchar(max),
ProjectValue float ,
 Controller varchar(max), 
					   Status varchar(max),
					    BilledTo varchar(max),
						 Date varchar(max),
						  CurrencyRate varchar(max),
						   [Group/Division] varchar(max),
						    ProjectTitle varchar(max),
							 EmployeeID varchar(max), 
                      RptCurrencyRate varchar(max),
					   RptCurrency varchar(max), 
					   Value varchar(max), 
					   Currency varchar(max),
					   ProjectValueGroupCurrency float,
					   ProjectValueCompanyValue float


 )      
 insert into ##ProjectValueConverted

 SELECT     ProjectNumber, CASE WHEN isnull(CurrencyRate, '') = '' THEN CAST(REPLACE(REPLACE(isnull(value, 0), ',', ''), '$', '') AS float) 
                      ELSE CASE WHEN FixedPrice = 'False' THEN CAST(REPLACE(REPLACE(isnull(value, 0), ',', ''), '$', '') AS float) ELSE CAST(REPLACE(REPLACE(isnull(value, 0), ',', ''), '$', '') AS float) 
                      / CAST(REPLACE(REPLACE(isnull(CurrencyRate, 0), ',', ''), '$', '') AS float) END END AS ProjectValue, Controller, Status, BilledTo, Date, CurrencyRate, [Group/Division], ProjectTitle, EmployeeID, 
                      RptCurrencyRate, RptCurrency, Value, Currency, dbo.ConvertToGroupCurrency(CAST(REPLACE(REPLACE(ISNULL(Value, 0), ',', ''), '$', '') AS float), CAST(REPLACE(REPLACE(ISNULL(CurrencyRate, 0), 
                      ',', ''), '$', '') AS float), CAST(REPLACE(REPLACE(ISNULL(RptCurrencyRate, 0), ',', ''), '$', '') AS float)) AS ProjectValueGroupCurrency, CASE WHEN isnull(CurrencyRate, '') 
                      = '' THEN CAST(REPLACE(REPLACE(isnull(value, 0), ',', ''), '$', '') AS float) ELSE CAST(REPLACE(REPLACE(isnull(value, 0), ',', ''), '$', '') AS float) / CAST(REPLACE(REPLACE(isnull(CurrencyRate, 0), ',', 
                      ''), '$', '') AS float) END AS ProjectValueCompanyValue
FROM         Account24918.vProject AS Project

   
  
   
END;
