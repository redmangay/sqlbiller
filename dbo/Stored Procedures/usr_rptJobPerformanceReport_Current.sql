﻿-- =============================================
-- Author:		<Red Mangay>
-- Create date: <19 Jan 2018>
-- Job Performance Report
-- =============================================
CREATE PROCEDURE [dbo].[usr_rptJobPerformanceReport_Current]
	  @ProjectNumbers	int
	, @ProjectNumbers_Second	int =null
	, @StartDate		DATETIME =null
	, @EndDate			DATETIME =null
	, @GroupDivision	NVARCHAR(255) =null
	, @Controller		NVARCHAR(255) =null
AS
BEGIN

/*
============================
Test:

EXEC [dbo].[usr_rptJobPerformanceReport_Current] @ProjectNumbers =  2708341, @ProjectNumbers_Second=  2708402 



*/

SET DATEFORMAT dmy

DECLARE @RecordIDs varchar(max)

IF @ProjectNumbers_Second IS NULL
BEGIN
	SET @RecordIDs = CAST(@ProjectNumbers AS VARCHAR(MAX))
END
ELSE 
BEGIN
	IF @ProjectNumbers > @ProjectNumbers_Second
	BEGIN
		
		SELECT  @RecordIDs = LEFT(RecordIDs, LEN(RecordIDs) - 1) 
				FROM ( SELECT CONVERT(VARCHAR(50), [RecordID]) + ',' FROM [Record] WHERE
				RecordID >= @ProjectNumbers_Second AND RecordID <= @ProjectNumbers AND TableID = 2475 FOR XML PATH ('') )   C (RecordIDs)
	END
	ELSE 
	BEGIN
		SELECT  @RecordIDs = LEFT(RecordIDs, LEN(RecordIDs) - 1) 
				FROM ( SELECT CONVERT(VARCHAR(50), [RecordID]) + ',' FROM [Record] WHERE
				RecordID >= @ProjectNumbers AND RecordID <= @ProjectNumbers_Second AND TableID = 2475 FOR XML PATH ('') )   C (RecordIDs)
	END
END

	SELECT 	
   [Timesheet Details].V009 AS [Date Current]-- DATE OF INVOICE
  ,[Timesheet Details].V003 AS [Details Current]   --, Get DETAILS in TS Details - 2729
  ,[EmployeeList].V004 AS [Initials Current] --, Get INITIALS from Time
  ,CAST([Timesheet Details].V007 AS MONEY) AS [TimeSheet Current]--, Get the Timesheet (Amount) from TS Details
  ,[Timesheet Details].V008 AS [Status Current]
  ,CAST([Timesheet Details].V007 AS MONEY) AS [Billable Current]--, Get the Timesheet (Amount) from TS Details
  ,[Job Information].V001 AS [Job No]
   

FROM [Record] [Job Information] WITH (NOLOCK) 

	--LEFT JOIN [Record] [Bill & Payment Activity] ON [Bill & Payment Activity].TableID = 2711
	--	AND		[Bill & Payment Activity].IsActive = 1
	--	AND		[Bill & Payment Activity].V002 = CAST([Job Information].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/

	INNER JOIN [Record] [Timesheet Details] WITH (NOLOCK) ON [Timesheet Details].TableID = 2729
		AND		[Timesheet Details].IsActive = 1
		AND		[Timesheet Details].V016 IS NULL		/*	Job Number	*/
		AND		[Timesheet Details].V001 = CAST([Job Information].RecordID AS VARCHAR(MAX))
		--AND		[Timesheet Details].V009 IS NOT NULL 
		--AND		[Timesheet Details].V003 IS NOT NULL
		--	AND		[Timesheet Details].V003 IS NOT NULL
		--		AND		[Timesheet Details].V007 IS NOT NULL
		--			AND		[Timesheet Details].V008 IS NOT NULL

	--LEFT JOIN [Record] [Timesheet Details Current] WITH (NOLOCK) ON [Timesheet Details Current].TableID = 2729
	--	AND		[Timesheet Details Current].IsActive = 1
	--	AND		[Timesheet Details Current].V001 = CAST([Job Information].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
	--	AND		[Timesheet Details Current].V016 IS NULL

	--LEFT JOIN [Record] [Timesheet] WITH (NOLOCK) ON [Timesheet].TableID = 2488
	--	AND		[Timesheet].IsActive = 1
	--	AND		[Timesheet].V018 = CAST( [Job Information].RecordID AS VARCHAR(MAX))		/*	Job Number	*/

	LEFT JOIN [Record] [EmployeeList] WITH (NOLOCK) ON [EmployeeList].TableID = 2669
		AND		[EmployeeList].IsActive = 1
		--AND		[EmployeeList].V004 IS NOT NULL
		AND		CAST([EmployeeList].RecordID AS VARCHAR(MAX)) = [Timesheet Details].V002 		/*	Job Number	*/

	--LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
	--	AND		[EmployeeList_Job Controller].IsActive = 1
	--	AND		CAST([EmployeeList_Job Controller].RecordID AS VARCHAR(MAX)) = [Job Information].V026 		/*	Job Number	*/

	--LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
	--	AND		[Currency Rates].IsActive = 1
	--	AND		CAST([Currency Rates].RecordID AS VARCHAR(MAX)) = [Job Information].V034 		/*	Job Number	*/

WHERE [Job Information].TableID = 2475 AND
	  [Job Information].IsActive = 1 AND	
	  [Job Information].RecordID IN (SELECT Int_Value FROM DBO.fn_ParseText2Table(@RecordIDs,','))

ORDER BY [Timesheet Details].V009


END

