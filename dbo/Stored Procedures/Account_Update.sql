﻿ 

CREATE PROCEDURE   [dbo].[Account_Update]
(
	@nAccountID int ,
	@sAccountName nvarchar(100) ,	
	@Logo image = NULL,
	@IsActive bit = 1,	
	@iAccountTypeID int=NULL,
	@dateExpiryDate datetime=NULL,
	@bUseDefaultLogo bit=1,
	@dMapCentreLat decimal(9,6)=NULL,
	@dMapCentreLong decimal(9,6)=NULL,
	@nMapZoomLevel int=NULL,
	@nMapDefaultTableID int=NULL,
	@dateExceedLastEmail datetime=NULL,
	@nOtherMapZoomLevel int=NULL,
	@nCountryID int=NULL,
	@sPhoneNumber nvarchar(20) =NULL,
	@bCreatedByWizard bit=0,
	@nExtensionPacks int=0,
	@bAlerts bit=0,
	@bReportGen bit=0,	
	@nLayout int=0,
	@bSystemAccount bit  = NULL,
	@nNextBilledAccountTypeID int  = NULL,
	@sClientRef varchar(20)  = NULL,
	@bGSTApplicable bit  = 1,
	@sOrganisationName nvarchar(200)  = NULL,
	@sBillingPhoneNumber nvarchar(20)  = NULL,
	@sBillingAddress nvarchar(MAX)  = NULL,
	@sBillingEmail nvarchar(200)  = NULL,
	@nBillEveryXMonths tinyint  = 1,
	@sPaymentMethod char(1)  = 'E',
	@sBillingFirstName nvarchar(50)  = NULL,
	@sBillingLastName nvarchar(50)  = NULL,
	@sComment nvarchar(MAX)= NULL,
	@nDefaultGraphOptionID int=NULL,
	@sMasterPage varchar(100)=NULL,
	@bUseDataScope bit=NULL,
	@sHomeMenuCaption nvarchar(100) =NULL,
	@bShowOpenMenu bit=1,
	@bIsReportTopMenu bit=NULL,
	@bUploadAfterVerificaition bit=NULL,
	@nEmailCount int=0,
	@nSMSCount int=0,
	@bLabelOnTop bit=NULL,
	@sReportServer varchar(100)=NULL,
	@sReportUser varchar(100)=NULL,
	@sReportPW varchar(100)=NULL,
	@sReportServerUrl varchar(250)=NULL,
	@sSMTPEmail varchar(255)= NULL,
	@sSMTPUserName varchar(255) =NULL,
	@sSMTPPassword varchar(50)= NULL,
	@sSMTPPort varchar(10) =NULL,
	@sSMTPServer varchar(100)=NULL,
	@sSMTPSSL varchar(10)= NULL,
	@sSMTPReplyToEmail varchar(255)= NULL,
	@sPOP3Email varchar(255) =NULL,
	@sPOP3UserName varchar(255) =NULL,
	@sPOP3Password varchar(50) =NULL,
	@sPOP3Port varchar(10) =NULL,
	@sPOP3Server varchar(100) =NULL,
	@sPOP3SSL varchar(10) =NULL
)
 
AS

BEGIN TRY

	IF @dateExceedLastEmail is not NULL
		SET @dateExceedLastEmail=cast(@dateExceedLastEmail as date)
	
	
	UPDATE [Account]
	SET 
		EmailCount=@nEmailCount,
		SMSCount=@nSMSCount,
		AccountName = @sAccountName,
		Logo=@Logo,
		IsActive=@IsActive,	
		AccountTypeID=@iAccountTypeID,
		ExpiryDate=@dateExpiryDate,
		UseDefaultLogo=@bUseDefaultLogo,
		MapCentreLat=@dMapCentreLat,
		MapCentreLong=@dMapCentreLong,
		MapZoomLevel=@nMapZoomLevel,
		MapDefaultTableID=@nMapDefaultTableID,
		ExceedLastEmail= @dateExceedLastEmail,
		OtherMapZoomLevel=@nOtherMapZoomLevel,
		CountryID=@nCountryID,
		PhoneNumber=@sPhoneNumber,
		CreatedByWizard=@bCreatedByWizard,
		ExtensionPacks=@nExtensionPacks,
		Alerts=@bAlerts,
		ReportGen=@bReportGen,	
		Layout=@nLayout,
		SystemAccount = @bSystemAccount,
		NextBilledAccountTypeID = @nNextBilledAccountTypeID,
		ClientRef = @sClientRef,
		GSTApplicable = @bGSTApplicable,
		OrganisationName = @sOrganisationName,
		BillingPhoneNumber = @sBillingPhoneNumber,
		BillingAddress = @sBillingAddress,
		BillingEmail = @sBillingEmail,
		BillEveryXMonths = @nBillEveryXMonths,
		PaymentMethod = @sPaymentMethod,
		BillingFirstName = @sBillingFirstName,
		BillingLastName = @sBillingLastName,
		Comment=@sComment,
		DefaultGraphOptionID=@nDefaultGraphOptionID,
		MasterPage=@sMasterPage,
		UseDataScope=@bUseDataScope,
		HomeMenuCaption=@sHomeMenuCaption,
		ShowOpenMenu=@bShowOpenMenu,
		IsReportTopMenu=@bIsReportTopMenu,
		UploadAfterVerificaition=@bUploadAfterVerificaition,
		LabelOnTop=@bLabelOnTop,
		ReportServer=@sReportServer,
		ReportUser=@sReportUser,
		ReportPW=@sReportPW,
		ReportServerUrl=@sReportServerUrl,
		SMTPEmail=@sSMTPEmail,
		SMTPUserName=@sSMTPUserName,
		SMTPPassword=@sSMTPPassword,
		SMTPPort=@sSMTPPort,
		SMTPServer=@sSMTPServer,
		SMTPSSL=@sSMTPSSL,
		SMTPReplyToEmail=@sSMTPReplyToEmail,
		POP3Email=@sPOP3Email,
		POP3UserName=@sPOP3UserName,
		POP3Password=@sPOP3Password,
		POP3Port=@sPOP3Port,
		POP3Server=@sPOP3Server,
		POP3SSL=@sPOP3SSL
	
	WHERE AccountID = @nAccountID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Account_Update', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
