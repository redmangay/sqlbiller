﻿-- =============================================
-- Author:		<Manoj Sharma>
-- Create date: <27 june 2016>
-- Description:	<Merge Time sheet detail with disubrsement and timesheet>
-- =============================================
CREATE PROCEDURE		[dbo].[Renzo_AddTimesheet]
(
	@RecordID  int,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output
)
AS
/*	Insert Disbursement and Timesheet into Timesheet Detail	*/
​
/*	Disbursement Parma	*/
Declare 
@DTableId VarChar(max),
@DDateTimeRecorded VarChar(max),
@DEnteredBy VarChar(max),
@DIsActive VarChar(max),
@DDateUpdate VarChar(Max),
@DJob VarChar(MAX),
@DDate VarChar(MAX),
@DEmployee VarChar(MAX),
@DDetails VarChar(MAX),
@DX VarChar(MAX),
@DUnit VarChar(MAX),
@DRate VarChar(MAX),
@D$_Amount VarChar(MAX),
@DStatus VarChar(MAX),
@DDisbursement VarChar(MAX),
@DDateAdded VarChar(MAX)


SELECT		     @DTableId=	'2729'		/*	Timesheet Details	*/
				,@DDateTimeRecorded =GETDATE()
				,@DEnteredBy =[Disbursement].EnteredBy
				,@DIsActive=[Disbursement].IsActive
				,@DDateUpdate=GETDATE()
				,@DJob=[Disbursement].V001	
				,@DDate=[Disbursement].V002		
				,@DEmployee=[Disbursement].V019		
				,@DDetails=[Disbursement].V006		
				,@DX='1'						
				,@DUnit=[Disbursement].V004		
				,@DRate=[Disbursement].V005		
				,@D$_Amount=[Disbursement].V013	
				,@DStatus=[Disbursement].V015		
				,@DDisbursement=[Disbursement].RecordID	
				,@DDateAdded=[Disbursement].DateAdded
	FROM		[Record] [Disbursement]
	WHERE		[Disbursement].TableID = '2725'		/*	Disbursement	*/
	AND			[Disbursement].IsActive = 1
	AND [Disbursement].RecordID = @RecordID  and isnumeric(@RecordID)=1
​

/*	Disbursement Parma	End here*/ 



	Merge [Record] 	 as  [Timesheet Details]
			using (select 
			     @DTableId as	TableId		/*	Timesheet Details	*/
				,@DDateTimeRecorded as DateTimeRecorded
				,@DEnteredBy as EnteredBy
				,@DIsActive as IsActive
				,@DDateUpdate as DateUpdate
				,@DJob as JobNo
				,@DDate as [Date]
				,@DEmployee as Employee 
				,@DDetails as Details 
				,@DX as x					
				,@DUnit as Unit
				,@DRate as Rate
				,@D$_Amount as [$_Amount] 
				,@DStatus as [Status]  
				,@DDisbursement as [Disbursement]
				,@DDateAdded as DateAdded
			 
			
			) as [Disbursement] 
			on ([Timesheet Details].V010=@RecordID and isnumeric(@RecordID)=1 and [Timesheet Details].TableID = '2729'	AND			[Timesheet Details].IsActive = 1)
			
			when matched  then UPDATE 
			set  
			 
			 [Timesheet Details].V001 = Disbursement.JobNo,
			 [Timesheet Details].V009 = Disbursement.[Date],
			 [Timesheet Details].V002 = Disbursement.Employee,
			 [Timesheet Details].V003 = Disbursement.Details ,
			 [Timesheet Details].V004 = Disbursement.x,
			 [Timesheet Details].V005 = Disbursement.Unit,
			 [Timesheet Details].V006 = Disbursement.Rate,
			 [Timesheet Details].V007 =  Disbursement.[$_Amount]  ,
			 [Timesheet Details].V008 = Disbursement.[Status],
			 [Timesheet Details].V010 = Disbursement.Disbursement
			
			when not matched then 
			
			INSERT ([TableID],DateTimeRecorded,EnteredBy,IsActive,DateUpdated,
			     V001	            /*	[Job No]	*/
				,V009				/*	[Date]	*/
				,V002				/*	[Initials]	*/
				,V003				/*	[Details]	*/
				,V004				/*	[(X)]	*/
				,V005				/*	[Hr/Unit]	*/
				,V006				/*	[Rate]	*/
				,V007				/*	[$ Amount]	*/
				,V008				/*	[Status]	*/
				,V010				/*	[Disbursement]	*/
				,DateAdded)
			 values (
			    Disbursement.TableId,Disbursement.DateTimeRecorded,Disbursement.EnteredBy,Disbursement.IsActive,Disbursement.DateUpdate,
				Disbursement.JobNo,
				Disbursement.[Date],
				Disbursement.Employee,
				Disbursement.Details ,
				Disbursement.x,
				Disbursement.Unit,
				Disbursement.Rate,
				Disbursement.[$_Amount]  ,
				Disbursement.[Status],
				Disbursement.Disbursement,
				Disbursement.DateAdded

			);	





	/*	Timesheet Paramerter	*/
	Declare
	@TTableID VarChar(MAX)
   ,@TDateTimeRecorded VarChar(MAX)
   ,@TEnteredBy VarChar(MAX)
   ,@TIsActive VarChar(MAX)
   ,@TDateUpdated VarChar(MAX)
   ,@TJobNo VarChar(MAX)
   ,@TDate VarChar(MAX)
   ,@TEmployee VarChar(MAX)
   ,@TDetails VarChar(MAX)
   ,@TX VarChar(MAX)
   ,@TUnit VarChar(MAX)
   ,@TRate VarChar(MAX)
   ,@TAmount VarChar(MAX)
   ,@TStatus VarChar(MAX)
   ,@TTimesheet VarChar(MAX)
   ,@TDateAdded VarChar(MAX)
   /*	Timesheet Paramerter end	*/


   SELECT		@TTableID=	'2729'		
				,@TDateTimeRecorded =GETDATE()
				,@TEnteredBy =[Timesheet].EnteredBy
				,@TIsActive =[Timesheet].IsActive
				,@TDateUpdated =GETDATE()
				,@TJobNo =[Timesheet].V018		
				,@TDate =[Timesheet].V004		
				,@TEmployee =[Timesheet].V017		
				,@TDetails =[Timesheet].V006		
				,@TX =[Timesheet].V007		
				,@TUnit =NULL					
				,@TRate =[Timesheet].V009		
				,@TAmount =[Timesheet].V019		
				,@TStatus =[Timesheet].V011		
				,@TTimesheet =[Timesheet].RecordID	
				,@TDateAdded =[Timesheet].DateAdded
	FROM		[Record] [Timesheet]
	WHERE		[Timesheet].TableID = '2488'		/*	Timesheet	*/
	AND			[Timesheet].IsActive = 1
	AND 	[Timesheet].RecordID  =@RecordID  and isnumeric(@RecordID)=1







​
/*	Merge Timesheet	*/
​
Merge [Record] 	 as  [Timesheet Details]
			using (select 
			   
			     @TTableID   as TableID
				,@TDateTimeRecorded as DateTimeRecorded
				,@TEnteredBy as EnteredBy
				,@TIsActive as IsActive
				,@TDateUpdated as DateUpdated
				,@TJobNo as JobNo
				,@TDate as [Date] 
				,@TEmployee as [Employee]
				,@TDetails as Details
				,@TX as X
				,@TUnit as Unit
				,@TRate as Rate 
				,@TAmount as Amount
				,@TStatus as [Status]
				,@TTimesheet as Timesheet 
				,@TDateAdded  as DateAdded 
			 
			
			) as Timesheet 
			on ([Timesheet Details].V011=@RecordID  and isnumeric(@RecordID)=1 and [Timesheet Details].TableID = '2729'	AND			[Timesheet Details].IsActive = 1)
			
			when matched  then UPDATE 
			set  
			 
				[Timesheet Details].V001	    = Timesheet.JobNo			
				,[Timesheet Details].V009		= Timesheet.[Date] 		
				,[Timesheet Details].V002		=Timesheet.[Employee]		
				,[Timesheet Details].V003		=Timesheet.Details		
				,[Timesheet Details].V004		=Timesheet.X		
				,[Timesheet Details].V005	    =Timesheet.Unit			
				,[Timesheet Details].V006		=Timesheet.Rate 		
				,[Timesheet Details].V007		=Timesheet.Amount		
				,[Timesheet Details].V008	    =Timesheet.[Status]			
				,[Timesheet Details].V011	    =Timesheet.Timesheet 	
				,[Timesheet Details].DateAdded	=getdate()
			
			when not matched then 
			
			INSERT ([TableID]
				,DateTimeRecorded
				,EnteredBy
				,IsActive
				,DateUpdated
				,V001				/*	[Job No]	*/
				,V009				/*	[Date]	*/
				,V002				/*	[Initials]	*/
				,V003				/*	[Details]	*/
				,V004				/*	[(X)]	*/
				,V005				/*	[Hr/Unit]	*/
				,V006				/*	[Rate]	*/
				,V007				/*	[$ Amount]	*/
				,V008				/*	[Status]	*/
				,V011				/*	[Timesheet]	*/
				,DateAdded)
			 values (
			   Timesheet.TableID   
				,Timesheet.DateTimeRecorded 
				,Timesheet.EnteredBy 
				,Timesheet.IsActive 
				,Timesheet.DateUpdated 
				,Timesheet.JobNo
				,Timesheet.[Date]  
				,Timesheet.Employee 
				,Timesheet.Details 
				,Timesheet.X 
				,Timesheet.Unit 
				,Timesheet.Rate
				,Timesheet.Amount 
				,Timesheet.[Status]
				,Timesheet.Timesheet 
				,Timesheet.DateAdded  

			);	
		
RETURN @@ERROR
