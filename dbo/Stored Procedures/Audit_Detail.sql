﻿
CREATE PROCEDURE [dbo].[Audit_Detail]
(
    @sTableName varchar(128),
      @nPKID int,
      @UpdateDate datetime
)

AS

BEGIN TRY
	--EXEC Record_Audit_Detail 391392,'2012-02-23 10:17:53.930'
		  -- DECLARE @RecordID int
		  -- SET @RecordID = 2152 

	IF @sTableName='Record'
		  SELECT [Audit].DateAdded,[Column].SystemName,
				[User].FirstName + ' ' + [User].LastName as [User],
				--CASE WHEN [Column].DisplayTextDetail IS NULL THEN FieldName ELSE [Column].DisplayTextDetail END as ColumnName,
				 --[Column].DisplayName  as ColumnName,
				 CASE WHEN [Column].DisplayName IS NULL THEN FieldName ELSE [Column].DisplayName END as ColumnName,
				OldValue,
				NewValue
				FROM [Audit]
				JOIN [User] ON [User].UserID = [Audit].UserID
				JOIN  [Record] ON [Record].RecordID = @nPKID
				LEFT JOIN [Column] on [Column].TableID = [Record].TableID AND [Column].SystemName = FieldName
				WHERE TableName =@sTableName

				AND FieldName != 'DateUpdated'
				AND FieldName != 'LastUpdatedUserID'
				AND FieldName != 'WarningResults'
				  AND FieldName != 'ChangeReason'		
				AND PrimaryKeyValue = @nPKID
				AND [Audit].DateAdded = @UpdateDate
				ORDER BY [Audit].DateAdded

 
	 IF @sTableName<>'Record'
	  SELECT [Audit].DateAdded,'' as SystemName,
				[User].FirstName + ' ' + [User].LastName as [User],
				 FieldName as ColumnName,
				OldValue,
				NewValue
				FROM [Audit]
				JOIN [User] ON [User].UserID = [Audit].UserID
				WHERE TableName=@sTableName
				AND FieldName != 'DateUpdated'
				AND FieldName != 'LastUpdatedUserID'
					AND FieldName != 'UserUpdated'
				AND PrimaryKeyValue = @nPKID
				AND [Audit].DateAdded = @UpdateDate
				ORDER BY [Audit].DateAdded

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Audit_Detail', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
