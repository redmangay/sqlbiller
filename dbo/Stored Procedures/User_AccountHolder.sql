﻿

CREATE PROCEDURE [dbo].[User_AccountHolder]
(
	@nAccountID int 
)
AS


/*
EXEC [dbo].[User_AccountHolder] 24800
*/
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @UserID INT =[dbo].[fnGetAccountHolderUserID] (@nAccountID)	--Account Holder
	SELECT * 
		FROM [User] 
		WHERE UserID=@UserID

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('User_AccountHolder', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
