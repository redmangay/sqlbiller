﻿CREATE PROCEDURE [dbo].[usr_rptClientAuthority]
(
    @JobRecordID int
)
AS
BEGIN
    

    /*
    
    Testing:


    exec [usr_rptClientAuthority]  12943624
    
    */
    
SELECT  [Job Information].V001 as JobNo,
	   [Job Information].V003 as JobTitle,
	   [Job Information].V008 as JobValue,
	   CASE WHEN [Job Information].V009 = 'False' THEN '(Estimate)' ELSE '' END as IsEstimate,
	   [Employee].V003 as OurContact,
	   [Job Information].V014 as OurReference,
	   [Billing Address].V017 as ClientName,
	   [Billing Address].V002 as Address1,
	   [Billing Address].V015 as Address2,
	   [Billing Address].V003 as Suburb,
	   [Billing Address].V004 as [State],
	   [Billing Address].V005 as PostCode,
	   [Billing Address].V006 as Country,
	   [Billing Address].V018 as Attention,
	   [Billing Address].V009 as Phone,
	   [Billing Address].V010 as Fax,
	   [Billing Address].V011 as Mobile,
	   [Billing Address].V012 as Email,

	    (SELECT OptionValue FROM SYSTEMOPTION WHERE OPTIONKEY = 'FilesLocation') + '/UserFiles/AppFiles/' + 
			   [Document Layout].V037
			 AS HeaderImage

    FROM Record [Job Information]
    JOIN Record [Billing Address] ON [Billing Address].TableID = 2476
    AND [Billing Address].IsActive = 1
    AND CAST([Billing Address].V013 as int) = [Job Information].RecordID

    JOIN Record [Employee] ON [Employee].TableID = 2669
    AND [Employee].IsActive = 1
    AND [Employee].RecordID = CAST([Job Information].V026 as int)
  
     LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
    	   AND			[Job Group].IsActive = 1
	   AND		CAST( [Job Group].RecordID AS VARCHAR(MAX)) = [Job Information].V027		/*	Group	*/

	LEFT JOIN Record [Document Layout] WITH (NOLOCK) ON [Document Layout].TableID = 2744	/*	Document Layout	*/
	   AND			[Document Layout].IsActive = 1
	   AND		[Document Layout].V005 = CAST([Job Group].RecordID AS VARCHAR)		/*	Group	*/

    WHERE [Job Information].TableID = 2475
	   AND [Job Information].IsActive = 1
	   AND [Job Information].RecordID = @JobRecordID


END