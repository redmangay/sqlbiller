﻿
 
CREATE PROCEDURE [dbo].[ets_MonitorSchedule_SelectByDateRange]
(
	
	@nTableID int  ,
	@dStartDateTime datetime  ,
	@dEndDateTime datetime  		
	
)
	/*----------------------------------------------------------------
	
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	--SET ROWCOUNT @nMaxRows
	-- DECLARE local variables

		BEGIN
		SELECT MonitorScheduleID 
			FROM MonitorSchedule 
			WHERE TableID=@nTableID 
			AND CONVERT(datetime,FLOOR(CONVERT(FLOAT, ScheduleDateTime))) 
			BETWEEN @dStartDateTime   
			AND @dEndDateTime	
		END
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_MonitorSchedule_SelectByDateRange', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
