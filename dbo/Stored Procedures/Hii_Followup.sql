﻿CREATE PROCEDURE [dbo].[Hii_Followup]
(
	@RecordID int,
	@UserID int,
	@Return varchar(max) output
)
AS
/*
	SELECT * FROM [Record] WHERE TableID = 2146 ORDER BY DateAdded DESC
	EXEC Hii_Followup @RecordID=905682
*/
SELECT @Return = 'Hii_Followup executed'
DECLARE @TableID int
SELECT @TableID = TableID FROM [Record] WHERE RecordID = @RecordID

Print 'Remove existing record first'
DELETE FROM [Record] WHERE TableID = 2485 AND V007 = @RecordID

IF @TableID = 2142
BEGIN
	print '2142'
	/*
	SELECT C.DisplayName, C.SystemName, C.Notes FROM [Column] C	WHERE C.TableID = 2485 order by C.SystemName
	SELECT C.DisplayName, C.SystemName, C.Notes FROM [Column] C	WHERE C.TableID = 2142
	*/
	INSERT INTO [Record] (TableID, 
			V009,  -- Patient ID number
			V001,  -- Patient First Name
			V002,  -- Patient Surname
			V003,  -- Initial Condition
			V004,  -- [Date of Original Surgery]
			V005,  -- [Follow Up Due]
			V006, -- Tick when done
			V007, -- Original Record ID
			V008, -- [Follow up time point]
			EnteredBy)
		SELECT 2485,
			P.V005 AS [Patient ID],
			P.V007 AS [Patient First Name],
			P.V008 AS [Patient Surname],
			P.V048 AS [Initial Condition],
			S.V085 AS [Date of Original Surgery],
			F.V003 AS [Follow Up Due],
			'No' AS [Tick when done],
			@RecordID as [Original Record ID],
			F.V004 as [Follow up time point],
			@UserID
			FROM [Record] F -- FollowUp
			LEFT JOIN [Record] P ON P.TableID = 2058 AND F.V001 = P.V005 and P.IsActive = 1-- Patient
			LEFT JOIN [Record] S ON S.TableID = 2063 AND S.V031 = P.V005 and S.IsActive = 1-- Surgery
			WHERE F.RecordID = @RecordID
END	

IF @TableID = 2146 -- 1.3.1 OG Cancer follow up
BEGIN
	print '2146'
	/*
	SELECT C.DisplayName, C.SystemName, C.Notes FROM [Column] C	WHERE C.TableID = 2485 order by C.SystemName
	SELECT C.DisplayName, C.SystemName, C.Notes FROM [Column] C	WHERE C.TableID = 2146
	*/
	INSERT INTO [Record] (TableID, 
			V009,  -- Patient ID number
			V001,  -- Patient First Name
			V002,  -- Patient Surname
			V003,  -- Initial Condition
			V004,  -- [Date of Original Surgery]
			V005,  -- [Follow Up Due]
			V006, -- Tick when done
			V007, -- Original Record ID
			V008, -- [Follow up time point]
			EnteredBy)
		SELECT 2485,
			P.V005 AS [Patient ID],
			P.V007 AS [Patient First Name],
			P.V008 AS [Patient Surname],
			P.V048 AS [Initial Condition],
			S.V085 AS [Date of Original Surgery],
			F.V002 AS [Follow Up Due],  --<-- table specific
			'No' AS [Tick when done],
			@RecordID as [Original Record ID],
			F.V003 as [Follow up time point], --<-- table specific
			@UserID
			FROM [Record] F -- FollowUp
			LEFT JOIN [Record] P ON P.TableID = 2058 AND F.V001 = P.V005 and P.IsActive = 1-- Patient --<-- check the F.
			LEFT JOIN [Record] S ON S.TableID = 2063 AND S.V031 = P.V005 and S.IsActive = 1-- Surgery
			WHERE F.RecordID = @RecordID
END	

IF @TableID = 2149	-- 2.2.1 Hiatus hernia follow up
BEGIN
	print '2149'
	/*
	SELECT C.DisplayName, C.SystemName, C.Notes FROM [Column] C	WHERE C.TableID = 2485 order by C.SystemName
	SELECT C.DisplayName, C.SystemName, C.Notes FROM [Column] C	WHERE C.TableID = 2149
	*/
	INSERT INTO [Record] (TableID, 
			V009,  -- Patient ID number
			V001,  -- Patient First Name
			V002,  -- Patient Surname
			V003,  -- Initial Condition
			V004,  -- [Date of Original Surgery]
			V005,  -- [Follow Up Due]
			V006, -- Tick when done
			V007, -- Original Record ID
			V008, -- [Follow up time point]
			EnteredBy)
		SELECT 2485,
			P.V005 AS [Patient ID],
			P.V007 AS [Patient First Name],
			P.V008 AS [Patient Surname],
			P.V048 AS [Initial Condition],
			S.V085 AS [Date of Original Surgery],
			F.V008 AS [Follow Up Due],  --<-- table specific
			'No' AS [Tick when done],
			@RecordID as [Original Record ID],
			F.V009 as [Follow up time point], --<-- table specific
			@UserID
			FROM [Record] F -- FollowUp
			LEFT JOIN [Record] P ON P.TableID = 2058 AND F.V004 = P.V005 and P.IsActive = 1-- Patient --<-- check the F. for [Patient ID number]
			LEFT JOIN [Record] S ON S.TableID = 2063 AND S.V031 = P.V005 and S.IsActive = 1-- Surgery
			WHERE F.RecordID = @RecordID
END	

IF @TableID = 2155 -- 3.2.1 Achalasia follow up
BEGIN
	print '2155'
	/*
	SELECT C.DisplayName, C.SystemName, C.Notes FROM [Column] C	WHERE C.TableID = 2485 order by C.SystemName
	SELECT C.DisplayName, C.SystemName, C.Notes FROM [Column] C	WHERE C.TableID = 2155
	*/
	INSERT INTO [Record] (TableID, 
			V009,  -- Patient ID number
			V001,  -- Patient First Name
			V002,  -- Patient Surname
			V003,  -- Initial Condition
			V004,  -- [Date of Original Surgery]
			V005,  -- [Follow Up Due]
			V006, -- Tick when done
			V007, -- Original Record ID
			V008, -- [Follow up time point]
			EnteredBy)
		SELECT 2485,
			P.V005 AS [Patient ID],
			P.V007 AS [Patient First Name],
			P.V008 AS [Patient Surname],
			P.V048 AS [Initial Condition],
			S.V085 AS [Date of Original Surgery],
			F.V002 AS [Follow Up Due],  --<-- table specific
			'No' AS [Tick when done],
			@RecordID as [Original Record ID],
			F.V003 as [Follow up time point], --<-- table specific
			@UserID
			FROM [Record] F -- FollowUp
			LEFT JOIN [Record] P ON P.TableID = 2058 AND F.V001 = P.V005 and P.IsActive = 1-- Patient --<-- check the F. for [Patient ID number]
			LEFT JOIN [Record] S ON S.TableID = 2063 AND S.V031 = P.V005 and S.IsActive = 1-- Surgery
			WHERE F.RecordID = @RecordID
END	

IF @TableID = 2285 -- 10.0.1 Hepatic Tumor Follow Up
BEGIN
	print '2285'
	/*
	SELECT C.DisplayName, C.SystemName, C.Notes FROM [Column] C	WHERE C.TableID = 2485 order by C.SystemName
	SELECT C.DisplayName, C.SystemName, C.Notes FROM [Column] C	WHERE C.TableID = 2285
	*/
	INSERT INTO [Record] (TableID, 
			V009,  -- Patient ID number
			V001,  -- Patient First Name
			V002,  -- Patient Surname
			V003,  -- Initial Condition
			V004,  -- [Date of Original Surgery]
			V005,  -- [Follow Up Due]
			V006, -- Tick when done
			V007, -- Original Record ID
			V008, -- [Follow up time point]
			EnteredBy)
		SELECT 2485,
			P.V005 AS [Patient ID],
			P.V007 AS [Patient First Name],
			P.V008 AS [Patient Surname],
			P.V048 AS [Initial Condition],
			S.V085 AS [Date of Original Surgery],
			F.V004 AS [Follow Up Due],  --<-- table specific
			'No' AS [Tick when done],
			@RecordID as [Original Record ID],
			F.V005 as [Follow up time point], --<-- table specific
			@UserID
			FROM [Record] F -- FollowUp
			LEFT JOIN [Record] P ON P.TableID = 2058 AND F.V002 = P.V005 and P.IsActive = 1-- Patient --<-- check the F. for [Patient ID number]
			LEFT JOIN [Record] S ON S.TableID = 2063 AND S.V031 = P.V005 and S.IsActive = 1-- Surgery
			WHERE F.RecordID = @RecordID
END	



IF @TableID = 2307 -- 11.0.1 Pancreatic / Periampullary tumour follow up
BEGIN
	print '2307'
	/*
	SELECT C.DisplayName, C.SystemName, C.Notes FROM [Column] C	WHERE C.TableID = 2485 order by C.SystemName
	SELECT C.DisplayName, C.SystemName, C.Notes FROM [Column] C	WHERE C.TableID = 2307
	*/
	INSERT INTO [Record] (TableID, 
			V009,  -- Patient ID number
			V001,  -- Patient First Name
			V002,  -- Patient Surname
			V003,  -- Initial Condition
			V004,  -- [Date of Original Surgery]
			V005,  -- [Follow Up Due]
			V006, -- Tick when done
			V007, -- Original Record ID
			V008, -- [Follow up time point]
			EnteredBy)
		SELECT 2485,
			P.V005 AS [Patient ID],
			P.V007 AS [Patient First Name],
			P.V008 AS [Patient Surname],
			P.V048 AS [Initial Condition],
			S.V085 AS [Date of Original Surgery],
			F.V004 AS [Follow Up Due],  --<-- table specific
			'No' AS [Tick when done],
			@RecordID as [Original Record ID],
			F.V005 as [Follow up time point], --<-- table specific
			@UserID
			FROM [Record] F -- FollowUp
			LEFT JOIN [Record] P ON P.TableID = 2058 AND F.V002 = P.V005 and P.IsActive = 1-- Patient --<-- check the F. for [Patient ID number]
			LEFT JOIN [Record] S ON S.TableID = 2063 AND S.V031 = P.V005 and S.IsActive = 1-- Surgery
			WHERE F.RecordID = @RecordID
END	

RETURN @@ERROR

