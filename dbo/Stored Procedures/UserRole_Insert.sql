﻿
 
CREATE PROCEDURE [dbo].[UserRole_Insert]
(
	@nNewID int output,
	@nUserID int ,
	@nRoleID int ,@nAccountID int, @bIsPrimaryAccount bit=1,
	@bIsAccountHolder bit=0,
	@bIsAdvancedSecurity bit=0,
	@bIsDocSecurityAdvanced bit=NULL,
	@sDocSecurityType varchar(20)=NULL,
	@nDashBoardDocumentID int=NULL,
	@nDataScopeColumnID int=NULL,
	@sDataScopeValue nvarchar(500) =NULL,
	@AllowDeleteTable bit =NULL,
	@AllowDeleteColumn bit =NULL,
	@AllowDeleteRecord bit= NULL
	
)
 
AS

BEGIN TRY 
	INSERT INTO [UserRole]
	(
		UserID, 
		RoleID,AccountID,IsPrimaryAccount,
		IsAccountHolder,IsAdvancedSecurity,
		IsDocSecurityAdvanced,DocSecurityType,DashBoardDocumentID,
		DataScopeColumnID,DataScopeValue,AllowDeleteTable,
		AllowDeleteColumn,AllowDeleteRecord
	) VALUES (
		@nUserID,
		@nRoleID,@nAccountID,@bIsPrimaryAccount,
		@bIsAccountHolder,@bIsAdvancedSecurity,
		@bIsDocSecurityAdvanced,@sDocSecurityType,@nDashBoardDocumentID,
		@nDataScopeColumnID,@sDataScopeValue,@AllowDeleteTable,
		@AllowDeleteColumn,@AllowDeleteRecord
	)
	SELECT @nNewID = @@IDENTITY
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('UserRole_Insert', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
