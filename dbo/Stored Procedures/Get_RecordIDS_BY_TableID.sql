﻿


CREATE PROCEDURE [dbo].[Get_RecordIDS_BY_TableID]
(
	@nTableID INT
)
AS


-- EXEC [dbo].[Get_RecordIDS_BY_TableID] 2924
BEGIN TRY

	DECLARE @sID varchar(MAX)=NULL
	SELECT @sID=COALESCE(@sID+',','')+CAST(RecordID AS varchar) FROM [Record] WHERE IsActive=1 AND TableID=@nTableID 
	SELECT @sID


END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Get_RecordIDS_BY_TableID', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
