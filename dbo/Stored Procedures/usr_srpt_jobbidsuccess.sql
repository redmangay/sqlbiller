﻿-- Author:		<Manoj sharma>
-- Create date: <2016-02-02 16:33:21.433>
-- Description:	<Used in ssrs report>

CREATE procedure [dbo].[usr_srpt_jobbidsuccess]
(
@From date,
@To date,
 @group varchar(max) =null,
 @controller varchar(max) =null


)
as



--#region calling temp table

exec  usr_rptjobbidsuccess_view

--#endregion

SELECT rptJobBidSuccess.GroupDivision, rptJobBidSuccess.Date AS JobDate, rptJobBidSuccess.Controller, rptJobBidSuccess.JobNo AS ProjectNumber, rptJobBidSuccess.Title AS ProjectTitle, rptJobBidSuccess.JobValue AS ProjectValue, rptJobBidSuccess.Status, rptJobBidSuccess.WonAmount
, rptJobBidSuccess.[Convert]
into #srptjobbidsuccess
FROM  ##rptJobBidSucces rptJobBidSuccess 
select * from #srptjobbidsuccess
where(
 GroupDivision is not null
 and convert(date,#srptjobbidsuccess.JobDate) between @From and @TO
 
 
  
 and ((@group ='ALL')  or (#srptjobbidsuccess.GroupDivision =@group)) and ((@controller ='ALL')  or (#srptjobbidsuccess.Controller =@controller))
 
 )
--WHERE (((rptJobBidSuccess.GUID)=getJobBidSuccessGUID()));
