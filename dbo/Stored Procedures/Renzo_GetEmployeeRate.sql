﻿-- =============================================
-- Author:		<Red Mangay>
-- Create date:	<28/11/2018>
-- Description:	<>
-- =============================================
/*

exec [Renzo_GetBAPATabBalances]  11538529

*/


CREATE PROCEDURE [dbo].[Renzo_GetEmployeeRate]
(
	@RecordID int
	
)
AS
BEGIN
	
	SELECT DesigRate.V002 as EmployeeRate
	   FROM Record EmpList
		  JOIN Record DesigRate ON DesigRate.TableID = 2712
		  AND DesigRate.IsActive = 1
		  AND DesigRate.v004 = '1'
		  AND DesigRate.V003 = CAST(EmpList.V021 as int)
	 WHERE EmpList.TableID = 2669
	   AND EmpList.IsActive = 1
	   AND EmpList.RecordID =  @RecordID
    
END

