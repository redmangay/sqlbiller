﻿-- Author:		<Manoj sharma>
-- Create date: <2016-02-02 16:33:21.433>
-- Description:	<Used as ssrs report>
CREATE procedure [dbo].[usr_rptCurrentJobList_27102018]
(
@rptcurrentjoblist int ,
@sgroup varchar(max)=null,
@pcontroller varchar(max)=null,
@Group_Controller int =1

)
as

exec usr_rpt_CurrentJobListGroupCurrency

declare @HaveGroupsSameCurrency int



SELECT    @HaveGroupsSameCurrency =COUNT(*) FROM         Account24918.vList_Of_Group_Division where ReportingCurrency is not null

--if(@HaveGroupsSameCurrency >0)
begin

SELECT NEWID() as Guid,ProjectNumber, [Group/Division] AS GroupDivision, Name, Approved, Invoiced, Balanced  as  Balance 
into #rptCurrentJobsList FROM  ##CurrentJobListGroupCurrency
 
 
 
 
if @rptcurrentjoblist =1
begin  
  
 SELECT Sum(rptCurrentJobsList.Approved) AS JobApproved,
  Sum(rptCurrentJobsList.Invoiced) AS JobInvoiced,
   Sum(rptCurrentJobsList.Balance) AS JobBalanced, 
   Count(*) AS Jobs, 
   rptCurrentJobsList.GroupDivision,
   rptCurrentJobsList.Guid,
   
    rptCurrentJobsList.Name as Controller, 
   isnull(ReportingCurrency,0) AS GroupCurrency
FROM  #rptCurrentJobsList rptCurrentJobsList LEFT JOIN   Account24918.vList_Of_Group_Division [List_Of_Group/Division] ON rptCurrentJobsList.GroupDivision = [List_Of_Group/Division].[Group/Division]
where (((rptCurrentJobsList.GroupDivision =@sgroup) or ( @sgroup in('ALL','NULL'))) and ((rptCurrentJobsList.Name = @pcontroller )or (@pcontroller='NULL') ))
GROUP BY rptCurrentJobsList.GroupDivision,  rptCurrentJobsList.Name, 
isnull(ReportingCurrency,0),rptCurrentJobsList.Guid
ORDER BY rptCurrentJobsList.GroupDivision;
end

 if @rptcurrentjoblist =2 
 begin
 
 SELECT 
  Sum(rptCurrentJobsList.Approved) AS JobApproved,
  Sum(rptCurrentJobsList.Invoiced) AS JobInvoiced,
   Sum(rptCurrentJobsList.Balance) AS JobBalanced, 
   min(ReportingCurrency) as ReportingCurrency,
   Count(*) AS Jobs, 
    rptCurrentJobsList.Name as Controller,  rptCurrentJobsList.GroupDivision, rptCurrentJobsList.Guid
FROM  #rptCurrentJobsList rptCurrentJobsList
LEFT JOIN   Account24918.vList_Of_Group_Division [List_Of_Group/Division] ON rptCurrentJobsList.GroupDivision = [List_Of_Group/Division].[Group/Division]
where (((rptCurrentJobsList.GroupDivision =@sgroup) or ( @sgroup in('ALL','NULL'))) and ((rptCurrentJobsList.Name = @pcontroller )or (@pcontroller='NULL') ))
GROUP BY  rptCurrentJobsList.Name, rptCurrentJobsList.GroupDivision, rptCurrentJobsList.Guid
ORDER BY  rptCurrentJobsList.Name

 
 end
 
 if @rptcurrentjoblist =3
 begin
 
 --Group
 if @Group_Controller ='2'
begin

 SELECT DISTINCT rptCurrentJobsList.GroupDivision, 1 AS id
FROM  #rptCurrentJobsList rptCurrentJobsList

end
	
else if @Group_Controller ='3'	
  begin
	SELECT        'NULL' AS GroupDivision, 0 AS id
ORDER BY id, GroupDivision
	end
	else
	SELECT        'All' AS GroupDivision, 0 AS id
ORDER BY id, GroupDivision




 --Group end



  
 end
 
if @rptcurrentjoblist =4
 begin
 
 --Controller

 
if @Group_Controller =3
begin
  SELECT DISTINCT rptCurrentJobsList.Name as Controller, 1 AS id
FROM  #rptCurrentJobsList rptCurrentJobsList

end
else
begin
SELECT        'NULL' AS Controller, 0 AS id
ORDER BY id, Controller
end

 --Controller





 
  
 end
  
 
  
  
  
  
end