﻿
-- =============================================
-- Author:		Dathq
-- Create date: 16 Jun 2012
-- Description:	Get Record site as series for chart section
-- =============================================
CREATE PROCEDURE [dbo].[dgChartSection_GetSeries] 
	-- Add the parameters for the stored procedure here
	@TableID int
AS

BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	A.LocationID AS Value, 
			A.LocationName AS Title
	FROM	Location AS A INNER JOIN LocationTable AS B 
	ON		A.LocationID = B.LocationID
	WHERE	A.IsActive = 1 AND B.TableID = @TableID
	ORDER BY A.LocationName
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dgChartSection_GetSeries', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
