﻿
 
CREATE PROCEDURE [dbo].[Role_Select]
(
	@nRoleID int  = NULL,
	@nAccountID int  = NULL,
	@bIsSystemRole bit=NULL,
	@bIsActive bit =NULL,
	@sRole nvarchar(200)  = NULL,
	@sRoleTye char(1)  = NULL,
	@sRoleNotes nvarchar(150)  = NULL,
	@dDateAdded datetime  = NULL,
	@dDateUpdated datetime  = NULL,
	@sOrder nvarchar(200) = Role, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647
)
	/*----------------------------------------------------------------
	-- Stored Procedure: Role_Select
	-- Generated by: gpGenerateSP (Jon Bosker, DB Gurus Australia) 
	-- Date: Jan 14 2011 11:26AM
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'
	IF @nAccountID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND AccountID = '+CAST(@nAccountID AS NVARCHAR)

	IF @nAccountID IS NULL 
		SET @sWhere = @sWhere + ' AND AccountID IS NULL '

	IF @bIsSystemRole IS NOT NULL 
		SET @sWhere = @sWhere + ' AND IsSystemRole = '+CAST(@bIsSystemRole AS NVARCHAR)

	IF @bIsActive IS NOT NULL 
		SET @sWhere = @sWhere + ' AND IsActive = '+CAST(@bIsActive AS NVARCHAR)

	IF @nRoleID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND RoleID = '+CAST(@nRoleID AS NVARCHAR)
	IF @sRole IS NOT NULL 
		SET @sWhere = @sWhere + ' AND Role LIKE '+'''%' + @sRole + '%'''
	IF @sRoleTye IS NOT NULL 
		SET @sWhere = @sWhere + ' AND RoleType = '''+ + @sRoleTye + ''''
	IF @sRoleNotes IS NOT NULL 
		SET @sWhere = @sWhere + ' AND RoleNotes LIKE '+'''%' + @sRoleNotes + '%'''
	IF @dDateAdded IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DateAdded = '+CAST(@dDateAdded AS NVARCHAR)
	IF @dDateUpdated IS NOT NULL 
		SET @sWhere = @sWhere + ' AND DateUpdated = '+CAST(@dDateUpdated AS NVARCHAR)
	SET @sSelect = 'SELECT * FROM 
	(SELECT [Role].*, ROW_NUMBER() OVER(ORDER BY ' + @sOrder + ') as RowNum FROM [Role]' + @sWhere + ') 
	as UserInfo Where RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [Role] ' + @sWhere 
 
	-- Extend WHERE to include paging:
	--SET @sWhere = @sWhere + ' AND RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	--SET @sOrder = ' ORDER BY ' + @sOrder
	--SET @sSelect = @sSelect + @sWhere + @sOrder
 
	EXEC (@sSelect)
	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('Role_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
