﻿-- =============================================
-- Author:		Barry Matutina
-- Create date: 09/25/2016
-- Description:	Updates Job Number
-- =============================================
CREATE PROCEDURE [dbo].[Renzo_IsJobInvoioce] 
	
	@RecordID  int,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output
AS

BEGIN
	
	DECLARE		@JOBINVOICE		INT

	/*	Get the most recent record	*/
	 
	SELECT		@JOBINVOICE =  [Bill & Payment Activity].V050						/*	[Bill & Payment Activity]	*/
	FROM		[Record] [Bill & Payment Activity] WITH (NOLOCK)
	WHERE		[Bill & Payment Activity].TableID = '2486'						/*	Bill & Payment Activity	*/
	AND			[Bill & Payment Activity].IsActive = 1
	AND			[Bill & Payment Activity].RecordID = @RecordID

	

/*	Update the last job number	*/

	UPDATE	    [Bill & Payment Activity] 			/*	[Bill & Payment Activity]	*/
	SET			V050 = @JOBINVOICE
	FROM		[Record] [Job Information]  
	WHERE		[Job Information].RecordID = @RecordID	
	AND			[Job Information].TableID = '2486'		/*	Bill & Payment Activity	*/
	AND			[Job Information].IsActive = 1	
	
	RETURN 	@@ERROR	/*	Mandatory	*/
END
