﻿
CREATE PROCEDURE [dbo].[ets_GraphOptionDetail_Select]
(
	@nGraphOptionID int  = NULL,
	@nTableID int  = NULL,
	@sOrder nvarchar(200) = GraphOptionDetailID, 
	@nStartRow int = 1, 
	@nMaxRows int = 2147483647 
)
	/*----------------------------------------------------------------
	ets_GraphOptionDetail_Select @nGraphOptionID=65, @nStartRow=2,@nMaxRows=1
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SET NOCOUNT ON
	SET CONCAT_NULL_YIELDS_NULL OFF
	SET ROWCOUNT @nMaxRows
	-- DECLARE local variables
	DECLARE @sSelect nvarchar(MAX)
	DECLARE @sSelectCount nvarchar(MAX)
	DECLARE @sWhere nvarchar(MAX)
	-- Build WHERE clause
	SET @sWhere = ' WHERE 1=1'

	IF @nGraphOptionID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND GraphOptionID = '+CAST(@nGraphOptionID AS NVARCHAR)
	IF @nTableID IS NOT NULL 
		SET @sWhere = @sWhere + ' AND TableID = '+CAST(@nTableID AS NVARCHAR)

	SET @sSelect = 'SELECT * FROM (SELECT     GraphOptionDetail.GraphOptionDetailID, GraphOptionDetail.GraphOptionID, GraphOptionDetail.TableID, GraphOptionDetail.ColumnID, 
	GraphOptionDetail.Label, GraphOptionDetail.Axis, GraphOptionDetail.GraphOrder, GraphOptionDetail.Scale, GraphOptionDetail.GraphType, GraphOptionDetail.Colour, 
	GraphOptionDetail.High, GraphOptionDetail.Low, [Table].TableName, [Column].SystemName, 
	[Column].DisplayName, [Column].GraphLabel, [Column].IgnoreSymbols, 
	GraphOptionDetail.GraphSeriesColumnID, GraphSeriesID, TrueTableID,
	ROW_NUMBER() OVER(ORDER BY GraphOrder) as RowNum
	FROM         GraphOptionDetail INNER JOIN
	[Table] ON GraphOptionDetail.TableID = [Table].TableID INNER JOIN
	[Column] ON GraphOptionDetail.ColumnID = [Column].ColumnID  
	 AND [Table].TableID = [Column].TableID) AS GD
	' + @sWhere + ''
	SET @sSelectCount = 'SELECT COUNT(*) AS TotalRows FROM [GraphOptionDetail] ' + @sWhere 
 
	-- Extend WHERE to include paging:
	--SET @sWhere = @sWhere + ' AND RowNum >= ' + CONVERT(nvarchar(10), @nStartRow)
	--SET @sOrder = ' ORDER BY ' + @sOrder
	SET @sSelect = @sSelect + ' AND RowNum >= ' + CONVERT(nvarchar(10), @nStartRow) 
	 PRINT @sSelect

	EXEC (@sSelect)
	SET ROWCOUNT 0
 
	PRINT @sSelectCount
	EXEC (@sSelectCount)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_GraphOptionDetail_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH

-----------------------------------------------------------------------

