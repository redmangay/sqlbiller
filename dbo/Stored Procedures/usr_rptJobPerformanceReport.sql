﻿-- =============================================
-- Author:		<Red Mangay>
-- Create date: <19 Jan 2018>
-- Job Performance Report
-- =============================================
CREATE PROCEDURE [dbo].[usr_rptJobPerformanceReport]
	  @ReportType				int
	, @ProjectNumbers			int = NULL
	, @ProjectNumbers_Second	int =null
	, @StartDate				DATE =null
	, @EndDate					DATE =null
	, @GroupDivision			NVARCHAR(255) =null -- All or selected: if null, all group given it is not selected job number
	, @Controller				NVARCHAR(255) =null
AS
BEGIN

/*
============================
Test:

EXEC [dbo].[usr_rptJobPerformanceReport] 
	@ReportType = 0
	,@ProjectNumbers =   12954842
	--, @ProjectNumbers_Second=  2715879
	--, @StartDate = '01/11/2018' 
	--, @EndDate = '30/11/2018'
	--, @Controller = 1004044
	--, @GroupDivision = 'All Groups'


-- ==========================================================

@ReportType
	0	: Selected Job Number
	1	: StartDate, EndDate, Jobs With Invoices Dated, All Groups
	2	: StartDate, EndDate, Jobs With Invoices Dated, Selected Group
	3	: StartDate, EndDate, Jobs With Invoices Dated, Job Controller
	4	: StartDate, EndDate, Completed Jobs, All Groups
	5	: StartDate, EndDate, Completed Jobs, Selected Group
	6	: StartDate, EndDate, Completed Jobs, Job Controller
	7	: Current Jobs, All Groups
	8	: Current Jobs, Selected Group
	9	: Current Jobs, Job Controller

-- ==========================================================

*/

SET DATEFORMAT dmy
--declare @ReportType				int = 0
--INITIALIZE PARAMETERS
			IF @ReportType = 0
			BEGIN	
				SET @StartDate = NULL
				SET @EndDate = NULL
				SET @GroupDivision = NULL
				SET @Controller = NULL	
			END
			
			IF @ReportType = 1 -- All Groups
			BEGIN
				--print 'All Groups'
				IF  @GroupDivision IS NOT NULL
				BEGIN
				IF  @GroupDivision <>  'All Groups'
				BEGIN
					SET @ReportType = 2 -- Selected Group
					--SET @StartDate = NULL
					--SET @EndDate = NULL
					--SET @GroupDivision = NULL
					SET @Controller = NULL
					--PRINT 'SELECTED GROUP'
					END
				END

				ELSE IF @Controller IS NOT NULL
				BEGIN
					SET @ReportType = 3
					--SET @StartDate = NULL
					--SET @EndDate = NULL
					SET @GroupDivision = NULL
					--SET @Controller = NULL
					--PRINT 'Controller'
				END
	
				
	
			END


			IF @ReportType = 4 
			BEGIN
		
				IF @GroupDivision IS NOT NULL
				BEGIN
				    IF @GroupDivision <>  'All Groups' 
				    BEGIN
					SET @ReportType = 5
					--SET @StartDate = NULL
					--SET @EndDate = NULL
					--SET @GroupDivision = NULL
					SET @Controller = NULL
					END
				END

				ELSE IF @Controller IS NOT NULL
				BEGIN
					SET @ReportType = 6
					--SET @StartDate = NULL
					--SET @EndDate = NULL
					SET @GroupDivision = NULL
					--SET @Controller = NULL
				END
	
			END

			IF @ReportType = 7 
			BEGIN
	
				IF  @GroupDivision IS NOT NULL
				BEGIN
				    IF @GroupDivision <>  'All Groups'
				    BEGIN
					SET @ReportType = 8
					--SET @StartDate = NULL
					--SET @EndDate = NULL
					--SET @GroupDivision = NULL
					SET @Controller = NULL
				    END
				END

				ELSE IF @Controller IS NOT NULL
				BEGIN
					SET @ReportType = 9
					--SET @StartDate = NULL
					--SET @EndDate = NULL
					SET @GroupDivision = NULL
					--SET @Controller = NULL
				END
	
			END

    
    CREATE TABLE #ReportTable  (
			  [Counter] int identity(1,1)
			, [Date] date
			, [Timesheet Date] date
			, [Details] varchar(max)
			, [Initials] varchar(10)
			, [Timesheet] money
			, [Status] varchar(max)  
			, [Billed] money
			, [Percentage_Billed] float 
			, [Unbilled] money
			, [Percentage_Unbilled] float 
			, [Job Value] money
			, [Currency] varchar(10)
			, [Job Controller] varchar(max)
			, [Job Title] varchar(max)
			, [Invoice No] varchar(max)
			, [Job No] varchar(max)
			, [Invoice Amount] money
			, [TaxAmount] MONEY
			, [TaxRate] float
			, [TaxName] varchar(max)
			, [Report Section] varchar(max)
			
			)



	--DECLARE #ReportTable Table (
	--		  [Counter] int identity(1,1)
	--		, [Date] date, [Details] varchar(max)
	--		, [Initials] varchar(10)
	--		, [Timesheet] money
	--		, [Status] varchar(max)  
	--		, [Billed] money
	--		, [Percentage_Billed] float
	--		, [Unbilled] money
	--		, [Percentage_Unbilled] float
	--		, [Job Value] money
	--		, [Currency] varchar(10)
	--		, [Job Controller] varchar(max)
	--		, [Job Title] varchar(max)
	--		, [Invoice No] varchar(max)
	--		, [Job No] varchar(max)
	--		, [Invoice Amount] money
	--		, [TaxAmount] MONEY
	--		, [TaxRate] float
	--		, [TaxName] varchar(max)
	--		, [Report Section] varchar(max)
			
	--		)

	DECLARE @RecordIDs varchar(max)
	DECLARE @ClientID varchar(max)

-- ==========================================================
-- Single or Multiple Job Numbers
-- ==========================================================
	IF @ProjectNumbers IS NOT NULL
	BEGIN

	SELECT @ClientID = V024 FROM RECORD WHERE RECORDID = @ProjectNumbers

		IF @ProjectNumbers_Second IS NULL 
		BEGIN
			SET @RecordIDs = CAST(@ProjectNumbers AS VARCHAR(MAX))
		END
		ELSE 
		BEGIN
			IF @ProjectNumbers > @ProjectNumbers_Second
			BEGIN
		
				SELECT  @RecordIDs = LEFT(RecordIDs, LEN(RecordIDs) - 1) 
						FROM ( SELECT CONVERT(VARCHAR(50), [RecordID]) + ',' FROM [Record] WHERE
						RecordID >= @ProjectNumbers_Second AND RecordID <= @ProjectNumbers AND TableID = 2475 AND V024 =CAST(@ClientID AS VARCHAR) FOR XML PATH ('') )   C (RecordIDs)
			END
			ELSE 
			BEGIN
				SELECT  @RecordIDs = LEFT(RecordIDs, LEN(RecordIDs) - 1) 
						FROM ( SELECT CONVERT(VARCHAR(50), [RecordID]) + ',' FROM [Record] WHERE
						RecordID >= @ProjectNumbers AND RecordID <= @ProjectNumbers_Second AND TableID = 2475 AND V024 =CAST(@ClientID AS VARCHAR) FOR XML PATH ('') )   C (RecordIDs)
			END
		END
	END
-- ==========================================================

-- ==========================================================
	-- Report Type 0
-- ==========================================================	
	-- Invoiced	Record
	IF @ReportType = 0
	
	BEGIN
		INSERT INTO #ReportTable
		--declare @Rate float = .1
			SELECT 
				    [Bill & Payment Activity].V004 AS [Date]-- DATE OF INVOICE
				  , [Timesheet Details].V009 AS [Timesheet Date] 
				  , [Timesheet Details].V003 AS [Details]   --, Get DETAILS in TS Details - 2729
				  , [EmployeeList].V004 AS [Initials] --, Get INITIALS from Time
				  , ISNULL(CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) /*convert(money,[Group Currency Rates].V002)*/ AS MONEY),0) AS [TimeSheet]--, Get the Timesheet (Amount) from TS Details
				  , [Timesheet Details].V008 AS [Status]
				  , ISNULL(CAST(CONVERT(MONEY,[Timesheet Details].V020) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) /*convert(money,[Group Currency Rates].V002)*/ AS MONEY),0) AS [Billed]--, Get the Billed from TS Details
				  , ISNULL((CAST([Timesheet Details].V020 AS MONEY) / CAST([Timesheet Details].V007 AS MONEY)) * 100,0) AS [Percentage Billed]--, Billed / Timesheet (Amount) * 100 
				  , cast(round(CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) /* convert(money,[Group Currency Rates].V002) */ AS MONEY) - CAST(CONVERT(MONEY,[Timesheet Details].V020) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),2,1) as NUMERIC(18,2)  ) AS [Unbilled] --, Timesheet - Billed
				  , ISNULL((CAST([Timesheet Details].V007 AS MONEY) - CAST([Timesheet Details].V020 AS MONEY))/ CAST([Timesheet Details].V007 AS MONEY) * 100,0) AS [Percentage Unbilled]--, (Timesheet (Amount) - Billed) / Timesheet (Amount) * 100
				  , CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1)) /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Job Value]--, Job Value from Job Information
				  , [Currency Rates].V001 AS [Currency]--, Currency, get from Job Informaiton, from Currency Rate field Currency
				  , [EmployeeList_Job Controller].V003 AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
				  , [Job Information].V003 AS [Job Title]--Job Title, get from Job Information V003
				  , [Bill & Payment Activity].V017  AS [Invoice No] 
				  , [Job Information].V001 AS [Job No]
				  , CAST(Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Invoice Amount]
				  , CAST(Round((Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / (1 + ([Job Information].V011 / 100.00))) * [Job Information].V011 / 100.00,2)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [TaxAmount]--CAST([Bill & Payment Activity].V063 AS NUMERIC(18,2)) AS [GST]
				  , [Job Information].V011 AS [TaxRate]
				  , [Tax Name].V001 AS [Tax Name]
				  , 'Invoiced'
				  
				  
			FROM [Record] [Job Information] WITH (NOLOCK) 

			LEFT JOIN [Record] [Bill & Payment Activity] ON [Bill & Payment Activity].TableID = 2711
				AND		[Bill & Payment Activity].IsActive = 1
				AND		[Bill & Payment Activity].V002 = CAST([Job Information].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		[Bill & Payment Activity].V029 = 'Invoice'
				

			LEFT JOIN [Record] [Timesheet Details] WITH (NOLOCK) ON [Timesheet Details].TableID = 2729
				AND		[Timesheet Details].IsActive = 1
				AND		[Timesheet Details].V016 = CAST([Bill & Payment Activity].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		CAST([Timesheet Details].V007 AS MONEY) IS NOT NULL
				AND		CAST([Timesheet Details].V020 AS MONEY) IS NOT NULL
				AND	     CONVERT(MONEY,[Timesheet Details].V007) >0
				AND	     CONVERT(MONEY,[Timesheet Details].V020) >0
		
			LEFT JOIN [Record] [EmployeeList] WITH (NOLOCK) ON [EmployeeList].TableID = 2669
				AND		[EmployeeList].IsActive = 1
				AND		[EmployeeList].RecordID = CAST([Timesheet Details].V002 as int) 		/*	Job Number	*/

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		[EmployeeList_Job Controller].RecordID = CAST([Job Information].V026 as int)		/*	Job Number	*/

			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		[Currency Rates].RecordID = CAST([Job Information].V034 as int)		/*	Job Number	*/

			LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		[Tax Name].RecordID = CAST([Job Information].V042 as int) 		/*	Job Number	*/
			 
			LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
				AND			[Job Group].IsActive = 1
				AND		[Job Group].RecordID = CAST([Job Information].V027 as int)		/*	Currency	*/

			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/

					WHERE [Job Information].TableID = 2475 AND
					[Job Information].IsActive = 1 
					AND ISNUMERIC(ISNULL([Job Information].V008,0)) = 1
				     AND ISNUMERIC(ISNULL([Job Information].V007,0)) = 1 
					--AND CONVERT(MONEY,ISNULL([Job Information].[V008],0)) > 0 
					--AND CONVERT(MONEY,ISNULL([Job Information].[V007],0)) > 0 					
					AND [Job Information].RecordID IN (SELECT Int_Value FROM DBO.fn_ParseText2Table(@RecordIDs,','))
						--ORDER BY [Bill & Payment Activity].V017 

--===============================================================

	-- Current Records
	INSERT INTO #ReportTable ([Date],[Details],[Initials],[Timesheet],[Status],[Billed],[Job No],[TaxRate],[TaxName],[Report Section])
		SELECT 	
		   CAST([Timesheet Details].V009 AS DATE) AS [Date Current]-- DATE OF INVOICE
		  ,[Timesheet Details].V003 AS [Details Current]   --, Get DETAILS in TS Details - 2729
		  ,[EmployeeList].V004 AS [Initials Current] --, Get INITIALS from Time
		  ,CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY)  AS [TimeSheet Current]
		  --,CAST([Timesheet Details].V007 AS MONEY) AS [TimeSheet Current]--, Get the Timesheet (Amount) from TS Details
		  ,[Timesheet Details].V008 AS [Status Current]
		  ,CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY)  AS [TimeSheet Current]
		  --,CAST([Timesheet Details].V007 AS MONEY) AS [Billable Current]--, Get the Timesheet (Amount) from TS Details
		  ,[Job Information].V001 AS [Job No]
		  , [Job Information].V011 AS [TaxRate]
		  , [Tax Name].V001 AS [Tax Name]
		  , 'Current'

		FROM [Record] [Job Information] WITH (NOLOCK) 		

			INNER JOIN [Record] [Timesheet Details] WITH (NOLOCK) ON [Timesheet Details].TableID = 2729
				AND		[Timesheet Details].IsActive = 1
				AND		[Timesheet Details].V016 IS NULL		/*	Job Number	*/
				AND		[Timesheet Details].V001 = CAST([Job Information].RecordID AS VARCHAR(MAX))

			LEFT JOIN [Record] [EmployeeList] WITH (NOLOCK) ON [EmployeeList].TableID = 2669
				AND		[EmployeeList].IsActive = 1
				AND		[EmployeeList].RecordID = CAST([Timesheet Details].V002 as int) 		/*	Job Number	*/

			LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		[Tax Name].RecordID = CAST([Job Information].V042 as int)		/*	Job Number	*/

			LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
				AND			[Job Group].IsActive = 1
				AND		[Job Group].RecordID = CAST([Job Information].V027 as int)		/*	Currency	*/

			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/


							
		WHERE [Job Information].TableID = 2475 AND
			  [Job Information].IsActive = 1 AND	
			  [Job Information].RecordID IN (SELECT Int_Value FROM DBO.fn_ParseText2Table(@RecordIDs,','))
					ORDER BY CAST([Timesheet Details].V009 AS DATE) DESC
	
	
	--===============================================================

	-- Credit Records
	--INSERT INTO #ReportTable
	--	--declare @Rate float = .1
	--		SELECT 
	--				[Bill & Payment Activity].V004 AS [Date]-- DATE OF INVOICE
	--			  , [Timesheet Details].V003 AS [Details]   --, Get DETAILS in TS Details - 2729
	--			  , [EmployeeList].V004 AS [Initials] --, Get INITIALS from Time
	--			  , CAST([Timesheet Details].V007 AS MONEY) AS [TimeSheet]--, Get the Timesheet (Amount) from TS Details
	--			  , [Timesheet Details].V008 AS [Status]
	--			  , CAST([Timesheet Details].V020 AS MONEY) AS [Billed]--, Get the Billed from TS Details
	--			  , (CAST([Timesheet Details].V020 AS MONEY) / CAST([Timesheet Details].V007 AS MONEY)) * 100 AS [Percentage Billed]--, Billed / Timesheet (Amount) * 100 
	--			  , cast(round(CAST([Timesheet Details].V007 AS MONEY) - CAST([Timesheet Details].V020 AS MONEY),2,1) as NUMERIC(18,2)  ) AS [Unbilled] --, Timesheet - Billed
	--			  , (CAST([Timesheet Details].V007 AS MONEY) - CAST([Timesheet Details].V020 AS MONEY))/ CAST([Timesheet Details].V007 AS MONEY) * 100 AS [Percentage Unbilled]--, (Timesheet (Amount) - Billed) / Timesheet (Amount) * 100
	--			  , [Job Information].V008 AS [Job Value]--, Job Value from Job Information
	--			  , [Currency Rates].V001 AS [Currency]--, Currency, get from Job Informaiton, from Currency Rate field Currency
	--			  , [EmployeeList_Job Controller].V003 AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
	--			  , [Job Information].V003 AS [Job Title]--Job Title, get from Job Information V003
	--			  , [Bill & Payment Activity].V017  AS [Invoice No] 
	--			  , [Job Information].V001 AS [Job No]
	--			  , Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2)  AS [Invoice Amount]
	--			  , Round((Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / (1 + ([Job Information].V011 / 100.00))) * [Job Information].V011 / 100.00,2) AS [TaxAmount]--CAST([Bill & Payment Activity].V063 AS NUMERIC(18,2)) AS [GST]
	--			  , [Job Information].V011 AS [TaxRate]
	--			  , [Tax Name].V001 AS [Tax Name]
	--			  , 'Credit'
				  
				  
	--		FROM [Record] [Job Information] WITH (NOLOCK) 

	--		LEFT JOIN [Record] [Bill & Payment Activity] ON [Bill & Payment Activity].TableID = 2711
	--			AND		[Bill & Payment Activity].IsActive = 1
	--			AND		[Bill & Payment Activity].V002 = CAST([Job Information].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
	--			AND		[Bill & Payment Activity].V029 = 'Credit Entry'
				

	--		LEFT JOIN [Record] [Timesheet Details] WITH (NOLOCK) ON [Timesheet Details].TableID = 2729
	--			AND		[Timesheet Details].IsActive = 1
	--			AND		[Timesheet Details].V016 = CAST([Bill & Payment Activity].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
	--		--	AND		CAST([Timesheet Details].V007 AS MONEY) IS NOT NULL
	--		--	AND		CAST([Timesheet Details].V020 AS MONEY) IS NOT NULL
		
	--		LEFT JOIN [Record] [EmployeeList] WITH (NOLOCK) ON [EmployeeList].TableID = 2669
	--			AND		[EmployeeList].IsActive = 1
	--			AND		CAST([EmployeeList].RecordID AS VARCHAR(MAX)) = [Timesheet Details].V002 		/*	Job Number	*/

	--		LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
	--			AND		[EmployeeList_Job Controller].IsActive = 1
	--			AND		CAST([EmployeeList_Job Controller].RecordID AS VARCHAR(MAX)) = [Job Information].V026 		/*	Job Number	*/

	--		LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
	--			AND		[Currency Rates].IsActive = 1
	--			AND		CAST([Currency Rates].RecordID AS VARCHAR(MAX)) = [Job Information].V034 		/*	Job Number	*/

	--		LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
	--			AND		[Tax Name].IsActive = 1
	--			AND		CAST([Tax Name].RecordID AS VARCHAR(MAX)) = [Job Information].V042 		/*	Job Number	*/

	--				WHERE [Job Information].TableID = 2475 AND
	--				[Job Information].IsActive = 1 AND
	--				[Job Information].RecordID IN (SELECT Int_Value FROM DBO.fn_ParseText2Table(@RecordIDs,','))
	--					ORDER BY [Bill & Payment Activity].V017 
	
	
	
	END

-- ==========================================================
	-- End Report Type 0
-- ==========================================================		
--
-- ==========================================================
	-- Report Type 1
-- ==========================================================	
	IF @ReportType = 1  --AND @GroupDivision IS NULL
	BEGIN
		INSERT INTO #ReportTable
			SELECT 
				--  [Job Group].V001,[Job Information].V027, [EmployeeList].V003,[Job Information].V021,
				   [Bill & Payment Activity].V004 AS [Date]-- DATE OF INVOICE
				   , [Timesheet Details].V009 AS [Timesheet Date]  
				  , [Timesheet Details].V003 AS [Details]   --, Get DETAILS in TS Details - 2729
				  , [EmployeeList].V004 AS [Initials] --, Get INITIALS from Time
				  , ISNULL(CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),0) AS [TimeSheet]--, Get the Timesheet (Amount) from TS Details
				  --, CAST([Timesheet Details].V007 AS MONEY) AS [TimeSheet]--, Get the Timesheet (Amount) from TS Details
				  , [Timesheet Details].V008 AS [Status]
				  , ISNULL(CAST(CONVERT(MONEY,[Timesheet Details].V020) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),0) AS [Billed]--, Get the Billed from TS Details
				  --, CAST([Timesheet Details].V020 AS MONEY) AS [Billed]--, Get the Billed from TS Details
				  , ISNULL(CASE WHEN CAST([Timesheet Details].V020 AS MONEY) <=0 AND CAST([Timesheet Details].V007 AS MONEY) <=0 THEN 0 ELSE (CAST(isnull([Timesheet Details].V020,0) AS MONEY) / CAST(isnull([Timesheet Details].V007,0) AS MONEY)) * 100 END,0) AS [Percentage Billed]--, Billed / Timesheet (Amount) * 100 
				  , cast(round(CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) - CAST(CONVERT(MONEY,[Timesheet Details].V020) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),2,1) as NUMERIC(18,2)  ) AS [Unbilled] --, Timesheet - Billed
				  --, CAST(isnull([Timesheet Details].V007,0) AS MONEY) - CAST(isnull([Timesheet Details].V020,0) AS MONEY) AS [Unbilled] --, Timesheet - Billed
				  , ISNULL(CASE WHEN CAST([Timesheet Details].V020 AS MONEY) <=0 AND CAST([Timesheet Details].V007 AS MONEY) <=0 THEN 0 ELSE (CAST(isnull([Timesheet Details].V007,0) AS MONEY) - CAST(isnull([Timesheet Details].V020,0) AS MONEY))/ CAST(isnull([Timesheet Details].V007,0) AS MONEY) * 100 END,0) AS [Percentage Unbilled]--, (Timesheet (Amount) - Billed) / Timesheet (Amount) * 100
				  , CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Job Value]--, Job Value from Job Information
				  --, [Job Information].V008 AS [Job Value]--, Job Value from Job Information
				  , [Currency Rates].V001 AS [Currency]--, Currency, get from Job Informaiton, from Currency Rate field Currency
				  , [EmployeeList_Job Controller].V003 AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
				  , [Job Information].V003 AS [Job Title]--Job Title, get from Job Information V003
				  , [Bill & Payment Activity].V017  AS [Invoice No] 
				  , [Job Information].V001 AS [Job No]
				  , CAST(Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY) + cast(ISNULL([Bill & Payment Activity Interim].V005,0) AS MONEY)  ),2) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Invoice Amount]
				  --, Round(convert(varchar(max),cast([Bill & Payment Activity].V005 as money)),2)  AS [Invoice Amount]
				  , CAST(Round((Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY) + cast(ISNULL([Bill & Payment Activity Interim].V005,0) AS MONEY) ),2) / (1 + ([Job Information].V011 / 100.00))) * [Job Information].V011 / 100.00,2)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [TaxAmount]--CAST([Bill & Payment Activity].V063 AS NUMERIC(18,2)) AS [GST]
				  --, Round((Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / (1 + ([Job Information].V011 / 100.00))) * [Job Information].V011 / 100.00,2) AS [TaxAmount]--CAST([Bill & Payment Activity].V063 AS NUMERIC(18,2)) AS [GST]
				  , [Job Information].V011 AS [TaxRate]
				  , [Tax Name].V001 AS [Tax Name]
				  , 'Invoiced'

			FROM [Record] [Job Information] WITH (NOLOCK) 

			JOIN [Record] [Bill & Payment Activity] ON [Bill & Payment Activity].TableID = 2711
				AND		[Bill & Payment Activity].IsActive = 1
				AND		[Bill & Payment Activity].V002 = CAST([Job Information].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		cast([Bill & Payment Activity].V004 as date) >= @StartDate -- '01/01/2018'
				AND		cast([Bill & Payment Activity].V004 as date) <= @EndDate --'31/01/2018'
				AND		[Bill & Payment Activity].V017 IS NOT NULL
				AND		[Bill & Payment Activity].V029 = 'Invoice'

			 LEFT JOIN [Record] [Bill & Payment Activity Interim] ON [Bill & Payment Activity Interim].TableID = 2711
				AND		[Bill & Payment Activity Interim].IsActive = 1
				AND		[Bill & Payment Activity Interim].V002 = CAST([Job Information].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		 [Bill & Payment Activity Interim].V061 =  [Bill & Payment Activity].RecordID
				--AND		cast([Bill & Payment Activity Interim].V004 as date) >= @StartDate
				--AND		cast([Bill & Payment Activity Interim].V004 as date) <= @EndDate
				--AND		[Bill & Payment Activity Interim].V017 IS NOT NULL
				AND		[Bill & Payment Activity Interim].V029 = 'Interim Invoice'

			JOIN [Record] [Timesheet Details] WITH (NOLOCK) ON [Timesheet Details].TableID = 2729
				AND		[Timesheet Details].IsActive = 1
				AND		[Timesheet Details].V016 = CAST([Bill & Payment Activity].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		CAST([Timesheet Details].V007 AS MONEY) IS NOT NULL
				AND		CAST([Timesheet Details].V020 AS MONEY) IS NOT NULL
				AND	     CONVERT(MONEY,[Timesheet Details].V007) >0
				AND	     CONVERT(MONEY,[Timesheet Details].V020) >0
		
			LEFT JOIN [Record] [EmployeeList] WITH (NOLOCK) ON [EmployeeList].TableID = 2669
				AND		[EmployeeList].IsActive = 1
				AND		[EmployeeList].RecordID = CAST([Timesheet Details].V002 as int) 		/*	Job Number	*/

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		[EmployeeList_Job Controller].RecordID = CAST([Job Information].V026 as int) 		/*	Job Number	*/

			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		[Currency Rates].RecordID = CAST([Job Information].V034 as int) 		/*	Job Number	*/
			
			LEFT JOIN [Record] [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727
				AND		[Job Group].IsActive = 1
				AND		[Job Group].RecordID = CAST([Job Information].V027 as int) 		/*	Job Number	*/

			LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		[Tax Name].RecordID = CAST([Job Information].V042 as int)		/*	Job Number	*/

		  --	LEFT JOIN Record [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727		/*	Group	*/
				--AND			[Job Group].IsActive = 1
				--AND		CAST( [Job Group].RecordID AS VARCHAR(MAX)) = [Job Information].V027		/*	Currency	*/

			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/

					WHERE [Job Information].TableID = 2475 AND
					[Job Information].IsActive = 1 --AND
					--AND [Job Information].[V008] IS NOT NULL
					--AND [Job Information].[V007] IS NOT NULL
					--AND ISNUMERIC([Job Information].[V008]) = 1
					--AND ISNUMERIC([Job Information].[V007]) = 1
					AND ISNUMERIC(ISNULL([Job Information].V008,0)) = 1
				     AND ISNUMERIC(ISNULL([Job Information].V007,0)) = 1 
					--AND CONVERT(MONEY,ISNULL([Job Information].[V007],0)) > 0 
					--AND CONVERT(MONEY,ISNULL([Job Information].[V008],0)) > 0 
					--[Job Information].RecordID IN (SELECT Int_Value FROM DBO.fn_ParseText2Table(@RecordIDs,','))
						--ORDER BY [Bill & Payment Activity].V017 
	END
-- ==========================================================
	-- End Report Type 1
-- ==========================================================
--
-- ==========================================================
	-- Report Type 2
-- ==========================================================

IF @ReportType = 2 --AND @GroupDivision IS NOT NULL
	BEGIN
		INSERT INTO #ReportTable 
			SELECT 
			-- [Job Group].V001,[Job Information].V027,
				  	[Bill & Payment Activity].V004 AS [Date]-- DATE OF INVOICE
					, [Timesheet Details].V009 AS [Timesheet Date]  
				  , [Timesheet Details].V003 AS [Details]   --, Get DETAILS in TS Details - 2729
				  , [EmployeeList].V004 AS [Initials] --, Get INITIALS from Time
				  , ISNULL(CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),0) AS [TimeSheet]--, Get the Timesheet (Amount) from TS Details
				  --, CAST([Timesheet Details].V007 AS MONEY) AS [TimeSheet]--, Get the Timesheet (Amount) from TS Details
				  , [Timesheet Details].V008 AS [Status]
				  , ISNULL(CAST(CONVERT(MONEY,[Timesheet Details].V020) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),0) AS [Billed]--, Get the Billed from TS Details
				  --, CAST([Timesheet Details].V020 AS MONEY) AS [Billed]--, Get the Billed from TS Details
				  , ISNULL(CASE WHEN CAST([Timesheet Details].V020 AS MONEY) <=0 AND CAST([Timesheet Details].V007 AS MONEY) <=0 THEN 0 ELSE (CAST(isnull([Timesheet Details].V020,0) AS MONEY) / CAST(isnull([Timesheet Details].V007,0) AS MONEY)) * 100 END,0) AS [Percentage Billed]--, Billed / Timesheet (Amount) * 100 
				  , cast(round(CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) - CAST(CONVERT(MONEY,[Timesheet Details].V020) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),2,1) as NUMERIC(18,2)  ) AS [Unbilled] --, Timesheet - Billed
				  --, CAST(isnull([Timesheet Details].V007,0) AS MONEY) - CAST(isnull([Timesheet Details].V020,0) AS MONEY) AS [Unbilled] --, Timesheet - Billed
				  , ISNULL(CASE WHEN CAST([Timesheet Details].V020 AS MONEY) <=0 AND CAST([Timesheet Details].V007 AS MONEY) <=0 THEN 0 ELSE (CAST(isnull([Timesheet Details].V007,0) AS MONEY) - CAST(isnull([Timesheet Details].V020,0) AS MONEY))/ CAST(isnull([Timesheet Details].V007,0) AS MONEY) * 100 END,0) AS [Percentage Unbilled]--, (Timesheet (Amount) - Billed) / Timesheet (Amount) * 100
				  , CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Job Value]--, Job Value from Job Information
				  --, [Job Information].V008 AS [Job Value]--, Job Value from Job Information
				  , [Currency Rates].V001 AS [Currency]--, Currency, get from Job Informaiton, from Currency Rate field Currency
				  , [EmployeeList_Job Controller].V003 AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
				  , [Job Information].V003 AS [Job Title]--Job Title, get from Job Information V003
				  , [Bill & Payment Activity].V017  AS [Invoice No] 
				  , [Job Information].V001 AS [Job No]
				  , CAST(Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY) + cast(ISNULL([Bill & Payment Activity Interim].V005,0) AS MONEY) ),2) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Invoice Amount]
				  , CAST(Round((Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY) + cast(ISNULL([Bill & Payment Activity Interim].V005,0) AS MONEY) ),2) / (1 + ([Job Information].V011 / 100.00))) * [Job Information].V011 / 100.00,2)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [TaxAmount]--CAST([Bill & Payment Activity].V063 AS NUMERIC(18,2)) AS [GST]
				  --, Round(convert(varchar(max),cast([Bill & Payment Activity].V005 as money)),2)  AS [Invoice Amount]
				  --, Round((Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / (1 + ([Job Information].V011 / 100.00))) * [Job Information].V011 / 100.00,2) AS [TaxAmount]--CAST([Bill & Payment Activity].V063 AS NUMERIC(18,2)) AS [GST]
				  , [Job Information].V011 AS [TaxRate]
				  , [Tax Name].V001 AS [Tax Name]
				  , 'Invoiced'

			FROM [Record] [Job Information] WITH (NOLOCK) 

			JOIN [Record] [Bill & Payment Activity] ON [Bill & Payment Activity].TableID = 2711
				AND		[Bill & Payment Activity].IsActive = 1
				AND		[Bill & Payment Activity].V002 = CAST([Job Information].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		cast([Bill & Payment Activity].V004 as date) >= @StartDate
				AND		cast([Bill & Payment Activity].V004 as date) <= @EndDate
				AND		[Bill & Payment Activity].V017 IS NOT NULL
				AND		[Bill & Payment Activity].V029 = 'Invoice'

			LEFT JOIN [Record] [Bill & Payment Activity Interim] ON [Bill & Payment Activity Interim].TableID = 2711
				AND		[Bill & Payment Activity Interim].IsActive = 1
				AND		[Bill & Payment Activity Interim].V002 = CAST([Job Information].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		 [Bill & Payment Activity Interim].V061 =  [Bill & Payment Activity].RecordID
				--AND		cast([Bill & Payment Activity Interim].V004 as date) >= @StartDate
				--AND		cast([Bill & Payment Activity Interim].V004 as date) <= @EndDate
				--AND		[Bill & Payment Activity Interim].V017 IS NOT NULL
				AND		[Bill & Payment Activity Interim].V029 = 'Interim Invoice'

			JOIN [Record] [Timesheet Details] WITH (NOLOCK) ON [Timesheet Details].TableID = 2729
				AND		[Timesheet Details].IsActive = 1
				AND		[Timesheet Details].V016 = CAST([Bill & Payment Activity].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		[Timesheet Details].V007 IS NOT NULL
				AND		[Timesheet Details].V020 IS NOT NULL
				AND	     CONVERT(MONEY,[Timesheet Details].V007) > 0
				AND	     CONVERT(MONEY,[Timesheet Details].V020) >0
		
			LEFT JOIN [Record] [EmployeeList] WITH (NOLOCK) ON [EmployeeList].TableID = 2669
				AND		[EmployeeList].IsActive = 1
				AND		[EmployeeList].RecordID = CAST([Timesheet Details].V002 as int) 		/*	Job Number	*/

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		[EmployeeList_Job Controller].RecordID = CAST([Job Information].V026 as int) 		/*	Job Number	*/

			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		[Currency Rates].RecordID = CAST([Job Information].V034 as int) 		/*	Job Number	*/

			LEFT JOIN [Record] [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727
				AND		[Job Group].IsActive = 1
				AND		[Job Group].RecordID = CAST([Job Information].V027 as int) 		/*	Job Number	*/

			LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		[Tax Name].RecordID = CAST([Job Information].V042 as int)		/*	Job Number	*/

			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	   CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/

					WHERE [Job Information].TableID = 2475 AND
					[Job Information].IsActive = 1 AND
					[Job Information].V027 = CAST(@GroupDivision AS VARCHAR(MAX))
					AND ISNUMERIC(ISNULL([Job Information].V008,0)) = 1
				     AND ISNUMERIC(ISNULL([Job Information].V007,0)) = 1 
					--AND CONVERT(MONEY,ISNULL([Job Information].[V008],0)) > 0 -- Red removed this per checking in Old Biller 08/03/2019
					--AND CONVERT(MONEY,ISNULL([Job Information].[V007],0)) > 0 
					--AND ISNUMERIC([Job Information].[V008]) = 1
					--AND ISNUMERIC([Job Information].[V007]) = 1
						--ORDER BY [Bill & Payment Activity].V017 
						--ORDER BY	[Bill & Payment Activity].V004,[Timesheet Details].V009 
	END

-- ==========================================================
	-- End Report Type 2
-- ==========================================================
--
-- ==========================================================
	-- Report Type 3
-- ==========================================================

IF @ReportType = 3 
	BEGIN
		INSERT INTO #ReportTable 
			SELECT 
			--[Job Group].V001,[EmployeeList].V003,[Bill & Payment Activity].RECORDID,
				  	[Bill & Payment Activity].V004 AS [Date]-- DATE OF INVOICE
					, [Timesheet Details].V009 AS [Timesheet Date] 
				  , [Timesheet Details].V003 AS [Details]   --, Get DETAILS in TS Details - 2729
				  , [EmployeeList].V004 AS [Initials] --, Get INITIALS from Time
				  , ISNULL(CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),0) AS [TimeSheet]--, Get the Timesheet (Amount) from TS Details
				  --, CAST([Timesheet Details].V007 AS MONEY) AS [TimeSheet]--, Get the Timesheet (Amount) from TS Details
				  , [Timesheet Details].V008 AS [Status]
				  , ISNULL(CAST(CONVERT(MONEY,[Timesheet Details].V020) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),0) AS [Billed]--, Get the Billed from TS Details
				  --, CAST([Timesheet Details].V020 AS MONEY) AS [Billed]--, Get the Billed from TS Details
				  , ISNULL(CASE WHEN CAST([Timesheet Details].V020 AS MONEY) <=0 AND CAST([Timesheet Details].V007 AS MONEY) <=0 THEN 0 ELSE (CAST(isnull([Timesheet Details].V020,0) AS MONEY) / CAST(isnull([Timesheet Details].V007,0) AS MONEY)) * 100 END,0) AS [Percentage Billed]--, Billed / Timesheet (Amount) * 100 
				  , cast(round(CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) - CAST(CONVERT(MONEY,[Timesheet Details].V020) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),2,1) as NUMERIC(18,2)  ) AS [Unbilled] --, Timesheet - Billed
				  --, CAST(isnull([Timesheet Details].V007,0) AS MONEY) - CAST(isnull([Timesheet Details].V020,0) AS MONEY) AS [Unbilled] --, Timesheet - Billed
				  , ISNULL(CASE WHEN CAST([Timesheet Details].V020 AS MONEY) <=0 AND CAST([Timesheet Details].V007 AS MONEY) <=0 THEN 0 ELSE (CAST(isnull([Timesheet Details].V007,0) AS MONEY) - CAST(isnull([Timesheet Details].V020,0) AS MONEY))/ CAST(isnull([Timesheet Details].V007,0) AS MONEY) * 100 END,0) AS [Percentage Unbilled]--, (Timesheet (Amount) - Billed) / Timesheet (Amount) * 100
				  , CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Job Value]--, Job Value from Job Information
				  --, [Job Information].V008 AS [Job Value]--, Job Value from Job Information
				  , [Currency Rates].V001 AS [Currency]--, Currency, get from Job Informaiton, from Currency Rate field Currency
				  , [EmployeeList_Job Controller].V003 AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
				  , [Job Information].V003 AS [Job Title]--Job Title, get from Job Information V003
				  , [Bill & Payment Activity].V017  AS [Invoice No] 
				  , [Job Information].V001 AS [Job No]
				  , CAST(Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY) + cast(ISNULL([Bill & Payment Activity Interim].V005,0) AS MONEY) ),2) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Invoice Amount]
				  , CAST(Round((Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY) + cast(ISNULL([Bill & Payment Activity Interim].V005,0) AS MONEY) ),2) / (1 + ([Job Information].V011 / 100.00))) * [Job Information].V011 / 100.00,2)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [TaxAmount]--CAST([Bill & Payment Activity].V063 AS NUMERIC(18,2)) AS [GST]
				  --, Round(convert(varchar(max),cast([Bill & Payment Activity].V005 as money)),2)  AS [Invoice Amount]
				  --, Round((Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / (1 + ([Job Information].V011 / 100.00))) * [Job Information].V011 / 100.00,2) AS [TaxAmount]--CAST([Bill & Payment Activity].V063 AS NUMERIC(18,2)) AS [GST]
				  , [Job Information].V011 AS [TaxRate]
				  , [Tax Name].V001 AS [Tax Name]
				  , 'Invoiced'

			FROM [Record] [Job Information] WITH (NOLOCK) 

			JOIN [Record] [Bill & Payment Activity] ON [Bill & Payment Activity].TableID = 2711
				AND		[Bill & Payment Activity].IsActive = 1
				AND		[Bill & Payment Activity].V002 = CAST([Job Information].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		cast([Bill & Payment Activity].V004 as date) >= @StartDate
				AND		cast([Bill & Payment Activity].V004 as date) <= @EndDate
				AND		[Bill & Payment Activity].V017 IS NOT NULL
				AND		[Bill & Payment Activity].V029 = 'Invoice'

			 LEFT JOIN [Record] [Bill & Payment Activity Interim] ON [Bill & Payment Activity Interim].TableID = 2711
				AND		[Bill & Payment Activity Interim].IsActive = 1
				AND		[Bill & Payment Activity Interim].V002 = CAST([Job Information].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		 [Bill & Payment Activity Interim].V061 =  [Bill & Payment Activity].RecordID
				--AND		cast([Bill & Payment Activity Interim].V004 as date) >= @StartDate
				--AND		cast([Bill & Payment Activity Interim].V004 as date) <= @EndDate
				--AND		[Bill & Payment Activity Interim].V017 IS NOT NULL
				AND		[Bill & Payment Activity Interim].V029 = 'Interim Invoice'


			JOIN [Record] [Timesheet Details] WITH (NOLOCK) ON [Timesheet Details].TableID = 2729
				AND		[Timesheet Details].IsActive = 1
				AND		[Timesheet Details].V016 = CAST([Bill & Payment Activity].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		[Timesheet Details].V007 IS NOT NULL
				AND		[Timesheet Details].V020 IS NOT NULL
				AND	     CONVERT(MONEY,[Timesheet Details].V007) >0
				AND	     CONVERT(MONEY,[Timesheet Details].V020) >0
		
			LEFT JOIN [Record] [EmployeeList] WITH (NOLOCK) ON [EmployeeList].TableID = 2669
				AND		[EmployeeList].IsActive = 1
				AND		CAST([EmployeeList].RecordID AS VARCHAR(MAX)) = [Timesheet Details].V002 		/*	Job Number	*/
			

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		[EmployeeList_Job Controller].RecordID = CAST([Job Information].V026 as int)		/*	Job Number	*/
				--	AND		CAST([EmployeeList].RecordID AS VARCHAR(MAX)) = [Job Information].V026

			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		[Currency Rates].RecordID = CAST([Job Information].V034 as int) 		/*	Job Number	*/

			LEFT JOIN [Record] [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727
				AND		[Job Group].IsActive = 1
				AND		[Job Group].RecordID = CAST([Job Information].V027 as int) 		/*	Job Number	*/

		  	LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		[Tax Name].RecordID = CAST([Job Information].V042 as int) 		/*	Job Number	*/
				
			 LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/

					WHERE [Job Information].TableID = 2475 AND
					[Job Information].IsActive = 1 
					AND [Job Information].V026 = CAST(@Controller AS VARCHAR(MAX))
					--AND CONVERT(MONEY,ISNULL([Job Information].[V008],0)) > 0 
					--AND CONVERT(MONEY,ISNULL([Job Information].[V007],0)) > 0 
					AND ISNUMERIC(ISNULL([Job Information].V008,0)) = 1
				     AND ISNUMERIC(ISNULL([Job Information].V007,0)) = 1 
						--ORDER BY [Bill & Payment Activity].V017 
						--ORDER BY	[Bill & Payment Activity].V004,[Timesheet Details].V009 
	END

-- ==========================================================
	-- End Report Type 3
-- ==========================================================
--
-- ==========================================================
	-- Report Type 4
-- ==========================================================

IF @ReportType = 4 
	BEGIN
		INSERT INTO #ReportTable 
			SELECT 
			--[Job Group].V001,[EmployeeList].V003,[Bill & Payment Activity].RECORDID,
				  	[Bill & Payment Activity].V004 AS [Date]-- DATE OF INVOICE
					, [Timesheet Details].V009 AS [Timesheet Date]  
				  , [Timesheet Details].V003 AS [Details]   --, Get DETAILS in TS Details - 2729
				  , [EmployeeList].V004 AS [Initials] --, Get INITIALS from Time
				  , ISNULL(CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),0) AS [TimeSheet]--, Get the Timesheet (Amount) from TS Details
				  --, CAST([Timesheet Details].V007 AS MONEY) AS [TimeSheet]--, Get the Timesheet (Amount) from TS Details
				  , [Timesheet Details].V008 AS [Status]
				  , ISNULL(CAST(CONVERT(MONEY,[Timesheet Details].V020) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),0) AS [Billed]--, Get the Billed from TS Details
				  --, CAST([Timesheet Details].V020 AS MONEY) AS [Billed]--, Get the Billed from TS Details
				  , ISNULL(CASE WHEN CAST([Timesheet Details].V020 AS MONEY) <=0 AND CAST([Timesheet Details].V007 AS MONEY) <=0 THEN 0 ELSE (CAST(isnull([Timesheet Details].V020,0) AS MONEY) / CAST(isnull([Timesheet Details].V007,0) AS MONEY)) * 100 END,0) AS [Percentage Billed]--, Billed / Timesheet (Amount) * 100 
				  , cast(round(CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) - CAST(CONVERT(MONEY,[Timesheet Details].V020) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),2,1) as NUMERIC(18,2)  ) AS [Unbilled] --, Timesheet - Billed
				  --, CAST(isnull([Timesheet Details].V007,0) AS MONEY) - CAST(isnull([Timesheet Details].V020,0) AS MONEY) AS [Unbilled] --, Timesheet - Billed
				  , ISNULL(CASE WHEN CAST([Timesheet Details].V020 AS MONEY) <=0 AND CAST([Timesheet Details].V007 AS MONEY) <=0 THEN 0 ELSE (CAST(isnull([Timesheet Details].V007,0) AS MONEY) - CAST(isnull([Timesheet Details].V020,0) AS MONEY))/ CAST(isnull([Timesheet Details].V007,0) AS MONEY) * 100 END,0) AS [Percentage Unbilled]--, (Timesheet (Amount) - Billed) / Timesheet (Amount) * 100
				  , CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Job Value]--, Job Value from Job Information
				  --, [Job Information].V008 AS [Job Value]--, Job Value from Job Information
				  , [Currency Rates].V001 AS [Currency]--, Currency, get from Job Informaiton, from Currency Rate field Currency
				  , [EmployeeList_Job Controller].V003 AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
				  , [Job Information].V003 AS [Job Title]--Job Title, get from Job Information V003
				  , [Bill & Payment Activity].V017  AS [Invoice No] 
				  , [Job Information].V001 AS [Job No]
				  , CAST(Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Invoice Amount]
				  , CAST(Round((Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / (1 + ([Job Information].V011 / 100.00))) * [Job Information].V011 / 100.00,2)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [TaxAmount]--CAST([Bill & Payment Activity].V063 AS NUMERIC(18,2)) AS [GST]
				  --, Round(convert(varchar(max),cast([Bill & Payment Activity].V005 as money)),2)  AS [Invoice Amount]
				  --, Round((Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / (1 + ([Job Information].V011 / 100.00))) * [Job Information].V011 / 100.00,2) AS [TaxAmount]--CAST([Bill & Payment Activity].V063 AS NUMERIC(18,2)) AS [GST]
				  , [Job Information].V011 AS [TaxRate]
				  , [Tax Name].V001 AS [Tax Name]
				  , 'Invoiced'

			FROM [Record] [Job Information] WITH (NOLOCK) 

			JOIN [Record] [Bill & Payment Activity] ON [Bill & Payment Activity].TableID = 2711
				AND		[Bill & Payment Activity].IsActive = 1
				AND		[Bill & Payment Activity].V002 = CAST([Job Information].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		cast([Bill & Payment Activity].V004 as date) >= @StartDate
				AND		cast([Bill & Payment Activity].V004 as date) <= @EndDate
				AND		[Bill & Payment Activity].V017 IS NOT NULL
				AND		[Bill & Payment Activity].V029 = 'Invoice'

			JOIN [Record] [Timesheet Details] WITH (NOLOCK) ON [Timesheet Details].TableID = 2729
				AND		[Timesheet Details].IsActive = 1
				AND		[Timesheet Details].V016 = CAST([Bill & Payment Activity].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		[Timesheet Details].V007 IS NOT NULL
				AND		[Timesheet Details].V020 IS NOT NULL
				AND	     CONVERT(MONEY,[Timesheet Details].V007) >0
				AND	     CONVERT(MONEY,[Timesheet Details].V020) >0
		
			LEFT JOIN [Record] [EmployeeList] WITH (NOLOCK) ON [EmployeeList].TableID = 2669
				AND		[EmployeeList].IsActive = 1
				AND		[EmployeeList].RecordID = CAST([Timesheet Details].V002 as int) 		/*	Job Number	*/
			

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		[EmployeeList_Job Controller].RecordID = CAST([Job Information].V026 as int) 		/*	Job Number	*/
				--	AND		CAST([EmployeeList].RecordID AS VARCHAR(MAX)) = [Job Information].V026

			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		[Currency Rates].RecordID = CAST([Job Information].V034 as int) 		/*	Job Number	*/

			LEFT JOIN [Record] [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727
				AND		[Job Group].IsActive = 1
				AND		[Job Group].RecordID = CAST([Job Information].V027 as int) 		/*	Job Number	*/

		  	LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		[Tax Name].RecordID = CAST([Job Information].V042 as int) 		/*	Job Number	*/

		     LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	   CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/
				AND CONVERT(MONEY,ISNULL([Group Currency Rates].V002,0)) > 0

					WHERE [Job Information].TableID = 2475 AND
					[Job Information].IsActive = 1 
					--AND CONVERT(MONEY,ISNULL([Job Information].[V008],0)) > 0 
					--AND CONVERT(MONEY,ISNULL([Job Information].[V007],0)) > 0 
					AND ISNUMERIC(ISNULL([Job Information].V008,0)) = 1
				     AND ISNUMERIC(ISNULL([Job Information].V007,0)) = 1 
					AND [Job Information].V021 LIKE '%Complete%'
						--ORDER BY [Bill & Payment Activity].V017 
						--ORDER BY	[Bill & Payment Activity].V004,[Timesheet Details].V009 
	END

-- ==========================================================
	-- End Report Type 4
-- ==========================================================
--
-- ==========================================================
	-- Report Type 5
-- ==========================================================

IF @ReportType = 5 
	BEGIN
		INSERT INTO #ReportTable 
			SELECT 
			--[Job Group].V001,[EmployeeList].V003,[Bill & Payment Activity].RECORDID,
				  	[Bill & Payment Activity].V004 AS [Date]-- DATE OF INVOICE				  
				  , [Timesheet Details].V009 AS [Timesheet Date] 
				  , [Timesheet Details].V003 AS [Details]   --, Get DETAILS in TS Details - 2729
				  , [EmployeeList].V004 AS [Initials] --, Get INITIALS from Time
				  , ISNULL(CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),0) AS [TimeSheet]--, Get the Timesheet (Amount) from TS Details
				  --, CAST([Timesheet Details].V007 AS MONEY) AS [TimeSheet]--, Get the Timesheet (Amount) from TS Details
				  , [Timesheet Details].V008 AS [Status]
				  , ISNULL(CAST(CONVERT(MONEY,[Timesheet Details].V020) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),0) AS [Billed]--, Get the Billed from TS Details
				  --, CAST([Timesheet Details].V020 AS MONEY) AS [Billed]--, Get the Billed from TS Details
				  , ISNULL(CASE WHEN CAST([Timesheet Details].V020 AS MONEY) <=0 AND CAST([Timesheet Details].V007 AS MONEY) <=0 THEN 0 ELSE (CAST(isnull([Timesheet Details].V020,0) AS MONEY) / CAST(isnull([Timesheet Details].V007,0) AS MONEY)) * 100 END,0) AS [Percentage Billed]--, Billed / Timesheet (Amount) * 100 
				  , cast(round(CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) - CAST(CONVERT(MONEY,[Timesheet Details].V020) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),2,1) as NUMERIC(18,2)  ) AS [Unbilled] --, Timesheet - Billed
				  --, CAST(isnull([Timesheet Details].V007,0) AS MONEY) - CAST(isnull([Timesheet Details].V020,0) AS MONEY) AS [Unbilled] --, Timesheet - Billed
				  , ISNULL(CASE WHEN CAST([Timesheet Details].V020 AS MONEY) <=0 AND CAST([Timesheet Details].V007 AS MONEY) <=0 THEN 0 ELSE (CAST(isnull([Timesheet Details].V007,0) AS MONEY) - CAST(isnull([Timesheet Details].V020,0) AS MONEY))/ CAST(isnull([Timesheet Details].V007,0) AS MONEY) * 100 END,0) AS [Percentage Unbilled]--, (Timesheet (Amount) - Billed) / Timesheet (Amount) * 100
				  , CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Job Value]--, Job Value from Job Information
				  --, [Job Information].V008 AS [Job Value]--, Job Value from Job Information
				  , [Currency Rates].V001 AS [Currency]--, Currency, get from Job Informaiton, from Currency Rate field Currency
				  , [EmployeeList_Job Controller].V003 AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
				  , [Job Information].V003 AS [Job Title]--Job Title, get from Job Information V003
				  , [Bill & Payment Activity].V017  AS [Invoice No] 
				  , [Job Information].V001 AS [Job No]
				  , CAST(Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY) + cast(ISNULL([Bill & Payment Activity Interim].V005,0) AS MONEY)  ),2) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Invoice Amount]
				  , CAST(Round((Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY) + cast(ISNULL([Bill & Payment Activity Interim].V005,0) AS MONEY)  ),2) / (1 + ([Job Information].V011 / 100.00))) * [Job Information].V011 / 100.00,2)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [TaxAmount]--CAST([Bill & Payment Activity].V063 AS NUMERIC(18,2)) AS [GST]
				  --, Round(convert(varchar(max),cast([Bill & Payment Activity].V005 as money)),2)  AS [Invoice Amount]
				  --, Round((Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / (1 + ([Job Information].V011 / 100.00))) * [Job Information].V011 / 100.00,2) AS [TaxAmount]--CAST([Bill & Payment Activity].V063 AS NUMERIC(18,2)) AS [GST]
				  , [Job Information].V011 AS [TaxRate]
				  , [Tax Name].V001 AS [Tax Name]
				  , 'Invoiced'

			FROM [Record] [Job Information] WITH (NOLOCK) 

			JOIN [Record] [Bill & Payment Activity] ON [Bill & Payment Activity].TableID = 2711
				AND		[Bill & Payment Activity].IsActive = 1
				AND		[Bill & Payment Activity].V002 = CAST([Job Information].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		cast([Bill & Payment Activity].V004 as date) >= @StartDate
				AND		cast([Bill & Payment Activity].V004 as date) <= @EndDate
				AND		[Bill & Payment Activity].V017 IS NOT NULL
				AND		[Bill & Payment Activity].V029 = 'Invoice'

			LEFT JOIN [Record] [Bill & Payment Activity Interim] ON [Bill & Payment Activity Interim].TableID = 2711
				AND		[Bill & Payment Activity Interim].IsActive = 1
				AND		[Bill & Payment Activity Interim].V002 = CAST([Job Information].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		 [Bill & Payment Activity Interim].V061 =  [Bill & Payment Activity].RecordID
				--AND		cast([Bill & Payment Activity Interim].V004 as date) >= @StartDate
				--AND		cast([Bill & Payment Activity Interim].V004 as date) <= @EndDate
				--AND		[Bill & Payment Activity Interim].V017 IS NOT NULL
				AND		[Bill & Payment Activity Interim].V029 = 'Interim Invoice'

			JOIN [Record] [Timesheet Details] WITH (NOLOCK) ON [Timesheet Details].TableID = 2729
				AND		[Timesheet Details].IsActive = 1
				AND		[Timesheet Details].V016 = CAST([Bill & Payment Activity].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		[Timesheet Details].V007 IS NOT NULL
				AND		[Timesheet Details].V020 IS NOT NULL
				AND	     CONVERT(MONEY,[Timesheet Details].V007) >0
				AND	     CONVERT(MONEY,[Timesheet Details].V020) >0
		
			LEFT JOIN [Record] [EmployeeList] WITH (NOLOCK) ON [EmployeeList].TableID = 2669
				AND		[EmployeeList].IsActive = 1
				AND		[EmployeeList].RecordID = CAST([Timesheet Details].V002 as int) 		/*	Job Number	*/
			

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		[EmployeeList_Job Controller].RecordID = CAST([Job Information].V026 as int) 		/*	Job Number	*/
				--	AND		CAST([EmployeeList].RecordID AS VARCHAR(MAX)) = [Job Information].V026

			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		[Currency Rates].RecordID = CAST([Job Information].V034 as int)		/*	Job Number	*/

			LEFT JOIN [Record] [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727
				AND		[Job Group].IsActive = 1
				AND		[Job Group].RecordID = CAST([Job Information].V027 as int)		/*	Job Number	*/

		  	LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		[Tax Name].RecordID = CAST([Job Information].V042 as int) 		/*	Job Number	*/

			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	   CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/

					WHERE [Job Information].TableID = 2475 AND
					[Job Information].IsActive = 1 
					AND [Job Information].V021 LIKE '%Complete%'
					AND [Job Information].V027 = CAST(@GroupDivision AS VARCHAR(MAX))
					--AND CONVERT(MONEY,ISNULL([Job Information].[V008],0)) > 0 
					--AND CONVERT(MONEY,ISNULL([Job Information].[V007],0)) > 0 
					AND ISNUMERIC(ISNULL([Job Information].V008,0)) = 1
				     AND ISNUMERIC(ISNULL([Job Information].V007,0)) = 1 
						--ORDER BY [Bill & Payment Activity].V017 
						--ORDER BY	[Bill & Payment Activity].V004,[Timesheet Details].V009 
	END

-- ==========================================================
	-- End Report Type 5
-- ==========================================================
--
-- ==========================================================
	-- Report Type 6
-- ==========================================================

IF @ReportType = 6 
	BEGIN
		INSERT INTO #ReportTable 
			SELECT 
			--[Job Group].V001,[EmployeeList].V003,[Bill & Payment Activity].RECORDID,
				  	[Bill & Payment Activity].V004 AS [Date]-- DATE OF INVOICE
					, [Timesheet Details].V009 AS [Timesheet Date] 
				  , [Timesheet Details].V003 AS [Details]   --, Get DETAILS in TS Details - 2729
				  , [EmployeeList].V004 AS [Initials] --, Get INITIALS from Time
				  , ISNULL(CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),0) AS [TimeSheet]--, Get the Timesheet (Amount) from TS Details
				  --, CAST([Timesheet Details].V007 AS MONEY) AS [TimeSheet]--, Get the Timesheet (Amount) from TS Details
				  , [Timesheet Details].V008 AS [Status]
				  , ISNULL(CAST(CONVERT(MONEY,[Timesheet Details].V020) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),0) AS [Billed]--, Get the Billed from TS Details
				  --, CAST([Timesheet Details].V020 AS MONEY) AS [Billed]--, Get the Billed from TS Details
				  , ISNULL(CASE WHEN CAST([Timesheet Details].V020 AS MONEY) <=0 AND CAST([Timesheet Details].V007 AS MONEY) <=0 THEN 0 ELSE (CAST(isnull([Timesheet Details].V020,0) AS MONEY) / CAST(isnull([Timesheet Details].V007,0) AS MONEY)) * 100 END,0) AS [Percentage Billed]--, Billed / Timesheet (Amount) * 100 
				  , cast(round(CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) - CAST(CONVERT(MONEY,[Timesheet Details].V020) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY),2,1) as NUMERIC(18,2)  ) AS [Unbilled] --, Timesheet - Billed
				  --, CAST(isnull([Timesheet Details].V007,0) AS MONEY) - CAST(isnull([Timesheet Details].V020,0) AS MONEY) AS [Unbilled] --, Timesheet - Billed
				  , ISNULL(CASE WHEN CAST([Timesheet Details].V020 AS MONEY) <=0 AND CAST([Timesheet Details].V007 AS MONEY) <=0 THEN 0 ELSE (CAST(isnull([Timesheet Details].V007,0) AS MONEY) - CAST(isnull([Timesheet Details].V020,0) AS MONEY))/ CAST(isnull([Timesheet Details].V007,0) AS MONEY) * 100 END,0) AS [Percentage Unbilled]--, (Timesheet (Amount) - Billed) / Timesheet (Amount) * 100
				  , CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Job Value]--, Job Value from Job Information
				  --, [Job Information].V008 AS [Job Value]--, Job Value from Job Information
				  , [Currency Rates].V001 AS [Currency]--, Currency, get from Job Informaiton, from Currency Rate field Currency
				  , [EmployeeList_Job Controller].V003 AS [Job Controller]--, Job Controller, get from Job Information, from Employee List Employee Name
				  , [Job Information].V003 AS [Job Title]--Job Title, get from Job Information V003
				  , [Bill & Payment Activity].V017  AS [Invoice No] 
				  , [Job Information].V001 AS [Job No]
				  , CAST(Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Invoice Amount]
				  , CAST(Round((Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / (1 + ([Job Information].V011 / 100.00))) * [Job Information].V011 / 100.00,2)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [TaxAmount]--CAST([Bill & Payment Activity].V063 AS NUMERIC(18,2)) AS [GST]
				  --, Round(convert(varchar(max),cast([Bill & Payment Activity].V005 as money)),2)  AS [Invoice Amount]
				  --, Round((Round(convert(varchar(max),cast([Bill & Payment Activity].V005 AS MONEY)),2) / (1 + ([Job Information].V011 / 100.00))) * [Job Information].V011 / 100.00,2) AS [TaxAmount]--CAST([Bill & Payment Activity].V063 AS NUMERIC(18,2)) AS [GST]
				  , [Job Information].V011 AS [TaxRate]
				  , [Tax Name].V001 AS [Tax Name]
				  , 'Invoiced'

			FROM [Record] [Job Information] WITH (NOLOCK) 

			JOIN [Record] [Bill & Payment Activity] ON [Bill & Payment Activity].TableID = 2711
				AND		[Bill & Payment Activity].IsActive = 1
				AND		[Bill & Payment Activity].V002 = CAST([Job Information].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		cast([Bill & Payment Activity].V004 as date) >=  @StartDate
				AND		cast([Bill & Payment Activity].V004 as date) <=  @EndDate
				AND		[Bill & Payment Activity].V017 IS NOT NULL
				AND		[Bill & Payment Activity].V029 = 'Invoice'

			JOIN [Record] [Timesheet Details] WITH (NOLOCK) ON [Timesheet Details].TableID = 2729
				AND		[Timesheet Details].IsActive = 1
				AND		[Timesheet Details].V016 = CAST([Bill & Payment Activity].RecordID	AS VARCHAR(MAX))		/*	Job Number	*/
				AND		[Timesheet Details].V007 IS NOT NULL
				AND		[Timesheet Details].V020 IS NOT NULL
				AND	     CONVERT(MONEY,[Timesheet Details].V007) >0
				AND	     CONVERT(MONEY,[Timesheet Details].V020) >0
		
			LEFT JOIN [Record] [EmployeeList] WITH (NOLOCK) ON [EmployeeList].TableID = 2669
				AND		[EmployeeList].IsActive = 1
				AND		CAST([EmployeeList].RecordID AS VARCHAR(MAX)) = [Timesheet Details].V002 		/*	Job Number	*/
			

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		[EmployeeList_Job Controller].RecordID = CAST([Job Information].V026 as int)		/*	Job Number	*/
				--	AND		CAST([EmployeeList].RecordID AS VARCHAR(MAX)) = [Job Information].V026

			LEFT JOIN [Record] [Currency Rates] WITH (NOLOCK) ON [Currency Rates].TableID = 2665
				AND		[Currency Rates].IsActive = 1
				AND		[Currency Rates].RecordID = CAST([Job Information].V034 as int) 		/*	Job Number	*/

			LEFT JOIN [Record] [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727
				AND		[Job Group].IsActive = 1
				AND		[Job Group].RecordID = CAST([Job Information].V027 as int)		/*	Job Number	*/

		  	LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		[Tax Name].RecordID = CAST([Job Information].V042 as int) 		/*	Job Number	*/

			 LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	   CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/

					WHERE [Job Information].TableID = 2475 AND
					[Job Information].IsActive = 1 
					AND [Job Information].V021 LIKE '%Complete%'
					AND [Job Information].V026 = CAST(@Controller AS VARCHAR(MAX))
					--AND CONVERT(MONEY,ISNULL([Job Information].[V008],0)) > 0 
					--AND CONVERT(MONEY,ISNULL([Job Information].[V007],0)) > 0 
					AND ISNUMERIC(ISNULL([Job Information].V008,0)) = 1
				     AND ISNUMERIC(ISNULL([Job Information].V007,0)) = 1 
						--ORDER BY [Bill & Payment Activity].V017 
						--ORDER BY	[Bill & Payment Activity].V004,[Timesheet Details].V009 
	END

-- ==========================================================
	-- End Report Type 6
-- ==========================================================
--
-- ==========================================================
	-- Report Type 7
-- ==========================================================

IF @ReportType = 7 
	BEGIN
--	SELECT 'RED'
		INSERT INTO #ReportTable ([Date],[Details],[Initials],[Timesheet],[Status],[Billed],[Job No],[TaxRate],[TaxName],[Currency],[Job Controller],[Job Title],[Job Value],[Report Section])
		SELECT 	
		   CAST([Timesheet Details].V009 AS DATE) AS [Date Current]-- DATE OF INVOICE
		  ,[Timesheet Details].V003 AS [Details Current]   --, Get DETAILS in TS Details - 2729
		  ,[EmployeeList].V004 AS [Initials Current] --, Get INITIALS from Time
		  ,CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY)  AS [TimeSheet Current]
		  --,CAST([Timesheet Details].V007 AS MONEY) AS [TimeSheet Current]--, Get the Timesheet (Amount) from TS Details
		  ,[Timesheet Details].V008 AS [Status Current]
		  ,CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY)  AS [TimeSheet Current]
		  --,CAST([Timesheet Details].V007 AS MONEY) AS [Billable Current]--, Get the Timesheet (Amount) from TS Details
		  ,[Job Information].V001 AS [Job No]
		  , [Job Information].V011 AS [TaxRate]
		  , [Tax Name].V001 AS [Tax Name]
		  ,[Group Currency Rates].V001 AS [Currency]
		     , [EmployeeList_Job Controller].V003 AS [Job Controller]
		     , [Job Information].V003 AS [Job Title]
			, CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Job Value]
		  , 'Current'

		FROM [Record] [Job Information] WITH (NOLOCK) 		

			INNER JOIN [Record] [Timesheet Details] WITH (NOLOCK) ON [Timesheet Details].TableID = 2729
				AND		[Timesheet Details].IsActive = 1
				AND		[Timesheet Details].V016 IS NULL		/*	Job Number	*/
				AND		[Timesheet Details].V001 = CAST([Job Information].RecordID AS VARCHAR(MAX))
				AND		[Timesheet Details].V007 IS NOT NULL
				AND		[Timesheet Details].V020 IS NOT NULL
				AND	     CONVERT(MONEY,[Timesheet Details].V007) >0
				AND	     CONVERT(MONEY,[Timesheet Details].V020) >0

			LEFT JOIN [Record] [EmployeeList] WITH (NOLOCK) ON [EmployeeList].TableID = 2669
				AND		[EmployeeList].IsActive = 1
				AND		[EmployeeList].RecordID = CAST([Timesheet Details].V002 as int)		/*	Job Number	*/

					LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		[EmployeeList_Job Controller].RecordID = CAST([Job Information].V026 as int) 		/*	Job Number	*/
				--	AND		CAST([EmployeeList].RecordID AS VARCHAR(MAX)) = [Job Information].V026

		  	LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		[Tax Name].RecordID = CAST([Job Information].V042 as int) 		/*	Job Number	*/
			
			LEFT JOIN [Record] [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727
				AND		[Job Group].IsActive = 1
				AND		[Job Group].RecordID = CAST([Job Information].V027 as int)		/*	Job Number	*/

			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	   CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/
				--AND [Group Currency Rates].V001 IS NOT NULL
							
		WHERE [Job Information].TableID = 2475 AND
			  [Job Information].IsActive = 1 --AND
			 -- [Job Information].RecordID IN (SELECT Int_Value FROM DBO.fn_ParseText2Table(@RecordIDs,','))
			 	--AND CONVERT(MONEY,ISNULL([Job Information].[V008],0)) > 0 
					--AND CONVERT(MONEY,ISNULL([Job Information].[V007],0)) > 0 
					AND ISNUMERIC(ISNULL([Job Information].V008,0)) = 1
				     AND ISNUMERIC(ISNULL([Job Information].V007,0)) = 1 
					ORDER BY CAST([Timesheet Details].V009 AS DATE) DESC
	END

-- ==========================================================
	-- End Report Type 7
-- ==========================================================
--
-- ==========================================================
	-- Report Type 8
-- ==========================================================

IF @ReportType = 8
	BEGIN
		INSERT INTO #ReportTable ([Date],[Details],[Initials],[Timesheet],[Status],[Billed],[Job No],[TaxRate],[TaxName],[Job Controller],[Job Title],[Job Value],[Report Section])
		SELECT 	
		   CAST([Timesheet Details].V009 AS DATE) AS [Date Current]-- DATE OF INVOICE
		  ,[Timesheet Details].V003 AS [Details Current]   --, Get DETAILS in TS Details - 2729
		  ,[EmployeeList].V004 AS [Initials Current] --, Get INITIALS from Time
		  ,CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY)  AS [TimeSheet Current]
		  --,CAST([Timesheet Details].V007 AS MONEY) AS [TimeSheet Current]--, Get the Timesheet (Amount) from TS Details
		  ,[Timesheet Details].V008 AS [Status Current]
		  ,CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY)  AS [TimeSheet Current]
		  --,CAST([Timesheet Details].V007 AS MONEY) AS [Billable Current]--, Get the Timesheet (Amount) from TS Details
		  ,[Job Information].V001 AS [Job No]
		  , [Job Information].V011 AS [TaxRate]
		  , [Tax Name].V001 AS [Tax Name]
		     , [EmployeeList_Job Controller].V003 AS [Job Controller]
		     , [Job Information].V003 AS [Job Title]
			, CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Job Value]
		  , 'Current'

		FROM [Record] [Job Information] WITH (NOLOCK) 		

			INNER JOIN [Record] [Timesheet Details] WITH (NOLOCK) ON [Timesheet Details].TableID = 2729
				AND		[Timesheet Details].IsActive = 1
				AND		[Timesheet Details].V016 IS NULL		/*	Job Number	*/
				AND		[Timesheet Details].V001 = CAST([Job Information].RecordID AS VARCHAR(MAX))
				AND		[Timesheet Details].V007 IS NOT NULL
				AND		[Timesheet Details].V020 IS NOT NULL
				AND	     CONVERT(MONEY,[Timesheet Details].V007) >0
				AND	     CONVERT(MONEY,[Timesheet Details].V020) >0

			LEFT JOIN [Record] [EmployeeList] WITH (NOLOCK) ON [EmployeeList].TableID = 2669
				AND		[EmployeeList].IsActive = 1
				AND		[EmployeeList].RecordID = CAST([Timesheet Details].V002 as int)		/*	Job Number	*/

					LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		[EmployeeList_Job Controller].RecordID = CAST([Job Information].V026 as int)		/*	Job Number	*/
				--	AND		CAST([EmployeeList].RecordID AS VARCHAR(MAX)) = [Job Information].V026

		  	LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		[Tax Name].RecordID = CAST([Job Information].V042 as int)		/*	Job Number	*/

			LEFT JOIN [Record] [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727
				AND		[Job Group].IsActive = 1
				AND		[Job Group].RecordID = CAST([Job Information].V027 as int) 		/*	Job Number	*/

			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	   CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/
							
		WHERE [Job Information].TableID = 2475 AND
			  [Job Information].IsActive = 1 --AND
			  AND [Job Information].V027 = CAST(@GroupDivision AS VARCHAR(MAX))
			  	--AND CONVERT(MONEY,ISNULL([Job Information].[V008],0)) > 0 
					--AND CONVERT(MONEY,ISNULL([Job Information].[V007],0)) > 0 
					AND ISNUMERIC(ISNULL([Job Information].V008,0)) = 1
				     AND ISNUMERIC(ISNULL([Job Information].V007,0)) = 1 	
			 -- [Job Information].RecordID IN (SELECT Int_Value FROM DBO.fn_ParseText2Table(@RecordIDs,','))
					ORDER BY CAST([Timesheet Details].V009 AS DATE) DESC
	END

-- ==========================================================
	-- End Report Type 8
-- ==========================================================
--
-- ==========================================================
	-- Report Type 9
-- ==========================================================

IF @ReportType = 9 
	BEGIN
		INSERT INTO #ReportTable ([Date],[Details],[Initials],[Timesheet],[Status],[Billed],[Job No],[TaxRate],[TaxName],[Job Controller],[Job Title],[Job Value],[Report Section])
		SELECT 	
		   CAST([Timesheet Details].V009 AS DATE) AS [Date Current]-- DATE OF INVOICE
		  ,[Timesheet Details].V003 AS [Details Current]   --, Get DETAILS in TS Details - 2729
		  ,[EmployeeList].V004 AS [Initials Current] --, Get INITIALS from Time
		  ,CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY)  AS [TimeSheet Current]
		  --,CAST([Timesheet Details].V007 AS MONEY) AS [TimeSheet Current]--, Get the Timesheet (Amount) from TS Details
		  ,[Timesheet Details].V008 AS [Status Current]
		  ,CAST(CONVERT(MONEY,[Timesheet Details].V007)/ convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY)  AS [TimeSheet Current]
		  --,CAST([Timesheet Details].V007 AS MONEY) AS [Billable Current]--, Get the Timesheet (Amount) from TS Details
		  ,[Job Information].V001 AS [Job No]
		  , [Job Information].V011 AS [TaxRate]
		  , [Tax Name].V001 AS [Tax Name]
		    , [EmployeeList_Job Controller].V003 AS [Job Controller]
		     , [Job Information].V003 AS [Job Title]
			, CAST(CONVERT(MONEY,[Job Information].V008) / convert(money,ISNULL([Job Information].V007,1)) * convert(money,ISNULL([Job Information].V047,1))  /* convert(money,[Group Currency Rates].V002) */ AS MONEY) AS [Job Value]
		  , 'Current'

		FROM [Record] [Job Information] WITH (NOLOCK) 		

			INNER JOIN [Record] [Timesheet Details] WITH (NOLOCK) ON [Timesheet Details].TableID = 2729
				AND		[Timesheet Details].IsActive = 1
				AND		[Timesheet Details].V016 IS NULL		/*	Job Number	*/
				AND		[Timesheet Details].V001 = CAST([Job Information].RecordID AS VARCHAR(MAX))
					AND		[Timesheet Details].V007 IS NOT NULL
				AND		[Timesheet Details].V020 IS NOT NULL
				AND	     CONVERT(MONEY,[Timesheet Details].V007) >0
				AND	     CONVERT(MONEY,[Timesheet Details].V020) >0

			LEFT JOIN [Record] [EmployeeList] WITH (NOLOCK) ON [EmployeeList].TableID = 2669
				AND		[EmployeeList].IsActive = 1
				AND		[EmployeeList].RecordID = CAST([Timesheet Details].V002 as int) 		/*	Job Number	*/

			LEFT JOIN [Record] [EmployeeList_Job Controller] WITH (NOLOCK) ON [EmployeeList_Job Controller].TableID = 2669
				AND		[EmployeeList_Job Controller].IsActive = 1
				AND		[EmployeeList_Job Controller].RecordID = CAST([Job Information].V026 as int)		/*	Job Number	*/
				--	AND		CAST([EmployeeList].RecordID AS VARCHAR(MAX)) = [Job Information].V026

		  
		  	LEFT JOIN  [Record] [Tax Name] WITH (NOLOCK) ON [Tax Name].TableID = 2696
				AND		[Tax Name].IsActive = 1
				AND		[Tax Name].RecordID = CAST([Job Information].V042 as int) 		/*	Job Number	*/

			LEFT JOIN [Record] [Job Group] WITH (NOLOCK) ON [Job Group].TableID = 2727
				AND		[Job Group].IsActive = 1
				AND		[Job Group].RecordID = CAST([Job Information].V027 as int) 		/*	Job Number	*/

			LEFT JOIN Record [Group Currency Rates] WITH (NOLOCK) ON [Group Currency Rates].TableID = 2665		/*	Currency Rates	*/
				AND			[Group Currency Rates].IsActive = 1
				AND	   CAST( [Group Currency Rates].V001 AS VARCHAR(MAX)) = [Job Group].V004		/*	[Group Currency]	*/
							
		WHERE [Job Information].TableID = 2475 AND
			  [Job Information].IsActive = 1 --AND
			  	AND CAST([Job Information].V026 as int) = @Controller
				AND ISNUMERIC(ISNULL([Job Information].V008,0)) = 1
				     AND ISNUMERIC(ISNULL([Job Information].V007,0)) = 1 
					--AND CONVERT(MONEY,ISNULL([Job Information].[V008],0)) > 0 
					--AND CONVERT(MONEY,ISNULL([Job Information].[V007],0)) > 0 
			 -- [Job Information].RecordID IN (SELECT Int_Value FROM DBO.fn_ParseText2Table(@RecordIDs,','))
					ORDER BY CAST([Timesheet Details].V009 AS DATE) DESC
	END

-- ==========================================================
	-- End Report Type 9
-- ==========================================================




--===============================================================



 CREATE TABLE  #ReportTableFinal (
			  [Counter] int identity(1,1)
			 ,Counter_Old int
			, [Date] date
			, [Timesheet Date] date
			, [Details] varchar(max)
			, [Initials] varchar(10)
			, [Timesheet] money
			, [Status] varchar(max)  
			, [Billed] money
			, [Percentage_Billed] float
			, [Unbilled] money
			, [Percentage_Unbilled] float
			, [Job Value] money
			, [Currency] varchar(10)
			, [Job Controller] varchar(max)
			, [Job Title] varchar(max)
			, [Invoice No] varchar(max)
			, [Job No] varchar(max)
			, [Invoice Amount] money
			, [TaxAmount] MONEY
			, [TaxRate] float
			, [TaxName] varchar(max)
			, [Report Section] varchar(max)
			
			)

INSERT INTO #ReportTableFinal
SELECT * FROM #ReportTable
ORDER BY [Invoice No],[Date],[Timesheet Date]

--IF @ReportType = 0
--BEGIN

DECLARE @JobNox   VARCHAR(MAX) = '';
DECLARE @COUNTER   INT = 1;
WHILE EXISTS
(
    SELECT
	  *
    FROM  #ReportTableFinal
    WHERE [Counter] >= @COUNTER 
)
    BEGIN

	   DECLARE @JobNo   VARCHAR(MAX);

	   SELECT
		 @JobNo = [Invoice No]
	   FROM  #ReportTableFinal
	   WHERE [Counter] = @Counter;


	   IF @JobNox = @JobNo
		  BEGIN
			 UPDATE #ReportTableFinal
			   SET
				  [INVOICE AMOUNT] = 0
				  ,[TaxAmount] = 0
			 WHERE
			    [COUNTER] = @COUNTER;
		  END;


	   SET @JobNox = @JobNo;
	   SET @COUNTER = @COUNTER + 1;
    END;

  --  END
	
-- Show the reports

SELECT [Counter],
	  [Date],
	   [Timesheet Date],
	  [Details],
	  [Initials],
	  [Timesheet],
	  [Status],
	  [Billed],
	  [Percentage_Billed],
	  [Unbilled],
	  [Percentage_Unbilled],
	  [Job Value],
	  [Currency],
	  [Job Controller],
	  [Job Title],
	  [Invoice No],
	  [Job No],
	  ISNULL([Invoice Amount],'0.00') AS [Invoice Amount],
	  ISNULL([TaxAmount],'0.00') AS [TaxAmount],
	  [TaxRate],
	  [TaxName],
	  [Report Section]	   
	   
	    FROM #ReportTableFinal
--ORDER BY [Date],[Timesheet Date]
END

