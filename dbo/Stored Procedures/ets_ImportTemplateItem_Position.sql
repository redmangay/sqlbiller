﻿

CREATE PROCEDURE   [dbo].[ets_ImportTemplateItem_Position]
(
	@nImportTemplateID int ,
	@nPositionOnImport int 
)
AS

BEGIN TRY
	SET NOCOUNT ON;

	SELECT     T.TableName, ITI.*
FROM         ImportTemplate IT INNER JOIN
                      [Table] T ON IT.TableID = T.TableID INNER JOIN ImportTemplateItem ITI
					  ON ITI.ImportTemplateID=IT.ImportTemplateID
		WHERE IT.ImportTemplateID = @nImportTemplateID
		AND ITI.PositionOnImport=@nPositionOnImport

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_ImportTemplateItem_Position', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
