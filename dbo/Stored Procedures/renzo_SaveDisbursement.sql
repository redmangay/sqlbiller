﻿
CREATE PROCEDURE		[dbo].[renzo_SaveDisbursement]
(
	@RecordID  int,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output--,
    	--@Context varchar(max) = null
)
AS

/*
				Set/update the rate based on the Disbursement based on the disbursement rate

				DECLARE			@Return		VARCHAR(MAX)


				EXEC			[renzo_SaveDisbursement] @RecordID =  16041573, @Return = ''

				EXEC		[dbo].[dev_GetAccountSQL] @TableID = 2725, @TableName = 'Disbursement', @AccountID = '24918'

				EXEC		[dbo].[dev_GetAccountSQL] @AccountID = '24918'		/*	buffalobiller1 */

*/

DECLARE			 @Job			INT
				,@CurrencyRate	varchar(max)
				,@TaxRate varchar(max)


-- lets get Job RecordID
SELECT			@Job = [Record].[V001]
FROM			[Record]
WHERE			ISNUMERIC( [Record].[V001] ) = 1
AND				[Record].IsActive = 1
AND				[Record].RecordID = @RecordID


-- Red: lets get the currency rate

SELECT @CurrencyRate = ISNULL(V007,1),
  @TaxRate = (V011 / 100.00) FROM [Record]
WHERE RECORDID = @Job


/* Lets get the Entered By */

UPDATE [Record]
SET V024 = EnteredBy
WHERE [Record].[RecordID] = @RecordID


/* Update the Item to Item View */
IF (SELECT V030 FROM Record WHERE RecordID = @RecordID) = '2'
BEGIN
    UPDATE [Disbursement]
    SET [Disbursement].V034 = [Disbursement Rate].V010
    FROM Record [Disbursement]
    JOIN Record [Disbursement Rate] ON  [Disbursement Rate].TableID = 2478
    AND [Disbursement Rate].IsActive = 1
    AND [Disbursement Rate].RecordID = CAST([Disbursement].V003 as int)
    WHERE [Disbursement].TableID = 2725
    AND [Disbursement].IsActive = 1
    AND [Disbursement].RecordID = @RecordID

    UPDATE [Disbursement]
    SET [Disbursement].V032 = [Job Information].V001
    FROM Record [Disbursement]
    JOIN Record [Job Information] ON  [Job Information].TableID = 2475
    AND [Job Information].IsActive = 1
    AND [Job Information].RecordID = CAST([Disbursement].V001 as int)
    WHERE [Disbursement].TableID = 2725
    AND [Disbursement].IsActive = 1
    AND [Disbursement].RecordID = @RecordID 
  

END

IF (SELECT V030 FROM Record WHERE RecordID = @RecordID) = '1'
BEGIN
    UPDATE [Disbursement]
    SET [Disbursement].V034 = [Disbursement Master].V006 
    FROM Record [Disbursement]
    JOIN Record [Disbursement Master] ON  [Disbursement Master].TableID = 2713
    AND [Disbursement Master].IsActive = 1
    AND [Disbursement Master].RecordID = CAST([Disbursement].V033 as int)
    WHERE [Disbursement].TableID = 2725
    AND [Disbursement].IsActive = 1
    AND [Disbursement].RecordID = @RecordID

    UPDATE [Disbursement]
    SET [Disbursement].V032 = [Administration].V001
    FROM Record [Disbursement]
    JOIN Record [Administration] ON  [Administration].TableID = 2650
    AND [Administration].IsActive = 1
    AND [Administration].RecordID = CAST([Disbursement].V031 as int)
    WHERE [Disbursement].TableID = 2725
    AND [Disbursement].IsActive = 1
    AND [Disbursement].RecordID = @RecordID 
END



--				Set Rate
UPDATE			[Disbursement]
SET				[Disbursement].V005 = ISNULL([Disbursement Rate].V002, '0' )
				
FROM			[Record] [Disbursement]
JOIN			[Record] [Disbursement Rate] ON				1 = 1
													AND		[Disbursement Rate].RecordID = [Disbursement].V003
													AND		[Disbursement Rate].IsActive = 1
													AND		[Disbursement Rate].TableID = 2478
JOIN			[Record] [Disbursement Type] ON				1 = 1
													AND		[Disbursement Type].RecordID = [Disbursement Rate].V001
													AND		[Disbursement Type].IsActive = 1
													AND		[Disbursement Type].TableID = 2728
WHERE			1 = 1
AND				[Disbursement].[RecordID] = @RecordID
AND				[Disbursement].IsActive = 1
AND				[Disbursement].TableID = 2725
AND				[Disbursement].V005 IS NULL;

--				Set Details
UPDATE			[Disbursement]
SET				[Disbursement].V006 = ISNULL([Disbursement Type].V001, '' ) + CASE WHEN LEN( ISNULL([Disbursement Type].V001, '' )) > 0 AND LEN(ISNULL([Disbursement].V005, '' )) > 0 THEN ' ' ELSE '' END + ISNULL([Disbursement].V005, '' )

FROM			[Record] [Disbursement]
JOIN			[Record] [Disbursement Rate] ON				1 = 1
													AND		[Disbursement Rate].V004 = [Disbursement].V001
													AND		[Disbursement Rate].IsActive = 1
													AND		[Disbursement Rate].TableID = 2478
													AND		[Disbursement Rate].RecordID = [Disbursement].V003
JOIN			[Record] [Disbursement Type] ON				1 = 1
													AND		[Disbursement Type].RecordID =[Disbursement Rate].V001
													AND		[Disbursement Type].IsActive = 1
													AND		[Disbursement Type].TableID = 2728
WHERE			1 = 1
AND				[Disbursement].[RecordID] = @RecordID
AND				[Disbursement].IsActive = 1
AND				[Disbursement].TableID = 2725
AND				[Disbursement].V006 IS NULL;


-- Compute Converted Rate and Amount
Update [Record]
SET V022 =  Round(cast(ISNULL(V005, '0' )  AS DEC(10,2))  *  cast(@CurrencyRate AS DEC(10,2)),2)
,V023 = Round(cast(V005 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)) * cast(V004 AS money),2)
WHERE RECORDID = @RecordID



/*	Insert/update the Timesheet Detail entry	*/

/*	SELECT [Timesheet Details] */

IF EXISTS	(	SELECT			[Timesheet Details].RecordID	AS				[Record ID]
					FROM		[Record] [Timesheet Details]
					WHERE		[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
					AND			[Timesheet Details].IsActive = 1
					AND			[Timesheet Details].V010 = CAST( @RecordID AS VARCHAR(MAX))			/*	[Timesheet]	*/
			)
BEGIN

	/*	UPDATE [Timesheet Details] */
	
	
	UPDATE			[Timesheet Details] 			/*	[Timesheet Details]	*/
		SET			DateTimeRecorded = [Disbursement].DateTimeRecorded
					,EnteredBy = [Disbursement].EnteredBy
					,IsActive = [Disbursement].IsActive
					,DateUpdated = [Disbursement].DateUpdated
					,V001 = [Disbursement].V001				/*	[Job No]	*/
					,V009 = [Disbursement].V002				/*	[Date]	*/
					,V002 = [Disbursement].V019				/*	[Initials]	*/
					,V003 = [Disbursement].V006				/*	[Details]	*/
					,V004 = NULL							/*	[(X)]	*/

					,V005 = [Disbursement].V004				/*	[Hr/Unit]	*/
					,V006 = Round(cast(Disbursement.V005 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)),2)
					--,V006 = [Disbursement].V005				/*	[Rate]	*/
					--,V012 = [Timesheet].V018				/*	[GST]	*/
					,V007 = Round(cast(Disbursement.V005 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)) * cast(Disbursement.V004 AS money),2)
					--,V007 = [Disbursement].V007				/*	[$ Amount]	*/
					,V008 = 'Fixed'							/*	[Status]	*/
					,V010 = [Disbursement].RecordID			/*	[Disbursement]	*/
					--,V011 = [Disbursement].RecordID		/*	[Timesheet]	*/
					,V022 = '2'
					-- Dummy
					--,V028 = NULL
					--,V029 = [Disbursement].V004
					--,V030 = Round(cast(Disbursement.V005 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)),2)
					--,V031 = Round(cast(Disbursement.V005 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)) * cast(Disbursement.V004 AS money),2)
					--,[V032] = 'Fixed'
					--,[V033] = [Disbursement].V001
					,[IsReadOnly] = 1
					,V040 = cast(Disbursement.V005 AS DEC(10,2))
		FROM		[Record] [Timesheet Details]
		JOIN		Record [Disbursement] ON					[Disbursement].TableID = '2725'		/*	Disbursement	*/
														AND		[Disbursement].IsActive = 1
														AND		CAST( [Disbursement].RecordID AS VARCHAR(MAX)) = [Timesheet Details].V010		/*	V010	*/
														AND		[Disbursement].RecordID = @RecordID
		WHERE		[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
		AND			[Timesheet Details].IsActive = 1
	
END
ELSE
BEGIN

	/*	Insert Disbursement	*/

	INSERT INTO		[Record] 			/*	[Timesheet Details]	*/
		(			[TableID]
					,DateTimeRecorded
					,EnteredBy
					,IsActive
					,DateUpdated
					,V001				/*	[Job No]	*/
					,V009				/*	[Date]	*/
					,V002				/*	[Initials]	*/
					,V003				/*	[Details]	*/
					,V004				/*	[(X)]	*/
					,V005				/*	[Hr/Unit]	*/
					,V006				/*	[Rate]	*/
					,V007				/*	[$ Amount]	*/
					,V008				/*	[Status]	*/
					,V010				/*	[Disbursement]	*/
					,DateAdded
					,V022
					-- Dummy
					--,V028  
					--,V029  
					--,V030  
					--,V031
					--,[V032]  
					--,[V033]  
					,[IsReadOnly]
					,V040
		)
	SELECT			'2729'		/*	Timesheet Details	*/
					,[Disbursement].DateTimeRecorded
					,[Disbursement].EnteredBy
					,[Disbursement].IsActive
					,[Disbursement].DateUpdated
					,[Disbursement].V001		AS				[Job]
					,[Disbursement].V002		AS				[Date]
					,[Disbursement].V019		AS				[Employee]
					,[Disbursement].V006		AS				[Details]
					,NULL						AS				[(X)]
					,[Disbursement].V004						AS				[Unit]
					,Round(cast(Disbursement.V005 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)),2) AS [Rate]
					--,[Disbursement].V005						AS				[Rate]
					,Round(cast(Disbursement.V005 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)) * cast(Disbursement.V004 AS MONEY),2) /*	[$ Amount]	*/		AS				[Amount]
					--,[Disbursement].V007		AS				[$_Amount]
					,'Fixed'					AS				[Status]
					,CAST([Disbursement].RecordID AS VARCHAR)	AS				[Disbursement]
					,[Disbursement].DateAdded
					,'2'
					--,NULL
					--,[Disbursement].V004
					--,Round(cast(Disbursement.V005 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)),2) AS [Rate]
					--,Round(cast(Disbursement.V005 AS DEC(10,2)) * cast(@CurrencyRate AS DEC(10,2)) * cast(Disbursement.V004 AS MONEY),2) /*	[$ Amount]	*/		AS				[Amount]
					--,'Fixed'
					--,[Disbursement].V001
					,1
					,cast(Disbursement.V005 AS DEC(10,2))
		FROM		[Record] [Disbursement]
		WHERE		[Disbursement].TableID = '2725'		/*	Disbursement	*/
		AND			[Disbursement].IsActive = 1
		AND			[Disbursement].V030 = '2' /* Is Administration = Yes */
		AND			[Disbursement].RecordID = @RecordID;
END

/*	Update the GST */

UPDATE			[Timesheet Details] 									/*	[Timesheet Details]	*/
	SET			V012 = CAST( CAST( REPLACE([Timesheet Details].V007, ',', '')  AS DEC(20,4) ) * @TaxRate AS VARCHAR(MAX) )				/*	[GST]	*/
--SELECT			[Timesheet Details].V007
	FROM		[Record] [Timesheet Details]
	WHERE		[Timesheet Details].TableID = '2729'		/*	Timesheet Details	*/
	AND			[Timesheet Details].IsActive = 1
	AND			ISNUMERIC( ISNULL( [Timesheet Details].V007, 0 ) ) = 1
	AND			[Timesheet Details].V010 = CAST( @RecordID AS VARCHAR(MAX));

RETURN @@ERROR;

