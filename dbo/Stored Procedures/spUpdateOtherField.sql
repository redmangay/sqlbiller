﻿

CREATE  PROCEDURE [dbo].[spUpdateOtherField]
(
	@MainField varchar(254),
	@OtherField varchar(254),
	@MainTable int,
	@LookupTable int
)
AS 

BEGIN TRY
	DECLARE @sSQL varchar(4000)
	SET @sSQL = 
	'UPDATE [Record] SET ' + 
		@OtherField + ' = ' + @MainField + ', ' + 
		@MainField + ' = ''Other'' 
		WHERE TableID = ' + CAST(@MainTable as varchar) + ' and IsActive = 1 
		AND ' + @MainField + ' NOT IN 
		(SELECT V001 FROM [Record] WHERE TableID = ' + CAST(@LookupTable as varchar) + ' and IsActive = 1)'

	PRINT @sSQL
	EXEC (@sSQL)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('spUpdateOtherField', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
