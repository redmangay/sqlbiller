﻿
 
CREATE PROCEDURE [dbo].[dbg_Menu_FindLostMenuAndFix]

	/*----------------------------------------------------------------
	
SELECT A.MenuID FROM Menu A WHERE A.AccountID=24800 AND A.IsActive=1  
	AND A.ParentMenuID IN
	 (
		SELECT B.MenuID  FROM Menu B WHERE B.AccountID=24800 AND B.IsActive=1 
		AND (B.TableID IS NOT NULL OR B.DocumentID IS NOT NULL OR B.ExternalPageLink IS NOT NULL)
	 )



  EXEC [dbo].[dbg_Menu_FindLostMenuAndFix]
	---------------------------------------------------------------*/
 
AS
 
BEGIN TRY	  
	DECLARE Cur_AccountID CURSOR FOR
	SELECT AccountID FROM Account;

	DECLARE @nAccountID INT;

	OPEN Cur_AccountID;
	FETCH NEXT FROM	Cur_AccountID INTO @nAccountID;
	WHILE @@FETCH_STATUS = 0
	BEGIN
  
 
	   DECLARE @nNoneMenuID INT=NULL
   
	   SELECT TOP 1 @nNoneMenuID=MenuID FROM Menu WHERE AccountID=@nAccountID AND ParentMenuID IS NULL AND Menu='--None--'

	   -- TableID OR DocumentID OR ExternalPageLink should not be parent 
	   UPDATE Menu
	   SET ParentMenuID=@nNoneMenuID
	   WHERE MenuID IN
	   (  
			SELECT A.MenuID FROM Menu A WHERE A.AccountID=@nAccountID 
				AND A.ParentMenuID IN
								 (
									SELECT B.MenuID  FROM Menu B WHERE B.AccountID=@nAccountID 
									AND (B.TableID IS NOT NULL OR B.DocumentID IS NOT NULL OR B.ExternalPageLink IS NOT NULL)
								 )
		)


	   -- divider menu should not be parent  
	   UPDATE Menu
	   SET ParentMenuID=@nNoneMenuID
	   WHERE MenuID IN
	   (
		SELECT A.MenuID FROM Menu A WHERE A.AccountID=@nAccountID and A.ParentMenuID IN
			( SELECT B.MenuID FROM Menu B WHERE B.AccountID=@nAccountID AND Menu='---')
	   )
   
	   -- No Self parent
	   UPDATE Menu 
	   SET ParentMenuID=NULL WHERE MenuID IN 
			 (SELECT MenuID FROM [Menu] WHERE MenuID=ParentMenuID)


   			  
	  FETCH NEXT FROM Cur_AccountID INTO @nAccountID;
	END;
	CLOSE Cur_AccountID;
	DEALLOCATE Cur_AccountID;  

END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('dbg_Menu_FindLostMenuAndFix', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
