﻿

CREATE PROCEDURE   [dbo].[ets_Record_UpdateByBatch]
(
	@nBatchID int,
	@nUpdatedRowCount int output
)
AS

BEGIN TRY
	/*-------------------------------
	-- 1. Download the data 
	-- 2. User Updates the data
	-- 3. User upload the data
	-- 4. UPDATE and run this script
	DECLARE @nUpdatedRowCount INT
	EXEC DBO.spUpdateExistingData 25204, @nUpdatedRowCount output
	SELECT @nUpdatedRowCount

	-------------------------------*/
	SET dateformat dmy

	DECLARE @TableID int
	DECLARE @IsBatchUpdateAllowed bit
	DECLARE @IsTableUpdateAllowed bit
	DECLARE @UniqueField1ID int
	DECLARE @UniqueField2ID int
	DECLARE @UniqueField1SysName varchar(50)
	DECLARE @UniqueField2SysName varchar(50)
	DECLARE @UniqueField1ColumnType varchar(15)
	DECLARE @UniqueField2ColumnType varchar(15)

	SELECT  @TableID = TableID
		FROM [Batch] WHERE BatchID = @nBatchID 

	SELECT   @UniqueField1ID = UniqueColumnID, @UniqueField2ID = UniqueColumnID2
		FROM [Table] WHERE TableID = @TableID 

	IF (@UniqueField1ID IS NOT NULL)
	BEGIN 
		IF @UniqueField1ID IS NOT NULL
		BEGIN
			SELECT @UniqueField1SysName = SystemName, @UniqueField1ColumnType = ColumnType
				FROM [Column] WHERE ColumnID = @UniqueField1ID
		END
		IF @UniqueField2ID IS NOT NULL
		BEGIN
			SELECT @UniqueField2SysName = SystemName, @UniqueField2ColumnType = ColumnType
				FROM [Column] WHERE ColumnID = @UniqueField2ID
		END

		DECLARE @sSQL varchar(MAX)
		DECLARE @sSQL1 varchar(MAX)
		SELECT @sSQL = 'UPDATE Old SET '
		SELECT @sSQL = @sSQL + 'Old.' + SystemName + ' = ISNULL(New.' + SystemName + ', Old.' + SystemName + '), '
			FROM [Column] 
			WHERE [TableID] = @TableID AND SystemName <> @UniqueField1SysName AND SystemName <> ISNULL(@UniqueField2SysName, '')
				AND SystemName LIKE 'V%'

		--SELECT @sSQL = SUBSTRING(@sSQL, 1, LEN(@sSQL)-1)
		SELECT @sSQL= @sSQL + ' Old.WarningResults=New.WarningResults '

		SELECT @sSQL1 = ' 
			FROM [Record] Old
			JOIN [Record] New ON ' + 
				CASE @UniqueField1ColumnType
					WHEN 'date' THEN
						'CAST(New.' + @UniqueField1SysName + ' AS datetime) = CAST(Old.' + @UniqueField1SysName + ' AS datetime)'
					WHEN 'datetime' THEN
						'CAST(New.' + @UniqueField1SysName + ' AS datetime) = CAST(Old.' + @UniqueField1SysName + ' AS datetime)'
					ELSE
						'New.' + @UniqueField1SysName + ' = Old.' + @UniqueField1SysName
				END +
				CASE
					WHEN @UniqueField2ColumnType IS NULL THEN
						''
					WHEN @UniqueField2ColumnType = 'date' THEN
						' AND CAST(New.' + @UniqueField2SysName + ' AS datetime) = CAST(Old.' + @UniqueField2SysName + ' AS datetime)'
					WHEN @UniqueField2ColumnType = 'datetime' THEN
						' AND CAST(New.' + @UniqueField2SysName + ' AS datetime) = CAST(Old.' + @UniqueField2SysName + ' AS datetime)'
					ELSE
						' AND New.' + @UniqueField2SysName + ' = Old.' + @UniqueField2SysName
				END +
			' WHERE Old.[TableID] = ' + CAST(@TableID as varchar) + '
				AND (Old.BatchID != ' + CAST(@nBatchID AS varchar) + ' OR Old.BatchID IS NULL) 
				AND Old.IsActive = 1
			AND New.[TableID] = ' + CAST(@TableID as varchar) + ' 
				AND New.BatchID  = ' +CAST( @nBatchID AS varchar) + '
				AND New.IsActive = 1'

		SET @sSQL = @sSQL + @sSQL1
		--PRINT @sSQL 
		EXEC (@sSQL)

		SELECT @sSQL = 'UPDATE New SET New.IsActive = 0'
		SET @sSQL = @sSQL + @sSQL1
		--PRINT @sSQL 
		EXEC (@sSQL)
	END

	SELECT @nUpdatedRowCount= @@ROWCOUNT
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Record_UpdateByBatch', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
