﻿

CREATE PROCEDURE [dbo].[ets_Chart_GetData_StackBar]
       -- Add the parameters for the stored procedure here
       @nTableID int,
	   @sGraphXAxisColumnID varchar(MAX),
       @sValueFields varchar(MAX),
       @dStartDate datetime,
       @dEndDate datetime,
       @sPeriod varchar(20), -- Possible values: Year, Month, Day, Hour
       -- When you pass Year it will return the month averages
       -- when you pass Month it will return the day averages
       -- when you pass Week it will return the day averages
       -- when you pass day it will return hour aveages
       -- when you pass hour it will return all data
	   @GraphSeriesColumnID varchar(MAX),
	   @GraphSeriesID varchar(MAX),
	   @ForceAverage bit = 0,
       @bPercent bit = 0,
	   @nMaxRows int = NULL,
	   @sFilter varchar(MAX)
AS

BEGIN TRY
    SET NOCOUNT ON;
    --SET DATEFORMAT dmy  

    DECLARE @sql varchar(MAX)

	IF @ForceAverage = 0 -- then we get parameter series value
	BEGIN
		IF @nMaxRows IS NOT NULL
			SET @sql = 'SELECT TOP ' + CAST(@nMaxRows as varchar) + ' '+ @sGraphXAxisColumnID + ','
		ELSE
			SET @sql = 'SELECT '+ @sGraphXAxisColumnID + ','

        SET @sql = @sql + 'AVG(CAST(' + @sValueFields + ' AS float)) AS ' + CASE WHEN @GraphSeriesID IS NULL THEN 'Series' WHEN LEN(@GraphSeriesID) = 0 
		THEN 'Series' ELSE ''''+ @GraphSeriesID +'''' END +
			' FROM (SELECT CAST((SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')) AS date) AS '+ @sGraphXAxisColumnID +', '+ @sValueFields +' FROM [Record]' + 
			' INNER JOIN ('+ @sFilter +') tblFilter ON [Record].RecordID = tblFilter.DBGSystemRecordID' +
			--' WHERE IsActive=1 AND DateTimeRecorded BETWEEN ''' + CAST(@dStartDate AS Varchar) + ''' AND ''' +  CAST(@dEndDate AS Varchar) + '''' +
			' WHERE IsActive=1 AND CAST((SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')) AS datetime) >= '''+ CONVERT(varchar(20), @dStartDate) +'''' +
			' AND CAST((SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')) AS datetime) <= '''+ CONVERT(varchar(20), @dEndDate) +'''' +
			' AND TableID = ' + CAST(@nTableID AS Varchar) +
			CASE
				WHEN @GraphSeriesColumnID IS NULL THEN ''
				WHEN @GraphSeriesID IS NULL THEN ''
				WHEN LEN(@GraphSeriesColumnID) = 0 THEN ''
				WHEN LEN(@GraphSeriesID) = 0 THEN ''
				ELSE ' AND [' + @GraphSeriesColumnID + '] = ''' + @GraphSeriesID + ''''
			END +
			') AS Record' +
			' GROUP BY '+ @sGraphXAxisColumnID + 
			' ORDER BY '+ @sGraphXAxisColumnID + ' DESC'
	END
	ELSE -- we are going to get data of all site
	BEGIN
		IF @nMaxRows IS NOT NULL
			SET @sql = 'SELECT TOP ' + CAST(@nMaxRows as varchar) + ' CAST((SELECT dbo.[fnStringToDate](' + @sGraphXAxisColumnID + ')) AS date) AS ' + @sGraphXAxisColumnID + ', '
		ELSE
			SET @sql = 'SELECT CAST((SELECT dbo.[fnStringToDate](' + @sGraphXAxisColumnID + ')) AS date) AS ' + @sGraphXAxisColumnID + ', '

		SELECT @GraphSeriesColumnID = SystemName FROM [Column] WHERE ColumnID = (SELECT GraphSeriesColumnID FROM [Table] WHERE TableID = @nTableID)

        SET @sql = @sql + 'AVG(CAST(' + @sValueFields + ' AS float)) AS ' + @sValueFields + ', MAX(' + @GraphSeriesColumnID + ') AS ' + @GraphSeriesColumnID +
			' FROM [Record] INNER JOIN ('+ @sFilter +') tblFilter ON [Record].RecordID = tblFilter.DBGSystemRecordID' +
			--' WHERE IsActive=1 AND DateTimeRecorded BETWEEN ''' + CAST(@dStartDate AS Varchar) + ''' AND ''' +  CAST(@dEndDate AS Varchar) + '''' +
			' WHERE IsActive=1 AND CAST((SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')) AS datetime) >= ''' + CONVERT(varchar(20), @dStartDate) + '''' + 
			' AND CAST((SELECT dbo.[fnStringToDate]('+ @sGraphXAxisColumnID +')) AS datetime) <= ''' + CONVERT(varchar(20), @dEndDate) + '''' +
			' AND TableID = ' + CAST(@nTableID AS Varchar) + ' GROUP BY ' + @sGraphXAxisColumnID

		DECLARE @pivotSQL nvarchar(MAX)
		DECLARE @series nvarchar(MAX)

		SET @pivotSQL = 'SELECT @pivotColumn = COALESCE(@pivotColumn + '','','''') + QUOTENAME(' + @GraphSeriesColumnID + ') FROM (SELECT DISTINCT ' + @GraphSeriesColumnID +
		' FROM [Record] WHERE TableID = '+CAST(@nTableID AS Varchar)+') AS graphSeries'
		
		PRINT @pivotSQL
		EXEC sp_executesql @pivotSQL, N'@pivotColumn nvarchar(MAX) OUTPUT', @series OUTPUT
		
		--SELECT @series

		SET @sql = 'SELECT * FROM (' + @sql + ') AS source PIVOT (AVG(' + @sValueFields + ') FOR ' + @GraphSeriesColumnID + ' IN (' + @series + ')) AS tblPivot' +
					' ORDER BY ' + @sGraphXAxisColumnID + ' DESC'
    END

	PRINT @sql
	EXEC(@sql)
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_Chart_GetData_StackBar', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
