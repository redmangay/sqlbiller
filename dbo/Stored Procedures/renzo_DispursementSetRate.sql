﻿
CREATE PROCEDURE		[dbo].[renzo_DispursementSetRate]
(
	@RecordID  int,
	@FormSetID int = null,
    @UserID int=null,
    @Return varchar(max) output
)
AS

/*
		Set/update the rate based on the dispursement based on the disbursement rate

		DECLARE			@Return		VARCHAR(MAX)
		EXEC			[renzo_DispursementSetRate] @RecordID = 2072086, @Return = @Return

		EXEC [dbo].[spGetTableSQL] 2478, 'Disbursement Rate'
		EXEC [dbo].[spGetTableSQL] 2725, 'Disbursement'
		EXEC [dbo].[spGetTableSQL] 2728, 'Disbursement Type'

*/

--				Set Rate
UPDATE			[Disbursement]
SET				[Disbursement].V005 = ISNULL([Disbursement Rate].V002, '0' )
FROM			[Record] [Disbursement]
JOIN			[Record] [Disbursement Rate] ON				1 = 1
													AND		[Disbursement Rate].RecordID = [Disbursement].V003
													AND		[Disbursement Rate].IsActive = 1
													AND		[Disbursement Rate].TableID = 2478
JOIN			[Record] [Disbursement Type] ON				1 = 1
													AND		[Disbursement Type].RecordID = [Disbursement Rate].V001
													AND		[Disbursement Type].IsActive = 1
													AND		[Disbursement Type].TableID = 2728
WHERE			1 = 1
AND				[Disbursement].[RecordID] = @RecordID
AND				[Disbursement].IsActive = 1
AND				[Disbursement].TableID = 2725;

RETURN @@ERROR;

