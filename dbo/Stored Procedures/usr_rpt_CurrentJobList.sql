﻿
-- Author:		<Manoj sharma>
-- Create date: <12 March 2016>
-- Description:	<Used as ssrs report and in procedure rptcurrentjoblist>
CREATE procedure [dbo].[usr_rpt_CurrentJobList]
AS
BEGIN
     
exec usr_rpt_ProjectValueConverted
exec usr_rpt_CurrentJobListWithInvoicsAndPayment

IF EXISTS ( SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..##CurrentJobList' )) DROP TABLE ##CurrentJobList

create table ##CurrentJobList  (
ProjectNumber varchar(max),
ProjectTitle varchar(max),
Date varchar(max),
Controller varchar(max),
[Group/Division] varchar(max),
Status varchar(max),
CurrencyRate varchar(max),
Name varchar(max),
Balanced float,
Approved float ,
Invoiced float,
PaymentBalanced float)

 insert into ##CurrentJobList
 SELECT     ProjectValueConverted.ProjectNumber, ProjectValueConverted.ProjectTitle, ProjectValueConverted.Date, Employee_List.Initials AS Controller, ProjectValueConverted.[Group/Division], 
                      ProjectValueConverted.Status, ProjectValueConverted.CurrencyRate, Employee_List.Name, CASE WHEN isnull([ProjectValue], '0') = 0 THEN 0.0 ELSE [ProjectValue] - [Invoiced] END AS Balanced, 
                      ISNULL(ProjectValueConverted.ProjectValue, '0.0') AS Approved, CurrentJobListWithInvoicesAndPayment.Invoiced, 
                      CurrentJobListWithInvoicesAndPayment.Invoiced - CurrentJobListWithInvoicesAndPayment.Payment AS PaymentBalanced
FROM        ##ProjectValueConverted AS ProjectValueConverted LEFT OUTER JOIN
                      ##CurrentJobListWithInvoicsAndPayment AS CurrentJobListWithInvoicesAndPayment ON 
                      ProjectValueConverted.ProjectNumber = CurrentJobListWithInvoicesAndPayment.[Project Number] INNER JOIN
                      Account24918.vEmployee_List AS Employee_List ON ProjectValueConverted.EmployeeID = Employee_List.EmployeeID

   
   
  
   
END;
