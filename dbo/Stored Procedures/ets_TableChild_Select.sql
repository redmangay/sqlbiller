﻿


 
CREATE PROCEDURE   [dbo].[ets_TableChild_Select]
(
	@nParentTableID int  
)
	/*----------------------------------------------------------------
	EXEC [ets_TableChild_Select] 1331
	---------------------------------------------------------------*/
 
AS

BEGIN TRY 
	SELECT	[DisplayOrder]
			,[ParentTableID]
			,[TableChildID]
			,[Description]
			,[DetailPageType]
			,[ChildTableID]
			,[ShowAddButton]
			,[ShowEditButton]
			,[ConditionColumnID]
			,[ConditionValue]
			,[ConditionOperator]
			,(SELECT TableName FROM [Table] WHERE TableID=ChildTableID) AS [ChildTableName]
			,[FilterSQL]
			FROM TableChild
	WHERE ParentTableID = @nParentTableID 
	ORDER BY ParentTableID,DisplayOrder
END TRY
BEGIN CATCH
	DECLARE @ErrorTrack varchar(MAX) =
	 'ErrorNumber: '       +  ISNULL(CAST(ERROR_NUMBER() AS varchar), '0') + 
     '. ErrorSeverity: '   + ISNULL(CAST(ERROR_SEVERITY() AS varchar), '0') + 
     '. ErrorState: '      + ISNULL(CAST(ERROR_STATE() AS varchar), '0') + 
     '. ErrorLine:'        + ISNULL(CAST(ERROR_LINE() AS varchar), '0') 

	 INSERT INTO [ErrorLog](Module, ErrorMessage, ErrorTrack, ErrorTime, [Path]) 
		VALUES ('ets_TableChild_Select', ERROR_MESSAGE(), @ErrorTrack, GETDATE(), 'Stored Procedure on ' + DB_NAME())
END CATCH
